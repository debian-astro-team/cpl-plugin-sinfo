# import the needed modules
try:
  import reflex
  import_sucess = 'true'

# Then here is python code moved in xsh_object_interactive_common.py

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"


# import the needed modules
try:
#  import sys
  import numpy
  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits
    
  import sinfo_plot_common
  import sinfo_parameters_common
#  import wx
#  import matplotlib
  import reflex
  #from xsh_plot_common import *
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  import reflex_plot_widgets

  from pipeline_product import PipelineProduct
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:

    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames
    def readFitsData(self, fitsFiles):
      #print 'read fits data'
      #Control variable to check if the interesting files where at the input
      #print fitsFiles
      self.prepared = False
      self.rwave_coef_found = False
 
      self.mflat_found = False
      self.rresampled_wave = None
      self.rwave_lamp_stacked = None
      self.rwave_coef = None

      self.nima = 0
      self.binx = 1
      self.ext_ima=1
      self.biny = 1
      self.arm_id=0
      self.qc_id='SLIT_POS'
      self.rresampled_wave_found = False
      
      self.product_id = 'RESAMPLED'
      #self.labels_prod=['RESAMPLED','Diff RESAMPLED-Ref', 'C0','C1','C2','C3']
      self.labels_qc=['SLIT_POS','HOTPIX']

      #Read all the products
      frames = dict()
      #print 'ok1'
      #print fitsFiles
      for frame in fitsFiles:
        if frame == '' :
          continue
        #print frame.name
        header = pyfits.open(frame.name)
        #Make sure to have only products from the same recipe
        #that used this (common) script 
        if 'ESO PRO REC1 ID' in header[0].header :
           rec_id = header[0].header['ESO PRO REC1 ID']
           #print "rec_id=", rec_id,self.rec_id
           if rec_id == self.rec_id :
              category = frame.category
              frames[category] = frame
              print "frame name:", frame.name, "category: ", category
 
# For any arm search a list of input frames
      #print frames
      key = "SLIT_POS"
      if key in frames :
        self.slit_pos_found = True
        hdulist = frames[key]
        self.slit_pos = PipelineProduct(hdulist)
        print "found", key

      key = "WAVE_MAP"
      if key in frames :
        self.wave_map_found = True
        hdulist = frames[key]
        self.wave_map = PipelineProduct(hdulist)
        print "found", key

      key = "RWAVE_MAP"
      if key in frames :
        self.rwave_map_found = True
        hdulist = frames[key]
        self.rwave_map = PipelineProduct(hdulist)
        print "found", key


      key = "RESAMPLED_WAVE"
      if key in frames :
        self.resampled_wave_found = True
        self.ext_ima=0
        hdulist = frames[key]
        self.resampled_wave = PipelineProduct(hdulist)
        print "found", key


      key = "RRESAMPLED_WAVE"
      if key in frames :
        self.rresampled_wave_found = True
        hdulist = frames[key]
        self.rresampled_wave = PipelineProduct(hdulist)
        print "found", key


      key = "WAVE_LAMP_STACKED"
      if key in frames :
        self.wave_lamp_stacked_found = True
        hdulist = frames[key]
        self.wave_lamp_stacked = PipelineProduct(hdulist)
        print "found", key


      key = "RWAVE_LAMP_STACKED"
      if key in frames :
        self.rwave_lamp_stacked_found = True
        hdulist = frames[key]
        self.rwave_lamp_stacked = PipelineProduct(hdulist)
        print "found", key



      key = "WAVE_COEF_SLIT"
      if key in frames :
        self.wave_coef_found = True
        hdulist = frames[key]
        self.wave_coef = PipelineProduct(hdulist)
        print "found", key

      key = "RWAVE_COEF_SLIT"
      if key in frames :
        self.rwave_coef_found = True
        hdulist = frames[key]
        self.rwave_coef = PipelineProduct(hdulist)
        print "found", key

      if self.rresampled_wave_found == True :
           self.labels_prod=['RESAMPLED','Diff RESAMPLED-Ref', 'C0','C1','C2','C3']
      else :   
           self.labels_prod=['RESAMPLED', 'C0','C1','C2','C3']
        
    #Set rec id (to have proper recipe parameters prefix)
    def setRecId(self, rec_id):
      #Recipe ID variable to properly define params
      self.rec_id = rec_id

    #Get arm setting
    def getArmId(self, sof):
      #Recipe ID variable to properly define params
      self.rec_id = "sinfo_rec_wavecal"
      nf = 0

      frames = dict()
      files = sof.files
      for f in files:
        frame=f.name
        if frame == '' :
          continue
        else :
           nf += 1
           hdulist = pyfits.open(frame)
           rec_id_list = hdulist[0].header['ESO PRO REC1 ID']
      if nf != 0 :
         self.rec_id = rec_id_list[0:]

      #print "self.rec_id", self.rec_id
      if self.rec_id == "sinfo_rec_wavecal" :
         self.rec_subtitle = "Master Flat Creation. "
             
    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      nrows=self.nima
      row=1;

      if self.wave_map_found == True : 
        self.subplot_com  = figure.add_subplot(1,1,1)
        row +=1

        self.product_selector = figure.add_axes([0.00, 0.80, 0.07, 0.20])

      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)

    def plotData(self,arm_id):
        if self.product_id != 'RESAMPLED' :
           self.plotProductsGraphics()

    def plotWidgets(self) :
        widgets = list()
        if self.wave_map_found == True : 

           labels_prod = self.labels_prod
           self.radiobutton_pro = reflex_plot_widgets.InteractiveRadioButtons(self.product_selector, self.setProductSelectCallBack, labels_prod, 0, title="")
           widgets.append(self.radiobutton_pro)

        return widgets


    def setProductSelectCallBack(self, product_id) :
        print "Selected product id:", product_id
        self.product_id = product_id
        self.subplot_com.cla()
        self.displayProducts()   
        print "product_id:", self.product_id
         
    def setPlotSpectrum(self,obj,qual) :
        self.plotImageSlice(obj,qual,self.subplot_spectrum,title,tooltip,Xlab,Ylab);


    def prepTitleAndTooltipImage(self) :
        title_pref = 'Linear-extracted and Merged Spectrum.'	      
        title_spectrum   = title_pref

        if self.product_id == 'C0' :
           self.obj = self.slit_pos
           spec_frame   = 'Arc Lamp '
           title_wave   = 'Arc Lamp'
           self.tooltip_wave ="""\
                Image of the overscan corrected arc lamp frame. 
                """
        elif self.product_id == 'C1' :
           spec_frame   = 'Wave map '
           title_wave   = 'Wave map'
           self.tooltip_wave ="""\
                Image of the wave map frame. 
                """
           self.obj = self.wave_map
        if self.product_id == 'RESAMPLED' :
           spec_frame   = 'RESAMPLED '
           title_wave   = 'Resampled'
           self.tooltip_wave ="""\
                Image of the resampled wave map frame. 
                """
           self.obj = self.resampled_wave

        self.title_frame   = spec_frame+'image'

    def caseNodata(self) :
        #Data not found info
        self.subtext_nodata.set_axis_off()
        self.text_nodata = """\
                           The python GUI miss some required input frame
                           This may be due to a recipe failure or to missing
                           reference frames.
                           Check your input parameter values, 
                           correct possibly typos and
                           press 'Re-run recipe' button."""
        self.subtext_nodata.text(0.1, 0.6, self.text_nodata, color='#11557c', 
                                 fontsize=18, ha='left', va='center', alpha=1.0)
        self.subtext_nodata.tooltip="""\
                                    Merged spectrum not found in the products"""
        #print "found no spectrum data"


    def plotResampledWave(self,mast,Rmast,ref,Rref,subplot):
        #======================================================================
        # Plot 2:  Plotting the reference resampled WAVE image
        #======================================================================

        mast.readImage(0)
        if Rmast != None :
           Rmast.readImage(0)
        if ref != None :
           ref.readImage(0)
        if Rref != None :
           Rref.readImage(0)


        #z1 = MRmean - 0.25*MRstd
        #z2 = MRmean + 0.50*MRstd


        #imshow(MRref, vmin=z1, vmax=z2, interpolation='nearest', cmap=cm.hot, origin='lower')

        #xlim(0,nxR)
        #ylim(0,nyR)
        #xlabel('x-axis (pixels)')
        ##ylabel('y-axis (pixels)')
        #yticks([])
        title1 = ""
	tooltip = ""
        #title1 = 'reference: ' + ref_resamp_name
        #title(title1, fontsize=10)
        self.dpm.plotImageOnly(mast,0, 1, 1,'X [pix]', 'Y [pix]', subplot,title1,tooltip)

    def plotResampledWaveR(self,mast,Rmast,ref,Rref,subplot):
        #======================================================================
        # Plot 2:  Plotting the reference resampled WAVE image
        #======================================================================


        mast.readImage(0)
        Rmast.readImage(0)
        ref.readImage(0)
        Rref.readImage(0)



        #z1 = MRmean - 0.25*MRstd
        #z2 = MRmean + 0.50*MRstd
        #imshow(MRref, vmin=z1, vmax=z2, interpolation='nearest', cmap=cm.hot, origin='lower')

        #xlim(0,nxR)
        #ylim(0,nyR)
        #xlabel('x-axis (pixels)')
        ##ylabel('y-axis (pixels)')
        #yticks([])

        title1=''
        #title1 = 'reference: ' + ref_resamp_name
        #title(title1, fontsize=10)
	tooltip=''
        self.dpm.plotImageOnly(Rref,0, 1, 1,'X [pix]', 'Y [pix]', subplot,title1,tooltip)

    def plotResampledWaveDiff(self,mast,Rmast,ref,Rref,subplot):
        #======================================================================
        # Plot 3:  Plotting the difference between the current resampled WAVE image
        #          (master) and the reference resampled WAVE image (reference)
        #======================================================================

        #subplot(233)
        mast.readImage(0)
        Rmast.readImage(0)
        ref.readImage(0)
        Rref.readImage(0)

        findNAN_M = numpy.isnan(mast.image)
	findNAN_RM  = numpy.isnan(Rmast.image)     
        findNAN_R = numpy.isnan(ref.image)
        findNAN_RR = numpy.isnan(Rref.image)


	Nnan   =  numpy.sum(numpy.sum(findNAN_M))
        Mmast  = numpy.ma.array(mast.image, mask=findNAN_M, fill_value=[0.0])
        MRmast = numpy.ma.array(Rmast.image, mask=findNAN_RM, fill_value=[0.0])
        Mref   = numpy.ma.array(ref.image, mask=findNAN_R, fill_value=[0.0])
        MRref  = numpy.ma.array(Rref.image, mask=findNAN_RR, fill_value=[0.0])

        Mmast  = Mmast.filled()
        MRmast = MRmast.filled()
	Mref   = Mref.filled()
	MRref  = MRref.filled()

        Mmean=MRmast.mean()
        MRmean=Rmast.image.mean()
        MRmean=MRref.mean()

	nyMR, nxMR = Rmast.image.shape
	MRref = numpy.resize(Rref.image, (nyMR, nxMR))
	print '[info] a total of',Nnan,'nan pixels were discovered and set to zero.'

	
        diff  = MRref*(Mmean/MRmean) - MRmast
        #diff = MRref * (Mmean/MRmean) - Rmast.image
	#diff = 0
        diffmean = diff.mean()
        diffstd  = diff.std()

        z1 = diffmean - 0.25*diffstd
        z2 = diffmean + 0.50*diffstd
        print 'z1=',z1,'z2=',z2
        #diffima = imshow(diff, vmin=z1, vmax=z2, interpolation='nearest', cmap=cm.hot, origin='lower')
        #diffima = imshow(diff)
        #colorbar(diffima, pad=0.075, shrink=0.75)

        #xlim(0,nxR)
        #ylim(0,nyR)
        #xlabel('x-axis (pixels)')
        #yticks([])

        #title('Reference - Current (Resampled WAVE)', fontsize=12)


        #if (plot233 == 1):
	#    x2 = nyR + 0.03*nyR
	#    text(x2, 0, 'Reference & Current NOT same size!', color='red', size=10, rotation='vertical')
        title=''
        tooltip=''
	
	#imshow(diff)
        #self.dpm.plotImageOnly(diff,0, 1, 1,'X [pix]', 'Y [pix]', subplot,title1,tooltip)
        img_disp = ImageDisplay()
	img_disp.display(subplot, title, self.dpm.paragraph(tooltip),diff)


    def plotGra1(self,mast,ref,subplot):
    
        #=======================================================================
        # PLOT 1 (231):  Plotting the current FLAT image (master) and reference collapsed along columns
        #=======================================================================
        #subplot(231)
        nyR, nxR = ref.image.shape
        nyM, nxM = mast.image.shape
        Mcol = mast.image.sum(axis=1)/float(nxM)
        Rcol = ref.image.sum(axis=1)/float(nxR)
        xcol2 = numpy.arange(nyM)

        colM1 = str(0.10)         # current master (single column at x=1/4)
        colM2 = str(0.40)         # current master (single column at x=2/4)
        colM3 = str(0.70)         # current master (single column at x=3/4)


        #plot(xcol2, mast[:,250], color=colM1, alpha=0.5)
        #plot(xcol2, mast[:,500], color=colM2, alpha=0.5)
        #plot(xcol2, mast[:,750], color=colM3, alpha=0.5)

        subplot.plot(xcol2, mast.image[:,500], color=colM1, alpha=0.5)
        subplot.plot(xcol2, mast.image[:,1000], color=colM2, alpha=0.5)
        subplot.plot(xcol2, mast.image[:,1500], color=colM3, alpha=0.5)

        subplot.plot(xcol2, Rcol, color='red', linewidth=1.5)
        subplot.plot(xcol2, Mcol, color='blue', linewidth=1.5)

        xLim=0,nxM
        subplot.set_xlim(xLim)
        #subplot.set_ylim(yLim)
        YLab='column average (counts)'
        title1 = 'Column Average'
        subplot.set_title(title1, fontsize=12)
        #legend(('master x=500','master x=1000','master x=1500','reference','master'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)


    def readMCoefTab(self,mcoef):

        #=======================================================================
        # FIGURE 1: Coefficients C0, C1, C2, and C3 for the current and reference frames
        #=======================================================================


        mcoef.readTableColumn(fits_extension=1,colname='coeff0')
        tC0 =  mcoef.column

        mcoef.readTableColumn(fits_extension=1,colname='coeff1')
        tC1 =  mcoef.column

        mcoef.readTableColumn(fits_extension=1,colname='coeff2')
        tC2 =  mcoef.column

        mcoef.readTableColumn(fits_extension=1,colname='coeff3')
        tC3 =  mcoef.column
        
        Ncoef = len(tC0)
        tCN = numpy.zeros(Ncoef)
        
        for i in range(Ncoef):
            tCN[i]=i

        # I will do the same statistics on my own:

        coef0_med  = numpy.median(tC0)
        coef1_med  = numpy.median(tC1)
        coef2_med  = numpy.median(tC2)
        coef3_med  = numpy.median(tC3)
        
        coef0_mean = tC0.mean()
        coef1_mean = tC1.mean()
        coef2_mean = tC2.mean()
        coef3_mean = tC3.mean()
        
        coef0_std  = tC0.std()
        coef1_std  = tC1.std()
        coef2_std  = tC2.std()
        coef3_std  = tC3.std()


        C0top = tC0.max() - 0.02*(tC0.max() - tC0.min())
        self.C0top = C0top
        print 'C0top:',C0top
        print 'C0:',tC0[0:10]
        print len(tC0)
        
        self.tCN=tCN
        
        self.tC0=tC0
        self.tC1=tC1
        self.tC2=tC2
        self.tC3=tC3


    def readRCoefTab(self,rcoef):

        #=======================================================================
        # FIGURE 1: Coefficients C0, C1, C2, and C3 for the current and reference frames
        #=======================================================================



        rcoef.readTableColumn(fits_extension=1,colname='coeff0')
        tC0_ref =  rcoef.column

        rcoef.readTableColumn(fits_extension=1,colname='coeff1')
        tC1_ref =  rcoef.column

        rcoef.readTableColumn(fits_extension=1,colname='coeff2')
        tC2_ref =  rcoef.column

        rcoef.readTableColumn(fits_extension=1,colname='coeff3')
        tC3_ref =  rcoef.column

        coef0_med_ref = numpy.median(tC0_ref)
        coef1_med_ref = numpy.median(tC1_ref)
        coef2_med_ref = numpy.median(tC2_ref)
        coef3_med_ref = numpy.median(tC3_ref)
        
        coef0_std_ref = tC0_ref.std()
        coef1_std_ref = tC1_ref.std()
        coef2_std_ref = tC2_ref.std()
        coef3_std_ref = tC3_ref.std()

        print 'C0ref:',tC0_ref
        print len(tC0_ref)
        
        self.tC0_ref=tC0_ref
        self.tC1_ref=tC1_ref
        self.tC2_ref=tC2_ref
        self.tC3_ref=tC3_ref


        
    def plotPrepFig(self,mcoef,rcoef,subplot,i):

        #=======================================================================
        # FIGURE 1: Coefficients C0, C1, C2, and C3 for the current and reference frames
        #=======================================================================

            
        #figure(1,figsize=(15,10))     # set figure number (1) and size (inches: (X, Y))
        #figure(1,figsize=(11.8,8.0)) # figure size = A4 paper

        # Top line figure identification:
        #figtext(0.23,0.965, date +'   ' + raw_type +'   '+ ins_setup + '   ' + ins_opti1 + '    ' + AB, {'color' : 'r', 'fontsize' : 16})

        # Bottom line figure date (when produced):
        #lt = localtime()
        #year, month, day, hour, min, sec = lt[0:6]
        #today = str('%04i-%02i-%02i    %02i:%02i:%02i' %(year, month, day, hour, min, sec))
        #figtext(0.75,0.02, 'created   ' + today + '   ' + my_name, {'color' : 'r', 'fontsize' : 10})

        #fontLegend = FontProperties(size=9) # defining a smaller font size for plot legends

        #subplots_adjust(hspace=0.001)       # this closes the gaps between the vertically adjacent plots


            CCN     = []
            CC0     = []
            CC1     = []
            CC2     = []
            CC3     = []

            P1=self.P1
            P2=self.P2
            
            tCN=self.tCN
            
            tC0=self.tC0
            tC1=self.tC1
            tC2=self.tC2
            tC3=self.tC3

            for j in range(int(P1[i])+2, int(P2[i])-2, 1):

                CCN.append(tCN[j])

                CC0.append(tC0[j])
                CC1.append(tC1[j])
                CC2.append(tC2[j])
                CC3.append(tC3[j])

            self.CCN=CCN
            self.CC0=CC0
            self.CC1=CC1
            self.CC2=CC2
            self.CC3=CC3

            if rcoef != None :
               CC0_ref = []
               CC1_ref = []
               CC2_ref = []
               CC3_ref = []
               
               tC0_ref=self.tC0_ref
               tC1_ref=self.tC1_ref
               tC2_ref=self.tC2_ref
               tC3_ref=self.tC3_ref

               for j in range(int(P1[i])+2, int(P2[i])-2, 1):
                   CC0_ref.append(tC0_ref[j])
                   CC1_ref.append(tC1_ref[j])
                   CC2_ref.append(tC2_ref[j])
                   CC3_ref.append(tC3_ref[j])                

               self.CC0_ref=CC0_ref
               self.CC1_ref=CC1_ref
               self.CC2_ref=CC2_ref
               self.CC3_ref=CC3_ref

    def plotFig1(self,subplot,i):
                
            #subplot(411)
            slitorder = [9, 8, 10, 7, 11, 6, 12, 5, 13, 4, 14, 3, 15, 2, 16, 1, 32, 17, 31, 18, 30, 19, 29, 20, 28, 21, 27, 22, 26, 23, 25, 24]  # order of slitlets from left
            CCN=self.CCN
            CC0=self.CC0
            P1=self.P1
            C0top=self.C0top
            Nslitpos=self.Nslitpos
            if self.rwave_coef != None :
               CC0_ref=self.CC0_ref
               subplot.plot(CCN, CC0_ref, color='r', linestyle='-', linewidth=1)

            subplot.plot(CCN, CC0,     color='b', linestyle='-', linewidth=1)

            for i in range(Nslitpos):
                subplot.axvline(P1[i],     color='k', linestyle='-', linewidth=1, alpha=0.1)
                subplot.text(P1[i]+20,C0top, slitorder[i], fontsize=8) # labelling the slitlet positions
	    subplot.set_xlabel('row position')	
	    subplot.set_ylabel('column position')	
            subplot.tooltip='plot of wavelength calibration polynomial fit coefficient order 0 vs column position'

            title1='Wavelength Coefficients\n'+r'$\lambda\ =\ C_0 + C_1x + C_2x^2 + C_3x^3\ \ (where\ x = y\ -\ \frac{ny-1}{2})$'
          
            subplot.set_title(title1, fontsize=10)
    def plotFig2(self,subplot,i):
            #subplot(412)
            CCN=self.CCN
            CC1=self.CC1
            P1=self.P1
            Nslitpos=self.Nslitpos

            if self.rwave_coef != None :
               CC1_ref=self.CC1_ref
               subplot.plot(CCN, CC1_ref, color='r', linestyle='-', linewidth=1)
            subplot.plot(CCN, CC1,     color='b', linestyle='-', linewidth=1)

            for i in range(Nslitpos):
                subplot.axvline(P1[i],     color='k', linestyle='-', linewidth=1, alpha=0.1)
	    subplot.set_xlabel('row position')	
	    subplot.set_ylabel('column position')	
            subplot.tooltip='plot of wavelength calibration polynomial fit coefficient order 1 vs column position'
            title1='Wavelength Coefficients\n'+r'$\lambda\ =\ C_0 + C_1x + C_2x^2 + C_3x^3\ \ (where\ x = y\ -\ \frac{ny-1}{2})$'
          
            subplot.set_title(title1, fontsize=10)

    def plotFig3(self,subplot,i):
            #subplot(413)
            CCN=self.CCN
            CC2=self.CC2
            P1=self.P1
            Nslitpos=self.Nslitpos
            if self.rwave_coef != None :
               CC2_ref=self.CC2_ref
               subplot.plot(CCN, CC2_ref, color='r', linestyle='-', linewidth=1)
            subplot.plot(CCN, CC2,     color='b', linestyle='-', linewidth=1)

            for i in range(Nslitpos):
                subplot.axvline(P1[i],     color='k', linestyle='-', linewidth=1, alpha=0.1)
	    subplot.set_xlabel('row position')	
	    subplot.set_ylabel('column position')	
            subplot.tooltip='plot of wavelength calibration polynomial fit coefficient order 2 vs column position'
            title1='Wavelength Coefficients\n'+r'$\lambda\ =\ C_0 + C_1x + C_2x^2 + C_3x^3\ \ (where\ x = y\ -\ \frac{ny-1}{2})$'
          
            subplot.set_title(title1, fontsize=10)

    def plotFig4(self,subplot,i):
            #subplot(414)

            CCN=self.CCN
            CC3=self.CC3
            P1=self.P1
            P2=self.P2
            CN=self.tCN
            Nslitpos=self.Nslitpos
            C0top=self.C0top
            if self.rwave_coef != None :
               CC3_ref=self.CC3_ref
               subplot.plot(CCN, CC3_ref, color='r', linestyle='-', linewidth=1)
            subplot.plot(CCN, CC3,     color='b', linestyle='-', linewidth=1)
            for i in range(Nslitpos):
                subplot.axvline(P1[i],     color='k', linestyle='-', linewidth=1, alpha=0.1)


            #subplot(411)
            title=r'$\lambda\ =\ C_0 + C_1x + C_2x^2 + C_3x^3\ \ (where\ x = y\ -\ \frac{ny-1}{2})$'
            subplot.text(P2[Nslitpos-1]+15, C0top, 'slitlet number', fontsize=8)
            #AMO: TODO
            #subplot.legend(('reference', 'current frame'), 'center right', prop=fontLegend, handlelength=0.08, shadow=False)
            #xticks([])  # surpress x-axis tick marks on all but the bottom plot
            Ylab=r'$C_0$'
            xLim = CN.min(), CN.max()
            subplot.set_xlim(xLim)
            

            #subplot(412)
            #xticks([])
            Ylab=r'$C_1$'
            xLim = CN.min(), CN.max()
            subplot.set_xlim(xLim)
          

            #subplot(413)
            #xticks([])
            Ylab=r'$C_2$'
            xLim = CN.min(), CN.max()
            subplot.set_xlim(xLim)

            #subplot(414)
            Xlab='x-axis (pixels)'
            Ylab=r'$C_3$'
            xLim = CN.min(), CN.max()
            subplot.set_xlim(xLim)
	    subplot.set_xlabel('row position')	
	    subplot.set_ylabel('column position')	
            subplot.tooltip='plot of wavelength calibration polynomial fit coefficient order 3 vs column position'
            title1='Wavelength Coefficients\n'+r'$\lambda\ =\ C_0 + C_1x + C_2x^2 + C_3x^3\ \ (where\ x = y\ -\ \frac{ny-1}{2})$'
          
            subplot.set_title(title1, fontsize=10)




    def readSlitPos(self,spos):
        spos.readTableColumn(fits_extension=1,colname='pos1')
        P1 =  spos.column
        self.P1=P1
        Nslitpos = len(P1)
        self.Nslitpos = Nslitpos
        spos.readTableColumn(fits_extension=1,colname='pos2')
        P2 =  spos.column
        self.P2=P2
        
    def displayProducts(self):
        Nslitpos=self.Nslitpos
        subplot=self.subplot_com
	stack=self.wave_lamp_stacked
        resamp=self.resampled_wave
	rresamp=self.rresampled_wave
	rstack=self.rwave_lamp_stacked
	
        if self.product_id == 'C0' :
            for i in range(Nslitpos):
                self.plotPrepFig(self.wave_coef,self.rwave_coef,subplot,i)
                self.plotFig1(subplot,i)
        elif self.product_id == 'C1' :
            for i in range(Nslitpos):
                self.plotPrepFig(self.wave_coef,self.rwave_coef,subplot,i)
                self.plotFig2(subplot,i)

        elif self.product_id == 'C2' :
            for i in range(Nslitpos):
                self.plotPrepFig(self.wave_coef,self.rwave_coef,subplot,i)
                self.plotFig3(subplot,i)

        elif self.product_id == 'C3' :
            for i in range(Nslitpos):
                self.plotPrepFig(self.wave_coef,self.rwave_coef,subplot,i)
                self.plotFig4(subplot,i)

        elif self.product_id == 'RESAMPLED' :
	     self.plotResampledWave(stack,resamp,rstack,rresamp,subplot)

        elif self.product_id == 'Diff RESAMPLED-Ref' :
	     self.plotResampledWaveDiff(stack,resamp,rstack,rresamp,subplot)
 

    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
      if (self.wave_map_found == True) :
        self.prepTitleAndTooltipImage()
        #self.preparePlotQC()
        
        sy = self.obj.all_hdu[self.ext_ima].header['NAXIS2']
        self.ima_sy=sy
        self.dpm = sinfo_plot_common.DataPlotterManager()
        #self.plotGra1(self.mflat,self.rflat,self.subplot_gra1)
        self.readSlitPos(self.slit_pos)
        self.readMCoefTab(self.wave_coef)
        if self.rwave_coef_found == True :
           self.readRCoefTab(self.rwave_coef)
        self.displayProducts()
  
      else :
        self.caseNodata() 
  
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setInteractiveParameters(self):
      paramList = list()
      rec_id=self.rec_id
      self.par = sinfo_parameters_common.Parameters()

      if rec_id == "sinfo_rec_wavecal" :
        self.par.setWaveCalParameters(paramList,"sinfo_rec_wavecal","all")
        print "recipe" ,rec_id,  "no parameters"

      else :
         print "recipe" ,rec_id,  "not supported"

      return paramList

    def setWindowHelp(self):
      help_text = """
This is an interactive window which help asses the quality of the execution of a recipe.
"""
      return help_text

    def setWindowTitle(self):
      title = 'SINFONI Interactive ' + self.rec_subtitle  
      return title

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise

#This is the 'main' function
if __name__ == '__main__':

  # import reflex modules
  from reflex import *
  from reflex_interactive_app import *
  from pipeline_display import *
  from pipeline_product import *

  # Create interactive application
  interactive_app = PipelineInteractiveApp(enable_init_sop=True)

  # PECULIAR XSH needs this in order to be able later to get from an input FITS
  # the ins-mode, arm (and recipe) IDs, used in titles and param setting
  # get inputs from the command line
  interactive_app.parse_args()
  inputs = interactive_app.inputs
  #(inputs, args) = interactive_app.parse_args()

  #Check if import failed or not
  print "import_sucess=", import_sucess
  if import_sucess == 'false' :
     interactive_app.setEnableGUI(false)
    
  #interactive_app.setEnableGUI(True)
  #Open the interactive window if enabled
  if interactive_app.isGUIEnabled() :
    #Get the specific functions for this window
    dataPlotManager = DataPlotterManager()
    #print inputs.in_sof
    #dataPlotManager.checkSofIsNotEmpty(inputs.in_sof)
    #With the following call XSH get the: ins-mode, arm (and recipe) IDs
    dataPlotManager.getArmId(inputs.in_sof)
    #Set recipe ID in order to build proper param list, display layout
    dataPlotManager.setRecId("sinfo_rec_wavecal")
    interactive_app.setPlotManager(dataPlotManager)
    interactive_app.showGUI()
  
  else :
    interactive_app.passProductsThrough()
   
  #Print outputs. This is parsed by the Reflex python actor to get the results
  #Do not remove
  interactive_app.print_outputs()
  sys.exit()
