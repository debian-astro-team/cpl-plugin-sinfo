from reflex import RecipeParameter
class Parameters(RecipeParameter):

    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setGenParameters(self,paramList,rec_id,group):
        name1="lc_sw"
        help1="Correct for bad lines introduced by instrument software:. [FALSE]"
        paramList.append(RecipeParameter(rec_id,displayName=name1,group=group,description=help1))
       
    def setDarkParameters(self,paramList,rec_id,group):  
         #name1="lc_kappa"
         #help1="Kappa sigma value. [18]"
         #paramList.append(RecipeParameter(rec_id,displayName=name1,group=group,description=help1))

         #name2="lc_filt_rad"
         #help2="Filtering radii applied during median filter. Should be small. [3]"
         #paramList.append(RecipeParameter(rec_id,displayName=name2,group=group,description=help2))

         #name3="bp_noise-thresh_sigma_fct"
         #help3="Threshold Sigma Factor: If the mean noise exceeds this threshold times the clean standard deviation of the clean mean the corresponding pixels are declared as bad. [10.0]"
         #paramList.append(RecipeParameter(rec_id,displayName=name3,group=group,description=help3))

         name4="bp_noise-lo_rej"
         help4="low_rejection: percentage of rejected low intensity pixels before averaging. [10.0]"
         paramList.append(RecipeParameter(rec_id,displayName=name4,group=group,description=help4))

         
         name5="bp_noise-hi_rej"
         help5=" high_rejection: percentage of rejected high intensity pixels before averaging. [10.0]"
         paramList.append(RecipeParameter(rec_id,displayName=name5,group=group,description=help5))
         group2="stacking"
         name6="dark-lo_rej"
         help6="lower rejection. [0.1]"
         paramList.append(RecipeParameter(rec_id,displayName=name6,group=group2,description=help6))

         name7="dark-hi_rej"
         help7="higher rejection. [0.1]"
         paramList.append(RecipeParameter(rec_id,displayName=name7,group=group2,description=help7))


    
    def setDistortionParameters(self,paramList,rec_id,group):
      group1="bp"
      name1="bp-method"
      help1="Data reduction method:. <Normal | Linear | Noise> [Normal]"
      paramList.append(RecipeParameter(rec_id,displayName=name1,group=group1,description=help1))

      name2="bp_dist-lo_rej"
      help2="low_rejection: percentage of rejected low intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name2,group=group1,description=help2))

      name3="bp_dist-hi_rej"
      help3="high_rejection: percentage of rejected high intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name3,group=group1,description=help3))

      group2="dispersion"
      name4="dist-fwhm"
      help4="FWHM: initial guess value for the fwhm of the Gaussian used for the line fit. [2.83]"
      paramList.append(RecipeParameter(rec_id,displayName=name4,group=group2,description=help4))

      name5="dist-max_residual"
      help5="Maximum Residuals value: beyond this value the fit is rejected. [0.5]"
      paramList.append(RecipeParameter(rec_id,displayName=name5,group=group2,description=help5))

      name6="dist-n_a_coeffs"
      help6="Number of A coefficients: number of polynomial coefficients for the dispersion relation. [4]"
      paramList.append(RecipeParameter(rec_id,displayName=name6,group=group2,description=help6))

      name7="dist-n_b_coeffs"
      help7="Number of B coefficients: number of polynomial coefficients for the polynomial fit of the dispersion coefficients. [2]"
      paramList.append(RecipeParameter(rec_id,displayName=name7,group=group2,description=help7))

      name8="dist-sigma_factor"
      help8="Sigma Factor: Factor of the standard deviation of the polynomial coefficients of the dispersion relation beyond which the coefficients are not used for the fit. [1.5]"
      paramList.append(RecipeParameter(rec_id,displayName=name8,group=group2,description=help8))

      name9="dist-diff_toll"
      help9="Difference Tolearance: maximal tolerable difference of the resulting fit positions of the slitlet edges with respect to the expected positions. [2.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name9,group=group2,description=help9))

      group3="ns"
      name10="ns-lo_rejection"
      help10="lower rejection: percentage of rejected low intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name10,group=group3,description=help10))

      name11="ns-hi_rejection"
      help11="higher rejection: percentage of rejected high intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name11,group=group3,description=help11))

      name12="ns-lo_rej"
      help12="lower rejection: percentage of rejected low intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name12,group=group3,description=help12))

      name13="ns-hi_rej"
      help13="higher rejection: percentage of rejected high intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name13,group=group3,description=help13))




    def setDetlinParameters(self,paramList,rec_id,group):

      name1="bp_lin-order"
      help1="Order: order of the fit polynomial = number of coefficents - 1. [2]"
      paramList.append(RecipeParameter(rec_id,displayName=name1,group=group,description=help1))
     
      name2="bp_lin-nlin_threshold"
      help2="Non Linear Threshold. [0.5]"
      paramList.append(RecipeParameter(rec_id,displayName=name2,group=group,description=help2))

      name3="bp_lin-lo_rej"
      help3="low_rejection: percentage of rejected low intensity pixels before averaging. [10.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name3,group=group,description=help3))

      name4="bp_lin-hi_rej"
      help4="high_rejection: percentage of rejected high intensity pixels before averaging. [10.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name4,group=group,description=help4))


    def setFlatParameters(self,paramList,rec_id,group):
      group1="bp_norm"  
      name1="bp_norm-method_ind"
      help1="Bad pixel Method Index1: median of nearest neighbors,2: absolute distances check, 3: mean of nearest spectral neighbors. [1]"
      paramList.append(RecipeParameter(rec_id,displayName=name1,group=group1,description=help1))

      name2="bp_norm-lo_rej"
      help2="low_rejection: percentage of rejected low intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name2,group=group1,description=help2))

      name3="bp_norm-hi_rej"
      help3="high_rejection: percentage of rejected high intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name3,group=group1,description=help3))


      group2="stacking"  
      name4="lamp_flats-lo_rej"
      help4="lower rejection: percentage of rejected low intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name4,group=group2,description=help4))

      name5="lamp_flats-hi_rej"
      help5="high rejection: percentage of rejected high intensity pixels before averaging. [0.1]"
      paramList.append(RecipeParameter(rec_id,displayName=name5,group=group2,description=help5))


    def setWaveCalParameters(self,paramList,rec_id,group):
      print "wavecal"

      name1="wcal-hw"
      help1="half width of a box within which the line must be placed. [7]"
      paramList.append(RecipeParameter(rec_id,displayName=name1,group=group,description=help1))

      name2="wcal-sigma"
      help2="Sigma: sigma of Gaussian which is convolved with the artificial spectrum generated using the line list. [2.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name2,group=group,description=help2))

      name3="wcal-fwhm"
      help3="FWHM: initial guess value for the fwhm of the Gaussian used for the line fit. [2.83]"
      paramList.append(RecipeParameter(rec_id,displayName=name3,group=group,description=help3))

      name4="wcal-max_residual"
      help4="Maximum Residuals value: beyond this value the fit is rejected. [0.5]"
      paramList.append(RecipeParameter(rec_id,displayName=name4,group=group,description=help4))

      name5="wcal-n_a_coeffs"
      help5="Number of A coefficients: number of polynomial coefficients for the dispersion relation. [4]"
      paramList.append(RecipeParameter(rec_id,displayName=name5,group=group,description=help5))

      name6="wcal-n_b_coeffs"
      help6="Number of B coefficients: number of polynomial coefficients for the polynomial fit of the dispersion coefficients. [2]"
      paramList.append(RecipeParameter(rec_id,displayName=name6,group=group,description=help6))

      name7="wcal-sigma_factor"
      help7="Sigma Factor: Factor of the standard deviation of the polynomial coefficients of the dispersion relation beyond which the coefficients are not used for the fit."
      paramList.append(RecipeParameter(rec_id,displayName=name7,group=group,description=help7))

      name8="wcal-pixel_tol"
      help8="Pixel Tolerance: allowed pixel position tolerance between estimated and fitted line position. [5.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name8,group=group,description=help8))




    def setSciRedParametersOld(self,paramList,rec_id,group):
      print "parameters rec id",rec_id
      name1="ovsc_sigma_clipping_method"
      help1="method for sigma clipping in OVSC, can be mean or median. [mean]"
      paramList.append(RecipeParameter(rec_id,displayName=name1,group=group,description=help1))

      name2="ovsc_ksigma"
      help2="ksigma for sigma clipping in OVSC, must be between: 1.50 and 1000.00. [4.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name2,group=group,description=help2))

      name3="ovsc_max_iter"
      help3="maximal number of iterations in OVSC, must be between: 1 and 5000000. [10]"
      paramList.append(RecipeParameter(rec_id,displayName=name3,group=group,description=help3))

      name4="bias_correction_method"
      help4="method for BIAS correction, can be auto, master_bias or overscan, use master_bias only if there is a permanent BIAS structure. [overscan]"
      paramList.append(RecipeParameter(rec_id,displayName=name4,group=group,description=help4))

      name5="correct_dark_flag"
      help5="Switch on/off dark correction, 1=on 0=off. [0]"
      paramList.append(RecipeParameter(rec_id,displayName=name5,group=group,description=help5))

      name6="science_fiber_name"
      help6="Science fibre name. [A]"
      paramList.append(RecipeParameter(rec_id,displayName=name6,group=group,description=help6))

      name7="reference_fiber_name"
      help7="Reference fibre name. [A]"
      paramList.append(RecipeParameter(rec_id,displayName=name7,group=group,description=help7))

      name8="background_subtraction"
      help8="if 1 (default value) then background is subtracted, 1=on 0=off. [0]"
      paramList.append(RecipeParameter(rec_id,displayName=name8,group=group,description=help8))

      name9="bkgr_grid_size_x"
      help9="Grid size in x used to calculate the background, between: 128 and 512. [256]"
      paramList.append(RecipeParameter(rec_id,displayName=name9,group=group,description=help9))

      name10="bkgr_grid_size_y"
      help10="Grid size in y used to calculate the background, between: 128 and 512. [128]"
      paramList.append(RecipeParameter(rec_id,displayName=name10,group=group,description=help10))

      name11="inter_order_backg_function"
      help11="Function/method used to estimate background. [BKGR_SCI]"
      paramList.append(RecipeParameter(rec_id,displayName=name11,group=group,description=help11))

      name12="detector_realdim_x"
      help12="Real dimensions of detector in x dimension. [4096]"
      paramList.append(RecipeParameter(rec_id,displayName=name12,group=group,description=help12))

      name13="extraction_window"
      help13="Half-size of the window, in spatial direction, used to perform the optimum extraction. [8]"
      paramList.append(RecipeParameter(rec_id,displayName=name13,group=group,description=help13))

      name14="ksig_extraction_sky"
      help14="K used as flag for extraction selection (>=0: Horne, -1:rectangular)  and sigma clipping extraction for Horne. [3.5]"
      paramList.append(RecipeParameter(rec_id,displayName=name14,group=group,description=help14))

      name15="ksig_extraction_wave"
      help15="K used as flag for extraction selection (>=0: Horne, -1:rectangular)  and sigma clipping extraction for Horne. [-1.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name15,group=group,description=help15))

      name16="tolerance_rejection"
      help16="Tolerance for rejection profile extraction, in percentage. [0.01]"
      paramList.append(RecipeParameter(rec_id,displayName=name16,group=group,description=help16))

      name17="wave_cal_source"
      help17="Wavelength calibration source to be used on science fiber ('ThAr' or        'COMB'). [ThAr]"
      paramList.append(RecipeParameter(rec_id,displayName=name17,group=group,description=help17))

      name18="ref_fiber_source"
      help18="source of simultaneous reference fiber ('ThAr' or 'COMB' or 'FP' or   'SKY'). [ThAr]"
      paramList.append(RecipeParameter(rec_id,displayName=name18,group=group,description=help18))

      name19="emission_lines_fit_method"
      help19="method used to fit emission lines on reference fiber and tolerances on fitted line parameters. []"
      paramList.append(RecipeParameter(rec_id,displayName=name19,group=group,description=help19))

      name20="drift_method"
      help20="method used to compute drift on reference fiber ('Bouchy' or  'Line-by-line' or 'Fourier' or 'CCF'). [Bouchy]"
      paramList.append(RecipeParameter(rec_id,displayName=name20,group=group,description=help20))

      name21="drift_correction_scale"
      help21="drift correction scale ('Global' or 'Local' or TBD). [Global]"
      paramList.append(RecipeParameter(rec_id,displayName=name21,group=group,description=help21))

      name22="log_lambda_ini"
      help22="Starting point on wavelength for the 1d merged spectra. [200.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name22,group=group,description=help22))


      name23="log_lambda_end"
      help23="Ending point on wavelength for the 1d merged spectra. [300.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name23,group=group,description=help23))

      name24="delta_log_lambda"
      help24="Step in wavelength for the 1d merged spectra. [5.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name24,group=group,description=help24))

      name25="cosmics_part"
      help25="max acceptable cosmics in sci_red, between: 0.0 and 0.01. [40.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name25,group=group,description=help25))

      name26="rv_center"
      help26="Approximate RV. [20.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name26,group=group,description=help26))


      name27="rv_range"
      help27="Range for the RV table. [0.25]"
      paramList.append(RecipeParameter(rec_id,displayName=name27,group=group,description=help27))


      name28="rv_step"
      help28="Range's step for the RV table. [0.82]"
      paramList.append(RecipeParameter(rec_id,displayName=name28,group=group,description=help28))

      name29="mask_width"
      help29="Width of mask holes in km/s. [2.5e-06]"
      paramList.append(RecipeParameter(rec_id,displayName=name29,group=group,description=help29))

      name30="mask_table_id"
      help30="ask table to be used, defined by it's spectral type Id ('G2' 'M2' or 'K5'. [G2]"
      paramList.append(RecipeParameter(rec_id,displayName=name30,group=group,description=help30))

      name31="load"
      help31="Set the parameters loading mode:      'AUTO', values comes from *_inst_config.fits file or    'MANUAL', values comes from recipe configuration file. [AUTO]"
      paramList.append(RecipeParameter(rec_id,displayName=name31,group=group,description=help31))

      name32="snr_averaging_window"
      help32="Window size for averaging SNR calculation. [200]"
      paramList.append(RecipeParameter(rec_id,displayName=name32,group=group,description=help32))

      name33="extraction_method"
      help33="Method used to extract orders. [horne]"
      paramList.append(RecipeParameter(rec_id,displayName=name33,group=group,description=help33))





    def setSciRedParameters(self,paramList,rec_id,group):
      print "parameters rec id",rec_id
      name1="wave_cal_source"
      help1="Wavelength calibration source to be used on science fiber ('THAR' or        'COMB'). [THAR]"
      paramList.append(RecipeParameter(rec_id,displayName=name1,group=group,description=help1))

      name2="ksigma_sky"
      help2="ksigma for removing cosmics on fiber A or SKY, -1.0 - no cosmics removal. [3.5]"
      paramList.append(RecipeParameter(rec_id,displayName=name2,group=group,description=help2))

      name3="rv_center"
      help3="Approximate RV. [19.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name3,group=group,description=help3))

      name4="rv_range"
      help4="Range for the RV table. [20.0]"
      paramList.append(RecipeParameter(rec_id,displayName=name4,group=group,description=help4))

      name5="rv_step"
      help5=" Range's step for the RV table. [0.25]"
      paramList.append(RecipeParameter(rec_id,displayName=name5,group=group,description=help5))

      name6="mask_table_id"
      help6="Mask table to be used, defined by it's spectral type Id ('G2' 'M2' or 'K5'. [G2]"
      paramList.append(RecipeParameter(rec_id,displayName=name6,group=group,description=help6))

      name7="extraction_method"
      help7="Method used to extract orders. [horne]"
      paramList.append(RecipeParameter(rec_id,displayName=name7,group=group,description=help7))

      name8="background_sw"
      help8="Background measurement activation (on/off). [on]"
      paramList.append(RecipeParameter(rec_id,displayName=name8,group=group,description=help8))

      name9="flux_correction_sw"
      help9="Flux correction activation (on/off). [on]"
      paramList.append(RecipeParameter(rec_id,displayName=name9,group=group,description=help9))

      name10="drift_correction_sw"
      help10="Drift correction activation (on/off). [on]"
      paramList.append(RecipeParameter(rec_id,displayName=name10,group=group,description=help10))

      name11="MB_res_remove_flag"
      help11="Flag indicating to remove or not MB residuals. [0]"
      paramList.append(RecipeParameter(rec_id,displayName=name11,group=group,description=help11))





