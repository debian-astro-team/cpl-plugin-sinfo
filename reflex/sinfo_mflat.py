# import the needed modules
try:
  import reflex
  import_sucess = 'true'

# Then here is python code moved in xsh_object_interactive_common.py

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"


# import the needed modules
try:
#  import sys
  import numpy
  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits
  from matplotlib.font_manager import fontManager, FontProperties 
  import sinfo_plot_common
  import sinfo_parameters_common
#  import wx
#  import matplotlib
  import reflex
  #from xsh_plot_common import *
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  import reflex_plot_widgets

  from pipeline_product import PipelineProduct
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.


  
  class DataPlotterManager:



    def paragraph(self,text, width=None):
       """ wrap text string into paragraph
           text:  text to format, removes leading space and newlines
           width: if not None, wraps text, not recommended for tooltips as
                  they are wrapped by wxWidgets by default
       """
       import textwrap
       #print 'width=',width,'Text=',text
       if width is None:
           return textwrap.dedent(text).replace('\n', ' ').strip()
       else:
           return textwrap.fill(textwrap.dedent(text), width=width) 


    
    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames
    def readFitsData(self, fitsFiles):
      #Control variable to check if the interesting files where at the input
      #print fitsFiles
      
      self.mdark_found = False
      self.rflat = None
      self.nima = 0
      self.binx = 1
      self.ext_ima=1
      self.biny = 1
      self.arm_id=0
      self.qc_id='DARK_MEAN'
      
      self.product_id = 'Master Flat image'
      self.labels_prod= list()
      self.labels_qc=['DARK MEAN','HOTPIX']
      #Read all the products
      frames = dict()
      for frame in fitsFiles:
        if frame == '' :
          continue

        header = pyfits.open(frame.name)
        #Make sure to have only products from the same recipe
        #that used this (common) script 
        if 'ESO PRO REC1 ID' in header[0].header :
           rec_id = header[0].header['ESO PRO REC1 ID']
           #print "rec_id=", rec_id
           if rec_id == self.rec_id :
              category = frame.category
              frames[category] = frame
              print "frame name:", frame.name, category
 
# For any arm search a list of input frames
      #print frames
      key = "MASTER_FLAT_LAMP"
      if key in frames :
        self.mflat_found = True
        hdulist = frames[key]
        self.mflat = PipelineProduct(hdulist)
        self.labels_prod.append('Master Flat image')
        self.labels_prod.append('Column Average')
        self.labels_prod.append('Row Average')

        print "found", key

      key = "RMASTER_FLAT_LAMP"
      if key in frames :
        self.rflat_found = True
        hdulist = frames[key]
        self.rflat = PipelineProduct(hdulist)
        self.labels_prod.append('Master-Reference')
        print "found", key

      key = "BP_MAP_NO"
      if key in frames :
        self.bp_map_no_found = True
        hdulist = frames[key]
        self.bp_map_no = PipelineProduct(hdulist)
        print "found", key

      key = "MASTER_BP_MAP"
      if key in frames :
        self.master_bp_map_found = True
        hdulist = frames[key]
        self.master_bp_map = PipelineProduct(hdulist)
        print "found", key

    #Set rec id (to have proper recipe parameters prefix)
    def setRecId(self, rec_id):
      #Recipe ID variable to properly define params
      self.rec_id = rec_id

    #Get arm setting
    def getArmId(self, sof):
      #Recipe ID variable to properly define params
      self.rec_id = "sinfo_rec_mflat"
      nf = 0

      frames = dict()
      files = sof.files
      for f in files:
        frame=f.name
        if frame == '' :
          continue
        else :
           nf += 1
           hdulist = pyfits.open(frame)
           rec_id_list = hdulist[0].header['ESO PRO REC1 ID']
      if nf != 0 :
         self.rec_id = rec_id_list[0:]

      #print "self.rec_id", self.rec_id
      if self.rec_id == "sinfo_rec_mflat" :
         self.rec_subtitle = "Master Flat Creation. "
             
    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      nrows=self.nima
      row=1;

      if self.mflat_found == True : 
        self.subplot_common  = figure.add_subplot(1,1,1)
        row +=1

        self.product_selector = figure.add_axes([0.00, 0.85, 0.25, 0.15])


      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)

    def plotData(self,arm_id):
        self.plotProductsGraphics()

    def plotWidgets(self) :
        widgets = list()

        labels_prod = self.labels_prod
        self.radiobutton_pro = reflex_plot_widgets.InteractiveRadioButtons(self.product_selector, self.setProductSelectCallBack, labels_prod, 0, title="")
        widgets.append(self.radiobutton_pro)

        return widgets

    def setProductSelectCallBack(self, product_id) :
        print "Selected product id:", product_id
        self.product_id = product_id
        self.subplot_common.cla()
        self.displayProducts()  
        print "product_id:", self.product_id
         

    def prepTitleAndTooltipImage(self) :
        title_pref = 'Linear-extracted and Merged Spectrum.'	      
        title_spectrum   = title_pref
        spec_frame   = 'Flat '

        if self.product_id == 'Master Flat image' :
           self.obj = self.mflat
           spec_frame   = 'Master Flat '
           title_mflat   = 'Master flat'
           self.tooltip_mflat ="""\
                Image of the overscan corrected master flat frame. 
                """
        elif self.product_id == 'HOT_PIXEL_MASK' :
           spec_frame   = 'Hot pixel mask '
           title_mflat   = 'Hot pixel mask'
           self.tooltip_mflat ="""\
                Image of the hot pixel mask frame. 
                """
           self.obj = self.hot_pixel_mask

        self.title_frame   = spec_frame

    def plotImage(self) :
        self.prepTitleAndTooltipImage()
        self.dpm.plotImageOnly(self.obj,self.ext_ima, self.binx,self.biny,'X [pix]', 'Y [pix]', self.subplot_image,self.title_frame,self.tooltip_mflat)


    def plotImageMaster(self,obj,subplot) :
        self.prepTitleAndTooltipImage()
        obj.readImage(0)
        nyM, nxM = obj.image.shape
        self.nxM = nxM
        self.nyM = nyM
        self.dpm.plotImageOnly(obj,0, 1, 1,'X [pix]', 'Y [pix]', subplot,self.title_frame,self.tooltip_mflat)

    def plotImageMasterMinusRef(self,obj,ref,subplot) :
        self.prepTitleAndTooltipImage()
        obj.readImage(0)
        ref.readImage(0)
        Xlab='X [pix]'
        Ylab='Y [pix]'
        img = ImageDisplay()
      
        img.setLabels(Xlab,Ylab)
        title='Master-Reference'
        tooltip='Image of '+title
        

        diff = obj.image - ref.image
        self.Dmean = diff.mean()
        self.Dstd = diff.std()
        self.diff=diff
        img.display(subplot, title, self.paragraph(tooltip),self.diff)


    def plotGra1(self,mast,ref,subplot):
    
        #=======================================================================
        # PLOT 1 (231):  Plotting the current FLAT image (master) and reference collapsed along columns
        #=======================================================================
        #subplot(231)
        mast.readImage(0)
        nyM, nxM = mast.image.shape
        Mcol = mast.image.sum(axis=1)/float(nxM)

        if self.rflat != None :
           ref.readImage(0)
           nyR, nxR = ref.image.shape    
           Rcol = ref.image.sum(axis=1)/float(nxR)
        xcol2 = numpy.arange(nyM)

        colM1 = str(0.10)         # current master (single column at x=1/4)
        colM2 = str(0.40)         # current master (single column at x=2/4)
        colM3 = str(0.70)         # current master (single column at x=3/4)


        #plot(xcol2, mast[:,250], color=colM1, alpha=0.5)
        #plot(xcol2, mast[:,500], color=colM2, alpha=0.5)
        #plot(xcol2, mast[:,750], color=colM3, alpha=0.5)

        subplot.plot(xcol2, mast.image[:,500], color=colM1, alpha=0.5)
        subplot.plot(xcol2, mast.image[:,1000], color=colM2, alpha=0.5)
        subplot.plot(xcol2, mast.image[:,1500], color=colM3, alpha=0.5)

        if self.rflat != None :
            subplot.plot(xcol2, Rcol, color='red', linewidth=1.5)
        subplot.plot(xcol2, Mcol, color='blue', linewidth=1.5)

        xLim=0,nxM
        subplot.set_xlim(xLim)
        #subplot.set_ylim(yLim)
        YLab='column average (counts)'
        title1 = 'Column Average'
        tooltip = 'plot of '+title1+' vs row position'
	subplot.tooltip=tooltip
        subplot.set_title(title1, fontsize=12)

      	grey1_patch = matplotlib.patches.Patch(color=colM1, label='master @500')
      	grey2_patch = matplotlib.patches.Patch(color=colM2, label='master @1000')
      	grey3_patch = matplotlib.patches.Patch(color=colM3, label='master @1500')
        if self.rflat != None :
           red_patch = matplotlib.patches.Patch(color='red', label='reference')
	blue_patch = matplotlib.patches.Patch(color='blue', label='master')
	#black_patch = matplotlib.patches.Patch(color='black', label='reference-master')


        if self.rflat != None :
           subplot.legend(bbox_to_anchor=(0.81, 0.185),handles=[grey1_patch,grey2_patch,grey3_patch,red_patch,blue_patch])
        else :
           subplot.legend(bbox_to_anchor=(0.81, 0.185),handles=[grey1_patch,grey2_patch,grey3_patch,blue_patch])
          
        fontLegend = FontProperties(size=9)
        #subplot.legend(('master x=500','master x=1000','master x=1500','reference','master'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)


    def plotGra2(self,mast,ref,subplot):
        #=======================================================================
        # PLOT 2 (232):  Plotting the current FLAT image (master) and reference collapsed along rows
        #=======================================================================

        #subplot(232)
        mast.readImage(0)
        nyM, nxM = mast.image.shape
        Mrow = mast.image.sum(axis=0)/float(nyM)

        if self.rflat != None :
           ref.readImage(0)
           nyR, nxR = ref.image.shape
           Rrow = ref.image.sum(axis=0)/float(nyR)
           
        xrow2 = numpy.arange(nxM)
        xcol2 = numpy.arange(nyM)

        colM1 = str(0.10)         # current master (single column at x=1/4)
        colM2 = str(0.40)         # current master (single column at x=2/4)
        colM3 = str(0.70)         # current master (single column at x=3/4)

        
        #subplot.plot(xrow2, mast.image[250,:], color=colM1, alpha=0.5)
        #subplot.plot(xrow2, mast.image[500,:], color=colM2, alpha=0.5)
        #subplot.plot(xrow2, mast.image[750,:], color=colM3, alpha=0.5)

        subplot.plot(xrow2, mast.image[500,:], color=colM1, alpha=0.5)
        subplot.plot(xrow2, mast.image[1000,:], color=colM2, alpha=0.5)
        subplot.plot(xrow2, mast.image[1500,:], color=colM3, alpha=0.5)

        if self.rflat != None :
           subplot.plot(xrow2, Rrow, color='red', linewidth=1.5)
        subplot.plot(xrow2, Mrow, color='blue', linewidth=1.5)

        #ymin = Mrow.min()
        #ymax = Mrow.max()
        #ymin = ymin - abs(ymax-ymin)/2.
        #ymax = ymax + abs(ymax-ymin)/4.

        #xlim(0,nyM)
        xLim = 0,nyM
        #ylim(ymin, ymax)
        subplot.set_xlim(xLim)
        Ylabel='row average (counts)'
        title = 'Row Average'
        tooltip = 'plot of '+title+' vs column position'
	subplot.tooltip=tooltip
        #title(title1, fontsize=12)
        subplot.set_title(title, fontsize=12)

      	grey1_patch = matplotlib.patches.Patch(color=colM1, label='master @500')
      	grey2_patch = matplotlib.patches.Patch(color=colM2, label='master @1000')
      	grey3_patch = matplotlib.patches.Patch(color=colM3, label='master @1500')
        if self.rflat != None :
           red_patch = matplotlib.patches.Patch(color='red', label='reference')
	blue_patch = matplotlib.patches.Patch(color='blue', label='master')
	#black_patch = matplotlib.patches.Patch(color='black', label='reference-master')
        if self.rflat != None :
           subplot.legend(bbox_to_anchor=(0.81, 0.185),handles=[grey1_patch,grey2_patch,grey3_patch,red_patch,blue_patch])
        else :
           subplot.legend(bbox_to_anchor=(0.81, 0.185),handles=[grey1_patch,grey2_patch,grey3_patch,blue_patch])

        #legend(('master y=500','master y=1000','master y=1500','reference','master'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)
        #legend(('master y=100','master y=300','master y=500','master y=700','master y=900','reference','master'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)


    def histogram(self,a, bins):
	# Note that argument names below are reverse of the
	# searchsorted argument names
	n = numpy.searchsorted(numpy.sort(a), bins)
	n = numpy.concatenate([n, [len(a)]])
        return n[1:]-n[:-1]


        
    def plotHist1(self,mast,ref,subplot):

        #=======================================================================
        # PLOT 3 (233): Plotting the master reference and reference-master FLAT histograms
        #=======================================================================
        #subplot(233)
        delhist1  = 1.5
        bin_size1 = 0.02
        print mast.image.shape
        print ref.image.shape
        
        Mmean = mast.image.mean()
        Mstd  = mast.image.std()
        Rmean = ref.image.mean()
        Rstd  = ref.image.std()
        Dmean=self.Dmean
        diff=self.diff
        print Mmean,Mstd
        print Rmean,Rstd
        x1M_hist = Mmean - delhist1
        x2M_hist = Mmean + delhist1
        print x1M_hist,x2M_hist
        x1R_hist = Rmean - delhist1
        x2R_hist = Rmean + delhist1
        print x1R_hist,x2R_hist
        x1D_hist = Dmean - delhist1
        x2D_hist = Dmean + delhist1
        print x1D_hist,x2D_hist

        hrangeM = numpy.arange(x1M_hist,x2M_hist,bin_size1)
        hrangeR = numpy.arange(x1R_hist,x2R_hist,bin_size1)
        hrangeD = numpy.arange(x1D_hist,x2D_hist,bin_size1)
        #print hrangeM,hrangeR,hrangeD

        Mhist  = self.histogram(mast.image.flat, hrangeM)
        Rhist  = self.histogram(ref.image.flat, hrangeR)
        Dhist  = self.histogram(diff.flat, hrangeD)
        print len(Mhist),len(Rhist),len(Dhist)

        #semilogy(hrangeR, Rhist, linestyle='steps-mid', linewidth=1, color=colR)           # Creates a log plot from linear input!
        #semilogy(hrangeM, Mhist, linestyle='steps-mid', linewidth=1, color=colM)           # NOTE: USING linestyle='steps' FUCKS UP THE Y-AXIS LIMITS!
        #semilogy(hrangeD, Dhist, linestyle='steps-mid', linewidth=1, color=colD)
        print len(hrangeR), len(Rhist)

        subplot.semilogy(hrangeR, Rhist, linewidth=1.5, color='blue')                                 # Creates a log plot from linear input!
        subplot.semilogy(hrangeM, Mhist, linewidth=1.5, color='red')
        subplot.semilogy(hrangeD, Dhist, linewidth=1.5, color='black')

        Pline1M = subplot.semilogy((0.0, 0.0), (1.0, 1000000), 'g--', linewidth=1, alpha=1.0)
        # defining a smaller font size for plot legends
        fontLegend = FontProperties(size=8)
        
        Xlab='counts'
        Ylab='log(number)'
        title='log(histogram)'
        subplot.legend(('reference','master','ref-mast'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)
        

    def caseNodata(self) :
        #Data not found info
        self.subtext_nodata.set_axis_off()
        self.text_nodata = """\
                           The python GUI miss some required input frame
                           This may be due to a recipe failure or to missing
                           reference frames.
                           This may be due to a recipe failure. 
                           Check your input parameter values, 
                           correct possibly typos and
                           press 'Re-run recipe' button."""
        self.subtext_nodata.text(0.1, 0.6, self.text_nodata, color='#11557c', 
                                 fontsize=18, ha='left', va='center', alpha=1.0)
        self.subtext_nodata.tooltip="""\
                                    Merged spectrum not found in the products"""
        #print "found no spectrum data"

    

    def displayProducts(self):
        if self.product_id == 'Master Flat image' :
           self.plotImageMaster(self.mflat,self.subplot_common)
        elif self.product_id == 'Master-Reference' :
           self.plotImageMasterMinusRef(self.mflat,self.rflat,self.subplot_common)
        elif self.product_id == 'Column Average' :
           self.plotGra1(self.mflat,self.rflat,self.subplot_common)
        elif self.product_id == 'Row Average' : 
           self.plotGra2(self.mflat,self.rflat,self.subplot_common)
        elif self.product_id == 'Histogram1' :
           self.plotHist1(self.mflat,self.rflat,self.subplot_common)

        
    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
      if (self.mflat_found == True) :
        self.prepTitleAndTooltipImage()
        
        #sy = self.obj.all_hdu[self.ext_ima].header['NAXIS2']
        #self.ima_sy=sy
        self.dpm = sinfo_plot_common.DataPlotterManager()
        self.displayProducts()

      else :
        self.caseNodata() 
  
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setInteractiveParameters(self):
      paramList = list()
      rec_id=self.rec_id
      self.par = sinfo_parameters_common.Parameters()

      if rec_id == "sinfo_rec_mflat" :
        self.par.setFlatParameters(paramList,"sinfo_rec_mflat","all")
        print "recipe" ,rec_id,  "no parameters"

      else :
         print "recipe" ,rec_id,  "not supported"

      return paramList

    def setWindowHelp(self):
      help_text = """
This is an interactive window which help asses the quality of the execution of a recipe.
"""
      return help_text

    def setWindowTitle(self):
      title = 'SINFONI Interactive ' + self.rec_subtitle  
      return title

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise

#This is the 'main' function
if __name__ == '__main__':

  # import reflex modules
  from reflex import *
  from reflex_interactive_app import *
  from pipeline_display import *
  from pipeline_product import *

  # Create interactive application
  interactive_app = PipelineInteractiveApp(enable_init_sop=True)

  # PECULIAR XSH needs this in order to be able later to get from an input FITS
  # the ins-mode, arm (and recipe) IDs, used in titles and param setting
  # get inputs from the command line
  interactive_app.parse_args()
  inputs = interactive_app.inputs
  #(inputs, args) = interactive_app.parse_args()

  #Check if import failed or not
  print "import_sucess=", import_sucess
  if import_sucess == 'false' :
     interactive_app.setEnableGUI(false)
    
  #interactive_app.setEnableGUI(True)
  #Open the interactive window if enabled
  if interactive_app.isGUIEnabled() :
    #Get the specific functions for this window
    dataPlotManager = DataPlotterManager()
    #print inputs.in_sof
    #dataPlotManager.checkSofIsNotEmpty(inputs.in_sof)
    #With the following call XSH get the: ins-mode, arm (and recipe) IDs
    dataPlotManager.getArmId(inputs.in_sof)
    #Set recipe ID in order to build proper param list, display layout
    dataPlotManager.setRecId("sinfo_rec_mflat")
    interactive_app.setPlotManager(dataPlotManager)
    interactive_app.showGUI()
  
  else :
    interactive_app.passProductsThrough()
   
  #Print outputs. This is parsed by the Reflex python actor to get the results
  #Do not remove
  interactive_app.print_outputs()
  sys.exit()
