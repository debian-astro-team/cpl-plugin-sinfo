# import the needed modules
try:
  import reflex
  import_sucess = 'true'

# Then here is python code moved in xsh_object_interactive_common.py

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"


# import the needed modules
try:
#  import sys
  import numpy
  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits
    
  import sinfo_plot_common
  import sinfo_parameters_common
#  import wx
#  import matplotlib
  import reflex
  #from xsh_plot_common import *
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  import reflex_plot_widgets

  from pipeline_product import PipelineProduct
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:

    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames
    def readFitsData(self, fitsFiles):
      #Control variable to check if the interesting files where at the input
      #print fitsFiles
      
      self.linearity_found = False
      self.nima = 0
      self.binx = 1
      self.ext_ima=1
      self.biny = 1
      self.arm_id=0
      self.qc_id='DARK_MEAN'
      
      self.product_id = 'gain'
      self.labels_prod=['gain','flux vs. median flux/DIT','lamp flux fit','linearity fit residuals']
      self.labels_qc=['DARK MEAN','HOTPIX']

      #Read all the products
      frames = dict()
      for frame in fitsFiles:
        if frame == '' :
          continue

        header = pyfits.open(frame.name)
        #Make sure to have only products from the same recipe
        #that used this (common) script 
        if 'ESO PRO REC1 ID' in header[0].header :
           rec_id = header[0].header['ESO PRO REC1 ID']
           #print "rec_id=", rec_id
           if rec_id == self.rec_id :
              category = frame.category
              frames[category] = frame
              print "frame name:", frame.name, category
 
# For any arm search a list of input frames
      #print frames
      key = "BP_COEFF"
      if key in frames :
        self.bp_coeff_found = True
        hdulist = frames[key]
        self.bp_coeff = PipelineProduct(hdulist)
        print "found", key


      
      key = "BP_MAP_NL"
      if key in frames :
        self.bp_map_nl_found = True
        hdulist = frames[key]
        self.bp_map_nl = PipelineProduct(hdulist)
        print "found", key


      key = "GAIN_INFO"
      if key in frames :
        self.gain_info_found = True
        hdulist = frames[key]
        self.gain_info = PipelineProduct(hdulist)
        print "found", key

      key = "LIN_DET_INFO"
      if key in frames :
        self.lin_det_info_found = True
        hdulist = frames[key]
        self.lin_det_info = PipelineProduct(hdulist)
        print "found", key

    #Set rec id (to have proper recipe parameters prefix)
    def setRecId(self, rec_id):
      #Recipe ID variable to properly define params
      self.rec_id = rec_id

    #Get arm setting
    def getArmId(self, sof):
      #Recipe ID variable to properly define params
      self.rec_id = "sinfo_linearity"
      nf = 0

      frames = dict()
      files = sof.files
      for f in files:
        frame=f.name
        if frame == '' :
          continue
        else :
           nf += 1
           hdulist = pyfits.open(frame)
           rec_id_list = hdulist[0].header['ESO PRO REC1 ID']
      if nf != 0 :
         self.rec_id = rec_id_list[0:]

      #print "self.rec_id", self.rec_id
      if self.rec_id == "sinfo_rec_detlin" :
         self.rec_subtitle = "Linearity coefficients and gain Computation. "
             
    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      nrows=self.nima
      row=1;

      if self.bp_map_nl_found == True: 
        self.subplot_common  = figure.add_subplot(1,1,1)
        row +=1
 
        self.product_selector = figure.add_axes([0.0, 0.90, 0.33, 0.1])

      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)

    def plotData(self,arm_id):
        self.plotProductsGraphics()

    def plotWidgets(self) :
        widgets = list()

        labels_prod = self.labels_prod
        self.radiobutton_pro = reflex_plot_widgets.InteractiveRadioButtons(self.product_selector, self.setProductSelectCallBack, labels_prod, 0, title="")
        widgets.append(self.radiobutton_pro)

        return widgets

    def setProductSelectCallBack(self, product_id) :
        print "Selected product id:", product_id
        self.product_id = product_id
        self.subplot_common.cla()
        self.displayProducts()     
        print "product_id:", self.product_id

    def prepTitleAndTooltipImage(self) :
        title_pref = 'Linear-extracted and Merged Spectrum.'	      
        title_spectrum   = title_pref

        if self.product_id == 'gain' :
           spec_frame   = 'Non Linear pixel mask '
           title_linearity   = 'Non Linear pixel mask'
           self.tooltip_linearity ="""\
                Image of the non linear pixels mask frame. 
                """
           self.obj = self.bp_map_nl

        self.title_frame   = spec_frame+'image'

    def caseNodata(self) :
        #Data not found info
        self.subtext_nodata.set_axis_off()
        self.text_nodata = """\
                           The python GUI miss some required input frame
                           This may be due to a recipe failure or to missing
                           reference frames.
                           This may be due to a recipe failure. 
                           Check your input parameter values, 
                           correct possibly typos and
                           press 'Re-run recipe' button."""
        self.subtext_nodata.text(0.1, 0.6, self.text_nodata, color='#11557c', 
                                 fontsize=18, ha='left', va='center', alpha=1.0)
        self.subtext_nodata.tooltip="""\
                                    Merged spectrum not found in the products"""
        #print "found no spectrum data"





    def fig1(self,gaintab,subplot):
        #=======================================================================
        # Fig. 3 / Plot 1:  Plotting the gain as a function of flux level
        #=======================================================================
        #print "fits file:", gaintab.fits_file
      
        # Nominal gain value as per the SINFONI User's manual (pg. 44)
        gain_nominal = (2.42, 0.05)
	   
        key = 'HIERARCH ESO QC GAINERR'
        qc_gainerr =  gaintab.readKeyword(key)

        # detector gain (e-/ADU)
        key = 'HIERARCH ESO QC GAIN10'
        self.qc_gain10 = gaintab.readKeyword(key)
        
        gaintab.readTableColumn(fits_extension=1,colname='adu')
        adu =  gaintab.column
        
        gaintab.readTableColumn(fits_extension=1,colname='gain')
        gain =  gaintab.column
        Ngaintab=len(gain)
        #subplot(221)

        gainerr = numpy.ones(gain.shape, float)
        gainerr = qc_gainerr * gainerr

        subplot.errorbar(adu, gain, gainerr, fmt='o')

        title='gain'
        subplot.set_title(title, fontsize=12)
        # Plot the nominal gain as stated in the SINFONI User's Manual (pg. 44):

        subplot.axhline(y=gain_nominal[0], color='g', linestyle='-', linewidth=2.0, alpha=0.75)
        #axhspan(gain_nominal[0]-gain_nominal[1], gain_nominal[0]+gain_nominal[1], facecolor='g', alpha=0.10)
        gain_med = numpy.median(gain)
        gain_std = gain.std()
        subplot.axhline(y=gain_med, color='b', linestyle='-.', linewidth=1.0, alpha=1.0)
        subplot.axhspan(gain_med-gain_std, gain_med+gain_std, facecolor='b', alpha=0.10)
        yLim = 1.6, 3.0
        #subplot.set_xlim(xLim)
        subplot.set_ylim(yLim)
        
        xx1, xx2 = subplot.get_xlim()
        yy1, yy2 = subplot.get_ylim()

        subplot.text(xx1, yy2-0.07*(yy2-yy1), 'median gain  = %4.2f +/- %4.2f' %(gain_med, gain_std), color='blue', size=10)
        subplot.text(xx1, yy2-0.10*(yy2-yy1), 'nominal gain = %4.2f +/- %4.2f' %(gain_nominal[0],gain_nominal[1]), color='green', size=10)

        Xlab='ADU'
        Ylab=r'$\rm{e}^-/\rm{ADU}$'
        spectrum = SpectrumDisplay()
        spectrum.setLabels(Xlab,Ylab)
	subplot.tooltip='gain vs ADU'
	subplot.set_xlabel(Xlab)
        subplot.set_ylabel(Ylab)
        #spectrum.display(subplot,title,self.paragraph(tooltip),slitlet,Mdist,None, autolimits = False)

    def readLinTab(self,lintab) :
        # linearity data table:  median flux level (ADU)
        lintab.readTableColumn(fits_extension=1,colname='med')
        self.lin_med =  lintab.column
        
        # linearity data table:  average flux level (ADU)
        lintab.readTableColumn(fits_extension=1,colname='avg')
        self.lin_avg =  lintab.column
        
        # linearity data table:  median flux level/DIT (ADU/sec)
        lintab.readTableColumn(fits_extension=1,colname='med_dit')
        self.lin_med_dit =  lintab.column
        
        # linearity data table:  average flux level/DIT (ADU/sec)
        lintab.readTableColumn(fits_extension=1,colname='avg_dit')
        self.lin_avg_dit =  lintab.column
        
        # linearity data table:  DIT (seconds)
        lintab.readTableColumn(fits_extension=1,colname='dit')
        self.lin_dit =  lintab.column
        
        # linearity data table:  ADL
        lintab.readTableColumn(fits_extension=1,colname='adl')
        self.lin_adl =  lintab.column
      
    def fig2(self,lintab,subplot):
        #=======================================================================
        # Fig. 3 / Plot 2:  Plotting the median flux level/DIT as a function of flux level
        #=======================================================================

        #subplot(222)

        # All I am doing here is to average the two lamp-on flats for each DIT value.
        self.readLinTab(lintab)
        lin_med=self.lin_med 
        lin_avg=self.lin_avg 
        lin_med_dit=self.lin_med_dit 
        lin_avg_dit=self.lin_avg_dit 
        lin_dit=self.lin_dit 
        lin_adl=self.lin_adl 



        adu_dit_med     = numpy.median(lin_med_dit)
        adu_dit_std     = lin_med_dit.std()
        adu_dit_avg     = numpy.median(lin_avg_dit)
        adu_dit_avg_std = lin_avg_dit.std()

        
        Mlin_med = numpy.zeros(len(lin_med)/2, float)
        Mmed_dit = numpy.zeros(len(lin_med)/2, float)
        Mdit     = numpy.zeros(len(lin_med)/2, float)

        
        Mlin_med[0] = (lin_med[0]  + lin_med[1])/2.0
        Mlin_med[1] = (lin_med[2]  + lin_med[3])/2.0
        Mlin_med[2] = (lin_med[4]  + lin_med[5])/2.0 
        Mlin_med[3] = (lin_med[6]  + lin_med[7])/2.0
        Mlin_med[4] = (lin_med[8]  + lin_med[9])/2.0
        try:
            qc_gain10 = self.qc_gain10
	    Mlin_med[5] = (lin_med[10] + lin_med[11])/2.0
        except KeyError:
            print this_prog,'[error] qc_gain10 is missing '
        Mmed_dit[0] = (lin_med_dit[0]  + lin_med_dit[1])/2.0
        Mmed_dit[1] = (lin_med_dit[2]  + lin_med_dit[3])/2.0
        Mmed_dit[2] = (lin_med_dit[4]  + lin_med_dit[5])/2.0
        Mmed_dit[3] = (lin_med_dit[6]  + lin_med_dit[7])/2.0
        Mmed_dit[4] = (lin_med_dit[8]  + lin_med_dit[9])/2.0
        try:
            qc_gain10 = self.qc_gain10
	    Mmed_dit[5] = (lin_med_dit[10] + lin_med_dit[11])/2.0
        except KeyError:
            print this_prog,'[error] qc_gain10 is missing '

        Mdit[0]     = lin_dit[0]
        Mdit[1]     = lin_dit[2]
        Mdit[2]     = lin_dit[4]
        Mdit[3]     = lin_dit[6]
        Mdit[4]     = lin_dit[8]
        try:
            qc_gain10 = self.qc_gain10
            Mdit[5]     = lin_dit[10]
        except KeyError:
            print this_prog,'[error] qc_gain10 is missing '


        #plot((lin_med, lin_med_dit, 'bo')  # these values are doubled. . .i.e. there are two lamp-on flats for each DIT.
        subplot.plot(Mlin_med, Mmed_dit, 'bo')     # these values are for a single DIT. . .i.e. I have averaged the two lamp-on flats for each DIT.

        for i in range(len(Mmed_dit)):
 	    subplot.text(Mlin_med[i]+400, Mmed_dit[i], '%i' %(Mdit[i]), fontsize=9)

        subplot.axhline(y=adu_dit_med, color='b', linestyle='-.', linewidth=1.0, alpha=1.0)


        # Running the fit
        p1=numpy.polyfit(Mlin_med, Mmed_dit,1)
        #print p1

        xx = numpy.arange(0, 2.*Mlin_med.max(),1)
        yy = p1[0]*xx + p1[1]

        subplot.plot(xx, yy, color='g', linestyle='-.', linewidth=1.0, alpha=1.0)

        title='flux vs. median flux/DIT'
        subplot.set_title(title, fontsize=12)

        yy1 = Mmed_dit.min() - (Mmed_dit.max()-Mmed_dit.min())/4.
        yy2 = Mmed_dit.max() + (Mmed_dit.max()-Mmed_dit.min())/4.
        xx1, xx2 = subplot.get_xlim()
        yy1, yy2 = subplot.get_ylim()

        xLim=xx1, xx2
        yLim=yy1, yy2
        
        subplot.set_xlim(xLim)
        subplot.set_ylim(yLim)
 
        subplot.text(xx1, yy2-0.07*(yy2-yy1), 'median flux/DIT  = %6.2f +/- %4.2f' %(adu_dit_med, adu_dit_std), color='blue', size=10)
        subplot.text(xx1, yy2-0.10*(yy2-yy1), 'median flux/DIT (fit) = %9.6f*ADU + %4.2f' %(p1[0], p1[1]), color='green', size=10)

        Xlab='ADU'
        Ylab='median flux/DIT (ADU/sec)'
        spectrum = SpectrumDisplay()
        spectrum.setLabels(Xlab,Ylab)
        subplot.tooltip='flux vs median flux/DIT'
	subplot.set_xlabel(Xlab)
        subplot.set_ylabel(Ylab)

    def fig3(self,lintab,bp_coeff_tab,subplot):
 
        #=======================================================================
        # Fig. 3 / Plot 3:  Plotting the median flux level vs. the exposure time (DIT)
        #=======================================================================
        spectrum = SpectrumDisplay()
        #subplot(223)
        lin_med=self.lin_med
        subplot.plot(lin_med, lin_med, 'bo')

        title='lamp flux vs. lamp flux'
        subplot.set_title(title, fontsize=12)

        Xlab='lamp-on flux (ADU)'
        Ylab='fit (ADU)'
        spectrum.setLabels(Xlab,Ylab)
        xADU = numpy.arange(0., 60000., 1.)

        # Linearity polynomial coefficient 0 (median value)
        key = 'HIERARCH ESO QC BP-MAP LIN0 MED'
        qc_lin0_med = lintab.readKeyword(key)

        # Linearity polynomial coefficient 1 (median value)
        key = 'HIERARCH ESO QC BP-MAP LIN1 MED'
        qc_lin1_med = lintab.readKeyword(key)

        # Linearity polynomial coefficient 2 (median value)
        key = 'HIERARCH ESO QC BP-MAP LIN2 MED'
        qc_lin2_med = lintab.readKeyword(key)

        # Linearity polynomial coefficient 0 (mean value)
        key = 'HIERARCH ESO QC BP-MAP LIN0 MEAN'
        qc_lin0_mean = bp_coeff_tab.readKeyword(key)

        # Linearity polynomial coefficient 1 (mean value)
        key = 'HIERARCH ESO QC BP-MAP LIN1 MEAN'
        qc_lin1_mean = bp_coeff_tab.readKeyword(key)

        # Linearity polynomial coefficient 2 (mean value)
        key = 'HIERARCH ESO QC BP-MAP LIN2 MEAN'
        qc_lin2_mean = bp_coeff_tab.readKeyword(key)

        
        

        fit_med    =  qc_lin0_med  + qc_lin1_med *xADU + qc_lin2_med *xADU**2
        fit_avg    =  qc_lin0_mean + qc_lin1_mean*xADU + qc_lin2_mean*xADU**2
        fit_linear =  qc_lin0_med  + qc_lin1_med *xADU

	subplot.set_xlabel(Xlab)
        subplot.set_ylabel(Ylab)
        subplot.plot(xADU, fit_avg, color='r', linestyle='-', linewidth=1.0, alpha=1.0)
        subplot.plot(xADU, fit_med, color='b', linestyle='-', linewidth=2.0, alpha=1.0)
        subplot.plot(xADU, fit_linear, color='g', linestyle='-.', linewidth=2.0, alpha=1.0)

        xLim=0, 60000
        yLim=0, 60000
        subplot.set_xlim(xLim)
        subplot.set_ylim(yLim)
        
        #xx1, xx2 = xlim()
        #yy1, yy2 = ylim()
        xx1, xx2 = subplot.get_xlim()
        yy1, yy2 = subplot.get_ylim()

        subplot.text(xx1, yy2-0.10*(yy2-yy1), r'$\rm{fit:=} %5.3f + %5.3f*flux + %5.3e*flux^2\ \ \rm{(LIN0...2.MED)}$' %(qc_lin0_med, qc_lin1_med, qc_lin2_med), color='blue', size=11)
        subplot.text(xx1, yy2-0.15*(yy2-yy1), r'$\rm{fit:=} %5.3f + %5.3f*flux + %5.3e*flux^2\ \ \rm{(LIN0...2.MEAN)}$' %(qc_lin0_mean, qc_lin1_mean, qc_lin2_mean), color='red', size=11)
        subplot.text(xx1, yy2-0.20*(yy2-yy1), r'$\rm{linear\ fit:=} %5.3f + %5.3f*flux$' %(qc_lin0_med, qc_lin1_med), color='green', size=11)

        subplot.tooltip='lamp flux vs lamp flux'


    def fig4(self,lintab,bp_coeff_tab,subplot):

        #=======================================================================
        # Fig. 3 / Plot 4:  Plotting the linearity fit residuals
        #=======================================================================
        # Linearity polynomial coefficient 0 (median value)
        key = 'HIERARCH ESO QC BP-MAP LIN0 MED'
        qc_lin0_med = lintab.readKeyword(key)

        # Linearity polynomial coefficient 1 (median value)
        key = 'HIERARCH ESO QC BP-MAP LIN1 MED'
        qc_lin1_med = lintab.readKeyword(key)

        # Linearity polynomial coefficient 2 (median value)
        key = 'HIERARCH ESO QC BP-MAP LIN2 MED'
        qc_lin2_med = lintab.readKeyword(key)

        
        #subplot(224)
        lin_med=self.lin_med
        norm_lin_med = lin_med - (qc_lin0_med + qc_lin1_med*lin_med + qc_lin2_med*lin_med**2)

        linearity_residual = numpy.sqrt(sum(norm_lin_med**2))

        #print this_prog, '[info] linearity_residual    =  ', linearity_residual, '             ADU'

        lin_dit=self.lin_dit
        subplot.plot(lin_dit, norm_lin_med, 'bo')

 
        subplot.axhline(y=0.0, color='b', linestyle='-', linewidth=2.0, alpha=1.0)

        title='flux residuals \n(median flux minus linearity fit)'
        subplot.set_title(title, fontsize=12)

        Xlab='DIT (seconds)'
        Ylab='median flux (lamp-on) minus linearity fit (ADU)'
        spectrum = SpectrumDisplay()
        spectrum.setLabels(Xlab,Ylab)
      
        xLim = 0,80
        subplot.set_xlim(xLim)
        xx1, xx2 = subplot.get_xlim()
        yy1, yy2 = subplot.get_ylim()

        subplot.text(xx1, yy2-0.10*(yy2-yy1), r'$\rm{fit:=} %5.3f + %5.3f*flux + %5.3e*flux^2\ \ \rm{(LIN0...2.MED)}$' %(qc_lin0_med, qc_lin1_med, qc_lin2_med), color='blue', size=11)
        subplot.text(xx1, yy2-0.20*(yy2-yy1), r'$\rm{residual} (\sqrt{\sum(medianflux_i - fit)^2}) = %8.4f$  ADU' %(linearity_residual), color='blue', size=11)

	subplot.set_xlabel(Xlab)
        subplot.set_ylabel(Ylab)
        subplot.tooltip='flux residuals vs DIT'


                  
    def displayProducts(self):
        if self.product_id == 'gain' :
           self.fig1(self.gain_info,self.subplot_common)
        elif self.product_id == 'flux vs. median flux/DIT' :
           self.fig2(self.lin_det_info,self.subplot_common)
        elif self.product_id == 'lamp flux fit' :
          self.fig3(self.lin_det_info,self.bp_coeff,self.subplot_common)
        elif self.product_id == 'linearity fit residuals' :
           self.fig4(self.lin_det_info,self.bp_coeff,self.subplot_common)
        
    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
      if (self.bp_map_nl_found == True) :
        self.prepTitleAndTooltipImage()

        self.dpm = sinfo_plot_common.DataPlotterManager() 
        self.displayProducts()
 
      else :
        self.caseNodata() 
  
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setInteractiveParameters(self):
      paramList = list()
      rec_id=self.rec_id
      self.par = sinfo_parameters_common.Parameters()

      if rec_id == "sinfo_rec_detlin" :
        self.par.setDetlinParameters(paramList,"sinfo_rec_detlin","bp_lin")
        print "recipe" ,rec_id,  "no parameters"

      else :
         print "recipe" ,rec_id,  "not supported"

      return paramList

    def setWindowHelp(self):
      help_text = """
This is an interactive window which help asses the quality of the execution of a recipe.
"""
      return help_text

    def setWindowTitle(self):
      title = 'SINFONI Interactive ' + self.rec_subtitle  
      return title

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise

#This is the 'main' function
if __name__ == '__main__':

  # import reflex modules
  from reflex import *
  from reflex_interactive_app import *
  from pipeline_display import *
  from pipeline_product import *

  # Create interactive application
  interactive_app = PipelineInteractiveApp(enable_init_sop=True)

  # PECULIAR XSH needs this in order to be able later to get from an input FITS
  # the ins-mode, arm (and recipe) IDs, used in titles and param setting
  # get inputs from the command line
  interactive_app.parse_args()
  inputs = interactive_app.inputs
  #(inputs, args) = interactive_app.parse_args()

  #Check if import failed or not
  print "import_sucess=", import_sucess
  if import_sucess == 'false' :
     interactive_app.setEnableGUI(false)
    
  #interactive_app.setEnableGUI(True)
  #Open the interactive window if enabled
  if interactive_app.isGUIEnabled() :
    #Get the specific functions for this window
    dataPlotManager = DataPlotterManager()
    #print inputs.in_sof
    #dataPlotManager.checkSofIsNotEmpty(inputs.in_sof)
    #With the following call XSH get the: ins-mode, arm (and recipe) IDs
    dataPlotManager.getArmId(inputs.in_sof)
    #Set recipe ID in order to build proper param list, display layout
    dataPlotManager.setRecId("sinfo_rec_detlin")
    interactive_app.setPlotManager(dataPlotManager)
    interactive_app.showGUI()
  
  else :
    interactive_app.passProductsThrough()
   
  #Print outputs. This is parsed by the Reflex python actor to get the results
  #Do not remove
  interactive_app.print_outputs()
  sys.exit()
