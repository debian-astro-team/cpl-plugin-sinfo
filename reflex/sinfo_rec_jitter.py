# import the needed modules
try:
  import reflex
  import_sucess = 'true'

# Then here is python code moved in sinfo_calib_interactive_common.py

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"

#This is the 'main' function
if __name__ == '__main__':
  from sinfo_log_invocation import *
  log_invocation()
  # import reflex modules
  from reflex import *
  from reflex_interactive_app import *
  from pipeline_display import *
  from pipeline_product import *
  from sinfo_obs_interactive_common import *

  # Create interactive application
  interactive_app = PipelineInteractiveApp(enable_init_sop=True)

  # PECULIAR XSH needs this in order to be able later to get from an input FITS
  # the ins-mode, arm (and recipe) IDs, used in titles and param setting
  # get inputs from the command line
  interactive_app.parse_args()
  inputs = interactive_app.inputs
  #(inputs, args) = interactive_app.parse_args()

  #Check if import failed or not
  if import_sucess == 'false' :
     interactive_app.setEnableGUI(false)
    
  #Open the interactive window if enabled
  if interactive_app.isGUIEnabled() :
    #Get the specific functions for this window
    dataPlotManager = DataPlotterManager()
    dataPlotManager.getBandId(inputs.in_sof)
    #Set recipe ID in order to build proper param list, display layout
    dataPlotManager.setRecId("sinfo_rec_jitter")
    interactive_app.setPlotManager(dataPlotManager)
    interactive_app.showGUI()
  else :
    interactive_app.passProductsThrough()
   
  #Print outputs. This is parsed by the Reflex python actor to get the results
  #Do not remove
  interactive_app.print_outputs()
  sys.exit()
