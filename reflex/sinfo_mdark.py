# import the needed modules
try:
  import reflex
  import_sucess = 'true'
  
# Then here is python code moved in xsh_object_interactive_common.py

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"


# import the needed modules
try:
#  import sys
  import numpy
  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits
    
  import sinfo_plot_common
  import sinfo_parameters_common

#  import wx
#  import matplotlib
  
  import reflex
  #from xsh_plot_common import *
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  import reflex_plot_widgets

  from pipeline_product import PipelineProduct
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:

    def paragraph(self,text, width=None):
       """ wrap text string into paragraph
           text:  text to format, removes leading space and newlines
           width: if not None, wraps text, not recommended for tooltips as
                  they are wrapped by wxWidgets by default
       """
       import textwrap
       #print 'width=',width,'Text=',text
       if width is None:
           return textwrap.dedent(text).replace('\n', ' ').strip()
       else:
           return textwrap.fill(textwrap.dedent(text), width=width) 


    

    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames
    def readFitsData(self, fitsFiles):
      #Control variable to check if the interesting files where at the input
      #print fitsFiles
      
      self.rmdark = None
      self.mdark_found = False
      self.rmdark_found = False
      self.nima = 0
      self.binx = 1
      self.ext_ima=0
      self.biny = 1
      self.arm_id=0
      self.qc_id='DARK_MEAN'
      self.bp_map_hp_found = False
      
      self.product_id = 'Master Dark'
      self.labels_prod=['Master Dark','Hot Pixel Mask','Column Average','Row Average','Histogram']
      self.labels_qc=['DARK MEAN','HOTPIX']
      #Read all the products
      frames = dict()
      for frame in fitsFiles:
        if frame == '' :
          continue

        #print 'frame name: ', frame.name
        header = pyfits.open(frame.name)
        #Make sure to have only products from the same recipe
        #that used this (common) script 
        if 'ESO PRO REC1 ID' in header[0].header :
           rec_id = header[0].header['ESO PRO REC1 ID']
           print "rec_id=", rec_id
           print self.rec_id
           if rec_id == self.rec_id :
              category = frame.category
              frames[category] = frame
              print "frame name:", frame.name
 
# For any arm search a list of input frames
      print frames
      key = "MASTER_DARK"
      if key in frames :
        self.mdark_found = True
        hdulist = frames[key]
        self.mdark = PipelineProduct(hdulist)
        #print self.mdark.image.shape
        print "found", key

      key = "RMASTER_DARK"
      if key in frames :
        self.rmdark_found = True
        hdulist = frames[key]
        self.rmdark = PipelineProduct(hdulist)
        #print self.rmdark.image
        print "found", key

      key = "BP_MAP_HP"
      if key in frames :
        self.bp_map_hp_found = True
        hdulist = frames[key]
        self.bp_map_hp = PipelineProduct(hdulist)
        print "found", key

      key = "RBP_MAP_HP"
      if key in frames :
        self.rbp_map_hp_found = True
        hdulist = frames[key]
        self.rbp_map_hp = PipelineProduct(hdulist)
        print "found", key


        
    #Set rec id (to have proper recipe parameters prefix)
    def setRecId(self, rec_id):
      #Recipe ID variable to properly define params
      self.rec_id = rec_id

    #Get arm setting
    def getArmId(self, sof):
      #Recipe ID variable to properly define params
      self.rec_id = "sinfo_rec_mdark"
      nf = 0

      frames = dict()
      files = sof.files
      for f in files:
        frame=f.name
        if frame == '' :
          continue
        else :
           nf += 1
           hdulist = pyfits.open(frame)
           rec_id_list = hdulist[0].header['ESO PRO REC1 ID']
      if nf != 0 :
         self.rec_id = rec_id_list[0:]

      #print "self.rec_id", self.rec_id
      if self.rec_id == "sinfo_rec_mdark" :
         self.rec_subtitle = "Master Dark Creation. "
             
    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      nrows=self.nima
      row=1;

      if self.mdark_found == True and self.bp_map_hp_found == True: 
        self.subplot_common  = figure.add_subplot(1,1,1)

        row +=1

        self.product_selector = figure.add_axes([0.1, 0.87, 0.23, 0.13])

      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)

    def plotData(self,arm_id):
        self.plotProductsGraphics()

    def plotWidgets(self) :
        widgets = list()
        if self.mdark_found == True and self.bp_map_hp_found == True: 

           labels_prod = self.labels_prod
           self.radiobutton_pro = reflex_plot_widgets.InteractiveRadioButtons(self.product_selector, self.setProductSelectCallBack, labels_prod, 0, title="")
           widgets.append(self.radiobutton_pro)



        #labels_qc = self.labels_qc
        #self.radiobutton_pro = reflex_plot_widgets.InteractiveRadioButtons(self.qc_selector, self.setQCSelectCallBack, labels_qc, 0, title="")
        #widgets.append(self.radiobutton_pro)

        return widgets

    def setArmSelectCallBack(self, arm_id) :
        if arm_id == 'RED':
           self.arm_id = 2
           self.ext_ima = 2
        else :
           self.arm_id = 1
           self.ext_ima = 1
      
        self.plotImage()
 

    def setQCSelectCallBack(self, qc_id) :
        self.qc_id = qc_id
        self.subplot_hot_pixels.cla()
 

    def setProductSelectCallBack(self, product_id) :
        print "Selected product id:", product_id
        self.product_id = product_id
        self.subplot_common.cla()
        self.displayProducts()
   
        print "product_id:", self.product_id
         
    def setPlotSpectrum(self,obj,qual) :
        self.plotImageSlice(obj,qual,self.subplot_spectrum,title,tooltip,Xlab,Ylab);

    def readRefImage(self,ref):
        ref.readImage(0)
        self.Rmed  = numpy.median(ref.image)
        self.Rmean = ref.image.mean()
        self.Rstd  = ref.image.std()
           
    def readMastImage(self,mast):
        #ref.readImage(0)
        nyM, nxM = mast.image.shape
        self.Mmed  = numpy.median(mast.image)
        self.Mmean = mast.image.mean()
        self.Mstd  = mast.image.std()
           
    

    def plotColAverage(self,mast,ref,subplot) :
        # PLOTS (1) :  Plotting the current DARK image (master) and reference collapsed along columns
        spectrum = SpectrumDisplay()
        if mast != None :
           self.readMastImage(mast)
           nyM, nxM = mast.image.shape
           
           Mcol = mast.image.sum(axis=1)/float(nxM)
           
           self.xcol2 = range(nyM)
           self.y1_plot1 = Mcol.min()
           self.y2_plot1 = Mcol.max()
           #AMO: temporrarily failing
           #plot(xcol2, Mcol, color=colM, linewidth=1)
           #xlim(0,nxM)
           #ylim(self.y1_plot1,self.y2_plot1)
           #Ylab=ylabel('column average (counts)')
           Xlab=''
           Ylab='column average (counts)'
           #title1 = 'Column Average'
           #title(title1, fontsize=10)
           title='Column Average'
           tooltip='plot of '+title+' vs raws'
           subplot.set_xlim(nxM)
           subplot.set_ylim(self.y2_plot1)
           subplot.set_title(title, fontsize=10)
	   red_patch = matplotlib.patches.Patch(color='red', label='reference dark')
	   blue_patch = matplotlib.patches.Patch(color='blue', label='master dark')

           if ref != None :
              subplot.legend(handles=[red_patch,blue_patch])
           else :   
              subplot.legend(handles=[blue_patch])
	   
           spectrum.setLabels(Xlab,Ylab)
           spectrum.display(subplot,title,self.paragraph(tooltip),self.xcol2,Mcol,None, autolimits = True)
           
        if ref != None :
           self.readRefImage(ref)
	   nyR, nxR = ref.image.shape
           Rcol = ref.image.sum(axis=1)/float(nxR)
           #plot(xcol2, Rcol, color=colR, linewidth=1)
           spectrum.overplot(subplot,self.xcol2,Rcol,color='red')

    def plotRowAverage(self,mast,ref,subplot) :
        # PLOTS (2) : Plotting the current DARK image (master) and reference collapsed along rows
        spectrum = SpectrumDisplay()

        if mast != None :
           nyM, nxM = mast.image.shape
           Mrow = mast.image.sum(axis=0)/float(nyM)
           self.xcol2 = range(nyM)
          
           #plot(xrow2, mast[100,:], color=colM1, alpha=0.5)
           #plot(xrow2, mast[500,:], color=colM2, alpha=0.5)
           #plot(xrow2, mast[900,:], color=colM3, alpha=0.5)
           #plot(xrow2, Mrow, color=colM, linewidth=1)

           #xlim(0,nyM)
           #ylim(self.y1_plot1,self.y2_plot1)
           title = 'Row Average'
           tooltip='plot of '+title+' vs cols'
           subplot.set_xlim(nxM)
           self.y2_plot1 = Mrow.max()
           subplot.set_ylim(self.y2_plot1)

           subplot.set_title(title, fontsize=10)
	   red_patch = matplotlib.patches.Patch(color='red', label='reference dark')
	   blue_patch = matplotlib.patches.Patch(color='blue', label='master dark')
           if ref != None :
              subplot.legend(handles=[red_patch,blue_patch])
           else :   
              subplot.legend(handles=[blue_patch])

           Xlab=''
           Ylab='row average (counts)'
           spectrum.setLabels(Xlab,Ylab)
           spectrum.display(subplot,title,self.paragraph(tooltip),self.xcol2,Mrow,None, autolimits = True) 
           
           #self.plotImageSlice(mast,None,self.subplot_gra_row,title,tooltip,Xlab,Ylab);
           
        if ref != None :
           self.readRefImage(ref)
	   nyR, nxR = ref.image.shape
           Rrow = ref.image.sum(axis=0)/float(nyR)
           #plot(xrow2, Rrow, color=colR, linewidth=1)
           spectrum.overplot(subplot,self.xcol2,Rrow,color='red')


    def histogram(self,a, bins):
	# Note that argument names below are reverse of the
	# searchsorted argument names
	n = numpy.searchsorted(numpy.sort(a), bins)
	n = numpy.concatenate([n, [len(a)]])
        return n[1:]-n[:-1]


    def gaussian(self,height, center_x, width_x):
        """Returns a gaussian function with the given parameters"""
        #width_x = float(width_x/2.354)
        width_x = float(width_x)
        return lambda x: height*exp(-(((x - center_x)/width_x)**2)/2.0)


      
    def moments(self,data, Xrange):
        """Returns (height, x, width_x)
        the gaussian parameters of a 1D distribution by calculating its
        moments """
        total = data.sum()
        X = Xrange
        x_cent = (X*data).sum()/total
        width_x = numpy.sqrt((X**2*data).sum()/total - ((X*data).sum()/total)**2)
        #width_x = sqrt(((data - x_cent)**2).sum()/total)
        height = data.max()
        return height, x_cent, width_x

    def hist1(self,mast,ref,subplot):
        spectrum = SpectrumDisplay()

        #
        #===================================================================================
        # Histograms (1): Plotting the master reference and reference-master DARK histograms
        #===================================================================================

        delhist1  = 30
        bin_size1 = 0.5
        if mast != None :
           self.readMastImage(mast)
           x1M_hist = self.Mmed - delhist1
           x2M_hist = self.Mmed + delhist1
           hrangeM = numpy.arange(x1M_hist,x2M_hist,bin_size1)
           Mhist  = self.histogram(mast.image.flat, hrangeM)
           subplot.semilogy(hrangeM, Mhist, linestyle='steps-mid', linewidth=1.5, color='blue')           # NOTE: USING linestyle='steps' FUCKS UP THE Y-AXIS LIMITS!
           Pline1M = subplot.semilogy((0.0, 0.0), (1.0, 1000000), 'g--', linewidth=1, alpha=1.0)

           min_hist = Mhist.min()
           max_hist = Mhist.max()
           y1hist = min_hist # - 2.*min_hist
           y2hist = max_hist + (max_hist - min_hist)/2.0

           #xlim(Mmed - delhist1, Mmed + delhist1)
           #ylim(y1hist, y2hist)                    # limits focussed on master dark histogram (this is better)



           Xlab='counts'
           Ylab='log(number)'
           title='log(histogram)'
           title = 'Histogram'
           tooltip='Plot of '+title+' vs cols'
         
           subplot.set_xlim(self.Mmed - delhist1, self.Mmed + delhist1)
           subplot.set_ylim(-y1hist, y2hist)
           subplot.set_title(title, fontsize=12)
           subplot.tooltip = tooltip
	   blue_patch = matplotlib.patches.Patch(color='blue', label='master dark')           
           subplot.legend(handles=[blue_patch])
           spectrum.setLabels(Xlab,Ylab)


           #=======================================================
           # Fitting a Gaussian to the histogram of the MASTER_DARK
           #=======================================================


           params = self.moments(Mhist, hrangeM)
           fit = self.gaussian(*params)
           (height, xcent, width_x) = params

           qc_gaf_fpn = width_x/2.0                                                               # qc_gaf_fpn is a local variable needed for the QC1 DB


        #subplot(233)
        if ref != None and mast != None :
	   self.readRefImage(ref)
	   nyR, nxR = ref.image.shape


           diff = ref.image/self.Rmed - mast.image/self.Mmed  # Computing the difference image:  reference - master dark
           Dmed  = numpy.median(diff)

           x1R_hist = self.Rmed - delhist1
           x2R_hist = self.Rmed + delhist1
           x1D_hist = Dmed - delhist1
           x2D_hist = Dmed + delhist1

           hrangeR = numpy.arange(x1R_hist,x2R_hist,bin_size1)
           hrangeD = numpy.arange(x1D_hist,x2D_hist,bin_size1)

           Rhist  = self.histogram(ref.image.flat, hrangeR)
           Dhist  = self.histogram(diff.flat, hrangeD)

           subplot.semilogy(hrangeR, Rhist, linestyle='steps-mid', linewidth=1.5, color='red')           # Creates a log plot from linear input!
           subplot.semilogy(hrangeD, Dhist, linestyle='steps-mid', linewidth=1.5, color='black')


           #IPE removed after upgrade IPE legend('reference','master','ref-mast'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)


      	   red_patch = matplotlib.patches.Patch(color='red', label='reference dark')

	   black_patch = matplotlib.patches.Patch(color='black', label='reference-master')
           subplot.legend(handles=[red_patch,blue_patch,black_patch])


        
           #spectrum.display(subplot,title,self.paragraph(tooltip),self.xcol2,Dcol,None, autolimits = True) 


           #=======================================================
           # Fitting a Gaussian to the histogram of the MASTER_DARK
           #=======================================================



           params = self.moments(Dhist, hrangeD)
           fit = self.gaussian(*params)
           (Dheight, Dxcent, Dwidth_x) = params


           qc_gaf_fpn = width_x/2.0


           plotfit = 'no'
           if plotfit == 'yes':
	      print ' '
	      print this_prog,'[info] Gauss fit parameters (A, x_cent, x_width) =', height, xcent, width_x
	      print ' '
	      fitG = height * exp(-0.5*((hrangeM - xcent)/qc_gaf_fpn)**2)                    # using the fit parameters in a gaussian

	      semilogy(hrangeM, fitG, linestyle='steps-mid', linewidth=1.5, color='green')   # over-plotting the fit gaussian on the master dark histogram
	      ylim(y1hist, y2hist)


              print ' '
              print this_prog,'[info] Pipeline master DARK median:         qc_darkmed_ave = %8.4f +/- %5.2f ADU' %(qc_darkmed_ave, qc_darkmed_stdev)
              print this_prog,'[info] From Gauss fit to master dark:           qc_gaf_fpn = %8.4f' %(qc_gaf_fpn)


    def hist2(self,mast,ref,subplot):

      
        #=============================================================
        # Histograms (2): Plotting the histograms of the RAW DARK frames
        #=============================================================


        #subplot(236)

        raw_21 = raw_2 - raw_1
        raw_32 = raw_3 - raw_2

        R21mean  = raw_21.mean()
        R21std   = raw_21.std()
        R32mean  = raw_32.mean()
        R32std   = raw_32.std()

        x1R21_hist  = 0.0 - delhist2
        x2R21_hist  = 0.0 + delhist2

        hrangeR21  = arange(x1R21_hist, x2R21_hist, bin_size2)

        R21  = raw_21.flat  - R21mean
        R32  = raw_32.flat  - R32mean

        R21hist   = histogram(R21,  hrangeR21)
        R32hist   = histogram(R32,  hrangeR21)

        R21_max  = max(R21hist)


        semilogy(hrangeR21,  R21hist,  linestyle='steps-mid', linewidth=1.5, color='magenta')        # Creates a log plot from linear input!
        semilogy(hrangeR21, R32hist, linestyle='steps-mid', linewidth=1.5, color='cyan')             # NOTE: USING linestyle='steps' FUCKS UP THE Y-AXIS LIMITS!

        Pline1M = semilogy((0.0, 0.0), (1.0, 1000000), 'g--', linewidth=1, alpha=1.0)

        y1, y2 = ylim()
        ylim(10.,y2)

        xlabel('counts')
        ylabel('log(number)')
        title('log(histogram) of raw darks', fontsize=12)


        #IPE removed after upgrade IPE legend('(raw_2 - raw_1)','(raw_3 - raw_2)'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)


    def imageResize(self, img_disp,obj) :
        import numpy as np
        y_size, x_size = obj.image.shape
        #print "Image Resize: x=", x_size, "y=", y_size
        if y_size > 200:
           if x_size > 9200 :
               scal_x=8
           elif x_size > 4600 :
               scal_x=4
           elif x_size > 2300 :
               scal_x=2
           else :
               scal_x=1

           if y_size > 9200 :
               scal_y=8
           elif y_size > 4600 :
               scal_y=4
           elif y_size > 2300 :
               scal_y=2
           else :
               scal_y=1
               
           #print "Image Resize: scal_x=",scal_x, "scal_y=",scal_y
           self.scal_x=scal_x
           self.scal_y=scal_y
           
           y_size, x_size = y_size/scal_y, x_size/scal_x
           m=obj.image
           n = m.reshape((y_size,m.shape[0]//y_size,x_size,-1)).mean(axis=3).mean(1)

           
           #print n.shape
           obj.image=n
           #print "Image Resize: y_size=",y_size,"x_size=",x_size
  	   img_disp.setLimits((0,x_size),(0,y_size))
           self.sx=x_size
           self.sy=y_size


    def prepImage(self,img_disp,obj,ext,Xlab,Ylab):
          self.readImage(obj,ext)
 	  img_disp.setLabels(Xlab,Ylab)
          self.imageResize(img_disp,obj)
           

    def plotImageOnly(self,obj,ext_ima,binx,biny,Xlab,Ylab,subplot,title,tooltip,cmap=None):
          img_disp = ImageDisplay()
	  img_disp.cmap=cmap
          self.prepImage(img_disp,obj,ext_ima,Xlab,Ylab)
          img_disp.display(subplot, title, self.paragraph(tooltip),obj.image)
      
    def prepTitleAndTooltipImage(self) :
        title_pref = 'Linear-extracted and Merged Spectrum.'	      
        title_spectrum   = title_pref

        if self.product_id == 'Master Dark' :
           self.obj=self.mdark
           spec_frame   = 'Master Dark '
           title_mdark   = 'Master Dark'
           self.tooltip_mdark ="""\
                Image of the master dark frame. 
                """
        elif self.product_id == 'Hot Pixel Mask' :
           spec_frame   = 'Hot pixel mask '
           title_mdark   = 'Hot pixel mask'
           self.tooltip_bp_map_hp ="""\
                Image of the hot pixel mask frame. 
                """
        elif self.product_id == 'Column Average' :
           spec_frame   = 'Hot pixel mask '
           title_mdark   = 'Hot pixel mask'
           self.tooltip_bp_map_hp ="""\
                Image of the hot pixel mask frame. 
                """
        elif self.product_id == 'Row Average' :
           spec_frame   = 'Hot pixel mask '
           title_mdark   = 'Hot pixel mask'
           self.tooltip_bp_map_hp ="""\
                Image of the hot pixel mask frame. 
                """
        elif self.product_id == 'Histogram' :
           spec_frame   = 'Hot pixel mask '
           title_mdark   = 'Hot pixel mask'
           self.tooltip_bp_map_hp ="""\
                Image of the hot pixel mask frame. 
                """
           self.obj = self.bp_map_hp

        self.title_frame   = spec_frame

    def plotImage(self) :
        self.prepTitleAndTooltipImage()
        self.plotImageOnly(self.obj,self.ext_ima, self.binx,self.biny,'X [pix]', 'Y [pix]', self.subplot_common,self.title_frame,self.tooltip_mdark)


    def plotImageMDark(self,obj,subplot) :
        self.prepTitleAndTooltipImage()
        self.plotImageOnly(obj,self.ext_ima, self.binx,self.biny,'X [pix]', 'Y [pix]', subplot,self.title_frame,self.tooltip_mdark)


    def readImage(self,obj,ext):
          obj.readImage(fits_extension=ext)
          obj.read2DLinearWCS(fits_extension=ext)
 

    def plotImageHP(self,obj,subplot) :
        self.prepTitleAndTooltipImage()
        cmap =matplotlib.pyplot.cm.gray_r
	
        self.plotImageOnly(obj,self.ext_ima, self.binx,self.biny,'X [pix]', 'Y [pix]', subplot,self.title_frame,self.tooltip_bp_map_hp,cmap)


    def caseNodata(self) :
        #Data not found info
        self.subtext_nodata.set_axis_off()
        self.text_nodata = """\
                           ON-frame, Residual table 
                           (PRO.CATG=MASTER_DARK,BP_MAP_HP) 
                           not found in the products. 
                           This may be due to a recipe failure. 
                           Check your input parameter values, 
                           correct possibly typos and
                           press 'Re-run recipe' button."""
        self.subtext_nodata.text(0.1, 0.6, self.text_nodata, color='#11557c', 
                                 fontsize=18, ha='left', va='center', alpha=1.0)
        self.subtext_nodata.tooltip="""\
                                    Merged spectrum not found in the products"""
        #print "found no spectrum data"

    def displayProducts(self):

        if self.product_id == 'Master Dark' :
            self.plotImageMDark(self.mdark,self.subplot_common)
        elif self.product_id == 'Hot Pixel Mask' :
            self.plotImageHP(self.bp_map_hp,self.subplot_common)
        elif self.product_id == 'Column Average' :
            self.plotColAverage(self.mdark,self.rmdark,self.subplot_common)
        elif self.product_id == 'Row Average' :
            self.plotRowAverage(self.mdark,self.rmdark,self.subplot_common)
        elif self.product_id == 'Histogram' :
            self.hist1(self.mdark,self.rmdark,self.subplot_common)
 
        
    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
      if (self.mdark_found == True and self.bp_map_hp_found == True) :
        self.prepTitleAndTooltipImage()
        
        sy = self.obj.all_hdu[self.ext_ima].header['NAXIS2']
        self.ima_sy=sy
        self.dpm = sinfo_plot_common.DataPlotterManager()
        self.displayProducts()

      else :
        self.caseNodata() 
  
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setInteractiveParameters(self):
      paramList = list()
      rec_id=self.rec_id
      self.par = sinfo_parameters_common.Parameters()

      if rec_id == "sinfo_rec_mdark" :
         #self.par.setGenParameters(paramList,"sinfo_rec_mdark","common")
         self.par.setDarkParameters(paramList,"sinfo_rec_mdark","bp_noise")
         print 'pippo'

      else :
         print "recipe" ,rec_id,  "not supported"

      return paramList

    def setWindowHelp(self):
      help_text = """
This is an interactive window which help asses the quality of the execution of a recipe.
"""
      return help_text

    def setWindowTitle(self):
      title = 'SINFONI Interactive ' + self.rec_subtitle  
      return title

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise

#This is the 'main' function
if __name__ == '__main__':

  # import reflex modules
  from reflex import *
  from reflex_interactive_app import *
  from pipeline_display import *
  from pipeline_product import *

  # Create interactive application
  interactive_app = PipelineInteractiveApp(enable_init_sop=True)

  # PECULIAR XSH needs this in order to be able later to get from an input FITS
  # the ins-mode, arm (and recipe) IDs, used in titles and param setting
  # get inputs from the command line
  interactive_app.parse_args()
  inputs = interactive_app.inputs
  #(inputs, args) = interactive_app.parse_args()

  #Check if import failed or not
  print "import_sucess=", import_sucess
  if import_sucess == 'false' :
     interactive_app.setEnableGUI(false)
    
  #interactive_app.setEnableGUI(True)
  #Open the interactive window if enabled
  if interactive_app.isGUIEnabled() :
    #Get the specific functions for this window
    dataPlotManager = DataPlotterManager()
    #print inputs.in_sof
    #dataPlotManager.checkSofIsNotEmpty(inputs.in_sof)
    #With the following call XSH get the: ins-mode, arm (and recipe) IDs
    dataPlotManager.getArmId(inputs.in_sof)
    #Set recipe ID in order to build proper param list, display layout
    dataPlotManager.setRecId("sinfo_rec_mdark")
    interactive_app.setPlotManager(dataPlotManager)
    interactive_app.showGUI()
  
  else :
    interactive_app.passProductsThrough()
   
  #Print outputs. This is parsed by the Reflex python actor to get the results
  #Do not remove
  interactive_app.print_outputs()
  sys.exit()
