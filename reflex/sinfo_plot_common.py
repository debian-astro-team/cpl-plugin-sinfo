# import the needed modules
try:
  import reflex
  import_sucess = 'true'

# Then here is python code moved in xsh_object_interactive_common.py

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"


# import the needed modules
try:
#  import sys
  import numpy

  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits
  
  import sinfo_parameters_common
#  import wx
#  import matplotlib
  import reflex
  #from xsh_plot_common import *
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  import reflex_plot_widgets
  from pipeline_product import PipelineProduct
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  plotSpectrum()    to control 1D image product layout
#  plotImage()       to control 2D image product layout
#  plotTable()       to control 1D table product layout
#  oplotTable()      to control 1D table product overplots
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:

    def paragraph(self,text, width=None):
       """ wrap text string into paragraph
           text:  text to format, removes leading space and newlines
           width: if not None, wraps text, not recommended for tooltips as
                  they are wrapped by wxWidgets by default
       """
       import textwrap
       #print 'width=',width,'Text=',text
       if width is None:
           return textwrap.dedent(text).replace('\n', ' ').strip()
       else:
           return textwrap.fill(textwrap.dedent(text), width=width) 


    #1D spectrum plots
    def plotSpectrum(self,data,errs,subplot,title,tooltip):
        data.readSpectrum(0)
        errs.readSpectrum(1)
        spectrum = SpectrumDisplay()
        spectrum.setLabels('Wavelength [nm]','Total Flux ['+data.bunit+']')
        spectrum.display(subplot,title,self.paragraph(tooltip),data.wave,data.flux,errs.flux, autolimits = True) 

    #2D image display
    def plotImage(self,obj,bpm,subplot,title,tooltip):

          obj.readImage(fits_extension=0)
          obj.read2DLinearWCS(fits_extension=0)
          img_obj = ImageDisplay()

          bpm.readImage(fits_extension=2)
          img_bpm = ImageDisplay()

	  img_obj.setLabels('Wavelength [nm]','Position Along Slit [arcsec]')
          y_size, x_size = obj.image.shape

          img_obj.setXLinearWCSAxis(obj.crval1,obj.cdelt1,obj.crpix1)
          img_obj.setYLinearWCSAxis(obj.crval2,obj.cdelt2,obj.crpix2)

	  #img_obj.setLimits((1,x_size),(0,y_size))
          img_obj.display(subplot, title, self.paragraph(tooltip),obj.image,bpm.image)
          
    def imageResize(self, img_disp,obj) :
        import numpy as np
        y_size, x_size = obj.image.shape
        #print "Image Resize: x=", x_size, "y=", y_size
        if y_size > 200:
           if x_size > 9200 :
               scal_x=8
           elif x_size > 4600 :
               scal_x=4
           elif x_size > 2300 :
               scal_x=2
           else :
               scal_x=1

           if y_size > 9200 :
               scal_y=8
           elif y_size > 4600 :
               scal_y=4
           elif y_size > 2300 :
               scal_y=2
           else :
               scal_y=1
               
           #print "Image Resize: scal_x=",scal_x, "scal_y=",scal_y
           self.scal_x=scal_x
           self.scal_y=scal_y
           
           y_size, x_size = y_size/scal_y, x_size/scal_x
           m=obj.image
           n = m.reshape((y_size,m.shape[0]//y_size,x_size,-1)).mean(axis=3).mean(1)

           
           #print n.shape
           obj.image=n
           #print "Image Resize: y_size=",y_size,"x_size=",x_size
  	   img_disp.setLimits((0,x_size),(0,y_size))
           self.sx=x_size
           self.sy=y_size
    def readImage(self,obj,ext):
          obj.readImage(fits_extension=ext)
          obj.read2DLinearWCS(fits_extension=ext)
          
    def prepImage(self,img_disp,obj,ext,Xlab,Ylab):
          self.readImage(obj,ext)
 	  img_disp.setLabels(Xlab,Ylab)
          self.imageResize(img_disp,obj)
           
    def plotImageOnly(self,obj,ext_ima,binx,biny,Xlab,Ylab,subplot,title,tooltip):
          img_disp = ImageDisplay()
          self.prepImage(img_disp,obj,ext_ima,Xlab,Ylab)
          img_disp.display(subplot, title, self.paragraph(tooltip),obj.image)

    def plotImageOrderdef(self,obj,ext_ima,ext_tab,binx,biny,Xlab,Ylab,Xcol1,Ycol1,subplot,title,tooltip):
          self.img_disp = ImageDisplay()
          self.prepImage(self.img_disp,obj,ext_ima,Xlab,Ylab)
 
          #As this image has intensities in the range 0-50 we need to rescale z
          if ext_ima == 1 :
             z_lim = (0,45)
          else :
             z_lim = (0,26)
          self.img_disp.setZLimits(z_lim)
          self.img_disp.display(subplot, title, self.paragraph(tooltip),obj.image)



    def oplotImageScatterTrace(self,tab,ext_tab,sx,subplot, marker = 'o', size = 10, color = 'blue' ):
          #print 'ext_tab=',ext_tab, tab.all_hdu[ext_tab].data
          coeff_0 = tab.all_hdu[ext_tab].data.field('COEFF_0')
          coeff_1 = tab.all_hdu[ext_tab].data.field('COEFF_1')
          coeff_2 = tab.all_hdu[ext_tab].data.field('COEFF_2')
          coeff_3 = tab.all_hdu[ext_tab].data.field('COEFF_3')
          coeff_4 = tab.all_hdu[ext_tab].data.field('COEFF_4')

          #print "coeff_0=",coeff_0
          nrows=tab.all_hdu[ext_tab].data.shape[0]
          #print "nrows=",nrows,"sx=",sx
          x=numpy.linspace(1,sx,20)/self.scal_x
          for i in range(nrows):
              #x = float(i)
              #print x
              x2=x*x
              x3=x2*x
              x4=x2*x2
              y=coeff_0[i]+ \
                   coeff_1[i]*x+ \
                   coeff_2[i]*x2+ \
                   coeff_3[i]*x3+ \
                   coeff_4[i]*x4
 
              y = y/self.scal_y
              #print i,coeff_0[i],coeff_1[i],x,y
              #print y
              subplot.plot(x,y,marker=marker,color=color)

              


          
    def plotImageWave(self,obj,tab,ext_ima,ext_tab,binx,biny,Xlab,Ylab,Xcol1,Ycol1,subplot,title,tooltip):

          img_disp = ImageDisplay()
          self.plotImageOnly(obj,ext_ima,binx,biny,Xlab,Ylab,subplot,title,tooltip)
          self.oplotImageScatter(img_disp,tab,ext_tab,binx,biny,Xcol1,Ycol1,subplot, 'o', 10, 'green' )


    #2D image display
    def plotImageSlice(self,obj,bpm,order_id,subplot,title,tooltip,Xlab,Ylab):

          obj.readImage(fits_extension=0)
          obj.read2DLinearWCS(fits_extension=0)
          img_obj = ImageDisplay()

	  img_obj.setLabels(Xlab,Ylab)
          y_size, x_size = obj.image.shape
          x = numpy.arange(0, x_size, 1)
          #y = obj.all_hdu[1].data[order_id,:]
  
          img_obj.setXLinearWCSAxis(0,1,0)
	  img_obj.setLabels(Xlab,Ylab)
          #img_bpm = ImageDisplay()
          #imadsp.display(subplot, title, tooltip, image)

          #subplot.plot.setLabels(Xlab,Ylab)
          #subplot.plot.xlabel(Xlab)
          #subplot.plot.ylabel(Ylab)
          obj.plot(subplot, title, tooltip, x_label = Xlab, y_label = Ylab)
          #subplot.plot(x, y, color='blue')
	  #img_obj.setLimits((1,x_size),(0,y_size))
          #img_obj.display(subplot, title, self.paragraph(tooltip),obj.image,bpm.image)

    #2D image display
    def plotImageSlice2(self,obj,bpm,order_id,subplot,title,tooltip,Xlab,Ylab):

          obj.readImage(fits_extension=1)
          obj.read2DLinearWCS(fits_extension=1)
          img_obj = ImageDisplay()

	  img_obj.setLabels(Xlab,Ylab)
          y_size, x_size = obj.image.shape
          x = numpy.arange(0, x_size, 1)
          y = obj.all_hdu[1].data[order_id,:]
          self.scatterPlotScatter2(subplot, x, y, Xlab, Ylab, title, tooltip)

    def oplotImageScatter(self,img,tab,ext,binx,biny,Xcol,Ycol,subplot, marker = 'o', size = 10, color = 'blue' ):
          tab.readTableXYColumns(fits_extension=ext,xcolname=Xcol, ycolname=Ycol)
 

          img.overplotScatter(tab.x_column/binx, tab.y_column/biny, marker, size, color)


              
    def prepImageScatter(self,img_disp,obj,bpm,Xlab,Ylab):

          obj.readImage(fits_extension=0)
          obj.read2DLinearWCS(fits_extension=0)

          bpm.readImage(fits_extension=2)
          img_bpm = ImageDisplay()

	  img_disp.setLabels(Xlab,Ylab)
          y_size, x_size = obj.image.shape

          img_disp.setXLinearWCSAxis(obj.crval1,obj.cdelt1,obj.crpix1)
          img_disp.setYLinearWCSAxis(obj.crval2,obj.cdelt2,obj.crpix2)

	  #self.img_obj.setLimits((0,x_size),(0,y_size))


    #1D spectrum plots
    def plotSpectrum(self,data,errs,qual,subplot,title,tooltip):
        data.readSpectrum(1)
        if errs != None :
           errs.readSpectrum(2)
        nwave = len(data.flux)
        minlimwave = int(nwave*0.25)
        maxlimwave = int(nwave*0.75)

        spectrum = SpectrumDisplay()
        spectrum.setLabels('Wavelength [nm]','Total Flux ['+data.bunit+']')

        spectrum.display(subplot,title,self.paragraph(tooltip),data.wave,data.flux,errs.flux, autolimits = True) 
        #matplotlib.pyplot.tight_layout()
        if self.response_found is True and self.rec_id[16:19] != "nod":
           #in case response is available we need to reduce fonts
           subplot.set_title(title, fontsize=7)
        try:
            subplot.figure.tight_layout()
        except AttributeError:
            pass

        #Flag BP as errors
        qual.readSpectrum(2)

        bpm_all = (qual.flux == 0 ) 
        mask_a_data=numpy.ma.MaskedArray(data.flux, mask=bpm_all).compressed()
        mask_a_wave=numpy.ma.MaskedArray(data.wave,mask=bpm_all).compressed()
        size_a_err=numpy.ma.MaskedArray(errs.flux,mask=bpm_all).compressed()
        #print "size all", mask_a_data.size
        if mask_a_data.size > 0 :
           subplot.errorbar(mask_a_wave, mask_a_data, xerr=None, yerr=size_a_err, color='k', linestyle='', capsize=0)
        #print "size masked", mask_f_data.size
        #print "size spectrum", data.flux.size
        if mask_f_data.size > 0 :
           subplot.errorbar(mask_f_wave, mask_f_data, xerr=None, yerr=size_f_err, color='r', linestyle='', capsize=0)
        subplot.set_xlim(spectrum.wave_lim)
        subplot.set_ylim(spectrum.flux_lim)
       
    def plotImageScatter1Col(self,obj,bpm,tab,ext,binx,biny,Xlab,Ylab,Xcol1,Ycol1,subplot,title,tooltip):

          img_disp = ImageDisplay()

          self.prepImageScatter(img_disp,obj,bpm,Xlab,Ylab)

          self.oplotImageScatter(img_disp,tab,ext,binx,biny,Xcol1,Ycol1,subplot, 'o', 10, 'green' )



          #bpm_filt = ( (bpm.image.astype(int) & self.decode_bp) != 0 )
          #img_disp.display(subplot, title, self.paragraph(tooltip),obj.image,bpm_filt)
          #img_disp.display(subplot, title, self.paragraph(tooltip),obj.image)
          #as the scatter display add extra space round the image (not
          #to have points at edges) we force the bounds of the scatter
          #plot to be the same as the image size.
          #subplot.set_xbound(0,obj.image.shape[1])
          #subplot.set_ybound(0,obj.image.shape[0])

    #2D image display
    def plotImageScatter2Col(self,obj,bpm,tab,ext,binx,biny,Xlab,Ylab,Xcol1,Ycol1,Xcol2,Ycol2,subplot,title,tooltip):

          img_disp = ImageDisplay()

          self.prepImageScatter(img_disp,obj,bpm,Xlab,Ylab)

          self.oplotImageScatter(img_disp,tab,ext,binx,biny,Xcol1,Ycol1,subplot, 'o', 10, 'blue' )
          self.oplotImageScatter(img_disp,tab,ext,binx,biny,Xcol2,Ycol2,subplot, 'o', 10, 'yellow' )

          bpm_filt = ( (bpm.image.astype(int) & self.decode_bp) != 0 )
          img_disp.display(subplot, title, self.paragraph(tooltip),obj.image,bpm_filt)
          #as the scatter display add extra space round the image (not
          #to have points at edges) we force the bounds of the scatter
          #plot to be the same as the image size.
          #subplot.set_xbound(0,obj.image.shape[1])
          #subplot.set_ybound(0,obj.image.shape[0])

    #2D image display
    def plotImageScatter3Col(self,obj,bpm,tab,ext,binx,biny,Xlab,Ylab,Xcol1,Ycol1,Xcol2,Ycol2,Xcol3,Ycol3,subplot,title,tooltip):

          img_disp = ImageDisplay()

          self.prepImageScatter(img_disp,obj,bpm,Xlab,Ylab)


          self.oplotImageScatter(img_disp,tab,ext,binx,biny,Xcol1,Ycol1,subplot, 'o', 10, 'blue' )
          self.oplotImageScatter(img_disp,tab,ext,binx,biny,Xcol2,Ycol2,subplot, 'o', 10, 'yellow' )
          self.oplotImageScatter(img_disp,tab,ext,binx,biny,Xcol3,Ycol3,subplot, 'o', 10, 'green' )

#          self.img_obj.setXLinearWCSAxis(img_obj.crval1,img_obj.cdelt1,img_obj.crpix1)

          bpm_filt = ( (bpm.image.astype(int) & self.decode_bp) != 0 )
          img_disp.display(subplot, title,self.paragraph(tooltip),obj.image,bpm_filt)
          #as the scatter display add extra space round the image (not
          #to have points at edges) we force the bounds of the scatter
          #plot to be the same as the image size.
          #subplot.set_xbound(0,obj.image.shape[1])
          #subplot.set_ybound(0,obj.image.shape[0])


    #1D spectrum table plots
    def plotTable(self,tab,Xcol,Ycol,Xlab,Ylab,subplot,title,tooltip,
    wave = None):

        tab.readTableXYColumns(fits_extension=1,xcolname=Xcol, ycolname=Ycol)
        spectrum = SpectrumDisplay()
        flux_plotmax = 1.2 * numpy.nanmax(tab.y_column)
        flux_plotmin = 0.0
        spectrum.setLabels(Xlab,Ylab)
        spectrum.flux_lim = flux_plotmin, flux_plotmax
        #print flux_plotmin, flux_plotmax
        wave_plotmax = numpy.nanmax(tab.x_column)
        wave_plotmin = numpy.nanmin(tab.x_column)
        spectrum.setWaveLimits((wave_plotmin,  wave_plotmax))
        if wave is not None:
           spectrum.setWaveLimits((wave[0], wave[len(wave)-1]))

        spectrum.display(subplot,title,self.paragraph(tooltip),tab.x_column,tab.y_column) 

        #data = np.clip(randn(250, 250), 0, 100)


    #1D spectrum table plots
    def plotTableErrQual(self,tab,Xcol,Ycol,Ecol,Qcol,bpcode,Error,Xlab,Ylab,subplot,title,tooltip,
    wave = None):

        tab.readTableXYColumns(fits_extension=1,xcolname=Xcol, ycolname=Ycol)
        tab.readTableColumn(fits_extension=1,colname=Ecol)
	yerr=tab.column
        

        spectrum = SpectrumDisplay()
        flux_plotmax = 1.2 * numpy.nanmax(tab.y_column)
        flux_plotmin = 0.0
        spectrum.setLabels(Xlab,Ylab)
        spectrum.flux_lim = flux_plotmin, flux_plotmax
        #print flux_plotmin, flux_plotmax
        wave_plotmax = numpy.nanmax(tab.x_column)
        wave_plotmin = numpy.nanmin(tab.x_column)
        spectrum.setWaveLimits((wave_plotmin,  wave_plotmax))
        if wave is not None:
           spectrum.setWaveLimits((wave[0], wave[len(wave)-1]))
           
        print 'Qcol', Qcol, 'Error', Error   
        if Qcol is not None  and Error is not 'no' :
           tab.readTableColumn(fits_extension=1,colname=Qcol)
	   qual=tab.column
           #Flag BP as errors
           #print int(bpcode)
           bpm_flag = numpy.bitwise_and(qual,int(bpcode) ) == 1
           #print bpm_flag
           mask_f_data=numpy.ma.MaskedArray(tab.y_column, mask=bpm_flag).compressed()
           mask_f_wave=numpy.ma.MaskedArray(tab.x_column,mask=bpm_flag).compressed()
           size_f_err=numpy.ma.MaskedArray(yerr,mask=bpm_flag).compressed()

           bpm_all = (qual == 0 ) 
           mask_a_data=numpy.ma.MaskedArray(tab.y_column, mask=bpm_all).compressed()
           mask_a_wave=numpy.ma.MaskedArray(tab.x_column,mask=bpm_all).compressed()
           size_a_err=numpy.ma.MaskedArray(yerr,mask=bpm_all).compressed()

           if mask_a_data.size > 0 :
              subplot.errorbar(mask_a_wave, mask_a_data, xerr=None, yerr=size_a_err, color='k', linestyle='', capsize=0)


           if mask_f_data.size > 0 :
              subplot.errorbar(mask_f_wave, mask_f_data, xerr=None, yerr=size_f_err, color='r', linestyle='', capsize=0)
        else :
              print 'clear plot'
              subplot.cla()

        if Error is not 'no' :
           spectrum.display(subplot,title,self.paragraph(tooltip),tab.x_column,tab.y_column,yerr, autolimits = True)
        else  : 
           spectrum.display(subplot,title,self.paragraph(tooltip),tab.x_column,tab.y_column, autolimits = True)


    #1D spectrum table plots
    def plotTableColor(self,tab,Xcol,Ycol,Zcol,Xlab,Ylab,colorbar,subplot,title,tooltip,
    wave = None):
        import numpy as np
        import matplotlib as mpl
        tab.readTableXYColumns(fits_extension=1,xcolname=Xcol, ycolname=Ycol)
        tab.readTableColumn(fits_extension=1,colname=Zcol)
	z=tab.column
        #print z
        med_z=np.median(z)
        min_z=np.min(z)
        max_z=np.max(z)
        rms_z=np.std(z)
        mean_z=np.mean(z)
        #print "med ", med_z, "mean ", mean_z, "rms ", rms_z
        x_info = " pos. Resol. med: %#.2e mean: %#.2e rms: %#.2e min: %#.2e max: %#.2e"% (med_z, mean_z, rms_z, min_z, max_z)
        Xlab= Xlab + x_info
        min_z=med_z-rms_z
        if min_z <= 0 : min_z=10000
        #print 'min_z', min_z
        max_z=med_z+rms_z
        #print 'max_z', max_z
        size_z=max_z-min_z
        t1=min_z
        t2=t1+0.25*size_z
        t3=t1+0.50*size_z
        t4=t1+0.75*size_z
        t5=max_z
        mid_z=0.5*(min_z+max_z)
	m=z/(max_z-min_z)*100
        #subplot.scatter(Xcol, Ycol, s=0.4, c=m)
        subplot.scatter(tab.x_column, tab.y_column, s=15, c=m, cmap='hot')
        subplot.set_xlabel(Xlab)
        subplot.set_ylabel(Ylab)
        cmap = mpl.cm.hot
        self.scatterPlotLimits(subplot, tab.x_column, tab.y_column)
        #self.setLabels(x_col, y_col)
        ticks=[t1, t2, t3, t4, t5]
        norm = mpl.colors.Normalize(vmin=min_z, vmax=max_z)
        cb = mpl.colorbar.ColorbarBase(colorbar, ticks=ticks, cmap=cmap, norm=norm, orientation='horizontal')
        #cb.set_label('Color Scale')


    def scatterPlotScatter2(self, subplot, x, y, xLab, yLab, title, tooltip):
        spectrum = SpectrumDisplay()
        spectrum.setLabels(xLab,yLab)
        spectrum.display(subplot,title,self.paragraph(tooltip),x,y,None, autolimits = True) 

        #scatterdisp = ScatterDisplay()
        #scatterdisp.setLabels(xLab, yLab)
        #xMin=0.9*min(x)
        #xMax=1.1*max(x)
        #yMin=0.9*min(y)
        #yMax=1.1*max(y)
        
        #scatterdisp.setLimits(xMin, xMax, yMin, yMax)
        #subplot.autoscale_view(tight=None, scalex=True, scaley=True)
        ##scatterdisp.display(self.subplot_scatter, title,tooltip, x, y)
        #scatterdisp.display(subplot, title,tooltip, x, y)
        ##subplot.ticklabel_format(style='sci',scilimits=(0,3))


    def scatterPlotScatter3(self, subplot, x, y, x_col, y_col, title, tooltip):
        scatterdisp = ScatterDisplay()
        scatterdisp.setLabels(x_col, y_col)
        scatterdisp.display(subplot, title,tooltip, x, y)
        #subplot.scatter(x, y, s=0.4, color='blue')
        #subplot.ticklabel_format(style='sci',scilimits=(0,3))

        
    #1D spectrum table over-plots
    def oplotTable(self,tab,Xcol,Ycol,Xlab,Ylab,subplot,color):

        tab.readTableXYColumns(fits_extension=1,xcolname=Xcol, ycolname=Ycol)
        spectrum = SpectrumDisplay()
        spectrum.overplot(subplot,tab.x_column,tab.y_column,color)


    def scatterPlotScatter(self, subplot, x, y):
        subplot.scatter(x, y, s=0.4, color='blue')
        #subplot.ticklabel_format(style='sci',scilimits=(0,3))

    def scatterPlotLabels(self, subplot, x_label, y_label):
        subplot.set_xlabel(x_label, fontsize=10)
        subplot.set_ylabel(y_label, fontsize=10)

    def finalSetup(self, subplot, title, tooltip):
        subplot.grid(True)
        subplot.set_title(title, fontsize=10)
        subplot.tooltip = self.paragraph(tooltip)


    def scatterPlot(self, subplot, x, y, x_label, y_label, title, tooltip):

        self.scatterPlotScatter(subplot, x, y)
        self.scatterPlotLabels(subplot, x_label, y_label)
        self.scatterPlotLimits(subplot, x, y)
        self.finalSetup(subplot, title, self.paragraph(tooltip))

    def scatterPlotLimits(self, subplot, x, y):
        subplot.set_xlim(numpy.nanmin(x)-1, numpy.nanmax(x) + 1.)
        subplot.set_ylim(1.05 * numpy.nanmin(y), 1.05 * numpy.nanmax(y))



    #1D table scatter plots
    def plotTableScatter(self,tab,Xcol,Ycol,Xlab,Ylab,subplot,title,tooltip,ext_no=None):
        extno=1
        if ext_no != None :
           extno=ext_no

        tab.readTableXYColumns(fits_extension=extno,xcolname=Xcol,ycolname=Ycol)
        self.scatterPlotScatter(subplot, tab.x_column, tab.y_column)
        self.scatterPlotLabels(subplot, Xlab, Ylab) 
        self.scatterPlotLimits(subplot, tab.x_column, tab.y_column)
        self.finalSetup(subplot, title, self.paragraph(tooltip))
        self.scatterPlot(subplot, tab.x_column, tab.y_column, Xlab, Ylab, title, tooltip)

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise
