# import the needed modules
try:
#  import sys
  import numpy
  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits

#  import wx
  import matplotlib
  import reflex
  import sinfo_plot_common 
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  from pipeline_product import PipelineProduct
  import sinfo_plot_widgets
  import reflex_plot_widgets
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  plotSpectrum()    to control 1D image product layout
#  plotImage()       to control 2D image product layout
#  plotTable()       to control 1D table product layout
#  oplotTable()      to control 1D table product overplots
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:
    def paragraph(self,text, width=None):
       """ wrap text string into paragraph
           text:  text to format, removes leading space and newlines
           width: if not None, wraps text, not recommended for tooltips as
                  they are wrapped by wxWidgets by default
       """

       import textwrap
       if width is None:
           return textwrap.dedent(text).replace('\n', ' ').strip()
       else:
           return textwrap.fill(textwrap.dedent(text), width=width) 

    #2D image display
    def plotImage(self,obj,subplot,title,tooltip):

          obj.readImage(fits_extension=0)
          obj.read2DLinearWCS(fits_extension=0)
          img_obj = ImageDisplay()

          img_obj.setLabels('[arcsec]','[arcsec]')
          y_size, x_size = obj.image.shape
          img_obj.setXLinearWCSAxis(obj.crval1,obj.cdelt1,obj.crpix1)
          img_obj.setYLinearWCSAxis(obj.crval2,obj.cdelt2,obj.crpix2)

          #img_obj.setLimits((1,x_size),(0,y_size))
          subplot.set_title(title, fontsize=7)
          img_obj.display(subplot, title, self.paragraph(tooltip),obj.image)





    #3D cube slices display
    def plotCubeSlice(self,obj,slice,subplot,title,tooltip):
          print 'slice=',slice
          obj.readImage(fits_extension=0)
          obj.read2DLinearWCS(fits_extension=0)
          img_obj = ImageDisplay()
          image = obj.image[slice,:,:]
          img_obj.setLabels('[arcsec]','[arcsec]')
          #img_obj.setXLinearWCSAxis(0,64,1)
          #img_obj.setYLinearWCSAxis(0,64,1)
          img_obj.display(subplot, title, self.paragraph(tooltip),image)
 
          
    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames
    def readFitsData(self, fitsFiles):
      #Control variable to check if the interesting files where at the input
      prefix = "OBS_"
      self.coadd_obj_found = False
      self.coadd_std_found = False
      self.coadd_psf_found = False
      self.med_coadd_obj_found = False
      self.med_coadd_std_found = False
      self.med_coadd_psf_found = False
      self.object_nodding_stacked_found = False
      self.psf_nodding_stacked_found = False
      self.std_nodding_stacked_found = False
      self.std_star_spectra_found = False
      self.sky_med_found = False
      self.enc_energy_found = False
      self.ao_performance_found = False
      self.rec_subtitle = ""
      self.obs_targ = ""
      self.nima = 1
      self.cube_plane_selector_is_created = 'False'
      self.product_id = 'stacked frame'

      #Read all the products
      frames = dict()
      for frame in fitsFiles:
        if frame == '' :
          continue

        header = pyfits.open(frame.name)
        #Make sure to have only products from the same recipe
        #that used this (common) script 
        if 'ESO PRO REC1 ID' in header[0].header :
           rec_id = header[0].header['ESO PRO REC1 ID']
           if rec_id == self.rec_id :
              category = frame.category
              frames[category] = frame
	      print "frame name:", frame.name
	      if category == "COADD_OBJ" or \
	         category == "COADD_STD" or \
		 category == "COADD_PSF" :
	         self.band = header[0].header['ESO INS FILT1 NAME']
	         self.scale = header[0].header['ESO INS OPTI1 NAME']
	         self.obs_targ = header[0].header['ESO OBS TARG NAME']

      print frames
      prefixes = [k.split('_')[0] for k in frames.keys()]
      if "FMTCHK" in prefixes and self.rec_id == "sinfo_rec_wavecal" :
        prefix = "FMTCHK_"

      #products not used: OBS_OBJ,OBS_STD,OBS_PSF


# For any arm search a list of input frames
      #print "arm=",arm , "prefix=",prefix 
      #print frames
      key = "COADD_OBJ"
      if key in frames :
        self.coadd_obj_found = True
        hdulist = frames["COADD_OBJ"]
        self.coadd_obj = PipelineProduct(hdulist)
        self.getNAXIS3(hdulist)
        self.getWCS(hdulist)
        self.mode = 'SCI'
	#self.obs_targ = header[0].header['ESO OBS TARG NAME']
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found coadded obj cube"      
	
	
      key = "COADD_STD"
      if key in frames :
        self.coadd_obj_found = True
        hdulist = frames["COADD_STD"]
        self.coadd_obj = PipelineProduct(hdulist)
        self.getNAXIS3(hdulist)
        self.getWCS(hdulist)
        self.mode = 'STD'
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found coadded std cube"  
	
      key = "COADD_PSF"
      if key in frames :
        self.coadd_obj_found = True
        hdulist = frames["COADD_PSF"]
        self.coadd_obj = PipelineProduct(hdulist)
        self.getNAXIS3(hdulist)
        self.mode = 'PSF'
        self.getWCS(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found coadded psf cube"     
 
 
      key = "MED_COADD_OBJ"
      if key in frames :
        self.med_coadd_obj_found = True
        hdulist = frames["MED_COADD_OBJ"]
        self.med_coadd_obj = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found med coadded obj cube"   
	
	
	
      key = "MED_COADD_STD"
      if key in frames :
        self.med_coadd_obj_found = True
        hdulist = frames["MED_COADD_STD"]
        self.med_coadd_obj = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found med coadded obj cube"  
	
	
      key = "MED_COADD_PSF"
      if key in frames :
        self.med_coadd_obj_found = True
        hdulist = frames["MED_COADD_PSF"]
        self.med_coadd_obj = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found med coadded psf cube"   	
	 	
	   

      key = "OBJECT_NODDING_STACKED"
      if key in frames :
        self.object_nodding_stacked_found = True
        hdulist = frames["OBJECT_NODDING_STACKED"]
        self.object_stacked = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found med coadded obj cube"  
	
      key = "STD_NODDING_STACKED"
      if key in frames :
        self.object_nodding_stacked_found = True
        hdulist = frames["STD_NODDING_STACKED"]
        self.object_stacked = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found med coadded std cube"   	    
      
      
      key = "PSF_CALIBRATOR_STACKED"
      if key in frames :
        self.object_nodding_stacked_found = True
        hdulist = frames["PSF_CALIBRATOR_STACKED"]
        self.object_stacked = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found med coadded psf cube"         
      
      
      key = "STD_STAR_SPECTRA"
      if key in frames :
        self.std_star_spectra_found = True
        hdulist = frames["STD_STAR_SPECTRA"]
        self.std_star_spectra = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found std star spectra"

      key = "SKY_MED"
      if key in frames :
        self.sky_med_found = True
        hdulist = frames["SKY_MED"]
        self.sky_med = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found sky med"
        
      key = "ENC_ENERGY"
      if key in frames :
        self.enc_energy_found = True
        hdulist = frames["ENC_ENERGY"]
        self.enc_energy = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found enc energy params"
        
      key = "AO_PERFORMANCE"
      if key in frames :
        self.ao_performance_found = True
        hdulist = frames["AO_PERFORMANCE"]
        self.ao_performance = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found ao performance"
   

    #Set rec id (to have proper recipe parameters prefix)
    def setRecId(self, rec_id):
      #Recipe ID variable to properly define params
      self.rec_id = rec_id

    #Get NAXIS info (Xshooter and arm specific)
    def getNAXIS3(self, fitsfile):
      hdulist = pyfits.open(fitsfile.name)
      self.NAXIS3 = hdulist[0].header['NAXIS3']



    def getWCS(self, fitsfile):
      hdulist = pyfits.open(fitsfile.name)
      self.NAXIS1 = hdulist[0].header['NAXIS1']
      self.NAXIS2 = hdulist[0].header['NAXIS2']
      self.NAXIS3 = hdulist[0].header['NAXIS3']

      self.crpix1 = hdulist[0].header['CRPIX1']
      self.crval1 = hdulist[0].header['CRVAL1']
      self.cdelt1 = hdulist[0].header['CDELT1']

      self.crpix2 = hdulist[0].header['CRPIX2']
      self.crval2 = hdulist[0].header['CRVAL2']
      self.cdelt2 = hdulist[0].header['CDELT2']

      self.crpix3 = hdulist[0].header['CRPIX3']
      self.crval3 = hdulist[0].header['CRVAL3']
      self.cdelt3 = hdulist[0].header['CDELT3']
      self.wmax= self.crval3+(self.NAXIS3-1)*self.cdelt3
      
    #Get arm setting
    def getBandId(self, sof):
      #Recipe ID variable to properly define params
      self.filt_id = "K"
      self.scale_id = "0.025"
      nf = 0

      frames = dict()
      files = sof.files
      for f in files:
        frame=f.name
        if frame == '' :
          continue
        else :
           nf += 1
           hdulist = pyfits.open(frame)
           print frame
           rec_id_list = hdulist[0].header['ESO PRO REC1 ID']
           #filt_list = hdulist[0].header['ESO FILT1 NAME']
           #scale_list = hdulist[0].header['ESO OPTI1 NAME']
      if nf != 0 :
         self.rec_id = rec_id_list[0:]
         #self.filt_id = filt_list[0:]
         #self.scale_id = scale_list[0:]

      if self.rec_id == "sinfo_rec_jitter":
         self.rec_subtitle = "Jitter Observation. "

    def plotWidgets(self) :
        widgets = list()
        min=0
        max=100
        title_slider="plane id"
        if self.coadd_obj_found == True :
           max=self.NAXIS3 
           val=numpy.floor(self.NAXIS3/2+0.5)
	   print 'val=',val
           # Cube plane selector
           self.slider =sinfo_plot_widgets.InteractiveSlider2(self.cube_plane_selector,self.setPlaneSelectCallBack,min,max,val,100,1, title_slider)
           widgets.append(self.slider)

        #labels_prod = self.labels_prod
        #self.radiobutton_pro = reflex_plot_widgets.InteractiveRadioButtons(self.product_selector, self.setProductSelectCallBack, labels_prod, 0, title="")
        #widgets.append(self.radiobutton_pro)



        return widgets

    def setProductSelectCallBack(self, product_id) :
        print "Selected product id:", product_id
        self.product_id = product_id

        #self.displayProducts()
        self.plotProductsGraphics()

        print "product_id:", self.product_id
         


    def setPlaneSelectCallBack(self, plane_id) :
        self.plane_id = int(plane_id)
	plane_id=self.plane_id
        self.subplot_cube.cla()
	print 'plane_id',plane_id
        self.setDisplayCubeSlice(plane_id)

    def setDisplayCubeSlice(self, plane_id) :
        self.wcen= self.crval3+(plane_id-1)*self.cdelt3
	plane = int(plane_id)
        print "crval3=",self.crval3, "cdelt3=",self.cdelt3, "wcen=",self.wcen
        self.title_cube_obj   = r'plane id: %#4.4d $\lambda$: %#6.5g [$\mu$m]'% (plane,self.wcen) + "\n" + self.mode + " band:" + self.band + " " + self.scale + "[mas] target: " +self.obs_targ 

        print plane_id
        self.plotCubeSlice(self.coadd_obj,plane_id,self.subplot_cube,self.title_cube_obj,self.tooltip_cube_obj)



    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      nrows=self.nima
      row=1;
      #print "nrows=",nrows,row
      #print "resid_tab_found",self.resid_tab_found, "frameON_found",self.frameON_found
      #self.figsize(80,100)
      print "self.coadd_obj_found", self.coadd_obj_found
      if self.coadd_obj_found == True :
        self.subplot_cube  = figure.add_subplot(2,2,1)
        self.subplot_med_coadd  = figure.add_subplot(2,2,2)
        if self.std_star_spectra_found == True :
           self.subplot_obj_spect  = figure.add_subplot(2,2,3)
        self.subplot_obj_stack  = figure.add_subplot(2,2,4)
        #self.subplot_resid_x_tab  = figure.add_subplot(2,2,3)
        row +=1

        self.cube_plane_selector_is_created = 'True'
        self.cube_plane_selector = figure.add_axes([0.10, 0.49, 0.80, 0.01])
        #self.cube_plane_pix_range = figure.add_axes([0.1, 0.875, 0.83, 0.01])
        #self.cube_plane_wav_range = figure.add_axes([0.1, 0.885, 0.83, 0.01])
        # Set the colormap and norm to correspond to the data for which
        # the colorbar will be used.

        cmap = matplotlib.cm.afmhot
        norm1 = matplotlib.colors.Normalize(vmin=0, vmax=self.NAXIS3)
        norm2 = matplotlib.colors.Normalize(vmin=self.crval3, vmax=self.wmax)

        #add colorbar
        #cb1 = matplotlib.colorbar.ColorbarBase(self.cube_plane_pix_range, cmap=cmap,norm=norm1,orientation='horizontal')
        #cb2 = matplotlib.colorbar.ColorbarBase(self.cube_plane_wav_range, cmap=cmap,norm=norm2,orientation='horizontal')
				   
      #self.product_selector = figure.add_axes([0.1, 0.17, 0.30, 0.13])

      if self.med_coadd_obj_found == True :
        print "med coadd obj found"
        #self.subplot_med_cube  = figure.add_subplot(2,2,2)

      if self.object_nodding_stacked_found == True :
        print "object nodding stacked found"
        #self.subplot_object_stacked  = figure.add_subplot(2,2,3)

      if self.coadd_obj_found == True :
        print "med coadd obj found"
        #self.subplot_ima_diff  = figure.add_subplot(2,2,4)
        
      elif self.enc_energy_found == True :
        print ''
        
      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)
        #print "found no spectrum data"
      #print "nrows=",nrows,rowself.rec_subtitl

          
    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
      #print "arc check:",  self.resid_tab_found, self.frameON_found
      #print "mfat check:",  self.edges_tab_found, self.mflat_found
      if self.coadd_obj_found == True : 
        #and self.lineguess_found == True 
        title_pref = 'Linear-extracted and Merged Spectrum.'	      

        tooltip_spectrum ="""\
        Plot of the linear-extracted and merged spectrum of the object 
        (blue line) as total flux (ADU) versus wavelength (nm). 
        The  +-1 sigma uncertainties are plotted as upper and lower
        red curves. Note that this spectrum is not flux calibrated.
        """
      print "coadd_obj_found=",self.coadd_obj_found
      if self.coadd_obj_found == True :


        print "prepare cube display"
        plane_id=int(numpy.floor(self.NAXIS3/2+0.5))
        #self.wcen= self.crval3+(plane_id-1)*self.cdelt3

        self.tooltip_cube_obj ="""\
                            The displayed image corresponds to the
        plane id indicated by the slider below. To view a different
        plane of the science reconstructed cube, the user should select
        the desired plane id with the mouse left button, using the
        slider shown below the image.  
                                      """
        title_traces_ifu_slices   = 'IFU slices traces' 
        tooltip_traces_ifu_slices ="""\
                          Traces of the IFU object central position
        observed by slicer number 1,2,3 are displayed respectively in
        colors blue, green, red. If the cube has been perfectly 
        reconstructed the three traces should overlap.
                                      """
    
        self.setDisplayCubeSlice(plane_id)
        print "display plane: ", plane_id
        if self.std_star_spectra_found == True :
	   self.dpm = sinfo_plot_common.DataPlotterManager()
	   self.xLab = 'Wavelength [nm]'
	   self.yLab = 'Flux [ADU]'
	   self.error_id = 'no'
	   self.decode_bp=2147483647
	   self.x='wavelength'
           self.y='counts_bkg'
           self.e='counts_bkg'
           self.q=None
	
	   self.dpm.plotTableErrQual(self.std_star_spectra,self.x,self.y,self.e,self.q,self.decode_bp,self.error_id,self.xLab,self.yLab,self.subplot_obj_spect,title_traces_ifu_slices,tooltip_traces_ifu_slices)
           #self.dpm.plotTableScatter(self.std_star_spectra,'wavelength','counts_bkg', 'Wavelength [nm]','Flux [ADU]',self.subplot_obj_spect,title_traces_ifu_slices,tooltip_traces_ifu_slices)
           #self.oplotTableScatter(self.traces_ifu_slices,'WAVELENGTH','POS_2', self.subplot_traces_ifu_slices,color='green')
           #self.oplotTableScatter(self.traces_ifu_slices,'WAVELENGTH','POS_3', self.subplot_traces_ifu_slices,color='red')

      print self.med_coadd_obj_found
      print self.med_coadd_std_found
      print self.med_coadd_psf_found
      print 'pippo'
      if self.med_coadd_obj_found == True :

        print "prepare image display"
        title   = 'collapsed median'

        tooltip ="""\
                            The displayed image corresponds to the
        plane id indicated by the slider below. To view a different
        plane of the science reconstructed cube, the user should select
        the desired plane id with the mouse left button, using the
        slider shown below the image.  
                                      """
        self.plotImage(self.med_coadd_obj,self.subplot_med_coadd,title,tooltip)

      if self.object_nodding_stacked_found == True and self.product_id == "stacked frame" :
        title   = 'stacked frame'

        tooltip ="""\
                            The displayed image corresponds to the
        plane id indicated by the slider below. To view a different
        plane of the science reconstructed cube, the user should select
        the desired plane id with the mouse left button, using the
        slider shown below the image.  
                                      """
        self.plotImage(self.object_stacked,self.subplot_obj_stack,title,tooltip)
  
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setGeneralParameters(self,paramList,rec_id):
      group="general"

      help1="""Overwrite DRS ini parameters:. [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="gen-overpar",group=group,description=help1))
      
      help2="""Correct for bad lines introduced by instrument software:. [FALSE]."""
      paramList.append(RecipeParameter(rec_id,displayName="lc_sw",group=group,description=help2))
      
      help3="""Kappa sigma value. [18]."""
      paramList.append(RecipeParameter(rec_id,displayName="lc_kappa",group=group,description=help3))
      
      help4="""Filtering radii applied during median filter. Should be small. [3]."""
      paramList.append(RecipeParameter(rec_id,displayName="lc_filt_rad",group=group,description=help4))

      help5="""Density of pipeline products: 0 (low), 1 (low+skycor), 2 (med-QC), 3 (high-debug+skycor). [2]."""
      paramList.append(RecipeParameter(rec_id,displayName="product-density",group=group,description=help5))
      
    def setStackParameters(self,paramList,rec_id):
      group="stack"

      help2="""lower rejection. [0.1]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-hi_rej",group=group,description=help2))

      help3="""higher rejection. [0.1]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-hi_rej",group=group,description=help3))

      help4="""Flat Index:. [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-flat_ind",group=group,description=help4))

      help5="""Normalize master flat to its smoothed value (to remove lamp response curve). 0 (no smooth). 1 (apply fft filter along y).2
                               (apply running median filter along y). [0]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mflat_norm_smooth",group=group,description=help5))

      help6=""" Normalization smoothing radii. [16]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mflat_smooth_rad",group=group,description=help6))
      
      help7="""BP Mask Interpolation Switch: indicates if the bad pixel mask should be applied (1) or not (0). [1]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mask_ind",group=group,description=help7))
      
      help8="""indicates if the bad pixels should be indicated (yes) or interpolated (no). [FALSE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-ind_ind",group=group,description=help8))
      
      help9="""Max distance bad-good pix:. [4]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mask_rad",group=group,description=help9))
      
      help10="""Gaussian Convolution Switch:. [FALSE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-gauss_ind",group=group,description=help10))
      
      help11="""Kernel Half Width. [2]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-khw",group=group,description=help11))
      
      help12="""Warp Fix Index:. [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-warpfix_ind",group=group,description=help12))
      
      help13="""Warpfix kernel:. <tanh | sinc | sinc2 | lanczos | hamming | hann> [tanh]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-warpfix_kernel",group=group,description=help13))
      
      help14="""qc_thresh_min. [0]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-qc_thresh_min",group=group,description=help14))
     
      help15="""qc_thresh_max. [49000]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-qx_thresh_max",group=group,description=help15))
     
      help16="""indicates if the raw sky frame should be subtracted (TRUE) or (FALSE). [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-sub_raw_sky",group=group,description=help16))
      
    def setObjnodParameters1(self,paramList,rec_id):
      group="objnod1"
      
      help1="""Method to reduce autojitter template frames Raw frames are object only exposures. object-fake_sky pairs are generated. 0: no sky
                          for all objects. [1]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-aj_method",group=group,description=help1))
      
      help2="""Spatial median (sky) subtraction from cube: (If autojitter_method==1),indicates if the spatial median of each plane should be
                          subtracted (TRUE) or not (FALSE) from each cube plane. [FALSE]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-ks_clip",group=group,description=help2))
      
      help3="""kappa value for kappa-sigma clipping of coadded cube. [2.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-kappa",group=group,description=help3))
      
      help4="""Cube x size: x-pixel size of the final combined data cube,must lie between 64 and 128. If 0 it is computed automatically. [0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-size_x",group=group,description=help4))
      
      help5="""Cube y size: y-pixel size of the final combined data cube,must lie between 64 and 128.If 0 it is computed automatically. [0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-size_y",group=group,description=help5))
      
      help6="""number of coefficients for the polynomial interpolation. [3]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-no_coeffs",group=group,description=help6))
      
      help7="""Nord South Index Switch: indicates if the slitlet distances are determined by a north-south-test (TRUE) or slitlet edge fits
                          (FALSE). [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-ns_ind",group=group,description=help7))
      
      help8="""Fine Tuning Method: indicator for the shifting method to use (P: polynomial interpolation,  S: cubic spline interpolation). <P |
                          S> [P]"""   
      paramList.append(RecipeParameter(rec_id,displayName="objnod-fine_tune_mtd",group=group,description=help8))
      
      help9="""Fine Tuning polynomial order: order of the polynomial if the polynomial interpolation shifting method is used. [2]"""
      paramList.append(RecipeParameter(rec_id,displayName="oobjnod-order",group=group,description=help9))
    
      help10="""lower rejection: percentage of rejected low value pixels for averaging the sky spectra. [10.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-lo_rej",group=group,description=help10))

    def setObjnodParameters2(self,paramList,rec_id):
      group="objnod2"


      help11="""higher rejection: percentage of rejected high value pixels for averaging the sky spectra. [10.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-hi_rej",group=group,description=help11))
      
      help12="""Tolerance: pixel tolerance, this distance tolerance to the diagonal dividing line is not considered for the sky extraction to be
                          sure to have a clean sky due to positioning tolerance and crossing through pixels. [2]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-tol",group=group,description=help12))               
                          
      help13="""Jitter Index: jitter mode indicator: TRUE: Auto-Jitter, FALSE: user defined jitterThe size_x size_y kernel_type parameters are
                          only used if jitterInd is set to yes, that means in auto-jittering mode!. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-jit_ind",group=group,description=help13))                
 
      help14=""" Kernel Type:the name of the interpolation kernel to shift the single cubes  to the correct places inside the big combined cube.
                          <NULL | default | tanh | sinc2 | lanczos | hamming | hann> [tanh]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-kernel_typ",group=group,description=help14))          
                          
      help15="""Vignetting on llx: pixels vignetted from lower left corner X coordinate of contributing cubes before coaddition. [0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-vllx",group=group,description=help15))
      
      help16="""Vignetting on llx: pixels vignetted from lower left corner Y coordinate of contributing cubes before coaddition. [0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-vlly",group=group,description=help16))
      
      help17="""Vignetting on urx: pixels vignetted from upper right corner X coordinate of contributing cubes before coaddition. [0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-vllx",group=group,description=help17))
      
      help18="""Vignetting on ury: pixels vignetted from upper right corner Y coordinate of contributing cubes before coaddition. [0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-vlly",group=group,description=help18))
      
      help19="""First column offset:. [0.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-fcol",group=group,description=help19))
      
      help20="""Sky residuals correction:. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-sky_cor",group=group,description=help20))
      
      help21="""Apply flux correction after rebinning:. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-flux_cor",group=group,description=help21))
      
      help22="""Maximum allowed size for cubes mosaic is 100*mosaic_max_size. [14196]"""
      paramList.append(RecipeParameter(rec_id,displayName="objnod-mosaic_max_size",group=group,description=help22))
      
    def setSkycorrParameters(self,paramList,rec_id):
      group="skycor"
     
      help1="""Starting wavelength for object-sky cross correlation. [1.4]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-mask_ws",group=group,description=help1))
      
      help2="""End wavelength for object-sky cross correlation. [2.5]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-mask_we",group=group,description=help2))
      
      help3="""Threshold value for fraction of spatial pixels to be sky. [0.8]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-min-frac",group=group,description=help3))
      
      help4="""Width of sky-thermal background pre filter (to remove emission lines before fitting a Black Body). [12]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-sky_bkg_filter_width",group=group,description=help4))
      
      help5="""Threshold value for full width in pixels of unresolved emission line. Lines with FWHM smaller than this value are not considered
                          in the object-sky cross correlation and in computation of the optimal sky lines scaling factor. [4.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-line_hw",group=group,description=help5))     
      
      help6="""Optimal sky lines scaling factor computation method: amoeba fit (0), maximum likelihood (1). [1]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-line_hw",group=group,description=help6)) 
 
      help7="""Optimal sky lines scaling factor computation method: amoeba fit (0), maximum likelihood (1). [1]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-scale_method",group=group,description=help7)) 
      
      help8="""Computes scaling factor correction due to rotational levels transitions. [FALSE]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-rot_cor",group=group,description=help8))
      
      help9="""Do Gaussian fit of object noise. [FALSE]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-fit_obj_noise",group=group,description=help9))
      
      help10="""Number of iterations of background fit. [10]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-niter",group=group,description=help10))
  
      help11="""Sky spectrum shift towar object. [0.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-pshift",group=group,description=help11))
      
      help12="""Lower left X defining object spectrum location. [1]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-llx",group=group,description=help12))
      
      help13="""Lower left Y defining object spectrum location. [1]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-lly",group=group,description=help13))
      
      help14="""Upper left X defining object spectrum location. [64]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-urx",group=group,description=help14))
      
      help15="""Upper left Y defining object spectrum location. [64]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-ury",group=group,description=help15))
      
      help16="""Subtract thermal background contribute from object spectra. Set it to TRUE if stack-sub_raw_sky is set to FALSE. [FALSE]"""
      paramList.append(RecipeParameter(rec_id,displayName="skycor-sub_thr_bkg_from_obj",group=group,description=help16))

    def setStdStarParameters(self,paramList,rec_id):
      group="stdstar"
      
      help1="""Switch to activate spectrum extraction. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="std_star-switch",group=group,description=help1))
                 
      help2="""lower rejection. [0.1]"""
      paramList.append(RecipeParameter(rec_id,displayName="std_star-lo_rej",group=group,description=help2))
                               
      help3="""high rejection. [0.1]"""
      paramList.append(RecipeParameter(rec_id,displayName="std_star-hi_rej",group=group,description=help3))
      
      help4="""Factor to find 2D-Gauss FWHM. The extraction box is: halfbox_x=halfbox_y=fwhm_factor*(fwhm_x+fwhm_y)*0.5. [5.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="std_star-fwhm_fct",group=group,description=help4))
      
      help5="""Intensity Conversion Index:. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="std_star-conv_ind",group=group,description=help5))
    
      help6="""Compute efficiency: TRUE/FALSE. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="std_star-compute_eff",group=group,description=help6))
      
      
    def setPsfParameters(self,paramList,rec_id):
      group="psf"
      
      help1="""Switch to activate PSF-related (Strehl/Encircled energy) computation. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="psf-switch",group=group,description=help1))
      


    def setInteractiveParameters(self):
      paramList = list()
      rec_id=self.rec_id
      self.setGeneralParameters(paramList,rec_id)
    
      if (rec_id == "sinfo_rec_wavecal" or rec_id == "sinfo_rec_jitter") :
         self.setStackParameters(paramList,rec_id)
         
      if rec_id == "sinfo_rec_wavecal" :
         self.setWavecalParameters(paramList,rec_id)

      if rec_id == "sinfo_rec_jitter" :
         self.setObjnodParameters1(paramList,rec_id)
         self.setObjnodParameters2(paramList,rec_id)
         self.setSkycorrParameters(paramList,rec_id)
         self.setStdStarParameters(paramList,rec_id)
         self.setPsfParameters(paramList,rec_id)
 
      if rec_id[0:9] != "sinfo_rec" :
         print "recipe" ,rec_id[0:9],  "not supported"

      return paramList

    def setWindowHelp(self):
      help_text = """
This is an interactive window which help asses the quality of the execution of a recipe.
"""
      return help_text

    def setWindowTitle(self):
      #title = 'X-shooter Interactive ' + self.rec_subtitle + self.seq_arm + ' Arm. ' 
      title = 'SINFONI Jitter Interactive GUI'
      return title

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise

