# import the needed modules
try:
#  import sys
  import numpy
  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits


#  import wx
#  import matplotlib
  import reflex
  #from xsh_plot_common import *
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  from pipeline_product import PipelineProduct
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  plotSpectrum()    to control 1D image product layout
#  plotImage()       to control 2D image product layout
#  plotTable()       to control 1D table product layout
#  oplotTable()      to control 1D table product overplots
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:
    def paragraph(self,text, width=None):
       """ wrap text string into paragraph
           text:  text to format, removes leading space and newlines
           width: if not None, wraps text, not recommended for tooltips as
                  they are wrapped by wxWidgets by default
       """

       import textwrap
       if width is None:
           return textwrap.dedent(text).replace('\n', ' ').strip()
       else:
           return textwrap.fill(textwrap.dedent(text), width=width) 

    #2D image display
    def plotImage(self,obj,subplot,title,tooltip):

          obj.readImage(fits_extension=0)
          obj.read2DLinearWCS(fits_extension=0)
          img_obj = ImageDisplay()


          img_obj.setLabels('Wavelength [nm]','Position Along Slit [arcsec]')
          y_size, x_size = obj.image.shape
          print obj.image.shape
          img_obj.setXLinearWCSAxis(obj.crval1,obj.cdelt1,obj.crpix1)
          img_obj.setYLinearWCSAxis(obj.crval2,obj.cdelt2,obj.crpix2)

          #img_obj.setLimits((1,x_size),(0,y_size))
          img_obj.display(subplot, title, self.paragraph(tooltip),obj.image)

    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames
    def readFitsData(self, fitsFiles):
      #Control variable to check if the interesting files where at the input
      prefix = "CAL_"
      self.wave_map_found = False
      self.wave_lamp_stacked_found = False
      self.wave_coef_slit_found = False
      self.wave_fit_params_found = False
      self.slit_pos_found = False
      self.resampled_wave_found = False
      self.rec_subtitle = ""
      self.nima = 1
      #Read all the products
      frames = dict()
      for frame in fitsFiles:
        if frame == '' :
          continue

        header = pyfits.open(frame.name)
        #Make sure to have only products from the same recipe
        #that used this (common) script 
        if 'ESO PRO REC1 ID' in header[0].header :
           rec_id = header[0].header['ESO PRO REC1 ID']
           if rec_id == self.rec_id :
              category = frame.category
              frames[category] = frame

      print frames
      prefixes = [k.split('_')[0] for k in frames.keys()]
      if "FMTCHK" in prefixes and self.rec_id == "sinfo_rec_wavecal" :
        prefix = "FMTCHK_"

# For any arm search a list of input frames
      #print "arm=",arm , "prefix=",prefix 
      #print frames
      key = "WAVE_LAMP_STACKED"
      if key in frames :
        self.wave_lamp_stacked_found = True
        hdulist = frames["WAVE_LAMP_STACKED"]
        self.wave_lamp_stacked = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found wave lamp stacked"      
      
      
      key = "WAVE_MAP"
      if key in frames :
        self.wave_map_found = True
        hdulist = frames["WAVE_MAP"]
        self.wave_map = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found wave map"

      key = "WAVE_COEF_SLIT"
      if key in frames :
        self.wave_coef_slit_found = True
        hdulist = frames["WAVE_COEF_SLIT"]
        self.wave_coef_slit = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found wave coef slit"
        
      key = "WAVE_FIT_PARAMS"
      if key in frames :
        self.wave_fit_params_found = True
        hdulist = frames["WAVE_FIT_PARAMS"]
        self.wave_fit_params = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found wave fit params"
        
      key = "SLIT_POS"
      if key in frames :
        self.slit_pos_found = True
        hdulist = frames["SLIT_POS"]
        self.slit_pos = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found slit pos"
        
      key = "RESAMPLED_WAVE"
      if key in frames :
        self.resampled_wave_found = True
        hdulist = frames["RESAMPLED_WAVE"]
        self.resampled_wave = PipelineProduct(hdulist)
        #self.resid_tab_2plot = PlotableResidLineTable(hdulist,1)
        #self.nima += 1
        print "found resampled wave"


    #Set rec id (to have proper recipe parameters prefix)
    def setRecId(self, rec_id):
      #Recipe ID variable to properly define params
      self.rec_id = rec_id

    #Get arm setting
    def getBandId(self, sof):
      #Recipe ID variable to properly define params
      self.filt_id = "K"
      self.scale_id = "0.025"
      nf = 0

      frames = dict()
      files = sof.files
      for f in files:
        frame=f.name
        if frame == '' :
          continue
        else :
           nf += 1
           hdulist = pyfits.open(frame)
           print frame
           rec_id_list = hdulist[0].header['ESO PRO REC1 ID']
           #filt_list = hdulist[0].header['ESO FILT1 NAME']
           #scale_list = hdulist[0].header['ESO OPTI1 NAME']
      if nf != 0 :
         self.rec_id = rec_id_list[0:]
         #self.filt_id = filt_list[0:]
         #self.scale_id = scale_list[0:]

      if self.rec_id == "sinfo_rec_wavecal":
         self.rec_subtitle = "Wavelength Calibration. "

    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      nrows=self.nima
      row=1;
      #print "nrows=",nrows,row
      #print "resid_tab_found",self.resid_tab_found, "frameON_found",self.frameON_found
      if self.wave_coef_slit_found == True :
        self.subplot_wave_pos  = figure.add_subplot(4,1,1)
        self.subplot_wave_pos  = figure.add_subplot(4,1,2)
        self.subplot_wave_pos  = figure.add_subplot(4,1,3)
        #self.subplot_resid_x_tab  = figure.add_subplot(2,2,3)
        row +=1

      if self.resampled_wave_found == True :
        #self.subplot_mflat  = figure.add_subplot(nrows,1,row)
        self.subplot_image_resampled_wave  = figure.add_subplot(4,1,4)
        row +=1
        print "crea resampled wave image display",self.resampled_wave_found

      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)
        #print "found no spectrum data"
      #print "nrows=",nrows,rowself.rec_subtitl

          
    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
  

      #display 2D image of RESAMPLED_WAVE
      if self.resampled_wave_found  == True:
         print "pippo"
         title_resampled_wave= "Resampled Wave"
         tooltip_resampled_wave="tooltip resampled wave"
         self.plotImage(self.resampled_wave,self.subplot_image_resampled_wave,title_resampled_wave,tooltip_resampled_wave)
           
       
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setGeneralParameters(self,paramList,rec_id):
      group="general"

      help1="""Overwrite DRS ini parameters:. [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="gen-overpar",group=group,description=help1))
      
      help2="""Correct for bad lines introduced by instrument software:. [FALSE]."""
      paramList.append(RecipeParameter(rec_id,displayName="lc_sw",group=group,description=help2))
      
      help3="""Kappa sigma value. [18]."""
      paramList.append(RecipeParameter(rec_id,displayName="lc_kappa",group=group,description=help3))
      
      help4="""Filtering radii applied during median filter. Should be small. [3]."""
      paramList.append(RecipeParameter(rec_id,displayName="lc_filt_rad",group=group,description=help4))

      help5="""Density of pipeline products: 0 (low), 1 (low+skycor), 2 (med-QC), 3 (high-debug+skycor). [2]."""
      paramList.append(RecipeParameter(rec_id,displayName="product-density",group=group,description=help5))
      
    def setStackParameters(self,paramList,rec_id):
      group="stack"

      help2="""lower rejection. [0.1]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-hi_rej",group=group,description=help2))

      help3="""higher rejection. [0.1]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-hi_rej",group=group,description=help3))

      help4="""Flat Index:. [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-flat_ind",group=group,description=help4))

      help5="""Normalize master flat to its smoothed value (to remove lamp response curve). 0 (no smooth). 1 (apply fft filter along y).2
                               (apply running median filter along y). [0]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mflat_norm_smooth",group=group,description=help5))

      help6=""" Normalization smoothing radii. [16]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mflat_smooth_rad",group=group,description=help6))
      
      help7="""BP Mask Interpolation Switch: indicates if the bad pixel mask should be applied (1) or not (0). [1]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mask_ind",group=group,description=help7))
      
      help8="""indicates if the bad pixels should be indicated (yes) or interpolated (no). [FALSE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-ind_ind",group=group,description=help8))
      
      help9="""Max distance bad-good pix:. [4]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-mask_rad",group=group,description=help9))
      
      help10="""Gaussian Convolution Switch:. [FALSE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-gauss_ind",group=group,description=help10))
      
      help11="""Kernel Half Width. [2]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-khw",group=group,description=help11))
      
      help12="""Warp Fix Index:. [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-warpfix_ind",group=group,description=help12))
      
      help13="""Warpfix kernel:. <tanh | sinc | sinc2 | lanczos | hamming | hann> [tanh]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-warpfix_kernel",group=group,description=help13))
      
      help14="""qc_thresh_min. [0]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-qc_thresh_min",group=group,description=help14))
     
      help15="""qc_thresh_max. [49000]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-qx_thresh_max",group=group,description=help15))
     
      help16="""indicates if the raw sky frame should be subtracted (TRUE) or (FALSE). [TRUE]."""
      paramList.append(RecipeParameter(rec_id,displayName="stack-sub_raw_sky",group=group,description=help16))
      
    def setWavecalParameters1(self,paramList,rec_id):
      group="wavecal1"
      
      help1="""witch to get a new slitpos without a reference:. [FALSE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-slitpos_bootstrap",group=group,description=help1))
      
      help2="""Calib Indicator: FALSE: if the dispersion relation is already known, the routine can jump to the sinfo_waveMap section TRUE: if
                          the dispersion relation must first be determined. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-calib_indicator",group=group,description=help2))
      
      help3="""Minimum Of Difference: minimum difference of mean and sinfo_median column intensity to carry out the cross sinfo_correlation.
                          [1.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-min_diff",group=group,description=help3))
      
      help4="""Half Width: half width of a box within which the line must be placed. [7]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-hw",group=group,description=help4))
      
      help5="""Sigma: sigma of Gaussian which is convolved with the artificial spectrum generated using the line list. [2.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-sigma",group=group,description=help5))
      
      help6="""FWHM: initial guess value for the fwhm of the Gaussian used for the line fit. [2.83]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-fwhmw",group=group,description=help6))
      
      help7="""Minimum Of Amplitude: of the Gaussian to do the fit. [5.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-min_amplitude",group=group,description=help7))
      
      help8="""Maximum Residuals value: beyond this value the fit is rejected. [0.5]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-max_residual",group=group,description=help8))
      
      
      help9="""Number of A coefficients: number of polynomial coefficients for the dispersion relation. [4]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-n_a_coeffs",group=group,description=help9))
  
      help10="""Number of B coefficients: number of polynomial coefficients for the polynomial fit of the dispersion coefficients. [2]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-n_b_coeffs",group=group,description=help10))
  
    def setWavecalParameters2(self,paramList,rec_id):
      group="wavecal2"
      
      help11="""Sigma Factor: Factor of the standard deviation of the polynomial coefficients of the dispersion relation beyond which the
                          coefficients are not used for the fit. [1.5]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-sigma_factor",group=group,description=help11))
      
      help12="""Write Coefficients Index: indicates if the coefficients should be written into a file or not. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-wcoeff_ind",group=group,description=help12))
      
      help13="""Write Parameter Index: indicates if the fit parameters should be written into a file or not. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-par_ind",group=group,description=help13))
      
      help14="""Minimal Slitlets's Distance in spectral direction. [15]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-pixel_dist",group=group,description=help14))
      
      help15="""Pixel Tolerance: allowed pixel position tolerance between estimated and fitted line position. [5.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-pixel_tol",group=group,description=help15))
      
      help16="""Wavelength Map Indicator: indicates if the wavelength calibration map should be generated (TRUE) or not (FALSE). [FALSE] [0.5]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-wave_map_ind",group=group,description=help16))
      
      help17="""Magnificator Factor: magnifying factor for the number of pixels in the columns needed for FFT. [8]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-mag_factor",group=group,description=help17))
      
      help18="""Slit Position Indicator: indicates if the fits of the slitlet edge positions should be carried through or not. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-slit_pos_ind",group=group,description=help18))
      
      help19="""Fit Boltzmann Indicator: indicates if the fits of the slitlet edge positions is carried trough by using a Boltzmann function as
                          model function. [TRUE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-fit_boltz_ind",group=group,description=help19))
            
      help20="""Fit Edge Indicator: indicates if the fits of the slitlet edge positions is carried through by using a simple edge function as
                          model function. [FALSE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-fit_edge_ind",group=group,description=help20))
      
    def setWavecalParameters3(self,paramList,rec_id):
      group="wavecal3"
      help21="""Estimate Indicator: indicates if the fits of the slitlet edge positions is carried through by using a list of estimated guess
                          positions in a file (TRUE)or if the initial positions are calculated automatically (FALSE). The estimation case is more stable.
                          [FALSE]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-estimate_ind",group=group,description=help21))
      
      help22="""Box Length: pixel length of the row box within which the fit is carried out. [32]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-box_len",group=group,description=help22))
      
      help23="""Y Box: half width of a small box in spectral direction within which the maximal intensity pixel is searched. [5.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-y_box",group=group,description=help23))
      
      help24="""Difference Tolearance: maximal tolerable difference of the resulting fit positions of the slitlet edges with respect to the
                          expected positions. [2.0]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-diff_toll",group=group,description=help24))
      
      help25="""qc_thresh_min. [0]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-qc_thresh_min",group=group,description=help25))
      
      help26="""qc_thresh_max. [49000]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-qc_thresh_max",group=group,description=help26))
      
      help27="""number of coefficients for the polynomial interpolation. [3]"""
      paramList.append(RecipeParameter(rec_id,displayName="wcal-no_coeffs",group=group,description=help27))
    
    def setInteractiveParameters(self):
      paramList = list()
      rec_id=self.rec_id
      self.setGeneralParameters(paramList,rec_id)
    
      if rec_id == "sinfo_rec_wavecal" :
         self.setStackParameters(paramList,rec_id)
         self.setWavecalParameters1(paramList,rec_id)
         self.setWavecalParameters2(paramList,rec_id)
         self.setWavecalParameters3(paramList,rec_id)

      else :
         print "recipe" ,rec_id,  "not supported"

      return paramList

    def setWindowHelp(self):
      help_text = """
This is an interactive window which help asses the quality of the execution of a recipe.
"""
      return help_text

    def setWindowTitle(self):
      #title = 'X-shooter Interactive ' + self.rec_subtitle + self.seq_arm + ' Arm. ' 
      title = 'SINFONI Interactive ' + ' Arm. '
      return title

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise

