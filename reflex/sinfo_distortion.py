# import the needed modules
try:
  import reflex
  import_sucess = 'true'

# Then here is python code moved in xsh_object_interactive_common.py

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"


# import the needed modules
try:
#  import sys
  import numpy
  try:
    from astropy.io import fits as pyfits
  except ImportError:
    import pyfits
    
  import sinfo_plot_common
  import sinfo_parameters_common
#  import wx
#  import matplotlib
  from matplotlib.font_manager import fontManager, FontProperties   
  import reflex
  #from xsh_plot_common import *
  from reflex import parseSofJson,RecipeParameter
  from reflex_interactive_app import PipelineInteractiveApp
  import reflex_plot_widgets

  from pipeline_product import PipelineProduct
  from pipeline_display import SpectrumDisplay, ImageDisplay, ScatterDisplay
#  matplotlib.use('WXAgg')
  import_sucess = 'true'
  import warnings
  warnings.simplefilter('ignore',UserWarning)

#NOTE for developers: 
# -If you want to modify the current script to cope
#  with different parameters, this is the function to modify:
#  setInteractiveParameters()
# -If you want to modify the current script to read different data from
#  the input FITS, these are the functions (class DataPlotterManager) to modify:
#  readFitsData()    to indicate what are the FITS input to plot
#  setRecId()        to set rec id (used in parameter prefix)
#  getArmId()        to get arm id FITS header info
#
# -If you want to modify the current script to modify the plots (using the same
#  data),  this is the function to modify:
#  plotProductsGraphics()          (from class DataPlotterManager)
# -If you want to modify the text that appears in the "Help" button,
#  this is the function to modify:
#  setWindowHelp()
# -If you want to modify the title of the window, modify this function:
#  setWindowTitle()


  #This class deals with the specific details of data reading and final plotting.
  class DataPlotterManager:

    # This function will read all the columns, images and whatever is needed
    # from the products. The variables , self.plot_x, self.plot_y, etc...
    # are used later in function plotProductsGraphics().
    # Add/delete these variables as you need (only that plotProductsGraphics()
    # has to use the same names).
    # You can also create some additional variables (like statistics) after
    # reading the files.
    # If you use a control variable (self.xxx_found), you can modify 
    # later on the layout of the plotting window based on the presence of 
    # given input files. 
    # sof contains all the set of frames


    def paragraph(self,text, width=None):
       """ wrap text string into paragraph
           text:  text to format, removes leading space and newlines
           width: if not None, wraps text, not recommended for tooltips as
                  they are wrapped by wxWidgets by default
       """
       import textwrap
       #print 'width=',width,'Text=',text
       if width is None:
           return textwrap.dedent(text).replace('\n', ' ').strip()
       else:
           return textwrap.fill(textwrap.dedent(text), width=width) 



    
    def readFitsData(self, fitsFiles):
      #Control variable to check if the interesting files where at the input
      #print fitsFiles
      
      self.distortion_found = False
      self.nima = 0
      self.binx = 1
      self.ext_ima=0
      self.biny = 1
      self.arm_id=0
      self.qc_id='DARK_MEAN'
      
      self.product_id = 'FIBRE_NS_STACKED_ON'
      #self.labels_prod = list()
      self.labels_prod = ['FIBRE_NS_STACKED_ON','Slitlet distance']
      self.labels_qc=['DARK MEAN','HOTPIX']
      self.labels_arm=['BLUE','RED']
      #Read all the products
      frames = dict()
      for frame in fitsFiles:
        if frame == '' :
          continue

        header = pyfits.open(frame.name)
        #Make sure to have only products from the same recipe
        #that used this (common) script 
        if 'ESO PRO REC1 ID' in header[0].header :
           rec_id = header[0].header['ESO PRO REC1 ID']
           #print "rec_id=", rec_id
           if rec_id == self.rec_id :
              category = frame.category
              frames[category] = frame
              print "frame name:", frame.name, category
 
# For any arm search a list of input frames
      #print frames
      
      key = "DISTORTION"
      if key in frames :
        self.distortion_found = True
        hdulist = frames[key]
        self.distortion = PipelineProduct(hdulist)
        print "found", key

      key = "FIBRE_NS_STACKED_ON"
      if key in frames :
        self.fibre_ns_stacked_on_found = True
        hdulist = frames[key]
        self.fibre_ns_stacked_on = PipelineProduct(hdulist)
        print "found", key


      key = "RFIBRE_NS_STACKED_ON"
      if key in frames :
        self.rfibre_ns_stacked_on_found = True
        hdulist = frames[key]
        self.rfibre_ns_stacked_on = PipelineProduct(hdulist)
        print "found", key


      key = "BP_MAP_DI"
      if key in frames :
        self.bp_map_di_found = True
        hdulist = frames[key]
        self.bp_map_di = PipelineProduct(hdulist)
        print "found", key

      key = "SLITLETS_DISTANCE"
      if key in frames :
        self.slitlets_distance_found = True
        hdulist = frames[key]
        self.slitlets_distance = PipelineProduct(hdulist)
        print "found", key        


      key = "RDISTORTION"
      if key in frames :
        self.rdistortion_found = True
        hdulist = frames[key]
        self.rdistortion = PipelineProduct(hdulist)
        print "found", key

      key = "RBP_MAP_DI"
      if key in frames :
        self.rbp_map_di_found = True
        hdulist = frames[key]
        self.rbp_map_di = PipelineProduct(hdulist)
        print "found", key

      key = "RSLITLETS_DISTANCE"
      if key in frames :
        self.rslitlets_distance_found = True
        hdulist = frames[key]
        self.rslitlets_distance = PipelineProduct(hdulist)
        print "found", key
        opt   = 'Dist at ref pos'
        self.labels_prod.append(opt)

        
    #Set rec id (to have proper recipe parameters prefix)
    def setRecId(self, rec_id):
      #Recipe ID variable to properly define params
      self.rec_id = rec_id

    #Get arm setting
    def getArmId(self, sof):
      #Recipe ID variable to properly define params
      self.rec_id = "sinfo_rec_distortion"
      nf = 0

      frames = dict()
      files = sof.files
      for f in files:
        frame=f.name
        if frame == '' :
          continue
        else :
           nf += 1
           hdulist = pyfits.open(frame)
           rec_id_list = hdulist[0].header['ESO PRO REC1 ID']
      if nf != 0 :
         self.rec_id = rec_id_list[0:]

      #print "self.rec_id", self.rec_id
      if self.rec_id == "sinfo_rec_distortion" :
         self.rec_subtitle = "Distortion Computation. "
             
    # This function creates all the subplots. It is responsible for the plotting 
    # layouts. 
    # There can different layouts, depending on the availability of data
    # Note that subplot(I,J,K) means the Kth plot in a IxJ grid 
    # Note also that the last one is actually a box with text, no graphs.
    def addSubplots(self, figure):
      nrows=self.nima
      row=1;

      if self.distortion_found == True and self.bp_map_di_found == True: 
        self.subplot_common  = figure.add_subplot(1,1,1)
        row +=1

        self.product_selector = figure.add_axes([0.00, 0.85, 0.31, 0.15])

      else : 
        self.subtext_nodata    = figure.add_subplot(1,1,1)

    def plotData(self,arm_id):
        self.plotProductsGraphics()

    def plotWidgets(self) :
        widgets = list()

        labels_prod = self.labels_prod
        self.radiobutton_pro = reflex_plot_widgets.InteractiveRadioButtons(self.product_selector, self.setProductSelectCallBack, labels_prod, 0, title="")
        widgets.append(self.radiobutton_pro)

        return widgets

    def setArmSelectCallBack(self, arm_id) :
        if arm_id == 'RED':
           self.arm_id = 2
           self.ext_ima = 2
        else :
           self.arm_id = 1
           self.ext_ima = 1
      
        self.plotImage()
 
    def setQCSelectCallBack(self, qc_id) :
        self.qc_id = qc_id
        self.subplot_scatter.cla()

    def setProductSelectCallBack(self, product_id) :
        print "Selected product id:", product_id
        self.product_id = product_id
        self.subplot_common.cla()
        self.displayProducts()      
        print "product_id:", self.product_id
         
    def setPlotSpectrum(self,obj,qual) :
        self.plotImageSlice(obj,qual,self.subplot_spectrum,title,tooltip,Xlab,Ylab);

    def preparePlotQC(self) :
        x = self.rop
        xLab = 'Read Out Port'
        if self.qc_id == 'HOTPIX' :
          y = self.dark_mean 
          yLab = 'Hot Pixels number'
          title = 'Hotpixel number vs Read Out Port'
 
        else :
          y = self.hotpix 
          yLab = 'Dark Mean value [ADU]'
          title = 'Dark Mean vs Read Out Port'
          
        tooltip = 'plot of '+title
        self.title=title
        self.tooltip=tooltip
        
        self.x=x
        self.y=y
        self.xLab=xLab
        self.yLab=yLab

    def prepTitleAndTooltipImage(self) :
        title_pref = 'Linear-extracted and Merged Spectrum.'	      
        title_spectrum   = title_pref

        if self.product_id == 'Distortion Pixel Mask' :
           self.obj = self.distortion
           spec_frame   = 'Distortion Pixel Mask'
           title_distortion   = 'Distortion Pixel Mask'
           self.tooltip_distortion ="""\
                Image of the distortion pixel map frame. 
                """
        elif self.product_id == 'Slitlet distance' :
           spec_frame   = 'Slitlet distance '
           title_distortion   = 'Slitlet distance'
           self.tooltip_distortion ="""\
                Plot of the slitlet distances. 
                """

        elif self.product_id == 'Dist at ref pos' :
           spec_frame   = 'Dist at ref pos '
           title_distortion   = 'Dist at ref pos'
           self.tooltip_distortion ="""\
                Plot of the distortion at reference positions. 
                """


        elif self.product_id == 'FIBRE_NS_STACKED_ON' :
           spec_frame   = 'FIBRE_NS_STACKED '
           title_distortion   = 'FIBRE_NS_STACKED  frame'
           self.tooltip_distortion ="""\
                Image of the FIBRE_NS_STACKED  frame. 
                """

           self.obj = self.bp_map_di

        self.title_frame   = spec_frame+'image'

    def plotImage(self) :
        self.prepTitleAndTooltipImage()
        self.dpm.plotImageOnly(self.obj,self.ext_ima, self.binx,self.biny,'X [pix]', 'Y [pix]', self.subplot_image,self.title_frame,self.tooltip_distortion)

    def plotImageFibreStackedON(self,obj,subplot) :
        self.prepTitleAndTooltipImage()
        obj.readImage(0)
        nyM, nxM = obj.image.shape
        self.nxM = nxM
        self.nyM = nyM
        self.dpm.plotImageOnly(obj,0, 1, 1,'X [pix]', 'Y [pix]', subplot,self.title_frame,self.tooltip_distortion)


    def readTables(self,m_dist_tab,m_coef_tab) :
        # copy current distortion FITS table columns into arrays
        m_dist_tab.readTableColumn(fits_extension=1,colname='slitlet_distance')
        Mdist=m_dist_tab.column
        self.Mdist=Mdist
        
        m_coef_tab.readTableColumn(fits_extension=1,colname='degx')
        MdegX=m_coef_tab.column
        self.MdegX=MdegX
        
        m_coef_tab.readTableColumn(fits_extension=1,colname='degy')
        MdegY=m_coef_tab.column
        self.MdegY=MdegY
        
        m_coef_tab.readTableColumn(fits_extension=1,colname='coeff')
        Mcoef=m_coef_tab.column
        self.Mcoef=Mcoef

    #def plotSlitletDistances(self,m_dist_tab,r_dist_tab,m_coef_tab,r_coef_tab,subplot) :
    def plotSlitletDistances(self,m_dist_tab,m_coef_tab,subplot) :

        #=======================================================================
        # PLOT 1 (221):  Plotting the distances between slitlets (in pixels) +
        #                computing a median value
        #=======================================================================
        #subplot(221)

        slitlet = range(1,32,1)
        self.readTables(m_dist_tab,m_coef_tab)
        Mdist=self.Mdist
        MdegX=self.MdegX
        MdegY=self.MdegY
        Mcoef=self.Mcoef
         
         
        # copy reference distortion FITS table columns into arrays
        #r_dist_tab.readTableColumn(fits_extension=1,colname='slitlet_distance')
        #Rdist=m_dist_tab.column
        

        Mmedslit = numpy.median(Mdist)
        Msigslit = Mdist.std()
        #Rmedslit = numpy.median(Rdist)
        #Rsigslit = Rdist.std()

        #plot(slitlet, Mdist, 'o', color=colM, linewidth=1.0) #, linestyle='steps-')
        line = subplot.axhline(y=Mmedslit, color='b', linestyle='-', linewidth=1.0, alpha=1.0)

        #line = subplot.axhline(y=Rmedslit, color='r', linestyle='-.', linewidth=1.0)
        # +/- 1 sigma shaded region around current DIST median
        line = subplot.axhspan(Mmedslit-Msigslit, Mmedslit+Msigslit, facecolor='g', alpha=0.10)

        Ylab=r'$\Delta\ \rm{distance\ between\ slitlets\ (pixels)}$'
        Xlab='Slitlet Number'
        title = 'Slitlet-to-Slitlet distances \n (median =  %5.2f +/- %3.1f pixels)'%(Mmedslit, Msigslit)

        subplot.set_xlabel(Xlab)
        subplot.set_ylabel(Ylab)
        tooltip='plot of slitlet distances as function of slitlet number'
        subplot.set_title(title, fontsize=12)
	subplot.tooltip=tooltip
        
        #subplot.legend(('slitlet distances','current median','reference median'), 'best', prop=fontLegend, handlelength=0.08, shadow=False)
        tooltip='plot of '+title
        xLim = 0,33
        yLim = 60,68
        subplot.set_xlim(xLim)
        subplot.set_ylim(yLim)
        subplot.plot(slitlet, Mdist, 'o', color='blue', linewidth=1.0) #, linestyle='steps-')
        #subplot.display(subplot,title,self.paragraph(tooltip),slitlet,Mdist)

      
    def plotDistortion(self,m_dist_tab,r_dist_tab,m_coef_tab,r_coef_tab,subplot) :


        #=======================================================================
        # PLOT 2 (222):  Plotting 4 quadrant + center representative Mdistortions
        #=======================================================================

        # The distortion field positions: LL, LR, CC, UL, and UR
        dX = [512., 1536., 1024., 512.,  1536.]
        dY = [512., 512.,  1024., 1536., 1536.]
        #ax = subplot(222)

        MdXY        = numpy.zeros(len(dX), float)
        M_delta_dXY = numpy.zeros(len(dX), float)
        RdXY        = numpy.zeros(len(dX), float)
        R_delta_dXY = numpy.zeros(len(dX), float)
        
        r_coef_tab.readTableColumn(fits_extension=1,colname='coeff')
        Rcoef=r_coef_tab.column
        self.Rcoef=Rcoef

        Mcoef=self.Mcoef
        Rcoef=self.Rcoef

        for i in range(len(dX)):
	    MdXY[i] = Mcoef[0]+Mcoef[1]*dX[i]+Mcoef[2]*dY[i]+Mcoef[3]*dX[i]*dY[i]+Mcoef[4]*dX[i]**2+Mcoef[5]*dY[i]**2+Mcoef[6]*dX[i]**2*dY[i]+Mcoef[7]*dX[i]*dY[i]**2+Mcoef[8]*dX[i]**3+Mcoef[9]*dY[i]**3
	
        for i in range(len(dX)):
	    RdXY[i] = Rcoef[0]+Rcoef[1]*dX[i]+Rcoef[2]*dY[i]+Rcoef[3]*dX[i]*dY[i]+Rcoef[4]*dX[i]**2+Rcoef[5]*dY[i]**2+Rcoef[6]*dX[i]**2*dY[i]+Rcoef[7]*dX[i]*dY[i]**2+Rcoef[8]*dX[i]**3+Rcoef[9]*dY[i]**3


        for i in range(len(dX)):
	    M_delta_dXY[i] = MdXY[i] - dX[i]
	    R_delta_dXY[i] = RdXY[i] - dX[i]




        # these are the nominal distortion lines, the distortions of the current frame, and the distortions of the reference frame:

        subplot.plot(dX, dY, 'o', color='k', markersize=20, markerfacecolor='w')
        subplot.plot(RdXY, dY, 's', color='red', markersize=10)
        subplot.plot(MdXY, dY, 's', color='blue', markersize=5)

        for i in range(len(dX)):
	    subplot.axvline(dX[i], color='k', linestyle='-.', linewidth=1.0)
       	    subplot.axhline(dY[i], color='k', linestyle='-.', linewidth=1.0)
	


        #xlim(0,nxM)
        #ylim(0,nyM)
        nxM = self.nxM
        nyM = self.nyM
        xLim = 0,nxM
        yLim = 0,nyM
        subplot.set_xlim(xLim)
        subplot.set_ylim(yLim)


        x2 = nxM + 0.02*nxM

        #text(x2, dY[0]-60, r'$\Delta x=%5.2f/%5.2f$'%(M_delta_dXY[0],M_delta_dXY[1]), color='blue', size=11)
        #text(x2, dY[2]-60, r'$\Delta x=%5.2f$'%(M_delta_dXY[2]), color='blue', size=11)
        #text(x2, dY[3]-60, r'$\Delta x=%5.2f/%5.2f$'%(M_delta_dXY[3],M_delta_dXY[4]), color='blue', size=11)
        subplot.text(x2, dY[0]-60, r'$\Delta x=%5.2f/%5.2f$'%(M_delta_dXY[1],M_delta_dXY[0]), color='blue', size=11)
        subplot.text(x2, dY[2]-60, r'$\Delta x=%5.2f$'%(M_delta_dXY[2]), color='blue', size=11)
        subplot.text(x2, dY[3]-60, r'$\Delta x=%5.2f/%5.2f$'%(M_delta_dXY[4],M_delta_dXY[3]), color='blue', size=11)

        subplot.set_aspect('equal')

        #subplot.xlabel('X axis (pixels)')
        #subplot.ylabel('Y axis (pixels)')
        Xlab='X axis (pixels)'
        Ylab='Y axis (pixels)'
        spectrum = SpectrumDisplay()
        spectrum.setLabels(Xlab,Ylab)
        
        title1 = 'Distortion at 5 detector positions'
        tooltip = 'This plot shows the distortion at 5 detector positions'
        #subplot.title(title1, fontsize=12)
        subplot.set_title(title1, fontsize=12)
        fontLegend = FontProperties(size=8)
	subplot.tooltip = tooltip
        subplot.legend(('nominal_(detector_centered)','reference distortion','current distortion'), 'upper right', prop=fontLegend, handlelength=0.08, shadow=False)


    def caseNodata(self) :
        #Data not found info
        self.subtext_nodata.set_axis_off()
        self.text_nodata = """\
                           The python GUI miss some required input frame
                           This may be due to a recipe failure or to missing
                           reference frames.
                           This may be due to a recipe failure. 
                           Check your input parameter values, 
                           correct possibly typos and
                           press 'Re-run recipe' button."""
        self.subtext_nodata.text(0.1, 0.6, self.text_nodata, color='#11557c', 
                                 fontsize=18, ha='left', va='center', alpha=1.0)
        self.subtext_nodata.tooltip="""\
                                    Merged spectrum not found in the products"""
        #print "found no spectrum data"

        
    def displayProducts(self):
        self.prepTitleAndTooltipImage()

        if self.product_id == 'FIBRE_NS_STACKED_ON' :
            self.plotImageFibreStackedON(self.fibre_ns_stacked_on,self.subplot_common)
        elif self.product_id == 'Distortion Pixel Mask' :
            self.plotImageFibreStackedON(self.rfibre_ns_stacked_on,self.subplot_common)
        elif self.product_id == 'Slitlet distance' :
            #self.plotSlitletDistances(self.slitlets_distance,self.rslitlets_distance,self.distortion,self.rdistortion,self.subplot_common)
            self.plotSlitletDistances(self.slitlets_distance,self.distortion,self.subplot_common)
        elif self.product_id == 'Dist at ref pos' :
            self.plotDistortion(self.slitlets_distance,self.rslitlets_distance,self.distortion,self.rdistortion,self.subplot_common)
        
  
    # This is the function that makes the plots.
    # Add new plots or delete them using the given scheme.
    # The data has been already stored in self.plot_x, self.plot_xdif, etc ...
    # It is mandatory to add a tooltip variable to each subplot.
    # One might be tempted to merge addSubplots() and plotProductsGraphics().
    # There is a reason not to do it: addSubplots() is called only once at
    # startup, while plotProductsGraphics() is called always there is a resize.
    def plotProductsGraphics(self):
      if (self.distortion_found == True and self.bp_map_di_found == True) :
        self.prepTitleAndTooltipImage()
        #self.preparePlotQC()
        
        sy = self.obj.all_hdu[self.ext_ima].header['NAXIS2']
        self.ima_sy=sy
        self.dpm = sinfo_plot_common.DataPlotterManager()
        self.displayProducts()
  
      else :
        self.caseNodata() 
  
    # This function specifies which are the parameters that should be presented
    # in the window to be edited.
    # Note that the parameter has to be also in the in_sop port (otherwise it 
    # won't appear in the window) 
    # The descriptions are used to show a tooltip. They should match one to one
    # with the parameter list 
    # Note also that parameters have to be prefixed by the 'recipe name:'

    def setInteractiveParameters(self):
      paramList = list()
      rec_id=self.rec_id
      self.par = sinfo_parameters_common.Parameters()

      if rec_id == "sinfo_rec_distortion" :
        self.par.setDistortionParameters(paramList,"sinfo_rec_distortion","all")
        print "recipe" ,rec_id,  "no parameters"

      else :
         print "recipe" ,rec_id,  "not supported"

      return paramList

    def setWindowHelp(self):
      help_text = """
This is an interactive window which help asses the quality of the execution of a recipe.
"""
      return help_text

    def setWindowTitle(self):
      title = 'SINFONI Interactive ' + self.rec_subtitle  
      return title

except ImportError:
  import_sucess = 'false'
  print "Error importing modules pyfits, wx, matplotlib, numpy"
  raise

#This is the 'main' function
if __name__ == '__main__':

  # import reflex modules
  from reflex import *
  from reflex_interactive_app import *
  from pipeline_display import *
  from pipeline_product import *

  # Create interactive application
  interactive_app = PipelineInteractiveApp(enable_init_sop=True)

  # PECULIAR XSH needs this in order to be able later to get from an input FITS
  # the ins-mode, arm (and recipe) IDs, used in titles and param setting
  # get inputs from the command line
  interactive_app.parse_args()
  inputs = interactive_app.inputs
  #(inputs, args) = interactive_app.parse_args()

  #Check if import failed or not
  print "import_sucess=", import_sucess
  if import_sucess == 'false' :
     interactive_app.setEnableGUI(false)
    
  #interactive_app.setEnableGUI(True)
  #Open the interactive window if enabled
  if interactive_app.isGUIEnabled() :
    #Get the specific functions for this window
    dataPlotManager = DataPlotterManager()
    #print inputs.in_sof
    #dataPlotManager.checkSofIsNotEmpty(inputs.in_sof)
    #With the following call XSH get the: ins-mode, arm (and recipe) IDs
    dataPlotManager.getArmId(inputs.in_sof)
    #Set recipe ID in order to build proper param list, display layout
    dataPlotManager.setRecId("sinfo_rec_distortion")
    interactive_app.setPlotManager(dataPlotManager)
    interactive_app.showGUI()
  
  else :
    interactive_app.passProductsThrough()
   
  #Print outputs. This is parsed by the Reflex python actor to get the results
  #Do not remove
  interactive_app.print_outputs()
  sys.exit()
