/* $Id: sinfo_utl_line_oh_select.c,v 1.3 2013-07-15 08:14:38 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-07-15 08:14:38 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <math.h>
#include <cpl.h>

#include "irplib_utils.h"
#include <sinfo_utils_wrappers.h>
#include <sinfo_msg.h>

#include "sinfo_raw_types.h"
#include "sinfo_pro_types.h"
#include "sinfo_tpl_utils.h"
#include "sinfo_tpl_dfs.h"
#include "sinfo_globals.h"

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/

static int sinfo_utl_line_oh_select_create(cpl_plugin *) ;
static int sinfo_utl_line_oh_select_exec(cpl_plugin *) ;
static int sinfo_utl_line_oh_select_destroy(cpl_plugin *) ;
static int sinfo_utl_line_oh_select(cpl_parameterlist *, cpl_frameset *) ;

/*-----------------------------------------------------------------------------
                            Static variables
 -----------------------------------------------------------------------------*/

static char sinfo_utl_line_oh_select_description[] = 
                "sinfo_utl_line_oh_select -- SINFONI OH LINE table creation.\n"
                "The files listed in the Set Of Frames (sof-file) must be tagged:\n"
                "raw-file.fits REF_LINE_OH \n" ;

/*-----------------------------------------------------------------------------
                                Functions code
 -----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module. 
  @param    list    the plugin list
  @return   0 if everything is ok

  This function is exported.
 */
/*----------------------------------------------------------------------------*/
int cpl_plugin_get_info(cpl_pluginlist * list)
{
    cpl_recipe  *   recipe = cpl_calloc(1, sizeof(*recipe)) ;
    cpl_plugin  *   plugin = &recipe->interface ;

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_utl_line_oh_select",
                    "OH line table creation",
                    sinfo_utl_line_oh_select_description,
                    "Andrea Modigliani",
                    "amodigli@eso.org",
                    sinfo_get_license(),
                    sinfo_utl_line_oh_select_create,
                    sinfo_utl_line_oh_select_exec,
                    sinfo_utl_line_oh_select_destroy) ;

    cpl_pluginlist_append(list, plugin) ;

    return 0;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using the 
  interface. 
 */
/*----------------------------------------------------------------------------*/
static int sinfo_utl_line_oh_select_create(cpl_plugin * plugin)
{
    cpl_recipe      * recipe ;
    cpl_parameter* p=NULL;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    /* Create the parameters list in the cpl_recipe object */
    recipe->parameters = cpl_parameterlist_new() ;

    /* --doubleopt */
    p = cpl_parameter_new_range("sinfoni.sinfo_utl_line_oh_select.wmin", 
                    CPL_TYPE_DOUBLE, "wmin",
                    "sinfoni.sinfo_utl_line_oh_select",
                    1040.,1000.,2500.) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "wmin") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_range("sinfoni.sinfo_utl_line_oh_select.wmax", 
                    CPL_TYPE_DOUBLE, "wmax",
                    "sinfoni.sinfo_utl_line_oh_select",
                    1880.,1000.,2500.) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "wmax") ;
    cpl_parameterlist_append(recipe->parameters, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_line_oh_select.intnorm", 
                    CPL_TYPE_DOUBLE, "intnorm",
                    "sinfoni.sinfo_utl_line_oh_select",
                    4.) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "intnorm") ;
    cpl_parameterlist_append(recipe->parameters, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_line_oh_select.intlimit", 
                    CPL_TYPE_DOUBLE, "intlimit",
                    "sinfoni.sinfo_utl_line_oh_select",
                    50.) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "intlimit") ;
    cpl_parameterlist_append(recipe->parameters, p) ;




    /* Return */
    return 0;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int sinfo_utl_line_oh_select_exec(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    return sinfo_utl_line_oh_select(recipe->parameters, recipe->frames) ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int sinfo_utl_line_oh_select_destroy(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE)
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_parameterlist_delete(recipe->parameters) ;
    return 0 ;
}

/*----------------------------------------------------------------------------*/
/**
  @brief    The recipe data reduction part is implemented here 
  @param    parlist     the parameters list
  @param    framelist   the frames list
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int sinfo_utl_line_oh_select(
                cpl_parameterlist   *   parlist,
                cpl_frameset        *   framelist)
{
    cpl_parameter* p=NULL;
    cpl_frame* frame=NULL;

    cpl_frameset    *   rawframes ;
    int                 nframes;
    cpl_propertylist* phead=NULL;
    cpl_table       *   tab_in ;
    cpl_table       *   tab_ou ;

    int nm2AA=10.;


    double wmin=0;
    double wmax=0;
    double intnorm=0;
    double intlimit=0;
    int next=0;
    int nrow=0;
    /* Identify the RAW and CALIB frames in the input frameset */
    if (sinfo_dfs_set_groups(framelist)) {
        cpl_msg_error(__func__, "Cannot identify RAW and CALIB frames") ;
        return -1 ;
    }

    /* Retrieve raw frames */
    if ((rawframes = sinfo_extract_frameset(framelist,
                    "REF_LINE_OH")) == NULL) {
        cpl_msg_error(__func__, "Cannot find raw frames in the input list") ;
        return -1 ;
    }

    p=cpl_parameterlist_find(parlist, "sinfoni.sinfo_utl_line_oh_select.wmin");
    wmin = cpl_parameter_get_double(p);

    p=cpl_parameterlist_find(parlist, "sinfoni.sinfo_utl_line_oh_select.wmax");
    wmax = cpl_parameter_get_double(p);

    p=cpl_parameterlist_find(parlist, "sinfoni.sinfo_utl_line_oh_select.intnorm");
    intnorm = cpl_parameter_get_double(p);
    p=cpl_parameterlist_find(parlist, "sinfoni.sinfo_utl_line_oh_select.intlimit");
    intlimit = cpl_parameter_get_double(p);

    nframes = cpl_frameset_get_size(rawframes) ;

    /* Load */
    if(nframes > 0) {
        frame=cpl_frameset_get_frame(rawframes,0);
    }
    tab_in=cpl_table_load(cpl_frame_get_filename(frame),1,0);
    nrow=cpl_table_get_nrow(tab_in);

    cpl_table_divide_scalar(tab_in,"wave",nm2AA);
    cpl_table_divide_scalar(tab_in,"int",intnorm);
    sinfo_msg("wmin=%g wmax=%g",wmin,wmax);
    sinfo_msg("nrow=%d",nrow);
    cpl_table_dump(tab_in,1,2,stdout);
    cpl_table_dump(tab_in,nrow-10,2,stdout);
    next=cpl_table_and_selected_double(tab_in,"wave",CPL_GREATER_THAN,wmin);
    sinfo_msg("next=%d",next);
    next=cpl_table_and_selected_double(tab_in,"wave",CPL_LESS_THAN,wmax);
    sinfo_msg("next=%d",next);
    next=cpl_table_and_selected_double(tab_in,"int",CPL_GREATER_THAN,intlimit);
    sinfo_msg("next=%d",next);
    tab_ou=cpl_table_extract_selected(tab_in);

    cpl_table_save(tab_ou,phead,NULL,"oh_selected.fits",CPL_IO_DEFAULT);

    /* Free and return */
    sinfo_free_table(&tab_in);
    sinfo_free_table(&tab_ou);
    sinfo_free_propertylist(&phead);


    return 0 ;
}



