/* $Id: sinfo_rec_pupil.c,v 1.17 2008-02-12 14:56:50 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2008-02-12 14:56:50 $
 * $Revision: 1.17 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *          Wave_Cal Frames Data Reduction                          *
 ****************************************************************/

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#include <config.h>          /* allows the program compilation */
#endif

/* std */
#include <strings.h>
#include <string.h>
#include <stdio.h>


/* cpl */
#include <cpl.h>      

/* irplib */
#include <irplib_utils.h>


/* sinfoni */
#include <sinfo_pro_types.h>
#include <sinfo_product_config.h>
#include <sinfo_prepare_stacked_frames_config.h>
#include <sinfo_objnod_config.h>
#include <sinfo_raw_types.h>
#include <sinfo_tpl_utils.h>
#include <sinfo_tpl_dfs.h>
#include <sinfo_hidden.h>
#include <sinfo_globals.h>
#include <sinfo_functions.h>
#include <sinfo_msg.h>
#include <sinfo_new_prepare_stacked_frames.h>
#include <sinfo_new_objnod.h>
#include <sinfo_error.h>
#include <sinfo_utils_wrappers.h>
/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/
static int sinfo_rec_pupil_create(cpl_plugin *);
static int sinfo_rec_pupil_exec(cpl_plugin *);
static int sinfo_rec_pupil_destroy(cpl_plugin *);
static int sinfo_rec_pupil(cpl_parameterlist *, cpl_frameset *);

/*-----------------------------------------------------------------------------
                            Static variables
 -----------------------------------------------------------------------------*/
static char sinfo_rec_pupil_description1[] =
                "This recipe performs science data reduction.\n"
                "The input files are science object and sky frames \n"
                "with tags OBJECT_NODDING and SKY_NODDING\n"
                "Master calibration frames:\n";


static char sinfo_rec_pupil_description2[] =
                "A corresponding (band,preoptics) wavelength map image with tag WAVE_MAP\n"
                "A corresponding (band,preoptics) master flat field with tag MASTER_FLAT_LAMP\n"
                "A corresponding (band,preoptics) master bad pixel map with tag MASTER_BP_MAP\n"
                "A corresponding (band,preoptics) slitlets position frame with tag SLIT_POS\n"
                "A corresponding (band) distortion table with tag DISTORTION\n"
                "A corresponding (band) slitlet distance table with tag SLITLETS_DISTANCE\n";


static char sinfo_rec_pupil_description3[] =
                "The output is an image resulting from the IMA1 op IMA2 where op indicates\n"
                "A reference table with the position of the 1st column with tag FIRST_COLUMN\n"
                "Relevant outputs are:\n"
                "combined cubes (PRO.CATG=x_OBS x=STD,OBJ,PSF)\n"
                "reconstructed cube (PRO.CATG=COADD_x_OBS x=STD,OBJ,PSF)\n"
                "An average along Z of the reconstructed cube \n"
                "(PRO.CATG=MED_x_OBS x=STD,OBJ,PSF)\n"
                "The bad pixel map associated to the cube \n"
                "(PRO.CATG=BP_MAP_COADD_x_OBS x=STD,OBJ,PSF)\n"
                "\n";


static char sinfo_rec_pupil_description[1300];

/*-----------------------------------------------------------------------------
                                Functions code
 ----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_rec_pupil   Recipe to reduce pupil data
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/*---------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module. 
  @param    list    the plugin list
  @return   0 if everything is ok, -1 otherwise

  Create the recipe instance and make it available to the application using the 
  interface. This function is exported.
 */
/*---------------------------------------------------------------------------*/

int
cpl_plugin_get_info(cpl_pluginlist *list)
{

    cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe);
    cpl_plugin *plugin = &recipe->interface;


    strcpy(sinfo_rec_pupil_description,sinfo_rec_pupil_description1);
    strcat(sinfo_rec_pupil_description,sinfo_rec_pupil_description2);
    strcat(sinfo_rec_pupil_description,sinfo_rec_pupil_description3);

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_rec_pupil",
                    "Pupil data reduction",
                    sinfo_rec_pupil_description,
                    "Andrea Modigliani",
                    "Andrea.Modigliani@eso.org",
                    sinfo_get_license(),
                    sinfo_rec_pupil_create,
                    sinfo_rec_pupil_exec,
                    sinfo_rec_pupil_destroy);

    cpl_pluginlist_append(list, plugin);

    return 0;

}

/*---------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    plugin  the plugin
  @return   0 if everything is ok

  Defining the command-line/configuration parameters for the recipe.
 */
/*---------------------------------------------------------------------------*/

static int sinfo_rec_pupil_create(cpl_plugin *plugin)
{
    cpl_recipe      * recipe ;

    /* Check that the plugin is part of a valid recipe */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;
    cpl_error_reset();
    irplib_reset();

    /* Create the parameters list in the cpl_recipe object */
    recipe->parameters = cpl_parameterlist_new() ; 


    /*
     * Fill the parameter list.
     */

    sinfo_product_config_add(recipe->parameters);
    sinfo_prepare_stacked_frames_config_add(recipe->parameters);
    sinfo_objnod_config_add(recipe->parameters);
    return 0;

}

/*---------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/


static int sinfo_rec_pupil_exec(cpl_plugin *plugin)
{

    cpl_recipe *recipe = (cpl_recipe *) plugin;
    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    return sinfo_rec_pupil(recipe->parameters, recipe->frames);

}

/*---------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_rec_pupil_destroy(cpl_plugin *plugin)
{
    cpl_recipe  *   recipe ;
    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_parameterlist_delete(recipe->parameters);

    return 0;

}

/*---------------------------------------------------------------------------*/
/**
  @brief    Interpret the command line options and execute the data processing
  @param    parlist     the parameters list
  @param    frameset   the frames list
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
/*
 * The actual recipe actually start here.
 */

static int sinfo_rec_pupil(cpl_parameterlist *config, cpl_frameset *set)
{

    fake* fk=NULL;
    fk=sinfo_fake_new();

    sinfo_msg("Welcome to SINFONI Pipeline release %d.%d.%d",
              SINFONI_MAJOR_VERSION,SINFONI_MINOR_VERSION,SINFONI_MICRO_VERSION);

    ck0(sinfo_dfs_set_groups(set),"Cannot indentify RAW and CALIB frames") ;

    /* hidden parameters */
    sinfo_msg("---------------------------------------------");
    sinfo_msg("%s FRAME DETERMINATION", PRO_PUPIL_LAMP_STACKED);
    sinfo_msg("---------------------------------------------");

    ck0(sinfo_new_prepare_stacked_frames(cpl_func,config, set, NULL,
                    PRO_PUPIL_LAMP_STACKED,0,fk ),
        "Failed %s FRAME DETERMINATION", PRO_PUPIL_LAMP_STACKED);
    sinfo_msg("%s FRAME DETERMINATION SUCCESS", PRO_PUPIL_LAMP_STACKED);

    sinfo_msg("---------------------------------------------");
    sinfo_msg("PUPIL DATA REDUCTION");
    sinfo_msg("---------------------------------------------");

    ck0(sinfo_new_objnod(cpl_func,config, set,PRO_COADD_PUPIL),
        "Failed PUPIL DATA REDUCTION") ;
    sinfo_msg("PUPIL DATA REDUCTION SUCCESS");

    cleanup:
    sinfo_fake_delete(&fk);

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        return -1;
    } else {
        return 0;
    }


}
/**@}*/
