/* $Id: sinfo_utl_cube_combine.c,v 1.12 2007-10-26 09:40:28 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2007-10-26 09:40:28 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

/* cpl */
#include <cpl.h>

/* irplib */
#include <irplib_utils.h>

#include <sinfo_tpl_utils.h>
#include <sinfo_pfits.h>
#include <sinfo_tpl_dfs.h>
#include <sinfo_msg.h>
#include <sinfo_utl_cube_combine.h>

/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/

static int sinfo_utl_cube_combine_create(cpl_plugin *) ;
static int sinfo_utl_cube_combine_exec(cpl_plugin *) ;
static int sinfo_utl_cube_combine_destroy(cpl_plugin *) ;

/*-----------------------------------------------------------------------------
                            Static variables
 ----------------------------------------------------------------------------*/

static char sinfo_utl_cube_combine_description[] =
                "This recipe perform cubes combination.\n"
                "The input files are several cubeses\n"
                "their associated tags should be the same that is supported by the \n"
                "recipe sinfo_rec_jitter: OBS_OBJ, OBS_STD, OBS_PSF.\n"
                "The output is a cube PRO_CUBE resulting from the input cubes accurding \n"
                "to the value of op, where op indicates\n"
                "the operation to be performed specified by the parameter \n"
                "sinfoni.sinfo_utl_cube_combine.op\n"
                " having alias 'op'\n"
                "\n";

/*-----------------------------------------------------------------------------
                                Functions code
 ----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_utl_cube_combine  Recipe to coadd cubes
 */
/*---------------------------------------------------------------------------*/

/**@{*/
/*---------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module. 
  @param    list    the plugin list
  @return   0 if everything is ok

  This function is exported.
 */
/*---------------------------------------------------------------------------*/
int cpl_plugin_get_info(cpl_pluginlist * list)
{
    cpl_recipe  *   recipe = cpl_calloc(1, sizeof *recipe ) ;
    cpl_plugin  *   plugin = &recipe->interface ;

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_utl_cube_combine",
                    "Coadd cubes in an output cube according "
                    "to user defined offsets",
                    sinfo_utl_cube_combine_description,
                    "Andrea Modigliani",
                    "Andrea.Modigliani@eso.org",
                    sinfo_get_license(),
                    sinfo_utl_cube_combine_create,
                    sinfo_utl_cube_combine_exec,
                    sinfo_utl_cube_combine_destroy) ;

    cpl_pluginlist_append(list, plugin) ;

    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using the 
  interface. 
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube_combine_create(cpl_plugin * plugin)
{
    cpl_recipe      * recipe ;
    cpl_parameter   * p ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;
    cpl_error_reset();
    irplib_reset();

    /* Create the parameters list in the cpl_recipe object */
    recipe->parameters = cpl_parameterlist_new() ; 

    /* Fill the parameters list */
    /* --stropt */
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.name_i", 
                    CPL_TYPE_STRING,
                    "Input filename. This must be provided and allow the user to set X "
                    "and Y cumulative offsets in a two column format",
                    "sinfoni.sinfo_utl_cube_combine","offset.list");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "name_i") ;
    cpl_parameterlist_append(recipe->parameters, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.name_o", 
                    CPL_TYPE_STRING, "Output filename",
                    "sinfoni.sinfo_utl_cube_combine","out_coadd_cube.fits");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "name_o") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.ks_clip", 
                    CPL_TYPE_BOOL, "Kappa sigma clipping",
                    "sinfoni.sinfo_utl_cube_combine",FALSE);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ks_clip") ;
    cpl_parameterlist_append(recipe->parameters, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.scale_sky", 
                    CPL_TYPE_BOOL, "Scale spatial mean",
                    "sinfoni.sinfo_utl_cube_combine",FALSE);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "scale_sky") ;
    cpl_parameterlist_append(recipe->parameters, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.kappa", 
                    CPL_TYPE_DOUBLE, "Kappa value for sigma clip",
                    "sinfoni.sinfo_utl_cube_combine",2.);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "kappa") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    /* --doubleopt */
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.xsize", 
                    CPL_TYPE_INT, "Output cube X size",
                    "sinfoni.sinfo_utl_cube_combine", 80) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "xsize") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.ysize", 
                    CPL_TYPE_INT, "Output cube Y size",
                    "sinfoni.sinfo_utl_cube_combine", 80) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ysize") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_combine.offset_mode",
                       CPL_TYPE_BOOL, "Offset conventions. If TRUE applies "
                                      "reference offset correction. If FALSE: "
                                      "take user offsets. The reference offset "
                                      "is computed as (min_off+max_off)/2",
                       "sinfoni.sinfo_utl_cube_combine", CPL_TRUE) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "offset_mode") ;
    cpl_parameterlist_append(recipe->parameters, p) ;




    /* Return */
    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube_combine_exec(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;
    int result=0;    
    cpl_errorstate initial_errorstate = cpl_errorstate_get();

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;
    sinfo_msg("Welcome to SINFONI Pipeline release %d.%d.%d",
              SINFONI_MAJOR_VERSION,SINFONI_MINOR_VERSION,SINFONI_MICRO_VERSION);

    result=sinfo_utl_cube_combine(recipe->parameters, recipe->frames) ;
    if (!cpl_errorstate_is_equal(initial_errorstate)) {                      
        /* Dump the error history since recipe execution start.                
           At this point the recipe cannot recover from the error */           
        cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);              
    } 
    return result;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube_combine_destroy(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_parameterlist_delete(recipe->parameters) ; 
    return 0 ;
}

/**@}*/
