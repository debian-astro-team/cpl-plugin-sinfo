/* $Id: sinfo_utl_cube2spectrum.c,v 1.12 2007-10-26 08:33:11 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2007-10-26 08:33:11 $
 * $Revision: 1.12 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

/* cpl */
#include <cpl.h>
/* irplib */
#include <irplib_utils.h>

#include <sinfo_tpl_utils.h>
#include <sinfo_pfits.h>
#include <sinfo_tpl_dfs.h>
#include <sinfo_utl_cube2spectrum.h>
#include <sinfo_msg.h>
/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/

static int sinfo_utl_cube2spectrum_create(cpl_plugin *) ;
static int sinfo_utl_cube2spectrum_exec(cpl_plugin *) ;
static int sinfo_utl_cube2spectrum_destroy(cpl_plugin *) ;

/*-----------------------------------------------------------------------------
                            Static variables
 -----------------------------------------------------------------------------*/

static char sinfo_utl_cube2spectrum_description[] =
                "This recipe performs cube to 1D spectrum image conversion.\n"
                "The input files is a cube\n"
                "Its associated tag should be CUBE.\n"
                "The output is an image resulting from the cube manipulated \n"
                "according to the value of op\n"
                "Over an aperture as specified by the parameter \n"
                "sinfoni.sinfo_utl_cube2spectrum.aperture having alias 'op', 'ap'"
                "\n";

/*-----------------------------------------------------------------------------
                                Functions code
 ----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_utl_cube2spectrum  Recipe to collapse a cube in a spectrum
 */
/*---------------------------------------------------------------------------*/

/**@{*/
/*---------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module. 
  @param    list    the plugin list
  @return   0 if everything is ok

  This function is exported.
 */
/*---------------------------------------------------------------------------*/
int cpl_plugin_get_info(cpl_pluginlist * list)
{
    cpl_recipe  *   recipe = cpl_calloc(1, sizeof *recipe ) ;
    cpl_plugin  *   plugin = &recipe->interface ;

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_utl_cube2spectrum",
                    "Collapse a cube to an image over an aperture",
                    sinfo_utl_cube2spectrum_description,
                    "Andrea Modigliani",
                    "Andrea.Modigliani@eso.org",
                    sinfo_get_license(),
                    sinfo_utl_cube2spectrum_create,
                    sinfo_utl_cube2spectrum_exec,
                    sinfo_utl_cube2spectrum_destroy) ;

    cpl_pluginlist_append(list, plugin) ;

    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using the 
  interface. 
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube2spectrum_create(cpl_plugin * plugin)
{
    cpl_recipe      * recipe ;
    cpl_parameter   * p ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;
    cpl_error_reset();
    irplib_reset();

    /* Create the parameters list in the cpl_recipe object */
    recipe->parameters = cpl_parameterlist_new() ; 

    /* Fill the parameters list */
    /* --stropt */
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.op", 
                    CPL_TYPE_STRING, "A possible operation:"
                    " average, clean_mean, median, sum",
                    "sinfoni.sinfo_utl_cube2spectrum","average");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "op") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    /* --stropt */
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.ap", 
                    CPL_TYPE_STRING, "A possible aperture: rectangle, circle",
                    "sinfoni.sinfo_utl_cube2spectrum","rectangle");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ap") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    /* --doubleopt */
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.llx", 
                    CPL_TYPE_INT,
                    "Lower left X rectangle coordinate",
                    "sinfoni.sinfo_utl_cube2spectrum", 2) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "llx") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.lly", 
                    CPL_TYPE_INT,
                    "Lower left Y rectangle coordinate",
                    "sinfoni.sinfo_utl_cube2spectrum", 2) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lly") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.urx", 
                    CPL_TYPE_INT, "Upper right X rectangle coordinate",
                    "sinfoni.sinfo_utl_cube2spectrum", 28) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "urx") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.ury", 
                    CPL_TYPE_INT,
                    "Upper right Y rectangle coordinate",
                    "sinfoni.sinfo_utl_cube2spectrum", 28) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ury") ;
    cpl_parameterlist_append(recipe->parameters, p) ;



    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.lo_rej", 
                    CPL_TYPE_INT,
                    "Clean mean low rejection",
                    "sinfoni.sinfo_utl_cube2spectrum", 10) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lo_rej") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.hi_rej", 
                    CPL_TYPE_INT,
                    "Clean mean low rejection",
                    "sinfoni.sinfo_utl_cube2spectrum", 10) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "hi_rej") ;
    cpl_parameterlist_append(recipe->parameters, p) ;



    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.centerx", 
                    CPL_TYPE_INT,
                    "Circle center X coordinate",
                    "sinfoni.sinfo_utl_cube2spectrum", 16) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "centerx") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.centery", 
                    CPL_TYPE_INT,
                    "Circle center Y coordinate",
                    "sinfoni.sinfo_utl_cube2spectrum", 16) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "centery") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube2spectrum.radius", 
                    CPL_TYPE_INT, "Circle radii",
                    "sinfoni.sinfo_utl_cube2spectrum", 5) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "radius") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    /* Return */
    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube2spectrum_exec(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;
    int result=0;
    cpl_errorstate initial_errorstate = cpl_errorstate_get();

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    if(sinfo_dfs_set_groups(recipe->frames)) {
        sinfo_msg_error( "Cannot indentify RAW and CALIB frames") ;
        return -1;
    }
    sinfo_msg("Welcome to SINFONI Pipeline release %d.%d.%d",
              SINFONI_MAJOR_VERSION,SINFONI_MINOR_VERSION,SINFONI_MICRO_VERSION);

    result = sinfo_utl_cube2spectrum(recipe->parameters, recipe->frames,NULL);
    if (!cpl_errorstate_is_equal(initial_errorstate)) {                      
        /* Dump the error history since recipe execution start.                
           At this point the recipe cannot recover from the error */           
        cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);              
    } 

    return result;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*----------------------------------------------------------------------------*/
static int sinfo_utl_cube2spectrum_destroy(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_parameterlist_delete(recipe->parameters) ; 
    return 0 ;
}

/**@}*/
