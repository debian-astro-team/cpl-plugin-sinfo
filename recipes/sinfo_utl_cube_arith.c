/* $Id: sinfo_utl_cube_arith.c,v 1.10 2007-10-26 09:40:28 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2007-10-26 09:40:28 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/
#include <string.h>

/* cpl */
#include <cpl.h>
/* irplib */
#include <irplib_utils.h>

#include <sinfo_tpl_utils.h>
#include <sinfo_pfits.h>
#include <sinfo_tpl_dfs.h>
#include <sinfo_utl_cube_arith.h>
#include <sinfo_msg.h>

/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/

static int sinfo_utl_cube_arith_create(cpl_plugin *) ;
static int sinfo_utl_cube_arith_exec(cpl_plugin *) ;
static int sinfo_utl_cube_arith_destroy(cpl_plugin *) ;

/*-----------------------------------------------------------------------------
                            Static variables
 ----------------------------------------------------------------------------*/

static char sinfo_utl_cube_arith_description1[] =
                "This recipe perform cube arithmetics.\n"
                "If parameter value is specified the input frame is a cube \n"
                "in a sof file with tag CUBE\n"
                "Else the input files are a cube and an images or a spectrum\n"
                "their associated tags should be respectively CUBE, IMA or SPECTRUM.\n"
                "The output is a cube with tag PRO_CUBE resulting from the operation \n"
                "CUBE op IMA or \n"
                "CUBE op SPECTRUM or\n"
                "CUBE op value where op indicates\n"
                "the operation to be performed\n"
                "\n";


static char sinfo_utl_cube_arith_description[600];



/*-----------------------------------------------------------------------------
                                Functions code
 ----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_utl_cube_arith  Recipe for cube arithmetics
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/*---------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module. 
  @param    list    the plugin list
  @return   0 if everything is ok

  This function is exported.
 */
/*---------------------------------------------------------------------------*/
int cpl_plugin_get_info(cpl_pluginlist * list)
{
    cpl_recipe  *   recipe = cpl_calloc(1, sizeof *recipe ) ;
    cpl_plugin  *   plugin = &recipe->interface ;

    strcpy(sinfo_utl_cube_arith_description,sinfo_utl_cube_arith_description1);

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_utl_cube_arith",
                    "Cube arithmetics",
                    sinfo_utl_cube_arith_description,
                    "Andrea Modigliani",
                    "Andrea.Modigliani@eso.org",
                    sinfo_get_license(),
                    sinfo_utl_cube_arith_create,
                    sinfo_utl_cube_arith_exec,
                    sinfo_utl_cube_arith_destroy) ;

    cpl_pluginlist_append(list, plugin) ;

    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using 
  the interface. 
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube_arith_create(cpl_plugin * plugin)
{
    cpl_recipe      * recipe ;
    cpl_parameter   * p ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;
    cpl_error_reset();
    irplib_reset();
    /* Create the parameters list in the cpl_recipe object */
    recipe->parameters = cpl_parameterlist_new() ; 

    /* Fill the parameters list */
    /* --stropt */
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_arith.op", 
                    CPL_TYPE_STRING,
                    "A possible operation: "
                    "`/','*','+' or `-'",
                    "sinfoni.sinfo_utl_cube_arith","/");
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "op") ;
    cpl_parameterlist_append(recipe->parameters, p) ;


    /* --doubleopt */
    /*
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_arith.temperature", 
                                 CPL_TYPE_DOUBLE, "Black Body Temperature", 
                                 "sinfoni.sinfo_utl_cube_arith", 100000.) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "temp") ;
    cpl_parameterlist_append(recipe->parameters, p) ;
     */

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_cube_arith.value", 
                    CPL_TYPE_DOUBLE, "A constant to add",
                    "sinfoni.sinfo_utl_cube_arith", 99999.0) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "value") ;
    cpl_parameterlist_append(recipe->parameters, p) ;

    /* Return */
    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube_arith_exec(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;
    int code=0;
    cpl_errorstate initial_errorstate = cpl_errorstate_get();

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;
    sinfo_msg("Welcome to SINFONI Pipeline release %d.%d.%d",
              SINFONI_MAJOR_VERSION,SINFONI_MINOR_VERSION,SINFONI_MICRO_VERSION);

    code = sinfo_utl_cube_arith(recipe->parameters, recipe->frames) ;

    if (!cpl_errorstate_is_equal(initial_errorstate)) {                      
        /* Dump the error history since recipe execution start.                
           At this point the recipe cannot recover from the error */           
        cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);              
    } 
    return code;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_cube_arith_destroy(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_parameterlist_delete(recipe->parameters) ; 
    return 0 ;
}
/**@}*/
