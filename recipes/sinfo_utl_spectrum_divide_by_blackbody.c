/* $Id: sinfo_utl_spectrum_divide_by_blackbody.c,v 1.10 2007-10-26 09:40:28 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2007-10-26 09:40:28 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

/* cpl */
#include <cpl.h>

/* irplib */
#include <irplib_utils.h>
#include <string.h>
#include <sinfo_tpl_utils.h>
#include <sinfo_pfits.h>
#include <sinfo_tpl_dfs.h>
#include <sinfo_msg.h>
#include <sinfo_utl_spectrum_divide_by_blackbody.h>
/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/

static int sinfo_utl_spectrum_divide_by_blackbody_create(cpl_plugin *) ;
static int sinfo_utl_spectrum_divide_by_blackbody_exec(cpl_plugin *) ;
static int sinfo_utl_spectrum_divide_by_blackbody_destroy(cpl_plugin *) ;

/*-----------------------------------------------------------------------------
                            Static variables
 ----------------------------------------------------------------------------*/

static char sinfo_utl_spectrum_divide_by_blackbody_description1[] =
                "This recipe divides a spectrum by a black body "
                "spectrum of given temperature.\n"
                "The input file is a spectrum. Its associated tag must be SPECTRUM.\n"
                "The output is a spectrum\n";


static char sinfo_utl_spectrum_divide_by_blackbody_description2[] =
                "Parameter is \n"
                "sinfoni.sinfo_utl_spectrum_divide_by_blackbody.temperature\n"
                "having aliases 'temp' \n"
                "\n";

static char sinfo_utl_spectrum_divide_by_blackbody_description[900];

/*-----------------------------------------------------------------------------
                                Functions code
 ----------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_utl_spectrum_divide_by_blackbody  Recipe to correct a \
   spectrum from the blackbody thermal emission 
 */
/*---------------------------------------------------------------------------*/
/**@{*/

/*---------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module. 
  @param    list    the plugin list
  @return   0 if everything is ok

  This function is exported.
 */
/*---------------------------------------------------------------------------*/
int cpl_plugin_get_info(cpl_pluginlist * list)
{
    cpl_recipe  *   recipe = cpl_calloc(1, sizeof *recipe ) ;
    cpl_plugin  *   plugin = &recipe->interface ;

    strcpy(sinfo_utl_spectrum_divide_by_blackbody_description,
           sinfo_utl_spectrum_divide_by_blackbody_description1);
    strcat(sinfo_utl_spectrum_divide_by_blackbody_description,
           sinfo_utl_spectrum_divide_by_blackbody_description2);

    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_utl_spectrum_divide_by_blackbody",
                    "Spectrum normalization by a blackbody",
                    sinfo_utl_spectrum_divide_by_blackbody_description,
                    "Andrea Modigliani",
                    "Andrea.Modigliani@eso.org",
                    sinfo_get_license(),
                    sinfo_utl_spectrum_divide_by_blackbody_create,
                    sinfo_utl_spectrum_divide_by_blackbody_exec,
                    sinfo_utl_spectrum_divide_by_blackbody_destroy) ;

    cpl_pluginlist_append(list, plugin) ;

    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    plugin  the plugin
  @return   0 if everything is ok

  Create the recipe instance and make it available to the application using 
  the interface. 
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_spectrum_divide_by_blackbody_create(cpl_plugin * plugin)
{
    cpl_recipe      * recipe ;
    cpl_parameter   * p ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_error_reset();
    irplib_reset();
    /* Create the parameters list in the cpl_recipe object */
    recipe->parameters = cpl_parameterlist_new() ; 

    /* Fill the parameters list */
    /* --stropt */
    /* --doubleopt */
    p = cpl_parameter_new_value("sinfoni.sinfo_utl_spectrum_divide_by_blackbody.temperature", 
                    CPL_TYPE_DOUBLE, "Black Body Temperature",
                    "sinfoni.sinfo_utl_spectrum_divide_by_blackbody", 100000.) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "temp") ;
    cpl_parameterlist_append(recipe->parameters, p) ;

    /* Return */
    return 0;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_spectrum_divide_by_blackbody_exec(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;
    int code=0;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    sinfo_msg("Welcome to SINFONI Pipeline release %d.%d.%d",
              SINFONI_MAJOR_VERSION,SINFONI_MINOR_VERSION,SINFONI_MICRO_VERSION);
    code=sinfo_utl_spectrum_divide_by_blackbody(recipe->parameters, 
                    recipe->frames) ;
    return code;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
static int sinfo_utl_spectrum_divide_by_blackbody_destroy(cpl_plugin * plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_parameterlist_delete(recipe->parameters) ; 
    return 0 ;
}

/**@}*/
