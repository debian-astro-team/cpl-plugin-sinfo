/* $Id: sinfo_rec_detlin.c,v 1.21 2008-02-04 17:23:02 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2008-02-04 17:23:02 $
 * $Revision: 1.21 $
 * $Name: not supported by cvs2svn $
 */

/**
 * @defgroup sinfo_rec_detlin Recipe to determine detector linearity
 */
/*---------------------------------------------------------------------------*/
/**@{*/

/****************************************************************
 *           Bad pixel search  (normal method)                  *
 ****************************************************************/

/* --------------------------------------------------------------- 
                         INCLUDES
   --------------------------------------------------------------- */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ----------------------------------------------------------------------------*/

/* std libraries */
#include <strings.h>
#include <string.h>
#include <stdio.h>

/* cpl */
#include <cpl.h>

/* irplib */
#include <irplib_utils.h>


/* sinfoni */
#include <sinfo_utilities.h>
#include <sinfo_general_config.h>
#include <sinfo_bp_lin_config.h>
#include <sinfo_bp_config.h>
#include <sinfo_bp_lin.h>
#include <sinfo_product_config.h>

#include <sinfo_bp_lin.h> 
#include <sinfo_tpl_utils.h>
#include <sinfo_functions.h>
#include <sinfo_tpl_dfs.h>
#include <sinfo_msg.h>
#include <sinfo_error.h>
#include <sinfo_utils_wrappers.h>

/*---------------------------------------------------------------------------
                            Functions prototypes
 ---------------------------------------------------------------------------*/


static int sinfo_rec_detlin_create(cpl_plugin *plugin);
static int sinfo_rec_detlin_exec(cpl_plugin *plugin);
static int sinfo_rec_detlin_destroy(cpl_plugin *plugin);
static int sinfo_rec_detlin(cpl_parameterlist *, cpl_frameset *);

/*----------------------------------------------------------------------------
                            Static variables
 ----------------------------------------------------------------------------*/

static char sinfo_rec_detlin_description[] =
                "This recipe computes detector non linearities and a bad pixel map.\n"
                "The input files are increasing intensity raw flats\n"
                "their associated tags should be LINEARITY_LAMP.\n"
                "The output are: \n"
                "A table (PRO.CATG=LIN_DET_INFO) with information \n"
                "on the detector non linearities\n"
                "A table (PRO.CATG=GAIN_INFO) with information on the detector gain\n"
                "A cube (PRO.CATG=BP_COEFF) with the polynomial fit parameters \n"
                "of the detector non linearities\n"
                "A bad pixel map (PRO.CATG=BP_MAP_NL)\n"
                "\n";
/*---------------------------------------------------------------------------
                                Functions code
 ---------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/**
  @brief    Build the list of available plugins, for this module. 
  @param    list    the plugin list
  @return   0 if everything is ok, -1 otherwise

  Create the recipe instance and make it available to the application using the 
  interface. This function is exported.
 */
/*--------------------------------------------------------------------------*/


int cpl_plugin_get_info(cpl_pluginlist *list)
{

    cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe);
    cpl_plugin *plugin = &recipe->interface;


    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_rec_detlin",
                    "Detector's linearity & non linear bad pixels determination.",
                    sinfo_rec_detlin_description,
                    "Andrea Modigliani",
                    "Andrea.Modigliani@eso.org",
                    sinfo_get_license(),
                    sinfo_rec_detlin_create,
                    sinfo_rec_detlin_exec,
                    sinfo_rec_detlin_destroy);

    cpl_pluginlist_append(list, plugin);

    return 0;

}

/*--------------------------------------------------------------------------*/
/**
  @brief    Setup the recipe options    
  @param    plugin  the plugin
  @return   0 if everything is ok

  Defining the command-line/configuration parameters for the recipe.
 */
/*--------------------------------------------------------------------------*/
static int sinfo_rec_detlin_create(cpl_plugin *plugin)
{

    /*
     * We have to provide the option we accept to the application.
     * We need to setup our parameter list and hook it into the recipe
     * interface.
     */


    cpl_recipe      * recipe ;

    /* Check that the plugin is part of a valid recipe */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_error_reset();
    irplib_reset();
    /* Create the parameters list in the cpl_recipe object */
    recipe->parameters = cpl_parameterlist_new() ; 

    sinfo_general_config_add(recipe->parameters);
    sinfo_product_config_add(recipe->parameters);
    sinfo_bp_lin_config_add(recipe->parameters); 

    return 0;

}

/*--------------------------------------------------------------------------*/
/**
  @brief    Execute the plugin instance given by the interface
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*--------------------------------------------------------------------------*/

static int sinfo_rec_detlin_exec(cpl_plugin *plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    cpl_errorstate initial_errorstate = cpl_errorstate_get();

    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    if (!cpl_errorstate_is_equal(initial_errorstate)) {                      
        /* Dump the error history since recipe execution start.                
           At this point the recipe cannot recover from the error */           
        cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);              
    } 

    return sinfo_rec_detlin(recipe->parameters, recipe->frames);

}
/*--------------------------------------------------------------------------*/
/**
  @brief    Destroy what has been created by the 'create' function
  @param    plugin  the plugin
  @return   0 if everything is ok
 */
/*--------------------------------------------------------------------------*/

static int sinfo_rec_detlin_destroy(cpl_plugin *plugin)
{
    cpl_recipe  *   recipe ;

    /* Get the recipe out of the plugin */
    if (cpl_plugin_get_type(plugin) == CPL_PLUGIN_TYPE_RECIPE) 
        recipe = (cpl_recipe *)plugin ;
    else return -1 ;

    cpl_parameterlist_delete(recipe->parameters) ; 

    return 0;

}

/*
 * The actual recipe actually start here.
 */
/*---------------------------------------------------------------------------*/
/**
  @brief    Interpret the command line options and execute the data processing
  @param    parlist     the parameters list
  @param    frameset   the frames list
  @return   0 if everything is ok

 This recipe computes detector non linearities and a bad pixel map.
 The input files are increasing intensity raw flats their associated tags 
 should be LINEARITY_LAMP.

 The output are:

 A table (PRO.CATG=LIN_DET_INFO) with information on the detector non 
 linearities
 A table (PRO.CATG=GAIN_INFO) with information on the detector gain
 A cube (PRO.CATG=BP_COEFF) with the polynomial fit parameters of the detector 
 non linearities
 A bad pixel map (PRO.CATG=BP_MAP_NL)

 */
/*--------------------------------------------------------------------------*/

static int sinfo_rec_detlin(cpl_parameterlist *config, cpl_frameset *set)
{
    cpl_parameter *p=NULL;
    int line_cor=0;

    sinfo_msg("Welcome to SINFONI Pipeline release %d.%d.%d",
              SINFONI_MAJOR_VERSION,SINFONI_MINOR_VERSION,SINFONI_MICRO_VERSION);
    /* Hidden parameters */
    /* name of the data cube storing the found polynomial coefficients */
    sinfo_bp_config_add(config);
    check_nomsg(p = cpl_parameterlist_find(config,"sinfoni.bp.method"));
    check_nomsg(cpl_parameter_set_string(p,"Linear"));

    ck0(sinfo_dfs_set_groups(set),"Cannot indentify RAW and CALIB frames") ;

    check_nomsg(p=cpl_parameterlist_find(config, "sinfoni.general.lc_sw"));
    check_nomsg(line_cor=cpl_parameter_get_bool(p));
    if(line_cor==1) {
        check_nomsg(sinfo_ima_line_cor(config,set));
    }


    sinfo_msg("---------------------------------------");
    sinfo_msg("BP_MAP_NL BAD PIXEL MAP DETERMINATION  ");
    sinfo_msg("---------------------------------------");
    ck0(sinfo_new_bp_search_lin(cpl_func,config,set),
        "BP_MAP_NL BAD PIXEL MAP DETERMINATION FAILED") ;
    sinfo_msg("BP_MAP_NL BAD PIXEL MAP DETERMINATION SUCCESS") ;

    cleanup:

    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        return -1;
    } else {
        return 0;
    }

}

/**@}*/
