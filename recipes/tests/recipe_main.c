/* $Id: recipe_main.c,v 1.5 2009-09-15 08:13:38 amodigli Exp $
 *
 * This file is part of the VISIR Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2009-09-15 08:13:38 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

#include <irplib_plugin.h>
#include <cpl_test.h>
#include <sinfo_pro_types.h>
#include <sinfo_msg.h>
#include <sinfo_raw_types.h>


/*----------------------------------------------------------------------------*/
/**
 * @defgroup recipe_main   General plugin tests
 */
/*----------------------------------------------------------------------------*/

/**@{*/

/*-----------------------------------------------------------------------------
                            Function definitions
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
  @brief    Find a plugin and submit it to some tests
  @return   0 iff succesful

 */
/*----------------------------------------------------------------------------*/
int main(void)
{

    const char * tags[] = {
       RAW_LINEARITY_LAMP,
       RAW_DARK,
       RAW_PINHOLE_LAMP,
       RAW_SLIT_LAMP,
       RAW_FIBRE_PSF,
       RAW_FIBRE_DARK,
       RAW_FIBRE_LAMP,
       RAW_FIBRE_NS,
       RAW_FIBRE_EW,
       RAW_WAVE_LAMP,
       RAW_WAVE_LAMP_DITHER,
       RAW_WAVE_NS,
       RAW_WAVE_NS_DITHER,
       RAW_FLAT_LAMP,
       RAW_FLAT_LAMP_DITHER,
       RAW_FLAT_NS,
       RAW_FLAT_NS_DITHER,
       RAW_FLAT_SKY,
       RAW_FLUX_LAMP,
       RAW_PSF_CALIBRATOR,
       RAW_FOCUS,
       RAW_SKY_DUMMY,
       RAW_PUPIL_LAMP,
       RAW_OBJECT,
       RAW_IMAGE_PRE_OBJECT,
       RAW_IMAGE_PRE_SKY,
       RAW_OBJECT_SKYSPIDER,
       RAW_OBJECT_SKYSPIDER_DITHER,
       RAW_OBJECT_NODDING,
       RAW_SKY_NODDING,
       RAW_OBJECT_JITTER,
       RAW_SKY_JITTER,
       RAW_OBJECT_NODDING_DITHER,
       RAW_OBJECT_JITTER_DITHER,
       RAW_SKY_NODDING_DITHER,
       RAW_SKY_JITTER_DITHER,
       RAW_ACQUISITION_SKY,
       RAW_ACQUISITION_OBJECT,
       RAW_STD,
       RAW_STACKED_SLITPOS,
       RAW_SKY_STD,
       RAW_SKY_OH,
       RAW_SKY_PSF_CALIBRATOR,
       RAW_STD_STAR,
       RAW_STD_STAR_DITHER,
       SINFO_UTL_STDSTARS_RAW,
       SINFO_UTL_SEDS_RAW,
       RAW_OFF,
       RAW_OFF1,
       RAW_OFF2,
       RAW_SKY,
       RAW_SKY1,
       RAW_SKY2,
       RAW_ON,
       RAW_ON1,
       RAW_ON2,
       RAW_INT_ON,
       RAW_INT_OFF,
       PRO_LIN_DET_INFO,
       PRO_GAIN_INFO,
       PRO_AO_INFO,
       PRO_AO_PERFORMANCE,
       PRO_ENC_ENERGY,
       PRO_OBS_SKY,
       PRO_SKY_MED,
       PRO_SKY_DUMMY,
       PRO_SKY_STACKED_DUMMY,
       PRO_DEFAULT,
       PRO_INT_COL_TILT_COR,
       PRO_STD_STACKED,
       PRO_SKY_STD_STACKED,
       PRO_SKY_OH_STACKED,
       PRO_SKY_PSF_CALIBRATOR_STACKED,
       PRO_STD_STAR_STACKED,
       PRO_STD_STAR_DITHER_STACKED,
       PRO_SKY_STACKED,
       PRO_NODDING_STACKED,
       PRO_ILLUMCORR,
       SINFO_UTL_STDSTARS_RES,
       SINFO_UTL_SEDS_RES,
       SINFO_CALIB_STDSTARS,
       SINFO_CALIB_SED,
       PRO_STACKED,
       PRO_STACK_SKY_DIST,
       PRO_STACK_MFLAT_DIST,
       PRO_STACK_MFLAT_DITHER_DIST,
       PRO_MFLAT_CUBE,
       PRO_MFLAT_AVG,
       PRO_MFLAT_MED,
       PRO_ILL_COR,
       PRO_BP_MAP,
       PRO_BP_MAP_HP,
       PRO_BP_MAP_NL,
       PRO_BP_MAP_NO,
       PRO_BP_MAP_DI,
       PRO_BP_MAP_SKY,
       PRO_MASTER_BP_MAP,
       PRO_BP_MAP,
       PRO_MASTER_DARK,
       PRO_SLOPE,
       PRO_DISTORTION,
       PRO_SLITLETS_DISTANCE,
       PRO_MASTER_SLIT,
       PRO_MASTER_FLAT_LAMP,
       PRO_MASTER_FLAT_LAMP1,
       PRO_MASTER_FLAT_LAMP2,
       PRO_SLIT_POS,
       PRO_SLITLETS_POS_PREDIST,
       PRO_SLIT_POS_GUESS,
       PRO_FIBRE_EW_STACKED,
       PRO_FIBRE_NS_STACKED_ON,
       PRO_FIBRE_NS_STACKED_OFF,
       PRO_FIBRE_NS_STACKED,
       PRO_FIBRE_NS_STACKED_DIST,
       PRO_FIBRE_LAMP_STACKED,
       PRO_SLIT_LAMP_STACKED,
       PRO_FLUX_LAMP_STACKED,
       PRO_WAVE_LAMP_STACKED,
       PRO_WAVE_SLITPOS_STACKED,
       PRO_WAVE_LAMP_DITHER_STACKED,
       PRO_WAVE_NS_STACKED,
       PRO_WAVE_NS_DITHER_STACKED,
       PRO_WAVE_PAR_LIST,
       PRO_WAVE_COEF_SLIT,
       PRO_PSF_CALIBRATOR_STACKED,
       PRO_FOCUS_STACKED,
       PRO_OBJECT_NODDING_STACKED,
       PRO_OBJECT_SKYSPIDER_STACKED,
       PRO_RESAMPLED_WAVE,
       PRO_RESAMPLED_OBJ,
       PRO_RESAMPLED_SKY,
       PRO_RESAMPLED_FLAT_LAMP,
       PRO_OBS_CUBE_SKY,
       PRO_STD_CUBE_SKY,
       PRO_PSF_CUBE_SKY,
       PRO_PUPIL_CUBE_SKY,
       PRO_PUPIL_CUBE,
       PRO_OBS_MED_SKY,
       PRO_STD_MED_SKY,
       PRO_PSF_MED_SKY,
       PRO_PUPIL_MED_SKY,
       PRO_PUPIL_LAMP_STACKED,
       PRO_SKY_NODDING_STACKED,
       PRO_STD_NODDING_STACKED,
       PRO_MASTER_LAMP_SPEC,
       PRO_MASTER_TWIFLAT,
       PRO_COEFF_LIST,
       PRO_INDEX_LIST,
       PRO_HALO_SPECT,
       PRO_FIRST_COL,
       PRO_MASK_CUBE,
       PRO_PSF,
       TMP_FOCUS,
       TMP_FOCUS_ON,
       TMP_FOCUS_OFF,
       PRO_FOCUS,
       PRO_FOCUS_GAUSS,
       PRO_WAVE_MAP,
       PRO_STD_STAR_SPECTRA,
       PRO_STD_STAR_SPECTRUM,
       PRO_CUBE,
       PRO_IMA,
       PRO_SPECTRUM,
       PRO_COADD_SKY,
       PRO_COADD_PSF,
       PRO_COADD_STD,
       PRO_COADD_OBJ,
       PRO_COADD_PUPIL,
       PRO_OBS_PSF,
       PRO_OBS_STD,
       PRO_OBS_OBJ,
       PRO_OBS_PUPIL,
       PRO_SPECTRA_QC,
       PRO_MED_COADD_PSF,
       PRO_MED_COADD_STD,
       PRO_MED_COADD_OBJ,
       PRO_MED_COADD_PUPIL,
       PRO_MED_OBS_PSF,
       PRO_MED_OBS_STD,
       PRO_MED_OBS_OBJ,
       PRO_MED_OBS_PUPIL,
       PRO_CUBE_COLL,
       PRO_SLOPEX,
       PRO_SLOPEY,
       PRO_MASK_CUBE,
       PRO_MASK_COADD_PSF,
       PRO_MASK_COADD_STD,
       PRO_MASK_COADD_OBJ,
       PRO_MASK_COADD_PUPIL,
       PRO_OBJ_CUBE,
       PRO_BP_COEFF
    };

    cpl_pluginlist * pluginlist;
    const size_t ntags = sizeof(tags) / sizeof(char*);
    cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
    pluginlist = cpl_pluginlist_new();
    
    sinfo_msg("Hello wordl!");


    cpl_test(!cpl_plugin_get_info(pluginlist));

    cpl_test(!irplib_plugin_test(pluginlist, ntags, tags));

    cpl_pluginlist_delete(pluginlist);

    return cpl_test_end(0);
}

/**@}*/
