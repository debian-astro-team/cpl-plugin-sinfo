/* $Id: sinfo_utl_bp_mask_add.c,v 1.18 2008-08-21 09:46:47 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2008-08-21 09:46:47 $
 * $Revision: 1.18 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *           Bad pixel search  (normal method)                  *
 ****************************************************************/

/* --------------------------------------------------------------- 
                         INCLUDES
   --------------------------------------------------------------- */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* std libraries */
#include <strings.h>
#include <string.h>
#include <stdio.h>

/* cpl */
#include <cpl.h>     
/* irplib */
#include <irplib_utils.h>

/* sinfoni */
#include <sinfo_general_config.h>
#include <sinfo_bp_norm_config.h>
#include <sinfo_bp_lin_config.h>
#include <sinfo_bp_noise_config.h>
#include <sinfo_new_add_bp_map.h>
#include <sinfo_tpl_utils.h>
#include <sinfo_tpl_dfs.h>
#include <sinfo_msg.h>
#include <sinfo_error.h>
#include <sinfo_utils_wrappers.h>

/* --------------------------------------------------------------- 
                         DEFINES
   --------------------------------------------------------------- */
/* --------------------------------------------------------------- 
                         FUNCTIONS PROTOTYPES
   --------------------------------------------------------------- */

const char * sinfoni_get_licence(void);
static int sinfo_utl_bp_mask_add_create(cpl_plugin *plugin);
static int sinfo_utl_bp_mask_add_exec(cpl_plugin *plugin);
static int sinfo_utl_bp_mask_add_destroy(cpl_plugin *plugin);
static int sinfo_utl_bp_mask_add(cpl_parameterlist *, cpl_frameset *);

/* --------------------------------------------------------------- 
                         STATIC VARIABLES
   --------------------------------------------------------------- */

static char sinfo_utl_bp_mask_add_description[] =
                "This recipe performs bad pixel map coaddition.\n"
                "The input files are several (at least 2)  bad pixel masks in the sof file\n"
                "Their tab should contain the string BP_MAP.\n"
                "The output is an image resulting from the logical operator OR \n"
                "applied to all the masks.\n"
                "\n";


/* --------------------------------------------------------------- 
                         FUNCTIONS CODE
   --------------------------------------------------------------- */

/*---------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_utl_bp_mask_add  Recipe to coadd bad pixel masks
 */
/*---------------------------------------------------------------------------*/
/**@{*/
/* --------------------------------------------------------------- */

int
cpl_plugin_get_info(cpl_pluginlist *list)
{

    cpl_recipe *recipe = cpl_calloc(1, sizeof *recipe);
    cpl_plugin *plugin = &recipe->interface;


    cpl_plugin_init(plugin,
                    CPL_PLUGIN_API,
                    SINFONI_BINARY_VERSION,
                    CPL_PLUGIN_TYPE_RECIPE,
                    "sinfo_utl_bp_mask_add",
                    "Add bad pixels masks",
                    sinfo_utl_bp_mask_add_description,
                    "Andrea Modigliani",
                    "Andrea.Modigliani@eso.org",
                    sinfo_get_license(),
                    sinfo_utl_bp_mask_add_create,
                    sinfo_utl_bp_mask_add_exec,
                    sinfo_utl_bp_mask_add_destroy);

    cpl_pluginlist_append(list, plugin);

    return 0;

}

/* --------------------------------------------------------------- */
/**
  @brief   Set up the recipe options
  @param   plugin   the plugin
  @return  0 if everything is ok
 */

static int
sinfo_utl_bp_mask_add_create(cpl_plugin *plugin)
{

    /*
     * We have to provide the option we accept to the application.
     * We need to setup our parameter list and hook it into the recipe
     * interface.
     */
    cpl_recipe *recipe = (cpl_recipe *)plugin;
    recipe->parameters = cpl_parameterlist_new();
    if(recipe->parameters == NULL) {
        return 1;
    }
    cpl_error_reset();
    irplib_reset();

    /*
     * Fill the parameter list.
     */


    return 0;

}

/* --------------------------------------------------------------- */
/**
  @brief   Executes the plugin instance given by the interface
  @param   plugin   the plugin
  @return  0 if everything is ok
 */
static int
sinfo_utl_bp_mask_add_exec(cpl_plugin *plugin)
{


    cpl_recipe *recipe = (cpl_recipe *) plugin;
    cpl_errorstate initial_errorstate = cpl_errorstate_get();
    int code=0;

    if(recipe->parameters == NULL ) {
        return 1;
    }
    if(recipe->frames == NULL) {
        return 1;
    }

    check_nomsg(code=sinfo_utl_bp_mask_add(recipe->parameters, recipe->frames));

    if (!cpl_errorstate_is_equal(initial_errorstate)) {
        /* Dump the error history since recipe execution start.
       At this point the recipe cannot recover from the error */           
        cpl_errorstate_dump(initial_errorstate, CPL_FALSE, NULL);
    }
    cleanup:

    return code;

}

/* --------------------------------------------------------------- */
/**
  @brief   Destroy what has been created by the 'create' function
  @param   plugin   the plugin
  @return  0 if everything is ok
 */

static int
sinfo_utl_bp_mask_add_destroy(cpl_plugin *plugin)
{
    cpl_recipe *recipe = (cpl_recipe *) plugin;
    /*
     * We just destroy what was created during the plugin initializzation phase
     * i.e. the parameter list. The frame set is managed by the application which
     * called us, so that we must not touch it.
     */

    cpl_parameterlist_delete(recipe->parameters);
    return 0;

}

/*
 * The actual recipe actually start here.
 */


/* --------------------------------------------------------------- */
/**
  @brief   Do bad pixel search. Monitor parameters. Do something

  @param   config   the parameter list
  @param   set      the frames list
  @return  0 if everything is ok

  The parameter list must contain:

  The parameters are used to combine the input images and generate
  an output image which is saved on disk.

 */
static int
sinfo_utl_bp_mask_add(cpl_parameterlist *config, cpl_frameset *sof)
{
    cpl_frameset* ref_set=NULL;
    int n=0;

    sinfo_msg("Welcome to SINFONI Pipeline release %d.%d.%d",
              SINFONI_MAJOR_VERSION,SINFONI_MINOR_VERSION,SINFONI_MICRO_VERSION);

    if(sinfo_dfs_set_groups(sof)) {
        sinfo_msg_error("Cannot indentify RAW and CALIB frames") ;
    }

    n=cpl_frameset_get_size(sof);
    if(n<1) {
        sinfo_msg_error("Empty input frame list!");
        goto cleanup ;
    }

    check_nomsg(ref_set=cpl_frameset_duplicate(sof));
    ck0_nomsg(sinfo_new_add_bp_map(cpl_func,config,sof,ref_set));

    cleanup:
    sinfo_free_frameset(&ref_set);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        return -1;
    } else {
        return 0;
    }



}
/**@}*/
