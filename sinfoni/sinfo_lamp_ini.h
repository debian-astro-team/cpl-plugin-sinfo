/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   File name    :   sinfo_lamp_ini.h
   Author       :    Juergen Schreiber
   Created on   :    Mar 08, 2002
   Description  :    preparing lamp ini file handling for SPIFFI
 ---------------------------------------------------------------------------*/
#ifndef SINFO_LAMP_INI_H
#define SINFO_LAMP_INI_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_lamp_cfg.h"
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/
/**
  @name        generateLamp_ini_file
  @memo        Generate a default ini file for the preparing 
                of halogen lamp spectrum command.
  @param    ini_name    Name of the file to generate.
  @param    name_i        Name of the input file.
  @param    name_o        Name of the output file.
  @return    int 0 if Ok, -1 otherwise.
  @doc          This function generates a default ini file for the preparing 
                of flatfield frames command. The generated file will have the 
                requested name. 
 */
/*--------------------------------------------------------------------------*/
int 
generateLamp_ini_file(
        char * ini_name,
        char * name_i,
        char * name_o,
        char * name_c
);

/**
  @name     parse_lamp_ini_file
  @memo     Parse a ini_name.ini file and create a blackboard.
  @param    ini_name    Name of the ASCII file to parse.
  @return   1 newly allocated lamp_config blackboard structure.
  @doc      The requested ini file is parsed and a blackboard object is 
            created, then updated accordingly. Returns NULL in case of error.
 */

lamp_config * 
parse_lamp_ini_file(char * ini_name) ;

#endif
