/*
 * This file is part of the ESO UVES Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2009-04-28 11:42:18 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifndef SINFO_BARYVEL_H
#define SINFO_BARYVEL_H

#include <cpl.h>

#define H_GEOLAT "ESO TEL GEOLAT"
#define H_GEOLON "ESO TEL GEOLON"
#define H_UTC "UTC"
cpl_error_code
sinfo_baryvel(const cpl_propertylist *raw_header,
         double *barycor,
         double *helicor);

#endif  /* SINFO_BARYVEL_H */
