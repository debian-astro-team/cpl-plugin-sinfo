/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_vltPort.h,v 1.3 2006-10-25 06:46:09 amodigli Exp $" 
*
* vltPort.h for Sun Solaris 2
*
* who        when      what
* --------  --------  ----------------------------------------------
* gfilippi  22/05/96  created form vltPort.h
* gfilippi  23/05/96  define SUN_COMP added
*
*/

/************************************************************************
*  vltPort.h - Include file to mask differences between platforms.
*              This file should be included in all source files.
*              It relies on macro definitions preceeding the
*              inclusion of this file.
*                
*  REMARK: This file belongs to the "vltMake" module.
*------------------------------------------------------------------------
*/

#ifndef SINFO_VLTPORT_H
#define SINFO_VLTPORT_H

/*
* When it is used, vltPort.h MUST be the very first file included
* in ANSI ".c" files.
* Cause a syntax error if we detect that any other include file has been
* included before vltPort.h in an ANSI ".c" file.
*/
#if defined(__STDC__) && \
                         (defined(_H_STANDARDS) || \
                          defined(_SYS_STDSYMS_INCLUDED) || \
                          defined(_STANDARDS_H_))
#    error "vltPort.h MUST BE THE VERY FIRST FILE INCLUDED IN ANSI '.c' FILES"
#endif

/*
 * This file is used also by some VxWorks code.
 * To be compatible with existing code, SUN_COMP is defined for both
 * gcc and cc68k, but the following definitiond do not influence cc68k
 */
 
#define SUN_COMP

/* 
 * at present, SELECT is defined in the code using it. It should be done here
 * for all. May be in the next release.
 */

/*
 * Adjust name-space information.
 */
#if defined(_ALL_SOURCE)
#    undef _POSIX_C_SOURCE
#endif

#if defined(_XOPEN_SOURCE)
#    undef _POSIX_C_SOURCE
#endif

#ifndef MAKE_VXWORKS
#include <stddef.h>
#include <sys/types.h>
#include <sys/time.h>

#if !defined(timercmp)
/*
AMO: 03/09/03 commented out for Linux
    struct timeval
        {
        long      tv_sec;         
        long      tv_usec;        
        };
*/
#define crTIMEVAL_TIMEZONE_DEFINED
#endif
#endif /* MAKE_VXWORKS */

#endif /*!SINFO_VLTPORT_H*/
