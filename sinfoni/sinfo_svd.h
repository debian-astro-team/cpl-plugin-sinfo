#ifndef SINFO_SVD_H
#define SINFO_SVD_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_svd.h,v 1.4 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  16/04/03  created
*/

/************************************************************************
 * sinfo_svd.h
 * singular value decomposition fit routines
 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <math.h>
#include <cpl.h>

/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

void sinfo_fpol(float x, float *p, int np) ;

void 
sinfo_svb_kas(float **u, float w[], float **v, int m, 
              int n, float b[],float x[]) ;

void sinfo_svd_variance(float **v , int ma , float w[] , float **cvm) ;

void sinfo_svd_fitting ( float *x,
                   float *y,
                   float *sig,
                   int   ndata,
                   float *a,
                   int   ma,
                   float **u,
                   float **v,
                   float *w,
                   float **cvm,
                   float *chisq,
                   void (*funcs)(float,float *,int) ) ;

void sinfo_svd_compare(float **a,int m,int n,float w[],float **v) ;


float *sinfo_vector(long nl, long nh) ;

void sinfo_free_vector(float *v, long nl /*, long nh*/) ;

float **sinfo_matrix(long nrl, long nrh, long ncl, long nch) ;

void 
sinfo_free_matrix(float **m,long nrl/*, long nrh*/, long ncl/*, long nch*/) ;


#endif /*!SINFO_SVD_H*/

/*--------------------------------------------------------------------------*/
