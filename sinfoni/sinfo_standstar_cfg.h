/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------
   
   File name     :    sinfo_standstar_cfg.h
   Author    :    Juergen Schreiber
   Created on    :    March 2002
   Description    :    standstar_ini definitions + handling prototypes

 ---------------------------------------------------------------------------*/


#ifndef SINFO_STANDSTAR_CFG_H
#define SINFO_STANDSTAR_CFG_H

/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/

#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>


/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/


/*
  standard star data reduction blackboard container

  This structure holds all information related to the standard 
  star data reduction
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct standstar_config {
/*-------General---------*/
        char inFile[FILE_NAME_SZ]  ;/* input file of reduced jittered 
                                       data cubes of a standard star */
        char ** inFrameList ; /* input list of cubes */
        int     nframes ;         /* number of input frames */
        char outName[FILE_NAME_SZ] ; /* output name of resulting 
                                        extracted spectrum stored 
                                        as fits image */

/*------ spectral extraction and determination of conversion factor ------*/
        /* the fraction [0...1] of rejected low intensity pixels when 
           taking the average of jittered spectra */
        float lo_reject ;
        /* the fraction [0...1] of rejected high intensity pixels when 
           taking the average of jittered spectra */
        float hi_reject ;
        /* lower left sinfo_edge coordinates of fitting box for 
           2d Gaussian fit */
        int llx ;
        int lly ;
        /* size of a box inside which the 2D-Gaussian fit is carried through */
        int halfbox_x ;
        int halfbox_y ;
    
        /* factor applied to the found fwhms of a 2D-Gaussian
           fit, defines the radius of the aperture from which the
           spectral extraction is carried out (default: 0.7). */
        float fwhm_factor ;
        /* (readnoise^2 + sinfo_dark current) needed to determine the 
           noise variance of the background. Must be given in counts/sec. */
        float backvariance ;
        /* estimated sky counts/sec */
        float sky ;
        /* gain: counts per electron */
        float gain ;
        /* indicator if an intensity conversion factor should be 
           determined or not */
        int convInd ;      
        /* name of the ASCII file that stores the intensity conversion factor */
        char convName[FILE_NAME_SZ] ;      
        /* magnitude of the standard star */
        float mag ;      
} standstar_config ;



/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------
   Function :   sinfo_standstar_cfg_create()
   In       :   void
   Out      :   pointer to allocated base standstar_config structure
   Job      :   allocate memory for a standstar_config struct
   Notice   :   only the main (base) structure is allocated
 ---------------------------------------------------------------------------*/

standstar_config * sinfo_standstar_cfg_create(void);


/*---------------------------------------------------------------------------
   Function :   sinfo_standstar_cfg_destroy()
   In       :   standstar_config to deallocate
   Out      :   void
   Job      :   deallocate all memory associated with a 
                standstar_config data structure
   Notice   :   
 ---------------------------------------------------------------------------*/
void sinfo_standstar_cfg_destroy(standstar_config * cc);

#endif
