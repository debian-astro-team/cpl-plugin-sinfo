/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
#ifndef SINFO_DEFINE_OPT_H
#define SINFO_DEFINE_OPT_H
#define OPT_FILE        1001
#define OPT_GENERATE            1002
#define OPT_RB            1003
#define OPT_SORT        1004
#define OPT_IN            2000
#define OPT_OUT            2001
#define OPT_CALIB        2002
#define OPT_CIN         2003
#endif
