/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------

   File name     :   sinfo_object_cfg.c
   Author     :   Juergen Schreiber
   Created on    :   April 2002
   Description    :   configuration handling tools for the creation of an object
                    data cube

 *--------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_object_cfg.h"


/*---------------------------------------------------------------------------
                              Function codes
 ---------------------------------------------------------------------------*/
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Objnod structure configuration
 *
 * TBD
 */

/*---------------------------------------------------------------------------
   Function :   sinfo_object_cfg_create()
   In       :   void
   Out      :   pointer to allocated base object_config structure
   Job      :   allocate memory for an object_config struct
   Notice   :   only the main (base) structure is allocated
 ---------------------------------------------------------------------------*/

object_config * sinfo_object_cfg_create(void)
{
    return cpl_calloc(1, sizeof(object_config));
}


/*---------------------------------------------------------------------------
   Function :   sinfo_object_cfg_destroy()
   In       :   object_config to deallocate
   Out      :   void
   Job      :   deallocate all memory associated with an 
                object_config data structure
   Notice   :   
 ---------------------------------------------------------------------------*/

void sinfo_object_cfg_destroy(object_config * cc)
{
    if (cc==NULL) return ;

    /* Free main struct */
    cpl_free(cc);

    return ;
}

/**@}*/


