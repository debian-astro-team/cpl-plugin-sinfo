/* $Id: sinfo_product_config.c,v 1.7 2012-03-03 09:50:08 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 09:50:08 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *           Bad pixel search  (normal method)                  *
 ****************************************************************/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_product_config.h"

/**@{*/
/**
 * @defgroup sinfo_produc_config.c Pipeline products configurations
 *
 * TBD
 */

/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* General data reduction parameters */

void
sinfo_product_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /* indicates if parameters will be overwritten */
    p = cpl_parameter_new_enum("sinfoni.product.density",
                    CPL_TYPE_INT,
                    "Density of pipeline products: "
                    "0 (low), 1 (low+skycor), 2 (med-QC), "
                    "3 (high-debug+skycor)",
                    "sinfoni.product",
                    2,4,0,1,2,3);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "product-density");
    cpl_parameterlist_append(list, p);


    return;

}
/**@}*/
