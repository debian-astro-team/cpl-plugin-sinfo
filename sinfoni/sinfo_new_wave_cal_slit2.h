#ifndef SINFO_NEW_WAVE_CAL_SLIT2_H
#define SINFO_NEW_WAVE_CAL_SLIT2_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_wave_cal_slit2.h,v 1.7 2007-09-21 14:13:43 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  17/09/03  created
*/

/************************************************************************
 * sinfo_new_wave_cal_slit2.h

  Normal method:

  does the wavelength calibration and the fitting of the slitlet sinfo_edge 
  positions (ASCII file 32 x 2 values) if wished
  produces an array of the bcoefs and of the fit parameters if wished and a 
  wavelength calibration map input is an emission line frame and a line list

  o searching for lines by cross sinfo_correlation with a line list
  o Gaussian fitting of emission lines in each column->positions of the lines->
    resulting fit parameters can be stored in an ASCII file
  o Fitting of a polynomial to the line positions for each column
  o Smoothing: fitting of each polynomial coefficient by another polynomial
    across the whole frame -> resulting polynomial coefficients can be stored 
    in an ASCII file.
  o Wavelength calibration map (micron value for each frame pixel) can be
    produced by using these coefficients and a cross sinfo_correlation to the
    original frame

  o The slitlet sinfo_edge positions can be fitted:
    1) Automatically (not really stable) or by using guess sinfo_edge positions
    2) By using a Boltzmann or a linear slope function


  Slit method:

  does the wavelength calibration and the fitting of the slitlet sinfo_edge 
  positions (ASCII file 32 x 2 values) if wished produces a list of the fit 
  parameters and of the smoothed coefficients if wished and a wavelength 
  calibration map input is an emission line frame and a line list

  o Does the same as other method but smoothes the found polynomial 
    coefficients within each slitlet and not over the whole frame.

  o Produces always a wavelength calibration map and does not crosscorrelate.

 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <cpl.h>   
#include "sinfo_msg.h"

/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Function     :       sinfo_new_wave_cal_slit2()
   In           :       ini_file: file name of according .ini file
   Out          :       integer (0 if it worked, -1 if it doesn't) 
   Job          :


  Normal method:

  does the wavelength calibration and the fitting of the slitlet sinfo_edge 
  positions (ASCII file 32 x 2 values) if wished
  produces an array of the bcoefs and of the fit parameters if wished and a 
  wavelength calibration map input is an emission line frame and a line list


  o searching for lines by cross sinfo_correlation with a line list
  o Gaussian fitting of emission lines in each column->positions of the lines->
    resulting fit parameters can be stored in an ASCII file
  o Fitting of a polynomial to the line positions for each column
  o Smoothing: fitting of each polynomial coefficient by another polynomial
    across the whole frame -> resulting polynomial coefficients can be stored 
    in an ASCII file.
  o Wavelength calibration map (micron value for each frame pixel) can be
    produced by using these coefficients and a cross sinfo_correlation to the
    original frame

  o The slitlet sinfo_edge positions can be fitted:
    1) Automatically (not really stable) or by using guess sinfo_edge positions
    2) By using a Boltzmann or a linear slope function

  Slit method:

  does the wavelength calibration and the fitting of the slitlet sinfo_edge 
  positions (ASCII file 32 x 2 values) if wished produces a list of the fit 
  parameters and of the smoothed coefficients if wished and a wavelength 
  calibration map input is an emission line frame and a line list

  o Does the same as other method but smoothes the found polynomial 
    coefficients within each slitlet and not over the whole frame.

  o Produces always a wavelength calibration map and does not crosscorrelate.
 ---------------------------------------------------------------------------*/
int 
sinfo_new_wave_cal_slit2 (const char* plugin_id,
                          cpl_parameterlist* config, 
                          cpl_frameset* sof,cpl_frameset* ref_set) ;


#endif 

/*--------------------------------------------------------------------------*/
