/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_detnoise_ini.h
   Author       :    Juergen Schreiber
   Created on   :    September 3, 2002
   Description  :   prepare handling of .ini file for the search for static 
                        bad pixels
 ---------------------------------------------------------------------------*/
#ifndef SINFO_DETNOISE_INI_BY_CPL_H
#define SINFO_DETNOISE_INI_BY_CPL_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_detnoise_cfg.h"
#include "sinfo_msg.h" 
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/**
  @name     sinfo_parse_cpl_input_detnoise
  @memo     Parse an ini_name.ini file and create a blackboard.
  @param    cpl_cfg pointer to parameterlist
  @param    sof     pointer to set of frame
  @param    raw     pointer to set of raw frames
  @return   1 newly allocated detnoise_config blackboard structure.
  @doc      The requested ini file is parsed and a blackboard object is 
            created, then updated accordingly. Returns NULL in case of error.
 */

detnoise_config * 
sinfo_parse_cpl_input_detnoise(cpl_parameterlist * cpl_cfg, 
                               cpl_frameset* sof, 
                               cpl_frameset** raw);

/**
   @name    sinfo_detnoise_free()
   @memo deallocate all memory associated with a detnoise_config data structure
   @param   cfg detnoise_config to deallocate
   @return  void
*/
void 
sinfo_detnoise_free(detnoise_config * cfg);

#endif
