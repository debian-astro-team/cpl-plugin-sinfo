/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------

   File name    :       sinfo_bp_noise.c
   Author       :    J. Schreiber
   Created on   :    May 5, 2003
   Description  :    Different methods for searching for bad pixels
                        used in the recipe spiffi_bp_noise 

 ---------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*----------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_bp_noise.h"
#include "sinfo_detnoise_ini_by_cpl.h"
#include "sinfo_pro_save.h"
#include "sinfo_pro_types.h"
#include "sinfo_raw_types.h"
#include "sinfo_functions.h"
#include "sinfo_detlin.h"
#include "sinfo_error.h"
#include "sinfo_utils_wrappers.h"

/**
 * @addtogroup sinfo_bad_pix_search Bad Pixel Search
 *
 * TBD
 */
/*----------------------------------------------------------------------------
                                Defines
 ---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
                             Function Definitions
 ---------------------------------------------------------------------------*/

/**
   @name  sinfo_new_bp_search_noise
   @memo Finds hot pixels

   @param   plugin_id recipe name
   @param   config input parameters list
   @param   sof  input set of frames
   @param out_name name of product

   @returns         integer (0 if it worked, -1 if it doesn't) 
   @doc

   this function searches for static bad pixels in stacks of sinfo_dark frames
   taken with NDIT = 1 and equal exposure times
   The noise in each pixel is computed and compared to the clean mean noise 
   A pixel is declared bad if the deviation exceeds a threshold
 */
int 
sinfo_new_bp_search_noise (const char* plugin_id,
                           cpl_parameterlist* config,
                           cpl_frameset* sof,
                           const char* out_name )
{

    detnoise_config * cfg =NULL;

    cpl_imagelist * image_list=NULL ;
    cpl_image * img_tmp=NULL ;
    cpl_image       *  mask=NULL ;
    cpl_parameter *p=NULL;
    cpl_frameset* raw=NULL;
    cpl_table* qclog_tbl=NULL;
    char key_value[FILE_NAME_SZ];
    int i=0;
    int n_bad =0;

    /*parse the file names and parameters to the detnoise_config data 
      structure cfg*/
    check_nomsg(raw=cpl_frameset_new());
    ck0(sinfo_extract_raw_frames_type1(sof,raw,RAW_DARK),
        "Error extracting %s frames",RAW_DARK);
    cknull(cfg = sinfo_parse_cpl_input_detnoise(config,sof,&raw),
           " could not parse .ini file!") ;
    check_nomsg(image_list = cpl_imagelist_new());
    for ( i = 0 ; i < cfg->nframes ; i++ )
    {
        if(sinfo_is_fits_file (cfg->framelist[i]) != 1) {
            sinfo_msg_error("Input file %s is not FITS",cfg->framelist[i] );
            goto cleanup;
        }
        check_nomsg(img_tmp=cpl_image_load(cfg->framelist[i],
                        CPL_TYPE_FLOAT,0,0));

        check_nomsg(cpl_imagelist_set(image_list,img_tmp,i));
    }

    /*-----------------------------------------------------------------
     *---------------------- SEARCH FOR BAD PIXELS---------------------
     *-----------------------------------------------------------------*/
    sinfo_msg("Noise Search for bad pixels");
    /*---generate the bad pixel mask-------------*/
    cknull(mask=sinfo_new_search_bad_pixels_via_noise (image_list, 
                    cfg->threshSigmaFactor,
                    cfg->loReject,
                    cfg->hiReject),
           " could not create bad pixel mask!") ;

    n_bad = sinfo_new_count_bad_pixels(mask) ;
    sinfo_msg ("number of bad pixels: %d\n", n_bad) ;

    /* QC LOG */
    cknull_nomsg(qclog_tbl = sinfo_qclog_init());
    check_nomsg(p = cpl_parameterlist_find(config, "sinfoni.bp.method"));
    snprintf(key_value, MAX_NAME_SIZE-1,"%s",cpl_parameter_get_string(p));
    ck0_nomsg(sinfo_qclog_add_string(qclog_tbl,"QC BP-MAP METHOD",key_value,
                    "BP search method"));
    ck0_nomsg(sinfo_qclog_add_int(qclog_tbl,"QC BP-MAP NBADPIX",n_bad,
                    "No of bad pixels"));

    ck0(sinfo_pro_save_ima(mask,raw,sof,(char *) out_name,
                    PRO_BP_MAP_HP,qclog_tbl,plugin_id,config),
        "cannot save ima %s", out_name);

    sinfo_free_image(&mask);
    sinfo_free_table(&qclog_tbl);
    sinfo_free_imagelist(&image_list) ;
    sinfo_detnoise_free(cfg);
    sinfo_free_frameset(&raw);

    return 0 ;

    cleanup:

    sinfo_free_table(&qclog_tbl);
    sinfo_free_imagelist(&image_list) ;
    sinfo_free_image(&mask) ;
    sinfo_detnoise_free(cfg);
    sinfo_free_frameset(&raw);
    return -1 ;

}
/**@}*/
