/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_psf_ini_by_cpl.c
   Author       :   Andrea Modigliani
   Created on   :   May 20, 2004
   Description  :   cpl input for sinfo_psf step for SPIFFI


 ---------------------------------------------------------------------------*/



#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_psf_ini_by_cpl.h"
#include "sinfo_hidden.h"


/*---------------------------------------------------------------------------
                    Functions private to this module
 ---------------------------------------------------------------------------*/
static void      parse_section_frames(psf_config *, 
                   cpl_frameset* sof,cpl_frameset** stk,int* status);
static void     parse_section_reconstruction(psf_config *);


/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Blackboard to psf structure
 *
 * TBD
 */


/*-------------------------------------------------------------------------*/
/**
  @name     parse_psf_ini_file
  @memo     Parse a ini_name.ini file and create a blackboard.
  @param    ini_name    Name of the ASCII file to parse.
  @return   1 newly allocate psf_config blackboard structure.
  @doc

  The requested ini file is parsed and a blackboard object is created, then
  updated accordingly. Returns NULL in case of error.
 */
/*--------------------------------------------------------------------------*/

psf_config * sinfo_parse_cpl_input_psf(cpl_frameset* sof, 
                 cpl_frameset** stk)
{
        psf_config  *       cfg =NULL;
        int status=0;
        /*
         * Perform sanity checks, fill up the structure with what was
         * found in the ini file
         */

        cfg = sinfo_psf_cfg_create();
        parse_section_reconstruction   (cfg);
         parse_section_frames       (cfg, sof,stk,&status);
        if (status > 0) {
                sinfo_msg_error("parsing cpl input");
                sinfo_psf_cfg_destroy(cfg);
                cfg = NULL ;
                return NULL ;
        }
        return cfg ;
}

static void
parse_section_frames(psf_config * cfg, 
             cpl_frameset* sof,
                     cpl_frameset** stk,
                     int* status)
{
   cpl_frame* frame   = NULL;

   int                 npsf=0;
   //int nraw=0;
   char spat_res[FILE_NAME_SZ];
   char lamp_status[FILE_NAME_SZ];
   char band[FILE_NAME_SZ];
   int ins_set=0;

     sinfo_contains_frames_type(sof,stk,PRO_PSF_CALIBRATOR_STACKED);
     npsf = cpl_frameset_get_size(*stk);
     if (npsf < 1) {
       sinfo_contains_frames_type(sof,stk,PRO_STD_NODDING_STACKED);
       npsf = cpl_frameset_get_size(*stk);
     }

     npsf = cpl_frameset_get_size(*stk);
     if (npsf < 1) {
       sinfo_contains_frames_type(sof,stk,PRO_OBJECT_NODDING_STACKED);
       npsf = cpl_frameset_get_size(*stk);
     }


     npsf = cpl_frameset_get_size(*stk);
     if (npsf < 1) {
       sinfo_contains_frames_type(sof,stk,PRO_PUPIL_LAMP_STACKED);
       npsf = cpl_frameset_get_size(*stk);
     }

     npsf = cpl_frameset_get_size(*stk);
     if (npsf < 1) {
         sinfo_msg_error("Cannot find input stacked frames") ;
         (*status)++;
         return;
     }

    if(NULL != cpl_frameset_find(sof,PRO_COADD_PSF)) {
      frame = cpl_frameset_find(sof,PRO_COADD_PSF);
      strcpy(cfg -> inFrame,cpl_frame_get_filename(frame));
    } else if(NULL != cpl_frameset_find(sof,PRO_OBS_PSF)) {
      frame = cpl_frameset_find(sof,PRO_OBS_PSF);
      strcpy(cfg -> inFrame,cpl_frame_get_filename(frame));
    } else if(NULL != cpl_frameset_find(sof,PRO_COADD_STD)) {
      frame = cpl_frameset_find(sof,PRO_COADD_STD);
      strcpy(cfg -> inFrame,cpl_frame_get_filename(frame));
    } else if(NULL != cpl_frameset_find(sof,PRO_OBS_STD)) {
      frame = cpl_frameset_find(sof,PRO_OBS_STD);
      strcpy(cfg -> inFrame,cpl_frame_get_filename(frame));
    } else if(NULL != cpl_frameset_find(sof,PRO_COADD_OBJ)) {
      frame = cpl_frameset_find(sof,PRO_COADD_OBJ);
      strcpy(cfg -> inFrame,cpl_frame_get_filename(frame));
    } else if(NULL != cpl_frameset_find(sof,PRO_OBS_OBJ)) {
      frame = cpl_frameset_find(sof,PRO_OBS_OBJ);
      strcpy(cfg -> inFrame,cpl_frame_get_filename(frame));
    } else {
      sinfo_msg_error("Frame %s or %s or %s or %s or %s or %s not found!", 
       PRO_COADD_PSF,PRO_OBS_PSF,
       PRO_COADD_STD,PRO_OBS_STD,
       PRO_COADD_OBJ,PRO_OBS_OBJ);
      (*status)++;
      return;
    }

   strcpy(cfg -> outName, PSF_OUT_FILENAME);
   //nraw    = cpl_frameset_get_size(*stk);
   frame = cpl_frameset_get_frame(*stk,0);

   sinfo_get_spatial_res(frame,spat_res);
   switch(sinfo_frame_is_on(frame))
     {

    case 0: 
      strcpy(lamp_status,"on");
      break;
    case 1: 
      strcpy(lamp_status,"off");
      break;
    case -1:
      strcpy(lamp_status,"undefined");
      break;
    default: 
      strcpy(lamp_status,"undefined");
      break;

     }

   sinfo_get_band(frame,band);
   sinfo_msg("Spatial resolution: %s lamp status: %s band: %s \n",
                     spat_res,    lamp_status,    band);

   sinfo_get_ins_set(band,&ins_set);

   return;

}

static void     
parse_section_reconstruction(psf_config * cfg)
{
   cfg -> nslits = NSLITLETS;
   return;
}

void
sinfo_free_psf(psf_config ** cfg) {
   sinfo_psf_cfg_destroy (*cfg);
   *cfg=NULL;
  return;
}
/**@}*/
