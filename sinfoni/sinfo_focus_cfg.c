/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

 File name     :    sinfo_focus_cfg.c
 Author     :       Juergen Schreiber
 Created on    :    February 2002
 Description    :    configuration handling tools for the 2d-Gaussian fit
 of a point source

 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_focus_cfg.h"
/**@{*/
/**
 * @addtogroup sinfo_focus Focus determination functions
 *
 * TBD
 */

/*---------------------------------------------------------------------------
 Function codes
 ---------------------------------------------------------------------------*/
/**
 @name     sinfo_focus_cfg_create()
 @return   pointer to allocated base focus_cfg structure
 @memo     allocate memory for a focus_config struct
 @note     only the main (base) structure is allocated
 */
focus_config *
sinfo_focus_cfg_create(void)
{
    return cpl_calloc(1, sizeof(focus_config));
}
/**
 @name   sinfo_focus_cfg_destroy()
 @memo   deallocate all memory associated with a focus_config data structure
 @param  focus_config to deallocate
 @return void
 */
void
sinfo_focus_cfg_destroy(focus_config * cc)
{
    if (cc == NULL )
        return;

    /* Free main struct */
    cpl_free(cc);

    return;
}
/**@}*/



