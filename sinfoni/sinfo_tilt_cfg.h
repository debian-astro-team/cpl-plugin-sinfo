/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   File name     :    sinfo_tilt_cfg.h
   Author         :    Juergen Schreiber
   Created on    :    October 2001
   Description    :    tilt_ini definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_TILT_CFG_H
#define SINFO_TILT_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  determination of spectral tilt blackboard container

  This structure holds all information related to the determination 
  of the spectral tilt routine. It is used as a container for the flux 
  of ancillary data, computed values, and algorithm status. Pixel flux 
  is separated from the blackboard.
  */

typedef struct tilt_config {
/*-------General---------*/
        char inFile[FILE_NAME_SZ] ;    /* file name of the file containing 
                                          the list of all input frames */
        char outName[FILE_NAME_SZ] ;   /* output name of resulting fits 
                                          wavelength map */
        char ** framelist ; /* list of frames */
        int  *  frametype ; /* list of frame types on or off */
        int     nframes ;   /* number of frames in frame list */
        int     nobj ;      /* number of object frames in frame list */
        int     noff ;      /* number of off frames in frame list */

/*------ CleanMean ------*/
        /* percentage of rejected low intensity pixels */
        float loReject ;
        /* percentage of rejected high intensity pixels */
        float hiReject ;
        /* file name of the bad pixel mask fits file */
        char mask[FILE_NAME_SZ] ;

/*------ Slope ------*/
        /* width of a box along the slitlets within which the spectrum 
           is fitted by a Gaussian */
        int box_length ;
        /* initial guess of the FWHM */
        float fwhm ;
        /* amplitude below which the fit is not carried through */
        float minDiff ;

} tilt_config ;

/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name   sinfo_tilt_cfg_create()
   @memo   allocate memory for a tilt_cfg struct
   @return pointer to allocated base tilt_cfg structure
   @note  only the main (base) structure is allocated
*/

tilt_config * 
sinfo_tilt_cfg_create(void);


/**
   @name    sinfo_tilt_cfg_destroy()
   @memo   deallocate all memory associated with a tilt_config data structure
   @param   tilt_config to deallocate
   @return  void
*/

void 
sinfo_tilt_cfg_destroy(tilt_config * sc);

#endif
