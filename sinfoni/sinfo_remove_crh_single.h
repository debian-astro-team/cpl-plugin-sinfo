cpl_image * sinfo_remove_crh_single( cpl_image * sci_image, 
				   double frac_max,
				   double sigma_lim,
				   double f_lim,
				     int max_iter,
				     double gain,
				     double ron);
