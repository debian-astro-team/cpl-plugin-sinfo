/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

 File name     :    sinfo_flat_cfg.c
 Author         : Juergen Schreiber
 Created on    :    March 2002
 Description    :    prepare flatfield frames configuration handling tools
 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_flat_cfg.h"

/**@{*/
/**
 * @defgroup sinfo_flat_cfg Flat manipulation functions
 *
 * TBD
 */

/*---------------------------------------------------------------------------
 Function codes
 ---------------------------------------------------------------------------*/
/**
 @name    sinfo_flat_cfg_create()
 @memo    allocate memory for a flat_cfg struct
 @return  pointer to allocated base flat_cfg structure
 @note    only the main (base) structure is allocated
 */

flat_config *
sinfo_flat_cfg_create(void)
{
    return cpl_calloc(1, sizeof(flat_config));
}
/**
 @name    sinfo_flat_cfg_destroy()
 @memo    deallocate all memory associated with a flat_config data structure
 @param   flat_config to deallocate
 @return  void
 */

void
sinfo_flat_cfg_destroy(flat_config * sc)
{
    if (sc == NULL )
        return;

    /* Free list of frame types */
    /*cpl_free(sc->frametype);*/
    /* Free positions */
    /*cpl_free(sc->frameposition);*/

    /* Free main struct */
    cpl_free(sc);

    return;
}
/**@}*/

