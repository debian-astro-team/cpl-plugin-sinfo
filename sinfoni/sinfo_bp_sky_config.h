/* $Id: sinfo_bp_sky_config.h,v 1.1 2006-10-20 08:06:32 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2006-10-20 08:06:32 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */

 /****************************************************************
  *           Bad pixel search  (normal method)                  *
  ****************************************************************/
#ifndef SINFO_BP_SKY_CONFIG_H
#define SINFO_BP_SKY_CONFIG_H

#include "cpl.h"  
#include "sinfo_globals.h"

void sinfo_bp_sky_config_add(cpl_parameterlist *list);

#endif
