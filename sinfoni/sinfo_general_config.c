/* $Id: sinfo_general_config.c,v 1.6 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *           Bad pixel search  (normal method)                  *
 ****************************************************************/


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_general_config.h"

/**@{*/
/**
 * @defgroup spiffi_general_config General configuration parameters
 *
 * TBD
 */

/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* General data reduction parameters */

void
sinfo_general_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /* indicates if parameters will be overwritten */
    p = cpl_parameter_new_value("sinfoni.general.overwrite_parameters",
                    CPL_TYPE_BOOL,
                    "Overwrite DRS ini parameters: ",
                    "sinfoni.general",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "gen-overpar");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.general.lc_sw",
                    CPL_TYPE_BOOL,
                    "Correct for bad lines introduced by "
                    "instrument software: ",
                    "sinfoni.general",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lc_sw");
    cpl_parameterlist_append(list, p);


    /* Fill the parameters list */
    p = cpl_parameter_new_value("sinfoni.general.lc_kappa",
                    CPL_TYPE_INT,
                    "Kappa sigma value",
                    "sinfoni.general",18);
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lc_kappa") ;
    cpl_parameterlist_append(list, p) ;

    p = cpl_parameter_new_value("sinfoni.general.lc_filt_rad",
                    CPL_TYPE_INT,
                    "Filtering radii applied during median filter."
                    " Should be small",
                    "sinfoni.general",3) ;

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "lc_filt_rad") ;
    cpl_parameterlist_append(list, p) ;


    return;

}
/**@}*/
