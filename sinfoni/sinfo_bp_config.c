/* $Id: sinfo_bp_config.c,v 1.5 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/****************************************************************
 *           Bad pixel search                                   *
 ****************************************************************/

#include "sinfo_bp_config.h"
/**@{*/
/**
 * @addtogroup sinfo_bad_pix_search Bad Pixel Search
 *
 * TBD
 */

/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* Bad pixel parameters */

void
sinfo_bp_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /* indicates which method will be used */
    p = cpl_parameter_new_enum("sinfoni.bp.method",
                    CPL_TYPE_STRING,
                    "Data reduction method: ",
                    "sinfoni.general_noise",
                    "Normal",
                    3,
                    "Normal","Linear","Noise");


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "bp-method");
    cpl_parameterlist_append(list, p);

    return;

}
/**@}*/
