/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

   File name     :    sinfo_object_cfg.h
   Author    :    Juergen Schreiber
   Created on    :    April 2002
   Description    :    object_ini definitions + handling prototypes

 ---------------------------------------------------------------------------*/
#ifndef SINFO_OBJECT_CFG_H
#define SINFO_OBJECT_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  data cube creation blackboard container

  This structure holds all information related to the object
  data cube creation routine. It is used as a container for the
  flux of ancillary data, computed values, and algorithm status.
  Pixel flux is separated from the blackboard.
  */

typedef struct object_config {
/*-------General---------*/
   char inFile[FILE_NAME_SZ]  ; /* input file of reduced jittered
                                        data cubes of a standard star */
   char ** framelist ; /* input averaged, bad pixel corrected, sky
                               subtracted, flatfielded and interleaved
                               jittered frame list */
   char wavemap[FILE_NAME_SZ] ; /* input wavelength calibration map */
   char mflat[FILE_NAME_SZ] ;   /* input master flat field  */
   char outName[FILE_NAME_SZ] ; /* output name of resulting fits
                                        data cube(s) */
   char sky_dist[FILE_NAME_SZ] ; /* master flat corrected for distortion */
   char mflat_dist[FILE_NAME_SZ] ; /* master flat corrected for distortion */
   char mflat_dither_dist[FILE_NAME_SZ] ; /* master flat dithered
                                                  corrected for distortion */
   int     nframes ; /* number of jittered frames */

/*------ jittering ------*/
   /* jitter mode indicator: 0 for user jittering mode,
           1 for auto jittering mode */
   int jitterind ;
   /* x-pixel size of the final combined data cube */
   int size_x ;
   /* y-pixel size of the final combined data cube */
   int size_y ;
   /* the name of the interpolation kernel */
   char kernel_type[FILE_NAME_SZ] ;
   /* the name of the final image sinfo_median of cube */
   char med_cube_name[FILE_NAME_SZ] ;
   /* the name of the final mask cube */
   char maskname[FILE_NAME_SZ] ;
   /* the name of the fits file with the polynom for the shift
    * due atmospheric refraction */
   char polyshiftname[FILE_NAME_SZ] ;

/*------ Resampling ------*/
   /* number of coefficients for the polynomial
   interpolation (order + 1) */
   int ncoeffs ;
   /* number of rows in the resulting resampled
           image = number of spectral bins */
   int nrows ;

/*------ Calibration ------*/
   /* indicates if the halogen lamp feature from
           flatfielding should be corrected for or not */
   int halocorrectInd ;
   /* name of the fits file of the calibrated halogen lamp spectrum */
   char halospectrum[FILE_NAME_SZ] ;

/*------ CubeCreation ------*/
   /* indicates if the slitlet distances are determined
           by a north-south test (1)
           or slitlet edge fits (0) */
   int northsouthInd  ;
   /* name of the ASCII list of the distances of the slitlets */
   char distlist[FILE_NAME_SZ] ;
   /* name of the ASCII list of the fitted slitlet sinfo_edge positions */
   char poslist[FILE_NAME_SZ] ;
   /* number of slitlets (32) */
   int nslits ;
   /* sub pixel position of the column position of the left sinfo_edge of
           the first slitlet needed if the slitlet distances were determined
           by a north south test */
   char firstCol[FILE_NAME_SZ] ;

/*------ FineTuning ------*/
   /* indicator for the shifting method to use */
   char  method[FILE_NAME_SZ] ;
   /* order of polynomial if the polynomial interpolation shifting
           method is used */
   int order ;

/*------ SkyExtraction ------*/
   /* percentage of rejected low value pixels when averaging the
           sky spectra */
   float  loReject ;
   /* percentage of rejected high value pixels when averaging the
           sky spectra */
   float  hiReject ;
   /* pixel distance tolerance to the dividing diagonal line,
           these pixels are not considered to be sure to get only
           "clean" sky pixels */
   int    tolerance ;
} object_config ;

/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name   sinfo_object_cfg_create()
   @memo   allocate memory for a object_cfg struct
   @return pointer to allocated base object_cfg structure
   @note   only the main (base) structure is allocated
*/

object_config *
sinfo_object_cfg_create(void);


/**
   @name   sinfo_object_cfg_destroy()
   @memo deallocate all memory associated with a object_config data structure
   @param  object_config to deallocate
   @return void
*/
void
sinfo_object_cfg_destroy(object_config * cc);

#endif
