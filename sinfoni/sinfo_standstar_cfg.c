/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------

 File name     :    sinfo_standstar_cfg.c
 Author     :       Juergen Schreiber
 Created on    :    March 2002
 Description    :    configuration handling tools for the
 standard star data reduction

 *--------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_standstar_cfg.h"

/**@{*/
/**
 * @addtogroup sinfo_rec_jitter std star structure configuration
 *
 * TBD
 */

/*---------------------------------------------------------------------------
 Function codes
 ---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 @name   sinfo_standstar_cfg_create()
 @param  void
 @return pointer to allocated base standstar_config structure
 @brief  allocate memory for a standstar_config struct
 @note   only the main (base) structure is allocated
 ---------------------------------------------------------------------------*/

standstar_config *
sinfo_standstar_cfg_create(void)
{
    return cpl_calloc(1, sizeof(standstar_config));
}

/*---------------------------------------------------------------------------
 @name    sinfo_standstar_cfg_destroy()
 @param   cc   standstar_config to deallocate
 @return  void
 @brief   deallocate all memory associated with a \
            standstar_config data structure

 ---------------------------------------------------------------------------*/

void
sinfo_standstar_cfg_destroy(standstar_config * cc)
{
    if (cc == NULL )
        return;

    /* Free main struct */
    cpl_free(cc);

    return;
}

/**@}*/


