/* $Id: sinfo_functions.h,v 1.13 2009-03-04 10:17:38 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2009-03-04 10:17:38 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 */
#ifndef SINFO_FUNCTIONS_H
#define SINFO_FUNCTIONS_H

#include <cpl.h>
#include <sinfo_time.h>
#include <sinfo_globals.h>
#include <sinfo_skycor.h>

CPL_BEGIN_DECLS

int sinfo_print_rec_status(const int val);

cpl_frameset *
sinfo_frameset_extract(const cpl_frameset *frames,const char *tag);

cpl_vector* 
sinfo_vector_clip(const cpl_vector* vinp, 
                  const double kappa, 
                  const int n, 
                  const int method);


int
sinfo_image_estimate_noise(cpl_image* img,
                            const int noise_fit, 
                            double* centre, 
               double* noise);


cpl_table* sinfo_compute_gain(cpl_frameset* son, cpl_frameset* sof);
cpl_table* sinfo_compute_linearity(cpl_frameset* son, cpl_frameset* sof);


int sinfo_vector_dindgen(cpl_vector** v);
int sinfo_is_fits_file(const char *filename);
cpl_error_code
sinfo_extract_frames_group_type(const cpl_frameset * set, 
                                cpl_frameset** ext, cpl_frame_group type);
cpl_error_code sinfo_frameset_merge(cpl_frameset * set1, 
                                    cpl_frameset* set2);
cpl_error_code
sinfo_table_correl(cpl_table * t1, 
                   cpl_table* t2, 
                   cpl_table* range,
                   double* xcor);
int 
sinfo_get_pupil_shift(cpl_imagelist* iml,const int n,cpl_table** qclog_tbl);
int sinfo_get_preoptic(const char* file,const char* val);
int sinfo_get_keyvalue_int(cpl_frame * ref_frame, const char* key_name);
float sinfo_get_keyvalue_float(cpl_frame * ref_frame, const char* key_name);
double sinfo_get_keyvalue_double(cpl_frame * ref_frame, const char* key_name);
char sinfo_get_keyvalue_bool(cpl_frame * ref_frame, const char* key_name);
const char* 
sinfo_get_keyvalue_string(cpl_frame * ref_frame, const char* key_name);
int sinfo_get_strehl_type(cpl_frameset* sof);
double sinfo_get_wave_cent(const char* band);
double sinfo_get_dispersion(const char* band);
void sinfo_memory_status(void);
int sinfo_check_rec_status(const int val);

/* ---------------------------------------------------------------------- 
   group of frames
---------------------------------------------------------------------- */



int 
sinfoni_extract_raw_linearity_frames(cpl_frameset * sof, cpl_frameset** raw);
int sinfoni_extract_raw_dark_frames(cpl_frameset * sof, cpl_frameset** raw);
int sinfo_extract_raw_stack_frames(cpl_frameset * sof, cpl_frameset** pro);

int sinfo_extract_raw_frames(cpl_frameset * sof, cpl_frameset** raw); 
int sinfoni_extract_pro_frames(cpl_frameset * sof, cpl_frameset** pro); 
int sinfo_extract_cdb_frames(cpl_frameset * sof, cpl_frameset** cdb);
 
int sinfo_extract_obj_frames(cpl_frameset * sof, cpl_frameset* obj);
int sinfo_extract_sky_frames(cpl_frameset * sof, cpl_frameset* sky);
int sinfo_extract_mst_frames(cpl_frameset * sof, cpl_frameset* cdb);


double sinfo_get_cumoffsetx(cpl_frame * frame);
double sinfo_get_cumoffsety(cpl_frame * frame);
int sinfo_tag_is_objpro(char * tag);
int sinfo_extract_obj_products(cpl_frameset * sof, cpl_frameset* obj);

const char* sinfo_get_pix_scale(float ps);
int sinfo_pix_scale_isnot_const(float* pix_scale, int size);

int sinfo_contains_frames_kind(cpl_frameset * sof, 
                                 cpl_frameset* raw,
                                 const char*         type);

int sinfo_contains_frames_type(cpl_frameset * sof, 
                                    cpl_frameset** raw,
                                    const char*          type);


int sinfo_extract_raw_frames_type(cpl_frameset * sof, 
                                    cpl_frameset** raw,
                                    const char*          type);

int sinfo_extract_raw_frames_type2(cpl_frameset * sof, 
                                    cpl_frameset** raw,
                                    const char*          type);

int sinfo_extract_raw_frames_type1(cpl_frameset * sof, 
                                    cpl_frameset* raw,
                                    const char*          type);

int sinfo_extract_frames_type(cpl_frameset * sof, 
                                cpl_frameset * raw,
                const char*          type);

int sinfo_remove_qc_frames(cpl_frameset* sof,cpl_frameset** raw);
/* ---------------------------------------------------------------------- 
   single frames
---------------------------------------------------------------------- */
double sinfo_get_mjd_obs(cpl_frame * frame);
int sinfo_frame_is_raw(char * tag); 
int sinfoni_frame_is_pro(char * tag); 
int sinfo_frame_is_cdb(char * tag); 
int sinfo_frame_is_stk(char * tag); 
int sinfo_frame_is_preoptic(cpl_frame*, const char* val); 

int sinfo_frame_is_pinhole_lamp(char * tag); 
int sinfo_frame_is_raw_stack(char * tag); 
int sinfo_frame_is_slit_lamp(char * tag); 


int sinfo_is_flat_bp(char * tag);
int sinfo_is_flat_lindet(char * tag);
int sinfo_is_dark(char * tag);
int sinfoni_is_pinhole_lamp(char * tag) ;
int sinfoni_is_raw_stack(char * tag) ;

int sinfoni_is_lamp_slit(char * tag) ;
int sinfo_is_sky_flat(char * tag) ;

int sinfo_is_mflat(char * tag) ;
int sinfo_is_master_flat(char * tag) ;
int sinfo_is_master_flat_dither(char * tag) ;

int sinfo_is_stack(char * tag) ;
int sinfo_is_lamp_wave_stacked(char * tag) ;
int sinfo_is_lamp_flux_stacked(char * tag) ;
int sinfo_is_psf_calibrator_stacked(char * tag) ;
int sinfo_is_focus_stacked(char * tag) ;
int sinfo_is_object_nodding_stacked(char * tag) ;
int sinfo_is_sky_nodding_stacked(char * tag) ;
int sinfo_is_object_skyspider_stacked(char * tag) ;


int sinfo_blank2dot(const char * in, char* ou) ;
int sinfo_is_bpmap(char *) ;
int sinfo_is_slitpos(char * tag) ;
int sinfo_is_wavemap(char * tag) ;
int sinfo_is_halosp(char * tag) ;
int sinfo_is_distlist(char * tag) ;
int sinfo_is_firstcol(char * tag) ;
int sinfo_is_fibres_on_off(cpl_frameset * sof, 
                 cpl_frameset* raw);
/* ---------------------------------------------------------------------- 
   Extra functionalities
---------------------------------------------------------------------- */
int
sinfo_clean_nan(cpl_image** im);

int  sinfo_get_clean_mean_window(cpl_image* img, 
                                 int llx, 
                                 int lly, 
                                 int urx, 
                                 int ury, 
                                 const int kappa, 
                                 const int nclip, 
                                 double* sinfo_clean_mean, 
                                 double* clean_stdev);

int sinfo_get_obsname(cpl_frame * ref_frame, const char* ob_name);


 int sinfo_get_ron(cpl_frameset    *   framelist,
                const int ron_xmin,
                const int ron_xmax,
                const int ron_ymin,
                const int ron_ymax,
                const int ron_hsize,
                const int ron_nsamp,
             double** ron);
int sinfo_stack_get_pro_tag(char * tag_in, char* tag_out) ; 
int sinfo_compare_tags(const cpl_frame *, const cpl_frame *) ;

int sinfo_get_spatial_res(cpl_frame * ref_frame,char * spat_res);
int sinfo_frame_is_dither(cpl_frame * ref_frame);
int sinfo_frame_is_sky(cpl_frame * ref_frame);
int sinfo_tag_is_obj(char * ref_frame);
int sinfo_tag_is_sky(char * ref_frame);

int sinfo_frame_is_on(cpl_frame * ref_frame);
int sinfo_get_band(cpl_frame * ref_frame,char * band);
int sinfo_get_ins_set(char* band,int* ins_set);
int sinfoni_get_ins_setting(cpl_frame * ref_file,char * set_id);
int sinfo_pfits_add_qc(cpl_propertylist * plist,qc_log * qclog);






CPL_END_DECLS

#endif
