#ifndef SINFO_REC_UTILS_H
#define SINFO_REC_UTILS_H

/* $Id: sinfo_rec_utils.h,v 1.2 2007-06-06 07:10:45 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2007-06-06 07:10:45 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

 /****************************************************************
  *           Object Data reduction                              *
  ****************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>          /* allows the program compilation */
#endif

/*-----------------------------------------------------------------------------
                                Includes
-----------------------------------------------------------------------------*/

/* std */
#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <libgen.h>

/* cpl */
#include <cpl.h>  

/* sinfoni */
#include <sinfo_pro_types.h>
#include <sinfo_key_names.h>
#include <sinfo_raw_types.h>
#include <sinfo_globals.h>
#include <sinfo_new_prepare_stacked_frames.h>
#include <sinfo_functions.h>


/*-----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/


int 
sinfo_new_get_dummy_obj_sky_stacked(cpl_frameset* obj_set,
                                    cpl_frameset** set, 
                                    cpl_parameterlist* config,
                                    fake* fk,
                                    char* pro_ctg, 
                                    const char* plugin_id);

cpl_frame* 
sinfo_new_get_dummy_sky(cpl_frameset* obj_set);

int 
sinfo_cub_stk_frames(cpl_parameterlist* config,
                     cpl_frameset** set,
                     const char* recipe_id,
             const char** pro_ctg_cube);


int
sinfo_new_stack_frames(cpl_parameterlist* cfg, 
                       cpl_frameset* set, 
                       const char* procatg,
                       const int id, 
                       fake* fk, 
                       const char* plugin_id);

const char* 
sinfo_new_set_obj_procatg(const char* tag);

int
sinfo_get_dummy_obj_sky_stacked_and_cubes(cpl_frameset* obj_set,
                                          cpl_frameset** set, 
                                          cpl_parameterlist* config,
                                          fake* fk, 
                                          char* pro_ctg, 
                                          const char* plugin_id);
#endif /*!SI_REC_UTILS*/
