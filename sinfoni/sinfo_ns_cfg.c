/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------

 File name     :    sinfo_ns_cfg.c
 Author     :       Juergen Schreiber
 Created on    :    November 2001
 Description    :    configuration handling tools for the north-south test

 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_ns_cfg.h"

/*---------------------------------------------------------------------------
 Function codes
 ---------------------------------------------------------------------------*/
/**@{*/
/**
 * @addtogroup sinfo_rec_distortion North South Test structure configuration
 *
 * TBD
 */

/*---------------------------------------------------------------------------
 Function :   sinfo_ns_cfg_create()
 In       :   void
 Out      :   pointer to allocated base ns_config structure
 Job      :   allocate memory for a ns_config struct
 Notice   :   only the main (base) structure is allocated
 ---------------------------------------------------------------------------*/

ns_config *
sinfo_ns_cfg_create(void)
{
    return cpl_calloc(1, sizeof(ns_config));
}

/*---------------------------------------------------------------------------
 Function :   sinfo_ns_cfg_destroy()
 In       :   ns_config to deallocate
 Out      :   void
 Job      :   deallocate all memory associated with a ns_config data structure
 Notice   :
 ---------------------------------------------------------------------------*/

void
sinfo_ns_cfg_destroy(ns_config * nc)
{
    if (nc == NULL )
        return;

    /*cpl_free(nc->frametype);*/

    /* Free main struct */
    cpl_free(nc);

    return;
}

/**@}*/


