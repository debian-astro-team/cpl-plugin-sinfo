#ifndef SINFO_BP_NOISE_H
#define SINFO_BP_NOISE_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_bp_noise.h,v 1.3 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  06/05/03  created
*/

/************************************************************************
 * bp_noise.h
 * routine to search for bad pixels
 *----------------------------------------------------------------------
 */
/*
 * header files
 */
#include <cpl.h>
#include "sinfo_msg.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

int 
sinfo_new_bp_search_noise (const char* plugin_id, 
                           cpl_parameterlist* config, 
                           cpl_frameset* set, 
                           const char* out_name);

#endif 
