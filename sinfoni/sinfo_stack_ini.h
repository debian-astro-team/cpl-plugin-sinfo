/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*----------------------------------------------------------------------------
   
   File name    :   sinfo_stack_ini.h
   Author       :    Juergen Schreiber
   Created on   :    Sept 25, 2001
   Description  :    preparing stack of frames ini file handling for SPIFFI

 ---------------------------------------------------------------------------*/


#ifndef SINFO_STACK_INI_H
#define SINFO_STACK_INI_H


/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/

#include <cpl.h>

#include "sinfo_stack_cfg.h"

 
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/

#define FRAME_ON     1  /* object frames */
#define FRAME_OFF    0  /* off frames, that means sky frames or calibration 
                           frames with lamp switched off */
#define FRAME_POS1   2  /* frames exposed with grating position 1 */
#define FRAME_POS2   3  /* frames exposed with dithered grating position 2 */
 
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/**
  @name        generateStack_ini_file
  @memo        Generate a default ini file for the preparing of stack 
                frames command.
  @param    ini_name    Name of the file to generate.
  @param    name_i        Name of the input file.
  @param    name_o        Name of the output file.
  @return    int 0 if Ok, -1 otherwise.
  @doc

  This function generates a default ini file for the preparing of 
  stacked frames command. The
  generated file will have the requested name. 
 */
/*--------------------------------------------------------------------------*/
int generateStack_ini_file(
        char * ini_name,
        char * name_i,
        char * name_o
);

/*-------------------------------------------------------------------------*/
/**
  @name     parse_stack_ini_file
  @memo     Parse a ini_name.ini file and create a blackboard.
  @param    ini_name    Name of the ASCII file to parse.
  @return   1 newly allocated stack_config blackboard structure.
  @doc

  The requested ini file is parsed and a blackboard object is created, then
  updated accordingly. Returns NULL in case of error.
 */
/*--------------------------------------------------------------------------*/

stack_config_n * parse_stack_ini_file(char * ini_name) ;

#endif
