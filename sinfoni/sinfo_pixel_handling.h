/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
 /*---------------------------------------------------------------------------
   
   File name     :    sinfo_pixel_handling.c
   Author         :    Nicolas Devillard
   Created on    :    March 4th, 1997
   Description    :    Functions to handle list of pixels and their use

 ---------------------------------------------------------------------------*/
/*
 $Id: sinfo_pixel_handling.h,v 1.5 2007-06-06 07:10:45 amodigli Exp $
 $Author: amodigli $
 $Date: 2007-06-06 07:10:45 $
 $Revision: 1.5 $
 */
#ifndef SINFO_PIXEL_HANDLING_H
#define SINFO_PIXEL_HANDLING_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_local_types.h"
#include "sinfo_msg.h"
/*---------------------------------------------------------------------------
                                 Function ANSI prototypes
 ---------------------------------------------------------------------------*/
/**
  @name     sinfo_pixel_qsort
  @memo     Sort an array of pixels by increasing pixelvalue.
  @param    pix_arr     Array to sort.
  @param    npix        Number of pixels in the array.
  @return   void
  @doc
 
  Optimized implementation of a fast pixel sort. The input array is
  modified.
 */

void 
sinfo_pixel_qsort(pixelvalue *pix_arr, int npix) ;
#endif
