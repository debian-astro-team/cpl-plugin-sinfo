/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

   File name     :    sinfo_detlin_cfg.c
   Autor    :       Juergen Schreiber
   Created on    :    April 2002
   Description    :    handles the data structure detlin_config
 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
 ---------------------------------------------------------------------------*/
#include "sinfo_detlin_cfg.h"
/**@{*/
/**
 * @addtogroup sinfo_detlin Detector Linearity Determination Functions
 *
 * TBD
 */
/*---------------------------------------------------------------------------
                              Function codes
 ---------------------------------------------------------------------------*/
/**
   @name   sinfo_detlin_cfg_create()
   @memo   allocate memory for a detlin_cfg struct
   @return pointer to allocated base detlin_cfg structure
   @note   only the main (base) structure is allocated
 */

detlin_config * 
sinfo_detlin_cfg_create(void)
{
    return cpl_calloc(1, sizeof(detlin_config));
}

/**
  @name   sinfo_detlin_cfg_destroy()
  @memo   deallocate all memory associated with a detlin_config data structure
  @param  detlin_config to deallocate
  @return void
 */
void sinfo_detlin_cfg_destroy(detlin_config * sc)
{

    if (sc==NULL) return ;
    /* cpl_free(sc->framelist) ; */
    /* Free main struct */
    cpl_free(sc);

    return ;
}

/**@}*/


