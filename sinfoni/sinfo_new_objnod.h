#ifndef SINFO_NEW_OBJNOD_H
#define SINFO_NEW_OBJNOD_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_objnod.h,v 1.8 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  06/05/03  created
*/

/************************************************************************
 * sinfo_objnod.h
 * routines to create a data cube
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <math.h>
#include <cpl.h>   
#include "sinfo_msg.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   @name sinfo_new_objnod()
   @param ini_file: file name of according .ini file
   @return integer (0 if it worked, -1 if it doesn't) 
   @doc this routine does the resampling of an offset-corrected,
        flatfielded, bad pixel corrected and
        eventually interleaved data frame. Additionally, an intensity 
    calibration is carried through by using
        a standard star or a black body measurement. 
    The spectral features of the flatfield halogen lamp are corrected.
        Afterwards a data cube is created out of the resampled image.
    It is the users choice to use either
        the fitted sinfo_edge positions of the slitlets or the distances
    of the slitlets gained from a north-south-test. 
 ---------------------------------------------------------------------------*/
int 
sinfo_new_objnod (const char* plugin_id,
                  cpl_parameterlist* config, 
                  cpl_frameset* sof, 
                  const char* procatg) ;


#endif /*!SINFO_NEW_OBJNOD_H*/

/*--------------------------------------------------------------------------*/
