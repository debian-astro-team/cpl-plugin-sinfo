#ifndef SINFO_NEW_STDSTAR_H
#define SINFO_NEW_STDSTAR_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_stdstar.h,v 1.11 2007-09-21 14:13:43 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  06/05/03  created
*/

/************************************************************************
 * sinfo_new_stdstar.h
 * routines to create a data cube
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h>  
#include "sinfo_msg.h"
#include <sinfo_standstar_cfg.h>
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
   Function     :       sinfo_new_stdstar()
   In           :       ini_file: file name of according .ini file
   Out          :       integer (0 if it worked, -1 if it doesn't) 
   Description  :    this routine does the optimal extraction of a spectrum 
                        of an already reduced data cube of a standard star 
                        observation. Additionally, a conversion factor from 
                        mag to counts/sec can be determined if the magnitude 
                        of the standard star is known. 
                        This is done for a number of jittered data cubes and 
                        the results are averaged by rejecting the extreme 
                        values
 ---------------------------------------------------------------------------*/
int 
sinfo_new_stdstar (const char* plugin_id,
                   cpl_parameterlist* config,
                   cpl_frameset* sof,cpl_frameset* ref_set) ;

cpl_mask * sinfo_bpm_filter(
        const cpl_mask    *   input_mask,
        cpl_size        kernel_nx,
        cpl_size        kernel_ny,
        cpl_filter_mode filter);

cpl_image *
sinfo_simple_extraction_from_cube(cpl_imagelist * cube,
                                  const cpl_mask* obj_mask,
                                  const char* name,
                                  cpl_table** spectrum,
                                  int qc_info);
#endif /*!SINFO_STDSTAR_H*/

/*--------------------------------------------------------------------------*/
