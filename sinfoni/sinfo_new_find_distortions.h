#ifndef SINFO_NEW_FIND_DISTORTIONS_H
#define SINFO_NEW_FIND_DISTORTIONS_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_find_distortions.h,v 1.9 2007-09-14 14:40:04 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  12/08/04  created
*/

/************************************************************************
 * sinfo_find_distortions.h

 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <cpl.h>   
#include "sinfo_msg.h"


/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Function     :       sinfo_find_distortions()
   In           :       
   Out          :        
   Job          :

 ---------------------------------------------------------------------------*/
int 
sinfo_new_find_distortions (const char* plugin_id,
                            cpl_parameterlist* config, 
                            cpl_frameset* sof,
			    cpl_frameset* set_fibre_ns) ;


#endif 

/*--------------------------------------------------------------------------*/
