/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

   File name     :    sinfo_psf_cfg.c
   Author     :       Juergen Schreiber
   Created on    :    February 2002
   Description    :    configuration handling tools for the 
                        psf image reconstruction of a star.

 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_psf_cfg.h"
/**@{*/
/** 
 * @addtogroup sinfo_rec_jitter PSF structure definition
 *
 * TBD
 */

/*---------------------------------------------------------------------------
                              Function codes
 ---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
   Function :   sinfo_psf_cfg_create()
   In       :   void
   Out      :   pointer to allocated base psf_config structure
   Job      :   allocate memory for a psf_config struct
   Notice   :   only the main (base) structure is allocated
 ---------------------------------------------------------------------------*/

psf_config * sinfo_psf_cfg_create(void)
{
    return cpl_calloc(1, sizeof(psf_config));
}


/*---------------------------------------------------------------------------
   Function :   sinfo_psf_cfg_destroy()
   In       :   psf_config to deallocate
   Out      :   void
   Job      :   deallocate all memory associated with a config data structure
   Notice   :   
 ---------------------------------------------------------------------------*/

void sinfo_psf_cfg_destroy(psf_config * cc)
{
    if (cc==NULL) return ;

    /* Free main struct */
    cpl_free(cc);

    return ;
}

/**@}*/


