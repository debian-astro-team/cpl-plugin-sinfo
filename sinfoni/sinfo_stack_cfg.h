/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    stack_cfg.h
   Author         :    Juergen Schreiber
   Created on    :    September 2001
   Description    :    stack_ini definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_STACK_CFG_H
#define SINFO_STACK_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <cpl.h>
#include "sinfo_globals.h"
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  prepare stacked images blackboard container

  This structure holds all information related to the stacked images handling
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct stack_config_n {
/*-------General---------*/
        char inFile[FILE_NAME_SZ] ; /* file name of the file containing 
                                       the list of all input frames */
        char outName[FILE_NAME_SZ] ; /* output name of resulting fits 
                                        wavelength map */
        char sky_name[FILE_NAME_SZ] ; /* output name of resulting fits 
                                         sky frame */
        char ** framelist ; /* list of frames */
        int  * frametype ; /* list of frame types on or off */
        int  * frameposition ; /* list of grating positions */  
        int    contains_sky ; /* indicates if off or sky frames were exposed */
        int    contains_ref ; /* indicates if ref frames were exposed */
        int    contains_dither ; /* indicates if spectral dithering 
                                    was applied */
        int    contains_dark ; /* indicates if spectral 
                                  dithering was applied */
        int    nframes ; /* number of frames in frame list */
        int    nobj ;  /* number of object frames in frame list */
        int    noff ;  /* number of off frames in frame list */
        int    ndark ;  /* number of sinfo_dark frames in frame list */
        int    nditherobj ;  /* number of dithered object 
                                frames in frame list */
        int    nditheroff ;  /* number of dithered off frames in frame list */

/*------ CleanMean ------*/
        /* percentage of rejected low intensity pixels */
        float loReject ;      
        /* percentage of rejected high intensity pixels */
        float hiReject ;

/*------ Flatfielding ------*/
        /* indicator if a flatfielding is carried through or not */
        int flatInd ;
        char flatfield1[FILE_NAME_SZ] ; /* name of the flatfield fits file 
                                           for grating position 1 */
        char flatfield2[FILE_NAME_SZ] ; /* name of the flatfield fits file 
                                           for grating position 2 */

/*------ BadPixel ------*/
        /* indicates if the bad pixel mask should be applied or not */
        int maskInd ;
        /* indicates if the pixels should be interpolated or indicated */
        int indind ;
        /* file name of the bad pixel mask fits file */
        char mask[FILE_NAME_SZ] ;
        /* maximal pixel distance from the bad pixel to which valid 
           pixels are searched for*/ 
        int maxRad ;
        /* file name of the slitlet sinfo_edge position list */
        char slitposList[FILE_NAME_SZ] ;
        /* index file list*/
        char indexlist[FILE_NAME_SZ] ;
        /* sigma factor for the additional bad pixel search 
           in method maskInd = 2 */
        float sigmaFactor ;

/*------ ColTilt ------*/
        /* indicator if the tilt correction should be carried through */
        /*int coltiltInd ;*/
        /* file name of the ASCII file containing the slope value */
        /*char slopeFile[FILE_NAME_SZ] ;*/
        /* order of polynomial used for polynomial interpolation 
           for the row shifting */
        /*int order ;*/
/*------ WarpFix ------*/
        /* indicator if the warping should be carried through */
        int warpfixInd ;
        /* file name of the ASCII file containing the polynomial values */
        char polyFile[FILE_NAME_SZ] ;
        /* file name of MASTER DARK */
        char mdark[FILE_NAME_SZ] ;
  /* swich to indicate if a mdark file has been found */
        int mdark_ind;  
      /* kernel to be used for the interpolation */
        char kernel[FILE_NAME_SZ] ;

/*------ Interleaving ------*/
        /* indicator if the interleaving or Convolution with a 
           Gaussian or nothing should take place */
        int interInd ;
    /* number of rows from which the general offset between 
           the frames is determined */
    int noRows ;

/*------ GaussConvolution ------*/
    /* indicator if Gaussian convolution is applied or not */
        int gaussInd ;
    /* kernel half width of the Gaussian response function */
    int hw ;

/*------ ShifFrames ------*/
    /* indicator if shiftting frames is applied or not */
        int sfInd;
    /* interpolation type*/
    int sfType;
    /* interpolation order*/
    int sfOrder;

  /* qc log */
  int qc_thresh_min;
  int qc_thresh_max;

} stack_config_n ;
/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name  sinfo_stack_cfg_create()
   @memo   allocate memory for a stack_cfg struct
   @return pointer to allocated base stack_cfg structure
   @note   only the main (base) structure is allocated
*/

stack_config_n * 
sinfo_stack_cfg_create_n(void);


/**
   @name   sinfo_stack_cfg_destroy()
   @memo  deallocate all memory associated with a stack_config data structure
   @param  stack_config to deallocate
   @return   void
*/
void 
sinfo_stack_cfg_destroy_n(stack_config_n * sc);

#endif
