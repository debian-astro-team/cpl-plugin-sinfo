/* $Id: sinfo_bp_noise_config.c,v 1.5 2008-01-17 07:54:04 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2008-01-17 07:54:04 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/****************************************************************
 *           Bad pixel search  (noise method)                  *
 ****************************************************************/
#include "sinfo_bp_noise_config.h"
/**
 * @addtogroup sinfo_bad_pix_search Bad Pixel Search
 *
 * TBD
 */

/**@{*/
/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* Bad pixel parameters */

void
sinfo_bp_noise_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }



    /* factor of noise within which the pixels are used to fit a straight line
   to the column intensity */
    p = cpl_parameter_new_value("sinfoni.bp_noise.thresh_sigma_factor",
                    CPL_TYPE_DOUBLE,
                    "Threshold Sigma Factor: "
                    "If the mean noise exceeds this "
                    "threshold times the clean standard deviation "
                    "of the clean mean the corresponding pixels "
                    "are declared as bad ",
                    "sinfoni.bp_noise",
                    10.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,
                            "bp_noise-thresh_sigma_fct");
    cpl_parameterlist_append(list, p);

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.bp_noise.low_rejection",
                    CPL_TYPE_DOUBLE,
                    "low_rejection: "
                    "percentage of rejected low intensity "
                    "pixels before averaging",
                    "sinfoni.bp_noise",
                    10.,0.,100.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_noise-lo_rej");
    cpl_parameterlist_append(list, p);

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.bp_noise.high_rejection",
                    CPL_TYPE_DOUBLE,
                    "high_rejection: "
                    "percentage of rejected high intensity "
                    "pixels before averaging",
                    "sinfoni.bp_noise",
                    10.,0.,100.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_noise-hi_rej");
    cpl_parameterlist_append(list, p);

    return;

}
/**@}*/
