#ifndef SINFO_SPIFFI_TYPES_H
#define SINFO_SPIFFI_TYPES_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_spiffi_types.h,v 1.3 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  25/05/00  created
*/

/************************************************************************
 * sinfo_spiffi_types.h
 * all shared local new data types and defines for spiffi data reduction
 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <sys/types.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "sinfo_local_types.h"

/*----------------------------------------------------------------------------
                                   Defines
 *--------------------------------------------------------------------------*/

#define NullVector   (Vector *) NULL
#define SLOPE        1000.  /*slope limit of the linear fit*/
#define SATURATION   50000. /*saturation level of the detector pixel values*/
#define ESTIMATE     200    /*estimation of number of parameters stored in an
                              ascii file*/
#define THRESH       100000. /* Threshold for operation 1/pixelvalue of 
                                an image division*/

/*--------------------------------------------------------------------------*/

#define PIXEL_WIDTH  0.0185 /* mm */
#define FOCAL_LENGTH 178.   /* mm */
 
#define N_SLITLETS   32     /* number of slitlets */
#define SLITLENGTH   64     /* nominal pixel length of one slitlet */
#define SLIT_LEN_ERR  5     /* error on SLITLENGTH */
#define SLIT_POS_ERR  5     /* error on SLITLENGTH */

#define LOW_REJECT   0.1    /* fraction of rejected extreme low values of fit 
                               coefficients in sinfo_coefsCrossFit() */
#define HIGH_REJECT  0.9    /* fraction of rejected extreme high values of fit
                               coefficients in sinfo_coefsCrossFit() */
#define PIXEL        25     /* number of pixels for offsets in 
                               sinfo_coeffsCrossSlitFit() */

#define CENTRALLAMBDA_K 2.2 /* defined central wavelengths and pixel 
                               dispersions for each grating for 
                               sinfo_definedResampling() */
#define CENTRALLAMBDA_H    1.65
#define CENTRALLAMBDA_J    1.25
#define CENTRALLAMBDA_HK   1.95
#define DISPERSION_K       0.00049
#define DISPERSION_K_DITH  0.000245
#define DISPERSION_H       0.00039
#define DISPERSION_H_DITH  0.000195
#define DISPERSION_J       0.00029
#define DISPERSION_J_DITH  0.000145
#define DISPERSION_HK      0.001
#define DISPERSION_HK_DITH 0.0005

/*----------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------*/
/* The following structure stores a sinfo_vector                           */
/*--------------------------------------------------------------------------*/

typedef struct _VECTOR_
{
    int           n_elements ;       /* number of sinfo_vector elements */
    pixelvalue    *       data ;         /* array of sinfo_vector values */
} Vector ;   

/*--------------------------------------------------------------------------*/
/* The following structure stores the (gauss) fitting parameters of the     */
/* emission lines of the calibration lamps                                  */
/*--------------------------------------------------------------------------*/

typedef struct _FIT_PARAMS_
{
    int      n_params ;        /* total number of fits to allocate 
                                  FitParams array */
    int      column ;          /* index of the column in image coordinates */
    int      line   ;          /* index of fitted emission line 
                                  of the column */            
    float    wavelength ;      /* associated wavelength of the calibration 
                                  lamp emission line taken from the 
                                  line center list file */
    float  * fit_par ;         /* fit_par[0]: amplitude,
                                  fit_par[1]: FWHM,
                                  fit_par[2]: position of 
                                              center of gauss,
                                  fit_par[3]: zero level offset */
    float  * derv_par ;        /* corresponding derivatives of fit_par[] */
} FitParams ;   

typedef struct _STATS_
{
    float cleanmean ;          /* mean of pixel values without considering 
                                  the extreme values */
    float cleanstdev ;         /* standard deviation of pixel values without 
                                  considering the extreme values */
    int   npix ;               /* number of clean pixel values */
} Stats ;

typedef struct _BCOEFFS_
{
    int slitlet ;              /* current slitlet */
    int n_slitlets ;           /* number of slitlets */
    int n_acoeffs ;            /* number of a fit coefficients */
    int n_bcoeffs ;            /* number of b fit coefficients */
    float ** b ;               /* sinfo_matrix of fit coefficients: 
                                  b[index acoefs][index bcoefs] */
} Bcoeffs ;

#endif /*!SINFO_SPIFFI_TYPES_H*/

/*--------------------------------------------------------------------------*/


