/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

   File name     :    sinfo_detnoise_cfg.c
   Autor    :       Juergen Schreiber
   Created on    :    September 2002
   Description    :    handles the data structure detnoise_config

 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_detnoise_cfg.h"
/**@{*/
/**
 * @addtogroup sinfo_bad_pix_search Bad Pixel Search
 *
 * TBD
 */

/*---------------------------------------------------------------------------
                              Function codes
 ---------------------------------------------------------------------------*/
/**
   @name    sinfo_detnoise_cfg_create()
   @return  pointer to allocated base detnoise_cfg structure
   @memo    allocate memory for a detnoise_cfg struct
   @note    only the main (base) structure is allocated
 */
detnoise_config * 
sinfo_detnoise_cfg_create(void)
{
    return cpl_calloc(1, sizeof(detnoise_config));
}

/**
   @name    sinfo_detnoise_cfg_destroy()
   @memo deallocate all memory associated with a detnoise_config data structure
   @param   detnoise_config to deallocate
   @return  void
 */
void 
sinfo_detnoise_cfg_destroy(detnoise_config * sc)
{
    if (sc==NULL) return ;
    /* Free main struct */
    cpl_free(sc);

    return ;
}

/**@}*/


