#ifndef SINFO_PRO_SAVE_H
#define SINFO_PRO_SAVE_H

/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <cpl.h>
#include "sinfo_pfits.h"
#include "sinfo_msg.h"


/**
@name sinfo_pro_save_ima
@memo save image of FITS file
@param ima image to ge saved
@param ref reference frame set
@param sof output frame set
@param name output filename
@param proid output PRO.CATG
@param qclog qc-log table
@param recid recipe ID
@param parlist input parameter list
*/


int
sinfo_pro_save_ima(
        cpl_image* ima,
        cpl_frameset* ref,
        cpl_frameset* sof,
        const char* name, 
        const char* proid, 
        cpl_table* qclog,
        const char* recid,
        cpl_parameterlist* parlist);





/**
@name sinfo_save_pro_tbl
@memo save table of FITS file
@param tbl input table
@param ref reference frame set
@param sof output frame set
@param name output filename
@param proid output PRO.CATG
@param qclog qc-log table
@param recid recipe ID
@param parlist input parameter list
*/

int
sinfo_save_pro_tbl(
    cpl_table* tbl,
        cpl_frameset* ref,
        cpl_frameset* sof,
        const char* name, 
        const char* proid, 
        cpl_propertylist* qclog,
        const char* recid,
        cpl_parameterlist* parlist);


/**
@name sinfo_save_pro_ima
@memo save image of FITS file
@param ima image to ge saved
@param ref reference frame set
@param sof output frame set
@param name output filename
@param proid output PRO.CATG
@param qclog qc-log table
@param recid recipe ID
@param parlist input parameter list
*/


int
sinfo_save_pro_ima(
        cpl_image* ima,
        cpl_frameset* ref,
        cpl_frameset* sof,
        const char* name, 
        const char* proid, 
        cpl_propertylist* qclog,
        const char* recid,
        cpl_parameterlist* parlist);

/**
@name sinfo_save_pro_ims
@memo save imagelist of FITS file
@param ims imagelist to be saved
@param ref reference frame set
@param sof output frame set
@param name output filename
@param proid output PRO.CATG
@param qclog qc-log table
@param recid recipe ID
@param parlist input parameter list
*/

int
sinfo_save_pro_ims(
        cpl_imagelist* ims,
        cpl_frameset* ref,
        cpl_frameset* sof,
        const char* name, 
        const char* proid, 
        cpl_propertylist* qclog,
        const char* recid,
        cpl_parameterlist* parlist);

/**
@name sinfo_pro_save_tbl
@memo save table of FITS file
@param tbl input table
@param ref reference frame set
@param sof output frame set
@param name output filename
@param proid output PRO.CATG
@param qclog qc-log table
@param recid recipe ID
@param parlist input parameter list
*/

int
sinfo_pro_save_tbl(
    cpl_table* tbl,
        cpl_frameset* ref,
        cpl_frameset* sof,
        const char* name,
        const char* proid,
        cpl_table* qclog,
        const char* recid,
        cpl_parameterlist* parlist);

/**
@name sinfo_pro_save_ims
@memo save imagelist of FITS file
@param ims imagelist to be saved
@param ref reference frame set
@param sof output frame set
@param name output filename
@param proid output PRO.CATG
@param qclog qc-log table
@param recid recipe ID
@param parlist input parameter list
*/

int
sinfo_pro_save_ims(
        cpl_imagelist* ims,
        cpl_frameset* ref,
        cpl_frameset* sof,
        const char* name,
        const char* proid,
        cpl_table* qclog,
        const char* recid,
        cpl_parameterlist* parlist);

cpl_table* sinfo_qclog_init(void);

int
sinfo_qclog_add_int(cpl_table* table,
                 const char* name,  
                 const int   value,
                 const char* help);


int
sinfo_qclog_add_bool(cpl_table* table,
                 const char* name,  
                 const char  value,
                 const char* help);

int
sinfo_qclog_add_double(cpl_table* table,
                 const char* name,  
                 const double   value,
                 const char* help);

int
sinfo_qclog_add_double_f(cpl_table* table,
                       const char*  key_name,
                       const double value,
                       const char*  key_help);
int
sinfo_qclog_add_double_format(cpl_table* table,
                 const char* name,
                 const double   value,
                 const char* help);

int
sinfo_qclog_add_string(cpl_table* table,
                 const char* name,  
                 const char*   value,
                 const char* help);



cpl_propertylist* sinfo_qc_init(void);

int
sinfo_qc_add_int(cpl_propertylist* table,
                 const char* name,  
                 const int   value,
                 const char* help,
         const char* format);


int
sinfo_qc_add_bool(cpl_propertylist* table,
                 const char* name,  
                 const char  value,
                 const char* help,
         const char* format);


int
sinfo_qc_add_float(cpl_propertylist* table,
                 const char* name,  
                 const float   value,
                 const char* help,
         const char* format);


int
sinfo_qc_add_double(cpl_propertylist* table,
                 const char* name,  
                 const double   value,
                 const char* help,
         const char* format);


int
sinfo_qc_add_string(cpl_propertylist* table,
                 const char* name,  
                 const char*   value,
                 const char* help,
         const char* format);



#endif /* SINFO_PRO_SAVE */
