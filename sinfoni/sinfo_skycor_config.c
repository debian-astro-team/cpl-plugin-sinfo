/* $Id: sinfo_skycor_config.c,v 1.17 2012-03-03 10:18:26 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 10:18:26 $
 * $Revision: 1.17 $
 * $Name: not supported by cvs2svn $
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_skycor_config.h"
#include "sinfo_skycor.h"

/**@{*/
/**
 * @defgroup sinfo_skycor_config Sky residuals corrections configuration 
 *   parameters
 *
 * TBD
 */

/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* General data reduction parameters */

void
sinfo_skycor_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }


    /* Fill the parameters list */
    /* --stropt */

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.mask_ws", 
                    CPL_TYPE_DOUBLE,
                    "Starting wavelength for object-sky cross correlation",
                    "sinfoni.sinfo_utl_skycor",
                    SINFO_MASK_WAVE_MIN) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-mask_ws") ;
    cpl_parameterlist_append(list, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.mask_we", 
                    CPL_TYPE_DOUBLE,
                    "End wavelength for object-sky cross correlation",
                    "sinfoni.sinfo_utl_skycor",
                    SINFO_MASK_WAVE_MAX) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-mask_we") ;
    cpl_parameterlist_append(list, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.min_frac", 
                    CPL_TYPE_DOUBLE,
                    "Threshold value for fraction of spatial pixels to be sky",
                    "sinfoni.sinfo_utl_skycor",
                    SINFO_MIN_FRAC) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-min_frac") ;
    cpl_parameterlist_append(list, p) ;



    p = cpl_parameter_new_range(
                    "sinfoni.sinfo_utl_skycor.sky_bkg_filter_width",
                    CPL_TYPE_INT,
                    "Width of sky-thermal background pre filter "
                    "(to remove emission lines before fitting a "
                    "Black Body).",
                    "sinfoni.sinfo_utl_skycor",
                    SINFO_SKY_BKG_FILTER_WIDTH,2,25) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, 
                            "skycor-sky_bkg_filter_width") ;
    cpl_parameterlist_append(list, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.line_half_width", 
                    CPL_TYPE_DOUBLE,
                    "Threshold value for full width in pixels "
                    "of unresolved emission line. Lines with FWHM "
                    "smaller than this value are not considered "
                    "in the object-sky cross correlation and in "
                    "computation of the optimal sky lines scaling "
                    "factor",
                    "sinfoni.sinfo_utl_skycor",
                    SINFO_LINE_HALF_WIDTH) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-line_hw") ;
    cpl_parameterlist_append(list, p) ;

    p = cpl_parameter_new_enum("sinfoni.sinfo_utl_skycor.scale_method", 
                    CPL_TYPE_INT,
                    "Optimal sky lines scaling factor computation "
                    "method: amoeba fit (0), "
                    "maximum likelihood (1) ",
                    "sinfoni.sinfo_utl_skycor",
                    1,2,0,1) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, 
                            "skycor-scale_method") ;
    cpl_parameterlist_append(list, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.rot_cor", 
                    CPL_TYPE_BOOL,
                    "Computes scaling factor correction due to "
                    "rotational levels transitions",
                    "sinfoni.sinfo_utl_skycor",
                    FALSE) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-rot_cor") ;
    cpl_parameterlist_append(list, p) ;



    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.fit_obj_noise", 
                    CPL_TYPE_BOOL,
                    "Do Gaussian fit of object noise",
                    "sinfoni.sinfo_utl_skycor",
                    FALSE) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-fit_obj_noise");
    cpl_parameterlist_append(list, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.niter", 
                    CPL_TYPE_INT,
                    "Number of iterations of background "
                    "fit",
                    "sinfoni.sinfo_utl_skycor",
                    10) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-niter") ;
    cpl_parameterlist_append(list, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.pshift", 
                    CPL_TYPE_DOUBLE,
                    "Sky spectrum shift towar object",
                    "sinfoni.sinfo_utl_skycor",
                    0.) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-pshift") ;
    cpl_parameterlist_append(list, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.llx", 
                    CPL_TYPE_INT,
                    "Lower left X defining object "
                    "spectrum location",
                    "sinfoni.sinfo_utl_skycor",
                    1) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-llx") ;
    cpl_parameterlist_append(list, p) ;

    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.lly", 
                    CPL_TYPE_INT,
                    "Lower left Y defining object "
                    "spectrum location",
                    "sinfoni.sinfo_utl_skycor",
                    1) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-lly") ;
    cpl_parameterlist_append(list, p) ;



    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.urx", 
                    CPL_TYPE_INT,
                    "Upper right X defining object "
                    "spectrum location",
                    "sinfoni.sinfo_utl_skycor",
                    64) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-urx") ;
    cpl_parameterlist_append(list, p) ;


    p = cpl_parameter_new_value("sinfoni.sinfo_utl_skycor.ury", 
                    CPL_TYPE_INT,
                    "Upper right Y defining object "
                    "spectrum location",
                    "sinfoni.sinfo_utl_skycor",
                    64) ;
    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "skycor-ury") ;
    cpl_parameterlist_append(list, p) ;



    p = cpl_parameter_new_value(

                    "sinfoni.sinfo_utl_skycor.sub_thr_bkg_from_obj",
                    CPL_TYPE_BOOL,
                    "Subtract thermal background contribute from "
                    "object spectra. Set it to TRUE if "
                    "stack-sub_raw_sky is set to FALSE",
                    "sinfoni.sinfo_utl_skycor",
                    FALSE) ;

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, 
                            "skycor-sub_thr_bkg_from_obj") ;
    cpl_parameterlist_append(list, p) ;


}
/**@}*/
