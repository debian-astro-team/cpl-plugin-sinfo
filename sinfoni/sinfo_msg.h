/*
 * This file is part of the ESO SINFO Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2006-11-21 11:56:10 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */
#ifndef SINFO_MSG_H
#define SINFO_MSG_H

/*
#include <sinfo_utils.h>
*/
#include <cpl_msg.h>

/* Nothing bad happens if user also calls cpl_msg_info()
 * but prevent it as a service to the user of this module
#define cpl_msg_info(...)  use__sinfo_msg__instead__of__cpl_msg_info
#define cpl_msg_indent()
 */

/*----------------------------------------------------------------------------*/
/**
 * @addtogroup sinfo_msg
 *
 */
/*----------------------------------------------------------------------------*/
/**@{*/


void sinfo_msg_init(int outlevel, const char *dom);


void sinfo_msg_set_level(int olevel);

const char *sinfo_msg_get_domain(void);
void sinfo_msg_set_domain(const char *d);


/* Convenience macros to save the user from typing function id */

/*----------------------------------------------------------------------------*/
/**
   @brief    Print an error message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_error(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define sinfo_msg_error(...) cpl_msg_error(cpl_func, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a progress message
   @param    i           See @c cpl_msg_progress()
   @param    iter        See @c cpl_msg_progress()
   @param    ...         Message to print

   This function is used instead of @c cpl_msg_progress(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define sinfo_msg_progress(i, iter, ...) \
  cpl_msg_progress(cpl_func, (i), (iter), __VA_ARGS__)
/*----------------------------------------------------------------------------*/
/**
   @brief    Print an warning message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_warning(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define sinfo_msg_warning(...) sinfo_msg_warning_macro(cpl_func, __VA_ARGS__)
/*----------------------------------------------------------------------------*/
/**
   @brief    Print a debug message
   @param    ...             Message to print

   This function is used instead of @c cpl_msg_debug(), and saves
   the user from typing the calling function name.
*/
/*----------------------------------------------------------------------------*/
#define sinfo_msg_debug(...) cpl_msg_debug(cpl_func, __VA_ARGS__)

/*----------------------------------------------------------------------------*/
/**
   @brief    Print a message on a lower message level
   @param    ...             Message to print
*/
/*----------------------------------------------------------------------------*/
#define sinfo_msg_low(...)  do {                     \
                           sinfo_msg_softer();       \
                           sinfo_msg(__VA_ARGS__);   \
                           sinfo_msg_louder();       \
                           } while (FALSE)




#define sinfo_msg(...) sinfo_msg_macro(cpl_func, __VA_ARGS__)
#define sinfo_msg_softer() sinfo_msg_softer_macro(cpl_func)
#define sinfo_msg_louder() sinfo_msg_louder_macro(cpl_func)


void sinfo_msg_macro(const char *fct, const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 2, 3)))
#endif
;

void sinfo_msg_warning_macro(const char *fct, const char *format, ...)
#ifdef __GNUC__
__attribute__((format (printf, 2, 3)))
#endif
;

int sinfo_msg_get_warnings(void);
void sinfo_msg_add_warnings(int n);

void sinfo_msg_softer_macro(const char *fct);
void sinfo_msg_louder_macro(const char *fct);

#endif /* SINFO_MSG_H */

/**@}*/
