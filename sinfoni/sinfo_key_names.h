/* $Id: sinfo_key_names.h,v 1.1 2006-10-20 08:06:32 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2006-10-20 08:06:32 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_KEY_NAMES_H
#define SINFO_KEY_NAMES_H



CPL_BEGIN_DECLS
#define KEY_NAME_CDELT1                     "CDELT1"
#define KEY_NAME_CDELT2                     "CDELT2"
#define KEY_NAME_CDELT3                     "CDELT3"

#define KEY_NAME_CRPIX1                     "CRPIX1"
#define KEY_NAME_CRPIX2                     "CRPIX2"
#define KEY_NAME_CRPIX3                     "CRPIX3"

#define KEY_NAME_CRVAL1                     "CRVAL1"
#define KEY_NAME_CRVAL2                     "CRVAL2"
#define KEY_NAME_CRVAL3                     "CRVAL3"

#define KEY_NAME_LOOP_STATE                "ESO AOS RTC LOOP STATE"
#define KEY_NAME_LOOP_LGS                  "ESO AOS RTC LOOP LGS"
#define KEY_NAME_INS1_MODE                 "ESO INS1 MODE"

#define PAF_NAME_LOOP_STATE                "AOS RTC LOOP STATE"
#define PAF_NAME_LOOP_LGS                  "AOS RTC LOOP LGS"
#define PAF_NAME_INS1_MODE                 "INS1 MODE"

#define KEY_HELP_LOOP_STATE                "Loop state, open or closed"
#define KEY_HELP_LOOP_LGS                  "LGS loop on or off"
#define KEY_HELP_INS1_MODE                 "Instrument mode used."

#define KEY_NAME_CUMOFFX                   "ESO SEQ CUMOFFSETX"
#define KEY_NAME_CUMOFFY                   "ESO SEQ CUMOFFSETY"

#define KEY_NAME_MJD_OBS                   "MJD-OBS"
#define KEY_NAME_OBS_NAME                   "ESO OBS NAME"

#define KEY_NAME_PRO_CATG                  "ESO PRO CATG"
#define PAF_NAME_PRO_CATG                  "PRO CATG"
#define KEY_HELP_PRO_CATG                  "Category of pipeline product frame"


#define KEY_NAME_DET_DIT                   "ESO DET DIT"
#define PAF_NAME_DET_DIT                   "DET DIT"
#define KEY_HELP_DET_DIT                   "Integration Time"

#define KEY_NAME_DET_NDIT                  "ESO DET NDIT"
#define PAF_NAME_DET_NDIT                  "DET NDIT"
#define KEY_HELP_DET_NDIT                  "# of Sub-Integrations"

#define KEY_NAME_FILT_NAME                 "ESO INS FILT1 NAME"
#define PAF_NAME_FILT_NAME                 "INS FILT1 NAME"
#define KEY_HELP_FILT_NAME                 "Filter name."

#define KEY_NAME_INS_SETUP                 "ESO INS SETUP ID"
#define PAF_NAME_INS_SETUP                 "INS SETUP ID"
#define KEY_HELP_INS_SETUP                 "Instrument setup identifier."

#define KEY_NAME_PRO_REC1_RAW1_NAME        "ESO PRO REC1 RAW1 NAME"

#define KEY_NAME_LAMP_XE                   "ESO INS1 LAMP1 ST"
#define KEY_NAME_LAMP_KR                   "ESO INS1 LAMP2 ST"
#define KEY_NAME_LAMP_NE                   "ESO INS1 LAMP3 ST"
#define KEY_NAME_LAMP_AR                   "ESO INS1 LAMP4 ST"
#define KEY_NAME_LAMP_HALO                 "ESO INS1 LAMP5 ST"
#define PAF_NAME_LAMP_HALO                 "INS1 LAMP5 ST"
#define KEY_HELP_LAMP_HALO                 "Lamp activated."

#define KEY_NAME_SHUT2_ST                  "ESO INS1 SHUT2 ST"
#define PAF_NAME_SHUT2_ST                  "INS1 SHUT2 ST"
#define KEY_HELP_SHUT2_ST                  "Shutter open."

#define KEY_NAME_PREOPTICS                 "ESO INS OPTI1 NAME"
#define PAF_NAME_PREOPTICS                 "INS OPTI1 NAME"
#define KEY_HELP_PREOPTICS                 "OPTIi name."

#define KEY_NAME_PIPEFILE                  "PIPEFILE"
#define KEY_HELP_PIPEFILE                  "Filename of data product"

#define KEY_NAME_PRO_TYPE                  "ESO PRO TYPE"
#define PAF_NAME_PRO_TYPE                  "PRO TYPE"
#define KEY_HELP_PRO_TYPE                  "Product Type"

#define KEY_NAME_PRO_CATG                  "ESO PRO CATG"
#define PAF_NAME_PRO_CATG                  "PRO CATG"

#define KEY_NAME_PRO_STATUS                "ESO PRO STATUS"

#define KEY_NAME_PRO_DATE                  "ESO PRO DATE"

#define KEY_NAME_DATE_OBS                  "DATE-OBS"
#define KEY_HELP_DATE_OBS                  "Observing date"

#define KEY_NAME_TEL_AIRM_START            "ESO TEL AIRM START"
#define PAF_NAME_TEL_AIRM_START            "TEL AIRM START"
#define KEY_HELP_TEL_AIRM_START            "Airmass at start"



#define KEY_NAME_ARCFILE                   "ARCFILE"
#define KEY_HELP_ARCFILE                   "Archive file name"


#define KEY_NAME_HPRO_TYPE                 "HIERARCH ESO PRO TYPE"
#define KEY_HELP_HPRO_TYPE                "product type"

#define KEY_NAME_HPRO_CATG                 "HIERARCH ESO PRO CATG"
#define KEY_HELP_HPRO_CATG                 "product category"

#define KEY_NAME_HPRO_STATUS               "HIERARCH ESO PRO STATUS"
#define KEY_HELP_HPRO_STATUS               "pipeline status"

#define KEY_NAME_HPRO_DATE                 "HIERARCH ESO PRO DATE"
#define KEY_HELP_HPRO_DATE                 "pipeline execution date"


#define KEY_NAME_HPRO_RECID                "HIERARCH ESO PRO REC ID"
#define KEY_HELP_HPRO_RECID                "recipe ID"


#define KEY_NAME_HPRO_DATANCOM             "HIERARCH ESO PRO DATANCOM"
#define KEY_NAME_HPRO_DID                  "HIERARCH ESO PRO DID"
#define KEY_VALUE_HPRO_DID                 "PRO-1.15"
#define KEY_HELP_HPRO_DID                  "Data dictionary for PRO"


#define KEY_NAME_REC1_RAW1_NAME            "ESO PRO REC1 RAW1 NAME"

#define KEY_NAME_GRAT_ENC                  "ESO INS GRAT1 ENC"

#define KEY_NAME_DPR_TYPE                  "ESO DPR TYPE"
#define PAF_NAME_DPR_TYPE                  "DPR TYPE"
#define KEY_HELP_DPR_TYPE                  "Observation type"


CPL_END_DECLS

#endif
