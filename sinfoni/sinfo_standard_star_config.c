/* $Id: sinfo_standard_star_config.c,v 1.7 2012-03-03 10:18:26 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 10:18:26 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *   Standard_Star Frames Data Reduction Parameter Initialization        *
 ****************************************************************/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include "sinfo_standard_star_config.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Define and initialize std star parameters
 *
 * TBD
 */

/**
 @name sinfo_standard_star_config_add
 @param list pointer to input parameter list
 @return void
 */
void
sinfo_standard_star_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /*
     --------------------------------------------------------------------------
     In/Out
     --------------------------------------------------------------------------
     */

    /* switch to generate the extracted spectrum */
    /*
     p = cpl_parameter_new_value("sinfoni.std_star.qc_info",
     CPL_TYPE_BOOL,
     "Switch to activate extra QC information "
     "together with the spectrum",
     "sinfoni.std_star",
     FALSE);

     cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"std_star-qc_info");
     cpl_parameterlist_append(list, p);
     */

    p = cpl_parameter_new_value("sinfoni.std_star.switch", CPL_TYPE_BOOL,
                    "Switch to activate spectrum extraction",
                    "sinfoni.std_star", TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-switch");
    cpl_parameterlist_append(list, p);

    /* the fraction [0...1] of rejected low intensity pixels when taking
     the average of columns */
    p = cpl_parameter_new_value("sinfoni.std_star.low_rejection",
                    CPL_TYPE_DOUBLE, "lower rejection", "sinfoni.std_star",
                    0.1);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-lo_rej");
    cpl_parameterlist_append(list, p);

    /* the fraction [0...1] of rejected high intensity pixels when taking
     the average of columns */
    p = cpl_parameter_new_value("sinfoni.std_star.high_rejection",
                    CPL_TYPE_DOUBLE, "high rejection", "sinfoni.std_star", 0.1);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-hi_rej");
    cpl_parameterlist_append(list, p);

    /* factor applied to the found fwhms of a 2D-Gaussian fit, defines the radius
     of the aperture inside which the spectral extraction is carried out.
     */
    p = cpl_parameter_new_value("sinfoni.std_star.fwhm_factor", CPL_TYPE_DOUBLE,
                    "Factor to find 2D-Gauss FWHM. "
                                    "The extraction box is: "
                                    "halfbox_x=halfbox_y="
                                    "fwhm_factor*(fwhm_x+fwhm_y)*0.5",
                    "sinfoni.std_star", 5.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-fwhm_fct");
    cpl_parameterlist_append(list, p);

    /* indicator if the intensity conversion factor should be determined or not
     */

    p = cpl_parameter_new_value("sinfoni.std_star.conversion_index",
                    CPL_TYPE_BOOL, "Intensity Conversion Index: ",
                    "sinfoni.std_star", TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-conv_ind");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.std_star.compute_eff", CPL_TYPE_BOOL,
                    "Compute efficiency: TRUE/FALSE", "sinfoni.std_star", TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-compute_eff");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.std_star.extract", CPL_TYPE_STRING,
                    "Extraction Method: simple/optimal", "sinfoni.std_star", "simple");

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-extract");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.std_star.pfm", CPL_TYPE_STRING,
                     "Object Mask Post Filtering Method: erosion/dilation/closing",
                     "sinfoni.std_star", "erosion");

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-pfm");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.std_star.pfx", CPL_TYPE_INT,
                     "X Size of the post filtering kernel.",
                     "sinfoni.std_star", 3);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-pfx");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.std_star.pfy", CPL_TYPE_INT,
                     "Y Size of the post filtering kernel.",
                     "sinfoni.std_star", 3);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "std_star-pfy");
    cpl_parameterlist_append(list, p);


}
/**@}*/
