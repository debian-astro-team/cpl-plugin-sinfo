/* $Id: sinfo_pro_types.h,v 1.10 2010-02-12 17:57:38 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later verrtd sion.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-02-12 17:57:38 $
 * $Revision: 1.10 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_PRO_TYPES_H
#define SINFO_PRO_TYPES_H

CPL_BEGIN_DECLS


#define REF_BP_MAP_HP                      "RBP_MAP_HP"
#define REF_BP_MAP_NL                      "RBP_MAP_NL"
#define REF_MASTER_DARK                    "RMASTER_DARK"
#define REF_MASTER_FLAT_LAMP               "RMASTER_FLAT_LAMP"
#define REF_DISTORTION                     "RDISTORTION"
#define REF_SLITLETS_DISTANCE              "RSLITLETS_DISTANCE"

#define PRO_LIN_DET_INFO                   "LIN_DET_INFO"
#define PRO_GAIN_INFO                      "GAIN_INFO"
#define PRO_AO_INFO                        "AO_INFO"
#define PRO_AO_PERFORMANCE                 "AO_PERFORMANCE"
#define PRO_ENC_ENERGY                     "ENC_ENERGY"
#define PRO_OBS_SKY                        "OBS_SKY"
#define PRO_SKY_MED                        "SKY_MED"
#define PRO_SKY_DUMMY                      "SKY_DUMMY"
#define PRO_SKY_STACKED_DUMMY              "SKY_STACKED_DUMMY"
#define PRO_DEFAULT                        "DEFAULT"
#define PRO_INT_COL_TILT_COR               "INT_COL_TILT_COR"
#define PRO_STD_STACKED                    "STD_STACKED"
#define PRO_SKY_STD_STACKED                "SKY_STD_STACKED"
#define PRO_SKY_OH_STACKED                 "SKY_OH_STACKED"
#define PRO_SKY_PSF_CALIBRATOR_STACKED     "SKY_PSF_CALIBRATOR_STACKED"
#define PRO_STD_STAR_STACKED               "STD_STAR_STACKED"
#define PRO_STD_STAR_DITHER_STACKED        "STD_STAR_DITHER_STACKED"
#define PRO_SKY_STACKED                    "SKY_STACKED"
#define PRO_NODDING_STACKED                "_NODDING_STACKED"
#define PRO_ILLUMCORR                      "ILLUMCORR"

#define SINFO_UTL_STDSTARS_RES             "STDSTARS_CATS"
#define SINFO_UTL_SEDS_RES                 "SEDS"
#define SINFO_CALIB_STDSTARS               "STDSTARS_CATS"
#define SINFO_CALIB_SED                    "SEDS"

#define PRO_STACKED                        "_STACKED"

#define PRO_STACK_SKY_DIST                 "SKY_STACKED_DIST"
#define PRO_STACK_MFLAT_DIST               "MFLAT_STACKED_DIST"
#define PRO_STACK_MFLAT_DITHER_DIST        "MFLAT_DITHER_STACKED_DIST"

#define PRO_MFLAT_CUBE                     "MFLAT_CUBE"
#define PRO_MFLAT_AVG                      "MFLAT_AVG"
#define PRO_MFLAT_MED                      "MFLAT_MED"


#define PRO_ILL_COR                        "ILLUMINATION_CORRECTION"
#define PRO_BP_MAP                         "BP_MAP"
#define PRO_BP_MAP_HP                      "BP_MAP_HP"
#define PRO_BP_MAP_NL                      "BP_MAP_NL"
#define PRO_BP_MAP_NO                      "BP_MAP_NO"
#define PRO_BP_MAP_DI                      "BP_MAP_DI"
#define PRO_BP_MAP_SKY                     "BP_MAP_SKY"
#define PRO_MASTER_BP_MAP                  "MASTER_BP_MAP"
#define PRO_BP_MAP                         "BP_MAP"
#define PRO_MASTER_DARK                    "MASTER_DARK"
#define PRO_SLOPE                          "SLOPE"
#define PRO_DISTORTION                     "DISTORTION"
#define PRO_SLITLETS_DISTANCE              "SLITLETS_DISTANCE"
#define PRO_MASTER_SLIT                    "MASTER_SLIT"
#define PRO_MASTER_FLAT_LAMP               "MASTER_FLAT_LAMP"
#define PRO_MASTER_FLAT_LAMP1              "MASTER_FLAT_LAMP1"
#define PRO_MASTER_FLAT_LAMP2              "MASTER_FLAT_LAMP2"
#define PRO_SLIT_POS                       "SLIT_POS"
#define PRO_REF_ATM_REF_CORR               "ATM_REF_CORR"
#define PRO_SLITLETS_POS_PREDIST           "SLITLETS_POS_PREDIST"
#define PRO_SLIT_POS_GUESS                 "SLIT_POS_GUESS"
#define PRO_FIBRE_EW_STACKED               "FIBRE_EW_STACKED"
#define PRO_FIBRE_NS_STACKED_ON            "FIBRE_NS_STACKED_ON"
#define PRO_FIBRE_NS_STACKED_OFF           "FIBRE_NS_STACKED_OFF"
#define PRO_FIBRE_NS_STACKED               "FIBRE_NS_STACKED"
#define PRO_FIBRE_NS_STACKED_DIST          "FIBRE_NS_STACKED_DIST"
#define PRO_FIBRE_LAMP_STACKED             "FIBRE_LAMP_STACKED"
#define PRO_SLIT_LAMP_STACKED              "SLIT_LAMP_STACKED"
#define PRO_FLUX_LAMP_STACKED              "FLUX_LAMP_STACKED"
#define PRO_WAVE_LAMP_STACKED              "WAVE_LAMP_STACKED"
#define PRO_WAVE_SLITPOS_STACKED           "WAVE_LAMP_SLITPOS_STACKED"
#define PRO_WAVE_LAMP_DITHER_STACKED       "WAVE_LAMP_DITHER_STACKED"
#define PRO_WAVE_NS_STACKED                "WAVE_NS_STACKED"
#define PRO_WAVE_NS_DITHER_STACKED         "WAVE_NS_DITHER_STACKED"
#define PRO_WAVE_PAR_LIST                  "WAVE_FIT_PARAMS"
#define PRO_WAVE_COEF_SLIT                 "WAVE_COEF_SLIT"
#define PRO_PSF_CALIBRATOR_STACKED         "PSF_CALIBRATOR_STACKED"
#define PRO_FOCUS_STACKED                  "FOCUS_STACKED"
#define PRO_OBJECT_NODDING_STACKED         "OBJECT_NODDING_STACKED"
#define PRO_OBJECT_SKYSPIDER_STACKED       "OBJECT_SKYSPIDER_STACKED"


#define PRO_RESAMPLED_WAVE                 "RESAMPLED_WAVE"
#define PRO_RESAMPLED_OBJ                  "RESAMPLED_OBJ"
#define PRO_RESAMPLED_SKY                  "RESAMPLED_SKY"
#define PRO_RESAMPLED_FLAT_LAMP            "RESAMPLED_FLAT_LAMP"

#define PRO_OBS_CUBE_SKY                   "OBS_CUBE_SKY"
#define PRO_STD_CUBE_SKY                   "STD_CUBE_SKY"
#define PRO_PSF_CUBE_SKY                   "PSF_CUBE_SKY"
#define PRO_PUPIL_CUBE_SKY                 "PUPIL_CUBE_SKY"

#define PRO_PUPIL_CUBE                     "PUPIL_LAMP_CUBE"


#define PRO_OBS_MED_SKY                    "OBS_MED_SKY"
#define PRO_STD_MED_SKY                    "STD_MED_SKY"
#define PRO_PSF_MED_SKY                    "PSF_MED_SKY"
#define PRO_PUPIL_MED_SKY                  "PUPIL_MED_SKY"

#define PRO_PUPIL_LAMP_STACKED             "PUPIL_LAMP_STACKED"
#define PRO_SKY_NODDING_STACKED            "SKY_NODDING_STACKED"
#define PRO_STD_NODDING_STACKED            "STD_NODDING_STACKED"
#define PRO_MASTER_LAMP_SPEC               "MASTER_LAMP_SPEC"
#define PRO_MASTER_TWIFLAT                 "MASTER_TWIFLAT"
#define PRO_COEFF_LIST                     "COEFF_LIST"
#define PRO_INDEX_LIST                     "INDEX_LIST"
#define PRO_HALO_SPECT                     "HALO_SPECT"
#define PRO_FIRST_COL                      "FIRST_COL"
#define PRO_MASK_CUBE                      "MASK_CUBE"  /* not used*/
#define PRO_PSF                            "MASTER_PSF"

#define TMP_FOCUS                          "FOCUS_STACKED"
#define TMP_FOCUS_ON                       "FOCUS_ON_STACKED"
#define TMP_FOCUS_OFF                      "FOCUS_OFF_STACKED"

#define PRO_FOCUS                          "MASTER_FOCUS"
#define PRO_FOCUS_GAUSS                    "FOCUS_GAUSS"
#define PRO_WAVE_MAP                       "WAVE_MAP"
#define PRO_STD_STAR_SPECTRA               "STD_STAR_SPECTRA"
#define PRO_STD_STAR_SPECTRA_FLUXCAL       "STD_STAR_SPECTRA_FLUXCAL"
#define PRO_STD_STAR_SPECTRUM              "STD_STAR_SPECTRUM"

#define PRO_CUBE                           "CUBE"
#define PRO_IMA                            "IMA"
#define PRO_SPECTRUM                       "SPECTRUM"
#define PRO_COADD_SKY                      "COADD_SKY"
#define PRO_COADD_PSF                      "COADD_PSF"
#define PRO_COADD_STD                      "COADD_STD"
#define PRO_COADD_OBJ                      "COADD_OBJ"
#define PRO_COADD_PUPIL                    "COADD_PUPIL"



#define PRO_COADD_SKY_FLUXCAL              "COADD_SKY_FLUXCAL"
#define PRO_COADD_PSF_FLUXCAL              "COADD_PSF_FLUXCAL"
#define PRO_COADD_STD_FLUXCAL              "COADD_STD_FLUXCAL"
#define PRO_COADD_OBJ_FLUXCAL              "COADD_OBJ_FLUXCAL"

#define PRO_OBS_PSF                        "OBS_PSF"
#define PRO_OBS_STD                        "OBS_STD"
#define PRO_OBS_OBJ                        "OBS_OBJ"
#define PRO_OBS_PUPIL                      "OBS_PUPIL"
#define PRO_SPECTRA_QC                     "SPECTRA_QC"

#define PRO_MED_COADD_PSF                  "MED_COADD_PSF"
#define PRO_MED_COADD_STD                  "MED_COADD_STD"
#define PRO_MED_COADD_OBJ                  "MED_COADD_OBJ"
#define PRO_MED_COADD_PUPIL                "MED_COADD_PUPIL"


#define PRO_MED_OBS_PSF                    "MED_OBS_PSF"
#define PRO_MED_OBS_STD                    "MED_OBS_STD"
#define PRO_MED_OBS_OBJ                    "MED_OBS_OBJ"
#define PRO_MED_OBS_PUPIL                  "MED_OBS_PUPIL"


#define PRO_CUBE_COLL                      "CUBE_COLL"
#define PRO_SLOPEX                         "CUBE_COLL_SLOPEX"
#define PRO_SLOPEY                         "CUBE_COLL_SLOPEY"
#define PRO_MASK_CUBE                      "MASK_CUBE"
#define PRO_MASK_COADD_PSF                 "MASK_COADD_PSF"
#define PRO_MASK_COADD_STD                 "MASK_COADD_STD"
#define PRO_MASK_COADD_OBJ                 "MASK_COADD_OBJ"
#define PRO_MASK_COADD_PUPIL               "MASK_COADD_PUPIL"
#define PRO_OBJ_CUBE                       "OBJ_CUBE"
#define PRO_BP_COEFF                       "BP_COEFF"
#define PRO_EFFICIENCY                     "EFFICIENCY"
#define PRO_RESPONSE                       "RESPONSE"
CPL_END_DECLS
#endif
