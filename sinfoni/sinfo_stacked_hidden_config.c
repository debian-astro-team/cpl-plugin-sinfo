/* $Id: sinfo_stacked_hidden_config.c,v 1.5 2012-03-03 10:18:26 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 10:18:26 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

/**************************************************************************
 *  Prepare_Stacked_Frames Frames Data Reduction Parameter Initialization *
 **************************************************************************/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_stacked_hidden_config.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Objnod structure configuration
 *
 * TBD
 */


/**
@name sinfo_stacked_hidden_config_add
@param list pointer to input parameter list]
@return void
 */
void
sinfo_stacked_hidden_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }


    /*
  --------------------------------------------------------------------------
  In/Out  
  --------------------------------------------------------------------------
     */



    /* Output file name */
    /* output name of resulting fits wavelength map */
    p = cpl_parameter_new_value("sinfoni.stacked.output_filename",
                    CPL_TYPE_STRING,
                    "Output File Name: ",
                    "sinfoni.stacked",
                    "out_stack.fits");


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"out-stack_filename");
    cpl_parameterlist_append(list, p);




}
