/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------

   File name     :    tilt_cfg.c
   Autor    :       Juergen Schreiber
   Created on    :    October 2001
   Description    :    handles the data structure tilt_config

 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_tilt_cfg.h"
/**@{*/
/**
 * @defgroup sinfo_tilt_cfg tilt computation
 *
 * TBD
 */

/*---------------------------------------------------------------------------
                              Function codes
 ---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
   Function :   sinfo_tilt_cfg_create()
   In       :   void
   Out      :   pointer to allocated base tilt_config structure
   Job      :   allocate memory for a tilt_config struct
   Notice   :   only the main (base) structure is allocated
   TODO: not used
 ---------------------------------------------------------------------------*/

tilt_config * sinfo_tilt_cfg_create(void)
{
    return cpl_calloc(1, sizeof(tilt_config));
}


/*---------------------------------------------------------------------------
   Function :   sinfo_tilt_cfg_destroy()
   In       :   tilt_config to deallocate
   Out      :   void
   Job      :   deallocate all memory associated with a 
                tilt_config data structure
   Notice   :   
   TODO: not used
 ---------------------------------------------------------------------------*/

void sinfo_tilt_cfg_destroy(tilt_config * sc)
{
    if (sc==NULL) return ;
    /* cpl_free(sc->frametype);*/

    /* Free main struct */
    cpl_free(sc);

    return ;
}


/**@}*/

