#ifndef SINFO_NEW_ADD_BP_MAP_H
#define SINFO_NEW_ADD_BP_MAP_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_add_bp_map.h,v 1.7 2007-09-21 14:13:43 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  13/10/04  created
*/

/************************************************************************
 * sinfo_add_bp_map.h
 * routines to search for bad pixels
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h>
#include <sinfo_hidden.h>  
#include "sinfo_msg.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/
/**
   @name       sinfo_new_add_bp_map()
   @param      plugin_id  recipe name
   @param      config     input parameter list
   @param      set        input set of frames

   @return     integer (0 if it worked, -1 if it doesn't) 
*/

int 
sinfo_new_add_bp_map(const char* plugin_id, 
                     cpl_parameterlist* config, 
                     cpl_frameset* set,
                     cpl_frameset* ref_set);


#endif 

/*--------------------------------------------------------------------------*/
