#ifndef SINFO_UTILITIES_SCIRED_H
#define SINFO_UTILITIES_SCIRED_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
#include <cpl.h>
#include "sinfo_tpl_utils.h"
#include "sinfo_key_names.h"
#include "sinfo_pro_types.h"
#include "sinfo_msg.h"
#include "sinfo_cube_construct.h"
#include "sinfo_utilities.h"
#include "sinfo_object_cfg.h"

float*
sinfo_read_distances(const int nslits, const char* distlist);

float** 
sinfo_read_slitlets_edges(const int nslits, const char* poslist);

int
sinfo_auto_size_cube_new(object_config * cfg,
                    float* ref_offx, float* ref_offy,
                    float* min_offx, float* min_offy,
                    float* max_offx, float* max_offy);
int 
sinfo_check_input_data(object_config* cfg);  

int
sinfo_auto_size_cube(float* offsetx, 
                      float* offsety,
              const int nframes, 
                    float* ref_offx, 
                    float* ref_offy,
                    int* size_x,
		     int* size_y);

int
sinfo_auto_size_cube4(float* offsetx, 
                      float* offsety,
              const int nframes, 
                    float* ref_offx, 
                    float* ref_offy,
                    int* size_x,
              int* size_y);



int
sinfo_auto_size_cube5(object_config * cfg, 
                    float* ref_offx, float* ref_offy,
                    float* min_offx, float* min_offy,
              float* max_offx, float* max_offy);

cpl_imagelist * 
sinfo_new_cube_getvig(
        cpl_imagelist *       cube_in,
        int             loleft_x,
        int             loleft_y,
        int             upright_x,
        int             upright_y);


int
sinfo_new_set_wcs_cube(cpl_imagelist* cub, const char* name, double clambda, 
         double dis, double cpix, double cx, double cy);

int
sinfo_new_set_wcs_image(cpl_image* img, const char* name,  
                        double cx, double cy);

int
sinfo_new_set_wcs_spectrum(cpl_image* img, const char* name,   
                           double clambda, double dis, double cpix);



cpl_imagelist* 
sinfo_new_fine_tune(cpl_imagelist* cube,float* correct_dist, 
                    const char* method, const int order, const int nslits) ;

int
sinfo_assign_offset_usr(const int n,
                        const char* name,
                        float* offsetx,
                        float* offsety,
                        const float ref_offx,
                        const float ref_offy);

int
sinfo_assign_offset_from_fits_header(const int n,const char* name,float* offsetx,
                        float* offsety,const float ref_offx,
                        const float ref_offy);

int 
sinfo_new_sinfoni_correct_median_it(cpl_imagelist** inp);


int sinfo_calib_flux_std(
        const char  *   seds_file,
        const char  *   stdstars,
        const char  *   filter,
        cpl_frame   *   frame,
        cpl_table   *   tab,
        double          mag);


sinfo_band sinfo_get_associated_filter(const char * f);
sinfo_band sinfo_get_bbfilter(const char * f);
int
sinfo_set_wcs_cal_image(cpl_image* img,
                        const char* name,
                        double crpix1,
                        double crval1,
                        double cdelt1,
                        double crpix2,
                        double crval2,
                        double cdelt2);
#endif
