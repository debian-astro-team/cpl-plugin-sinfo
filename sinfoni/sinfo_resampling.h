/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name     :    sinfo_resampling.h
   Author         :    Nicolas Devillard
   Created on    :    Jan 04, 1996
   Description    :    resampling routines

 ---------------------------------------------------------------------------*/

/*

 $Id: sinfo_resampling.h,v 1.4 2007-06-06 07:10:45 amodigli Exp $
 $Author: amodigli $
 $Date: 2007-06-06 07:10:45 $
 $Revision: 1.4 $

 */

#ifndef SINFO_RESAMPLING_H
#define SINFO_RESAMPLING_H

/*---------------------------------------------------------------------------
                                  Includes
 ---------------------------------------------------------------------------*/

#include <cpl.h>
#include "sinfo_msg.h"
/*---------------------------------------------------------------------------
                                  Defines
 ---------------------------------------------------------------------------*/
#define TRANSFO_AFFINE          0
#define TRANSFO_DEG2            1
#define TRANSFO_HOMOGRAPHIC     2

/* Number of pixels set to 0 by the shift resampling */
#define    SHIFT_REJECT_L            2
#define    SHIFT_REJECT_R            2
#define    SHIFT_REJECT_T            2
#define    SHIFT_REJECT_B            2

/*
 * Kernel definition in terms of sampling
 */


/* Number of tabulations in kernel  */
#define TABSPERPIX      (1000)
#define KERNEL_WIDTH    (2.0)
#define KERNEL_SAMPLES  (1+(int)(TABSPERPIX * KERNEL_WIDTH))

#define TANH_STEEPNESS    (5.0)


/*---------------------------------------------------------------------------
                         Function ANSI C prototypes
 ---------------------------------------------------------------------------*/

/**
  @name     sinfo_generate_interpolation_kernel
  @memo     Generate an interpolation kernel to use in this module.
  @param    kernel_type     Type of interpolation kernel.
  @return   1 newly allocated array of doubles.
  @doc
 
  Provide the name of the kernel you want to generate. Supported kernel
  types are:
 
  \begin{tabular}{ll}
  NULL          &   default kernel, currently "tanh" \\
  "default"     &   default kernel, currently "tanh" \\
  "tanh"        &   Hyperbolic tangent \\
  "sinc2"       &   Square sinc \\
  "lanczos"     &   Lanczos2 kernel \\
  "hamming"     &   Hamming kernel \\
  "hann"        &   Hann kernel
  \end{tabular}
 
  The returned array of doubles is ready of use in the various re-sampling
  functions in this module. It must be deallocated using free().
 */

double   *
sinfo_generate_interpolation_kernel(const char * kernel_type) ;


/**
  @name     sinfo_sinc
  @memo     Cardinal sine.
  @param    x   double value.
  @return   1 double.
  @doc
 
  Compute the value of the function sinfo_sinc(x)=sin(pi*x)/(pi*x) at the
  requested x.
 */

double
sinfo_sinc(double x) ;

/**
  @name     sinfo_invert_linear_transform
  @memo     Invert a linear transformation.
  @param    trans   Transformation to sinfo_invert.
  @return   1 newly allocated array of 6 doubles.
  @doc
 
  Given 6 parameters a, b, c, d, e, f defining a linear transform such as:
  \begin{verbatim}
  u = ax + by + c
  v = dx + ey + f
  \end{verbatim}
 
  The inverse transform is also linear, and is defined by:
  \begin{verbatim}
  x = Au + Bv + C
  y = Du + Ev + F
  \end{verbatim}
 
  where:
 
  \begin{verbatim}
  if G = (ae-bd)
 
  A =  e/G
  B = -b/G
  C = (bf-ce)/G
  D = -d/G
  E =  a/G
  F = (cd-af)/G
  \end{verbatim}
 
  Notice that if G=0 (ae=bd) the transform cannot be reversed.
 
  The returned array must be deallocated using free().
 */

double *
sinfo_invert_linear_transform(double *trans) ;
#endif
