/* $Id: sinfo_utl_spectrum_divide_by_blackbody.c,v 1.8 2012-03-03 10:17:31 amodigli Exp $
 *
 * This file is part of the IIINSTRUMENT Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-03-03 10:17:31 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*----------------------------------------------------------------------------
 Includes
 ----------------------------------------------------------------------------*/

#include <string.h>
#include "sinfo_utl_spectrum_divide_by_blackbody.h"
#include <sinfo_spectrum_ops.h>
#include "sinfo_key_names.h"
#include "sinfo_error.h"
#include "sinfo_utils_wrappers.h"
#include "sinfo_msg.h"
/*----------------------------------------------------------------------------
 Functions prototypes
 ----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
 Static variables
 ----------------------------------------------------------------------------*/

/**@{*/
/**
 * @addtogroup sinfo_utl_spectrum_divide_by_blackbody Utility to divide a \
   spectrum by a black body
 *
 * TBD
 */

/*----------------------------------------------------------------------------
 Functions code
 ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
 @brief    Get the command line options and execute the data reduction
 @param    parlist     the parameters list
 @param    framelist   the frames list
 @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
int
sinfo_utl_spectrum_divide_by_blackbody(cpl_parameterlist * parlist,
                                       cpl_frameset * framelist)
{
    cpl_parameter * param = NULL;
    const char * name_i = NULL;
    const char * name_o = NULL;

    double temp = 0;

    cpl_frame * frm_spct = NULL;

    cpl_propertylist * plist = NULL;

    cpl_frame * product_frame = NULL;
    cpl_image * bb_img = NULL;
    cpl_image * image_o = NULL;
    cpl_image * image_i = NULL;

    Vector* bb = NULL;
    /* double * ker=NULL; */

    /* HOW TO RETRIEVE INPUT PARAMETERS */
    /* --stropt */
    name_o = "out_ima.fits";

    /* --doubleopt */

    check_nomsg(
                    param =
                                    cpl_parameterlist_find(parlist,
                                                    "sinfoni.sinfo_utl_spectrum_divide_by_blackbody.temperature"));
    check_nomsg(temp = cpl_parameter_get_double(param));

    /* HOW TO ACCESS INPUT DATA */
    check(
                    frm_spct = cpl_frameset_find(framelist, SI_UTL_SPECTRUM_DIVIDE_BY_BLACKBODY_SPECTRUM),
                    "SOF does not have a file tagged as %s",
                    SI_UTL_SPECTRUM_DIVIDE_BY_BLACKBODY_SPECTRUM);

    check(plist=cpl_propertylist_load(cpl_frame_get_filename(frm_spct),
                                    0),"Cannot read the FITS header");

/* Now performing the data reduction */
/* Let's generate one image for the example */
check_nomsg(name_i = cpl_frame_get_filename(frm_spct));
check_nomsg(image_i = cpl_image_load((char* )name_i, CPL_TYPE_FLOAT, 0, 0));
cknull_nomsg(bb = sinfo_new_blackbody_spectrum((char* )name_i, temp));
cknull_nomsg(bb_img = sinfo_new_vector_to_image(bb));
cknull_nomsg(image_o = sinfo_new_div_image_by_spectrum(image_i, bb_img));

/* HOW TO SAVE A PRODUCT ON DISK  */
/* Set the file name */

/* Create product frame */
check_nomsg(product_frame = cpl_frame_new());
check_nomsg(cpl_frame_set_filename(product_frame, name_o));
check_nomsg(
            cpl_frame_set_tag(product_frame, SI_UTL_SPECTRUM_DIVIDE_BY_BLACKBODY_PROSPECTRUM));
check_nomsg(cpl_frame_set_type(product_frame, CPL_FRAME_TYPE_IMAGE));
check_nomsg(cpl_frame_set_group(product_frame, CPL_FRAME_GROUP_PRODUCT));
check_nomsg(cpl_frame_set_level(product_frame, CPL_FRAME_LEVEL_FINAL));

/* Add DataFlow keywords */
/*
 check(cpl_dfs_setup_product_header(plist, product_frame,
 framelist, parlist,
 "sinfo_utl_spectrum_divide_by_blackbody", "SINFONI",
 KEY_VALUE_HPRO_DID),"Problem in the product DFS-compliance") ;
 */

/* Save the file */
check(
                cpl_image_save(image_o, name_o, CPL_BPP_IEEE_FLOAT, plist, CPL_IO_DEFAULT),
                "Could not save product");

/* Log the saved file in the input frameset */
check_nomsg(cpl_frameset_insert(framelist, product_frame));

cleanup:

sinfo_free_propertylist(&plist);
sinfo_free_image(&image_i);
sinfo_free_image(&image_o);
sinfo_free_image(&bb_img);

/* Return */
if (cpl_error_get_code())
    return -1;
else
    return 0;
}
/**@}*/
