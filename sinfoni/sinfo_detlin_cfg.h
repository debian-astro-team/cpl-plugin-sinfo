/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_detlin_cfg.h
   Author         :    Juergen Schreiber
   Created on    :    April 2002
   Description    :    sinfo_detlin_ini definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_DETLIN_CFG_H
#define SINFO_DETLIN_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  bad pixels search blackboard container

  This structure holds all information related to the bad pixels search
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct detlin_config {
/*-------General---------*/
        char inFile[FILE_NAME_SZ] ; /* file name of the file containing 
                                       the list of all input frames */
        char outName[FILE_NAME_SZ] ; /* output name of resulting bad pixel 
                                        mask (fits file)*/
        char ** framelist ; /* list of frames */
        int     nframes ; /* number of frames in frame list */

/*------ Response------*/
        /* order of the fit polynomial */
        int order ;      
        /* factor to the standard deviation of the zero and slope polynomial 
           coefficient. if the deviation exceeds the resulting value the 
           corresponding pixel is declared as bad */ 
        double threshSigmaFactor ;      
        /*if a non-linear coefficient exceeds this value the corresponding 
          pixel is declared as bad*/
        double nonlinearThresh ;      
        /* percentage of rejected low intensity pixels before determining
           image statistics (mean and standard deviation)*/
        float loReject ;
        /* percentage of rejected high intensity pixels before determining
           image statistics (mean and standard deviation) */
        float hiReject ;
        /*name of the data cube storing the found polynomial coefficients*/
        char coeffsCubeName[FILE_NAME_SZ] ;
} detlin_config ;

/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name   sinfo_detlin_cfg_create()
   @memo   allocate memory for a detlin_cfg struct
   @return pointer to allocated base detlin_cfg structure
   @note   only the main (base) structure is allocated
*/

detlin_config * 
sinfo_detlin_cfg_create(void);

/**
  @name   sinfo_detlin_cfg_destroy()
  @memo   deallocate all memory associated with a detlin_config data structure
  @param  detlin_config to deallocate
  @return void
*/
void 
sinfo_detlin_cfg_destroy(detlin_config * sc);

#endif
