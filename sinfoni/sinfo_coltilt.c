/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
 * E.S.O. - VLT project
 *
 *
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * schreib  19/12/00  created
 */

/************************************************************************
 *   NAME
 *        sinfo_coltilt.c
 *------------------------------------------------------------------------
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#define POSIX_SOURCE 1
#include "sinfo_vltPort.h"

/*
 * System Headers
 */

/*
 * Local Headers
 */
#include "sinfo_error.h"
#include "sinfo_coltilt.h"
#include "sinfo_new_resampling.h"
#include "sinfo_fit_curve.h"
#include "sinfo_functions.h"
/**@{*/
/**
 * @defgroup sinfo_coltilt Column tilt computation
 *
 * procedures to correct for tilted spectra
 */


/**
@name sinfo_new_image_warp_fits
@memo correct optical distortions

@param image distorted image
@param kernel_type type of kernel to correct distortions
@param poly_table table containing distortion coefficients

@return image distortion corrected
 */
cpl_image * 
sinfo_new_image_warp_fits( cpl_image * image,
                           const char      * kernel_type,
                           const char      * poly_table )
{
    cpl_image  * warped=NULL;
    /* Following are for polynomial transforms */
    cpl_polynomial    * poly_u=NULL;        /* polynomial definition */
    cpl_polynomial    * poly_v=NULL;        /* polynomial definition */

    cpl_vector      *   profile=NULL ;
    cpl_size local_pow[2];

    /*fscanf(poly_in,"%s",poly_string);*/
    /* sinfo_msg("%s",poly_string); */

    poly_u = cpl_polynomial_new(2);
    if (poly_u == NULL) {
        sinfo_msg_error("cannot read 2D poly from arc table") ;
        return NULL ;
    }

    if (poly_u != NULL) {
        sinfo_msg_debug("Get the arc distortion from the file %s",
                        poly_table);
        if(sinfo_is_fits_file(poly_table) != 1) {
            sinfo_msg_error("Input file %s is not FITS",poly_table);
            return NULL;
        }
        cpl_table* poly_tbl=NULL;
        if(NULL==(poly_tbl = cpl_table_load(poly_table,1,0))) {
            sinfo_msg_error("cannot load the arc table") ;
            cpl_polynomial_delete(poly_u) ;
            return NULL ;
        }

        for (int i=0 ; i<cpl_table_get_nrow(poly_tbl) ; i++) {
            local_pow[0] = cpl_table_get_int(poly_tbl, "degx", i, NULL) ; 
            local_pow[1] = cpl_table_get_int(poly_tbl, "degy", i, NULL) ;
            cpl_polynomial_set_coeff(poly_u, local_pow,
                                     cpl_table_get_double(poly_tbl, "coeff", i, NULL)) ;
        }

        cpl_table_delete(poly_tbl) ;
    } else {
        sinfo_msg("Use the ID polynomial for the arc dist") ;
        local_pow[0] = 1 ;
        local_pow[1] = 0 ;
        cpl_polynomial_set_coeff(poly_u, local_pow, 1.0) ;
    }

    poly_v=cpl_polynomial_new(2);
    local_pow[0]=0;
    local_pow[1]=1;

    cpl_polynomial_set_coeff(poly_v,local_pow,1.0);
    cknull_nomsg(profile = cpl_vector_new(CPL_KERNEL_DEF_SAMPLES)) ;

    ck0_nomsg(cpl_vector_fill_kernel_profile(profile, CPL_KERNEL_TANH,
                                   CPL_KERNEL_DEF_WIDTH)) ;

    cknull_nomsg(warped=sinfo_new_warp_image_generic(image,kernel_type,poly_u,poly_v));

    /* YVES WAY 
      warped = cpl_image_new(cpl_image_get_size_x(image),
                                 cpl_image_get_size_y(image),
                                 CPL_TYPE_FLOAT);

    if (cpl_image_warp_polynomial(warped, image, poly_u, poly_v, 
                      profile,CPL_KERNEL_DEF_WIDTH,
                      profile,CPL_KERNEL_DEF_WIDTH)
        != CPL_ERROR_NONE) {
            sinfo_msg_error("cannot correct the distortion") ;
            cpl_image_delete(warped) ;
            cpl_polynomial_delete(poly_u) ;
            cpl_polynomial_delete(poly_v) ;
            cpl_vector_delete(profile) ;
            return NULL;
    }
     */

    cpl_vector_delete(profile) ;
    if (poly_u!=NULL) cpl_polynomial_delete(poly_u);
    if (poly_v!=NULL) cpl_polynomial_delete(poly_v);

    cleanup:
    return warped;
}

/**@}*/
