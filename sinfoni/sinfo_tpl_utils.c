/* $Id: sinfo_tpl_utils.c,v 1.4 2013-07-15 08:13:35 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-07-15 08:13:35 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
                                                                Includes
 -----------------------------------------------------------------------------*/
#include <string.h>
#include <cpl.h>

#include "sinfo_tpl_utils.h"
#include "sinfo_globals.h"

/**@{*/
/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_tpl_utils     Miscellaneous Utilities
 */
/*----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
  @brief        Get the pipeline copyright and license
  @return   The copyright and license string

  The function returns a pointer to the statically allocated license string.
  This string should not be modified using the returned pointer.
 */
/*----------------------------------------------------------------------------*/
const char * sinfo_get_license(void)
{
    const char *   sinfoni_license =
                    "This file is part of the SINFONI Instrument Pipeline\n"
                    "Copyright (C) 2002,2003 European Southern Observatory\n"
                    "\n"
                    "This program is free software; you can redistribute it and/or modify\n"
                    "it under the terms of the GNU General Public License as published by\n"
                    "the Free Software Foundation; either version 2 of the License, or\n"
                    "(at your option) any later version.\n"
                    "\n"
                    "This program is distributed in the hope that it will be useful,\n"
                    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                    "GNU General Public License for more details.\n"
                    "\n"
                    "You should have received a copy of the GNU General Public License\n"
                    "along with this program; if not, write to the Free Software\n"
                    "Foundation, Inc., 59 Temple Place, Suite 330, Boston, \n"
                    "MA  02111-1307  USA" ;

    return sinfoni_license ;
}


/*---------------------------------------------------------------------------*/
/**
   @brief   Extract the frames with the given tag from a frameset
   @param   in      A non-empty frameset
   @param   tag     The tag of the requested frames   
   @return  The newly created frameset or NULL on error

   The returned frameset must be de allocated with cpl_frameset_delete()
 */
/*---------------------------------------------------------------------------*/
cpl_frameset * sinfo_extract_frameset(
                const cpl_frameset  *   in,
                const char          *   tag)
{
    cpl_frameset    *   out ;
    cpl_frame       *   loc_frame ;
    int                 nbframes, nbext ;
    int                 i ;

    /* Test entries */
    if (in == NULL) return NULL ;
    if (tag == NULL) return NULL ;

    /* Initialise */
    nbframes = cpl_frameset_get_size(in) ;

    /* Count the frames with the tag */
    if ((nbext = cpl_frameset_count_tags(in, tag)) == 0) return NULL ;

    /* Create the output frameset */
    out = cpl_frameset_new() ;

    /* Loop on the requested frames and store them in out */
    nbext = 0 ;
    for (i=0 ; i<nbframes ; i++) {
        const cpl_frame* cur_frame = cpl_frameset_get_frame_const(in, i) ;
        if (!strcmp(cpl_frame_get_tag(cur_frame), tag)) {
            loc_frame = cpl_frame_duplicate(cur_frame) ;
            cpl_frameset_insert(out, loc_frame) ;
            nbext ++ ;
        }
    }
    return out ;
}


/*-------------------------------------------------------------------------*/
/**
  @brief    Return a band name
  @param    band    a BB
  @return   1 pointer to a static band name.
  TODO: not used
 */
/*--------------------------------------------------------------------------*/
const char * sinfo_std_band_name(sinfo_band band)
{
    switch (band) {
    case SINFO_BAND_J:        return "J" ;
    case SINFO_BAND_JS:       return "Js" ;
    case SINFO_BAND_JBLOCK:   return "J+Block" ;
    case SINFO_BAND_H:        return "H" ;
    case SINFO_BAND_K:        return "K" ;
    case SINFO_BAND_KS:       return "Ks" ;
    case SINFO_BAND_L:        return "L" ;
    case SINFO_BAND_M:        return "M" ;
    case SINFO_BAND_LP:       return "Lp" ;
    case SINFO_BAND_MP:       return "Mp" ;
    case SINFO_BAND_Z:        return "Z" ;
    case SINFO_BAND_SZ:       return "SZ" ;
    case SINFO_BAND_SH:       return "SH" ;
    case SINFO_BAND_SK:       return "SK" ;
    case SINFO_BAND_SL:       return "SL" ;
    default:            return "Unknown" ;
    } 
}

/**@}*/
