/*                                                                           *
 *   This file is part of the ESO SINFONI Pipeline                           *
 *   Copyright (C) 2004,2005 European Southern Observatory                   *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 10:17:31 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/**@{*/
/*---------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_utils  Utility functions
 *
 * This module contains various functions that are shared between multiple 
   recipes
 */
/*---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
                            Includes
 ----------------------------------------------------------------------------*/

#include <sinfo_utils.h>
/**
 * @addtogroup sinfo_utilities utilities 
 *
 * TBD
 */

/*----------------------------------------------------------------------------
                            Functions prototypes
 ----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
                            Implementation
 ----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/**
   @brief   Round a number to the nearest integer
   @param   x       The number to round
   @return  Nearest integer

   This is implemented as a function rather than a macro to avoid multiple
   evaluations of expressions that have side effects.
 */
/*---------------------------------------------------------------------------*/
long
sinfo_round_double(double x)
{
    return (x >=0) ? (long)(x+0.5) : (long)(x-0.5);
}
/**
    @brief    Check if an input parameter has been changed by the user
    @param    p input parameter
    @return  if has not changed 0, else 1;
 */
int
sinfo_parameter_get_default_flag ( const cpl_parameter* p )
{
    int flag_gasgano=0;
    int flag_norm=0;
    int flag=0;
    cpl_type type =0;

    flag_norm = ( cpl_parameter_get_default_flag ( p ) == 0 ) ? 1 : 0;
    type=cpl_parameter_get_type ( p );

    switch ( type )
    {
    case CPL_TYPE_BOOL:
        flag_gasgano = ( cpl_parameter_get_default_bool ( p ) ==
                        cpl_parameter_get_bool ( p ) ) ? 1:0;
        break;
    case CPL_TYPE_INT:
        flag_gasgano = ( cpl_parameter_get_default_int ( p ) ==
                        cpl_parameter_get_int ( p ) ) ? 1:0;
        break;
    case CPL_TYPE_DOUBLE:
        flag_gasgano = ( cpl_parameter_get_default_double ( p ) ==
                        cpl_parameter_get_double ( p ) ) ? 1:0;
        break;
    case CPL_TYPE_STRING:
        flag_gasgano = ( cpl_parameter_get_default_string ( p ) ==
                        cpl_parameter_get_string ( p ) ) ? 1:0;
        break;

    default:
        cpl_msg_error (cpl_func, "type not supported" );
    }

    flag = ( flag_gasgano && flag_norm ) ? 0 : 1;

    return flag;
}
/**@}*/
