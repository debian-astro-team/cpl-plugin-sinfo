/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <string.h>
#include "sinfo_globals.h"
#include "sinfo_utils_wrappers.h"
#include <cpl.h>

/**@{*/
/**
 * @defgroup sinfo_globals global functions
 *
 * TBD
 */



/**
@name sinfo_fake_new
@brief structure to change behaviour of prepare stacked frame function
@return pointer to structure

 */
fake*
sinfo_fake_new(void)
{
    fake * f;
    f= cpl_malloc(sizeof(fake));

    strcpy(f->pro_class,"DEFAULT");
    f->frm_switch=0;
    f->is_fake_sky=0;
    f->mask_index=1;
    f->ind_index=0;
    f->flat_index=1;
    f->wfix_index=1;
    f->low_rej=0.1;
    f->hig_rej=0.1;
    return f;
}
/**
@name sinfo_fake_delete
@brief function to free a fake structure
@return void

 */
void
sinfo_fake_delete(fake** f)
{
    cpl_free(*f);
    *f=NULL;
}

/**
@name sinfo_wcal_new
@brief structure to change behaviour of prepare stacked frame function
@return pointer to structure

 */
wcal*
sinfo_wcal_new(void)
{
    wcal * w;
    w= cpl_malloc(sizeof(wcal));

    w->wstart=1.65;
    w->wgdisp1=-0.000200018796022;
    w->wgdisp2=9.30345245278e-10;
    w->min_dif=10.0;
    w->hw=7;
    w->fwhm=2.83;
    w->min_amp=5.0;
    w->na_coef=3;
    w->nb_coef=2;
    w->pixel_tol=7.0;
    w->y_box=2.0;
    w->low_pos=750;
    w->hig_pos=1000;

    return w;
}
/**
@name sinfo_wcal_delete
@brief function to free a wcal structure
@return void

 */
void
sinfo_wcal_delete(wcal* w)
{
    cpl_free(w);
}

/**
@name sinfo_nstpar_new
@brief structure to change behaviour of prepare stacked frame function
@return pointer to structure

 */
nstpar*
sinfo_nstpar_new(void)
{
    nstpar * n;
    n= cpl_malloc(sizeof(nstpar));

    n->fwhm[0]=2.0;
    n->fwhm[1]=5.0;
    n->fwhm[2]=2.0;
    n->fwhm[3]=2.0;

    n->min_dif[0]=1.0;
    n->min_dif[1]=5.0;
    n->min_dif[2]=5.0;
    n->min_dif[3]=5.0;

    return n;
}
/**
@name sinfo_nstpar_delete
@brief function to free a nstpar structure
@return void

 */
void
sinfo_nstpar_delete(nstpar* n)
{
    cpl_free(n);
}
/**
@name sinfo_distpar_new
@brief structure to change behaviour of prepare stacked frame function
@return pointer to structure

 */

distpar*
sinfo_distpar_new(void)
{
    distpar * d;
    d= cpl_malloc(sizeof(distpar));

    d->diff_tol[0]=2.0;
    d->diff_tol[1]=4.0;
    d->diff_tol[2]=2.0;
    d->diff_tol[3]=4.0;

    return d;
}
/**
@name sinfo_distpar_delete
@brief function to free a distpar structure
@return void

 */
void
sinfo_distpar_delete(distpar* d)
{
    cpl_free(d);
}

