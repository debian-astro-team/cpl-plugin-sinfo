#ifndef SINFO_SOLVE_POLY_ROOT_H
#define SINFO_SOLVE_POLY_ROOT_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
#include <stdio.h>
*/
#include <stdlib.h>
#include <math.h>
#include <stdlib.h>
#include <cpl.h>
#include "sinfo_msg.h"

/* C-style sinfo_matrix elements */
#define MAT(m,i,j,n) ((m)[(i)*(n) + (j)])

/* Fortran-style sinfo_matrix elements */
#define FMAT(m,i,j,n) ((m)[((i)-1)*(n) + ((j)-1)])


#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
# define __BEGIN_DECLS extern "C" {
# define __END_DECLS }
#else
# define __BEGIN_DECLS /* empty */
# define __END_DECLS /* empty */
#endif

typedef double *       gsl_complex_packed_ptr ;

void sinfo_balance_companion_matrix (double *m, size_t nc);
int sinfo_qr_companion (double *h, size_t nc, gsl_complex_packed_ptr z);
void sinfo_set_companion_matrix (const double *a, size_t n, double *m);

__BEGIN_DECLS

/* Solve for the complex roots of a general real polynomial */

typedef struct 
{ 
  size_t nc ;
  double * sinfo_matrix ; 
} 
gsl_poly_complex_workspace ;

gsl_poly_complex_workspace * sinfo_gsl_poly_complex_workspace_alloc (size_t n);
void sinfo_gsl_poly_complex_workspace_free (gsl_poly_complex_workspace * w);

int
sinfo_gsl_poly_complex_solve (const double * a, size_t n, 
                        gsl_poly_complex_workspace * w,
                        gsl_complex_packed_ptr z);
            
            

__END_DECLS

#endif /* SINFO_SOLVE_POLY_ROOT_H */
