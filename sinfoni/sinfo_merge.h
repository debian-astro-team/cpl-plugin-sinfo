#ifndef SINFO_MERGE_H
#define SINFO_MERGE_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_merge.h,v 1.4 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  04/07/00  created
*/

/************************************************************************
 * sinfo_merge.h
 * merges the rows of two image data frames into one frame with doubled
 * column length
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h>
#include "sinfo_msg.h"
#include "sinfo_recipes.h"

/*
 * function prototypes
 */

/**
  @name  sinfo_sinfo_merge_images()
  @memo merges the rows of two image frames in a way that the resulting
        image has double length in y-direction
  @param im1  image to merge
  @param im2  image to merge
  @param res_image dummy for residual image
  @note first must have smaller wavelength than the second for the same pixel 
        row.
  @result merged image, final residual image
*/

cpl_image * 
sinfo_sinfo_merge_images (cpl_image * im1, 
                  cpl_image * im2,
                  cpl_image * res_image ) ;

/**
   @name   sinfo_new_remove_general_offset()
   @memo  adds general offset between two frames to the second image and 
          delivers the residual image, assuming that the background
          cancellation did not work perfectly well.   @param  im1 image 
   @param  im2 image number of rows from which the offset is determined
   @param  residual image.
   @result changed second image, residual image

*/


cpl_image * 
sinfo_new_remove_general_offset(cpl_image * im1, 
                      cpl_image * im2, 
                      cpl_image * res_image,
                      int n ) ;

/**
  @name sinfo_new_remove_regional_tilt()
  @memo  removes a general tilt from the spectra , created e.g. by different 
         emissivities of the telescope itself and delivers the residual image
  @param im1 image to merge
  @param im2 image to merge
  @param  residual image (obligatory no NullImage).
  @result changed second image, residual image 
*/

cpl_image * 
sinfo_new_remove_regional_tilt (cpl_image * im1,
              cpl_image * im2, 
              cpl_image * res_image ) ;

/**
   @name  sinfo_new_remove_column_offset()
   @memo removes individual column offset, created e.g. by imperfect
         guiding. The offset is divided out. The ratio is derived
         from the medians of the contributions.
   @param im1 first image
   @param im2 already corrected second image to merge,
   @param residual image (obligatory no NullImage).
   @result changed second image, residual image
*/

cpl_image * sinfo_new_remove_column_offset (cpl_image * im1,
                      cpl_image * im2,
                      cpl_image * res_image );

/**
  @name  sinfo_new_remove_residual_tilt()
  @memo removes a residual column tilt (determined from the 
            residual image) created by previous operations
  @param im2 second image to merge
  @param res_image residual image (obligatory no NullImage).
  @result changed second image, residual image
*/

cpl_image * 
sinfo_new_remove_residual_tilt (cpl_image * im2,cpl_image * res_image ) ;


/**
   @name  sinfo_new_remove_residual_offset()
   @param im2     second image that will be changed,
   @param res_image        residual image must be given.
   @result changed second image, residual image
   @memo   removes the residual offset by subtracting the sinfo_median
           of the residual image from each column
*/


cpl_image * 
sinfo_new_remove_residual_offset(cpl_image * im2,cpl_image * res_image);


#endif /*!SINFO_MERGE_H*/

