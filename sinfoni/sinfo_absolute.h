/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_absolute.h,v 1.3 2007-08-20 10:01:05 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  14/11/00  created
*/
#ifndef SINFO_ABSOLUTE_H
#define SINFO_ABSOLUTE_H
/**
 * @addtogroup sinfo_absolute
 *
 */
/*----------------------------------------------------------------------------*/


/************************************************************************
 * absolute.h
 * routines to determine the absolute positions of the slitlets out of 
 * an emission line frame
 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <cpl.h>
#include "sinfo_spectrum_ops.h"
#include "sinfo_msg.h"
#include "sinfo_recipes.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

float 
sinfo_new_edge(float * xdat, float * parlist/*, int * npar, int * ndat */) ;


cpl_error_code
sinfo_new_edge_deriv(float * xdat, 
                     float * parlist, float * dervs/*, int * npar */) ;


int 
sinfo_new_lsqfit_edge ( float * xdat,
                  int   * xdim,
                  float * ydat,
                  float * wdat,
                  int   * ndat,
                  float * fpar,
                  float * epar,
                  int   * mpar,
                  int   * npar,
                  float * tol ,
                  int   * its ,
                  float * lab  ) ;

int 
sinfo_new_fit_slits_edge( cpl_image   * lineImage,
                  FitParams ** par,
                  float     ** sinfo_slit_pos,
                  int          box_length,
                  float        y_box,
                  float        diff_tol ) ;

int
sinfo_new_fit_slits_edge_with_estimate ( cpl_image   * lineImage,
                                float    ** sinfo_slit_pos,
                                int         box_length,
                                float       y_box,
                                float       diff_tol,
                                int         low_pos,
                                int         high_pos ) ;


#endif /*!SINFO_ABSOLUTE_H*/
/**@}*/
