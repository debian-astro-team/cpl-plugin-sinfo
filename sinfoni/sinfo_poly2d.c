/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

   File name     :    poly2d.c
   Author         :    N. Devillard
   Created on    :    22 Jun 1999
   Description    :    2D polynomial handling

 *--------------------------------------------------------------------------*/
/*
    $Id: sinfo_poly2d.c,v 1.4 2012-03-03 09:50:08 amodigli Exp $
    $Author: amodigli $
    $Date: 2012-03-03 09:50:08 $
    $Revision: 1.4 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_poly2d.h"
/**@{*/
/**
 * @addtogroup sinfo_utilities  2D polynomial computation 
 *
 * TBD
 */



/**
  @name        sinfo_poly2d_compute
  @memo        Compute the value of a poly2d at a given point.
  @param    p    Poly2d object.
  @param    x    x coordinate.
  @param    y    y coordinate.
  @return    The value of the 2d polynomial at (x,y) as a double.
  @doc

  This computes the value of a poly2d in a single point. To
  compute many values in a row, see sinfo_poly2d_compute_array().

  TODO: not used
 */

double
sinfo_poly2d_compute(
                poly2d    *    p,
                double        x,
                double        y
)
{
    double    z ;
    int        i ;

    z = 0.00 ;

    for (i=0 ; i<p->nc ; i++) {
        z += p->c[i] * sinfo_ipow(x, p->px[i]) * sinfo_ipow(y, p->py[i]) ;
    }
    return z ;
}
/**@}*/



