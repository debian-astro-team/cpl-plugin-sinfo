/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_focus_ini_by_cpl.h
   Author       :   Andrea Modigliani
   Created on   :   May 20, 2004
   Description  :   cpl input handling for SPIFFIs focus finding
 ---------------------------------------------------------------------------*/
#ifndef SINFO_FOCUS_INI_BY_CPL_H
#define SINFO_FOCUS_INI_BY_CPL_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <stdio.h>
#include <cpl.h>
#include "sinfo_msg.h"
#include "sinfo_focus_cfg.h"
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/

/**
@name sinfo_free_focus
@memo free sinfo_config structure
@param cfg pointer to focus_config structure
@return void
*/
void 
sinfo_free_focus(focus_config * cfg);

/**
  @name     sinfo_parse_cpl_input_focus
  @memo     Parse input frames and parameters and create a blackboard.
  @param    cpl_cfg pointer to parameterlist
  @param    sof pointer to input set of frames
  @param    stk pointer to input set of stacked frames
  @return   1 newly allocated focus_config blackboard structure.
  @doc

  The requested ini file is parsed and a blackboard object is created, then
  updated accordingly. Returns NULL in case of error.
 */
focus_config * 
sinfo_parse_cpl_input_focus(cpl_parameterlist * cpl_cfg, 
                            cpl_frameset* sof, 
                            cpl_frameset** stk) ;

#endif
