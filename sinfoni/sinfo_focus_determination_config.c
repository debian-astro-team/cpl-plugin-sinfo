/* $Id: sinfo_focus_determination_config.c,v 1.6 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/****************************************************************
 *   Focus Determination                                        *
 ****************************************************************/
#include "cpl_parameterlist.h"    /* defines parlist structure */
#include "sinfo_dfs.h"
#include "sinfo_focus_determination_config.h"

/**@{*/
/**
 * @addtogroup sinfo_focus Focus determination functions
 *
 * TBD
 */


/* Focus Determination Parameters Definition */


void
sinfo_focus_determination_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /* Output file name */
    /* output name of resulting fits wavelength map */
    p = cpl_parameter_new_value("sinfoni.focus.output_filename",
                    CPL_TYPE_STRING,
                    "Output File Name: ",
                    "sinfoni.focus",
                    FOCUS_OUT_FILENAME);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"out-focus_filename");
    cpl_parameterlist_append(list, p);



    p = cpl_parameter_new_enum("sinfoni.focus.method",
                    CPL_TYPE_STRING,
                    "Shifting method to use: ",
                    "sinfoni.focus",
                    "P",
                    3,
                    "P","F","S");

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-method");
    cpl_parameterlist_append(list, p);


    /* Reconstruction */

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.focus.lower_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection",
                    "sinfoni.focus",
                    0.1,0.0,1.0);


    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-low_rejection");
    cpl_parameterlist_append(list, p);

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.focus.higher_rejection",
                    CPL_TYPE_DOUBLE,
                    "high rejection",
                    "sinfoni.focus",
                    0.1,0.0,1.0);


    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI,"focus-det-high_rejection");
    cpl_parameterlist_append(list, p);

    /* indicates if the slitlet distances are determined by
   a north-south test (1) 
           or 
   slitlet sinfo_edge fits (0) */ 
    p = cpl_parameter_new_value("sinfoni.focus.north_south_index",
                    CPL_TYPE_BOOL,
                    "North South Index",
                    "sinfoni.focus",
                    TRUE);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-ns_index");
    cpl_parameterlist_append(list, p);

    /* number of slitlets (32) */
    p = cpl_parameter_new_value("sinfoni.focus.nslits",
                    CPL_TYPE_INT,
                    "Number Of Slits",
                    "sinfoni.focus",
                    32);


    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-nslits");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.focus.order",
                    CPL_TYPE_INT,
                    "Order",
                    "sinfoni.focus",
                    2);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-order");
    cpl_parameterlist_append(list, p);


    /* Gauss 2D Fit */
    /* lower left sinfo_edge coordinates of fitting box for 2D Gaussian fit */
    p = cpl_parameter_new_range("sinfoni.focus.llx",
                    CPL_TYPE_INT,
                    "llx",
                    "sinfoni.focus",
                    9,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-llx");
    cpl_parameterlist_append(list, p);

    /* lower left sinfo_edge coordinates of fitting box for 2D Gaussian fit */
    p = cpl_parameter_new_range("sinfoni.focus.lly",
                    CPL_TYPE_INT,
                    "lly",
                    "sinfoni.focus",
                    9,DET_PIX_MIN,DET_PIX_MAX);


    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-lly");
    cpl_parameterlist_append(list, p);

    /* half length in pixels of the box within the point source is fitted in x
   and y-direction */
    p = cpl_parameter_new_range("sinfoni.focus.halfbox_x",
                    CPL_TYPE_INT,
                    "half box x",
                    "sinfoni.focus",
                    7,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-hbx");
    cpl_parameterlist_append(list, p);

    /* half length in pixels of the box within the point source is fitted in x
   and y-direction */
    p = cpl_parameter_new_range("sinfoni.focus.halfbox_y",
                    CPL_TYPE_INT,
                    "half box y",
                    "sinfoni.focus",
                    7,DET_PIX_MIN,DET_PIX_MAX);


    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-hby");
    cpl_parameterlist_append(list, p);

    /* mask for the x-position */
    p = cpl_parameter_new_value("sinfoni.focus.mpar0",
                    CPL_TYPE_INT,
                    "mask par 0",
                    "sinfoni.focus",
                    1);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-mpar0");
    cpl_parameterlist_append(list, p);

    /* mask for the y-position */
    p = cpl_parameter_new_value("sinfoni.focus.mpar1",
                    CPL_TYPE_INT,
                    "mask par 1",
                    "sinfoni.focus",
                    1);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-mpar1");
    cpl_parameterlist_append(list, p);

    /* mask for the amplitude */
    p = cpl_parameter_new_value("sinfoni.focus.mpar2",
                    CPL_TYPE_INT,
                    "mask par 2",
                    "sinfoni.focus",
                    1);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-mpar2");
    cpl_parameterlist_append(list, p);

    /* mask for the background */
    p = cpl_parameter_new_value("sinfoni.focus.mpar3",
                    CPL_TYPE_INT,
                    "mask par 3",
                    "sinfoni.focus",
                    1);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-mpar3");
    cpl_parameterlist_append(list, p);

    /* mask for the fwhmx */
    p = cpl_parameter_new_value("sinfoni.focus.mpar4",
                    CPL_TYPE_INT,
                    "mask par 4",
                    "sinfoni.focus",
                    1);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-mpar4");
    cpl_parameterlist_append(list, p);

    /* mask for the fwhmy */
    p = cpl_parameter_new_value("sinfoni.focus.mpar5",
                    CPL_TYPE_INT,
                    "mask par 5",
                    "sinfoni.focus",
                    1);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-mpar5");
    cpl_parameterlist_append(list, p);

    /* mask for the position angle of fwhmx line */
    p = cpl_parameter_new_value("sinfoni.focus.mpar6",
                    CPL_TYPE_INT,
                    "mask par 6",
                    "sinfoni.focus",
                    1);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-mpar6");
    cpl_parameterlist_append(list, p);

    /* name of the resulting ASCII file containing the fit parameters */
    p = cpl_parameter_new_value("sinfoni.focus.fit_list",
                    CPL_TYPE_STRING,
                    "Fit List: ",
                    "sinfoni.fit_list",
                    FOCUS_FITPAR_OUT_FILENAME);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-fit_list");
    cpl_parameterlist_append(list, p);

    /* indicator if the resulting 2D-Gaussian is stored in a fits file or not */
    p = cpl_parameter_new_value("sinfoni.focus.plot_gauss_ind",
                    CPL_TYPE_BOOL,
                    "Plot Gauss Ind",
                    "sinfoni.focus",
                    TRUE);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"focus-det-gauss_ind");
    cpl_parameterlist_append(list, p);


    /* name of the fits file containing the resulting 2D-Gaussian */
    p = cpl_parameter_new_value("sinfoni.focus.gauss_plot_name",
                    CPL_TYPE_STRING,
                    "Gauss Plot Name: ",
                    "sinfoni.focus",
                    FOCUS_GAUSSPLOT_OUT_FILENAME);

    cpl_parameter_set_alias(p,
                            CPL_PARAMETER_MODE_CLI,"focus-det-gauss_plot_name");
    cpl_parameterlist_append(list, p);

}
/**@}*/
