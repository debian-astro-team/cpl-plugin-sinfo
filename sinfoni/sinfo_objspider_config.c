/* $Id: sinfo_objspider_config.c,v 1.7 2012-03-03 09:50:08 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 09:50:08 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *   Objspider Frames Data Reduction Parameter Initialization        *
 ****************************************************************/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_objspider_config.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Objnod parameters definition & initialization
 *
 * TBD
 */

/**
@name  sinfo_objspider_config_add
@brief Objspider parameters definition & initialization
@param list pointer to cpl_parameterlist
@return void
 */

void
sinfo_objspider_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }


    /* Input file name */
    /* Output file name */
    /* output name of resulting fits wavelength map */
    p = cpl_parameter_new_value("sinfoni.objspider.out_filename",
                    CPL_TYPE_STRING,
                    "Output File Name: ",
                    "sinfoni.objspider",
                    SKYSPIDER_OUT_FILENAME);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"out-skyspider_filename");
    cpl_parameterlist_append(list, p);


    /*Resampling */
    /* number of coefficients for the polynomial interpolation */
    p = cpl_parameter_new_value("sinfoni.objspider.n_coeffs",
                    CPL_TYPE_INT,
                    "number of coefficients for the polynomial "
                    "interpolation ",
                    "sinfoni.objspider",
                    3);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-no-coeffs");
    cpl_parameterlist_append(list, p);


    /* Calibration */
    /* indicates if the halogen lamp features from flatfielding should be
     corrected for or not */
    /*
  p = cpl_parameter_new_value("sinfoni.objspider.halo_correct_index",
                  CPL_TYPE_BOOL,
                              "Halo Correct Index: "
                               "indicates if the halogen lamp features from "
                              "flatfielding should be corrected for (TRUE) "
                              "or not (FALSE)",
                              "sinfoni.objspider",
                              FALSE);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-halo-corr-ind");
  cpl_parameterlist_append(list, p);
     */


    /* Cube Creation */
    /*indicates if the slitlet distances are determined by a north-south-test
     (yes) or slitlet edge fits (no)
     */
    p = cpl_parameter_new_value("sinfoni.objspider.nord_south_index",
                    CPL_TYPE_BOOL,
                    "Nord South Index Switch: "
                    "indicates if the slitlet distances are "
                    "determined by a north-south-test (TRUE) "
                    "or slitlet edge fits (FALSE)",
                    "sinfoni.objspider",
                    TRUE);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-ns-ind");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.objspider.slitlets_position_list",
                    CPL_TYPE_STRING,
                    "Slitlets positions filename: "
                    "list of the fitted slitlet edge positions "
                    "or the distances of the slitlets",
                    "sinfoni.objspider",
                    "distances.fits");


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-pos-list");
    cpl_parameterlist_append(list, p);

    /* number of slitlets (32) */
    p = cpl_parameter_new_value("sinfoni.objspider.nslits",
                    CPL_TYPE_INT,
                    "Number of slitlets: ",
                    "sinfoni.objspider",
                    32);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-pos-nslits");
    cpl_parameterlist_append(list, p);

    /* Fine tuning */
    /* Method */

    p = cpl_parameter_new_enum("sinfoni.objspider.fine_tuning_method",
                    CPL_TYPE_STRING,
                    "Fine Tuning Method: "
                    "indicator for the shifting method to use "
                    "(P: polynomial interpolation, "
                    " F: FFT, "
                    " S: cubic spline interpolation)",
                    "sinfoni.objspider",
                    "P",
                    3,"P","F","S");

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-fine-tune-mtd");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.objspider.order",
                    CPL_TYPE_INT,
                    "Fine Tuning polynomial order: "
                    "order of the polynomial if the polynomial "
                    "interpolation shifting method is used.",
                    "sinfoni.objspider",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-order");
    cpl_parameterlist_append(list, p);

    /* Sky Extraction */
    /*Reconstruction */
    /* the fraction [0...1] of rejected low intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_value("sinfoni.objspider.lower_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection: "
                    "percentage of rejected low value pixels "
                    "for averaging the sky spectra",
                    "sinfoni.objspider",
                    10.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-lo-rejection");
    cpl_parameterlist_append(list, p);

    /* the fraction [0...1] of rejected high intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_value("sinfoni.objspider.higher_rejection",
                    CPL_TYPE_DOUBLE,
                    "higher rejection: "
                    "percentage of rejected high value pixels "
                    "for averaging the sky spectra",
                    "sinfoni.objspider",
                    10.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-hi-rejection");
    cpl_parameterlist_append(list, p);

    /* pixel tolerance, this distance tolerance to the diagonal dividing
     line is not considered for the sky extraction to be sure to have a clean
     sky due to positioning tolerance and crossing through pixels
     */

    p = cpl_parameter_new_value("sinfoni.objspider.tolerance",
                    CPL_TYPE_INT,
                    "Tolerance: "
                    "pixel tolerance, this distance tolerance to "
                    "the diagonal dividing line is not considered "
                    "for the sky extraction to be sure to have a "
                    "clean sky due to positioning tolerance and "
                    "crossing through pixels",
                    "sinfoni.objspider",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-tol");
    cpl_parameterlist_append(list, p);

    /* Jittering */
    /* jitter mode indicator: yes: Auto-Jitter, no: user defined jitter
     The next three parameters are only used if jitterInd is set to yes, 
     that means in auto-jittering mode!
     */

    p = cpl_parameter_new_value("sinfoni.objspider.jitter_index",
                    CPL_TYPE_BOOL,
                    "Jitter Index: "
                    "jitter mode indicator: "
                    "TRUE: Auto-Jitter, "
                    "FALSE: user defined jitter"
                    "The size_x size_y kernel_type parameters "
                    "are only used if jitterInd is set to yes, "
                    "that means in auto-jittering mode!",
                    "sinfoni.objspider",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-jit-ind");
    cpl_parameterlist_append(list, p);
    /*x-pixel size of the final combined data cube, must lie between 64 and 128 */

    p = cpl_parameter_new_value("sinfoni.objspider.size_x",
                    CPL_TYPE_INT,
                    "Cube x size: "
                    "x-pixel size of the final combined data cube,"
                    "must lie between 64 and 128. "
                    "If 0 automatic setting.",
                    "sinfoni.objspider",
                    0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-size-x");
    cpl_parameterlist_append(list, p);
    /*y-pixel size of the final combined data cube, must lie between 64 and 128 */

    p = cpl_parameter_new_value("sinfoni.objspider.size_y",
                    CPL_TYPE_INT,
                    "Cube y size: "
                    "y-pixel size of the final combined data cube,"
                    "must lie between 64 and 128. "
                    "If 0 automatic setting.",
                    "sinfoni.objspider",
                    0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-size-y");
    cpl_parameterlist_append(list, p);

    /* Kernel Type */
    /* the name of the interpolation kernel to shift the single cubes  to the
     correct places inside the big combined cube. That you want to generate 
     using the eclipse routine sinfo_generate_interpolation_kernel()
     Supported kernels are:

                                     NULL:      default kernel, currently tanh
                                     default: dito
                                     tanh:    Hyperbolic tangent
                                     sinc2:   Square sinc
                                     lanczos: Lanczos2 kernel
                                     hamming: Hamming kernel
                                     hann:    Hann kernel
     */
    p = cpl_parameter_new_enum("sinfoni.objspider.kernel_type",
                    CPL_TYPE_STRING,
                    "Kernel Type:"
                    "the name of the interpolation kernel to shift "
                    "the single cubes  to the correct places inside "
                    "the big combined cube",
                    "sinfoni.objspider",
                    "tanh",
                    6,"NULL","default","tanh",
                    "sinc2","lanczos","hamming","hann");

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-kernel-typ");
    cpl_parameterlist_append(list, p);

    /* name of the final mask data cube, pixel value 0 if no data available,
     sum of exposure times in the overlapping regions
     */

    p = cpl_parameter_new_value("sinfoni.objspider.mask_name",
                    CPL_TYPE_STRING,
                    "Mask Name: "
                    "name of the final mask data cube, "
                    "pixel value 0 if no data available,"
                    "sum of exposure times in the overlapping "
                    "regions ",
                    "sinfoni.objspider",
                    SKYSPIDER_MASK_OUT_FILENAME);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objspider-mask");
    cpl_parameterlist_append(list, p);

}
/**@}*/
