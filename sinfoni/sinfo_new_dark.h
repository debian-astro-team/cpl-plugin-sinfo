#ifndef SINFO_NEW_DARK_H
#define SINFO_NEW_DARK_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_dark.h,v 1.6 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  17/09/03  created
*/

/************************************************************************
 * sinfo_dark.h
 * routines to create a master sinfo_dark
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h>    
#include "sinfo_msg.h"
/*@-skipposixheaders@*/
/*@=skipposixheaders@*/
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   @name cpl_dark()
   @param ini_file: file name of according .ini file
   @param cpl_parameterlist: structure containing recipe's parameters
   @param cpl_frameset: structure containing recipe's input files
   @result integer (0 if it worked, -1 if it doesn't) 
   @doc Sorts frames according to integration time
        Take the clean mean of a stack of frames with the same
        integration time

 ---------------------------------------------------------------------------*/
int 
sinfo_new_dark (const char* plugin_id, cpl_parameterlist* config, 
          cpl_frameset* set, char* dark_name);
#endif 

/*--------------------------------------------------------------------------*/
