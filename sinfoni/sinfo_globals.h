/* $Id: sinfo_globals.h,v 1.7 2013-07-15 08:14:08 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2013-07-15 08:14:08 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_GLOBALS_H
#define SINFO_GLOBALS_H
#include <cpl.h>
/*------------------------------------------------------------------------
                                   Defines
 --------------------------------------------------------------------------*/
#define SINFO_RESAMP_NROWS 2560
#define PI_NUMB     (3.1415926535897932384626433832795)
#define ZERO    0./0.
#define FLAG    -1.e+9
#define QC_DID_ID                          "SINFONI-1.0.0"
#define FILE_NAME_SZ                       512
#define MAX_NAME_SIZE                      512
#define TELESCOPE_SURFACE                  52.8101279

#define NOISE_HSIZE                        4
#define NOISE_NSAMPLES                     100
#define SINFO_DBL_MIN 1e-37
#define SINFO_DBL_MAX 1e+37

#define IMA_PIX_START                      0
#define IMA_PIX_END                        2047
#define SIZEX                              2048
#define SIZEY                              2048
#define DET_PIX_MIN                        1
#define DET_PIX_MAX                        2048


#define LLX                                1350
#define LLY                                1000
#define URX                                1390
#define URY                                1200

#define DISTORTION_LOPOS                    974
#define DISTORTION_HIPOS                   1074

#define GRAT_VAL1_HK                       3997330
#define GRAT_VAL2_HK                       3997339
#define GRAT_VAL3_HK                       3888888  //SPIFFI upgrade

#define GRAT_VAL1_H                        2948723
#define GRAT_VAL2_H                        2948733
#define GRAT_VAL3_H                        2888888 //SPIFFI upgrade

#define GRAT_VAL1_K                        1893844
#define GRAT_VAL2_K                        1893854
#define GRAT_VAL3_K                        1888888 //SPIFFI upgrade

#define GRAT_VAL1_J                         849618
#define GRAT_VAL2_J                         849628
#define GRAT_VAL3_J                         888888 //SPIFFI upgrade





#define GRAT_VAL_TOL                             4

#define SKY_FLUX                                 0
#define BKG_VARIANCE                            9.6
#define GAIN                                    2.42
#define MAGNITUDE                              11

#define MSG_OVER_WRITE_PAR        "Using default data reduction parameters"
#define LAMP_ON     TRUE
#define LAMP_OFF    FALSE

/* compat macro */
#if defined CPL_VERSION_CODE && CPL_VERSION_CODE >= CPL_VERSION(6, 3, 0)
#define cpl_frameset_get_frame cpl_frameset_get_position
#define cpl_frameset_get_frame_const cpl_frameset_get_position_const
#endif

struct amoeba_ {
  cpl_vector*   vx;
  cpl_vector*   vy;
  cpl_vector*   sx;
  cpl_vector*   sy;
};
typedef struct amoeba_ amoeba_dat; 
struct qc_log_ {
  char   name[30];
  char   type[30];
  char   s_val[30];
  char   comm[30];
  double n_val;
  int    n;
};

typedef struct qc_log_ qc_log; 


struct fake_ {
  char  pro_class[FILE_NAME_SZ];
  int   frm_switch;
  int   is_fake_sky;
  int   mask_index;
  int   ind_index;
  int   flat_index;
  int   wfix_index;
  double   low_rej;
  double   hig_rej;
};

typedef struct fake_ fake;
fake* sinfo_fake_new(void);
void sinfo_fake_delete(fake** f);


struct wcal_ {
  double wstart;
  double wgdisp1;
  double wgdisp2;
  double min_dif;
  double fwhm;
  double min_amp;
  double pixel_tol;
  double y_box;
  int low_pos;
  int hig_pos;
  int    hw;
  int    na_coef;
  int    nb_coef;

};

typedef struct wcal_ wcal;

wcal* sinfo_wcal_new(void);
void sinfo_wcal_delete(wcal* f);


struct stack_ {
  char do_class[FILE_NAME_SZ];
  char index_list[FILE_NAME_SZ];
  int  warp_fix_ind;
};

typedef struct stack_ stack;

struct nst_ {
  double min_dif[4];
  double fwhm[4];
};

typedef struct nst_ nstpar;

nstpar* sinfo_nstpar_new(void);
void sinfo_nstpar_delete(nstpar* n);

struct dist_ {
  double diff_tol[4];
};

typedef struct dist_ distpar;

distpar* sinfo_distpar_new(void);
void sinfo_distpar_delete(distpar* d);


#endif
