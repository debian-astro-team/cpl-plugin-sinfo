/* $Id: sinfo_raw_types.h,v 1.5 2008-07-04 13:06:02 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2008-07-04 13:06:02 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_RAW_TYPES_H
#define SINFO_RAW_TYPES_H

/*------------------------------------------------------------------------
                                   Raw TYPES
 --------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
                                   Raw Frames
 --------------------------------------------------------------------------*/
CPL_BEGIN_DECLS

#define RAW_LINEARITY_LAMP                 "LINEARITY_LAMP"
#define RAW_DARK                           "DARK"

#define RAW_PINHOLE_LAMP                   "PINHOLE_LAMP"
#define RAW_SLIT_LAMP                      "SLIT_LAMP"      /* ?? */
#define RAW_FIBRE_PSF                      "FIBRE_PSF"    
#define RAW_FIBRE_DARK                     "FIBRE_DARK"    
#define RAW_FIBRE_LAMP                     "FIBRE_LAMP"    
#define RAW_FIBRE_NS                       "FIBRE_NS"      
#define RAW_FIBRE_EW                       "FIBRE_EW"      
#define RAW_WAVE_LAMP                      "WAVE_LAMP"
#define RAW_WAVE_LAMP_DITHER               "WAVE_LAMP_DITHER"
#define RAW_WAVE_NS                        "WAVE_NS"
#define RAW_WAVE_NS_DITHER                 "WAVE_NS_DITHER"

#define RAW_FLAT_LAMP                      "FLAT_LAMP"
#define RAW_FLAT_LAMP_DITHER               "FLAT_LAMP_DITHER"
#define RAW_FLAT_NS                        "FLAT_NS"
#define RAW_FLAT_NS_DITHER                 "FLAT_NS_DITHER"
#define RAW_FLAT_SKY                       "FLAT_SKY"
#define RAW_FLUX_LAMP                      "FLUX_LAMP"
#define RAW_PSF_CALIBRATOR                 "PSF_CALIBRATOR"
#define RAW_FOCUS                          "FOCUS"
#define RAW_SKY_DUMMY                      "SKY_DUMMY"

#define RAW_PUPIL_LAMP                     "PUPIL_LAMP"
#define RAW_OBJECT                         "OBJECT"
#define RAW_IMAGE_PRE_OBJECT               "IMAGE_PRE_OBJECT"
#define RAW_IMAGE_PRE_SKY                  "IMAGE_PRE_SKY"
#define RAW_OBJECT_SKYSPIDER               "OBJECT_SKYSPIDER"
#define RAW_OBJECT_SKYSPIDER_DITHER        "OBJECT_SKYSPIDER_DITHER"

#define RAW_OBJECT_NODDING                 "OBJECT_NODDING"
#define RAW_SKY_NODDING                    "SKY_NODDING"
#define RAW_OBJECT_JITTER                  "OBJECT_JITTER"
#define RAW_SKY_JITTER                     "SKY_JITTER"
#define RAW_OBJECT_NODDING_DITHER          "OBJECT_NODDING_DITHER"
#define RAW_OBJECT_JITTER_DITHER           "OBJECT_JITTER_DITHER"
#define RAW_SKY_NODDING_DITHER             "SKY_NODDING_DITHER"
#define RAW_SKY_JITTER_DITHER              "SKY_JITTER_DITHER"


#define RAW_ACQUISITION_SKY                "ACQUISITION_SKY"
#define RAW_ACQUISITION_OBJECT             "ACQUISITION_OBJECT"


#define RAW_STD                            "STD"
#define RAW_STACKED_SLITPOS                "STACKED_SLITPOS"
#define RAW_SKY_STD                        "SKY_STD"
#define RAW_SKY_OH                         "SKY_OH"
#define RAW_SKY_PSF_CALIBRATOR             "SKY_PSF_CALIBRATOR"
#define RAW_STD_STAR                       "STD_STAR"
#define RAW_STD_STAR_DITHER                "STD_STAR_DITHER"

#define SINFO_UTL_STDSTARS_RAW             "STDSTAR_CAT"
#define SINFO_UTL_SEDS_RAW                 "SED"

/* next are TBD */
#define RAW_OFF                            "RAW_OFF"
#define RAW_OFF1                           "OFF1"
#define RAW_OFF2                           "OFF2"

#define RAW_SKY                            "SKY"
#define RAW_SKY1                           "SKY1"
#define RAW_SKY2                           "SKY2"

#define RAW_ON                             "RAW_ON"
#define RAW_ON1                            "ON1"
#define RAW_ON2                            "ON2"
#define RAW_INT_ON                         "INT_ON"
#define RAW_INT_OFF                        "INT_OFF"
CPL_END_DECLS

#endif
