/*
 * sinfo_cpl_size.h
 *
 *  Created on: Nov 23, 2011
 *      Author: amodigli
 */

#ifndef SINFO_CPL_SIZE_H_
#define SINFO_CPL_SIZE_H_

#include <cpl.h>
#if defined CPL_VERSION_CODE && CPL_VERSION_CODE <= CPL_VERSION(5, 5, 0)
typedef int cpl_size; /* The type as is was up to CPL 5.3 */
#define CPL_SIZE_FORMAT "d"
#endif

#endif /* SINFO_CPL_SIZE_H_ */
