/* $Id: sinfo_utl_cube_arith.h,v 1.1 2006-10-22 14:12:28 amodigli Exp $
 *
 * This file is part of the IIINSTRUMENT Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2006-10-22 14:12:28 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */
/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/
#ifndef SINFO_UTL_CUBE_ARITH_H
#define SINFO_UTL_CUBE_ARITH_H

#include <cpl.h>
#include "sinfo_tpl_utils.h"
#include "sinfo_pfits.h"
#include "sinfo_tpl_dfs.h"
/*-----------------------------------------------------------------------------
                            Functions prototypes
-----------------------------------------------------------------------------*/

int sinfo_utl_cube_arith(cpl_parameterlist *, cpl_frameset *) ;

#endif
