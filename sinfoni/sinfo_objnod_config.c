/* $Id: sinfo_objnod_config.c,v 1.11 2012-03-03 09:50:08 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 09:50:08 $
 * $Revision: 1.11 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *   Objnod Frames Data Reduction Parameter Initialization        *
 ****************************************************************/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_objnod_config.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Objnod parameters definition & initialization
 *
 * TBD
 */

/**
@name  sinfo_objnod_config_add
@brief Objnod parameters definition & initialization
@param list pointer to cpl_parameterlist
@return void
 */
void
sinfo_objnod_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }




    /* Science */
    p = cpl_parameter_new_enum("sinfoni.objnod.autojitter_method",
                    CPL_TYPE_INT,
                    "Method to reduce autojitter template frames "
                    "Raw frames are object only exposures. "
                    "object-fake_sky pairs are generated. "
                    "0: no sky for all objects ",
                    "1: fake_sky is next object, "
                    "2: fake_sky is sinfo_median of all objects "
                    "sinfoni.objnod",
                    1,3,0,1,2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-aj_method");
    cpl_parameterlist_append(list, p);


    /* Science */
    p = cpl_parameter_new_value("sinfoni.objnod.scales_sky",
                    CPL_TYPE_BOOL,
                    "Spatial median (sky) subtraction from cube: "
                    "(If autojitter_method==1),"
                    "indicates if the spatial median of each plane "
                    "should be subtracted (TRUE) or not (FALSE) "
                    "from each cube plane",
                    "sinfoni.objnod",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-scales_sky");
    cpl_parameterlist_append(list, p);



    /* Science */
    p = cpl_parameter_new_value("sinfoni.objnod.ks_clip",
                    CPL_TYPE_BOOL,
                    "Kappa-sigma clipping of coadded cube: "
                    "indicates if a kappa-sigma clipping "
                    "should be performed (TRUE) or not (FALSE) "
                    "on each plane of the coadded cube",
                    "sinfoni.objnod",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-ks_clip");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.objnod.kappa",
                    CPL_TYPE_DOUBLE,
                    "kappa value for kappa-sigma clipping "
                    "of coadded cube",
                    "sinfoni.objnod",
                    2.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-kappa");
    cpl_parameterlist_append(list, p);



    /*x-pixel size of the final combined data cube, must lie between 64 and 128 */
    p = cpl_parameter_new_value("sinfoni.objnod.size_x",
                    CPL_TYPE_INT,
                    "Cube x size: "
                    "x-pixel size of the final combined data cube,"
                    "must lie between 64 and 128. "
                    "If 0 it is computed automatically",
                    "sinfoni.objnod",
                    0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-size_x");
    cpl_parameterlist_append(list, p);


    /*y-pixel size of the final combined data cube, must lie between 64 and 128 */
    p = cpl_parameter_new_value("sinfoni.objnod.size_y",
                    CPL_TYPE_INT,
                    "Cube y size: "
                    "y-pixel size of the final combined data cube,"
                    "must lie between 64 and 128."
                    "If 0 it is computed automatically",
                    "sinfoni.objnod",
                    0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-size_y");
    cpl_parameterlist_append(list, p);

    /*Resampling */
    /* number of coefficients for the polynomial interpolation */
    p = cpl_parameter_new_value("sinfoni.objnod.n_coeffs",
                    CPL_TYPE_INT,
                    "number of coefficients for the polynomial "
                    "interpolation ",
                    "sinfoni.objnod",
                    3);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-no_coeffs");
    cpl_parameterlist_append(list, p);


    /* Calibration */
    /* indicates if the halogen lamp features from flatfielding should be
     corrected for or not */
    /*
  p = cpl_parameter_new_value("sinfoni.objnod.halo_correct_index",
                  CPL_TYPE_BOOL,
                              "Halo Correct Index: "
                              "indicates if the halogen lamp features from "
                              "flatfielding should be corrected for (TRUE) "
                              "or not (FALSE)",
                              "sinfoni.objnod",
                              FALSE);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-halo_corr_ind");
  cpl_parameterlist_append(list, p);
     */

    /* Cube Creation */
    /*indicates if the slitlet distances are determined by a north-south-test
     (yes) or slitlet edge fits (no)
     */
    p = cpl_parameter_new_value("sinfoni.objnod.nord_south_index",
                    CPL_TYPE_BOOL,
                    "Nord South Index Switch: "
                    "indicates if the slitlet distances are "
                    "determined by a north-south-test (TRUE) "
                    "or slitlet edge fits (FALSE)",
                    "sinfoni.objnod",
                    TRUE);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-ns_ind");
    cpl_parameterlist_append(list, p);

    /* Fine tuning */
    /* Method */
    p = cpl_parameter_new_enum("sinfoni.objnod.fine_tuning_method",
                    CPL_TYPE_STRING,
                    "Fine Tuning Method: "
                    "indicator for the shifting method to use "
                    "(P: polynomial interpolation, "
                    /* " F: FFT, " */
                    " S: cubic spline interpolation)",
                    "sinfoni.objnod",
                    "P",
                    2,
                    "P","S");

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-fine_tune_mtd");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.objnod.order",
                    CPL_TYPE_INT,
                    "Fine Tuning polynomial order: "
                    "order of the polynomial if the polynomial "
                    "interpolation shifting method is used.",
                    "sinfoni.objnod",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-order");
    cpl_parameterlist_append(list, p);

    /* Sky Extraction */
    /*Reconstruction */
    /* the fraction [0...1] of rejected low intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_value("sinfoni.objnod.low_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection: "
                    "percentage of rejected low value pixels "
                    "for averaging the sky spectra",
                    "sinfoni.objnod",
                    10.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-lo_rej");
    cpl_parameterlist_append(list, p);
    /* the fraction [0...1] of rejected high intensity pixels when taking
the average of columns */

    p = cpl_parameter_new_value("sinfoni.objnod.high_rejection",
                    CPL_TYPE_DOUBLE,
                    "higher rejection: "
                    "percentage of rejected high value pixels "
                    "for averaging the sky spectra",
                    "sinfoni.objnod",
                    10.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-hi_rej");
    cpl_parameterlist_append(list, p);
    /* pixel tolerance, this distance tolerance to the diagonal dividing
     line is not considered for the sky extraction to be sure to have a clean
     sky due to positioning tolerance and crossing through pixels
     */
    p = cpl_parameter_new_value("sinfoni.objnod.tolerance",
                    CPL_TYPE_INT,
                    "Tolerance: "
                    "pixel tolerance, this distance tolerance to "
                    "the diagonal dividing line is not considered "
                    "for the sky extraction to be sure to have a "
                    "clean sky due to positioning tolerance and "
                    "crossing through pixels",
                    "sinfoni.objnod",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-tol");
    cpl_parameterlist_append(list, p);
    /* Jittering */
    /* jitter mode indicator: yes: Auto-Jitter, no: user defined jitter
     The next three parameters are only used if jitterInd is set to yes, 
     that means in auto-jittering mode!
     */
    p = cpl_parameter_new_value("sinfoni.objnod.jitter_index",
                    CPL_TYPE_BOOL,
                    "Jitter Index: "
                    "jitter mode indicator: "
                    "TRUE: Auto-Jitter, "
                    "FALSE: user defined jitter"
                    "The size_x size_y kernel_type parameters "
                    "are only used if jitterInd is set to yes, "
                    "that means in auto-jittering mode!",
                    "sinfoni.objnod",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-jit_ind");
    cpl_parameterlist_append(list, p);

    /* Kernel Type */
    /* the name of the interpolation kernel to shift the single cubes  to the
     correct places inside the big combined cube. That you want to generate 
     using the eclipse routine sinfo_generate_interpolation_kernel()
     Supported kernels are:

                                     NULL:      default kernel, currently tanh
                                     default: dito
                                     tanh:    Hyperbolic tangent
                                     sinc2:   Square sinc
                                     lanczos: Lanczos2 kernel
                                     hamming: Hamming kernel
                                     hann:    Hann kernel
     */
    p = cpl_parameter_new_enum("sinfoni.objnod.kernel_type",
                    CPL_TYPE_STRING,
                    "Kernel Type:"
                    "the name of the interpolation kernel to shift "
                    "the single cubes  to the correct places inside "
                    "the big combined cube",
                    "sinfoni.objnod",
                    "tanh",
                    7,
                    "NULL","default","tanh","sinc2",
                    "lanczos","hamming","hann");

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-kernel_typ");
    cpl_parameterlist_append(list, p);


    /* amount of vignetting from output cube before coaddition */
    p = cpl_parameter_new_range("sinfoni.objnod.vllx",
                    CPL_TYPE_INT,
                    "Vignetting on llx: "
                    "pixels vignetted from lower left corner "
                    "X coordinate "
                    "of contributing cubes before coaddition ",
                    "sinfoni.objnod",
                    0,0,63);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"objnod-vllx");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.objnod.vlly",
                    CPL_TYPE_INT,
                    "Vignetting on lly: "
                    "pixels vignetted from lower left corner "
                    "Y coordinate "
                    "of contributing cubes before coaddition ",
                    "sinfoni.objnod",
                    0,0,63);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"objnod-vlly");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.objnod.vurx",
                    CPL_TYPE_INT,
                    "Vignetting on urx: "
                    "pixels vignetted from upper right corner "
                    "X coordinate "
                    "of contributing cubes before coaddition ",
                    "sinfoni.objnod",
                    0,0,63);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"objnod-vurx");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.objnod.vury",
                    CPL_TYPE_INT,
                    "Vignetting on ury: "
                    "pixels vignetted from upper right corner "
                    "Y coordinate "
                    "of contributing cubes before coaddition ",
                    "sinfoni.objnod",
                    0,0,63);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"objnod-vury");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.objnod.fcol",
                    CPL_TYPE_DOUBLE,
                    "First column offset: ",
                    "sinfoni.objnod",
                    0.);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"objnod-fcol");
    cpl_parameterlist_append(list, p);

    /* switch to activate the sky residuals correction */
    p = cpl_parameter_new_value("sinfoni.objnod.sky_cor",
                    CPL_TYPE_BOOL,
                    "Sky residuals correction: ",
                    "sinfoni.objnod",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-sky_cor");
    cpl_parameterlist_append(list, p);


    /* switch to activate the flux correction correction */
    p = cpl_parameter_new_value("sinfoni.objnod.flux_cor",
                    CPL_TYPE_BOOL,
                    "Apply flux correction after rebinning: ",
                    "sinfoni.objnod",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-flux_cor");
    cpl_parameterlist_append(list, p);


    /* switch to activate the flux correction correction */
    p = cpl_parameter_new_value("sinfoni.objnod.mosaic_max_size",
                    CPL_TYPE_INT,
                    "Maximum allowed size for cubes mosaic is "
                    "100*mosaic_max_size ",
                    "sinfoni.objnod",
                    14196);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"objnod-mosaic_max_size");
    cpl_parameterlist_append(list, p);


}
