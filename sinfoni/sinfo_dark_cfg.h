/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   File name     :    sinfo_dark_cfg.h
   Author    :    Juergen Schreiber
   Created on    :    February 2002
   Description    :    sinfo_dark_cfg.c definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_DARK_CFG_H
#define SINFO_DARK_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  master dark frame generation blackboard container

  This structure holds all information related to the master dark 
  frame generation
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct dark_config {
/*-------General---------*/
        char inFile[FILE_NAME_SZ] ;       /* file name of frame list */
        char ** inFrameList ; /* input averaged, bad pixel corrected, 
                                 off subtracted, flatfielded, spectral 
                                 tilt corrected list of frames */
        int nframes ;         /* number of frames in the list */
        char outName[FILE_NAME_SZ] ; /* output name of resulting fits 
                                        data cube */

/*------ CleanMean ------*/
        /* the fraction [0...1] of rejected low intensity pixels when 
           taking the average of columns */
        float lo_reject ;
        /* the fraction [0...1] of rejected high intensity pixels when 
           taking the average of columns */
        float hi_reject ;
/*------ QCLOG ------*/
        /* RON */
        
  int qc_ron_xmin;
  int qc_ron_xmax;
  int qc_ron_ymin;
  int qc_ron_ymax;
  int qc_ron_hsize;
  int qc_ron_nsamp;
  /* FPN */
  int qc_fpn_xmin;
  int qc_fpn_xmax;
  int qc_fpn_ymin;
  int qc_fpn_ymax;
  int qc_fpn_hsize;
  int qc_fpn_nsamp;

} dark_config ;
/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name    sinfo_dark_cfg_create()
   @memo    allocate memory for a dark_config struct
   @return  pointer to allocated base dark_cfg structure
   @note    only the main (base) structure is allocated
 */

dark_config * 
sinfo_dark_cfg_create(void);
/**
   @name    sinfo_dark_cfg_destroy()
   @memo   deallocate all memory associated with a dark_config data structure
   @param   dark_config to deallocate
   @return  void
*/
void 
sinfo_dark_cfg_destroy(dark_config * cc);
 

#endif
