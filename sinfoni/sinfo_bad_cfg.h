/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    bad_cfg.h
   Author         :    Juergen Schreiber
   Created on    :    October 2001
   Description    :    bad_ini definitions + handling prototypes

 ---------------------------------------------------------------------------*/
#ifndef SINFO_BAD_CFG_H
#define SINFO_BAD_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>

/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  bad pixels search blackboard container

  This structure holds all information related to the bad pixels search
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct bad_config {
/*-------General---------*/
        char inFile[FILE_NAME_SZ] ; /* file name of the file containing 
                                       the list of all input frames */
        char outName[FILE_NAME_SZ] ; /* output name of resulting bad pixel 
                                        mask (fits file)*/
        char ** framelist ; /* list of frames */
        int     nframes ; /* number of frames in frame list */

/*------ BadPix ------*/
        /* factor of noise within which the pixels are used to fit a 
           straight line to the column intensity */
        float sigmaFactor ;      
        /* factor of calculated standard deviation beyond which the 
           deviation of a pixel value from the 
           median of the 8 nearest neighbors declares a pixel as bad */
        float factor ;
        /* number of iterations of sinfo_median filter */
        int iterations ;
        /* percentage of extreme pixel value to reject when calculating 
           the mean and stdev */
        float loReject ;
        float hiReject ;
        /* pixel coordinate of lower left edge of a rectangle zone from 
           which image statistics are computed */
        int llx ;
        int lly ;
        /* pixel coordinate of upper right edge of a rectangle zone from 
           which image statistics are computed */
        int urx ;
        int ury ;
/*------ Thresh ------*/
        /* indicates if the values beyond threshold values should 
           be marked as bad before proceeding 
           to sinfo_median filtering */
        int threshInd ;
        /* factor to the clean standard deviation to define the 
           threshold deviation from the clean mean */
        float meanfactor ;
        /* minimum vlaue of good data */
        float mincut ;
        /* maximum vlaue of good data */
        float maxcut ;
        /* indicates which method will be used */
        int methodInd ;
} bad_config ;

/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/


/**
   @name    sinfo_bad_cfg_create()
   @memo    allocate memory for a bad_cfg struct
   @return  pointer to allocated base bad_cfg structure
   @note    only the main (base) structure is allocated
 */

bad_config * 
sinfo_bad_cfg_create(void);


/**
   @name   sinfo_bad_cfg_destroy()
   @memo deallocate all memory associated with a bad_config data structure
   @param  sc   bad_config to deallocate
   @return void
*/
void 
sinfo_bad_cfg_destroy(bad_config * sc);
 

#endif
