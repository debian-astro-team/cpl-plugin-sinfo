/* $Id: sinfo_pfits.h,v 1.6 2012-03-22 15:26:10 amodigli Exp $
 *
 * This file is part of the NACO Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-03-22 15:26:10 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_PFITS_H
#define SINFO_PFITS_H

/*-----------------------------------------------------------------------------
                                   Includes
 -----------------------------------------------------------------------------*/
#include <cpl.h>

/*
#include <sinfo_dfs.h>
*/

/*-----------------------------------------------------------------------------
                                   Functions prototypes
 -----------------------------------------------------------------------------*/
#define SINFO_AIRM_START  "ESO TEL AIRM START"
#define SINFO_AIRM_END    "ESO TEL AIRM END"
#define SINFO_DET_GAIN "ESO DET OUT1 GAIN"
#define SINFO_AIRM_START  "ESO TEL AIRM START"
#define SINFO_AIRM_END    "ESO TEL AIRM END"

CPL_BEGIN_DECLS
float sinfo_pfits_get_pixelscale (const char * filename );
double sinfo_pfits_get_targ_alpha(const cpl_propertylist * plist);
double sinfo_pfits_get_targ_delta(const cpl_propertylist * plist);
double sinfo_pfits_get_airm_mean (const cpl_propertylist * plist);
double sinfo_pfits_get_gain (const cpl_propertylist * plist);
double sinfo_pfits_get_airm_start (const cpl_propertylist * plist);
double sinfo_pfits_get_airm_end (const cpl_propertylist * plist);

/*
char * sinfo_pfits_get_rec1raw1name(const char * filename) ;
*/
double  sinfo_pfits_get_exptime(const char *) ;
char * sinfo_pfits_get_mode(const cpl_propertylist *) ;

double sinfo_pfits_get_pixscale(const cpl_propertylist * plist);
double sinfo_pfits_get_exp_time(const cpl_propertylist* plist);

double sinfo_pfits_get_posangle(const cpl_propertylist * plist);
int sinfo_pfits_get_rom(const cpl_propertylist * propertylist);
double sinfo_pfits_get_airmass_start(const cpl_propertylist * plist) ;
const char * sinfo_pfits_get_templateid(const cpl_propertylist * plist) ;
double sinfo_pfits_get_dit(const cpl_propertylist * plist) ;
int    sinfo_pfits_get_ndit(const cpl_propertylist * plist) ;

double sinfo_pfits_get_ditndit(const char* name);
/* not used */
double sinfo_pfits_get_ra(const cpl_propertylist *) ;

double sinfo_pfits_get_mjdobs(const cpl_propertylist *) ;
double sinfo_pfits_get_cumoffsetx(const cpl_propertylist *) ;
double sinfo_pfits_get_cumoffsety(const cpl_propertylist *) ;
double sinfo_pfits_get_airmass_end(const cpl_propertylist *) ;
double sinfo_pfits_get_dec(const cpl_propertylist *) ;
int sinfo_pfits_get_naxis1(const cpl_propertylist *) ;
int sinfo_pfits_get_naxis2(const cpl_propertylist *) ;
int sinfo_pfits_get_naxis3(const cpl_propertylist *) ;


double sinfo_pfits_get_crval1(const cpl_propertylist *) ;
double sinfo_pfits_get_crval2(const cpl_propertylist *) ;
double sinfo_pfits_get_crval3(const cpl_propertylist *) ;

double sinfo_pfits_get_crpix1(const cpl_propertylist *) ;
double sinfo_pfits_get_crpix2(const cpl_propertylist *) ;
double sinfo_pfits_get_crpix3(const cpl_propertylist *) ;

double sinfo_pfits_get_cdelt1(const cpl_propertylist *) ;
double sinfo_pfits_get_cdelt2(const cpl_propertylist *) ;
double sinfo_pfits_get_cdelt3(const cpl_propertylist *) ;
cpl_error_code
sinfo_get_wcs_cube(const cpl_propertylist* plist, double *clambda,
         double *dis, double *cpix, double *cx, double *cy);

cpl_error_code
sinfo_plist_set_extra_keys(cpl_propertylist* plist,
			 const char* hduclas1,
			 const char* hduclas2,
			 const char* hduclas3,
			 const char* scidata,
			 const char* errdata,
			 const char* qualdata,
                         const int type);
CPL_END_DECLS

#endif
