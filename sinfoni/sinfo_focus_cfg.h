/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_focus_cfg.h
   Author    :    Juergen Schreiber
   Created on    :    February 2002
   Description    :    focus_cfg.c definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_FOCUS_CFG_H
#define SINFO_FOCUS_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <cpl.h>
#include "sinfo_globals.h"
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  point source 2D-Gaussian fit blackboard container

  This structure holds all information related to the 2D-Gaussian fit
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct focus_config {
/*-------General---------*/
        char inFile[FILE_NAME_SZ] ;       /* file name of frame list */
        char ** inFrameList ; /* input averaged, bad pixel corrected, 
                                 off subtracted, flatfielded, spectral tilt 
                                 corrected list of frames */
        int nframes ;        /* number of frames in the list */
        char outName[FILE_NAME_SZ] ; /* output name of resulting 
                                        fits data cube */

/*------ Reconstruction ------*/
        /* the fraction [0...1] of rejected low intensity pixels 
           when taking the average of columns */
        float lo_reject ;
        /* the fraction [0...1] of rejected high intensity pixels 
           when taking the average of columns */
        float hi_reject ;
        /* indicates if the slitlet distances are determined by a 
           north-south test (1) 
           or slitlet edge fits (0) */ 
        int northsouthInd  ;
        /* name of the ASCII list of the fitted slitlet edge 
          positions or the distances of the slitlets */
        char poslist[FILE_NAME_SZ] ;
        /* number of slitlets (32) */
        int nslits ;
        /* sub pixel position of the column position of the left sinfo_edge of 
           the first slitlet needed if the slitlet distances were determined 
           by a north south test */
        char firstCol[FILE_NAME_SZ] ;
    /* indicator for the shifting method to use */
    char  method[1] ;
        /* order of polynomial if the polynomial interpolation shifting 
           method is used */
        int order ;
/*------ Gauss2Dfit ------*/
        /* lower left sinfo_edge coordinates of fitting box for 2D 
           Gaussian fit */
        int llx ;
        int lly ;
        /* half length in pixels of the box within the point source 
           is fitted in x and y-direction */
        int halfbox_x ;
        int halfbox_y ;
        /* mask parameters ( 1 or 0 ) for the fit parameters. If 1 
           the corresponding parameter
           is set free, if 0 the parameter is kept fixed. */
        int mpar0 ; /* mask for the x-position */
        int mpar1 ; /* mask for the y-position */
        int mpar2 ; /* mask for the amplitude */
        int mpar3 ; /* mask for the background */
        int mpar4 ; /* mask for the fwhmx */
        int mpar5 ; /* mask for the fwhmy */
        int mpar6 ; /* mask for the position angle of fwhmx line */
        /* name of the resulting ASCII file containing the fit parameters */
        char fitlist[FILE_NAME_SZ] ;
        /* indicator if the resulting 2D-Gaussian is stored in a fits 
           file or not */
        int plotGaussInd ;
        /* name of the fits file containing the resulting 2D-Gaussian */
        char gaussplotName[FILE_NAME_SZ] ;
} focus_config ;

/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name     sinfo_focus_cfg_create()
   @return   pointer to allocated base focus_cfg structure
   @memo     allocate memory for a focus_config struct
   @note     only the main (base) structure is allocated
 */

focus_config * 
sinfo_focus_cfg_create(void);

/**
   @name   sinfo_focus_cfg_destroy()
   @memo   deallocate all memory associated with a focus_config data structure
   @param  focus_config to deallocate
   @return void
*/
void 
sinfo_focus_cfg_destroy(focus_config * cc);

#endif
