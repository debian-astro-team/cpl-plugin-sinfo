/* $Id: sinfo_bp_sky_config.c,v 1.4 2007-06-06 07:10:45 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2007-06-06 07:10:45 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/****************************************************************
 *           Bad pixel search  (normal method)                  *
 ****************************************************************/
#include "sinfo_bp_sky_config.h"
/**
 * @addtogroup sinfo_bad_pix_search Bad Pixel Search
 *
 * TBD
 */

/**@{*/
/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* Bad pixel parameters */

void
sinfo_bp_sky_config_add(cpl_parameterlist *list)
{
    cpl_parameter *p;

    if (!list) {
        return;
    }


    /* Output file name */
    p = cpl_parameter_new_value("sinfoni.bp_sky.out_filename",
                    CPL_TYPE_STRING,
                    "Output File Name: ",
                    "sinfoni.bp_sky",
                    "out_bp_sky.fits");


    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI, "out-bp_sky_filename");
    cpl_parameterlist_append(list, p);


    /* factor of noise within which the pixels are used to fit a straight line
   to the column intensity */
    p = cpl_parameter_new_value("sinfoni.bp_sky.sigma_factor",
                    CPL_TYPE_DOUBLE,
                    "Threshold Sigma Factor: "
                    "to remove the column intensity tilt only "
                    "pixels which lie within a defined noise"
                    "limit are used to fit a straight line",
                    "sinfoni.bp_noise",
                    5.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-s_factor");
    cpl_parameterlist_append(list, p);

    /* bad pixel search determination method */
    p = cpl_parameter_new_enum("sinfoni.bp_sky.method_index",
                    CPL_TYPE_INT,
                    "Bad pixel Method Index"
                    "1: median of nearest neighbors,"
                    "2: absolute distances check, "
                    "3: mean of nearest spectral neighbors",
                    "sinfoni.bp_sky",
                    1,
                    3,1,2,3);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-method_ind");
    cpl_parameterlist_append(list, p);

    /* factor of calculated standard deviation beyond which the deviation
     of a pixel value from the median of the 8 nearest neighbors declares 
     a pixel as bad */
    p = cpl_parameter_new_value("sinfoni.bp_sky.factor",
                    CPL_TYPE_DOUBLE,
                    "Factor: "
                    "if |pixel - sinfo_median| > factor * standard deviation -> "
                    "then the pixel value is replaced by "
                    "the median of the 8 nearest neighbors",
                    "sinfoni.bp_sky",
                    3.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-fct");
    cpl_parameterlist_append(list, p);

    /* no of iterations to find bad pix clusters */
    /* number of iterations of sinfo_median filter */
    p = cpl_parameter_new_value("sinfoni.bp_sky.iterations",
                    CPL_TYPE_INT,
                    "Iterations: number of iterations to of median"
                    " filtering to find bad pixel clusters",
                    "sinfoni.bp_sky",
                    8);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-it");
    cpl_parameterlist_append(list, p);

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.bp_sky.low_rejection",
                    CPL_TYPE_DOUBLE,
                    "low_rejection: "
                    "percentage of rejected low intensity "
                    "pixels before averaging",
                    "sinfoni.bp_sky",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-low_rej");
    cpl_parameterlist_append(list, p);

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.bp_sky.high_rejection",
                    CPL_TYPE_DOUBLE,
                    "high_rejection: "
                    "percentage of rejected high intensity "
                    "pixels before averaging",
                    "sinfoni.bp_sky",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-upp_rej");
    cpl_parameterlist_append(list, p);


    /* position in the frame lower left in X */
    /* pixel coordinate of lower left sinfo_edge of a rectangle zone from which
     image statistics are computed */
    p = cpl_parameter_new_range("sinfoni.bp_sky.llx",
                    CPL_TYPE_INT,
                    "llx: "
                    "to compute image statistics on a rectangular"
                    "zone of the image the coordinates of the "
                    "rectangle are needed:"
                    "lower left x coordinate",
                    "sinfoni.bp_sky",
                    LLX,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-llx");
    cpl_parameterlist_append(list, p);



    /* position in the frame lower left in Y */
    /* pixel coordinate of lower left sinfo_edge of a rectangle zone from which
     image statistics are computed */
    p = cpl_parameter_new_range("sinfoni.bp_sky.lly",
                    CPL_TYPE_INT,
                    "lly: "
                    "to compute image statistics on a rectangular"
                    "zone of the image the coordinates of the "
                    "rectangle are needed:"
                    "lower left y coordinate",
                    "sinfoni.bp_sky",
                    LLY,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-lly");
    cpl_parameterlist_append(list, p);

    /* Upper right position in CCD : should be urx */
    /* pixel coordinate of upper right sinfo_edge of a rectangle zone from which
    image statistics are computed */
    p = cpl_parameter_new_range("sinfoni.bp_sky.urx",
                    CPL_TYPE_INT,
                    "urx: "
                    "to compute image statistics on a rectangular"
                    "zone of the image the coordinates of the "
                    "rectangle are needed:"
                    "upper right x coordinate",
                    "sinfoni.bp_sky",
                    URX,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-urx");
    cpl_parameterlist_append(list, p);

    /* Upper right position in CCD : should be ury */
    /* pixel coordinate of upper right sinfo_edge of a rectangle zone from which
    image statistics are computed */
    p = cpl_parameter_new_range("sinfoni.bp_sky.ury",
                    CPL_TYPE_INT,
                    "ury: "
                    "to compute image statistics on a rectangular"
                    "zone of the image the coordinates of the "
                    "rectangle are needed:"
                    "upper right y coordinate",
                    "sinfoni.bp_sky",
                    URY,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-ury");
    cpl_parameterlist_append(list, p);

    /* boolean (implemented as integer) useda s a switch */
    /* indicates if the values beyond threshold values should be marked
     as bad before proceeding to sinfo_median filtering */
    p = cpl_parameter_new_value("sinfoni.bp_sky.threshold_index",
                    CPL_TYPE_BOOL,
                    "Threshold Index: "
                    "indicator that indicates if the values "
                    "beyond a threshold deviation from the mean "
                    "are flagged as bad pixels",
                    "sinfoni.bp_sky",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-thr-ind");
    cpl_parameterlist_append(list, p);

    /* threshold value. Float. Threshold used to find bad pixel */
    /* factor to the clean standard deviation to define the threshold
     deviation from the clean mean */
    p = cpl_parameter_new_range("sinfoni.bp_sky.mean_factor",
                    CPL_TYPE_DOUBLE,
                    "Mean Factor: "
                    "factor to the clean standard deviation to "
                    "define the threshold deviation from the "
                    "clean mean",
                    "sinfoni.bp_sky",
                    10.,0.1,1.e10);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-mean-fct");
    cpl_parameterlist_append(list, p);



    p = cpl_parameter_new_value("sinfoni.bp_sky.min_cut",
                    CPL_TYPE_DOUBLE,
                    "the minimum value of real data",
                    "sinfoni.bp_sky",
                    0.1);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-min_cut");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.bp_sky.max_cut",
                    CPL_TYPE_DOUBLE,
                    "the minimum value of real data",
                    "sinfoni.bp_sky",
                    50000.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_sky-max_cut");
    cpl_parameterlist_append(list, p);

    return;

}
/**@}*/
