/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-02-08 07:15:18 $
 * $Revision: 1.4 $
 */

#ifndef SINFO_ATMO_DISP_H_
#define SINFO_ATMO_DISP_H_

cpl_error_code 
sinfo_atm_dispersion_cube(cpl_imagelist* pCube,
		int centpix, // central plane in the cube CRPIX3
		double centlambda, // wavelength of the central plane CRVAL3
		double Tc, // temperature in Celsius TEL.AMBI.TEMP
		double Rh, // relative humidity in % TEL.AMBI.RHUM
		double airm, // airmass for the moment of observation TEL.AMBI.PRES
		double p, // atmospheric pressure TEL.AMBI.PRES
		double parallactic, // TEL.PARANG
		double pixelscale, // could be for SINFONI 0.025, 0.100, 0.250
		double pixelsz // microns per pixel CDELT3
			  );


void calcAtmosphericDispersion( double lambda, double lambda0, double *shiftX, double *shiftY, double *deltaR,
                   double Tc,
                   double rh,
                   double airm,
                   double p,
                   double parallactic,
                   double pixelscale);
cpl_error_code sinfo_atmo_dispersion_cube(cpl_imagelist* pCube,
		int centpix, // central plane in the cube
		double centlambda, // wavelength of the central plane
		double Tc, // temperature in Celsius TEL.AMBI.TEMP
		double Rh, // relative humidity in % TEL.AMBI.RHUM
		double airm, // airmass for the moment of observation TEL.AMBI.PRES
		double p, // atmospheric pressure TEL.AMBI.PRES
		double parallactic, // TEL.PARANG
		double pixelscale, // could be for SINFONI 0.025, 0.100, 0.250
		double pixelsz // microns per pixel CDELT3
		);
cpl_imagelist* sinfo_atmo_apply_cube_polynomial_shift(
		cpl_polynomial* poly,
		cpl_imagelist* pCube,
		double lambda0,
		double airmass,
		double parallactic, // should be in radian
		double pixelsz,
		int centpix);
cpl_polynomial* sinfo_atmo_load_polynom(const char* filename);
#endif /* SINFO_ATM_DISP_H_ */
