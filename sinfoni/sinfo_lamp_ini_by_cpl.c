/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------

   File name    :   sinfo_lamp_ini_by_cpl.c
   Author       :   Andrea Modigliani
   Created on   :   May 26, 2004
   Description  :   lamp spectrum cpl input handling for SPIFFI
 ---------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
                                Includes
 ---------------------------------------------------------------------------*/
#include <string.h>
#include "sinfo_lamp_ini_by_cpl.h"
#include "sinfo_functions.h"
#include "sinfo_pro_types.h"
/*---------------------------------------------------------------------------
                    Functions private to this module
 ---------------------------------------------------------------------------*/
static void     
parse_section_frames(lamp_config *, 
                     cpl_parameterlist* cpl_cfg, 
                     cpl_frameset* sof, 
                     cpl_frameset** raw, 
                     int* status);

static void     
parse_section_resampling(lamp_config *, cpl_parameterlist* cpl_cfg);
static void     
parse_section_extractspectrum(lamp_config *, cpl_parameterlist* cpl_cfg);

/**@{*/
/**
 * @addtogroup sinfo_lamp_cfg Flat frame manipulation functions
 *
 * TBD
 */


/**
  @name     sinfo_parse_cpl_input_lamp
  @memo     Parse input frames & parameters and create a blackboard.
  @param    cpl_cfg pointer to parameterlist
  @param    sof pointer to input set of frames
  @param    raw pointer to input set of raw frames
  @return   1 newly allocated lamp_config blackboard structure.
  @doc      The requested ini file is parsed and a blackboard object is 
            created, then updated accordingly. Returns NULL in case of error.
 */
lamp_config * 
sinfo_parse_cpl_input_lamp(cpl_parameterlist* cpl_cfg, 
                           cpl_frameset* sof, 
                           cpl_frameset** raw)

{
    lamp_config   *       cfg= sinfo_lamp_cfg_create();
    int status=0;
    /*
     * Perform sanity checks, fill up the structure with what was
     * found in the ini file
     */


    parse_section_resampling    (cfg, cpl_cfg);
    parse_section_extractspectrum (cfg, cpl_cfg);
    parse_section_frames    (cfg, cpl_cfg, sof, raw,&status);

    if (status > 0) {
        sinfo_msg_error("parsing cpl input");
        sinfo_lamp_cfg_destroy(cfg);
        cfg = NULL ;
        return NULL ;
    }
    return cfg ;
}


/**
  @name     parse_section_frames
  @memo     Parse input frames.
  @param    cfg pointer to lamp_config structure
  @param    cpl_cfg pointer to parameterlist
  @param    sof pointer to input set of frames
  @param    raw pointer to input set of raw frames
  @return   void.
 */
static void     
parse_section_frames(lamp_config * cfg,
                     cpl_parameterlist * cpl_cfg,
                     cpl_frameset * sof,
                     cpl_frameset ** raw,
                     int* status)
{

    cpl_frame* frame   = NULL;
    cpl_parameter *p;
    int nraw=0;
    char spat_res[FILE_NAME_SZ];
    char lamp_status[FILE_NAME_SZ];
    char band[FILE_NAME_SZ];
    int ins_set=0;

    /* Input */

    *raw=cpl_frameset_new();
    sinfo_extract_raw_frames_type(sof,raw,PRO_FLUX_LAMP_STACKED);

    nraw    = cpl_frameset_get_size(*raw);

    if(nraw<1) {
        sinfo_msg_error("no good raw frame %s in input!",PRO_FLUX_LAMP_STACKED);
        (*status)++;
        return;
    }
    frame = cpl_frameset_get_frame(*raw,0);
    strcpy(cfg -> inFrame,cpl_strdup(cpl_frame_get_filename(frame)));

    /* Output */
    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.lamp_spec.output_filename");
    strcpy(cfg -> outName, cpl_parameter_get_string(p));

    sinfo_get_spatial_res(frame,spat_res);
    switch(sinfo_frame_is_on(frame))
    {

    case 0: 
        strcpy(lamp_status,"on");
        break;
    case 1: 
        strcpy(lamp_status,"off");
        break;
    case -1:
        strcpy(lamp_status,"undefined");
        break;
    default: 
        strcpy(lamp_status,"undefined");
        break;

    }
    sinfo_get_band(frame,band);
    sinfo_msg("Spatial resolution: %s lamp_status: %s band: %s \n",
              spat_res,    lamp_status,    band);


    sinfo_get_ins_set(band,&ins_set);

    if(NULL != cpl_frameset_find(sof,PRO_WAVE_MAP)) {
        frame = cpl_frameset_find(sof,PRO_WAVE_MAP);
        strcpy(cfg -> wavemapim,cpl_strdup(cpl_frame_get_filename(frame)));
    } else {
        sinfo_msg_error("Frame %s not found! Exit!", PRO_WAVE_MAP);
        (*status)++;
        return;
    }


    return ;

}



/**
  @name     parse_section_resampling
  @memo     Parse resampling parameters.
  @param    cfg pointer to lamp_config structure
  @param    cpl_cfg pointer to parameterlist
  @return   void.
 */
static void     
parse_section_resampling(lamp_config * cfg, cpl_parameterlist * cpl_cfg)
{

    cpl_parameter *p;
    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.lamp_spec.ncoeffs");
    cfg -> ncoeffs =  cpl_parameter_get_int(p);

    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.lamp_spec.nrows");
    cfg -> nrows =  cpl_parameter_get_int(p);

    return ;

}
/**
  @name     parse_section_extractspectrum
  @memo     Parse extractspectrum parameters.
  @param    cfg pointer to lamp_config structure
  @param    cpl_cfg pointer to parameterlist
  @return   void.
 */
static void     
parse_section_extractspectrum(lamp_config * cfg, cpl_parameterlist * cpl_cfg)
{
    cpl_parameter *p;

    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.lamp_spec.lower_rejection");
    cfg -> loReject = cpl_parameter_get_double(p);

    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.lamp_spec.higher_rejection");
    cfg -> hiReject = cpl_parameter_get_double(p);

    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.lamp_spec.counts_to_intensity");
    cfg -> countsToIntensity = cpl_parameter_get_double(p);

    return ;

}
/**
@name sinfo_lamp_free
@memo deallocate lamp_config structure
@param cfg pointer to lamp_config structure
@return void
 */
void
sinfo_lamp_free(lamp_config * cfg)
{  
    sinfo_lamp_cfg_destroy(cfg);
    return;

}
/**@}*/
