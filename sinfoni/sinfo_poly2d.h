/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------
   
   File name     :    sinfo_poly2d.h
   Author         :    N. Devillard
   Created on    :    22 Jun 1999
   Description    :    2D polynomial handling

 *--------------------------------------------------------------------------*/

/*
    $Id: sinfo_poly2d.h,v 1.4 2007-06-06 07:10:45 amodigli Exp $
    $Author: amodigli $
    $Date: 2007-06-06 07:10:45 $
    $Revision: 1.4 $
*/

#ifndef SINFO_POLY2D_H
#define SINFO_POLY2D_H

/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sinfo_ipow.h"
#include "sinfo_msg.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/

/**
  @name        poly2d
  @memo        2d Polynomial object.
  @doc

  The following structure defines a 2d polynomial. 'deg' is the
  highest degree in the polynomial.
  
  \begin{itemize}
  \item 'nc' contains the number of coefficients in px, py, and c.
  \item 'px' and 'py' contain the powers of resp. x and y.
  \item 'c' contains the coefficients themselves.
  \end{itemize}

  For example, if you want to store the following polynomial:
  \begin{verbatim}
  p(x,y) = p0 + p1.x + p2.y + p3.x.y + p4.x^2 + p5.y^2
  \end{verbatim}

  You would have:

  \begin{verbatim}
  nc  = 6 (from 0 to 5 incl.)
  px contains:  0  1  0  1  2  0
  py contains:  0  0  1  1  0  2
  c  contains: p0 p1 p2 p3 p4 p5
  So that given x0 and y0, computing the polynomial is done with:
 
  poly2d    p ;
  int       i ;
  double    x0, y0, poly ;
 
  poly = 0.00 ;
  for (i=0 ; i<p.nc ; i++) {
      poly += p.c[i] * sinfo_ipow(x0, p.px[i]) * sinfo_ipow(y0, p.py[i]) ;
  }

  or simply:
  poly = sinfo_poly2d_compute(&p, x0, y0);
  \end{verbatim}
 */

struct _2D_POLY_ {
    int            nc ;        /* number of coefficients in px, py, c */
    int        *    px ;        /* powers of x                         */
    int        *    py ;        /* powers of y                         */
    double    *    c ;            /* polynomial coefficients             */
} ;

typedef struct _2D_POLY_ poly2d ;



/*---------------------------------------------------------------------------
                               Function codes    
 ---------------------------------------------------------------------------*/

/**
  @name     sinfo_poly2d_compute
  @memo     Compute the value of a poly2d at a given point.
  @param    p   Poly2d object.
  @param    x   x coordinate.
  @param    y   y coordinate.
  @return   The value of the 2d polynomial at (x,y) as a double.
  @doc
 
  This computes the value of a poly2d in a single point. To
  compute many values in a row, see sinfo_poly2d_compute_array().
 */

double
sinfo_poly2d_compute(
    poly2d    *    p,
    double        x,
    double        y
);

#endif
