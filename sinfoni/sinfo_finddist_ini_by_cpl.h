/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_finddist_ini_by_cpl.h
   Author       :   Andrea Modigliani
   Created on   :   Aug 12, 2004
   Description  :   Distortions, slitlets distances, 
                    first column determination cpl input 
                    parameters parsing for SINFONI
 ---------------------------------------------------------------------------*/
#ifndef SINFO_FINDDIST_INI_BY_CPL_H
#define SINFO_FINDDIST_INI_BY_CPL_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_functions.h"
#include "sinfo_finddist_cfg.h"
#include "sinfo_msg.h"
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/
/**
  @name     sinfo_parse_cpl_input_finddist
  @memo     Parse CPL input.
  @param    cpl_cfg   pointer to parameter list
  @param    sof pointer to input set of frames
  @param    raw pointer to input set of raw frames
  @return   1 newly allocated wave_config blackboard structure.
  @doc      The requested ini file is parsed and a blackboard object is 
            created, then updated accordingly. Returns NULL in case of error.
 */

finddist_config * 
sinfo_parse_cpl_input_finddist(cpl_parameterlist * cpl_cfg, 
                               cpl_frameset* sof, 
                   cpl_frameset** raw);
/**
 @name sinfo_finddist_free
 @memo deallocate finddist_config structure
 @param cfg pointer to finddist_config structure
 @return void
*/
void 
sinfo_finddist_free(finddist_config ** cfg);

#endif
