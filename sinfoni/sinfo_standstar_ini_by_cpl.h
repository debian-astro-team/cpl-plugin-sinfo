/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*----------------------------------------------------------------------------
   
   File name    :   sinfo_standstar_ini_by_cpl.h
   Author       :   Andrea Modigliani
   Created on   :   May 23, 2004
   Description  :   cpl input handling for SINFONI data reduction 
                    of a standard star

 ---------------------------------------------------------------------------*/


#ifndef SINFO_STANDSTAR_INI_BY_CPL_H
#define SINFO_STANDSTAR_INI_BY_CPL_H


/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/

#include <stdio.h>
#include <cpl.h>
#include "sinfo_standstar_cfg.h"
#include "sinfo_msg.h"
 
 
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/


/* generateStandstar_ini_file */

/*-------------------------------------------------------------------------*/
/**
  @name     parse_standstar_ini_file
  @memo     Parse a ini_name.ini file and create a blackboard.
  @param    ini_name    Name of the ASCII file to parse.
  @return   1 newly allocated standstar_config blackboard structure.
  @doc

  The requested ini file is parsed and a blackboard object is created, then
  updated accordingly. Returns NULL in case of error.
 */
/*--------------------------------------------------------------------------*/

void
sinfo_stdstar_free(standstar_config  ** cfg);
standstar_config * 
sinfo_parse_cpl_input_standstar(cpl_parameterlist * cpl_cfg, 
                                cpl_frameset* sof, 
                                cpl_frameset** raw) ;

#endif
