/* $Id: sinfo_north_south_test_config.c,v 1.5 2012-03-03 09:50:08 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 09:50:08 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *   North_South_Test Frames Data Reduction Parameter Initialization        *
 ****************************************************************/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include "sinfo_north_south_test_config.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_distortion North South test configuration
 *
 * TBD
 */

/**
 @name sinfo_north_south_test_config_add
 @brief set parameters and their defaults for north south test
 @param list parameterlist
 @return void
 */
void
sinfo_north_south_test_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }
    /* Clean Mean */
    p =
                    cpl_parameter_new_range(
                                    "sinfoni.north_south_test.low_rejection",
                                    CPL_TYPE_DOUBLE,
                                    "lower rejection: "
                                                    "percentage of rejected low intensity pixels "
                                                    "before averaging",
                                    "sinfoni.north_south_test", 0.1, 0.0, 1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-lo_rej");
    cpl_parameterlist_append(list, p);

    p =
                    cpl_parameter_new_range(
                                    "sinfoni.north_south_test.high_rejection",
                                    CPL_TYPE_DOUBLE,
                                    "higher rejection: "
                                                    "percentage of rejected high intensity pixels "
                                                    "before averaging",
                                    "sinfoni.north_south_test", 0.1, 0.0, 1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-hi_rej");
    cpl_parameterlist_append(list, p);

    p =
                    cpl_parameter_new_value("sinfoni.north_south_test.mask_ind",
                                    CPL_TYPE_BOOL,
                                    "Mask Index: "
                                                    "indicator if a bad pixel mask is applied or not",
                                    "sinfoni.north_south_test", FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-mask_ind");
    cpl_parameterlist_append(list, p);

    /* Gauss Convolution */
    p = cpl_parameter_new_value("sinfoni.north_south_test.gauss_ind",
                    CPL_TYPE_BOOL, "Gauss Index: ", "sinfoni.north_south_test",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-gauss_ind");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.north_south_test.kernel_half_width",
                    CPL_TYPE_INT, "Kernel Half Width "
                                    "kernel half width of the Gaussian "
                                    "response function",
                    "sinfoni.north_south_test", 2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-khw");
    cpl_parameterlist_append(list, p);

    /* North South Test */

    p = cpl_parameter_new_value("sinfoni.north_south_test.half_width",
                    CPL_TYPE_INT, "Half Width", "sinfoni.north_south_test", 4);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-hw");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.north_south_test.fwhm",
                    CPL_TYPE_DOUBLE, "FWHM", "sinfoni.north_south_test", 2.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-fwhm");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.north_south_test.min_diff",
                    CPL_TYPE_DOUBLE, "Minimum of Difference",
                    "sinfoni.north_south_test", 1.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-min_diff");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.north_south_test.dev_tol",
                    CPL_TYPE_DOUBLE, "Dev Tol", "sinfoni.north_south_test",
                    20.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "ns-dev_tol");
    cpl_parameterlist_append(list, p);

}
