/* $Id: sinfo_dark_config.c,v 1.5 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/****************************************************************
 *   Dark Frames Data Reduction Parameter Initialization        *
 ****************************************************************/
#include "sinfo_dark_config.h"
/**@{*/
/**
 * @addtogroup sinfo_dark_cfg Dark manipulation functions
 *
 * TBD
 */

/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* Dark Frame Data Reduction parameters */


void
sinfo_dark_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.dark.low_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection",
                    "sinfoni.dark",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-lo_rej");
    cpl_parameterlist_append(list, p);

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */


    /* QC LOG */
    /* RON */

    p = cpl_parameter_new_range("sinfoni.dark.high_rejection",
                    CPL_TYPE_DOUBLE,
                    "higher rejection",
                    "sinfoni.dark",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-hi_rej");
    cpl_parameterlist_append(list, p);



    p = cpl_parameter_new_range("sinfoni.dark.qc_ron_xmin",
                    CPL_TYPE_INT,
                    "qc_ron_xmin",
                    "sinfoni.dark",
                    1,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_ron_xmin");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.dark.qc_ron_xmax",
                    CPL_TYPE_INT,
                    "qc_ron_xmax",
                    "sinfoni.dark",
                    2048,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_ron_xmax");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_range("sinfoni.dark.qc_ron_ymin",
                    CPL_TYPE_INT,
                    "qc_ron_ymin",
                    "sinfoni.dark",
                    1,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_ron_ymin");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.dark.qc_ron_ymax",
                    CPL_TYPE_INT,
                    "qc_ron_ymax",
                    "sinfoni.dark",
                    2048,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_ron_ymax");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.dark.qc_ron_hsize",
                    CPL_TYPE_INT,
                    "qc_ron_hsize",
                    "sinfoni.dark",
                    4);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_ron_hsize");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.dark.qc_ron_nsamp",
                    CPL_TYPE_INT,
                    "qc_ron_nsamp",
                    "sinfoni.dark",
                    100);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_ron_nsamp");
    cpl_parameterlist_append(list, p);

    /* FPN */


    p = cpl_parameter_new_range("sinfoni.dark.qc_fpn_xmin",
                    CPL_TYPE_INT,
                    "qc_fpn_xmin",
                    "sinfoni.dark",
                    1,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_fpn_xmin");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.dark.qc_fpn_xmax",
                    CPL_TYPE_INT,
                    "qc_fpn_xmax",
                    "sinfoni.dark",
                    2048,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_fpn_xmax");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_range("sinfoni.dark.qc_fpn_ymin",
                    CPL_TYPE_INT,
                    "qc_fpn_ymin",
                    "sinfoni.dark",
                    1,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_fpn_ymin");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.dark.qc_fpn_ymax",
                    CPL_TYPE_INT,
                    "qc_fpn_ymax",
                    "sinfoni.dark",
                    2048,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_fpn_ymax");
    cpl_parameterlist_append(list, p);



    p = cpl_parameter_new_value("sinfoni.dark.qc_fpn_hsize",
                    CPL_TYPE_INT,
                    "qc_fpn_hsize",
                    "sinfoni.dark",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_fpn_hsize");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.dark.qc_fpn_nsamp",
                    CPL_TYPE_INT,
                    "qc_fpn_nsamp",
                    "sinfoni.dark",
                    1000);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dark-qc_fpn_nsamp");
    cpl_parameterlist_append(list, p);

}
/**@}*/
