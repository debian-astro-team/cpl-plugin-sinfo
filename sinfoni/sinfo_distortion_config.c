/* $Id: sinfo_distortion_config.c,v 1.6 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/****************************************************************
 *   Wavecal Frames Data Reduction Parameter Initialization        *
 ****************************************************************/

#include "sinfo_distortion_config.h"
#include "sinfo_globals.h"
/**@{*/
/*---------------------------------------------------------------------------*/
/**
 * @addtogroup sinfo_distortion       Distortion correction functions
 */
/*---------------------------------------------------------------------------*/

void
sinfo_distortion_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /* Output file name */
    /* output name of resulting fits wavelength map */


    /* Find Lines */
    /* indicates if the dispersion relation is already determined or not */
    p = cpl_parameter_new_value("sinfoni.distortion.calib_indicator",
                    CPL_TYPE_BOOL,
                    "Calib Indicator: "
                    "FALSE: if the dispersion relation is already "
                    "known, the routine can jump to the waveMap "
                    "section "
                    "TRUE: if the dispersion relation "
                    "must first be determined",
                    "sinfoni.distortion",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-calib_indicator");
    cpl_parameterlist_append(list, p);


    /* minimal difference of mean and sinfo_median column intensity */
    p = cpl_parameter_new_value("sinfoni.distortion.min_diff_mean_med_col_int",
                    CPL_TYPE_DOUBLE,
                    "Minimum Of Difference: "
                    "minimum difference of mean and median column "
                    "intensity to carry out the cross correlation",
                    "sinfoni.distortion",
                    10.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,
                            "dist-min_diff_mean_med_col_int");
    cpl_parameterlist_append(list, p);

    /* half width of a box within which the line must sit */
    p = cpl_parameter_new_value("sinfoni.distortion.half_width",
                    CPL_TYPE_INT,
                    "Half Width: "
                    "half width of a box within which the line "
                    "must be placed",
                    "sinfoni.distortion",
                    7);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-hw");
    cpl_parameterlist_append(list, p);

    /* sigma of Gaussian of artificial model spectra */
    p = cpl_parameter_new_value("sinfoni.distortion.sigma",
                    CPL_TYPE_DOUBLE,
                    "Sigma: sigma of Gaussian which is convolved "
                    "with the artificial spectrum generated using "
                    "the line list",
                    "sinfoni.distortion",
                    2.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-sigma");
    cpl_parameterlist_append(list, p);

    /* Wavelength Calibration */
    /* guess value for fwhm of emission lines */
    p = cpl_parameter_new_value("sinfoni.distortion.fwhm",
                    CPL_TYPE_DOUBLE,
                    "FWHM: initial guess value for the fwhm of "
                    "the Gaussian used for the line fit",
                    "sinfoni.distortion",
                    2.83);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-fwhm");
    cpl_parameterlist_append(list, p);

    /* minimum amplitude of a line to be fitted */
    p = cpl_parameter_new_value("sinfoni.distortion.min_amplitude",
                    CPL_TYPE_DOUBLE,
                    "Minimum Of Amplitude: "
                    "of the Gaussian to do the fit",
                    "sinfoni.distortion",
                    5.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-min_amplitude");
    cpl_parameterlist_append(list, p);

    /* maximal residual value for a valid fit */
    p = cpl_parameter_new_value("sinfoni.distortion.max_residual",
                    CPL_TYPE_DOUBLE,
                    "Maximum Residuals value: "
                    "beyond this value the fit is rejected",
                    "sinfoni.distortion",
                    0.5);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-max_residual");
    cpl_parameterlist_append(list, p);

    /* # of polynomial coefficients used for the dispersion relation */
    p = cpl_parameter_new_value("sinfoni.distortion.n_a_coefficients",
                    CPL_TYPE_INT,
                    "Number of A coefficients: number of "
                    "polynomial coefficients for the "
                    "dispersion relation",
                    "sinfoni.distortion",
                    4);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-n_a_coeffs");
    cpl_parameterlist_append(list, p);

    /* # of polynomial coefficients used for the fit of the dispersion
    coefficients */
    p = cpl_parameter_new_value("sinfoni.distortion.n_b_coefficients",
                    CPL_TYPE_INT,
                    "Number of B coefficients: "
                    "number of polynomial coefficients for the "
                    "polynomial fit of the dispersion coefficients",
                    "sinfoni.distortion",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-n_b_coeffs");
    cpl_parameterlist_append(list, p);

    /* minimal factor of the standard deviation of the fit coefficients */
    p = cpl_parameter_new_value("sinfoni.distortion.sigma_factor",
                    CPL_TYPE_DOUBLE,
                    "Sigma Factor: "
                    "Factor of the standard deviation of the "
                    "polynomial coefficients of the dispersion "
                    "relation beyond which the coefficients are "
                    "not used for the fit",
                    "sinfoni.distortion",
                    1.5);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-sigma_factor");
    cpl_parameterlist_append(list, p);

    /* indicates if the parameterized dispersion relation coefficients
       should be written into an ASCII file */
    p = cpl_parameter_new_value("sinfoni.distortion.write_coeffs_ind",
                    CPL_TYPE_BOOL,
                    "Write Coefficients Index: "
                    "indicates if the coefficients should "
                    "be written into a file or not",
                    "sinfoni.distortion",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-wcoeff_ind");
    cpl_parameterlist_append(list, p);

    /* indicates if the fit parameters should be written into an ASCII file */
    p = cpl_parameter_new_value("sinfoni.distortion.write_par_ind",
                    CPL_TYPE_BOOL,
                    "Write Parameter Index: "
                    "indicates if the fit parameters should "
                    "be written into a file or not ",
                    "sinfoni.distortion",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-par_ind");
    cpl_parameterlist_append(list, p);


    /* minimal distance of the slitlets in spectral direction */
    p = cpl_parameter_new_value("sinfoni.distortion.pixel_dist",
                    CPL_TYPE_INT,
                    "Minimal Slitlets's Distance in spectral direction",
                    "sinfoni.distortion",
                    15);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-pixel_dist");
    cpl_parameterlist_append(list, p);



    /* allowed pixel position tolerance between estimated and fitted line
     position
     */
    p = cpl_parameter_new_value("sinfoni.distortion.pixel_tol",
                    CPL_TYPE_DOUBLE,
                    "Pixel Tolerance: allowed pixel position "
                    "tolerance between estimated and fitted "
                    "line position",
                    "sinfoni.distortion",
                    5.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-pixel_tol");
    cpl_parameterlist_append(list, p);

    /* Wavelength Map */
    /* indicator if wavelength map should be generated or not */
    p = cpl_parameter_new_value("sinfoni.distortion.wave_map_ind",
                    CPL_TYPE_BOOL,
                    "Wavelength Map Indicator: "
                    "indicates if the wavelength calibration map "
                    "should be generated (TRUE) or not (FALSE)",
                    "sinfoni.distortion",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-wave_map_ind");
    cpl_parameterlist_append(list, p);

    /* magnifying factor for FFT */
    p = cpl_parameter_new_value("sinfoni.distortion.mag_factor",
                    CPL_TYPE_INT,
                    "Magnificator Factor: "
                    "magnifying factor for the number of pixels "
                    "in the columns needed for FFT",
                    "sinfoni.distortion",
                    8);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-mag_factor");
    cpl_parameterlist_append(list, p);

    /* Fits Slits */
    /* indicator if the fit of the slit edge positions is carried through or not */
    p = cpl_parameter_new_value("sinfoni.distortion.slit_pos_indicator",
                    CPL_TYPE_BOOL,
                    "Slit Position Indicator: "
                    "indicates if the fits of the slitlet "
                    "edge positions should be carried "
                    "through or not",
                    "sinfoni.distortion",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-slit_pos_ind");
    cpl_parameterlist_append(list, p);

    /* indicator if the fit model function is a Boltzmann function or not */
    p = cpl_parameter_new_value("sinfoni.distortion.fit_boltz_indicator",
                    CPL_TYPE_BOOL ,
                    "Fit Boltzmann Indicator: "
                    "indicates if the fits of the slitlet edge "
                    "positions is carried trough by using a "
                    "Boltzmann function as model function",
                    "sinfoni.distortion",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-fit_boltz_ind");
    cpl_parameterlist_append(list, p);

    /* indicator if the fit model function is a simple edge function or not */
    p = cpl_parameter_new_value("sinfoni.distortion.fit_edge_indicator",
                    CPL_TYPE_BOOL,
                    "Fit Edge Indicator: "
                    "indicates if the fits of the slitlet edge "
                    "positions is carried through by using a "
                    "simple edge function as model function",
                    "sinfoni.distortion",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-fit_edge_ind");
    cpl_parameterlist_append(list, p);

    /* indicator if the fit guess position are user
    given or calculated automatically */

    p = cpl_parameter_new_value("sinfoni.distortion.estimate_indicator",
                    CPL_TYPE_BOOL,
                    "Estimate Indicator: "
                    "indicates if the fits of the slitlet edge "
                    "positions is carried through by using a list "
                    "of estimated guess positions in a file (TRUE)"
                    "or if the initial positions are calculated "
                    "automatically (FALSE). The estimation case "
                    "is more stable",
                    "sinfoni.distortion",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-estimate_ind");
    cpl_parameterlist_append(list, p);

    /* pixel length of the row box within which
   the fit of the slitlet positions is carried out*/

    p = cpl_parameter_new_value("sinfoni.distortion.box_length",
                    CPL_TYPE_INT,
                    "Box Length: "
                    "pixel length of the row box within "
                    "which the fit is carried out",
                    "sinfoni.distortion",
                    32);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-box_len");
    cpl_parameterlist_append(list, p);

    /* float box half width in spectral direction */
    p = cpl_parameter_new_value("sinfoni.distortion.y_box",
                    CPL_TYPE_DOUBLE,
                    "Y Box: half width of a small box in "
                    "spectral direction within which the "
                    "maximal intensity pixel is searched",
                    "sinfoni.distortion",
                    5.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-y_box");
    cpl_parameterlist_append(list, p);

    /* maximal tolerable difference to the expected slitlet positions */
    p = cpl_parameter_new_value("sinfoni.distortion.diff_tol",
                    CPL_TYPE_DOUBLE,
                    "Difference Tolearance: "
                    "maximal tolerable difference of the "
                    "resulting fit positions of the slitlet "
                    "edges with respect to the expected positions",
                    "sinfoni.distortion",
                    2.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-diff_toll");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.qc_thresh_min",
                    CPL_TYPE_INT,
                    "qc_thresh_min",
                    "sinfoni.distortion",
                    0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-qc_thresh_min");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.qc_thresh_max",
                    CPL_TYPE_INT,
                    "qc_thresh_max",
                    "sinfoni.distortion",
                    49000);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"dist-qc_thresh_max");
    cpl_parameterlist_append(list, p);






    /* NORD SOUTH TEST */



    /* Clean Mean */
    p = cpl_parameter_new_range("sinfoni.distortion.lower_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection: "
                    "percentage of rejected low intensity pixels "
                    "before averaging",
                    "sinfoni.distortion",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-lo_rejection");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_range("sinfoni.distortion.higher_rejection",
                    CPL_TYPE_DOUBLE,
                    "higher rejection: "
                    "percentage of rejected high intensity pixels "
                    "before averaging",
                    "sinfoni.distortion",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-hi_rejection");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.mask_ind",
                    CPL_TYPE_BOOL,
                    "Mask Index: "
                    "indicator if a bad pixel mask is applied or not",
                    "sinfoni.distortion",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-mask_ind");
    cpl_parameterlist_append(list, p);


    /* Gauss Convolution */
    p = cpl_parameter_new_value("sinfoni.distortion.gauss_ind",
                    CPL_TYPE_BOOL,
                    "Gauss Index: ",
                    "sinfoni.distortion",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-gauss_ind");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.distortion.kernel_half_width",
                    CPL_TYPE_INT,
                    "Kernel Half Width "
                    "kernel half width of the Gaussian "
                    "response function",
                    "sinfoni.distortion",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-khw");
    cpl_parameterlist_append(list, p);



    /* North South Test */



    p = cpl_parameter_new_value("sinfoni.distortion.ns_half_width",
                    CPL_TYPE_INT,
                    "Half Width",
                    "sinfoni.distortion",
                    4);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-hw");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_value("sinfoni.distortion.ns_fwhm",
                    CPL_TYPE_DOUBLE,
                    "FWHM",
                    "sinfoni.distortion",
                    2.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-fwhm");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.min_diff",
                    CPL_TYPE_DOUBLE,
                    "Minimum of Difference",
                    "sinfoni.distortion",
                    1.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-min_diff");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.dev_tol",
                    CPL_TYPE_DOUBLE,
                    "Dev Tol",
                    "sinfoni.distortion",
                    20.);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"ns-dev_tol");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.arcs_thresh_factor",
                    CPL_TYPE_DOUBLE,
                    "arcs threshold factor. "
                    "median_value(image)+ kappa*sigma is the "
                    "minimum intensity threshold of accepted image"
                    "pixels",
                    "sinfoni.distortion",
                    0.33333);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"arcs_thresh_factor");
    cpl_parameterlist_append(list, p);



    p = cpl_parameter_new_value("sinfoni.distortion.arcs_min_arclen_factor",
                    CPL_TYPE_DOUBLE,
                    "factor which sets minimum arc length (1.0-2)",
                    "sinfoni.distortion",
                    1.19);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"arcs_min_arclen_factor");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.arcs_window_size",
                    CPL_TYPE_INT,
                    "Size of window for low pass fileter used in"
                    "an horizzontal low pass filter to remove "
                    "unwanted arcs (5-64)",
                    "sinfoni.distortion",
                    14);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"arcs_window_size");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.distortion.smooth_rad",
                    CPL_TYPE_INT,
                    "Size of smoothing factor (1-11) used to "
                    "prevent for possible intensity drops from "
                    "detector electronics on fibre illuminated "
                    "slitlets (1-11)",
                    "sinfoni.distortion",
                    3);


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"smooth_rad");
    cpl_parameterlist_append(list, p);

}
/**@}*/
