/*
 * This file is part of the ESO SINFO Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*
 * $Author: kmirny $
 * $Date: 2010-09-30 14:00:03 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 *
 */
#ifndef SINFO_UTILS_H
#define SINFO_UTILS_H


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <math.h>
#include <sinfo_msg.h>
#include <sinfo_error.h>
#include <cpl.h>
/*
 * The purpose of this target is to
 * decrease the amount of messages
 * printed at the debug level.
 *
 * If set to non-zero, even more messages
 * are printed at the debug level
 * (sometimes 50 - 100 MB)
 *
 */
#ifndef WANT_BIG_LOGFILE
#define WANT_BIG_LOGFILE 0
#endif

/*
 * Set to 1 to show timing
 * information on msg-level = info
 */
#ifndef WANT_TIME_MEASURE
#define WANT_TIME_MEASURE 0
#endif


#if WANT_TIME_MEASURE
#define SINFO_TIME_START(what) sinfo_msg("Timing (%s, l%d) %s start", \
                                       __FILE__, __LINE__, what)
#define SINFO_TIME_END         sinfo_msg("Timing (%s, l%d) end", \
                                       __FILE__, __LINE__)
#else
#define SINFO_TIME_START(what) sinfo_msg_debug("Timing (%s, l%d) %s start", \
                                             __FILE__, __LINE__, what)
#define SINFO_TIME_END         sinfo_msg_debug("Timing (%s, l%d) end", \
                                             __FILE__, __LINE__)
#endif




#ifndef stringify
#ifndef make_str
#define stringify(X) #X
#define make_str(X) stringify(X)
#endif
#endif

#define TWOSQRT2LN2 2.35482004503095

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define COS_DEG(x) cos(((x)/180)*M_PI)
#define SIN_DEG(x) sin(((x)/180)*M_PI)
#define ACOS_DEG(x) (acos(x)*180/M_PI)



long sinfo_round_double(double x);
int
sinfo_parameter_get_default_flag ( const cpl_parameter* p );

#endif
