/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name     :    sinfo_local_types.h
   Author         :    Nicolas Devillard
   Created on    :    Nov 27, 1995
   Description    :    all shared local types for eclipse

 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
    PUBLIC NOTICE AS REQUIRED BY LAW: Any use of this product, in any
    manner whatsoever, will increase the amount of disorder in the
    universe. Although no liability is implied herein, the consumer is
    warned that this process will ultimately lead to the heat death of the
    universe.
 ---------------------------------------------------------------------------*/
/*
    $Id: sinfo_local_types.h,v 1.4 2007-06-06 07:10:45 amodigli Exp $
    $Author: amodigli $
    $Date: 2007-06-06 07:10:45 $
    $Revision: 1.4 $
 */

#ifndef SINFO_LOCAL_TYPES_H
#define SINFO_LOCAL_TYPES_H
/*----------------------------------------------------------------------------
                                   Includes
 *--------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
                                   Defines
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
 * These types are defined for portability issues
 * On DEC-Alpha stations, long is 64 bits, but int is 32
 * We have to redefine all int values accordingly to ensure
 * portability!
 */

#ifdef _DEC_ALPHA
typedef unsigned int ulong32 ;
typedef int long32 ;
#else
typedef unsigned long ulong32 ;
typedef long long32 ;
#endif

typedef unsigned short ushort16 ;
typedef short short16 ;

typedef unsigned char uchar8 ;
typedef char char8 ;
 
typedef unsigned char BYTE ;

/* defined in limits.h, redefined here for portability  */

#define LONG32_MIN  (long32)(-2147483647-1) 
#define LONG32_MAX  (long32)(2147483647)
#define ULONG32_MAX (ulong32)(4294967295)

#define SHRT16_MIN  (short16)(-32768)
#define SHRT16_MAX  (short16)(32767)
#define USHRT16_MAX (ushort16)(65535)

typedef struct _DOUBLE_COMPLEX_ {
    double x, y ;
} dcomplex ;

/*--------------------------------------------------------------------------*/
/* pixelvalue is the internal Pixel representation  */

#ifdef DOUBLEPIX
typedef double    pixelvalue ;
#else
typedef float    pixelvalue ;
#endif



/*
 * dpoint: useful to store point coordinates in double precision
 */

typedef struct _DPOINT_ {
    double x ;
    double y ;
} dpoint ;




/*--------------------------------------------------------------------------*/
/* Pixel map */


typedef uchar8 binpix ;

typedef struct _PIXEL_MAP_
{
    int            lx, ly ;
    int            nbpix ;
    int            ngoodpix ;
    binpix    *    data ;
} pixel_map ;


#define NullMap (pixel_map*)NULL


#endif 
