/* $Id: sinfo_skycor.h,v 1.13 2007-03-05 07:21:54 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2007-03-05 07:21:54 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef SINFO_SKYCOR_H
#define SINFO_SKYCOR_H
#include <cpl.h>

#define SINFO_MASK_WAVE_MIN 1.4
#define SINFO_MASK_WAVE_MAX 2.5
#define SINFO_MIN_FRAC 0.8
#define SINFO_LINE_HALF_WIDTH 4.0
#define SINFO_SKY_BKG_FILTER_WIDTH 12



#define HISTO_NBINS 100
#define HISTO_MIN_SIZE 10
#define HISTO_Y_CUT 10          /* 50 is a better value this 
                                   affects histo right marging*/
#define HISTO_X_LEFT_CUT  1.0   /* 0.2 is better */
#define HISTO_X_RIGHT_CUT  0.5  /* 1.0 is better */
#define HISTO_DIST_TEMPC_MIN_FCT 5.   /* 5.0 */
#define HISTO_DIST_TEMPC_MAX_FCT 0.25 /* 0.25 */

struct sinfo_skycor_qc_ {
  int   th_fit;
};
typedef struct sinfo_skycor_qc_ sinfo_skycor_qc; 

sinfo_skycor_qc* sinfo_skycor_qc_new(void);
void sinfo_skycor_qc_delete(sinfo_skycor_qc** s);


int 
sinfo_skycor(cpl_parameterlist * config, 
             cpl_frame* obj_frm, 
             cpl_frame* sky_frm,
             sinfo_skycor_qc* sqc,
             cpl_imagelist** obj_cor,
             cpl_table** int_obj);



int
sinfo_histogram(const cpl_table* data,
                const int nbins, 
                const double min, 
                const double max,
                cpl_table** histo);
int
sinfo_table_get_index_of_val(cpl_table* t,
                             const char* name,
                             double val,
                             cpl_type type);

int
sinfo_table_get_index_of_max(cpl_table* t,
                             const char* name,
                             cpl_type type);


double
sinfo_table_column_interpolate(const cpl_table* t,
                               const char* name,
                               const double x);





cpl_table* 
sinfo_where_tab_min_max(cpl_table* t, 
                        const char* col, 
                        cpl_table_select_operator op1,  
                        const double v1, 
                        cpl_table_select_operator op2, 
                        const double v2);

#endif
