#ifndef SINFO_NEW_PREPARE_STACKED_FRAMES_H
#define SINFO_NEW_PREPARE_STACKED_FRAMES_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_prepare_stacked_frames.h,v 1.7 2007-09-21 14:48:10 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  17/09/03  created
*/

/************************************************************************
 * preapare_stacked_frames.h

 sinfo_prepare_stacked_frames

  this handles stacks of input frames, that means it takes a clean mean,
  subtracts the off- from the on-frames, flatfields, corrects for static bad 
  pixels, corrects for a linear tilt of the spectra if necessary, and finally, 
  interleaves dithered exposures or convolves a single exposure with a 
  Gaussian, respectively.

 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h>   
#include "sinfo_globals.h"
#include "sinfo_msg.h"


/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Function     :       sinfo_new_prepare_stacked_frames()
   In           :       ini_file: file name of according .ini file
   Out          :       integer (0 if it worked, -1 if it doesn't) 
   Job          :

  this handles stacks of input frames, that means it takes a clean mean,
  subtracts the off- from the on-frames, flatfields, corrects for static bad 
  pixels, corrects for a linear tilt of the spectra if necessary, and finally, 
  interleaves dithered exposures or convolves a single exposure with a 
  Gaussian, respectively.

 ---------------------------------------------------------------------------*/
int 
sinfo_new_prepare_stacked_frames (const char* plugin_id,
                                  cpl_parameterlist* config, 
                                  cpl_frameset* sof, 
                                  cpl_frameset* ref_set, 
                                  const char* procatg,
                                  const int frm_ind, 
                                  fake* fk) ;

#endif 

/*--------------------------------------------------------------------------*/
