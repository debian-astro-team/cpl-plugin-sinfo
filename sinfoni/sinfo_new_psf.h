#ifndef SINFO_NEW_PSF_H
#define SINFO_NEW_PSF_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_psf.h,v 1.12 2008-02-12 16:33:50 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  17/09/03  created
*/

/************************************************************************
 * sinfo_new_psf.h

 sinfo_psf.py does the image reconstruction of a set of sky-subtracted, 
 flatfielded, 
 bad pixel corrected and slope of the spectra aligned exposures of a bright 
 star with continuum spectrum. The resulting image can be used to determine 
 the PSF
 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <cpl.h> 
#include <irplib_strehl.h>
#include "sinfo_msg.h"

/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Function     :       sinfo_new_psf()
   In           :       ini_file: file name of according .ini file
   Out          :       integer (0 if it worked, -1 if it doesn't) 
   Job          :

 sinfo_psf.py does the image reconstruction of a set of sky-subtracted, 
 flatfielded, bad pixel corrected and slope of the spectra aligned exposures 
 of a bright star with continuum spectrum. The resulting image can be used 
 to determine the PSF

 ---------------------------------------------------------------------------*/
int 
sinfo_new_psf(const char* plugin_id,
              cpl_parameterlist* config,
              cpl_frameset* sof,
              cpl_frameset* ref_set) ;


int sinfo_strehl_compute_two(
			  const cpl_image *   im1,
			  const cpl_image *   im2,
			  double              m1,
			  double              m2,
			  double              lam,
			  double              pscale1,
			  double              pscale2,
			  double              exptime1,
			  double              exptime2,
			  int                 xpos1,
			  int                 ypos1,
			  int                 xpos2,
			  int                 ypos2,
			  double              r1,
			  double              r2,
			  double              r3,
			  double          *   strehl,
			  double          *   strehl_err,
			  double          *   star_bg,
			  double          *   star_peak,
			  double          *   star_flux,
			  double          *   psf_peak,
			  double          *   psf_flux,
			  double          *   bg_noise);

cpl_error_code 
sinfo_strehl_compute_one(const cpl_image *   im,
			 double              m1,
			 double              m2,
			 double              lam,
			 double              dlam,
			 double              pscale,
			 int                 xpos,
			 int                 ypos,
			 double              r1,
			 double              r2,
			 double              r3,
                         int                 noise_box_sz,
			 double          *   strehl,
			 double          *   strehl_err,
			 double          *   star_bg,
			 double          *   star_peak,
			 double          *   star_flux,
			 double          *   psf_peak,
			 double          *   psf_flux,
			 double          *   bg_noise);




cpl_error_code 
sinfo_get_bkg_4corners(const cpl_image *img,
		       const int bkg_sx,
                       const int bkg_sy,
                       double* bkg,
                       double* std);

cpl_error_code
sinfo_compute_psf(const double dia, 
		  const double occ,
		  const double lambda,
		  const double psize,
		  const double cx,
		  const double cy,
		  const double anamorph,
		  double* psf_peak);
cpl_error_code
sinfo_get_flux_above_bkg(const cpl_image* img, 
                         const float kappa, 
                         const float std, 
                         double* f);


double
sinfo_scale_flux(const double p1, 
                 const double p2, 
                 const double t1, 
                 const double t2);

#endif 

/*--------------------------------------------------------------------------*/
