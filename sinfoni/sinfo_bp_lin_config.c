/* $Id: sinfo_bp_lin_config.c,v 1.5 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/****************************************************************
 *           Bad pixel search  (Linear method)                  *
 ****************************************************************/

#include "sinfo_bp_lin_config.h"  

/**@{*/
/**
 * @addtogroup sinfo_bad_pix_search Bad Pixel Search
 *
 * TBD
 */

/**
 * @brief
 *   Adds parameters for the spectrum extraction
 *
 * @param list Parameter list to which parameters are added.
 *
 * @return Nothing.
 *
 * TBD
 */

/* Bad pixel parameters */

void
sinfo_bp_lin_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }



    /* order of the fit polynomial = number of coefficents - 1 */
    p = cpl_parameter_new_value("sinfoni.bp_lin.order",
                    CPL_TYPE_INT,
                    "Order: "
                    "order of the fit polynomial = "
                    "number of coefficents - 1",
                    "sinfoni.bp_lin",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_lin-order");
    cpl_parameterlist_append(list, p);



    p = cpl_parameter_new_value("sinfoni.bp_lin.thresh_sigma_factor",
                    CPL_TYPE_DOUBLE,
                    "Threshold Sigma Factor: "
                    "threshold factor of the clean standard "
                    "deviation. If the deviations of the linear "
                    "polynomial coefficients exceed this threshold "
                    "the corresponding pixels are declared as bad ",
                    "sinfoni.bp_noise",
                    10.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_lin-thresh_sigma_fct");
    cpl_parameterlist_append(list, p);



    /* if a non-linear coefficient exceeds this value the
     corresponding pixel is declared as bad
     */
    p = cpl_parameter_new_value("sinfoni.bp_lin.nlin_threshold",
                    CPL_TYPE_DOUBLE,
                    "Non Linear Threshold",
                    "sinfoni.bp_lin",
                    0.5);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_lin-nlin_threshold");
    cpl_parameterlist_append(list, p);


    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.bp_lin.low_rejection",
                    CPL_TYPE_DOUBLE,
                    "low_rejection: "
                    "percentage of rejected low intensity "
                    "pixels before averaging",
                    "sinfoni.bp_lin",
                    10.,
                    0.,
                    100.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_lin-lo_rej");
    cpl_parameterlist_append(list, p);

    /* float
     threshold used in the clean mean percentage of rejection used to reject 
     low and high frame */
    /* percentage of extreme pixel value to reject when calculating the mean
    and stdev */
    p = cpl_parameter_new_range("sinfoni.bp_lin.high_rejection",
                    CPL_TYPE_DOUBLE,
                    "high_rejection: "
                    "percentage of rejected high intensity "
                    "pixels before averaging",
                    "sinfoni.bp_lin",
                    10.,0.,100.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"bp_lin-hi_rej");
    cpl_parameterlist_append(list, p);


    return;

}
/**@}*/
