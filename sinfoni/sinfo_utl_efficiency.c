/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 10:17:31 $
 * $Revision: 1.17 $
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string.h>
#include <math.h>
#include <cpl.h>
/* irplib */
#include <irplib_utils.h>
/* hdrl */
#include <../hdrl/hdrl.h>


#include <sinfo_pfits.h>
#include <sinfo_msg.h>
#include <sinfo_dfs.h>
#include <sinfo_error.h>
#include <sinfo_utils_wrappers.h>
#include <sinfo_utilities_scired.h>
#include "sinfo_utl_efficiency.h"
#include <sinfo_star_index.h>
#include "sinfo_pro_save.h"
#include "sinfo_pro_types.h"
#include "sinfo_hidden.h"
//static const char COL_NAME_WAVELENGTH[] = "WAVELENGTH";
//static const char COL_NAME_WAVELENGTH_C[] = "WAVELENGTH";
//static const char COL_NAME_LAMBDA[] 	= "LAMBDA";
//static const char COL_NAME_LA_SILLA[]	= "LA_SILLA";
static const char COL_NAME_REF[]		= "REF";
static const char COL_NAME_COR[]		= "COR";
static const char COL_NAME_SRC_COR[]	= "SRC_COR";
//static const char COL_NAME_COUNTS_BKG[]	= "INT_OBJ";//"counts_tot";
//static const char COL_NAME_FLUX[]		= "FLUX";
static const char COL_NAME_EPHOT[]		= "EPHOT";
static const char COL_NAME_EXT[]		= "EXT";
static const char COL_NAME_SRC_EFF[]	= "EFF";
//static const char PAR_NAME_DIT[]		= "ESO DET DIT";

static char FRM_RAW_IMA_SLIT[]	= PRO_STD_STAR_SPECTRA;
static char FRM_FLUX_STD_TAB[]	= FLUX_STD_TABLE;
static char FRM_FLUX_STD_CAT[]	= FLUX_STD_CATALOG;
static char FRM_EXTCOEFF_TAB[]	= EXTCOEFF_TABLE;


const hdrl_spectrum1D_wave_scale
global_scale = hdrl_spectrum1D_wave_scale_linear;


static int 
sinfo_column_to_double(cpl_table* ptable, const char* column);

static double 
sinfo_table_interpolate(cpl_table* tbl,
		  double wav,
		  const char* colx,
		  const char* coly);
static double* 
sinfo_create_column_double(cpl_table* tbl, const char* col_name, int nrow);



cpl_error_code
sinfo_get_std_obs_values(cpl_propertylist* plist,
                       double* exptime,
                       double* airmass,
                       double* dRA,
                       double* dDEC);

/*--------------------------------------------------------------------------*/

/**@{*/
/*---------------------------------------------------------------------------*/
/**
  @brief    load reference table

  @param    frames    input frames list
  @param    dRA       Right Ascension
  @param    dDEC      Declination
  @param    EPSILON   tolerance to find ref spectra on catalog on (ra,dec)
  @param    ptable    pointer to new table
  @return   Interpolated data points
 */
/*---------------------------------------------------------------------------*/

void 
sinfo_load_ref_table(cpl_frameset* frames, 
                   double dRA, 
                   double dDEC, 
                   double EPSILON, 
                   cpl_table** pptable)
{
  const char* name = NULL;
  cpl_frame* frm_ref = NULL;

  /* REF STD frame */
  frm_ref=cpl_frameset_find(frames,FRM_FLUX_STD_TAB);
  if (!frm_ref)
    {
      sinfo_msg("REF frame is not found, trying to get REF from the catalog");
     /* REF STD catalog frame */
      // get from catalog
      check_nomsg(frm_ref=cpl_frameset_find(frames,FRM_FLUX_STD_CAT));
      if (frm_ref)
	{
	  check_nomsg(name=cpl_frame_get_filename(frm_ref));
	  if (name)
	    {
	      star_index* pstarindex = star_index_load(name);
	      if (pstarindex)
		{
		  const char* star_name = 0;
		  sinfo_msg("The catalog is loaded, looking for star in RA[%f] DEC[%f] tolerance[%f]", dRA, dDEC, EPSILON);
		  *pptable = star_index_get(pstarindex, dRA, dDEC, EPSILON, EPSILON, &star_name);
		  if (*pptable && star_name)
		    {
		      sinfo_msg("REF table is found in the catalog, star name is [%s]", star_name);
		    }
		  else
		    {
		      sinfo_msg("ERROR - REF table could not be found in the catalog");
		    }
		}
	      else
		{
		  sinfo_msg("ERROR - could not load the catalog");
		}
	    }
	}
    }
  else
    {
      sinfo_msg("REF frame is found");
      check_nomsg(name=cpl_frame_get_filename(frm_ref));
      check_nomsg(*pptable=cpl_table_load(name,1,0));
    }
  return;
 cleanup:
  return;
}




/*---------------------------------------------------------------------------*/
/**
  @brief    load reference table

  @param    cat        input frame catalog
  @param    dRA        Right Ascension
  @param    dDEC       Declination
  @param    EPSILON    tolerance to find ref spectra on catalog on (ra,dec)
  @param    ptable     pointer to new table
  @return   Interpolated data points
 */
/*---------------------------------------------------------------------------*/

static void 
sinfo_parse_catalog_std_stars(cpl_frame* cat, 
			    double dRA, 
			    double dDEC, 
			    double EPSILON, 
			    cpl_table** pptable)
{


  if (cat != NULL) {

	  const char* name = cpl_frame_get_filename(cat);

      if (name) {
          star_index* pstarindex = star_index_load(name);
          if (pstarindex) {
              const char* star_name = 0;
              sinfo_msg("The catalog is loaded, looking for star in RA[%f] DEC[%f] tolerance[%f]", dRA, dDEC, EPSILON);
              *pptable = star_index_get(pstarindex, dRA, dDEC, EPSILON, EPSILON, &star_name);
              if (*pptable && star_name) {
                  sinfo_msg("REF table is found in the catalog, star name is [%s]", star_name);
              }
              else {
                  sinfo_msg("ERROR - REF table could not be found in the catalog");
              }
          }
          else {
              sinfo_msg("ERROR - could not load the catalog");
          }
          star_index_delete(pstarindex);

      }
  }


  return;
}




/*---------------------------------------------------------------------------*/
/**
  @brief    Interpolate efficiency data points
  @param    wav       wavelength
     number of table raws
  @param    pw        pointer to wave array
  @param    pe        pointer to efficiency array
  @return   Interpolated data points
 */
/*---------------------------------------------------------------------------*/


double
sinfo_data_interpolate(
		     double wav,
		     int nrow,
		     double* pw,
		     double* pe
		     )
{
  double y = 0;
  double w1=pw[0];
  double w2=pw[nrow-1];
  double y1_=pe[0]; /*was changed from y1 to y1_ due a warning from compiler - shadowed variable*/
  double y2=pe[nrow-1];
  if(wav < pw[0])
    {
      w1=pw[0];
      w2=pw[1];
      y1_=pe[0];
      y2=pe[1];
    }
  else if (wav > pw[nrow - 1])
    {
      w1=pw[nrow - 2];
      w2=pw[nrow - 1];
      y1_=pe[nrow - 2];
      y2=pe[nrow - 1];
    }
  else
    {
      int l = 0;
      int h = nrow - 1;
      int curr_row = 0;
      curr_row = (h-l) / 2;
      while( h-l >1 )
	{
	  if(wav < pw[curr_row])
	    {
	      h = curr_row;
	    }
	  else
	    {
	      l = curr_row;
	    }
	  curr_row = (h-l) / 2 + l;
	}
      w1=pw[curr_row];
      w2=pw[curr_row + 1];
      y1_=pe[curr_row];
      y2=pe[curr_row + 1];
    }
  y=y1_+(y2-y1_)/(w2-w1)*(wav-w1);
  return y;
}
static double
sinfo_table_interpolate(cpl_table* tbl,
		  double wav,
		  const char* colx,
		  const char* coly)
{

  double y=0;
  double* pe=NULL;
  double* pw=NULL;
  int nrow=0;

  check_nomsg(pw=cpl_table_get_data_double(tbl,colx));
  check_nomsg(pe=cpl_table_get_data_double(tbl,coly));
  check_nomsg(nrow=cpl_table_get_nrow(tbl));

  y = sinfo_data_interpolate(wav, nrow, pw, pe);
 cleanup:
  return y;

}

static double* 
sinfo_create_column_double(cpl_table* tbl, const char* col_name, int nrow)
{
  double* retval = 0;
  check_nomsg(cpl_table_new_column(tbl, col_name, CPL_TYPE_DOUBLE));
  check_nomsg(cpl_table_fill_column_window_double(tbl, col_name, 0, nrow, -1));
  check_nomsg(retval = cpl_table_get_data_double(tbl,col_name));
  return retval;
 cleanup:
  return retval;
}


/*---------------------------------------------------------------------------*/
/**
  @brief    get STD star observation exptime, airmass, RA, DEC
  @param    plist    (input) property list
  @param    exptime  (output) exposure time
  @param    airmass  (output) airmass (average of start and end)
  @param    dRA      (output) Right Ascension
  @param    dDEC     (output) Declination
  @return   CPL error code
 */
/*---------------------------------------------------------------------------*/

cpl_error_code
sinfo_get_std_obs_values(cpl_propertylist* plist,
                       double* exptime,
                       double* airmass,
                       double* dRA,
                       double* dDEC)
{
   double airmass_start=0;
   double airmass_end=0;

   check_nomsg(*exptime=sinfo_pfits_get_exp_time(plist));
   airmass_start=sinfo_pfits_get_airmass_start(plist);
   airmass_end=sinfo_pfits_get_airmass_end(plist);
   *dRA=sinfo_pfits_get_ra(plist);
   *dDEC=sinfo_pfits_get_dec(plist);
   *airmass=0.5*(airmass_start+airmass_end);

 cleanup:
   return cpl_error_get_code();

}

/*---------------------------------------------------------------------------*/
/**
  @brief Compute efficiency
  @param frames     input frameset
  @param prod_name  the product name
  @param dGain      detector's gain value
  @param dEpsilon   tolerance to find ref spectra on catalog on (ra,dec)
  @param airprim    airmass
  @param col_name_atm_wave atmospheric extinction table wave column name
  @param col_name_atm_abs  atmospheric extinction table absorption column name
  @param col_name_ref_wave reference flux std table wave column name
  @param col_name_ref_flux reference flux std table flux column name
  @param col_name_obj_wave observed std table wave column name
  @param col_name_obj_flux observed std table flux column name

  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
/* TODO: not used */
cpl_table*
sinfo_utl_efficiency(
		     cpl_frameset * frames,
		     double dGain,
		     double dEpsilon,
		     double aimprim,
                     const char* col_name_atm_wave,
		     const char* col_name_atm_abs,
                     const char* col_name_ref_wave,
                     const char* col_name_ref_flux,
                     const char* col_name_ref_bin,
                     const char* col_name_obj_wave,
                     const char* col_name_obj_flux
		     )
{
  cpl_frame* frm_sci = NULL;
  cpl_frame* frm_atmext = NULL;
  cpl_table* tbl_obj_spectrum = NULL; // input table with spectrum
  cpl_table* tbl_atmext = NULL;
  double exptime = 600;

  cpl_propertylist* plist = NULL;
  cpl_table* tbl_ref = NULL;
  cpl_table* tbl_result=NULL;

  const char* name=NULL;
  double dRA = 0;
  double dDEC = 0;

  double airmass=0;
  const double mk2AA=1E4; /* mkm/AA */


  // read all input data and call the internal function
  // read the input tables
  // 1. observation
  check_nomsg(frm_sci=cpl_frameset_find(frames, FRM_RAW_IMA_SLIT));
  check_nomsg(name=cpl_frame_get_filename(frm_sci));
  sinfo_msg("name=%s",name);
  check_nomsg(tbl_obj_spectrum=cpl_table_load(name,1,0));
  check_nomsg(plist=cpl_propertylist_load(name,0));

  sinfo_get_std_obs_values(plist,&exptime,&airmass,&dRA,&dDEC);

  // 2. reference table
  sinfo_load_ref_table(frames, dRA, dDEC, dEpsilon, &tbl_ref);
  if (tbl_ref)
    {
      // 3. atmext
      check_nomsg(frm_atmext=cpl_frameset_find(frames,FRM_EXTCOEFF_TAB));
      check_nomsg(name=cpl_frame_get_filename(frm_atmext));
      check_nomsg(tbl_atmext=cpl_table_load(name,1,0));

      tbl_result = sinfo_utl_efficiency_internal(
						 tbl_obj_spectrum,
						 tbl_atmext,
						 tbl_ref,
						 exptime,
						 airmass,
						 aimprim,
						 dGain,
                                                 1,
                                                 mk2AA,//valid only for SINFONI
                                                 col_name_atm_wave,
                                                 col_name_atm_abs,
                                                 col_name_ref_wave,
                                                 col_name_ref_flux,
                                                 col_name_ref_bin,
                                                 col_name_obj_wave,
                                                 col_name_obj_flux
         );
     }

 cleanup:
  sinfo_free_propertylist(&plist);
  sinfo_free_table(&tbl_atmext);
  sinfo_free_table(&tbl_obj_spectrum);
  sinfo_free_table(&tbl_ref);
  return tbl_result;
}
static int 
sinfo_column_to_double(cpl_table* ptable, const char* column)
{
  const char* TEMP = "_temp_";
  check_nomsg(cpl_table_duplicate_column(ptable, TEMP, ptable, column));
  check_nomsg(cpl_table_erase_column(ptable, column));
  check_nomsg(cpl_table_cast_column(ptable, TEMP, column, CPL_TYPE_DOUBLE));
  check_nomsg(cpl_table_erase_column(ptable, TEMP ));
  return 0;
 cleanup:
  sinfo_msg(" error column to double [%s]", column);
  return -1;
}


/*---------------------------------------------------------------------------*/
/**
  @brief Compute efficiency
  @param tbl_obj_spectrum     input object spectrum
  @param tbl_atmext           input atmospheric extinction table
  @param tbl_ref              input reference flux STD table
  @param exptime              input exposure time
  @param airprim              input airmass
  @param gain                 input gain
  @param src2ref_wave_sampling input cnversion factor to pass from src 2 ref units
  @param airprim              input airmass

  @param col_name_atm_wave atmospheric extinction table wave column name
  @param col_name_atm_abs  atmospheric extinction table absorption column name
  @param col_name_ref_wave reference flux std table wave column name
  @param col_name_ref_flux reference flux std table flux column name
  @param col_name_obj_wave observed std table wave column name
  @param col_name_obj_flux observed std table flux column name

  @return   0 if everything is ok
 */
/*---------------------------------------------------------------------------*/
cpl_table* 
sinfo_utl_efficiency_internal(
			      cpl_table* tbl_obj_spectrum,
			      cpl_table* tbl_atmext,
			      cpl_table* tbl_ref,
			      double exptime,
			      double airmass,
			      double aimprim,
			      double gain,
			      int    biny,
                              double src2ref_wave_sampling,
                              const char* col_name_atm_wave,
                              const char* col_name_atm_abs,
                              const char* col_name_ref_wave,
                              const char* col_name_ref_flux,
                              const char* col_name_ref_bin,
                              const char* col_name_obj_wave,
                              const char* col_name_obj_flux
			      )
{

  const double TEL_AREA		= 51.2e4; /* collecting area in cm2 */
  double cdelta1 = 0;
  int i = 0;
  cpl_table* tbl_sel = NULL;

  /* structure of the input table
   * col			type	description
   * wavelength	double
   * flux			double
   * */
  cpl_table* tbl_result = NULL; // output table

  /*
   * structure of the output table
   * col			type	description
   * wavelength	double
   * EFF			double	efficiency in range 0-1
   * */

  double* pref = NULL;
  double* pext = NULL;
  double* pcor = NULL;
  double* peph = NULL;

  double* pw = NULL; // wavelength of the input table
  /* double* pf = NULL; // flux of the input table */
  int nrow = 0;
  double ref_bin_size=0;

  nrow = cpl_table_get_nrow(tbl_obj_spectrum);
  sinfo_msg("Starting efficiency calculation: exptime[%f] airmass[%f] nrow[%d]", 
	  exptime, airmass, nrow);

  //cpl_table_dump(tbl_obj_spectrum,0,3,stdout);
  //sinfo_msg("col wave=%s col_flux=%s",col_name_obj_wave,col_name_obj_flux);

  /* convert to double */
  sinfo_column_to_double(tbl_obj_spectrum,col_name_obj_wave);
  sinfo_column_to_double(tbl_obj_spectrum,col_name_obj_flux);

  check_nomsg(sinfo_column_to_double(tbl_atmext,col_name_atm_wave ));
  check_nomsg(sinfo_column_to_double(tbl_atmext,col_name_atm_abs ));
  check_nomsg(sinfo_column_to_double(tbl_ref,col_name_ref_wave ));
  check_nomsg(sinfo_column_to_double(tbl_ref,col_name_ref_flux ));
  check_nomsg(sinfo_column_to_double(tbl_ref,col_name_ref_bin ));

  /* get bin size of ref STD star spectrum */
  ref_bin_size=cpl_table_get_double(tbl_ref,col_name_ref_bin,0,NULL);
  sinfo_msg("ref_bin_size[AA]=%g",ref_bin_size);
  /*
  sinfo_msg("Unit conversion factor src/ref STD spectrum=%g",
          src2ref_wave_sampling);
  */

  /* convert obj spectrum wave unit to the same of the reference one */
  check_nomsg(cpl_table_multiply_scalar(tbl_obj_spectrum,col_name_obj_wave,
                                 src2ref_wave_sampling));

  check_nomsg(cpl_table_save(tbl_ref,NULL,NULL,"ref2.fits",CPL_IO_DEFAULT));
  check_nomsg(cpl_table_save(tbl_atmext,NULL,NULL,"atm2.fits",CPL_IO_DEFAULT));
  check_nomsg(cpl_table_save(tbl_obj_spectrum,NULL,NULL,"sci2.fits",CPL_IO_DEFAULT));
  sinfo_msg("object 2 src std %g",src2ref_wave_sampling);
  check_nomsg(pw=cpl_table_get_data_double(tbl_obj_spectrum,col_name_obj_wave));
  /* check_nomsg(pf=cpl_table_get_data_double(tbl_obj_spectrum,col_name_obj_flux)); */

  // prepare columns for the output data
  check_nomsg(tbl_result=cpl_table_new(nrow));
  check_nomsg(pref=sinfo_create_column_double(tbl_result, COL_NAME_REF, nrow));
  check_nomsg(pext=sinfo_create_column_double(tbl_result, COL_NAME_EXT, nrow));
  check_nomsg(pcor=sinfo_create_column_double(tbl_result, COL_NAME_COR, nrow));
  check_nomsg(peph=sinfo_create_column_double(tbl_result, COL_NAME_EPHOT, nrow));
  sinfo_msg("wave range: [%g,%g]",pw[0],pw[nrow-1]); 
  sinfo_msg("src_bin_size[AA]=%g",pw[1] - pw[0]);
  
  cdelta1 = (pw[1] - pw[0]) / src2ref_wave_sampling ; /* we want the delta in original units. As we rescaled to AA we need to correct for this */
  //sinfo_msg("nrow=%d cdelta1=%g",nrow,cdelta1);
  for (i = 0; i < nrow; i++)
    {
      check_nomsg(pext[i] = sinfo_table_interpolate(tbl_atmext, pw[i],col_name_atm_wave, col_name_atm_abs));
      check_nomsg(pref[i] = sinfo_table_interpolate(tbl_ref, pw[i], col_name_ref_wave,col_name_ref_flux));
      pcor[i] = pow(10,(0.4*pext[i] * (aimprim - airmass)));
      peph[i] = 1.e7*1.986e-19/(pw[i]*1e-4);
      /* ph energy: 1.986*10^-19(J.ph^-1)/lam(um) ==> 
         in as pw is expressed in Angstrom units we need to multiply by 10-4 
       */
      /*
      if(i< 2) {
         sinfo_msg("pw[i]=%g,pcor=%g peph=%g",pw[i],pcor[i],peph[i]);
      }
      */
    }


  /*
  sinfo_msg("atm: %s, %s ref: %s, %s obj: %s, %s",
          col_name_atm_wave,col_name_atm_abs,
          col_name_ref_wave,col_name_ref_flux,
          col_name_obj_wave,col_name_obj_flux);
  */
  check_nomsg(cpl_table_duplicate_column(tbl_result,COL_NAME_SRC_COR,
				   tbl_obj_spectrum, col_name_obj_flux));
  check_nomsg(cpl_table_duplicate_column(tbl_result,col_name_obj_wave,
				   tbl_obj_spectrum,col_name_obj_wave));
  /* correct for atmospheric extintion */
  check_nomsg(cpl_table_multiply_columns(tbl_result,COL_NAME_SRC_COR,COL_NAME_COR));

  /* correct object flux by binning: 
      divides by binsize in ref_wave_sampling (usually AA): 
     cdelt1[src_sampling]*src2ref_wave_sampling */
  cpl_table_divide_scalar(tbl_result, COL_NAME_SRC_COR, src2ref_wave_sampling);

  cpl_table_divide_scalar(tbl_result, COL_NAME_SRC_COR, cdelta1);
  /* correct for spatial bin size */

  cpl_table_divide_scalar(tbl_result,COL_NAME_SRC_COR,biny); 


  /*correct ref std star flux by binning: 
    divides by the bin size in ref_wave_sampling (usually AA) */
  check_nomsg(cpl_table_divide_scalar(tbl_result,COL_NAME_REF,ref_bin_size));

  check_nomsg(cpl_table_duplicate_column(tbl_result,COL_NAME_SRC_EFF,
				   tbl_result,COL_NAME_SRC_COR));


  /* correct for detector gain, exposure time, telescope area */
  check_nomsg(cpl_table_multiply_scalar(tbl_result,COL_NAME_SRC_EFF,
				  gain / (exptime * TEL_AREA)));

  /* correct for photon energy */
  check_nomsg(cpl_table_multiply_columns(tbl_result,COL_NAME_SRC_EFF,
				   COL_NAME_EPHOT));


  check_nomsg(cpl_table_divide_columns(tbl_result,COL_NAME_SRC_EFF,COL_NAME_REF));
  /* apply factor 1.e16 as reference catalog has fluxes in units of 1e-16 */
  check_nomsg(cpl_table_multiply_scalar(tbl_result,COL_NAME_SRC_EFF,1.e16));
  //check_nomsg(cpl_table_save(tbl_result,NULL,NULL,"pippo.fits", CPL_IO_DEFAULT));
  /* clean from outliers */
  cpl_table_and_selected_double(tbl_result,COL_NAME_SRC_EFF,
				CPL_GREATER_THAN,1.e-5);
  cpl_table_and_selected_double(tbl_result,COL_NAME_SRC_EFF,
				CPL_LESS_THAN,100.);
  tbl_sel=cpl_table_extract_selected(tbl_result);
  check_nomsg(cpl_table_save(tbl_result,NULL,NULL,"result9.fits",CPL_IO_DEFAULT));

 cleanup:
  sinfo_free_table(&tbl_result);
  return tbl_sel;
}

static hdrl_spectrum1D *
sinfo_sci_frame_to_hdrl_spectrum1D(cpl_frame* frm_sci)
{
	cpl_ensure(frm_sci != NULL, CPL_ERROR_NULL_INPUT, NULL);
	hdrl_spectrum1D * sci_s1d;
	const char* name_sci=NULL;
	name_sci=cpl_frame_get_filename(frm_sci);
	cpl_table* sci_tab=NULL;
	sci_tab=cpl_table_load(name_sci,1,0);
	cpl_ensure(sci_tab != NULL, CPL_ERROR_NULL_INPUT, NULL);


	sci_s1d=hdrl_spectrum1D_convert_from_table(sci_tab, "counts_bkg",
				"wavelength", NULL, NULL, global_scale);
    sinfo_free_table(&sci_tab);
	return sci_s1d;

}




static hdrl_spectrum1D *
sinfo_std_cat_frame_to_hdrl_spectrum1D(cpl_frame* frm_std_cat,const double dRA,
		const double dDEC)
{
	cpl_ensure(frm_std_cat != NULL, CPL_ERROR_NULL_INPUT, NULL);
	hdrl_spectrum1D * std_s1d;

	//const char* name = cpl_frame_get_filename(frm_std_cat);
	cpl_table* tbl_ref = NULL;

	double dEpsilon=0.1;

	sinfo_parse_catalog_std_stars(frm_std_cat,dRA,dDEC,dEpsilon,&tbl_ref);

	if(tbl_ref == NULL) {
		sinfo_msg_error("Provide std sar catalog frame");
		return NULL;
	}

	std_s1d=hdrl_spectrum1D_convert_from_table(tbl_ref, "FLUX",
			"LAMBDA", NULL, NULL, global_scale);
	sinfo_free_table(&tbl_ref);
	return std_s1d;
}


static hdrl_spectrum1D *
sinfo_atmext_frame_to_hdrl_spectrum1D(cpl_frame* frm_atmext)
{
	cpl_ensure(frm_atmext != NULL, CPL_ERROR_NULL_INPUT, NULL);
	hdrl_spectrum1D * ext_s1d;
	const char* name_atm = cpl_frame_get_filename(frm_atmext);

	cpl_table* tbl_atmext = cpl_table_load(name_atm,1,0);


	ext_s1d=hdrl_spectrum1D_convert_from_table(tbl_atmext, "EXTINCTION",
			"LAMBDA", NULL, NULL, global_scale);
    sinfo_free_table(&tbl_atmext);
	return ext_s1d;
}

static hdrl_parameter*
sinfo_response_parameters_calc_par(cpl_propertylist* plist)
{
	cpl_ensure(plist != NULL, CPL_ERROR_NULL_INPUT, NULL);
	hdrl_value Ap = {0};

	double airmass=0;
	double airmass_start=0;
	double airmass_end=0;
	airmass_start=sinfo_pfits_get_airmass_end(plist);
	airmass_end=sinfo_pfits_get_airmass_end(plist);
	airmass=0.5*(airmass_start+airmass_end);

	hdrl_value Am = {airmass};

	double gain=2.42;
	hdrl_value G= {gain};
	sinfo_msg("resp gain=%g",gain);
	double exptime=sinfo_pfits_get_dit(plist);
	sinfo_msg("resp exptime=%g",exptime);

	hdrl_value Tex= {exptime};
	hdrl_parameter* calc_par = hdrl_response_parameter_create(Ap, Am, G, Tex);

	return calc_par;

}

static hdrl_spectrum1Dlist *
sinfo_get_telluric_models(const cpl_frame * telluric_cat){

	cpl_ensure(telluric_cat != NULL, CPL_ERROR_NULL_INPUT, NULL);
    const char * cat_name = cpl_frame_get_filename(telluric_cat);
    const cpl_size next = cpl_frame_get_nextensions(telluric_cat);

    hdrl_spectrum1Dlist * list = hdrl_spectrum1Dlist_new();

    for(cpl_size i = 0; i < next; ++i){
        cpl_table * tab = cpl_table_load(cat_name, 1 + i, 1);

        hdrl_spectrum1D * s =
                hdrl_spectrum1D_convert_from_table(tab,
                        "flux",
                        "lam",
                        NULL,
                        NULL,
                        global_scale);
        cpl_table_delete(tab);
        /*um to nm*/
        hdrl_spectrum1D_wavelength_mult_scalar_linear(s, 1e3);
        hdrl_spectrum1Dlist_set(list, s, i);
    }

    return list;
}


static cpl_bivector *
sinfo_read_wlen_windows(const cpl_frame * fit_areas_frm){

	cpl_ensure(fit_areas_frm != NULL, CPL_ERROR_NULL_INPUT, NULL);
    cpl_table * tab = NULL;

    tab = cpl_table_load(cpl_frame_get_filename(fit_areas_frm),1,0);


    const cpl_size nrow = cpl_table_get_nrow(tab);

    double * pwmin = cpl_table_unwrap(tab,"LAMBDA_MIN");
    double * pwmax = cpl_table_unwrap(tab,"LAMBDA_MAX");

    cpl_vector * wmin = cpl_vector_wrap(nrow, pwmin);
    cpl_vector * wmax = cpl_vector_wrap(nrow, pwmax);
    cpl_bivector * to_ret = cpl_bivector_wrap_vectors(wmin, wmax);
    cpl_table_delete(tab);
    sinfo_print_rec_status(0);
    return to_ret;
}

static cpl_array *
sinfo_read_fit_points(const cpl_frame * frm_fit_points,
		const double ra, const double dec, const double ra_dec_tolerance){

	cpl_ensure(frm_fit_points != NULL, CPL_ERROR_NULL_INPUT, NULL);
	const char * fname = cpl_frame_get_filename(frm_fit_points);

	cpl_table * index_table = cpl_table_load(fname, 1, CPL_FALSE);
	const cpl_size sz = cpl_table_get_nrow(index_table);

	cpl_size selected_ext = -1;

	for(cpl_size i = 0; i < sz; ++i){
		int rej;
		const int ext_id = cpl_table_get_int(index_table, "ext_id", i ,&rej);
		const double curr_ra = cpl_table_get(index_table, "ra", i, &rej);
		const double curr_dec = cpl_table_get(index_table, "dec", i, &rej);
		if ((ext_id > 0) && (fabs(curr_ra - ra) < ra_dec_tolerance) &&
				(fabs(curr_dec - dec) < ra_dec_tolerance)){
			selected_ext = ext_id;
		}
	}

	cpl_table_delete(index_table);
	cpl_ensure(selected_ext >= 0, CPL_ERROR_ILLEGAL_OUTPUT, NULL);


	cpl_table * tb = cpl_table_load(fname, selected_ext, CPL_FALSE);

	const cpl_size sz_points = cpl_table_get_nrow(tb);
	cpl_array * fit_points = cpl_array_new(sz_points, CPL_TYPE_DOUBLE);

	for(cpl_size i = 0; i < sz_points; ++i){
		int rej = 0;
		const double d = cpl_table_get(tb, "LAMBDA", i, &rej);
		cpl_array_set(fit_points, i, d);
	}

	cpl_table_delete(tb);
	return fit_points;
}

static inline cpl_size
get_before(const double wmin_spectrum, const double wmin_interval, const double step){
    const double num = (wmin_spectrum - wmin_interval) / step;
    if(num <= 0) return 0;
    return (cpl_size) ceil(num);
}

static inline cpl_size
get_after(const double wmax_spectrum, const double wmax_interval, const double step){
    const double num = (wmax_interval - wmax_spectrum) / step;
    if(num <= 0) return 0;
    return (cpl_size) ceil(num);
}

static inline
hdrl_spectrum1D * extend_hdrl_spectrum(const double wmin, const double wmax,
        const hdrl_spectrum1D * s){

	cpl_ensure(wmin < wmax, CPL_ERROR_ILLEGAL_INPUT, NULL);
	cpl_ensure(s != NULL, CPL_ERROR_NULL_INPUT, NULL);

    const cpl_array * lambadas_s = hdrl_spectrum1D_get_wavelength(s).wavelength;
    const double s_wmin = cpl_array_get_min(lambadas_s);
    const double s_wmax = cpl_array_get_max(lambadas_s);

    const cpl_size sz_s = cpl_array_get_size(lambadas_s);
    const double step = (s_wmax - s_wmin)/sz_s;

    sinfo_msg("min=%g max=%g step=%g",s_wmin,s_wmax,step);
    cpl_size n_before = get_before(s_wmin, wmin, step);
    cpl_size n_after = get_after(s_wmax, wmax, step);

    const cpl_size new_size = n_before + n_after + sz_s;
    cpl_array * new_lambdas = cpl_array_new(new_size,
            cpl_array_get_type(lambadas_s));
    hdrl_image * new_flux = hdrl_image_new(new_size, 1);

    double lambda = s_wmin;
    for(cpl_size i = n_before - 1; i >= 0; i--){
        lambda -= step;
        cpl_array_set(new_lambdas, i, lambda);
        hdrl_image_reject(new_flux, i + 1, 1);
    }

    lambda = s_wmax;
    for(cpl_size i = n_before + sz_s; i < new_size; i++){
            lambda += step;
            cpl_array_set(new_lambdas, i, lambda);
            hdrl_image_reject(new_flux, i + 1, 1);
    }

    for(cpl_size i = n_before; i < n_before + sz_s; i++){
        const cpl_size idx_ori = i - n_before;
        int rej = 0;
        const double lambda =
                hdrl_spectrum1D_get_wavelength_value(s, idx_ori, &rej);
        cpl_array_set(new_lambdas, i, lambda);

        if(rej) {
            hdrl_image_reject(new_flux, i + 1, 1);
            continue;
        }

        hdrl_value val = hdrl_spectrum1D_get_flux_value(s, idx_ori, NULL);
        hdrl_image_set_pixel(new_flux, i + 1, 1, val);
    }

    const hdrl_spectrum1D_wave_scale scale = hdrl_spectrum1D_get_scale(s);

    hdrl_spectrum1D * to_ret =
            hdrl_spectrum1D_create(hdrl_image_get_image(new_flux),
                    hdrl_image_get_error(new_flux),
                    new_lambdas,
                    scale);


    cpl_array_delete(new_lambdas);
    hdrl_image_delete(new_flux);

    return to_ret;
}

int sinfo_can_compute_response(cpl_frameset* sof) {

	int has_std_star_spectra=1;
	int has_flux_std_catalog=1;
	int has_tel_mod_catalog=1;
	int has_resp_fit_points_cat=1;
	int has_extcoeff_table=1;
	int has_high_abs_regions=1;
	int has_quality_areas=1;
	int has_fit_areas=1;
	int has_resp_windows=1;

	if (NULL == cpl_frameset_find(sof, PRO_STD_STAR_SPECTRA) ){
		sinfo_msg_warning("Provide %s to do compute response.",PRO_STD_STAR_SPECTRA);
		has_std_star_spectra=0;
	}

	if (NULL == cpl_frameset_find(sof, FLUX_STD_CATALOG) ){
		sinfo_msg_warning("Provide %s to do compute response.",FLUX_STD_CATALOG);
		has_flux_std_catalog=0;
	}
	if (NULL == cpl_frameset_find(sof, TELL_MOD_CATALOG) ){
		sinfo_msg_warning("Provide %s to do compute response.",TELL_MOD_CATALOG);
		has_tel_mod_catalog=0;
	}
	if (NULL == cpl_frameset_find(sof, RESP_FIT_POINTS_CAT) ){
		sinfo_msg_warning("Provide %s to do compute response.",RESP_FIT_POINTS_CAT);
		has_resp_fit_points_cat=0;
	}

	if (NULL == cpl_frameset_find(sof, EXTCOEFF_TABLE) ){
		sinfo_msg_warning("Provide %s to do compute response.",EXTCOEFF_TABLE);
		has_extcoeff_table=0;
	}

	if (NULL == cpl_frameset_find(sof, HIGH_ABS_REGIONS) ){
		sinfo_msg_warning("Frame %s not provided to do compute response.",HIGH_ABS_REGIONS);
		has_high_abs_regions=1;
		//return 0;
	}

	if (NULL == cpl_frameset_find(sof, QUALITY_AREAS) ){
		sinfo_msg_warning("Provide %s to do compute response.",QUALITY_AREAS);
		has_quality_areas=0;
	}

	if (NULL == cpl_frameset_find(sof, FIT_AREAS) ){
		sinfo_msg_warning("Provide %s to do compute response.",FIT_AREAS);
		has_fit_areas=0;
	}

	if (NULL == cpl_frameset_find(sof, RESPONSE_WINDOWS) ){
		sinfo_msg_warning("Provide %s to do compute response.",RESPONSE_WINDOWS);
		has_resp_windows=0;
	}

	return has_std_star_spectra *
			has_flux_std_catalog *
			has_tel_mod_catalog *
			has_resp_fit_points_cat *
			has_extcoeff_table *
			has_high_abs_regions *
			has_quality_areas *
			has_fit_areas *
			has_resp_windows;

}

int sinfo_can_flux_calibrate(cpl_frameset* sof) {


	int has_extcoeff_table=1;
	int has_response=1;
	if (NULL == cpl_frameset_find(sof, EXTCOEFF_TABLE) ){
		sinfo_msg_warning("Provide %s to do flux calibration.",EXTCOEFF_TABLE);
		has_extcoeff_table=0;
	}

	if (NULL == cpl_frameset_find(sof, PRO_RESPONSE) ) {
		sinfo_msg_warning("Provide %s to do flux calibration.",PRO_RESPONSE);
		has_response=0;

	}
	return has_extcoeff_table * has_response;
}

cpl_error_code
sinfo_flux_calibrate_spectra(const char* plugin_id, cpl_parameterlist* config,
		cpl_frameset* ref_set, cpl_frameset* sof){

	/* Flux calibrate the spectrum */
	const char* fname;

	hdrl_spectrum1D_wave_scale scale = hdrl_spectrum1D_wave_scale_linear;
	//cpl_frame* response=cpl_frameset_find(sof, PRO_RESPONSE);
	cpl_frame* spectra_frm=cpl_frameset_find(sof, PRO_STD_STAR_SPECTRA);
	fname = cpl_frame_get_filename(spectra_frm);
	cpl_table* spectra_tab=cpl_table_load(fname,1,0);
	char band[FILE_NAME_SZ];
	sinfo_get_band(spectra_frm,band);
	double bin_size=sinfo_get_dispersion(band);
	sinfo_msg("bin_size=%g",bin_size);
	double um2nm=1.e3;
	double nm2AA=10;
	double bin_size_scale_factor=bin_size*um2nm*nm2AA;

	//double sci_wmin=cpl_table_get_column_min(spectra_tab,"wavelength");
	//double sci_wmax=cpl_table_get_column_max(spectra_tab,"wavelength");


	cpl_propertylist* plist=cpl_propertylist_load(fname,0);
	double dit = sinfo_pfits_get_dit(plist);
	//int ndit = sinfo_pfits_get_ndit(plist);
	double exptime = dit; // for NIR (SINFONI data) we use only DIT
	double gain = 2.42; // sinfo_pfits_get_gain(plist);
    double airm = sinfo_pfits_get_airm_mean(plist);
    sinfo_free_propertylist(&plist);

    double factor=1./(gain*exptime*bin_size_scale_factor);


	hdrl_spectrum1D * obs_s1d =
			hdrl_spectrum1D_convert_from_table(spectra_tab, "counts_bkg",
					"wavelength", NULL, NULL, scale);
	//hdrl_spectrum1D_save(obs_s1d, "obs_s1d.fits");

	cpl_frame* resp_frm=cpl_frameset_find(sof, PRO_RESPONSE);
	fname = cpl_frame_get_filename(resp_frm);
	cpl_table* resp_tab=cpl_table_load(fname,1,0);

    hdrl_spectrum1D * resp_s1d =
				hdrl_spectrum1D_convert_from_table(resp_tab, "response_smo",
						"wavelength", "response_smo_error", "response_smo_bpm", scale);
    //hdrl_spectrum1D_save(resp_s1d, "resp_s1d.fits");


	cpl_frame* atm_ext_frm=cpl_frameset_find(sof, EXTCOEFF_TABLE);
	fname = cpl_frame_get_filename(atm_ext_frm);
	cpl_table* atm_ext_tab=cpl_table_load(fname,1,0);

    hdrl_spectrum1D * ext_s1d =
				hdrl_spectrum1D_convert_from_table(atm_ext_tab, "EXTINCTION",
						"LAMBDA", NULL, NULL, scale);

    //hdrl_spectrum1D * ext_s1d_e = extend_hdrl_spectrum(sci_wmin * um2nm, sci_wmax * um2nm, ext_s1d);
    //hdrl_spectrum1D_save(ext_s1d_e, "ext_s1d_e.fits");

    //sinfo_msg("factor=%g", factor);

    cpl_msg_warning("","%f %f", cpl_array_get_min(hdrl_spectrum1D_get_wavelength(obs_s1d).wavelength), cpl_array_get_max(hdrl_spectrum1D_get_wavelength(obs_s1d).wavelength));
    cpl_msg_warning("","%f %f", cpl_array_get_min(hdrl_spectrum1D_get_wavelength(resp_s1d).wavelength), cpl_array_get_max(hdrl_spectrum1D_get_wavelength(resp_s1d).wavelength));


    hdrl_spectrum1D* flux_s1d = hdrl_spectrum1D_mul_spectrum_create(obs_s1d, resp_s1d);

    hdrl_spectrum1D_mul_scalar(flux_s1d,(hdrl_value){factor,0.});

    //hdrl_spectrum1D_save(flux_s1d, "flux_s1d_0.fits");

    double* pflux = cpl_image_get_data_double(hdrl_image_get_image(flux_s1d->flux));
    int sx=hdrl_image_get_size_x(flux_s1d->flux);


    double* pext = cpl_image_get_data_double(hdrl_image_get_image(ext_s1d->flux));
    for(int i=0;i<sx;i++) {
    	double exp=-0.4*airm*pext[i];
    	pflux[i] *= pow(10.,exp);
    }
    //hdrl_spectrum1D_save(flux_s1d, "flux_s1d_1.fits");

    cpl_table* flux_tab = hdrl_spectrum1D_convert_to_table(flux_s1d,
    		"flux", "wavelength", "flux_error", "flux_bpm");
    //cpl_table_save(flux_tab,NULL,NULL,"spectrum_flux.fits",CPL_IO_DEFAULT);

    cpl_table* qclog_tbl = sinfo_qclog_init();
    sinfo_pro_save_tbl(flux_tab, ref_set, sof,
    		(char*) SPECTRA_FLUXCAL_FILENAME,
			PRO_STD_STAR_SPECTRA_FLUXCAL, qclog_tbl, plugin_id, config);
    sinfo_free_table(&qclog_tbl);

    hdrl_spectrum1D_delete(&obs_s1d);
    hdrl_spectrum1D_delete(&resp_s1d);
    hdrl_spectrum1D_delete(&ext_s1d);
   // hdrl_spectrum1D_delete(&ext_s1d_e);
    hdrl_spectrum1D_delete(&flux_s1d);
    sinfo_free_table(&atm_ext_tab);
    sinfo_free_table(&flux_tab);
    sinfo_free_table(&spectra_tab);
	sinfo_free_table(&resp_tab);
	sinfo_free_table(&resp_tab);

	sinfo_print_rec_status(0);
	return cpl_error_get_code();
}



cpl_error_code
sinfo_flux_calibrate_cube(const char* procatg, const char* plugin_id,
		cpl_parameterlist* config, cpl_frameset* ref_set, cpl_frameset* sof){

	/* Flux calibrate the spectrum */

	const char* fname;

	hdrl_spectrum1D_wave_scale scale = hdrl_spectrum1D_wave_scale_linear;

	char pro_mjit[MAX_NAME_SIZE];
	char pro_obs[MAX_NAME_SIZE];
	char pro_med[MAX_NAME_SIZE];


	if (strcmp(procatg, PRO_COADD_STD) == 0) {
		strcpy(pro_mjit, PRO_MASK_COADD_STD);
		strcpy(pro_obs, PRO_OBS_STD);
		strcpy(pro_med, PRO_MED_COADD_STD);

	}
	else if (strcmp(procatg, PRO_COADD_PSF) == 0) {
		strcpy(pro_mjit, PRO_MASK_COADD_PSF);
		strcpy(pro_obs, PRO_OBS_PSF);
		strcpy(pro_med, PRO_MED_COADD_PSF);
	}
	else {
		strcpy(pro_mjit, PRO_MASK_COADD_OBJ);
		strcpy(pro_obs, PRO_OBS_OBJ);
		strcpy(pro_med, PRO_MED_COADD_OBJ);
	}
	//cpl_frame* response = cpl_frameset_find(sof, PRO_RESPONSE);
	cpl_frame* coadd_frm=cpl_frameset_find(sof, procatg);
	fname = cpl_frame_get_filename(coadd_frm);
	sinfo_msg("fname=%s",fname);
	cpl_imagelist* cube=cpl_imagelist_load(fname,CPL_TYPE_FLOAT,0);


	cpl_propertylist* plist=cpl_propertylist_load(fname,0);

	//int crpix = sinfo_pfits_get_crpix3(plist);
	//double cdelt = sinfo_pfits_get_cdelt3(plist);
	//double crval = sinfo_pfits_get_crval3(plist);
	//int size=cpl_imagelist_get_size(cube);
	//double sci_wmin=crval-0.5*size*cdelt;
	//double sci_wmax=crval+0.5*(size-1)*cdelt;

	//sinfo_msg("sci_wmin=%g sci_wmax=%g",sci_wmin,sci_wmax);
	//double um2nm=1.e3;


	//cpl_propertylist_dump(plist,stdout);
	double dit = sinfo_pfits_get_dit(plist);
	int ndit = sinfo_pfits_get_ndit(plist);
	double centralLambda=0;
	double dis=0;
    double centralpix=0;
    double newcenter_x=0;
    double newcenter_y=0;
	sinfo_get_wcs_cube(plist, &centralLambda, &dis, &centralpix, &newcenter_x,
	    		&newcenter_y);
	double exptime = dit * ndit;
	sinfo_msg("exptime=%g",exptime);
	double gain = 2.42; // sinfo_pfits_get_gain(plist);
    sinfo_msg("gain=%g",gain);

    double airm = sinfo_pfits_get_airm_mean(plist);
    sinfo_msg("airm=%g",airm);
    double um2nm=1000.;
    double nm2AA=10.;
    char band[FILE_NAME_SZ];
    sinfo_get_band(coadd_frm,band);
    double bin_size=sinfo_get_dispersion(band);
    double bin_size_scale_factor=bin_size*um2nm*nm2AA;
    double factor=1./(gain*exptime*bin_size_scale_factor);
    sinfo_msg("factor=%g",factor);
	cpl_frame* resp_frm=cpl_frameset_find(sof, PRO_RESPONSE);
	fname = cpl_frame_get_filename(resp_frm);
	cpl_table* resp_tab=cpl_table_load(fname,1,0);

    hdrl_spectrum1D * resp_s1d =
				hdrl_spectrum1D_convert_from_table(resp_tab, "response_smo",
						"wavelength", "response_smo_error", "response_smo_bpm", scale);
    //hdrl_spectrum1D_save(resp_s1d, "resp_s1d.fits");

	cpl_frame* atm_ext_frm=cpl_frameset_find(sof, EXTCOEFF_TABLE);
	fname = cpl_frame_get_filename(atm_ext_frm);
	cpl_table* atm_ext_tab=cpl_table_load(fname,1,0);

    hdrl_spectrum1D * ext_s1d =
				hdrl_spectrum1D_convert_from_table(atm_ext_tab, "EXTINCTION",
						"LAMBDA", NULL, NULL, scale);
    //hdrl_spectrum1D_save(ext_s1d, "ext_s1d.fits");

    hdrl_parameter * res_par =
            hdrl_spectrum1D_resample_interpolate_parameter_create(hdrl_spectrum1D_interp_akima);

    hdrl_spectrum1D_wavelength wav = hdrl_spectrum1D_get_wavelength(resp_s1d);

    hdrl_spectrum1D * ext_s1d_e = hdrl_spectrum1D_resample(ext_s1d,
            &wav, res_par);

    hdrl_parameter_delete(res_par);

    //hdrl_spectrum1D_save(ext_s1d_e, "resp_s1d_e.fits");

    int sx=hdrl_image_get_size_x(resp_s1d->flux);
    double* pext = cpl_image_get_data_double(hdrl_image_get_image(ext_s1d->flux));

    /* Flux calibrate the cube */

	double* presp = cpl_image_get_data_double(hdrl_image_get_image(resp_s1d->flux));


	/*
	hdrl_value exp = {-0.4*airm, 0};
	hdrl_spectrum1D* ext_s1d_cor =
			hdrl_spectrum1D_mul_scalar_create(ext_s1d, exp);
    hdrl_spectrum1D_pow_scalar(ext_s1d_cor, )
    */

    for(int i=0;i<sx;i++) {

    	double exp=-0.4*airm*pext[i];

    	cpl_image* ima=cpl_imagelist_get(cube,i);
    	double atm_ext = pow(10.,exp);
    	cpl_image_multiply_scalar(ima,presp[i]);
    	cpl_image_multiply_scalar(ima,atm_ext);
    	cpl_image_multiply_scalar(ima,factor);

    }
    char ptag[80];
    sprintf(ptag,"%s%s",procatg,"_FLUXCAL");
    //cpl_imagelist_save(cube,"cube_flux.fits",CPL_TYPE_FLOAT,NULL,CPL_IO_DEFAULT);
    cpl_table* qclog = sinfo_qclog_init();



    sinfo_pro_save_ims(cube, sof, sof, "out_cube_fluxcal.fits",
    		ptag, qclog, plugin_id, config);
    sinfo_new_set_wcs_cube(cube, "out_cube_fluxcal.fits",centralLambda, dis,
                                      centralpix, newcenter_x, newcenter_y);
    sinfo_free_table(&qclog);

    sinfo_free_propertylist(&plist);
    hdrl_spectrum1D_delete(&resp_s1d);
    hdrl_spectrum1D_delete(&ext_s1d);
    hdrl_spectrum1D_delete(&ext_s1d_e);
    sinfo_free_imagelist(&cube);
	sinfo_free_table(&atm_ext_tab);
	sinfo_free_table(&resp_tab);
	sinfo_print_rec_status(0);

	return cpl_error_get_code();
}

static cpl_error_code
sinfo_resp_qc(cpl_table* resp, cpl_frame* frm_eff_wind_qc, const double wmin_g,
		const double wmax_g, cpl_table** qclog_tbl )
{


    const char* ew_name=cpl_frame_get_filename(frm_eff_wind_qc);
    cpl_table* ew_tab=cpl_table_load(ew_name,1,0);
    int nrow = cpl_table_get_nrow(ew_tab);

    float* pwmin = cpl_table_get_data_float(ew_tab,"WMIN");
    float* pwmax = cpl_table_get_data_float(ew_tab,"WMAX");
    cpl_table* tmp_resp=NULL;
    double wmin;
    double wmax;
    double resp_avg;
    double resp_med;
    double resp_min;
    double resp_max;
    double resp_std;
    char key_name[40];
    //double um2nm=1000;
    int nrow_tmp=0;
    for(int i=0;i<nrow;i++) {
    	cpl_table_select_all(resp);
        wmin=pwmin[i];
        wmax=pwmax[i];
        cpl_table_and_selected_double(resp,"wavelength",CPL_NOT_LESS_THAN,wmin_g);
        cpl_table_and_selected_double(resp,"wavelength",CPL_NOT_LESS_THAN,wmin);
        cpl_table_and_selected_double(resp,"wavelength",CPL_NOT_GREATER_THAN,wmax);
        cpl_table_and_selected_double(resp,"wavelength",CPL_NOT_GREATER_THAN,wmax_g);
        tmp_resp=cpl_table_extract_selected(resp);
        nrow_tmp = cpl_table_get_nrow(tmp_resp);
        if(nrow_tmp>0) {
        	resp_avg=cpl_table_get_column_mean(tmp_resp,"response_smo");
        	resp_med=cpl_table_get_column_median(tmp_resp,"response_smo");
        	resp_min=cpl_table_get_column_min(tmp_resp,"response_smo");
        	resp_max=cpl_table_get_column_max(tmp_resp,"response_smo");
        	resp_std=cpl_table_get_column_stdev(tmp_resp,"response_smo");
            /*
            sinfo_msg("wmin=%g wmax=%g avg=%g med=%g min=%g max=%g std=%g",
            		wmin, wmax, resp_avg,resp_med,resp_min,resp_max,resp_std);
            */
        	sprintf(key_name,"QC RESP WIN%d WLMIN",i);
        	sinfo_qclog_add_double(*qclog_tbl, key_name, wmin,
        			"[um] Min window wavelength for resp comp");

        	sprintf(key_name,"QC RESP WIN%d WLMAX",i);
        	sinfo_qclog_add_double(*qclog_tbl, key_name, wmax,
        			"[um] Max window wavelength for resp comp");


        	sprintf(key_name,"QC RESP WIN%d MEAN",i);
        	sinfo_qclog_add_double(*qclog_tbl, key_name, resp_avg,
        			"Mean response on window");

        	sprintf(key_name,"QC RESP WIN%d MEDIAN",i);
        	sinfo_qclog_add_double(*qclog_tbl, key_name, resp_med,
        			"Median response on window");

        	sprintf(key_name,"QC RESP WIN%d MIN",i);
        	sinfo_qclog_add_double(*qclog_tbl, key_name, resp_min,
        			"Min response on window");

        	sprintf(key_name,"QC RESP WIN%d MAX",i);
        	sinfo_qclog_add_double(*qclog_tbl, key_name, resp_max,
        			"Max response on window");

        	sprintf(key_name,"QC RESP WIN%d RMS",i);
        	sinfo_qclog_add_double(*qclog_tbl, key_name, resp_std,
        			"RMS response on window");
        }
        cpl_table_select_all(resp);
        sinfo_free_table(&tmp_resp);

    }
    cpl_table_select_all(resp);
    wmin=cpl_table_get_column_min(ew_tab,"WMIN");
    wmax=cpl_table_get_column_max(ew_tab,"WMAX");
    cpl_table_and_selected_double(resp,"wavelength",CPL_NOT_LESS_THAN,wmin_g);
    cpl_table_and_selected_double(resp,"wavelength",CPL_NOT_GREATER_THAN,wmax_g);
    tmp_resp=cpl_table_extract_selected(resp);

    resp_avg=cpl_table_get_column_mean(tmp_resp,"response_smo");
    resp_med=cpl_table_get_column_median(tmp_resp,"response_smo");
    resp_min=cpl_table_get_column_min(tmp_resp,"response_smo");
    resp_max=cpl_table_get_column_max(tmp_resp,"response_smo");
    resp_std=cpl_table_get_column_stdev(tmp_resp,"response_smo");
    /*
    sinfo_msg("wmin=%g wmax=%g avg=%g med=%g min=%g max=%g std=%g",
               wmin, wmax,resp_avg,resp_med,resp_min,resp_max,resp_std);
    */
    sprintf(key_name,"QC RESP WLMIN");
    sinfo_qclog_add_double(*qclog_tbl, key_name, wmin,
                                 "[um] Min wavelength for resp comp");

    sprintf(key_name,"QC RESP WLMAX");
    sinfo_qclog_add_double(*qclog_tbl, key_name, wmax,
                                  "[um] Max wavelength for resp comp");

    sprintf(key_name,"QC RESP MEAN");
    sinfo_qclog_add_double(*qclog_tbl, key_name, resp_avg,
                           "Mean response");

    sprintf(key_name,"QC RESP MEDIAN");
    sinfo_qclog_add_double(*qclog_tbl, key_name, resp_med,
                           "Median response");

    sprintf(key_name,"QC RESP MIN");
    sinfo_qclog_add_double(*qclog_tbl, key_name, resp_min,
                           "Min response");

    sprintf(key_name,"QC RESP MAX");
    sinfo_qclog_add_double(*qclog_tbl, key_name, resp_max,
                           "Max response");

    sprintf(key_name,"QC RESP RMS");
    sinfo_qclog_add_double(*qclog_tbl, key_name, resp_std,
                           "RMS response");
    sinfo_free_table(&tmp_resp);
    sinfo_free_table(&ew_tab);
    sinfo_print_rec_status(0);
    return cpl_error_get_code();
}

cpl_error_code
sinfo_response_compute(const char* plugin_id, cpl_parameterlist* config,
		cpl_frameset* ref_set,cpl_frameset* sof)
{


	cpl_frame* frm_sci = cpl_frameset_find(sof, PRO_STD_STAR_SPECTRA);
	cpl_frame* frm_std_cat = cpl_frameset_find(sof, FLUX_STD_CATALOG);
	cpl_frame* frm_atmext = cpl_frameset_find(sof, EXTCOEFF_TABLE);
	cpl_frame* frm_tell_cat = cpl_frameset_find(sof, TELL_MOD_CATALOG);
	cpl_frame* frm_resp_fit_points = cpl_frameset_find(sof, RESP_FIT_POINTS_CAT);
	cpl_frame* frm_high_abs_reg = cpl_frameset_find(sof, HIGH_ABS_REGIONS);
	cpl_frame* frm_resp_quality_areas = cpl_frameset_find(sof, QUALITY_AREAS);
	cpl_frame* frm_resp_fit_areas = cpl_frameset_find(sof, FIT_AREAS);

	cpl_ensure(frm_sci != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);
	cpl_ensure(frm_std_cat != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);
	cpl_ensure(frm_atmext != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);
	cpl_ensure(frm_tell_cat != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);
	cpl_ensure(frm_resp_fit_points != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);
        /* HIGH_ABS_REGIONS is an optional input
	cpl_ensure(frm_high_abs_reg != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);
        */
	cpl_ensure(frm_resp_quality_areas != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);
	cpl_ensure(frm_resp_fit_areas != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);

	const char* name_sci=NULL;
	cpl_propertylist* plist=NULL;
	cpl_table* sci_tab=NULL;
	name_sci=cpl_frame_get_filename(frm_sci);
	plist=cpl_propertylist_load(name_sci,0);
	double dRA=sinfo_pfits_get_ra(plist);
	double  dDEC=sinfo_pfits_get_dec(plist);
	sci_tab=cpl_table_load(name_sci,1,0);
    double sci_wmin=cpl_table_get_column_min(sci_tab,"wavelength");
    double sci_wmax=cpl_table_get_column_max(sci_tab,"wavelength");
    sinfo_free_table(&sci_tab);
	//cpl_table* resp=NULL;


	//hdrl_spectrum1D * eff_s1d;

	double um2nm=1.e3;

	hdrl_spectrum1D * sci_s1d = sinfo_sci_frame_to_hdrl_spectrum1D(frm_sci);


	/* HDRL expects results in nm */
	hdrl_spectrum1D_wavelength_mult_scalar_linear(sci_s1d,um2nm);

	/* because the reference flux std assumes the flux in units of ergs/cm2/s/A
	 * we need to convert the spectrum to Angstroms and know the size of a bin
	 * in angstroms
	 */
	char band[FILE_NAME_SZ];
	sinfo_get_band(frm_sci,band);

	double conv_factor=0;
	double nm2AA = 10.;
	/* the following is the bin size in um */
	double bin_size=sinfo_get_dispersion(band);
	sinfo_msg("bin_size=%g",bin_size);
	conv_factor=bin_size*um2nm*nm2AA;

	hdrl_spectrum1D_div_scalar(sci_s1d,(hdrl_value){conv_factor,0.});

	hdrl_spectrum1D * std_s1d = sinfo_std_cat_frame_to_hdrl_spectrum1D(frm_std_cat,dRA,dDEC);

	cpl_ensure(std_s1d != NULL, CPL_ERROR_NULL_INPUT, CPL_ERROR_NULL_INPUT);

	hdrl_spectrum1D * ext_s1d = sinfo_atmext_frame_to_hdrl_spectrum1D(frm_atmext);


	//hdrl_parameter* eff_pars;
	/*HDRL expects telescope area in cm2 */

	hdrl_spectrum1Dlist * telluric_models =
			sinfo_get_telluric_models(frm_tell_cat);


	cpl_bivector * fit_areas = sinfo_read_wlen_windows(frm_resp_fit_areas);
	cpl_bivector * quality_areas = sinfo_read_wlen_windows(frm_resp_quality_areas);
	cpl_bivector * high_abs_regions = NULL;

	if(frm_high_abs_reg!= NULL)
		high_abs_regions = sinfo_read_wlen_windows(frm_high_abs_reg);





    /* set [parameters */
	double wmin=0;
	double wmax=0;
	cpl_boolean velocity_enable = CPL_TRUE;
	if (strcmp(band,"J") == 0) {
		//J
		wmin=7.001; //log scale
		wmax=7.100;
		velocity_enable = CPL_TRUE;
	} else if (strcmp(band,"H") == 0) {
		//H
		wmin=7.268; //log scale
		wmax=7.531;
		velocity_enable = CPL_FALSE;
	} else if (strcmp(band,"K") == 0) {
		//K
		wmin=7.565; //log scale
		wmax=7.812;
		velocity_enable = CPL_FALSE;
	} else if (strcmp(band,"H+K") == 0) {
		//HK
		wmin=7.268; //log scale
		wmax=7.812;
		velocity_enable = CPL_FALSE;
	}

    //sinfo_msg("band=%s wmin=%g wmax=%g",band,wmin,wmax);

    const hdrl_data_t lmin = (hdrl_data_t)wmin;
    const hdrl_data_t lmax = (hdrl_data_t)wmax;


    const cpl_boolean xcorr_normalize = (cpl_boolean) CPL_TRUE;
    const cpl_boolean shift_in_log_scale = (cpl_boolean) CPL_TRUE;

	const hdrl_data_t wguess = 1094.12; // um
	const hdrl_data_t range_wmin =1000;
	const hdrl_data_t range_wmax =1200;
	const hdrl_data_t fit_wmin = 1090;
	const hdrl_data_t fit_wmax = 1100;
	const hdrl_data_t fit_half_win = 1.5;

    hdrl_parameter* shift_par = NULL;
	if(velocity_enable) {
		shift_par = hdrl_spectrum1D_shift_fit_parameter_create(wguess, range_wmin,
				range_wmax, fit_wmin, fit_wmax,fit_half_win);
	}


	hdrl_parameter* resp_calc_par = sinfo_response_parameters_calc_par(plist);

	double ra_dec_tolerance=0.1;
	cpl_array 	* fit_points = sinfo_read_fit_points(frm_resp_fit_points,
			dRA, dDEC, ra_dec_tolerance);

	hdrl_parameter * telluric_par=NULL;
	if(telluric_models != NULL && quality_areas != NULL && fit_areas != NULL) {
		const cpl_size xcorr_half_win = 200;
		hdrl_data_t xcorr_w_step=1e-5;
		telluric_par = hdrl_response_telluric_evaluation_parameter_create(telluric_models,
				xcorr_w_step, xcorr_half_win, xcorr_normalize, shift_in_log_scale, quality_areas,
				fit_areas, lmin, lmax);
	}

	const cpl_size post_smoothing_radius=11;

	//TODO: fill following params
	const hdrl_data_t fit_wrange=4.0;
    hdrl_parameter* resp_par =
	hdrl_response_fit_parameter_create(post_smoothing_radius, fit_points,
			fit_wrange, high_abs_regions);

    hdrl_spectrum1D * std_s1d_e = extend_hdrl_spectrum(sci_wmin * um2nm, sci_wmax * um2nm, std_s1d);
    hdrl_spectrum1D * ext_s1d_e = extend_hdrl_spectrum(sci_wmin * um2nm, sci_wmax * um2nm, ext_s1d);

    hdrl_response_result * response =
    		hdrl_response_compute(sci_s1d,std_s1d_e,ext_s1d_e,telluric_par,shift_par,
    				resp_calc_par,resp_par);
    sinfo_msg("size sci = %lld size resp = %lld",
    		hdrl_spectrum1D_get_size(sci_s1d),
			hdrl_spectrum1D_get_size(response->final_response));

    hdrl_spectrum1D_delete(&std_s1d_e);
    hdrl_spectrum1D_delete(&ext_s1d_e);
    /* extends response to same wavelength range as observed sci to flux-cal  */



    if(response != NULL){

        const hdrl_spectrum1D * resp_s1d_smo =
        		hdrl_response_result_get_final_response(response);

        const hdrl_spectrum1D * resp_s1d_raw =
        		hdrl_response_result_get_raw_response(response);
        //sinfo_msg("wmin=%g wmax=%g",sci_wmin,sci_wmax);



        cpl_table* tab=hdrl_spectrum1D_convert_to_table(resp_s1d_smo,
                						"response", "wavelength", "response_error", "response_bpm");

        sci_wmin=cpl_table_get_column_min(tab,"wavelength");
        sci_wmax=cpl_table_get_column_max(tab,"wavelength");
        //sinfo_msg("wmin=%g wmax=%g",sci_wmin,sci_wmax);

        cpl_table* resp_smo=NULL;
        cpl_table* resp_raw=NULL;
        cpl_table* resp_pts=NULL;
    	resp_smo = hdrl_spectrum1D_convert_to_table(resp_s1d_smo,
						"response_smo", "wavelength", "response_smo_error",
						"response_smo_bpm");

    	resp_raw = hdrl_spectrum1D_convert_to_table(resp_s1d_raw,
						"response_raw", "wavelength", "response_raw_error",
						"response_raw_bpm");

    	resp_pts = hdrl_spectrum1D_convert_to_table(
    					hdrl_response_result_get_selected_response(response),
						"response_fit_pts", "wavelength", "response_fit_pts_error",
						"response_fit_pts_bpm");
    	sinfo_free_table(&tab);


    	const double um2nm = 1e3;
    	cpl_table_divide_scalar(resp_smo,"wavelength",um2nm);
    	cpl_table_divide_scalar(resp_raw,"wavelength",um2nm);
    	cpl_table_divide_scalar(resp_pts,"wavelength",um2nm);

    	cpl_table* qclog_tbl = sinfo_qclog_init();
    	cpl_frame* frm_resp_wind_qc = cpl_frameset_find(sof, RESPONSE_WINDOWS);

    	const hdrl_spectrum1D * selected_points = hdrl_response_result_get_selected_response(response);
        const cpl_array * waves = hdrl_spectrum1D_get_wavelength(selected_points).wavelength;
        const double wmin_g = cpl_array_get_min(waves)/um2nm;
        const double wmax_g = cpl_array_get_max(waves)/um2nm;
        //sinfo_msg("good wavelengths wmin=%g wmax=%g",wmin_g,wmax_g);


    	if( resp_smo != NULL && frm_resp_wind_qc != NULL) {

    		sinfo_resp_qc(resp_smo, frm_resp_wind_qc, wmin_g, wmax_g, &qclog_tbl);

    	}

    	sinfo_pro_save_tbl(resp_smo, ref_set, sof,(char*) RESPONSE_FILENAME,
    			PRO_RESPONSE, qclog_tbl, plugin_id, config);

    	cpl_table_save(resp_raw,NULL,NULL,RESPONSE_FILENAME,CPL_IO_EXTEND);
    	cpl_table_save(resp_pts,NULL,NULL,RESPONSE_FILENAME,CPL_IO_EXTEND);
        cpl_table_delete(qclog_tbl);
    	cpl_table_delete(resp_smo);
    	cpl_table_delete(resp_raw);
    	cpl_table_delete(resp_pts);

    }
    cpl_bivector_delete(quality_areas);
    cpl_bivector_delete(fit_areas);
    cpl_bivector_delete(high_abs_regions);

    hdrl_parameter_delete(telluric_par);
    hdrl_parameter_delete(shift_par);
    hdrl_parameter_delete(resp_calc_par);
    hdrl_parameter_delete(resp_par);

    hdrl_spectrum1D_delete(&sci_s1d);
    hdrl_spectrum1D_delete(&std_s1d);
    hdrl_spectrum1D_delete(&ext_s1d);
    sinfo_free_propertylist(&plist);
    hdrl_spectrum1Dlist_delete(telluric_models);


    cpl_array_delete(fit_points);
    hdrl_response_result_delete(response);
    sinfo_print_rec_status(0);
	return cpl_error_get_code();
}



cpl_table*
sinfo_efficiency_compute(cpl_frame* frm_sci, 
                       cpl_frame* frm_cat,
                       cpl_frame* frm_atmext)

{

  cpl_propertylist* plist=NULL;

  cpl_table* tbl_eff=NULL;
  cpl_table* tbl_ref=NULL;
  cpl_table* tbl_atmext=NULL;
  cpl_table* sci_tab=NULL;


  double airmass=0;
  double airmass_start=0;
  double airmass_end=0;
  double dRA=0;
  double dDEC=0;
  double gain=0;
  //double aimprim=0;
  double dEpsilon=0.1;
  //double um2AA=1.e4;
  double um2nm=1000.;
  double nm2um=0.001;
  double m2tocm2=1.e4;

  /* int nrow=0; */


  const char* name_sci=NULL;
  const char* name_atm=NULL;

  name_sci=cpl_frame_get_filename(frm_sci);
  plist=cpl_propertylist_load(name_sci,0);
  sci_tab=cpl_table_load(name_sci,1,0);
  dRA=sinfo_pfits_get_ra(plist);
  dDEC=sinfo_pfits_get_dec(plist);
  airmass_start=sinfo_pfits_get_airmass_end(plist);
    airmass_end=sinfo_pfits_get_airmass_end(plist);
    airmass=0.5*(airmass_start+airmass_end);
  gain=2.42;
  //int biny=1;
  double exptime=sinfo_pfits_get_dit(plist);
  sinfo_free_propertylist(&plist);
  sinfo_msg("gain=%g airm=%g exptime=%g airmass=%g ra=%g dec=%g",
          gain,airmass,exptime,airmass,dRA,dDEC);

  sinfo_msg("table sci spectra=%s",name_sci);

  name_atm=cpl_frame_get_filename(frm_atmext);
  tbl_atmext=cpl_table_load(name_atm,1,0);
 
  sinfo_parse_catalog_std_stars(frm_cat,dRA,dDEC,dEpsilon,&tbl_ref);
 
  if(tbl_ref == NULL) {
    sinfo_msg_error("Provide std sar catalog frame");
    return NULL;
  }

  cpl_table_save(sci_tab,NULL,NULL,"sci.fits",CPL_IO_DEFAULT);


  hdrl_spectrum1D * sci_s1d;
  hdrl_spectrum1D * std_s1d;
  hdrl_spectrum1D * ext_s1d;
  hdrl_spectrum1D * eff_s1d;
  hdrl_spectrum1D_wave_scale scale = hdrl_spectrum1D_wave_scale_linear;

  sci_s1d=hdrl_spectrum1D_convert_from_table(sci_tab, "counts_bkg",
  "wavelength", NULL, NULL, scale);
  /* HDRL expects results in nm */
  hdrl_spectrum1D_wavelength_mult_scalar_linear(sci_s1d,um2nm);

  /* because the reference flux std assumes the flux in units of ergs/cm2/s/A
   * we need to convert the spectrum to Angstroms and know the size of a bin
   * in angstroms
   */
  char band[FILE_NAME_SZ];
  sinfo_get_band(frm_sci,band);

  double conv_factor=0;
  double nm2AA = 10.;
  //double um2AA = um2nm * nm2AA;
  /* the following is the bin size in um */
  double bin_size=sinfo_get_dispersion(band);
  conv_factor=bin_size*um2nm*nm2AA;

  hdrl_spectrum1D_div_scalar(sci_s1d,(hdrl_value){conv_factor,0.});
  std_s1d=hdrl_spectrum1D_convert_from_table(tbl_ref, "FLUX",
   "LAMBDA", NULL, NULL, scale);

  ext_s1d=hdrl_spectrum1D_convert_from_table(tbl_atmext, "EXTINCTION",
    "LAMBDA", NULL, NULL, scale);

  hdrl_parameter* eff_pars;

  hdrl_value Ap = {0};
  hdrl_value Am = {airmass};
  hdrl_value G= {gain};
  hdrl_value Tex= {exptime};
  /*HDRL expects telescope area in cm2 */
  hdrl_value Atel={TELESCOPE_SURFACE*m2tocm2};

  eff_pars = hdrl_efficiency_parameter_create(Ap, Am, G,Tex, Atel);


  eff_s1d = hdrl_efficiency_compute(sci_s1d, std_s1d, ext_s1d, eff_pars);
  /* HDRL expects results in nm */
  hdrl_spectrum1D_wavelength_mult_scalar_linear(eff_s1d,nm2um);
  cpl_propertylist_delete(plist);
  tbl_eff=hdrl_spectrum1D_convert_to_table(eff_s1d, "EFF", "WAVE", NULL,NULL);
  cpl_table_save(tbl_eff,NULL,NULL,"efficiency.fits",CPL_IO_DEFAULT);

  /*
  check_nomsg(tbl_eff=sinfo_utl_efficiency_internal(sci_tab,tbl_atmext,tbl_ref,
                                                 exptime,airmass,aimprim,gain,
                                                 biny,um2AA,
                                                 "LAMBDA",
                                                 "LA_SILLA",
                                                 "LAMBDA",
                                                 "F_LAMBDA",
                                                 "BIN_WIDTH",
                                                 "wavelength",
                                                 "counts_bkg"));
                                                 */


  hdrl_spectrum1D_delete(&std_s1d);
  hdrl_spectrum1D_delete(&sci_s1d);
  hdrl_spectrum1D_delete(&ext_s1d);
  hdrl_spectrum1D_delete(&eff_s1d);
  hdrl_parameter_delete(eff_pars);
  sinfo_free_table(&sci_tab);
  sinfo_free_table(&tbl_ref);
  sinfo_free_table(&tbl_atmext);
  sinfo_free_propertylist(&plist);
 
  return tbl_eff;

}
/**@}*/
