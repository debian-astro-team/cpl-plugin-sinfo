/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdio.h>
#include <string.h>
#include "sinfo_file_handling.h"

/**@{*/
/**
 * @addtogroup sinfo_utilities Function to handle files
 *
 * TBD
 */

/*----------------------------------------------------------------------------
                                                        Function codes
 ---------------------------------------------------------------------------*/


/*-------------------------------------------------------------------------*/
/**
  @name     sinfo_file_exists
  @memo     Find if a given file name corresponds to an existing file.
  @param    filename    Name of the file to look up.
  @return   int 1 if file exists, 0 if not
  @doc

  Find out if the given character string corresponds to a file that
  can be stat()'ed.
 */
/*--------------------------------------------------------------------------*/


int sinfo_file_exists(const char * filename)
{
    int exists=0;
    FILE* fo=NULL;
    if ((fo=fopen(filename,"r"))==NULL) {
        exists=0;
    } else {
        exists=1;
    }

    if(fo!=NULL) {
        fclose(fo);
    }

    return exists;
}
/**@}*/
