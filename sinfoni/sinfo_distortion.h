/* $Id: sinfo_distortion.h,v 1.7 2007-06-06 07:10:45 amodigli Exp $
 *
 * This file is part of the irplib package
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2007-06-06 07:10:45 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_DISTORTION_H
#define SINFO_DISTORTION_H

/*-----------------------------------------------------------------------------
                                   Includes
-----------------------------------------------------------------------------*/
#include <cpl.h>
/*-----------------------------------------------------------------------------
                                       Prototypes
-----------------------------------------------------------------------------*/

cpl_polynomial * sinfo_distortion_estimate_new(
        const cpl_image *   org,
        int                 xmin,
        int                 ymin,
        int                 xmax,
        int                 ymax,
        int                 auto_ramp_sub,
        int                 arc_sat,
        int                 max_arc_width,
        double              kappa,
        double              arcs_min_arclen_factor,
        int                 arcs_window_size,
        int                 smooth_rad,
        int                 degree,
        double              offset,
        cpl_apertures   **  arcs);


#endif
