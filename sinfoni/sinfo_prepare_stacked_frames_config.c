/* $Id: sinfo_prepare_stacked_frames_config.c,v 1.8 2008-02-27 15:10:05 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2008-02-27 15:10:05 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

/**************************************************************************
 * Prepare_Stacked_Frames Frames Data Reduction Parameter Initialization  *
 **************************************************************************/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_prepare_stacked_frames_config.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Stacking parameters definition & initialization
 *
 * TBD
 */

/**
@name  sinfo_prepare_stacked_config_add
@brief stacking parameters definition & initialization
@param list pointer to cpl_parameterlist
@return void
 */
void
sinfo_prepare_stacked_frames_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /*
  --------------------------------------------------------------------------
  Clean Mean 
  --------------------------------------------------------------------------
     */
    /* the fraction [0...1] of rejected low intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_range("sinfoni.stacked.low_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection",
                    "sinfoni.stacked",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI, "stack-lo_rej");
    cpl_parameterlist_append(list, p);

    /* the fraction [0...1] of rejected high intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_range("sinfoni.stacked.high_rejection",
                    CPL_TYPE_DOUBLE,
                    "higher rejection",
                    "sinfoni.stacked",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-hi_rej");
    cpl_parameterlist_append(list, p);


    /*
  --------------------------------------------------------------------------
  Flat Field 
  --------------------------------------------------------------------------
     */
    /* indicates if flatfielding is carried through or not */
    p = cpl_parameter_new_value("sinfoni.stacked.flat_index",
                    CPL_TYPE_BOOL,
                    "Flat Index: ",
                    "sinfoni.stacked",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-flat_ind");
    cpl_parameterlist_append(list, p);


    /* indicates if flatfield is normalized to itself
   (to remove lamp response curve) */
    p = cpl_parameter_new_enum("sinfoni.stacked.mflat_norm_smooth",
                    CPL_TYPE_INT,
                    "Normalize master flat to its smoothed value "
                    "(to remove lamp response curve). "
                    "0 (no smooth). 1 (apply fft filter along y)."
                    "2 (apply running median filter along y).",
                    "sinfoni.stacked",
                    0,3,0,1,2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-mflat_norm_smooth");
    cpl_parameterlist_append(list, p);


    /* indicates if flatfield is normalized to itself
   (to remove lamp response curve) */
    p = cpl_parameter_new_range("sinfoni.stacked.mflat_smooth_rad",
                    CPL_TYPE_INT,
                    "Normalization smoothing radii ",
                    "sinfoni.stacked",
                    16,3,2048);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-mflat_smooth_rad");
    cpl_parameterlist_append(list, p);


    /*
  --------------------------------------------------------------------------
  Bad Pixel 
  --------------------------------------------------------------------------
     */
    /* indicates if the bad pixels should be interpolated or not */
    p = cpl_parameter_new_enum("sinfoni.stacked.mask_index",
                    CPL_TYPE_INT,
                    "BP Mask Interpolation Switch: "
                    "indicates if the bad pixel mask should be "
                    "applied (1) or not (0) ",
                    /*
                              "2: indicates that "
                              "the bad pixels should be interpolated by "
                              "using bezier splines",
                     */
                    "sinfoni.stacked",
                    1,
                    2,0,1); /* there was also 2 allowed */

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-mask_ind");
    cpl_parameterlist_append(list, p);

    /* indicates if the bad pixels should be indicated (TRUE) or
   interpolated (FALSE)*/
    p = cpl_parameter_new_value("sinfoni.stacked.ind_index",
                    CPL_TYPE_BOOL,
                    "indicates if the bad pixels should be "
                    "indicated (yes) or interpolated (no)",
                    "sinfoni.stacked",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-ind_ind");
    cpl_parameterlist_append(list, p);


    /* maximal pixel distance from bad pixel to take valid pixels */
    p = cpl_parameter_new_value("sinfoni.stacked.mask_rad",
                    CPL_TYPE_INT,
                    "Max distance bad-good pix: ",
                    "sinfoni.stacked",
                    4);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-mask_rad");
    cpl_parameterlist_append(list, p);

    /* Temporally removed options: index_list, sigma-factor,
     used if mask_ind=2,3 */
    /* file list containing the index files for bezier interpolation */
    /*
  p = cpl_parameter_new_value("sinfoni.stacked.index_list",
                  CPL_TYPE_STRING,
                              "Contain Index Files: ",
                              "sinfoni.stacked",
                              "indexlist");

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-index_list");
  cpl_parameterlist_append(list, p);
     */

    /* sigmaFactor for bad pixel search in method maskInd = 3 */
    /*
  p = cpl_parameter_new_value("sinfoni.stacked.sigma_factor",
                  CPL_TYPE_DOUBLE,
                              "Sigma Factor for bp search: ",
                              "sinfoni.stacked",
                              3.);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-sigma_factor");
  cpl_parameterlist_append(list, p);
     */

    /*
  --------------------------------------------------------------------------
  Interleaving: only to support dither mode. We comment 
  --------------------------------------------------------------------------
     */
    /* indicates if interleaving should be carried through */
    /*
  p = cpl_parameter_new_value("sinfoni.stacked.inter_index",
                  CPL_TYPE_BOOL,
                              "Interleaving Switch: ",
                              "sinfoni.stacked",
                              FALSE);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-inter_ind");
  cpl_parameterlist_append(list, p);
     */

    /* number of image rows from which the general offset between the frames is
   determined */
    /*
  p = cpl_parameter_new_value("sinfoni.stacked.no_rows",
                  CPL_TYPE_INT,
                              "Number Of Rows",
                              "sinfoni.stacked",
                              400);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-no_rows");
  cpl_parameterlist_append(list, p);
     */

    /*
  --------------------------------------------------------------------------
  Gauss Convolution
  --------------------------------------------------------------------------
     */
    /* indicates if a Gaussian convolution is applied or not */
    p = cpl_parameter_new_value("sinfoni.stacked.gauss_index",
                    CPL_TYPE_BOOL,
                    "Gaussian Convolution Switch: ",
                    "sinfoni.stacked",
                    FALSE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-gauss_ind");
    cpl_parameterlist_append(list, p);

    /* kernel half width of the Gaussian response function */
    p = cpl_parameter_new_value("sinfoni.stacked.kernel_half_width",
                    CPL_TYPE_INT,
                    "Kernel Half Width",
                    "sinfoni.stacked",
                    2);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-khw");
    cpl_parameterlist_append(list, p);
    /*
  --------------------------------------------------------------------------
  Shift Frames (To be used?)
  --------------------------------------------------------------------------
     */
    /* Suppressed in release 1.1.0 */
    /* indicates if a Gaussian convolution is applied or not */
    /*
  p = cpl_parameter_new_value("sinfoni.stacked.shift_frame_index",
                  CPL_TYPE_BOOL,
                              "Shift Frame Switch: ",
                              "sinfoni.stacked",
                              FALSE);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-shft_ind");
  cpl_parameterlist_append(list, p);
     */
    /*type of interpolation to be used (0=polynomial , 1=cubic spline) */
    /*
  p = cpl_parameter_new_enum("sinfoni.stacked.shift_frame_type",
                  CPL_TYPE_INT,
                              "Shift Frame Type: 0 polynomial, 1 cubic,",
                              "sinfoni.stacked",
                   1,
                               2,0,1);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-shft_typ");
  cpl_parameterlist_append(list, p);
     */
    /* order of the interpolation for the spectral shift of frames */
    /*
  p = cpl_parameter_new_value("sinfoni.stacked.shift_frame_order",
                  CPL_TYPE_INT,
                              "Shift Frame Order",
                              "sinfoni.stacked",
                               2);

  cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-shft_ord");
  cpl_parameterlist_append(list, p);
     */

    /*
  --------------------------------------------------------------------------
  WarpFix
  --------------------------------------------------------------------------
     */
    p = cpl_parameter_new_value("sinfoni.stacked.warpfix_ind",
                    CPL_TYPE_BOOL,
                    "Warp Fix Index: ",
                    "sinfoni.stacked",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-warpfix_ind");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_enum("sinfoni.stacked.warpfix_kernel",
                    CPL_TYPE_STRING,
                    "Warpfix kernel: ",
                    "sinfoni.stacked",
                    "tanh",
                    6,"tanh","sinc","sinc2",
                    "lanczos","hamming","hann");

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-warpfix_kernel");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.stacked.qc_thresh_min",
                    CPL_TYPE_INT,
                    "qc_thresh_min",
                    "sinfoni.stack",
                    0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-qc_thresh_min");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.stacked.qc_thresh_max",
                    CPL_TYPE_INT,
                    "qc_thresh_max",
                    "sinfoni.stack",
                    49000);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-qc_thresh_max");
    cpl_parameterlist_append(list, p);



    /* indicates if sky raw frame should be subtracted (TRUE) or not (FALSE) */
    p = cpl_parameter_new_value("sinfoni.stacked.sub_raw_sky",
                    CPL_TYPE_BOOL,
                    "indicates if the raw sky frame should be "
                    "subtracted (TRUE) or (FALSE)",
                    "sinfoni.stacked",
                    TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"stack-sub_raw_sky");
    cpl_parameterlist_append(list, p);

}
/**@}*/
