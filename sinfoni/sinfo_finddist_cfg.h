/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_wavecal_cfg.h
   Author         :    Juergen Schreiber
   Created on    :    September 2001
   Description    :    wavecal_ini definitions + handling prototypes

 ---------------------------------------------------------------------------*/
#ifndef SINFO_FINDDIST_CFG_H
#define SINFO_FINDDIST_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_globals.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  Wavelength calibration blackboard container

  This structure holds all information related to the wavelength calibration
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct finddist_config {
/*-------General---------*/
        char inFrame[FILE_NAME_SZ] ; /* input emission line frame */
        char lineList[FILE_NAME_SZ] ; /* input wavelength and intensity 
                                         line list */
        char outName[FILE_NAME_SZ] ; /* output name of resulting fits 
                                        wavelength map */
        char nsFrame[FILE_NAME_SZ] ; /* input north-south frame */
        char mask[FILE_NAME_SZ] ;    /* input north-south frame */
        char fitsname[FILE_NAME_SZ] ;  
        char drs_setup[FILE_NAME_SZ] ;  
/*------ FindLines ------*/
        /* estimated central wavelength of the image */
        float guessBeginWavelength ;
        /* estimated linear dispersion of emission line frame */
        float guessDispersion1 ;
        /* estimated square dispersion of emission line frame */
        float guessDispersion2 ;
        /* minimal difference of mean and sinfo_median column intensity */
        float mindiff ;
        /* half width of a box within which the line must sit */
        int halfWidth ;
        /* sigma of Gaussian of artificial model spectra */
        float sigma ; 

/*------ WaveCalib ------*/
        /* guess value for fwhm of emission lines */ 
        float fwhm ;
        /* minimum amplitude of a line to be fitted */
        float minAmplitude ;
        /* maximal residual value for a valid fit */
        float maxResidual ;
        /* # of polynomial coefficients used for the dispersion relation */
        int nrDispCoefficients ;
        /* # of polynomial coefficients used for the fit of the dispersion 
             coefficients */
        int nrCoefCoefficients ;
        /* minimal factor of the standard deviation of the fit coefficients */
        float sigmaFactor ;
        /* number of slitlets */
        int    nslitlets ;
        /* minimal pixel distance of slitlets in spectral direction */
        int    pixeldist ;
        /* allowed pixel position tolerance between estimated and 
           fitted line position */
        float  pixel_tolerance  ;

/*------ WaveMap ------*/
    /* magnifying factor for FFT */
    int magFactor ;

/*------ FitSlits ------*/
    /* pixel length of the row box within which the fit of the 
           slitlet positions is carried out*/
    int boxLength ;
        /* lower row position for the estimate fit */
    int loPos ;
        /* upper row position for the estimate fit */
    int hiPos ;
    /* float box half width in spectral direction */
        float yBox ;
        /* maximal tolerable difference to the expected slitlet positions */
        float diffTol ;
/*------ NorthSouthTest ------*/
        /* number of slitlets */
        int nslits ;      
        /* pixel half width of a box within which the spatial 
           profile is fitted by a Gaussian */
        int nshalfWidth ;
        /* first guess of the fwhm of the Gaussian fit function */
        float nsfwhm ;
        /* minimum amplitude above which the fit is carried out */
        float minDiff ;
        /* estimated average distance of spectra */
        float estimated_dist ;
        /* maximal pixel tolerance of the slitlet distances */
        float devtol ;

  /*----qg log --------*/
  int qc_thresh_min;
  int qc_thresh_max;


} finddist_config ;
/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name    sinfo_wave_cfg_create()
   @memo    allocate memory for a wave_cfg struct
   @return  pointer to allocated base wave_cfg structure
   @note    only the main (base) structure is allocated
*/

finddist_config * 
sinfo_finddist_cfg_create(void);

/**
   @name    sinfo_wave_cfg_destroy()
   @memo    deallocate all memory associated with a wave_config data structure
   @param   wave_config to deallocate
   @return  void
*/
void 
sinfo_finddist_cfg_destroy(finddist_config * jc);

#endif
