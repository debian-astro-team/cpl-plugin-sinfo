#ifndef SINFO_NEW_LAMP_FLATS_H
#define SINFO_NEW_LAMP_FLATS_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_lamp_flats.h,v 1.9 2007-09-21 14:13:43 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  17/09/03  created
*/

/************************************************************************
 * sinfo_new_lamp_flats.h
 * this step handles stacks of lamp flat fields, 
 *  o it takes a clean mean,
 *  o subtracts the off- from the on-frames, 
 *  o corrects for static bad pixels and normalizes for a master flat field. 
 *  o It distinguishes the spectrally dithered frames and 
 *  o treats them the same way. 
 *  o It can also generate a static bad pixel mask if wished.
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h> 
#include "sinfo_msg.h"

/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Function     :       sinfo_new_lamp_flats()
   In           :       ini_file: file name of according .ini file
   Out          :       integer (0 if it worked, -1 if it doesn't) 
   Job          :
       
 * this step handles stacks of lamp flat fields, 
 *  o it takes a clean mean,
 *  o subtracts the off- from the on-frames, 
 *  o corrects for static bad pixels and normalizes for a master flat field. 
 *  o It distinguishes the spectrally dithered frames and 
 *  o treats them the same way. 
 *  o It can also generate a static bad pixel mask if wished.

 ---------------------------------------------------------------------------*/
int 
sinfo_new_lamp_flats (const char* plugin_id,
                      cpl_parameterlist* config, 
                      cpl_frameset* sof,
                      cpl_frameset* ref_set) ;


#endif 

/*--------------------------------------------------------------------------*/
