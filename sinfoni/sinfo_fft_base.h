/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   File name     :    sinfo_sinfo_fft_base.h
   Author         :    N. Devillard
   Created on    :    October 1999
   Description    :    base FFT routines
 *--------------------------------------------------------------------------*/
/*
    $Id: sinfo_fft_base.h,v 1.6 2007-06-06 07:10:45 amodigli Exp $
    $Author: amodigli $
    $Date: 2007-06-06 07:10:45 $
    $Revision: 1.6 $
*/
#ifndef SINFO_FFT_BASE_H
#define SINFO_FFT_BASE_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <math.h>
#include "cpl.h"
#include "sinfo_local_types.h"
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
#define FFT_FORWARD         1
#define FFT_INVERSE        -1
/*---------------------------------------------------------------------------
                              Function codes
 ---------------------------------------------------------------------------*/
/**
  @name     sinfo_fftn
  @memo     N-dimensional FFT.
  @param    data        N-dimensional data set stored in 1d.
  @param    nn          Dimensions of the set.
  @param    ndim        How many dimensions this set has.
  @param    isign       Transform direction.
  @return   void
  @doc      This routine is a public domain FFT. See extract of Usenet article
  below. Found on {\tt  http://www.tu-chemnitz.de/~arndt/joerg.html}.
 
  \begin{verbatim}
  From: alee@tybalt.caltech.edu (Andrew Lee)
  Newsgroups: comp.sources.misc
  Subject: N-dimensional, Radix 2 FFT Routine
  Date: 17 Jul 87 22:26:29 GMT
  Approved: allbery@ncoast.UUCP
  X-Archive: comp.sources.misc/8707/48
 
  [..]
  Now for the usage (finally):
  data[] is the array of complex numbers to be transformed,
  nn[] is the array giving the dimensions (I mean size) of the array,
  ndim is the number of dimensions of the array, and
  isign is +1 for a forward transform, and -1 for an inverse transform.
 
  data[] and nn[] are stored in the "natural" order for C:
  nn[0] gives the number of elements along the leftmost index,
  nn[ndim - 1] gives the number of elements along the rightmost index, and
  data should be declared along the lines of
  struct (f)complex data[nn[0], nn[1], ..., nn[ndim - 1]]
 
  Additional notes: The routine does NO NORMALIZATION, so if you do a
  forward, and then an inverse transform on an array, the result will
  be identical to the original array MULTIPLIED BY THE NUMBER OF
  ELEMENTS IN THE ARRAY.  Also, of course, the dimensions of data[]
  must all be powers of 2.
  \end{verbatim}
 
 */

void
sinfo_fftn(
    dcomplex data[],
    unsigned nn[],
    int ndim, 
    int isign);

/**
  @name     sinfo_is_power_of_2
  @memo     Find if a given integer is a power of 2.
  @param    p   Integer to check.
  @return   The corresponding power of 2, or -1.
  @doc      If the given number is a power of 2, the power is returned.
            Otherwise -1 is returned.
 
  Example:
  \begin{verbatim}
  sinfo_is_power_of_2(1024) returns 10
  sinfo_is_power_of_2(1023) returns -1
  \end{verbatim}
 */



#endif
/*--------------------------------------------------------------------------*/

