/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   File name     :    sinfo_lamp_cfg.h
   Author         :    Juergen Schreiber
   Created on    :    March 2002
   Description    :    lamp_ini definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_LAMP_CFG_H
#define SINFO_LAMP_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <cpl.h>
#include "sinfo_globals.h"
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  prepare lamp spectrum blackboard container

  This structure holds all information related to the halogen lamp 
  spectrum handling
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct lamp_config {
/*-------General---------*/
        char inFrame[FILE_NAME_SZ] ; /* file name of the input halogen 
                                        lamp frame */
        char wavemapim[FILE_NAME_SZ] ; /* file name of the wavelength map */
        char outName[FILE_NAME_SZ] ; /* output name of resulting fits frame */
/*------ Resampling ------*/
        /* number of coefficients for the polynomial interpolation 
          (order + 1) */
        int ncoeffs ;
        /* number of rows in the resulting resampled image = 
           number of spectral bins */
        int nrows ;
/*------ Extractspectrum ------*/
        /* percentage of rejected low intensity pixels */
        float loReject ;      
        /* percentage of rejected high intensity pixels */
        float hiReject ;
        /* conversion factor of detector counts per intensity unit */
        float countsToIntensity ;
} lamp_config ;

/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name    sinfo_lamp_cfg_create()
   @memo    allocate memory for a lamp_cfg struct
   @return  pointer to allocated base lamp_cfg structure
   @note    only the main (base) structure is allocated
 */

lamp_config * 
sinfo_lamp_cfg_create(void);

/**
   @name    sinfo_lamp_cfg_destroy()
   @memo    deallocate all memory associated with a lamp_config data structure
   @param  sc lamp_config to deallocate
   @return  void
*/

void 
sinfo_lamp_cfg_destroy(lamp_config * sc);

#endif
