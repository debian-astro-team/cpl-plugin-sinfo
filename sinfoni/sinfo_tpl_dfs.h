/* $Id: sinfo_tpl_dfs.h,v 1.3 2008-07-09 09:53:23 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2008-07-09 09:53:23 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_TPL_DFS_H
#define SINFO_TPL_DFS_H

/*-----------------------------------------------------------------------------
                                   Define
 -----------------------------------------------------------------------------*/

/* Define here the PRO.CATG keywords */
#define SI_UTL_IMA_ARITH_PROIMA               "PRO_IMA"
#define SINFO_RAW                           "RAW"
#define SINFO_CAL                           "CAL"

/* Define here the DO.CATG keywords */
#define SI_UTL_IMA_ARITH_IMA1                 "IMA1"
#define SI_UTL_IMA_ARITH_IMA2                 "IMA2"
#define SI_UTL_CUBE2IMA_CUBE                  "CUBE"
#define SI_UTL_CUBE2SPECTRUM_CUBE             "CUBE"
#define SI_UTL_CUBE_ARITH_CUBE                "CUBE"
#define SI_UTL_CUBE_ARITH_SPECTRUM            "SPECTRUM"
#define SI_UTL_CUBE2IMA_PROIMA                "PRO_IMA"
#define SI_UTL_CUBE2SPECTRUM_PROIMA           "PRO_IMA"
#define SI_UTL_CUBE_ARITH_PROCUBE             "PRO_CUBE_COADD"
#define SI_UTL_CUBE_COMBINE_PROCUBE           "PRO_CUBE"
#define SI_UTL_CUBE_COMBINE_PROMASK           "PRO_BP_MAP_CUBE_COADD"
#define SI_UTL_SPECTRUM_ARITH_PROSPECTRUM     "PRO_SPECTRUM"
#define SI_UTL_SPECTRUM_ARITH_SPECTRUM        "SPECTRUM"
#define SI_UTL_SPECTRUM_WAVELENGTH_SHIFT_PROSPECTRUM     "PRO_SPECTRUM"
#define SI_UTL_SPECTRUM_WAVELENGTH_SHIFT_SPECTRUM        "SPECTRUM"
#define SI_UTL_SPECTRUM_DIVIDE_BY_BLACKBODY_PROSPECTRUM     "PRO_SPECTRUM"
#define SI_UTL_SPECTRUM_DIVIDE_BY_BLACKBODY_SPECTRUM        "SPECTRUM"
#define SI_UTL_GENLOOKUP_CUBE1                "CUBE1"
#define SI_UTL_GENLOOKUP_CUBE2                "CUBE2"
#define SI_UTL_GENLOOKUP_PROIMA1              "PRO_IMA1"
#define SI_UTL_GENLOOKUP_PROIMA2              "PRO_IMA2"
#define SI_UTL_GENLOOKUP_PROIMA3              "PRO_IMA3"
#define SI_UTL_GENLOOKUP_PROIMA4              "PRO_IMA4"
#define SI_SKYMAP_CUBE                        "PRO_CUBE"
#define SI_SKYMAP_PROIMA                      "PRO_IMA"


/*-----------------------------------------------------------------------------
                                Functions prototypes
 -----------------------------------------------------------------------------*/

int sinfo_dfs_set_groups(cpl_frameset *) ;

#endif
