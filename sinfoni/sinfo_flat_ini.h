/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_flat_ini.h
   Author       :    Juergen Schreiber
   Created on   :    Mar 04, 2002
   Description  :    preparing flatfield ini file handling for SPIFFI
 ---------------------------------------------------------------------------*/
#ifndef SINFO_FLAT_INI_H
#define SINFO_FLAT_INI_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include "sinfo_flat_cfg.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/
#define FRAME_ON     1 /* object frames */
#define FRAME_OFF    0 /* off frames, that means sky frames or 
                          calibration frames with lamp switched off */
#define FRAME_POS1   2 /* frames exposed with grating position 1 */
#define FRAME_POS2   3 /* frames exposed with dithered grating position 2 */
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/

/**
  @name     parse_flat_ini_file
  @memo     Parse a ini_name.ini file and create a blackboard.
  @param    ini_name    Name of the ASCII file to parse.
  @return   1 newly allocated flat_config blackboard structure.
  @doc      The requested ini file is parsed and a blackboard object is 
            created, then updated accordingly. Returns NULL in case of error.
 */

flat_config * 
parse_flat_ini_file(char * ini_name) ;

#endif
