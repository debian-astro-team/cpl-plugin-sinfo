/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
#ifndef SINFO_FIT_H
#define SINFO_FIT_H

#include <cpl.h>

void
sinfo_fit_amoeba(double**p,
       double y[],
       int ndim,
       double ftol,
       double (*funk)(double[]),
       int* nfunk);

cpl_error_code sinfo_fit_lm(const cpl_matrix *x,
                            const cpl_matrix *sigma_x,
                const cpl_vector *y,
                            const cpl_vector *sigma_y,
                      cpl_vector *a,
                            const int ia[],
                int    (*f)(const double x[],
                                        const double a[],
                          double *result),
                 int (*dfda)(const double x[],
                                             const double a[],
                         double result[]),
                 double *mse,
                 double *red_chisq,
                 cpl_matrix **covariance);


#endif
