/* $Id: sinfo_wcal_functions.h,v 1.2 2006-10-22 14:12:28 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2006-10-22 14:12:28 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */
#ifndef SINFO_WCAL_FUNCTIONS_H
#define SINFO_WCAL_FUNCTIONS_H

struct qc_wcal_ {

  double avg_on; 
  double std_on;
  double avg_of; 
  double std_of;
  double avg_di; 
  double std_di;
  double max_on;
  double max_of;
  double max_di;
  double nsat_on;
  double noise_on;
  double noise_of;
  double flux_on;
  double    nsat;

};
 


typedef struct qc_wcal_ qc_wcal;

/* extern struct qc_wcal qc_wcal_par; */
#include "sinfo_functions.h"
#include <cpl.h>
#include <sinfo_globals.h>
#include <sinfo_spiffi_types.h>
#include <sinfo_wavecal_cfg.h>
#include "sinfo_msg.h"
/* ---------------------------------------------------------------------- 
   group of frames
---------------------------------------------------------------------- */
CPL_BEGIN_DECLS
int sinfo_dumpTblToFitParams ( FitParams ** params, char * filename );
int sinfo_det_ncounts(cpl_frameset* raw, int thresh_max, qc_wcal* qc);
qc_wcal* sinfo_qc_wcal_new(void);
void sinfo_qc_wcal_delete(qc_wcal** qc);
CPL_END_DECLS

#endif
