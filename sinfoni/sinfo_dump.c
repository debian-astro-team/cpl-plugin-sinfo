/*                                                                           *
 *   This file is part of the SINFONI   Pipeline                             *
 *   Copyright (C) 2002,2003 European Southern Observatory                   *
 *                                                                           *
 *   This library is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the Free Software             *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA    *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2012-09-21 10:55:19 $
 * $Revision: 1.9 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/**@{*/

/*----------------------------------------------------------------------------*/
/**
 @defgroup sinfo_dump  Print CPL objects

 Functions that enables dumping (using CPL's messaging system) some
 otherwise non-dumpable CPL objects
 */
/*----------------------------------------------------------------------------*/

#include <sinfo_dump.h>
#include <sinfo_utils.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <cpl.h>

/*----------------------------------------------------------------*/
/** 
 * @brief Convert a CPL type to a string
 * @param t  Type to convert
 * @return A textual representation of @em  t.
 */
/*----------------------------------------------------------------*/
const char *
sinfo_tostring_cpl_type(cpl_type t)
{

    /* Note that CPL_TYPE_STRING is shorthand
     for CPL_TYPE_CHAR | CPL_TYPE_FLAG_ARRAY . */

    if (!(t & CPL_TYPE_FLAG_ARRAY))
        switch (t & (~CPL_TYPE_FLAG_ARRAY))
            {
        case CPL_TYPE_CHAR:
            return "char";
        case CPL_TYPE_UCHAR:
            return "uchar";
        case CPL_TYPE_BOOL:
            return "boolean";
        case CPL_TYPE_INT:
            return "int";
        case CPL_TYPE_UINT:
            return "uint";
        case CPL_TYPE_LONG:
            return "long";
        case CPL_TYPE_ULONG:
            return "ulong";
        case CPL_TYPE_FLOAT:
            return "float";
        case CPL_TYPE_DOUBLE:
            return "double";
        case CPL_TYPE_POINTER:
            return "pointer";
            /* not in CPL3.0: case CPL_TYPE_COMPLEX:    return "complex"; */
        case CPL_TYPE_INVALID:
            return "invalid";
        default:
            return "unrecognized type";
            }
    else
        switch (t & (~CPL_TYPE_FLAG_ARRAY))
            {
        case CPL_TYPE_CHAR:
            return "string (char array)";
        case CPL_TYPE_UCHAR:
            return "uchar array";
        case CPL_TYPE_BOOL:
            return "boolean array";
        case CPL_TYPE_INT:
            return "int array";
        case CPL_TYPE_UINT:
            return "uint array";
        case CPL_TYPE_LONG:
            return "long array";
        case CPL_TYPE_ULONG:
            return "ulong array";
        case CPL_TYPE_FLOAT:
            return "float array";
        case CPL_TYPE_DOUBLE:
            return "double array";
        case CPL_TYPE_POINTER:
            return "pointer array";
            /* not in CPL3.0: case CPL_TYPE_COMPLEX:    return "complex array"; */
        case CPL_TYPE_INVALID:
            return "invalid (array)";
        default:
            return "unrecognized type";
            }
}

/**@}*/
