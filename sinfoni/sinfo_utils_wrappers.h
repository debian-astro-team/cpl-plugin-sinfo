/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

#ifndef SINFO_UTILS_WRAPPERS_H
#define SINFO_UTILS_WRAPPERS_H

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*-----------------------------------------------------------------------------
                    Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

/*-----------------------------------------------------------------------------
                             Defines
 -----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------
                                   Prototypes
 -----------------------------------------------------------------------------*/
void sinfo_free_float_array(float ***a, const int n) ;

cpl_table *
sinfo_extract_table_rows(const cpl_table *t, const char *column,
             cpl_table_select_operator operator, double value);
int
sinfo_select_table_rows(cpl_table *t,  const char *column, 
            cpl_table_select_operator operator, double value);

cpl_error_code sinfo_sort_table_1(cpl_table *t, const char *column1,
                 cpl_boolean reverse1);
cpl_error_code sinfo_sort_table_2(cpl_table *t, const char *column1, 
                 const char *column2, cpl_boolean reverse1, 
                 cpl_boolean reverse2);

void sinfoni_free_vector(cpl_vector **v);
void sinfo_free_array(cpl_array **i);
void sinfo_free_apertures(cpl_apertures **a);
void sinfo_free_image(cpl_image **i);
void sinfo_free_mask(cpl_mask **m);
void sinfo_free_imagelist(cpl_imagelist **i);
void sinfo_free_table(cpl_table **t);
void sinfo_free_propertylist(cpl_propertylist **p);
void sinfo_free_polynomial(cpl_polynomial **p);

void sinfo_unwrap_matrix(cpl_matrix **m);
void sinfo_unwrap_vector(cpl_vector **v);
void sinfo_unwrap_bivector_vectors(cpl_bivector **b);

void sinfo_free_frameset(cpl_frameset **f);
void sinfo_free_frame(cpl_frame **f);
void sinfo_free_int(int **i);
void sinfo_free_float(float **i);
void sinfo_free_double(double **i);
void sinfo_free_array_imagelist(cpl_imagelist ***a);
void sinfo_free_array_image(cpl_image ***a);
void sinfo_free_image_array(cpl_image ***a,const int n);
/* similar are also defined with same name in svd.h */
void sinfoni_free_matrix(cpl_matrix **m);
void sinfo_free_my_vector(cpl_vector **v);
void sinfo_free_bivector(cpl_bivector **bv);

#endif
