/* $Id: sinfo_psf_config.c,v 1.5 2012-03-03 09:50:08 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-03 09:50:08 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *   Psf Frames Data Reduction Parameter Initialization        *
 ****************************************************************/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "sinfo_psf_config.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter PSF parameters definition & initialization
 *
 * TBD
 */

/**
 @name  sinfo_psf_config_add
 @brief PSF parameters definition & initialization
 @param list pointer to cpl_parameterlist
 @return void
 */

void
sinfo_psf_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;
    if (!list) {
        return;
    }

    /* Input file name */

    p = cpl_parameter_new_value("sinfoni.psf.switch", CPL_TYPE_BOOL,
                    "Switch to activate PSF-related "
                                    "(Strehl/Encircled energy) computation",
                    "sinfoni.psf", TRUE);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI, "psf-switch");
    cpl_parameterlist_append(list, p);

}
/**@}*/
