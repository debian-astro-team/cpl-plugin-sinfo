/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_badnorm_ini_by_cpl.h
   Author       :   Andrea Modigliani
   Created on   :   Jun 16, 2004
   Description  :   parse cpl input for the search for static bad pixels

 ---------------------------------------------------------------------------*/
#ifndef SINFO_BADSKY_INI_BY_CPL_H
#define SINFO_BADSKY_INI_BY_CPL_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_badsky_cfg.h"
#include "sinfo_msg.h"
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/

badsky_config * 
sinfo_parse_cpl_input_badsky(cpl_parameterlist * cpl_cfg, 
                             cpl_frameset* sof, 
                             cpl_frameset** raw);

void 
sinfo_badsky_free(badsky_config * cfg);

#endif
