/* $Id: sinfo_tpl_utils.h,v 1.2 2007-08-20 10:03:49 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2007-08-20 10:03:49 $
 * $Revision: 1.2 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_TPL_UTILS_H
#define SINFO_TPL_UTILS_H

/*-----------------------------------------------------------------------------
                                                                Includes
 -----------------------------------------------------------------------------*/

#include <cpl.h>

typedef enum _SINFO_BAND_ {
    SINFO_BAND_J,
    SINFO_BAND_JS,
    SINFO_BAND_JBLOCK,
    SINFO_BAND_H,
    SINFO_BAND_K,
    SINFO_BAND_KS,
    SINFO_BAND_L,
    SINFO_BAND_M,
    SINFO_BAND_LP,
    SINFO_BAND_MP,
    SINFO_BAND_Z,
    SINFO_BAND_SZ,
    SINFO_BAND_SH,
    SINFO_BAND_SK,
    SINFO_BAND_SL,
    SINFO_BAND_UNKNOWN
} sinfo_band ;

/*-----------------------------------------------------------------------------
                                                                Prototypes
 -----------------------------------------------------------------------------*/

const char * sinfo_get_license(void) ;
cpl_frameset * sinfo_extract_frameset(const cpl_frameset *, const char *) ;
const char * sinfo_std_band_name(sinfo_band) ;
#endif
