/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_flat_ini.h
   Author       :   Andrea Modigliani
   Created on   :   Mar 04, 2004
   Description  :   flatfield cpl_input handling for SPIFFI
 ---------------------------------------------------------------------------*/
#ifndef SINFO_FLAT_INI_BY_CPL_H
#define SINFO_FLAT_INI_BY_CPL_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_flat_cfg.h"
#include "sinfo_msg.h"
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/
#define FRAME_ON     1 /* object frames */
#define FRAME_OFF    0 /* off frames, that means sky frames or 
                          calibration frames with lamp switched off */
#define FRAME_POS1   2 /* frames exposed with grating position 1 */
#define FRAME_POS2   3 /* frames exposed with dithered grating position 2 */
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/

/**
  @name     sinfo_parse_cpl_input_flat
  @memo     Parse input frames & parameters file and create a blackboard.
  @param    cpl_cfg pointer to parameterlist
  @param    sof pointer to input set of frames
  @param    raw pointer to input set of raw frames
  @return   1 newly allocated flat_config blackboard structure.
  @doc

  The requested ini file is parsed and a blackboard object is created, then
  updated accordingly. Returns NULL in case of error.
 */
flat_config * 
sinfo_parse_cpl_input_flat(cpl_parameterlist * cpl_cfg, 
               cpl_frameset* sof, 
                           cpl_frameset** raw) ;

/**
@name sinfo_flat_free
@memo free flat_config structure
@param cfg pointer to flat_config structure
@return void
*/
void sinfo_flat_free(flat_config ** cfg);

#endif
