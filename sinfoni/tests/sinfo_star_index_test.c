/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2013-08-15 12:07:25 $
 * $Revision: 1.8 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <string.h>
#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_utils_wrappers.h>
#include <sinfo_star_index.h>

const char* SINFO_STAR_BUG_REPORT = "amodigli@eso.org";
const double EPSILON = 1E-5;
const double DATA_1		= 209384.23;
const double DATA_2		= 378455.4398;
const char* COL_NAME_DATA = "DATA";
const char* FILE_NAME_FITS = "tmp_star_catalog.fits";
const char* FILE_NAME_FITS2 = "tmp_star_catalog2.fits";

struct _STAR_COORD_
{
	double RA;
	double DEC;
	double data_value;
	const char* STAR_NAME;

};
typedef struct _STAR_COORD_ STAR_COORD;

const STAR_COORD coords[] = {
		{300.982347, 18.34958, 958.229384, "STAR_UNO"},
		{30.234434, 180.23409, 2817.23847, "STAR_DUO"},
};

const STAR_COORD coords2[] = {
		{305.34892, 53.4319, 9545.234, "STAR_TRE"},
		{32.234434, 180.23409, 21348.78653, "STAR_QUA"},
		{2.234434, 45.2239, 8746.1236, "STAR_CIN"},
};

static void create_empty_test()
{
	star_index* pindex = star_index_create();
	cpl_test(pindex);
	star_index_delete(pindex);
        return;
}

static cpl_table* create_data_table(double data_value)
{
	cpl_table* retval = cpl_table_new(1);
	cpl_test(retval);
	cpl_test(CPL_ERROR_NONE == cpl_table_new_column(retval, COL_NAME_DATA, CPL_TYPE_DOUBLE));
	cpl_table_set_double(retval, COL_NAME_DATA, 0, data_value);
	return retval;
}

static void check_index(star_index* pindex, const STAR_COORD* pcoords, int size)
{
	int i = 0;
	for (i = 0; i < size; i++)
	{
		cpl_table* presult_data = star_index_get(pindex, pcoords[i].RA, pcoords[i].DEC, EPSILON, EPSILON, NULL);
		cpl_test(presult_data);
		int inull = 0;
		double result_data = cpl_table_get_double(presult_data, COL_NAME_DATA, 0, &inull);
		cpl_test_abs(result_data, pcoords[i].data_value, EPSILON);
		cpl_table_delete(presult_data);
	}
        return;
}

static void fill_index(star_index* pindex, const STAR_COORD* pcoords, int size)
{
	int i = 0;
	for (i = 0; i < size; i++ )
	{
		cpl_table* pdata = create_data_table(pcoords[i].data_value);
		star_index_add(pindex, pcoords[i].RA, pcoords[i].DEC, pcoords[i].STAR_NAME, pdata);
		cpl_table_delete(pdata);
	};
        return;
}

static void add_data_test()
{
	star_index* pindex = star_index_create();
	cpl_test(pindex);
       
	fill_index (pindex,coords, sizeof(coords) / sizeof(coords[0]));
	check_index(pindex,coords, sizeof(coords) / sizeof(coords[0]));
	star_index_save(pindex, FILE_NAME_FITS);
	star_index_delete(pindex);
        return;
}

static void load_file_test()
{
	star_index* pindex = star_index_load(FILE_NAME_FITS);
	check_index(pindex, coords, sizeof(coords) / sizeof(coords[0]));
	fill_index(pindex,coords2, sizeof(coords2) / sizeof(coords2[0]));
	check_index(pindex,coords2, sizeof(coords2) / sizeof(coords2[0]));
	check_index(pindex, coords, sizeof(coords) / sizeof(coords[0]));
	// remove from cache and main
	star_index_remove_by_name(pindex, coords[0].STAR_NAME);
	star_index_remove_by_name(pindex, coords2[0].STAR_NAME);
	check_index(pindex,coords2 + 1, sizeof(coords2) / sizeof(coords2[0]) - 1);
	check_index(pindex, coords + 1, sizeof(coords) / sizeof(coords[0]) - 1);
	// save, load and check again
	star_index_save(pindex, FILE_NAME_FITS2);
	star_index* pindex2 = star_index_load(FILE_NAME_FITS2);
	cpl_test(pindex2);
	check_index(pindex2,coords2 + 1, sizeof(coords2) / sizeof(coords2[0]) - 1);
	check_index(pindex2, coords + 1, sizeof(coords) / sizeof(coords[0]) - 1);
	star_index_delete(pindex);
	star_index_delete(pindex2);
        return;
}

static void create()
{
	star_index* pindex = star_index_load("star_index.fits");
	star_index_delete(pindex);
	return;
	cpl_table* pdata = cpl_table_load("gd71_stisnic_002.fits",1,0);
	if (pdata)
	{
		star_index_add(pindex, 88.115075, 15.88654, "gd71_stisnic", pdata);
		cpl_table_delete(pdata);
		star_index_save(pindex, "11star_index_v0_1.fits");
	}
	else
	{
		printf ("cannot load ref data\n");
		cpl_error_reset();
	}
	star_index_delete(pindex);
        return;
}

int main(void)
{
	cpl_test_init(SINFO_STAR_BUG_REPORT, CPL_MSG_WARNING);
	printf("test 1 \n");
	create_empty_test();
	printf("test 2 \n");
	add_data_test();
	printf("test 3 \n");
	load_file_test();
	printf("test 4 \n");
	create();
	return cpl_test_end(0);
}
