/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2013-08-15 12:05:49 $
 * $Revision: 1.8 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <math.h>
#include <string.h>

#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_spiffi_types.h>
#include <sinfo_utilities.h>
#include <sinfo_wave_calibration.h>
#include <getopt.h>

const char* SINFO_STAR_BUG_REPORT = "amodigli@eso.org";

int main(int argc, char * const argv[])
{
    cpl_test_init(SINFO_STAR_BUG_REPORT, CPL_MSG_WARNING);
    int print_usage = 0;
    const char* file_fit_coef = NULL ;
    const char* file_arc_line = NULL ;
    const char* file_arc_off = NULL ;
    const char* file_arc_on = NULL ;
    sinfo_msg_warning("ck1");
    if (argc < 5)
    {
        print_usage = 1;
    }
    else
    {
        file_fit_coef = argv[1];
        file_arc_line = argv[2];
        file_arc_off  = argv[3];
        file_arc_on  = argv[4];
    }
    if (file_arc_line == NULL)
    {
        print_usage = 1;
    }
    if(print_usage)
    {
        fprintf(stderr, "usage: %s fit_coef_frame.fits line_list.fits file_arc_off.fits file_arc_on.fits\n", argv[0]);
        return 0;
    }
    /*
	cpl_propertylist* harc=NULL;
	cpl_propertylist* htab=NULL;
	harc=cpl_propertylist_load(file_arc,0);
	htab=cpl_propertylist_load(file_tab,1);
     */
    sinfo_msg_warning("ck2");
    cpl_table* arc_line_tab=NULL;
    check_nomsg(arc_line_tab=cpl_table_load(file_arc_line,1,0));
    sinfo_msg_warning("ck3");
    cpl_table* fit_coef_tab=NULL;
    check_nomsg(fit_coef_tab=cpl_table_load(file_fit_coef,1,0));
    sinfo_msg("ck4");

    int nx=2048;
    int ny=2048;
    cpl_image* ima_arc=NULL;
    check_nomsg(ima_arc=cpl_image_new(nx,ny,CPL_TYPE_FLOAT));
    cpl_image* ima_off=NULL;
    ima_off=cpl_image_load(file_arc_off,CPL_TYPE_FLOAT,0,0);
    cpl_propertylist* plist=NULL;
    check_nomsg(plist=cpl_propertylist_load(file_arc_on,0));

    //cpl_table_dump_structure(line_tab,stdout);
    float* wave   = cpl_table_get_data_float(arc_line_tab,"wave");
    float* intens=NULL;
    cpl_type type=cpl_table_get_column_type(arc_line_tab,"int");
    if(type==CPL_TYPE_INT) {
        check_nomsg(cpl_table_cast_column(arc_line_tab,"int", "fint",CPL_TYPE_FLOAT));
        check_nomsg(intens = cpl_table_get_data_float(arc_line_tab,"fint"));
    } else {
        check_nomsg(intens = cpl_table_get_data_float(arc_line_tab,"int"));
    }
    sinfo_msg_warning("ck3");
    int n_lines = cpl_table_get_nrow(arc_line_tab);
    int n_coeff = cpl_table_get_nrow(fit_coef_tab);
    double* coeff0=cpl_table_get_data_double(fit_coef_tab,"coeff0");
    double* coeff1=cpl_table_get_data_double(fit_coef_tab,"coeff1");
    double* coeff2=cpl_table_get_data_double(fit_coef_tab,"coeff2");
    double* coeff3=cpl_table_get_data_double(fit_coef_tab,"coeff3");

    //double bkg=100;

    double x=0;
    double x2=0;
    double x3=0;

    double a=0;
    double arg=0;
    //float begin_wave=1.25;  //j
    //float begin_wave=1.65;  //h
    //float begin_wave=1.95;  //hk
    //float begin_wave=2.198; //k (corrected value)
    //float begin_wave=2.20;  //k
    double resol_j=2500; //j
    double resol_h=3200; //h
    double resol_k=5650; //k
    double resol_hk=2150; //hk
    double resol=resol_j;
    double factor=1.25e-3;
    double wavec=CENTRALLAMBDA_J;
    double FWHM=2.1*wavec/resol;
    double y0=(ny-1)*0.5;
    double sigma_scale=0.2;  /* 0.1 */
    double sigma=sigma_scale*FWHM/(2*sqrt(2*CPL_MATH_LN2));
    double inv_sigma=1/sigma;
    double flux_scale=4; /* 10 */
    double wave_scale=1.e-3;//1.e-3;
    double a0=flux_scale*inv_sigma/sqrt(CPL_MATH_2_SQRTPI);
    double wav=0;
    float* pima = cpl_image_get_data_float(ima_arc);

    sinfo_msg_warning("FWHM=%g",FWHM);
    for(int j=0;j<ny;j++) {
        double y=j;
        for(int i=1;i<n_coeff;i++) {
            x = (y - y0);
            x2=x*x;
            x3=x2*x;
            wav=coeff0[i]+coeff1[i]*x+coeff2[i]*x2+coeff3[i]*x3;
            //sinfo_msg_warning("wav=%g",wav);
            //sinfo_msg_warning("wave[0]=%g",wave[0]);
            for(int k=0;k<n_lines;k++) {
                a=intens[k];
                //sinfo_msg_warning("wdiff=%g",wav-wave[k]*wave_scale);
                arg=(wav-wave[k]*wave_scale)*inv_sigma;
                //sinfo_msg_warning("arg=%g",arg);
                arg *= arg;
                //sinfo_msg_warning("arg2=%g",arg);
                //sinfo_msg_warning("a=%g",a);
                pima[j*nx+i] += factor*a*a0*exp(-0.5*arg);
                //sinfo_msg_warning("ima=%g",pima[j*nx+i]);
            }
        }
    }
    //cpl_image_add_scalar(ima_arc,bkg);
    cpl_image_add(ima_arc,ima_off);
    cpl_image_save(ima_arc,"ima_arc.fits",CPL_BPP_IEEE_FLOAT,plist,CPL_IO_DEFAULT);
    sinfo_msg_warning("ck5");

    cleanup:
    cpl_propertylist_delete(plist);
    cpl_image_delete(ima_arc);
    cpl_image_delete(ima_off);
    cpl_table_delete(arc_line_tab);
    cpl_table_delete(fit_coef_tab);

    cpl_test_end(0);
    return 0;
}
