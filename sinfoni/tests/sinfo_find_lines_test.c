/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2013-08-15 12:05:49 $
 * $Revision: 1.8 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
//#include <unistd.h>
#include <string.h>

#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_utilities.h>
#include <sinfo_wave_calibration.h>
#include <getopt.h>

const char* SINFO_STAR_BUG_REPORT = "amodigli@eso.org";

int main(int argc, char * const argv[])
{
    cpl_test_init(SINFO_STAR_BUG_REPORT, CPL_MSG_WARNING);
    int print_usage = 0;
    const char* file_arc = NULL ;
    const char* file_tab = NULL ;

    int opt = 0;

    if (argc < 3)
    {
        print_usage = 1;
    }
    else
    {
        file_arc = argv[1];
        file_tab = argv[2];
    }
    if (file_tab == NULL)
    {
        print_usage = 1;
    }
    if(print_usage)
    {
        fprintf(stderr, "usage: %s arc_on-off_frame.fits line_list.fits arc_off_frame,fits\n", argv[0]);
        return 0;
    }
    /*
	cpl_propertylist* harc=NULL;
	cpl_propertylist* htab=NULL;
	harc=cpl_propertylist_load(file_arc,0);
	htab=cpl_propertylist_load(file_tab,1);
     */
    cpl_table* line_tab=NULL;
    line_tab=cpl_table_load(file_tab,1,0);
    cpl_image* ima_arc=NULL;


    double mean = 0;
    double median = 0;
    double stdev =0;

    ima_arc=cpl_image_load(file_arc,CPL_TYPE_FLOAT,0,0);
    sinfo_msg_warning("ima size: %lld %lld",
                      cpl_image_get_size_x(ima_arc),
                      cpl_image_get_size_y(ima_arc));

    mean=cpl_image_get_mean(ima_arc);
    median=cpl_image_get_median(ima_arc);
    stdev=cpl_image_get_stdev(ima_arc);
    //sinfo_msg_warning("On frame mean=%g median=%g stdev=%g",mean,median,stdev);
    //cpl_table_dump_structure(line_tab,stdout);
    float* wave   = cpl_table_get_data_float(line_tab,"wave");
    float* intens=NULL;
    cpl_type type=cpl_table_get_column_type(line_tab,"int");
    if(type==CPL_TYPE_INT) {
        check_nomsg(cpl_table_cast_column(line_tab,"int", "fint",CPL_TYPE_FLOAT));
        check_nomsg(intens = cpl_table_get_data_float(line_tab,"fint"));
    } else {
        check_nomsg(intens = cpl_table_get_data_float(line_tab,"int"));
    }
    int n_lines = cpl_table_get_nrow(line_tab);

    int lx = cpl_image_get_size_x(ima_arc);
    int** row_clean = sinfo_new_2Dintarray(lx, n_lines);
    float** wavelength_clean = sinfo_new_2Dfloatarray(lx, n_lines);

    //sinfo_msg_warning("n_lines=%d lx=%d",n_lines,lx);
    /* J band
        float begin_wave=1.25;
        float dispersion1=-0.000150058;
        float dispersion2=5.88707e-10;
        float min_diff=1;
        int half_width=2;
        float sigma=1;
     */

    /* H band
        float begin_wave=1.95;
        float dispersion1=-0.000500143;
        float dispersion2=4.56728e-09;
        float min_diff=1;
        int half_width=2;
        float sigma=1;
     */

    /* HK band
        float begin_wave=1.95;
        float dispersion1=-0.000500143;
        float dispersion2=4.56728e-09;
        float min_diff=1;
        int half_width=2;
        float sigma=1;
     */

    /* K band */
    float begin_wave=2.198;
    float dispersion1=-0.000251669;
    float dispersion2=1.77136e-09;
    float min_diff=1;
    int half_width=2;
    float sigma=1;

    int* n_found_lines    = sinfo_new_intarray(lx);
    int* sum_pointer      = sinfo_new_intarray(1) ;
    //sinfo_msg_warning("ok1");
    sinfo_new_find_lines(ima_arc, wave, intens, n_lines, row_clean,
                         wavelength_clean, begin_wave, dispersion1, dispersion2,
                         min_diff, half_width, n_found_lines, sigma,
                         sum_pointer);
    sinfo_msg_warning("n_found_line=%d",n_found_lines[10]);

    cleanup:
    cpl_image_delete(ima_arc);
    cpl_table_delete(line_tab);
    sinfo_new_destroy_2Dintarray (&row_clean, lx);
    sinfo_new_destroy_2Dfloatarray ( &wavelength_clean, lx );
    sinfo_new_destroy_intarray(&sum_pointer );
    sinfo_new_destroy_intarray(&n_found_lines );
    cpl_test_end(0);
    return 0;
}
