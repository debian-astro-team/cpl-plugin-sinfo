/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-09-18 06:53:36 $
 * $Revision: 1.5 $
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gsl/gsl_spline.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_bspline.h>

#include <math.h>

#include <stdio.h>
#include <string.h>
#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_globals.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_utils_wrappers.h>
#include <sinfo_tpl_utils.h>
#include <sinfo_pfits.h>
#include <sinfo_eff_resp.h>
#include <sinfo_key_names.h>
#define MODULE_ID "SINFO_RESPONSE"





#define SINFO_CRPIX2 "CRPIX2"
#define SINFO_CRVAL2 "CRVAL2"

#define SINFO_CDELT2 "CDELT2"
#define SINFO_CTYPE2 "CTYPE2"
#define SINFO_CUNIT2 "CUNIT1"
#define SINFO_CUNIT2_C "Wavelength units"

#define SINFO_CD11 "CD1_1"
#define SINFO_CD12 "CD1_2"
#define SINFO_CD21 "CD2_1"
#define SINFO_CD22 "CD2_2"



#define SINFO_QC_TELLCORR_OPTEXTID "ESO QC TELLCORR OPTEXTID"
#define SINFO_QC_TELLCORR_RATAVG "ESO QC TELLCORR RATAVG"
#define SINFO_EXPTIME "EXPTIME"

#define SINFO_QC_TELLCORR_RATAVG "ESO QC TELLCORR RATAVG"
#define SINFO_QC_TELLCORR_RATRMS "ESO QC TELLCORR RATRMS"
#define SINFO_ATMOS_EXT         "ATMOS_EXT"

#define SINFO_ATMOS_EXT_LIST_COLNAME_WAVELENGTH "LAMBDA"



#define SINFO_TABLE_LOAD( TABLE, NAME) \
  check( TABLE = cpl_table_load( NAME, 1, 0),\
    "Can't load %s FITS table", NAME)
typedef struct {
  double wguess ;    /* Reference line wavelength position */
  double range_wmin; /* minimum of wavelength box for line fit */
  double range_wmax; /* maximum of wavelength box for line fit */
  double ipol_wmin_max; /* maximum lower sub-range wavelength value used to fit line slope */
  double ipol_wmax_min; /* minimum upper sub-range wavelength value used to fit line slope */
  double ipol_hbox;     /* half box where polynomial  fit is performed */

} sinfo_rv_ref_wave_param ;


/*
  Structure to return gaussian fit results
*/
typedef struct {
  double peakpos,
    sigma,
    area,
    offset,
    mse ;
} SINFO_GAUSSIAN_FIT ;



typedef struct {
  int size ;
  cpl_propertylist * header ;
  double * lambda ;
  double * K ;
} sinfo_atmos_ext_list ;




HIGH_ABS_REGION UvbTellFitRegions[] = {
  {0., 0.}
  } ;
HIGH_ABS_REGION VisTellFitRegions[] = {
  {535.4, 536.2}, // 535.8 +/- 0.4
  {551.9, 552.7}, // 552.3
  {555.8, 556.6}, // 556.2
  {563.6, 564.4}, // 564.0
  {603.6, 604.4}, // 604.0 
  {616.6, 617.4}, // 617.0
  {638.6, 639.4}, // 639.0
  {682.9, 683.7}, // 683.3
  {707.3, 708.1}, // 707.7 
  {747.1, 747.9}, // 747.5
  {753.9, 754.7}, // 754.3
  {777.7, 778.3}, // 777.9
  {781.1, 781.9}, // 781.5
  {789.2, 790.0}, // 789.6
  {799.3, 800.1}, // 799.7
  {809.6, 810.4}, // 810.0
  {841.0, 841.8}, // 841.4
  {850.1, 850.9}, // 850.5
  {864.6, 865.4}, // 865.0
  {874.6, 875.4}, // 875.0
  {887.9, 888.7}, // 888.3
  {923.4, 924.2}, // 923.8
  {971.2, 972.0}, // 971.6
  {979.4, 980.2}, // 979.8
  {995.2, 996.0}, // 995.6
  {1006.1, 1006.9}, // 1006.5
  {1017.6, 1018.4}, // 1018.0
  {0., 0.}
  } ;


HIGH_ABS_REGION NirTellFitRegions[] = {
  {1039.0, 1041.0}, // 1040.0 nm
  {1059.0, 1061.0}, // 1060.0 nm
  {1190.0, 1192.0}, // 1191.0 nm
  {1228.0, 1230.0}, // 1229.0 nm
  {1243.0, 1245.0}, // 1244.0 nm
  {1552.0, 1554.0}, // 1553.0 nm
  {1589.0, 1591.0}, // 1590.0 nm
  {1619.0, 1621.0}, // 1620.0 nm
  {1658.0, 1660.0}, // 1659.0 nm
  {1691.0, 1693.0}, // 1692.0 nm
  {1702.0, 1704.0}, // 1703.0 nm
  {1709.0, 1711.0}, // 1710.0 nm
  {1717.0, 1719.0}, // 1718.0 nm
  {1990.0, 1992.0}, // 1991.0 nm
  {2036.0, 2038.0}, // 2037.0 nm
  {2102.0, 2103.0}, // 2102.0 nm
  {2112.0, 2114.0}, // 2113.0 nm
  {2125.0, 2127.0}, // 2126.0 nm
  {2132.0, 2134.0}, // 2133.0 nm
  {2137.0, 2139.0}, // 2138.0 nm
  {2141.0, 2143.0}, // 2142.0 nm
  {2144.0, 2146.0}, // 2145.0 nm
  {2156.0, 2158.0}, // 2157.0 nm
  {2163.0, 2165.0}, // 2164.0 nm
  {2186.0, 2188.0}, // 2187.0 nm
  {2209.0, 2211.0}, // 2210.0 nm
  {2239.0, 2241.0}, // 2240.0 nm
  {2259.0, 2261.0}, // 2260.0 nm
  {2279.0, 2281.0}, // 2280.0 nm
  {2306.0, 2308.0}, // 2307.0 nm
  {2324.0, 2326.0}, // 2325.0 nm
  {2342.0, 2344.0}, // 2343.0 nm
  {2379.0, 2381.0}, // 2380.0 nm
  {2429.0, 2431.0}, // 2430.0 nm
  {2475.0, 2477.0}, // 2476.0 nm
  {0., 0.}
} ;


HIGH_ABS_REGION AllTellFitRegions[] = {
  {535.4, 536.2}, // 535.8 +/- 0.4
  {551.9, 552.7}, // 552.3
  {555.8, 556.6}, // 556.2
  {563.6, 564.4}, // 564.0
  {603.6, 604.4}, // 604.0
  {616.6, 617.4}, // 617.0
  {638.6, 639.4}, // 639.0
  {682.9, 683.7}, // 683.3
  {707.3, 708.1}, // 707.7
  {747.1, 747.9}, // 747.5
  {753.9, 754.7}, // 754.3
  {777.7, 778.3}, // 777.9
  {781.1, 781.9}, // 781.5
  {789.2, 790.0}, // 789.6
  {799.3, 800.1}, // 799.7
  {809.6, 810.4}, // 810.0
  {841.0, 841.8}, // 841.4
  {850.1, 850.9}, // 850.5
  {864.6, 865.4}, // 865.0
  {874.6, 875.4}, // 875.0
  {887.9, 888.7}, // 888.3
  {923.4, 924.2}, // 923.8
  {971.2, 972.0}, // 971.6
  {979.4, 980.2}, // 979.8
  {995.2, 996.0}, // 995.6
  {1006.1, 1006.9}, // 1006.5
  {1017.6, 1018.4}, // 1018.0
  {1039.0, 1041.0}, // 1040.0 nm
  {1059.0, 1061.0}, // 1060.0 nm
  {1190.0, 1192.0}, // 1191.0 nm
  {1228.0, 1230.0}, // 1229.0 nm
  {1243.0, 1245.0}, // 1244.0 nm
  {1552.0, 1554.0}, // 1553.0 nm
  {1589.0, 1591.0}, // 1590.0 nm
  {1619.0, 1621.0}, // 1620.0 nm
  {1658.0, 1660.0}, // 1659.0 nm
  {1691.0, 1693.0}, // 1692.0 nm
  {1702.0, 1704.0}, // 1703.0 nm
  {1709.0, 1711.0}, // 1710.0 nm
  {1717.0, 1719.0}, // 1718.0 nm
  {1990.0, 1992.0}, // 1991.0 nm
  {2036.0, 2038.0}, // 2037.0 nm
  {2102.0, 2103.0}, // 2102.0 nm
  {2112.0, 2114.0}, // 2113.0 nm
  {2125.0, 2127.0}, // 2126.0 nm
  {2132.0, 2134.0}, // 2133.0 nm
  {2137.0, 2139.0}, // 2138.0 nm
  {2141.0, 2143.0}, // 2142.0 nm
  {2144.0, 2146.0}, // 2145.0 nm
  {2156.0, 2158.0}, // 2157.0 nm
  {2163.0, 2165.0}, // 2164.0 nm
  {2186.0, 2188.0}, // 2187.0 nm
  {0., 0.}
} ;

HIGH_ABS_REGION NirJTellFitRegions[] = {
  //{1096.0, 1097.0}, // 1040.0 nm
  //{1101.0, 1103.0}, // 1060.0 nm
  {1104.0, 1106.0}, // 1060.0 nm
  {1108.0, 1110.0}, // 1060.0 nm
  {1142.0, 1143.0}, // 1060.0 nm
  {1160.0, 1162.0}, // 1060.0 nm
  {1180.0, 1182.0}, // 1060.0 nm
  {1190.0, 1192.0}, // 1191.0 nm
  {1210.0, 1212.0}, // 1229.0 nm
  {1220.0, 1222.0}, // 1229.0 nm
  {1228.0, 1230.0}, // 1229.0 nm
  {1230.0, 1232.0}, // 1229.0 nm
  {1243.0, 1245.0}, // 1244.0 nm
  {1300.0, 1302.0}, // 1244.0 nm
  {1310.0, 1312.0}, // 1244.0 nm
  {1315.0, 1325.0}, // 1244.0 nm
  {1330.0, 1335.0}, // 1244.0 nm
//  {1340.0, 1343.0}, // 1244.0 nm
//  {1394.0, 1398.0}, // 1244.0 nm
  {0., 0.}
} ;


HIGH_ABS_REGION NirHTellFitRegions[] = {
//  {1039.0, 1041.0}, // 1040.0 nm
//  {1059.0, 1061.0}, // 1060.0 nm
//  {1190.0, 1192.0}, // 1191.0 nm
//  {1228.0, 1230.0}, // 1229.0 nm
//  {1243.0, 1245.0}, // 1244.0 nm
  {1552.0, 1554.0}, // 1553.0 nm
  {1589.0, 1591.0}, // 1590.0 nm
  {1619.0, 1621.0}, // 1620.0 nm
  {1658.0, 1660.0}, // 1659.0 nm
  {1691.0, 1693.0}, // 1692.0 nm
  {1702.0, 1704.0}, // 1703.0 nm
  {1709.0, 1711.0}, // 1710.0 nm
  {1717.0, 1719.0}, // 1718.0 nm
//  {1990.0, 1992.0}, // 1991.0 nm
  {0., 0.}
} ;

HIGH_ABS_REGION NirKTellFitRegions[] = {
  {1990.0, 1992.0}, // 1991.0 nm
  {2036.0, 2038.0}, // 2037.0 nm
  {2102.0, 2103.0}, // 2102.0 nm
  {2112.0, 2114.0}, // 2113.0 nm
  {2125.0, 2127.0}, // 2126.0 nm
  {2132.0, 2134.0}, // 2133.0 nm
  {2137.0, 2139.0}, // 2138.0 nm
  {2141.0, 2143.0}, // 2142.0 nm
  {2144.0, 2146.0}, // 2145.0 nm
  {2156.0, 2158.0}, // 2157.0 nm
  {2163.0, 2165.0}, // 2164.0 nm
  {2186.0, 2188.0}, // 2187.0 nm
  {2209.0, 2211.0}, // 2210.0 nm
  {2239.0, 2241.0}, // 2240.0 nm
  {2259.0, 2261.0}, // 2260.0 nm
  {2279.0, 2281.0}, // 2280.0 nm
  {2306.0, 2308.0}, // 2307.0 nm
  {2324.0, 2326.0}, // 2325.0 nm
  {2342.0, 2344.0}, // 2343.0 nm
  {2379.0, 2381.0}, // 2380.0 nm
  {2429.0, 2431.0}, // 2430.0 nm
//  {2475.0, 2477.0}, // 2476.0 nm
  {0., 0.}
} ;

HIGH_ABS_REGION NirHKTellFitRegions[] = {
//  {1039.0, 1041.0}, // 1040.0 nm
//  {1059.0, 1061.0}, // 1060.0 nm
//  {1190.0, 1192.0}, // 1191.0 nm
//  {1228.0, 1230.0}, // 1229.0 nm
//  {1243.0, 1245.0}, // 1244.0 nm
  {1552.0, 1554.0}, // 1553.0 nm
  {1589.0, 1591.0}, // 1590.0 nm
  {1619.0, 1621.0}, // 1620.0 nm
  {1658.0, 1660.0}, // 1659.0 nm
  {1691.0, 1693.0}, // 1692.0 nm
  {1702.0, 1704.0}, // 1703.0 nm
  {1709.0, 1711.0}, // 1710.0 nm
  {1717.0, 1719.0}, // 1718.0 nm
  {1990.0, 1992.0}, // 1991.0 nm
  {2036.0, 2038.0}, // 2037.0 nm
  {2102.0, 2103.0}, // 2102.0 nm
  {2112.0, 2114.0}, // 2113.0 nm
  {2125.0, 2127.0}, // 2126.0 nm
  {2132.0, 2134.0}, // 2133.0 nm
  {2137.0, 2139.0}, // 2138.0 nm
  {2141.0, 2143.0}, // 2142.0 nm
  {2144.0, 2146.0}, // 2145.0 nm
  {2156.0, 2158.0}, // 2157.0 nm
  {2163.0, 2165.0}, // 2164.0 nm
  {2186.0, 2188.0}, // 2187.0 nm
  {2209.0, 2211.0}, // 2210.0 nm
  {2239.0, 2241.0}, // 2240.0 nm
  {2259.0, 2261.0}, // 2260.0 nm
  {2279.0, 2281.0}, // 2280.0 nm
  {2306.0, 2308.0}, // 2307.0 nm
  {2324.0, 2326.0}, // 2325.0 nm
  {2342.0, 2344.0}, // 2343.0 nm
  {2379.0, 2381.0}, // 2380.0 nm
  {2429.0, 2431.0}, // 2430.0 nm
  {2475.0, 2477.0}, // 2476.0 nm
  {0., 0.}
} ;


HIGH_ABS_REGION UvbTellComputeResidRegions[] = {
  {0., 0.}
  } ;

HIGH_ABS_REGION VisTellComputeResidRegions[] = {
  {691., 695.},
  {715., 734.},
  {766., 770.},
  {893., 904.},
  {943., 983.},
  {0., 0.}
  } ;

HIGH_ABS_REGION NirTellComputeResidRegions[] = {
  {1105.0, 1266.0},
  {1300.0, 1343.0},
  {1468.0, 1780.0},
  {1940.0, 1994.0},
  {2030.0, 2046.0},
  {2080.0, 2100.0},
  {0., 0.}
} ;



HIGH_ABS_REGION AllTellComputeResidRegions[] = {
  {691., 695.},
  {715., 734.},
  {766., 770.},
  {893., 904.},
  {943., 983.},
  {1105.0, 1266.0},
  {1300.0, 1343.0},
  {1468.0, 1780.0},
  {1940.0, 1994.0},
  {2030.0, 2046.0},
  {2080.0, 2100.0},
  {0., 0.}
} ;

HIGH_ABS_REGION NirJTellComputeResidRegions[] = {
  {1105.0, 1266.0},
  {1300.0, 1343.0},
  {1468.0, 1780.0},
  {0., 0.}
} ;

HIGH_ABS_REGION NirHTellComputeResidRegions[] = {
  {1105.0, 1266.0},
  {1300.0, 1343.0},
  {1468.0, 1780.0},
  {1940.0, 1994.0},
  {0., 0.}
} ;

HIGH_ABS_REGION NirKTellComputeResidRegions[] = {
  {1940.0, 1994.0},
  {2030.0, 2046.0},
  {2080.0, 2100.0},
  {0., 0.}
} ;

HIGH_ABS_REGION NirHKTellComputeResidRegions[] = {
  {1105.0, 1266.0},
  {1300.0, 1343.0},
  {1468.0, 1780.0},
  {1940.0, 1994.0},
  {2030.0, 2046.0},
  {2080.0, 2100.0},
  {0., 0.}
} ;

/*
static double
sinfo_std_star_spectra_correlate(hdrl_spectrum1D * obs_s, sinfo_rv_ref_wave_param* w)

{
    cpl_vector* vec_wave_obs = NULL;
    cpl_vector* vec_flux_obs = NULL;
    cpl_vector* vec_slope_obs=NULL;
    cpl_vector* correl=NULL;

    const double wmin=w->range_wmin;
    const double wmax=w->range_wmax;

    sinfo_std_star_spectra_to_vector_range(obs_std_star_list,wmin,wmax,&vec_wave_obs,&vec_flux_obs);

    double wmin_max=w->ipol_wmin_max;
    double wmax_min=w->ipol_wmax_min;

    vec_slope_obs=sinfo_vector_fit_slope(vec_wave_obs,vec_flux_obs,wmin_max,wmax_min,2);

    cpl_vector_divide(vec_flux_obs,vec_slope_obs);
    cpl_vector_delete(vec_slope_obs);

    cpl_vector_add_scalar(vec_flux_obs,2);

    double wave_range=w->ipol_hbox;
    cpl_vector* xfit_obs=NULL;
    cpl_vector* yfit_obs=NULL;

    double* pxfit=NULL;
    double* pyfit=NULL;

    sinfo_select_line_core(vec_wave_obs,vec_flux_obs,w->wguess,wave_range,&xfit_obs,&yfit_obs);

    cpl_polynomial* poly_fit_obs=NULL;

    double mse=0;
    double* pindex=NULL;
    poly_fit_obs=sinfo_polynomial_fit_1d_create(xfit_obs,yfit_obs,4,&mse);

    cpl_vector* yfit_pol_obs=NULL;
    cpl_vector* yfit_index=NULL;
    cpl_size size_x_obs=cpl_vector_get_size(xfit_obs);
    yfit_pol_obs=cpl_vector_new(size_x_obs);
    yfit_index=cpl_vector_new(size_x_obs);
    check_nomsg(pyfit=cpl_vector_get_data(yfit_pol_obs));
    check_nomsg(pxfit=cpl_vector_get_data(xfit_obs));
    check_nomsg(pindex=cpl_vector_get_data(yfit_index));

    for(cpl_size i=0;i<size_x_obs;i++) {
        pyfit[i]=cpl_polynomial_eval_1d(poly_fit_obs,pxfit[i],NULL);
        pindex[i]=i;
    }
    sinfo_free_polynomial(&poly_fit_obs);

    double xmin=0;
    sinfo_sort_double_pairs(pyfit,pindex,size_x_obs);

    xmin=pxfit[(int)pindex[0]];

    cpl_vector_delete(yfit_index);

    const double peakpos_obs=xmin;
    const double peakpos_ref=w->wguess;


    const double wave_shift=peakpos_obs-peakpos_ref;

    cleanup:

    cpl_vector_delete(yfit_pol_obs);
    cpl_vector_delete(xfit_obs);
    cpl_vector_delete(yfit_obs);
    cpl_vector_delete(vec_wave_obs);
    cpl_vector_delete(vec_flux_obs);
    cpl_vector_delete(correl);

    return wave_shift;
}
*/

#define SINFO_STAR_FLUX_LIST_COLNAME_FLUX "FLUX"
#define SINFO_STAR_FLUX_LIST_COLNAME_WAVELENGTH "LAMBDA"

typedef struct {
  int size ;
  cpl_propertylist * header ;
  double * lambda ;
  double * flux ;
} sinfo_star_flux_list ;




typedef enum {false,true} bool;




#define SYNTAX "Test response computation algorithml\n"\
  "usage : \n"\
  "./sinfo_response_test STD_STAR_SPECTRA REF_STD_STAR_CATALOG EXT_TAB RESP_FIT_POINTS TELL_MOD\n"\
  "STD_STAR_SPECTRA     => File product with extracted std star spectrum\n"\
  "REF_STD_STAR_CATALOG => Table with std star FITS spectra\n"\
  "EXT_TAB              => Table with atm extinction coeffs\n"\
  "RESP_FIT_POINTS      => Table with response fit points\n"\
  "TELL_MOD             => Table with telluric model\n"

/* Get the value of a keyword */


#define SINFO_SEEING_START "ESO TEL AMBI FWHM START"
#define SINFO_SEEING_END "ESO TEL AMBI FWHM END"










static const double STAR_MATCH_DEPSILON=0.0166667; /*60 arcsecs */

static cpl_error_code
sinfo_table_save(cpl_table* t, cpl_propertylist* ph, cpl_propertylist* xh, 
                 const char* fname, const int ext) {

  if (ext==0) {
    cpl_table_save(t,ph,xh,fname,CPL_IO_DEFAULT);
  } else {
    cpl_table_save(t,ph,xh,fname,CPL_IO_EXTEND);
  }

  return cpl_error_get_code();

}
static void sinfo_rv_ref_wave_param_destroy(sinfo_rv_ref_wave_param * p){

  cpl_free(p);

  return ;
}




/*---------------------------------------------------------------------------*/
/** 
 * @brief Get size of spectrum
 * 
 * @param[in] s spectrum structure
 *
 * @return the size of flux data in spectrum
 */
/*---------------------------------------------------------------------------*/
static int sinfo_spectrum_get_size( sinfo_spectrum* s)
{
  int res=0;

  SINFO_ASSURE_NOT_NULL( s);

  res = s->size;

  cleanup:
    return res;
}


/**
 * @brief Get minimum lambda of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return minimum lambda spectrum
 */
/*---------------------------------------------------------------------------*/
static double sinfo_spectrum_get_lambda_min( sinfo_spectrum* s)
{
  double res=0.0;

  SINFO_ASSURE_NOT_NULL( s);

  res = s->lambda_min;

  cleanup:
    return res;
}

/*---------------------------------------------------------------------------*/
/**
 * @brief Get maximum lambda of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return maximum lambda of spectrum
 */
/*---------------------------------------------------------------------------*/
static double sinfo_spectrum_get_lambda_max( sinfo_spectrum* s)
{
  double res=0.0;

  SINFO_ASSURE_NOT_NULL( s);

  res = s->lambda_max;

  cleanup:
    return res;
}



/*---------------------------------------------------------------------------*/
/**
 * @brief Get bin in lambda of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return bin in lambda of spectrum
 */
/*---------------------------------------------------------------------------*/
static double sinfo_spectrum_get_lambda_step( sinfo_spectrum* s)
{
  double res=0.0;

  SINFO_ASSURE_NOT_NULL( s);

  res = s->lambda_step;

  cleanup:
    return res;
}




static void sinfo_star_flux_list_free( sinfo_star_flux_list ** list )
{
  if ( list != NULL && *list != NULL ) {
    sinfo_free_propertylist(&(*list)->header);
    cpl_free( (*list)->lambda ) ;
    cpl_free( (*list)->flux ) ;
    //cpl_free( (*list)->header ) ;
    cpl_free( *list ) ;
     *list = NULL ;
  }


  return ;
}


static cpl_frame * sinfo_star_flux_list_save( sinfo_star_flux_list * list,
                     const char * filename, const char * tag )
{
  cpl_table * table = NULL ;
  cpl_frame * result = NULL ;
  int size, i ;
  double * plambda, * pflux ;

  SINFO_ASSURE_NOT_NULL(list);
  SINFO_ASSURE_NOT_NULL(filename);

  /* create a table */
  table = cpl_table_new( 2 );

  /* create column names */
  cpl_table_new_column(table, SINFO_STAR_FLUX_LIST_COLNAME_WAVELENGTH,
      CPL_TYPE_FLOAT);
  cpl_table_new_column(table, SINFO_STAR_FLUX_LIST_COLNAME_FLUX,
      CPL_TYPE_FLOAT);

  size = list->size ;
  plambda = list->lambda ;
  pflux = list->flux ;

  cpl_table_set_size(table, size);

  /* insert data */
  for( i = 0 ; i<size ; i++, plambda++, pflux++ ) {
    float value ;
    //sinfo_msg_warning("ck1 i=%d",i);
    value = *plambda ;
    cpl_table_set_float(table, SINFO_STAR_FLUX_LIST_COLNAME_WAVELENGTH,
                   i, value );
    value = *pflux ;
    cpl_table_set_float(table, SINFO_STAR_FLUX_LIST_COLNAME_FLUX,
                   i, value );
  }

  /* create fits file */
  cpl_table_save( table, list->header, NULL, filename, CPL_IO_DEFAULT);
  //xsh_add_temporary_file( filename );

  /* Create the frame */
  result=sinfo_frame_product( filename, tag,
                  CPL_FRAME_TYPE_TABLE,
                  CPL_FRAME_GROUP_PRODUCT,
                  CPL_FRAME_LEVEL_TEMPORARY);

  sinfo_msg( "Star Flux Frame Saved" ) ;
 cleanup:
  sinfo_free_table( &table);
  return result ;
}
/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD1_1 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd11(cpl_propertylist * plist, double value)
{
  check (cpl_propertylist_update_double (plist, SINFO_CD11, value),
             "Error writing keyword '%s'", SINFO_CD11);
  cleanup:
    return;
}

/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD1_2 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd12(cpl_propertylist * plist, double value)
{
  check (cpl_propertylist_update_double (plist, SINFO_CD12, value),
             "Error writing keyword '%s'", SINFO_CD12);
  cleanup:
    return;
}

/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD2_1 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd21(cpl_propertylist * plist, double value)
{
  check (cpl_propertylist_update_double (plist, SINFO_CD21, value),
             "Error writing keyword '%s'", SINFO_CD21);
  cleanup:
    return;
}

/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD2_2 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd22(cpl_propertylist * plist, double value)
{
  check (cpl_propertylist_update_double (plist, SINFO_CD22, value),
             "Error writing keyword '%s'", SINFO_CD22);
  cleanup:
    return;
}

static cpl_error_code
sinfo_pfits_set_cd_matrix(cpl_propertylist* header,
                        const double cdelt1,
                        const double cdelt2) {

  sinfo_pfits_set_cd11(header, cdelt1);
  sinfo_pfits_set_cd12(header, 0);
  sinfo_pfits_set_cd21(header, 0);
  sinfo_pfits_set_cd22(header, cdelt2);

  return cpl_error_get_code();
}


/**
@brief set hdu keys
@param plist input propertylist
@param hduclas1 hdu classification1 value
@param hduclas2 hdu classification2 value
@param hduclas3 hdu classification3 value
@return updated propertylist
*/
static cpl_error_code
sinfo_plist_set_extra_common_keys(cpl_propertylist* plist)
{

    cpl_propertylist_append_string(plist,"HDUCLASS", "ESO") ;
    cpl_propertylist_set_comment(plist,"HDUCLASS","hdu classification") ;

    cpl_propertylist_append_string(plist,"HDUDOC", "DICD") ;
    cpl_propertylist_set_comment(plist,"HDUDOC","hdu reference document") ;

    cpl_propertylist_append_string(plist,"HDUVERS", "DICD V6.0") ;
    cpl_propertylist_set_comment(plist,"HDUVERS","hdu reference document version") ;

    return cpl_error_get_code();
}


/**
@brief set hdu keys
@param plist input propertylist
@param hduclas1 hdu classification1 value
@param hduclas2 hdu classification2 value
@param hduclas3 hdu classification3 value
@param scidata  name of data extension
@param errdata  name of errs extension
@param qualdata name of qual extension
@param type type of extension: 0 data, 1 errs, 2 qual
@return updated propertylist
*/
cpl_error_code
sinfo_plist_set_extra_keys(cpl_propertylist* plist,
                         const char* hduclas1,
                         const char* hduclas2,
                         const char* hduclas3,
                         const char* scidata,
                         const char* errdata,
                         const char* qualdata,
                         const int type)
{

    //SINFO_ASSURE_NOT_ILLEGAL_MSG(type<3,"type  < 3");
    SINFO_ASSURE_NOT_ILLEGAL_MSG(type>=0,"type  > 0");

    sinfo_plist_set_extra_common_keys(plist);

    cpl_propertylist_append_string(plist,"HDUCLAS1",hduclas1) ;
    cpl_propertylist_set_comment(plist,"HDUCLAS1","hdu format classification") ;

    cpl_propertylist_append_string(plist,"HDUCLAS2",hduclas2) ;
    cpl_propertylist_set_comment(plist,"HDUCLAS2","hdu type classification") ;

    if(type == 0) {
        if(!cpl_propertylist_has(plist,"EXTNAME")) {
            cpl_propertylist_append_string(plist,"EXTNAME",scidata) ;
            cpl_propertylist_set_comment(plist,"EXTNAME","name of data extension") ;
        }
    }

    if(type!=0) {
        if(!cpl_propertylist_has(plist,"HDUCLAS3")) {
            cpl_propertylist_append_string(plist,"HDUCLAS3",hduclas3) ;
            cpl_propertylist_set_comment(plist,"HDUCLAS3","hdu info classification") ;
        }
        if(!cpl_propertylist_has(plist,"SCIDATA")) {
            cpl_propertylist_append_string(plist,"SCIDATA",scidata) ;
            cpl_propertylist_set_comment(plist,"SCIDATA","name of data extension") ;
        }
    }

    if(type!=1) {
        if(!cpl_propertylist_has(plist,"ERRDATA")) {
            cpl_propertylist_append_string(plist,"ERRDATA",errdata) ;
            cpl_propertylist_set_comment(plist,"ERRDATA","name of errs extension") ;
        }
    }

    if(type!=2) {
        if(!cpl_propertylist_has(plist,"QUALDATA")) {
            cpl_propertylist_append_string(plist,"QUALDATA",qualdata) ;
            cpl_propertylist_set_comment(plist,"QUALDATA","name of qual extension") ;
        }
    }
    cleanup:
    return cpl_error_get_code();
}



static sinfo_atmos_ext_list * sinfo_atmos_ext_list_create( int size )
{
  sinfo_atmos_ext_list * result = NULL ;

  /* Create the list */
  SINFO_CALLOC( result, sinfo_atmos_ext_list, 1 ) ;
  result->size = size ;
  SINFO_CALLOC( result->lambda, double, size ) ;
  SINFO_CALLOC( result->K, double, size ) ;

 cleanup:
  return result ;
}


static double sinfo_pfits_get_seeing_start (const cpl_propertylist * plist)
{
  double ret = 0;

  sinfo_get_property_value( plist, SINFO_SEEING_START, CPL_TYPE_DOUBLE,&ret);
cleanup:
    return ret;
}



/*---------------------------------------------------------------------------*/
/**
 * @brief Load a 1D spectrum  structure
 *
 * @param[in] s1d_frame the 1D spectrum frame
 * @return the spectrum structure
 */
/*---------------------------------------------------------------------------*/
static sinfo_spectrum* sinfo_spectrum_load( cpl_frame* s1d_frame)
{
  sinfo_spectrum* result = NULL;
  const char *s1dname = NULL;
  int naxis;
  sinfo_msg_warning("pk1  s1d_frame=%p",s1d_frame);
  SINFO_ASSURE_NOT_NULL( s1d_frame);
  sinfo_msg_warning("pk2 fname=%s",cpl_frame_get_filename(s1d_frame));
  sinfo_msg_warning("pk2 dim=%lld",cpl_frame_get_nextensions(s1d_frame));
  SINFO_ASSURE_NOT_ILLEGAL(cpl_frame_get_nextensions(s1d_frame) == 2);

  check_nomsg( s1dname = cpl_frame_get_filename( s1d_frame));
  sinfo_msg_warning("pk2");
  SINFO_CALLOC(result, sinfo_spectrum,1);

  check_nomsg( result->flux_header = cpl_propertylist_load( s1dname,0));
  check_nomsg( result->errs_header = cpl_propertylist_load( s1dname,1));
  check_nomsg( result->qual_header = cpl_propertylist_load( s1dname,2));
  sinfo_msg_warning("pk3");
  check_nomsg( result->lambda_min = sinfo_pfits_get_crval1( result->flux_header));
  sinfo_msg_warning("pk3");
  check_nomsg( result->lambda_step = sinfo_pfits_get_cdelt1( result->flux_header));
  sinfo_msg_warning("pk3");
  check_nomsg( result->size =  sinfo_pfits_get_naxis1( result->flux_header));
  sinfo_msg_warning("pk3");
  check_nomsg( result->size_lambda =  sinfo_pfits_get_naxis1( result->flux_header));
  sinfo_msg_warning("pk3");
  result->lambda_max = result->lambda_min+
    result->lambda_step*(result->size-1);
  sinfo_msg_warning("pk3");
  check_nomsg( naxis = sinfo_pfits_get_naxis( result->flux_header));
  sinfo_msg_warning("pk4");
  if (naxis > 1){
    check_nomsg( result->slit_min = sinfo_pfits_get_crval2( result->flux_header));
    check_nomsg( result->slit_step = sinfo_pfits_get_cdelt2( result->flux_header));
    check_nomsg( result->size_slit = sinfo_pfits_get_naxis2( result->flux_header));
    result->slit_max = result->slit_min+
      result->slit_step*(result->size_slit-1);

    check_nomsg( result->flux = cpl_image_load( s1dname, CPL_TYPE_DOUBLE, 0, 0));
    check_nomsg( result->errs = cpl_image_load( s1dname, CPL_TYPE_DOUBLE, 0, 1));
    check_nomsg( result->qual = cpl_image_load( s1dname, CPL_TYPE_INT, 0, 2));
  }
  else{
    double *flux_data = NULL;
    double *errs_data = NULL;
    int *qual_data = NULL;
    int i;
    cpl_vector *flux = NULL;
    cpl_vector *errs = NULL;
    cpl_vector *qual = NULL;
    int size = 0;

    check_nomsg( flux = cpl_vector_load( s1dname, 0));
    check_nomsg( size = cpl_vector_get_size( flux));
    check_nomsg( errs = cpl_vector_load( s1dname, 1));
    check_nomsg( qual = cpl_vector_load( s1dname, 2));
    check_nomsg( flux_data = cpl_vector_get_data( flux));
    check_nomsg( result->flux = cpl_image_wrap_double( size, 1 , flux_data));
    cpl_vector_unwrap( flux);

    check_nomsg( errs_data = cpl_vector_get_data( errs));
    check_nomsg( result->errs = cpl_image_wrap_double( size, 1, errs_data));

    check_nomsg( result->qual = cpl_image_new ( size, 1, CPL_TYPE_INT));
    check_nomsg( qual_data = cpl_image_get_data_int( result->qual));
    for(i=0; i< size; i++){
      check_nomsg( qual_data[i] = (int)cpl_vector_get(qual,i));
    }
    cpl_vector_unwrap( errs);
    cpl_vector_delete( qual);
  }
  sinfo_msg_warning("pk5");
  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      sinfo_spectrum_free(&result);
    }
    return result;
}

/*---------------------------------------------------------------------------*/
/**
 * @brief Load a 1D spectrum  structure
 *
 * @param[in] s1d_frame the 1D spectrum frame
 * @return the spectrum structure
 */
/*---------------------------------------------------------------------------*/
static sinfo_spectrum*
sinfo_spectrum_load_from_table( cpl_frame* s1d_frame, const char* wname,
                                const char* fname)
{
  sinfo_spectrum* result = NULL;
  const char *s1dname = NULL;
  cpl_table* tab=NULL;

  int status;

  SINFO_ASSURE_NOT_NULL( s1d_frame);
  check_nomsg( s1dname = cpl_frame_get_filename( s1d_frame));
  tab = cpl_table_load(s1dname, 1, 0);
  sinfo_msg_warning("loading file %s",s1dname);
  SINFO_CALLOC(result, sinfo_spectrum,1);
  check_nomsg( result->flux_header = cpl_propertylist_load( s1dname,0));
  check_nomsg( result->errs_header = cpl_propertylist_load( s1dname,0));
  check_nomsg( result->qual_header = cpl_propertylist_load( s1dname,0));

  check_nomsg( result->lambda_min = cpl_table_get_column_min(tab,wname));

  float* pspec;
  //check_nomsg( result->lambda_step = sinfo_pfits_get_cdelt1( result->flux_header));
  
  //check_nomsg(result->lambda_step=cpl_table_get_float(tab,wname,1,&status)-result->lambda_min);
  pspec=cpl_table_get_data_float(tab, fname);
  
  check_nomsg(result->size =  cpl_table_get_nrow(tab));
  check_nomsg(result->size_lambda =  cpl_table_get_nrow(tab));
  
  result->lambda_max = cpl_table_get_column_max(tab,wname);
  result->lambda_step=(result->lambda_max-result->lambda_min)/(result->size-1);
  pspec=cpl_table_get_data_float(tab, fname);

  check_nomsg( result->flux = cpl_image_new( result->size, 1 , CPL_TYPE_DOUBLE));
  check_nomsg( result->errs = cpl_image_new( result->size, 1 , CPL_TYPE_DOUBLE));
  check_nomsg( result->qual = cpl_image_new( result->size, 1 , CPL_TYPE_INT));


  double* pflux=cpl_image_get_data_double(result->flux);
  double* perrs=cpl_image_get_data_double(result->errs);
  int* pqual=cpl_image_get_data_int(result->qual);

  for(int i=0;i<result->size_lambda;i++) {
      pflux[i]=pspec[i];
      perrs[i]=sqrt(fabs(pflux[i]));
      pqual[i]=0;
  }
  sinfo_msg_warning("spectrum size %d", result->size);
  cleanup:
    sinfo_free_table(&tab);
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
        sinfo_print_rec_status(9);
      sinfo_spectrum_free(&result);
    }
    return result;
}

static star_index* star_index_construct(const char* fits_file)
{
    star_index* pret = cpl_malloc(sizeof(star_index));
    pret->index_size = 0;
    pret->index_table = 0;
    pret->cache_size = 0;
    pret->cache = 0;
    pret->cache_index = 0;
    if (fits_file)
    {
        size_t bt = strlen(fits_file) * sizeof(*fits_file)+1;
        pret->fits_file_name = cpl_malloc(bt);
        strcpy(pret->fits_file_name, fits_file);
    }
    else
    {
        pret->fits_file_name = 0;
    }
    return pret;
}


static void star_index_destruct(star_index* pindex)
{
    if(pindex)
    {
        if (pindex->cache)
        {
            int i = 0;
            for ( i = 0; i < pindex->cache_size; i++)
            {
                cpl_table_delete(pindex->cache[i]);
            }
            cpl_free(pindex->cache);
            pindex->cache = 0;
            pindex->cache_size = 0;
        }
        cpl_table_delete(pindex->index_table);
        if(pindex->fits_file_name)
        {
            cpl_free(pindex->fits_file_name);
        }
        cpl_free(pindex->cache_index);
        cpl_free(pindex);
    }

}



static double
sinfo_iterpol_linear(double* data_x,double* data_y,int ndata,double x,
int* i_inf,int* i_sup)
{
  int i=0;
  double y=0;
  //int i_min=0;
  //int i_max=0;
  double x1=data_x[*i_inf];
  double x2=data_x[*i_sup];
  double y1=data_y[*i_inf];
  double y2=data_y[*i_sup];
  //x1=x2=y1=y2=0;
  //i_min=((*i_inf)>0) ? (*i_inf):0;
  //i_max=((*i_sup)<ndata) ? (*i_sup):ndata;
  //sinfo_msg("i_min=%d i_max=%d",i_min,i_max);
  //for(i=i_min;i<i_max;i++) {
  for(i=1;i<ndata-1;i++) {
    //sinfo_msg("data_x[%d]=%g,data_y[%d]=%g,x=%g",i,data_x[i],i,data_y[i],x);
    if(data_x[i]>x) {
      y1=data_y[i-1];
      y2=data_y[i];
      x1=data_x[i-1];
      x2=data_x[i];
      *i_inf=i-1;
      *i_sup=i+1;
      break;
    }
  }
  //sinfo_msg("i_inf=%d i_sup=%d",*i_inf,*i_sup);
  y=(y2-y1)/(x2-x1)*(x-x1)+y1;
  //sinfo_msg("x1=%g x2=%g y1=%g y2=%g x=%g y=%g",x1,x2,y1,y2,x,y);

  return y;
}

static sinfo_star_flux_list * sinfo_star_flux_list_create( int size )
{
  sinfo_star_flux_list * result = NULL ;

  /* Create the list */
  SINFO_CALLOC( result, sinfo_star_flux_list, 1 ) ;
  result->header = NULL ;
  result->size = size ;
  SINFO_CALLOC( result->lambda, double, size ) ;
  SINFO_CALLOC( result->flux, double, size ) ;

 cleanup:
  return result ;
}


static sinfo_star_flux_list * sinfo_star_flux_list_load( cpl_frame * star_frame)
{
  cpl_table *table = NULL ;
  const char * tablename = NULL ;
  sinfo_star_flux_list * result = NULL ;
  int nentries, i ;
  double * plambda, * pflux ;

  /* verify input */
  SINFO_ASSURE_NOT_NULL( star_frame);

  /* get table filename */
  check_nomsg(tablename = cpl_frame_get_filename( star_frame ));

  /* Load the table */
  table=cpl_table_load(tablename,1,0);
  //SINFO_TABLE_LOAD( table, tablename ) ;

  /*
  cpl_table_and_selected_double(table,SINFO_STAR_FLUX_LIST_COLNAME_WAVELENGTH,CPL_LESS_THAN,SINFO_STAR_FLUX_UVB_WAV_MIN);
  cpl_table_erase_selected(table);
  */

   check_nomsg( nentries = cpl_table_get_nrow( table ) ) ;

  /* Create the list */
  check_nomsg( result = sinfo_star_flux_list_create( nentries ) ) ;

  plambda = result->lambda ;
  pflux = result->flux ;

  check_nomsg(result->header = cpl_propertylist_load(tablename, 0));

  check_nomsg(cpl_table_cast_column(table,SINFO_STAR_FLUX_LIST_COLNAME_WAVELENGTH,"F_WAVELENGTH",CPL_TYPE_FLOAT));
  check_nomsg(cpl_table_cast_column(table,SINFO_STAR_FLUX_LIST_COLNAME_FLUX,"F_FLUX",CPL_TYPE_FLOAT))
;

  /* Fill the list */
  int status=0;
  for( i = 0 ; i< nentries ; i++, plambda++, pflux++ ) {
    float value ;

    check_nomsg( value=cpl_table_get_float( table, "F_WAVELENGTH",i, &status ) ) ;
    *plambda = value ;
    check_nomsg( value=cpl_table_get_float( table, "F_FLUX", i, &status ) ) ;
    *pflux = value ;
  }

 cleanup:
  if (cpl_error_get_code () != CPL_ERROR_NONE) {
    sinfo_msg("Error: can't load frame %s",cpl_frame_get_filename(star_frame));
    sinfo_star_flux_list_free(&result);
  }
  sinfo_free_table(&table);
  return result ;
}


/**
@brief spectra interpolation
@param table_frame table frame containing the spectrum
@param wstep wave step
@param wmin wave min
@param wmax wave max
 */
static cpl_frame*
sinfo_spectrum_interpolate_linear(cpl_frame* table_frame,
                       const double wstep,
                       const double wmin,
                       const double wmax)
{

   cpl_frame* result=NULL;
   cpl_table* table_i=NULL;
   cpl_table* table_o=NULL;
   const char* name_i=NULL;
   const char* tag_i=NULL;

   char* name_o=NULL;
   char* tag_o=NULL;

   cpl_propertylist* plist=NULL;
   int nrows_i=0;
   int nrows_o=0;
   int row=0;

   double* pwave_i=NULL;
   double* pflux_i=NULL;
   double* pwave_o=NULL;
   double* pflux_o=NULL;
   int i_inf=0;
   int i_sup=0;

   //int istart = 0;

   SINFO_ASSURE_NOT_NULL_MSG(table_frame,"Null input table frame");
   SINFO_ASSURE_NOT_ILLEGAL_MSG(wmax>wmin,"wmax  < wmin");
   SINFO_ASSURE_NOT_ILLEGAL_MSG(wstep>0,"wstep  <= 0");

   name_i=cpl_frame_get_filename(table_frame);
   tag_i=cpl_frame_get_tag(table_frame);
   //sinfo_msg("name_i=%s",name_i);
   check_nomsg(table_i=cpl_table_load(name_i,1,0));
   nrows_i=cpl_table_get_nrow(table_i);
   plist=cpl_propertylist_load(name_i,0);
   nrows_o=(int)((wmax-wmin)/wstep+0.5);
   table_o=cpl_table_new(nrows_o);
   cpl_table_new_column(table_o,"LAMBDA",CPL_TYPE_DOUBLE);
   cpl_table_new_column(table_o,"FLUX",CPL_TYPE_DOUBLE);
   check_nomsg(cpl_table_fill_column_window_double(table_o,"LAMBDA",0,nrows_o,0.));
   check_nomsg(cpl_table_fill_column_window_double(table_o,"FLUX",0,nrows_o,0.));
   check_nomsg(pwave_i=cpl_table_get_data_double(table_i,"LAMBDA"));
   check_nomsg(pflux_i=cpl_table_get_data_double(table_i,"FLUX"));
   check_nomsg(pwave_o=cpl_table_get_data_double(table_o,"LAMBDA"));
   check_nomsg(pflux_o=cpl_table_get_data_double(table_o,"FLUX"));
   i_inf=0;
   i_sup=nrows_o;

   for (row = 0; row < nrows_o; row++)
   {

      pwave_o[row]= wmin + row * wstep;
      pflux_o[row]= sinfo_iterpol_linear(pwave_i,pflux_i,nrows_i,pwave_o[row],
                       &i_inf,&i_sup);

      //sinfo_msg("interpolated flux[%g]=%g",pwave_o[row],pflux_o[row]);
   }
   tag_o=cpl_sprintf("INTERPOL_%s",tag_i);
   name_o=cpl_sprintf("INTERPOL_%s.fits",tag_i);
   sinfo_pfits_set_pcatg(plist,tag_o);
   check_nomsg(cpl_table_save(table_o,plist,NULL,name_o,CPL_IO_DEFAULT));
   check_nomsg(result=sinfo_frame_product(name_o,tag_o,CPL_FRAME_TYPE_TABLE,
                                  CPL_FRAME_GROUP_PRODUCT,
                                  CPL_FRAME_LEVEL_FINAL));

 cleanup:


   sinfo_free_table(&table_i);
   sinfo_free_table(&table_o);
   sinfo_free_propertylist(&plist);
   cpl_free(name_o);
   cpl_free(tag_o);

   return result;

}
static double *
sinfo_bspline_interpolate_data_at_pos(double* w_data, double* f_data,
                                    const int n_data,
                                    double* w_pos, const int n_pos)
{
    double * result=NULL;

    int i_min = 0;
    int i_max = n_pos;
    gsl_interp_accel *acc = gsl_interp_accel_alloc ();
    int i=0;
    sinfo_msg("w_pos[0]=%g w_data[0]=%g",w_pos[0],w_data[0]);
    sinfo_msg("w_pos[n_pos-1]=%g w_data[n_data-1]=%g",w_pos[n_pos-1],w_data[n_data-1]);
    cpl_ensure(w_pos[0] >= w_data[0], CPL_ERROR_ILLEGAL_INPUT,NULL);
    cpl_ensure(w_pos[n_pos-1] <= w_data[n_data-1], CPL_ERROR_ILLEGAL_INPUT,NULL);
    
    gsl_spline *spline = gsl_spline_alloc (gsl_interp_cspline, n_data);
    
    gsl_spline_init (spline, w_data, f_data, n_data);
    

    result=cpl_calloc(n_pos, sizeof(double));
    
    if(w_pos[0] == w_data[0]) {
        result[0] = f_data[0];
        i_min = 1;
    }
    
    if(w_pos[n_pos-1] == w_data[n_data-1]) {
        result[n_pos-1] = f_data[n_data-1];
        i_max = n_pos-1;
    }
   
    for (i = i_min; i < i_max; i ++) {
        double xi = w_pos[i];
        double yi = gsl_spline_eval (spline, xi, acc);
        //printf ("%g %g\n", xi, yi);
        result[i]=yi;
    }
   
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);

    return result;
}



static cpl_frame* sinfo_spectrum_save2( sinfo_spectrum* s, const char* filename,
                  const char* tag)
{
  cpl_frame *product_frame = NULL;

  SINFO_ASSURE_NOT_NULL(s);
  SINFO_ASSURE_NOT_NULL(filename);

  check_nomsg( sinfo_pfits_set_extname(s->flux_header , "FLUX"));
  check_nomsg(sinfo_plist_set_extra_keys(s->flux_header,"IMAGE","DATA","RMSE",
                                 "FLUX","ERRS","QUAL",0));

  check_nomsg( sinfo_pfits_set_extname(s->errs_header , "ERRS"));
  check_nomsg(sinfo_plist_set_extra_keys(s->errs_header,"IMAGE","DATA","RMSE",
                                 "FLUX","ERRS","QUAL",1));

  check_nomsg( sinfo_pfits_set_extname(s->qual_header , "QUAL"));
  check_nomsg(sinfo_plist_set_extra_keys(s->qual_header,"IMAGE","DATA","RMSE",
                                 "FLUX","ERRS","QUAL",2));

  /* Save the file */
   if ( s->size_slit > 1){

    double crval1=0;
    double crpix1=0;
    double cdelt1=0;

    double crval2=0;
    double crpix2=0;
    double cdelt2=0;

    crval1=sinfo_pfits_get_crval1(s->flux_header);
    crpix1=sinfo_pfits_get_crpix1(s->flux_header);
    cdelt1=sinfo_pfits_get_cdelt1(s->flux_header);

    crval2=sinfo_pfits_get_crval2(s->flux_header);
    crpix2=sinfo_pfits_get_crpix2(s->flux_header);
    cdelt2=sinfo_pfits_get_cdelt2(s->flux_header);

    sinfo_pfits_set_wcs(s->errs_header,crpix1,crval1,cdelt1,crpix2,crval2,cdelt2);
    sinfo_pfits_set_wcs(s->qual_header,crpix1,crval1,cdelt1,crpix2,crval2,cdelt2);

    check_nomsg( sinfo_pfits_set_pcatg( s->flux_header, tag));
    check(cpl_image_save ( s->flux, filename, CPL_BPP_IEEE_FLOAT,
      s->flux_header, CPL_IO_DEFAULT),
      "Could not save data to %s extension 0", filename);
    check(cpl_image_save ( s->errs, filename, CPL_BPP_IEEE_FLOAT,
      s->errs_header, CPL_IO_EXTEND),
      "Could not save errs to %s extension 1", filename);
    check(cpl_image_save ( s->qual, filename, CPL_BPP_32_SIGNED,
      s->qual_header, CPL_IO_EXTEND),
      "Could not save qual to %s extension 2", filename);
  }
  else{
    cpl_vector *flux1D = NULL;
    cpl_vector *err1D = NULL;
    cpl_vector *qual1D = NULL;

    double crval1=0;
    double crpix1=0;
    double cdelt1=0;

    crval1=sinfo_pfits_get_crval1(s->flux_header);
    crpix1=sinfo_pfits_get_crpix1(s->flux_header);
    cdelt1=sinfo_pfits_get_cdelt1(s->flux_header);



    sinfo_pfits_set_ctype1(s->flux_header,"LINEAR");
    sinfo_pfits_set_cunit1(s->flux_header,"nm");
    cpl_propertylist_erase_regexp(s->flux_header, "^CTYPE2", 0);



    //check_nomsg(sinfo_pfits_set_wcs1(s->errs_header, crpix1, crval1, cdelt1));
    sinfo_pfits_set_cunit1(s->errs_header,"nm");

    //check_nomsg(sinfo_pfits_set_wcs1(s->qual_header, crpix1, crval1, cdelt1));
    sinfo_pfits_set_cunit1(s->qual_header,"nm");
    sinfo_pfits_set_bunit(s->qual_header,SINFO_BUNIT_NONE_C);

    check_nomsg( flux1D = cpl_vector_new_from_image_row( s->flux, 1));
    check_nomsg( err1D = cpl_vector_new_from_image_row( s->errs, 1));
    check_nomsg( qual1D = cpl_vector_new_from_image_row( s->qual, 1));
    check_nomsg( cpl_vector_save( flux1D, filename, CPL_BPP_IEEE_FLOAT,
      s->flux_header, CPL_IO_DEFAULT));
    check_nomsg( cpl_vector_save( err1D, filename, CPL_BPP_IEEE_FLOAT,
      s->errs_header, CPL_IO_EXTEND));
    check_nomsg( cpl_vector_save( qual1D, filename, CPL_BPP_32_SIGNED,
      s->qual_header, CPL_IO_EXTEND));
    cpl_vector_delete( flux1D);
    cpl_vector_delete( err1D);
    cpl_vector_delete( qual1D);
  }


  check_nomsg( product_frame = cpl_frame_new() ) ;
  check_nomsg( cpl_frame_set_filename( product_frame,filename ));
  check_nomsg( cpl_frame_set_type( product_frame, CPL_FRAME_TYPE_IMAGE )) ;
  check_nomsg( cpl_frame_set_level( product_frame, CPL_FRAME_LEVEL_FINAL )) ;
  check_nomsg( cpl_frame_set_group( product_frame, CPL_FRAME_GROUP_PRODUCT ));

  cleanup:
    if (cpl_error_get_code () != CPL_ERROR_NONE) {
      sinfo_free_frame(&product_frame);
      product_frame = NULL;
    }
    return product_frame;
}

static cpl_table*
sinfo_spectrum_to_table(sinfo_spectrum* s)
{

  cpl_table* table_s=NULL;
  int size=0;
  int i=0;

  double wmin=0;
  double wstp=0;

  double* pwave=NULL;

  double* perrs=NULL;
  double* pflux=NULL;
  int* pqual=NULL;

  double* errs=NULL;
  double* flux=NULL;
  int* qual=NULL;

  size=sinfo_spectrum_get_size_lambda(s);
  wmin= sinfo_spectrum_get_lambda_min(s);
  wstp= sinfo_spectrum_get_lambda_step(s);
  //sinfo_msg("wmin=%g[nm] wstp=%g[nm]",wmin,wstp);

  //sinfo_msg("Lambda size spectrum=%d",size);
  table_s=cpl_table_new(size);
  cpl_table_new_column(table_s,"wavelength",CPL_TYPE_DOUBLE);
  cpl_table_fill_column_window_double(table_s,"wavelength",0,size,0);
  pwave=cpl_table_get_data_double(table_s,"wavelength");

  cpl_table_new_column(table_s,"flux",CPL_TYPE_DOUBLE);
  cpl_table_fill_column_window_double(table_s,"flux",0,size,0);
  pflux=cpl_table_get_data_double(table_s,"flux");

  cpl_table_new_column(table_s,"errs",CPL_TYPE_DOUBLE);
  cpl_table_fill_column_window_double(table_s,"errs",0,size,0);
  perrs=cpl_table_get_data_double(table_s,"errs");

  cpl_table_new_column(table_s,"qual",CPL_TYPE_INT);
  cpl_table_fill_column_window_int(table_s,"qual",0,size,0);
  pqual=cpl_table_get_data_int(table_s,"qual");


  flux=sinfo_spectrum_get_flux(s);
  errs=sinfo_spectrum_get_errs(s);
  qual=sinfo_spectrum_get_qual(s);

  for(i=0;i<size;i++) {
    pwave[i]=wmin+i*wstp;
    pflux[i]=flux[i];
    perrs[i]=errs[i];
    pqual[i]=qual[i];
  }

  cpl_table_duplicate_column(table_s,"logwave",table_s,"wavelength");
  cpl_table_logarithm_column(table_s,"logwave",CPL_MATH_E);

  return table_s;

}


static sinfo_star_flux_list *
sinfo_star_flux_list_load_spectrum( cpl_frame * star_frame )
{

  //const char * imagename = NULL ;
  sinfo_star_flux_list * result = NULL ;
  int nentries, i ;
  double * plambda, * pflux ;
  sinfo_spectrum* spectrum=NULL;

  /* verify input */
  SINFO_ASSURE_NOT_NULL( star_frame);

  /* get table filename */
  //check_nomsg(imagename = cpl_frame_get_filename( star_frame ));

  /* Load the image */

  check_nomsg(spectrum=sinfo_spectrum_load_from_table(star_frame,"wavelength",
                  "counts_bkg")) ;
  /* Original X-shooter code was:
   * nentries = sinfo_pfits_get_naxis1(spectrum->flux_header);
   * and is replaced by:
   */
  sinfo_msg_warning("pk1");
  nentries=spectrum->size;

  //PIPPO
  
  /* Create the list */
  check_nomsg( result = sinfo_star_flux_list_create( nentries ) ) ;
  result->header = cpl_propertylist_duplicate(spectrum->flux_header);
  
  sinfo_msg_warning("pk2");
  cpl_propertylist_append_int(result->header,"NAXIS1",spectrum->size);
  sinfo_msg_warning("pk3");
  cpl_propertylist_append_double(result->header,"CRVAL1",spectrum->lambda_min);
  sinfo_msg_warning("pk4");
  cpl_propertylist_append_double(result->header,"CDELT1",spectrum->lambda_step);
  sinfo_msg_warning("pk5");
  plambda = result->lambda ;
  pflux = result->flux ;

  double* pima=cpl_image_get_data_double(spectrum->flux);
  double wmin=spectrum->lambda_min;
  double wstep=spectrum->lambda_step;

  double wmax;
  
  /* Fill the list */
  sinfo_msg_warning("nentries=%d",nentries);

  for( i = 0 ; i< nentries ; i++, plambda++, pflux++ ) {
    double x=i;
    *plambda = (float) (wmin+x*wstep) ;
    *pflux =   (float) pima[i];
    wmax=*plambda;

  }
  sinfo_msg_warning("Test wmin=%g wmax=%g wstep=%g",wmin,wmax,wstep);
 cleanup:

  if (cpl_error_get_code () != CPL_ERROR_NONE) {
    sinfo_msg_error("can't load frame %s",cpl_frame_get_filename(star_frame));
    sinfo_print_rec_status(0);
    sinfo_star_flux_list_free(&result);
  }
  sinfo_spectrum_free(&spectrum);
  return result;
}



static HIGH_ABS_REGION *
sinfo_fill_tell_fit_regions(
   new_sinfo_band band,
   cpl_frame* high_abs_frame)
{
   HIGH_ABS_REGION * phigh=NULL;
   int nrow=0;
   double* pwmin=0;
   double* pwmax=0;
   int i=0;
   cpl_table* high_abs_tab=NULL;


  if(high_abs_frame !=NULL) {
     high_abs_tab=cpl_table_load(cpl_frame_get_filename(high_abs_frame),1,0);
  }
  //the_arm=xsh_instrument_get_arm(instrument);



   if(high_abs_tab!=NULL) {
      nrow=cpl_table_get_nrow(high_abs_tab);
      check_nomsg(pwmin=cpl_table_get_data_double(high_abs_tab,"LAMBDA_MIN"));
      check_nomsg(pwmax=cpl_table_get_data_double(high_abs_tab,"LAMBDA_MAX"));

      phigh = (HIGH_ABS_REGION *) cpl_calloc(nrow + 1, sizeof(HIGH_ABS_REGION));
      for(i=0;i<nrow;i++) {
         phigh[i].lambda_min=pwmin[i];
         phigh[i].lambda_max=pwmax[i];
      }
      phigh[nrow].lambda_min=0;
      phigh[nrow].lambda_max=0;

   } else {

       // Specific JH-NIR */
       /* adapted to SINFONI */
       if(band == INSTRUMENT_BAND_UVB) {
           phigh = UvbTellFitRegions ;
       } else if(band == INSTRUMENT_BAND_VIS) {
           phigh = VisTellFitRegions ;
       } else if(band == INSTRUMENT_BAND_NIR) {
           phigh = NirTellFitRegions ;
       } else if(band == INSTRUMENT_BAND_ALL) {
           phigh = AllTellFitRegions ;
       } else if(band == INSTRUMENT_BAND_J) {
           phigh = NirJTellFitRegions ;
       } else if(band == INSTRUMENT_BAND_H) {
           phigh = NirHTellFitRegions ;
       } else if(band == INSTRUMENT_BAND_K) {
           phigh = NirKTellFitRegions ;
       } else if(band == INSTRUMENT_BAND_HK) {
           phigh = NirHKTellFitRegions ;
       } else {
           phigh = NirHKTellFitRegions ;
       }
       
   }
  cleanup:
   sinfo_free_table(&high_abs_tab);
   return phigh;
}


static cpl_error_code
sinfo_star_flux_list_to_frame( sinfo_star_flux_list* list, cpl_frame* frame)
{
	cpl_msg_warning("", "START SAVE");
  cpl_table *s = NULL ;
  const char * name = NULL ;
  int size=0;
  float* pflux=NULL;
  int i=0;
  const char* tag=NULL;
  cpl_frame * frm = NULL;
  cpl_propertylist * plist = NULL;

  /* verify input */
  SINFO_ASSURE_NOT_NULL( frame);
  SINFO_ASSURE_NOT_NULL( list);
 

  /* save result */
  name = cpl_frame_get_filename(frame);

  s = cpl_table_load(name, 1, 0);
 
	cpl_msg_warning("", "OOOOO ");


  /* basic checks: verify list versus spectrum size and wave range */
  size= cpl_table_get_nrow(s);

	cpl_msg_warning(cpl_func, "sz %d sz2 %d", size, list->size);


  cpl_ensure_code(list->size == size, CPL_ERROR_ILLEGAL_INPUT);
 
  /* copy flux from list to specrum */
  pflux = cpl_table_get_data_float(s, "counts_bkg");


  if(pflux)
		cpl_msg_warning("", "OK ptr");
  else
		cpl_msg_warning("", "bad ptr");

  for(i=0;i<size;i++) {
    pflux[i]=list->flux[i];
  }
  
	cpl_msg_warning("", "OK p2222tr");


  plist = cpl_propertylist_load(name, 0);
  cpl_table_save(s, plist, NULL, name, CPL_IO_CREATE);

 cleanup:
  cpl_frame_delete(frm);
  cpl_table_delete(s);
  cpl_propertylist_delete(plist);

	cpl_msg_warning("", "END SAVE");

  return cpl_error_get_code() ;
}


/**
  @name    xsh_xcorrelate
  @memo    Cross-correlation of two 1d signals.
  @param   line_i        The reference signal.
  @param   width_i       Number of samples in reference signal.
  @param   line_t        Candidate signal to compare.
  @param   width_t       Number of samples in candidate signal.
  @param   half_search   Half-size of the search domain.
  @param   delta         Output sinfo_correlation offset.
  @return  Maximum cross-correlation value as a double.
  @doc

  Two signals are expected in input of this function: a reference
  signal (line_i) and a candidate signal (line_t) . 
  They are expected to be roughly the same signal up to an offset (delta)

  A cross-correlation is computed on 2*half_search+1 values. The
  maximum of likelihood is the maximum cross-correlation value between
  signals. The offset 'delta' corresponding to this position is returned.

  Returns -100.0 in case of error. Normally, the cross-sinfo_correlation
  coefficient is normalized so it should stay between -1 and +1.
 */

static double*
sinfo_function1d_xcorrelate2(
    double *    line_i,
    int         width_i,
    double *    line_t,
    int         width_t,
    int         half_search,
    int         normalise,
    double *    xcorr_max,
    double *    delta
)
{
    double * xcorr ;
    double   mean_i, mean_t ;
    double   rms_i, rms_t ;
    double   sum, sqsum ;
    double   norm ;
    int      maxpos ;
    int      nsteps ;
    int      i ;
    int      step ;

    int STEP_MIN=-half_search;
    int STEP_MAX=half_search;

    /* Compute normalization factors */
    sum = sqsum = 0.00 ;
    /*
    sinfo_msg_warning("width_i=%d width_t=%d half_search=%d normalise=%d",
              width_i,width_t,half_search,normalise);
              */
    for (i=0 ; i<width_i && !isnan(line_i[i]); i++) {
        
        sum += line_i[i] ;
        sqsum += line_i[i] * line_i[i];
       
        //sinfo_msg_warning("i=%d sum=%g sqsum=%g",i,sum,sqsum);
        
        
    }

    mean_i = sum / (double) width_i ;
    sqsum /= (double)width_i ;
    rms_i = sqsum - mean_i*mean_i ;
    //sinfo_msg_warning("mean_i=%d sqsum=%g rms_i=%g",mean_i,sqsum,rms_i);
    
    sum = sqsum = 0.00 ;
    for (i=0 ; i<width_t && !isnan(line_t[i]); i++) {
        sum += line_t[i] ;
        sqsum += line_t[i] * line_t[i];
    }
    mean_t = sum / (double)width_t ;
    sqsum /= (double)width_t ;
    rms_t = sqsum - mean_t*mean_t ;

    norm = 1.00 / sqrt(rms_i * rms_t);
    //sinfo_msg_warning("norm=%g",norm);
    nsteps = (STEP_MAX - STEP_MIN) +1 ;
    xcorr = cpl_malloc(nsteps * sizeof(double));
    if(normalise==0) {
      mean_t=0;
      mean_i=0;
      norm=1;
    }
    //sinfo_msg_warning("STEP MIN/MAX: %d %d",STEP_MIN,STEP_MAX);
    for (step=STEP_MIN ; step<=STEP_MAX ; step++) {
        xcorr[step-STEP_MIN] = 0.00 ;
        int nval = 0 ;
        for (i=0 ; i<width_t ; i++) {
            if ((i+step >= 0) &&
                (i+step < width_i)) {
               if( !isnan(line_i[i+step]) && !isnan(line_t[i]) ) {
            xcorr[step-STEP_MIN] += (line_t[i] - mean_t) *
                                    (line_i[i+step] - mean_i) *
                                    norm ;
               }
            /*
            sinfo_msg_warning("xcorr=%g f1=%g f2=%g norm=%g",
                              xcorr[step-STEP_MIN],
                              (line_t[i] - mean_t),
                              (line_i[i+step] - mean_i),norm);
                              */
                nval++ ;
            }
        }
        xcorr[step-STEP_MIN] /= (double) nval ;
    }

    *xcorr_max = xcorr[0] ;
    //sinfo_msg_warning("xcorr_max=%g",*xcorr_max);
    maxpos    = 0 ;
    for (i=0 ; i<nsteps ; i++) {
        if (xcorr[i]>(*xcorr_max)) {
            maxpos = i ;
            *xcorr_max = xcorr[i];
        }
    }
    double fraction = 0;
    //sinfo_msg_warning("maxpos=%d",maxpos);

    cpl_vector* vcor=cpl_vector_wrap(nsteps,xcorr);
    double a=xcorr[maxpos-1];
    double b=xcorr[maxpos+1];
    double c=xcorr[maxpos];
    fraction=(a-b)/(2.*a+2.*b-4.*c);
    //cpl_vector_save(vcor,"vcor_correlate.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
    cpl_vector_unwrap(vcor);

    //sinfo_msg_warning("STEP_MIN=%d maxpos=%d",STEP_MIN,maxpos);
    //sinfo_msg_warning("fraction=%g a=%g b=%g c=%g delta=%g",fraction,a,b,c,0.5*(b-a));

    (*delta) =  (double)STEP_MIN + (double)maxpos;
    *delta-=fraction;
    //sinfo_msg("fraction=%g a=%g b=%g c=%g diff=%g delta=%g",fraction,a,b,c,0.5*(b-a),*delta);
    return xcorr;
}
static cpl_error_code
sinfo_function1d_xcorrelate(cpl_vector* wcorr, cpl_vector* fcorr,SINFO_GAUSSIAN_FIT* gfit,const double range,const int ext)
{
  int size_x;
  cpl_table* tab = NULL;
  cpl_table* ext_tab;
  char fname[MAX_NAME_SIZE];
  cpl_vector* wave=NULL;
  cpl_vector* flux=NULL;

  size_x=cpl_vector_get_size(wcorr);
  tab = cpl_table_new(size_x);
  cpl_table_wrap_double(tab, cpl_vector_get_data(wcorr), "logwave");
  cpl_table_wrap_double(tab, cpl_vector_get_data(fcorr), "flux");

  //snprintf(fname,MAX_NAME_SIZE, "fcorr_org.fits");
  //sinfo_table_save(tab,NULL,NULL,fname,ext);
  cpl_table_and_selected_double(tab, "logwave", CPL_GREATER_THAN,
      (*gfit).peakpos - range);
  cpl_table_and_selected_double(tab, "logwave", CPL_LESS_THAN,
      (*gfit).peakpos + range);

  ext_tab = cpl_table_extract_selected(tab);
  cpl_table_unwrap(tab,"logwave");
  cpl_table_unwrap(tab,"flux");
  sinfo_free_table(&tab);
  //snprintf(fname,MAX_NAME_SIZE, "fcorr_ext.fits");
  //sinfo_table_save(ext_tab,NULL,NULL,fname,ext);
  int next = 0;
  next = cpl_table_get_nrow(ext_tab);
  //snprintf(fname, MAX_NAME_SIZE,"fcorr_tab.fits");
  //sinfo_table_save(ext_tab,NULL,NULL,fname,ext);
  double peakpos=0;
  double sigma=0;
      double area=0;
      double offset=0;
      double mse=0;

  wave = cpl_vector_wrap(next, cpl_table_get_data_double(ext_tab, "logwave"));
  flux = cpl_vector_wrap(next, cpl_table_get_data_double(ext_tab, "flux"));
  //sinfo_msg("gauss before fit: peak=%g sigma=%g",(*gfit).peakpos,(*gfit).sigma);

  cpl_vector_fit_gaussian( wave, NULL, flux,NULL, CPL_FIT_ALL,
       &peakpos, &sigma,&area, &offset, &mse,NULL,NULL);
  //sinfo_msg("gauss check fit: peak=%g sigma=%g",peakpos,sigma);
  cpl_vector_fit_gaussian( wave, NULL, flux,NULL, CPL_FIT_ALL,
      &gfit->peakpos, &gfit->sigma,&gfit->area, &gfit->offset, &gfit->mse,NULL,NULL);
  //double fwhm = CPL_MATH_FWHM_SIG * gfit.sigma;
  //sinfo_msg("gauss after fit: peak=%g sigma=%g",(*gfit).peakpos,(*gfit).sigma);

  //cleanup:
  cpl_vector_unwrap(wave);
  cpl_vector_unwrap(flux);
  sinfo_free_table(&ext_tab);
  return cpl_error_get_code();
}




/* perform correlation of spectrum and model over a given range. Correlation peak determination is refined by Gauss fit
   * result is the shift to be applied and the FWHM of the correlation function (useful to make 2nd iteration of correlation
   * near peak pos)
   */
static cpl_error_code
sinfo_correl_spectra(double* flux_s, double* flux_m, const int size,
                   const int hsearch, const double wlogstp,const int norm_xcorr,
           const double range, const int ext,SINFO_GAUSSIAN_FIT* gfit, int is_refine, cpl_vector ** pfcorr)
{
  double* pwcorr = NULL;
  double corr = 0;
  double delta=0;
  char fname[MAX_NAME_SIZE];
  int i=0;
  double size_corr = 2 * hsearch + 1;
  cpl_vector* f1=NULL;
  cpl_vector* f2=NULL;
  cpl_vector* correl=NULL;
  double shift=0;
  cpl_vector* wcorr = NULL;

  double * xcorr = NULL;

  check_nomsg(xcorr = sinfo_function1d_xcorrelate2(flux_s, size, flux_m, size, hsearch,
                      norm_xcorr,&corr, &delta));
  
  check_nomsg(f1=cpl_vector_wrap(size, flux_s));
  f2=cpl_vector_wrap(size, flux_m);
  //cpl_vector_save(f1,"f1.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  //cpl_vector_save(f2,"f2.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  //sinfo_msg("size=%d",size);
  correl=cpl_vector_new(size_corr);
  check_nomsg(shift=cpl_vector_correlate(correl,f1,f2));

  

  
  cpl_vector_save(correl,"correl.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  sinfo_msg("shift=%g",shift);
  //sinfo_msg("correlation value=%g measured shift[sample units]=%g measured shift[lognm]=%g", corr, delta,delta*);
  //sinfo_msg("hsearch=%d delta=%g sampling step=%g",hsearch,delta,wlogstp);

  gfit->peakpos = (hsearch + delta)*wlogstp;
  gfit->sigma = wlogstp*10;
  gfit->area = 1.;

  sinfo_msg("gauss guess: peak: %12.8g sigma %g", gfit->peakpos, gfit->sigma);
  cpl_vector * fcorr = NULL;
  check_nomsg(fcorr = cpl_vector_wrap(size_corr, xcorr));
  *pfcorr = fcorr;
  /* AMO changed: to remove a leak (next we override values and
   * later we do cpl_vector_unwrap
   */
  pwcorr= (double*) cpl_calloc(size_corr, sizeof(double));
  for (i = 0; i < size_corr; i++) {
    pwcorr[i] = i*wlogstp;
  }

  

  check_nomsg(wcorr = cpl_vector_wrap(size_corr, pwcorr));
  //snprintf(fname,MAX_NAME_SIZE,"fcorr.fits");
  //xsh_vector_save(fcorr,NULL,fname,ext);
  //snprintf(fname,MAX_NAME_SIZE,"wcorr.fits");
  //check_nomsg(xsh_vector_save(wcorr,NULL,fname,ext));

  /* 4) fit Gaussian to +/-0.001 in log_lam_nm around cross correlation peak */
  check_nomsg(sinfo_function1d_xcorrelate(wcorr,fcorr,gfit,range,ext));
  
  sinfo_msg("gauss fit: peak[lognm]: %12.8g sigma[lognm] %g peak[sampl_units]: %12.8g sigma[sampl_units] %g",
      gfit->peakpos, gfit->sigma,gfit->peakpos/wlogstp, gfit->sigma/wlogstp);


  cleanup:
  cpl_vector_unwrap(f1);
  cpl_vector_unwrap(f2);
  cpl_vector_unwrap(wcorr);
  cpl_vector_delete(correl);
  cpl_free(pwcorr);
  return cpl_error_get_code();

}

static cpl_error_code
sinfo_star_flux_list_divide( sinfo_star_flux_list * result,
		sinfo_star_flux_list * factor)
{

  SINFO_ASSURE_NOT_NULL( result ) ;
  SINFO_ASSURE_NOT_NULL( factor ) ;
  /* Create the list */

  SINFO_ASSURE_NOT_ILLEGAL_MSG(result->size==factor->size,"List of different sizes");
  int size=result->size;
  //SINFO_ASSURE_NOT_ILLEGAL_MSG(result->lambda[0]==factor->lambda[0],"List of different wave start");
  //SINFO_ASSURE_NOT_ILLEGAL_MSG(result->lambda[size-1]==factor->lambda[size-1],"List of different wave end");

  int i=0;
  for(i=0;i<size;i++) {
    result->flux[i] /= factor->flux[i];
  }

 cleanup:
  return cpl_error_get_code();
}

static double
sinfo_resample_double(double wout,double* pw1,double* pf1,double wmin,double wmax,int size_obs)
{

  double m=0;
  double w1=0;
  double w2=0;
  double f1=0;
  double f2=0;
  double fout=0;

  int i=0;

  /* are we in case of extrapolation ? */
  if( wout < wmin ) {
  /* w < wmin */
    m=(pf1[1]-pf1[0])/(pw1[1]-pw1[0]);
    fout=m*(wout-wmin)+pf1[0];
  } else if ( wout > wmax ) {
  /* w > wmax */
    m=(pf1[size_obs-1]-pf1[size_obs-2])/(pw1[size_obs-1]-pw1[size_obs-2]);
    fout=m*(wout-wmax)+pf1[size_obs-1];

  } else {
  /* else, interpolate */
    for(i=0;i<size_obs;i++) {
      if(pw1[i]<wout) {
        w1=pw1[i];
        f1=pf1[i];
      } else {
        w2=pw1[i];
        f2=pf1[i];
        break;
      }
    }
    m=(f2-f1)/(w2-w1);
    fout=m*(wout-w1)+f1;
  }
  return fout;
}



static cpl_table*
sinfo_table_downsample_table(cpl_table* tinp,const char* cwinp, const char* cfinp,
           cpl_table* tref,const char* cwref, const char* cfref)
{

  cpl_table* tres=NULL;
  double* pwinp=NULL;
  double* pwref=NULL;
  double* pwres=NULL;

  double* pfinp=NULL;
  //double* pfref=NULL;
  double* pfres=NULL;

  int size_inp=0;
  int size_ref=0;
  double wmin=0;
  double wmax=0;
  int i=0;

  check_nomsg(size_inp=cpl_table_get_nrow(tinp));
  check_nomsg(size_ref=cpl_table_get_nrow(tref));
  //cpl_table_dump_structure(tinp,stdout);
  //sinfo_msg_warning("cwinp=%s",cwinp);
  check_nomsg(wmin=cpl_table_get_column_min(tinp,cwinp));
  check_nomsg(wmax=cpl_table_get_column_max(tinp,cwinp));
  //sinfo_msg_warning("checking wmin=%g wmax=%g",wmin,wmax);

  /* we create a table of size as large as the reference one */
  tres=cpl_table_new(size_ref);
  /* we create the minimum tables required */
  cpl_table_new_column(tres,cwref,CPL_TYPE_DOUBLE);
  cpl_table_new_column(tres,cfref,CPL_TYPE_DOUBLE);
  cpl_table_fill_column_window_double(tres,cwref,0,size_ref,0.);
  cpl_table_fill_column_window_double(tres,cfref,0,size_ref,0.);
  //sinfo_msg_warning("columns: %s %s",cwref,cwinp);
  //cpl_table_dump_structure(tres,stdout);
  check_nomsg(pwinp=cpl_table_get_data_double(tinp,cwinp));
  check_nomsg(pwref=cpl_table_get_data_double(tref,cwref));
  check_nomsg(pfinp=cpl_table_get_data_double(tinp,cfinp));
  check_nomsg(pfres=cpl_table_get_data_double(tres,cfref));
  check_nomsg(pwres=cpl_table_get_data_double(tres,cwref));
  check_nomsg(pfres=cpl_table_get_data_double(tres,cfref));

  for(i=0;i<size_ref;i++) {
    pwres[i]=pwref[i];
    pfres[i]=sinfo_resample_double(pwres[i],pwinp,pfinp,wmin,wmax,size_inp);
    //sinfo_msg_warning("w=%g f=%g",pwres[i],pfres[i]);
  }

 cleanup:

  return tres;
}

//re-sample a table to a uniform sampling step */
static cpl_table*
sinfo_table_resample_uniform(cpl_table* tinp,const char* cwinp, const char* cfinp,
       const double wstp)
{

  cpl_table* tres=NULL;
  double* pwinp=NULL;
  double* pwref=NULL;
  double* pfinp=NULL;
  double* pfres=NULL;
  int size_inp=0;
  double wmin=0;
  double wmax=0;
  int i=0;
  int size=0;

  check_nomsg(size_inp=cpl_table_get_nrow(tinp));
  //cpl_table_dump(tinp,0,size_inp,stdout);
  //sinfo_msg("cwinp=%s",cwinp);
  check_nomsg(wmin=cpl_table_get_column_min(tinp,cwinp));
  check_nomsg(wmax=cpl_table_get_column_max(tinp,cwinp));
  size=(int)((wmax-wmin)/wstp+0.5);
  //sinfo_msg_warning("wmin=%15.10g wmax=%15.10g wstp=%15.10g size=%d",wmin,wmax,wstp,size);
  /* we first duplicate ref to resampled table to have the same wavelength
     sampling and then we take care of computing the flux */
  tres=cpl_table_new(size);
  cpl_table_new_column(tres,cwinp,CPL_TYPE_DOUBLE);
  cpl_table_new_column(tres,cfinp,CPL_TYPE_DOUBLE);

  cpl_table_fill_column_window_double(tres,cwinp,0,size,0.);
  cpl_table_fill_column_window_double(tres,cfinp,0,size,0.);
  check_nomsg(pwref=cpl_table_get_data_double(tres,cwinp));
  for(i=0;i<size;i++) {
    pwref[i]=wmin+i*wstp;
  }
  check_nomsg(pwinp=cpl_table_get_data_double(tinp,cwinp));
  check_nomsg(pfinp=cpl_table_get_data_double(tinp,cfinp));
  check_nomsg(pfres=cpl_table_get_data_double(tres,cfinp));

  for(i=0;i<size;i++) {
    pfres[i]=sinfo_resample_double(pwref[i],pwinp,pfinp,wmin,wmax,size_inp);
  }

 cleanup:
  sinfo_print_rec_status(0);
  return tres;
}


static cpl_error_code
sinfo_align_model_to_spectrum(cpl_table* table_me,cpl_table* table_se,
                              const double wlogstp,const int norm_xcorr,
                              const int ext,cpl_table** table_mm)
{
  cpl_table* table_rm = NULL;
  cpl_table* table_rs = NULL;
  char fname[MAX_NAME_SIZE];
  int i=0;
  int size_mm=cpl_table_get_nrow(*table_mm);
  //cpl_lowpass filter_type = CPL_LOWPASS_LINEAR;
  cpl_vector* vfluxm = NULL;
  cpl_vector* vlowpass_fluxm = NULL;

  cpl_vector      *   conv_kernel =NULL;

  /* TODO: this re-sampling was decided not to use */
  //check_nomsg(cpl_table_dump_structure(table_me,stdout));
 
  //sinfo_msg("logspt=%g",wlogstp);
  check_nomsg(sinfo_table_save(table_me,NULL,NULL,"table_me.fits",ext));
  check_nomsg(table_rm = sinfo_table_resample_uniform(table_me, "logwave", "flux", wlogstp));

  //sinfo_msg("size of spectrum tab=%d",(int)cpl_table_get_nrow(table_rm));
  snprintf(fname,MAX_NAME_SIZE, "model_selected_wrange_log_resampled_uniform.fits");
  check_nomsg(sinfo_table_save(table_rm,NULL,NULL,fname,ext));


  check_nomsg(table_rs = sinfo_table_downsample_table(table_se, "logwave", "flux", table_rm,
                          "logwave", "flux"));

  sinfo_table_save(table_se,NULL,NULL,"table_se.fits",ext);
  snprintf(fname,MAX_NAME_SIZE, "spectrum_resampled_used_in_correlation.fits");
  sinfo_table_save(table_rs,NULL,NULL,fname,ext);

  /* perform correlation of spectrum and model over a given range. Correlation peak determination is refined by Gauss fit
   * result is the shift to be applied and the FWHM of the correlation function (useful to make 2nd iteration of correlation
   * near peak pos)
   */


  //double corr = 0;
  double* flux_s = NULL;
  double* flux_m = NULL;
  int size_s = 0;
  int hsearch = 200;
  /*
  int size_m = 0;

  double delta = 0;
  double size_x = 2 * hsearch + 1;
  double* xcorr = NULL;
  double* pwcorr = NULL;
  int size=0;
  */
  SINFO_GAUSSIAN_FIT gfit;

  double wrange=0.0005;
  //size=cpl_table_get_nrow(table_rs);
   sinfo_msg_warning("pk1");
   sinfo_print_rec_status(1);
   
  flux_s = cpl_table_get_data_double(table_rs, "flux");
  flux_m = cpl_table_get_data_double(table_rm, "flux");
  cpl_table_save(table_rs,NULL,NULL,"table_rs.fits",CPL_IO_DEFAULT);
  cpl_table_save(table_rm,NULL,NULL,"table_rm.fits",CPL_IO_DEFAULT);
  size_s = cpl_table_get_nrow(table_rs);
  //size_m = cpl_table_get_nrow(table_me);

 
  sinfo_msg_warning("pk2");
    sinfo_print_rec_status(2);
  sinfo_msg_warning("hsearch=%d",hsearch);
  cpl_vector * xcorr = NULL;
  check_nomsg(sinfo_correl_spectra(flux_s,flux_m,size_s,hsearch,wlogstp,norm_xcorr,wrange,ext,&gfit, norm_xcorr, &xcorr));
  cpl_vector_delete(xcorr);

  //sinfo_msg("sigma=%g",gfit.sigma);
  sinfo_msg_warning("pk3");
   sinfo_print_rec_status(3);
  cpl_msg_warning("","computed shift[lognm]=%g",(gfit.peakpos-hsearch*wlogstp));
  hsearch=(int)3.*CPL_MATH_FWHM_SIG * gfit.sigma/wlogstp;
  sinfo_msg("hsearch=%d",hsearch);
  xcorr = NULL;
  check_nomsg(sinfo_correl_spectra(flux_s,flux_m,size_s,hsearch,wlogstp,norm_xcorr,wrange,ext,&gfit, norm_xcorr, &xcorr));
  sinfo_msg_warning("pk4");
   sinfo_print_rec_status(4);

   if(ext == 0)
	   cpl_vector_save(xcorr, "xcorr_after_refine.fits", CPL_TYPE_DOUBLE, NULL, CPL_IO_CREATE);

   cpl_vector_delete(xcorr);
  /* 5: adjust model wavelength scale:
   :log_lam_nm_0 = :log_lam_nm+<position>
   */
  cpl_table_add_scalar(*table_mm, "logwave", (gfit.peakpos-hsearch*wlogstp));
  sinfo_msg("computed shift[lognm]=%g",(gfit.peakpos-hsearch*wlogstp));

  //sinfo_table_save(*table_mm,NULL,NULL,"model_shift.fits",ext);
 
  /*
   6) create Gaussian with measured FWHM (corrected by FWHM of model spectrum,
   but that effect is small since the model spectra have a resolution of 60000,
   so the FWHM of the cross-correlation peak is dominated by the observation)

   7) convolve model with Gaussain in log_lam_nm space (X-shooter has contant lambda/dlambda)

   We apply this operations to the model sub-range corresponding to the spectrum
   */

  /* NB:
   * the only eventually allowed filer in cpl_vector_filter_lowpass_create is CPL_LOWPASS_LINEAR
   * as CPL_LOWPASS_GAUSSIAN uses a fixed FWHM. Checking result we later preferred to use
   * cpl_wlcalib_xc_convolve_create_kernel that is deprecated but gives better results.
   * There the FWHM parameter is actually the sigma: FWHM/CPL_MATH_FWHM_SIG
   */

  double* pvlowpass_fluxm = NULL;
  double* pflux_c = NULL;
  double fwhm = CPL_MATH_FWHM_SIG * gfit.sigma;
  double fwhm_nm = pow(CPL_MATH_E, fwhm);
  //cpl_vector * spec_clean=NULL;
  //int fwhm_pix=(int)(fwhm_nm+0.50000);
  int fwhm_pix = (int) (fwhm / wlogstp + 0.5);

  cpl_table_new_column(*table_mm, "flux_c", CPL_TYPE_DOUBLE);
  cpl_table_fill_column_window_double(*table_mm, "flux_c", 0, size_mm, 0);
  sinfo_msg("fwhm_nm=%12.10g fwhm_pix=%d",fwhm_nm,fwhm_pix);

  vfluxm = cpl_vector_wrap(size_mm,cpl_table_get_data_double(*table_mm, "flux"));
  sinfo_msg_warning("pk5");
    sinfo_print_rec_status(5);
  /* we use CPL deprecated function but it works properly */
  conv_kernel = cpl_wlcalib_xc_convolve_create_kernel(fwhm_pix/CPL_MATH_FWHM_SIG,fwhm_pix/CPL_MATH_FWHM_SIG);
  vlowpass_fluxm=cpl_vector_duplicate(vfluxm);
  cpl_wlcalib_xc_convolve(vlowpass_fluxm, conv_kernel);
  sinfo_msg_warning("pk6");
    sinfo_print_rec_status(6);
  //vlowpass_fluxm = cpl_vector_filter_lowpass_create(vfluxm, filter_type,fwhm_pix);

  pvlowpass_fluxm = cpl_vector_get_data(vlowpass_fluxm);
  cpl_vector_unwrap(vfluxm);

  pflux_c = cpl_table_get_data_double(*table_mm, "flux_c");
  for (i = 0; i < size_mm; i++) {
    pflux_c[i] = pvlowpass_fluxm[i];
  }
  snprintf(fname, MAX_NAME_SIZE,"model_convolved.fits");
  sinfo_table_save(*table_mm,NULL,NULL,fname,ext);
  
  cleanup:
  sinfo_msg_warning("pk7");
    sinfo_print_rec_status(7);
  cpl_vector_delete(conv_kernel);
  cpl_vector_delete(vlowpass_fluxm);
  sinfo_free_table(&table_rs);
  sinfo_free_table(&table_rm);
  return cpl_error_get_code();

}
static HIGH_ABS_REGION *
sinfo_fill_tell_compute_resid_regions(
   new_sinfo_band band,
   cpl_frame* high_abs_frame)
{
   HIGH_ABS_REGION * phigh=NULL;
   int nrow=0;
   double* pwmin=0;
   double* pwmax=0;
   int i=0;
   cpl_table* high_abs_tab=NULL;
   //XSH_ARM the_arm;

  if(high_abs_frame !=NULL) {
     high_abs_tab=cpl_table_load(cpl_frame_get_filename(high_abs_frame),1,0);
  }
  //the_arm=xsh_instrument_get_arm(instrument);



   if(high_abs_tab!=NULL) {
      nrow=cpl_table_get_nrow(high_abs_tab);
      check_nomsg(pwmin=cpl_table_get_data_double(high_abs_tab,"LAMBDA_MIN"));
      check_nomsg(pwmax=cpl_table_get_data_double(high_abs_tab,"LAMBDA_MAX"));

      phigh = (HIGH_ABS_REGION *) cpl_calloc(nrow + 1, sizeof(HIGH_ABS_REGION));
      for(i=0;i<nrow;i++) {
         phigh[i].lambda_min=pwmin[i];
         phigh[i].lambda_max=pwmax[i];
      }
      phigh[nrow].lambda_min=0;
      phigh[nrow].lambda_max=0;

   } else {

      /* Now specific to high absorption region (linear interpolation)
         NIR and VIS only
      */

         /* Adapted to SINFONI Specific NIR */
       if(band == INSTRUMENT_BAND_UVB) {
           phigh = UvbTellComputeResidRegions ;
       } else if(band == INSTRUMENT_BAND_VIS) {
           phigh = VisTellComputeResidRegions ;
       } else if(band == INSTRUMENT_BAND_NIR) {
           phigh = NirTellComputeResidRegions ;
       } else if(band == INSTRUMENT_BAND_ALL) {
           phigh = AllTellComputeResidRegions ;
       } else if(band == INSTRUMENT_BAND_J) {
           phigh = NirJTellComputeResidRegions ;
       } else  if(band == INSTRUMENT_BAND_H) {
           phigh = NirHTellComputeResidRegions ;
       } else  if(band == INSTRUMENT_BAND_K) {
           phigh = NirKTellComputeResidRegions ;
       } else  if(band == INSTRUMENT_BAND_HK) {
           phigh = NirHKTellComputeResidRegions ;
       } else {
           phigh = NirHKTellComputeResidRegions ;
       }


   }
  cleanup:
   sinfo_free_table(&high_abs_tab);
   return phigh;
}

static cpl_error_code sinfo_evaluate_tell_model(cpl_table* corr,
    new_sinfo_band band, const int ext, double* mean, double* rms) {

  HIGH_ABS_REGION * phigh = NULL;
  cpl_table* tab_tmp = NULL;
  cpl_table* tab_stack = NULL;
  char fname[MAX_NAME_SIZE];
  phigh = sinfo_fill_tell_compute_resid_regions(band, NULL);
  cpl_table_select_all(corr);
  /* we remove from table points that should not be sampled */
  if (phigh != NULL) {
    int counter = 0;
    //int kh = 0;
    //int nrow = 0;
    int nsamples = 0;
    //sinfo_msg("Extract from correction only sub-regions to compute residuals" );
    for (; phigh->lambda_min != 0.; phigh++) {
      //sinfo_msg("Flag [%g,%g]",phigh->lambda_min,phigh->lambda_max);
      int nrow = cpl_table_and_selected_double(corr, "wave", CPL_NOT_LESS_THAN,
          phigh->lambda_min);
      //sinfo_msg("Select min=%d",nrow);
      nrow = cpl_table_and_selected_double(corr, "wave", CPL_LESS_THAN,
          phigh->lambda_max);
      //sinfo_msg("Select max=%d",nrow);
      tab_tmp = cpl_table_extract_selected(corr);
      cpl_table_select_all(corr);
      if (counter == 0) {
        //sinfo_msg("create stack table nraws=%d",cpl_table_get_nrow(tab_corr));
        tab_stack = cpl_table_duplicate(tab_tmp);
        counter++;
      } else {
        nrow = cpl_table_get_nrow(tab_stack);
        //sinfo_msg("append to stack table size=%d",nrow);
        cpl_table_insert(tab_stack, tab_tmp, nrow);
      }
      sinfo_free_table(&tab_tmp);
      nsamples++;
    }

  }
  snprintf(fname,MAX_NAME_SIZE, "tab_corr_sampl.fits");
  sinfo_table_save(tab_stack, NULL, NULL, fname, ext);

  *mean = fabs(  ( cpl_table_get_column_mean(tab_stack, "correction") - 1. ) );
  *rms = cpl_table_get_column_stdev(tab_stack, "correction");

  //sinfo_msg("mean=%g rms=%g", *mean, *rms);
  sinfo_free_table(&tab_tmp);
  sinfo_free_table(&tab_stack);
  return cpl_error_get_code();

}


static cpl_error_code
sinfo_extract_points_to_fit(cpl_table* table,const char* cwav,const char* cratio,
                            const int nsamples, new_sinfo_band band,
                            cpl_vector** vec_wave,cpl_vector** vec_flux)
{


  double* pvec_wave = NULL;
  double* pvec_flux = NULL;
  int kh=0;
  HIGH_ABS_REGION * phigh = NULL;
  cpl_table* tab_tmp=NULL;
  double wmin=0;
  double wmax=0;
  double wstp=1.;
  /*
  double wmin_fit_region=9999;
  double wmax_fit_region=-9999;
  */
 
  wmin=cpl_table_get_column_min(table,cwav);
  wmax=cpl_table_get_column_max(table,cwav);

  *vec_wave = cpl_vector_new(nsamples+2);
  *vec_flux = cpl_vector_new(nsamples+2);

  /*
  phigh=sinfo_fill_tell_fit_regions(band,NULL);
  for (kh = 0; phigh->lambda_min != 0.; phigh++) {
    if( wmin_fit_region>  phigh->lambda_min) wmin_fit_region=phigh->lambda_min;
    if( wmax_fit_region<  phigh->lambda_max) wmax_fit_region=phigh->lambda_max;
  }

  if(wmax_fit_region>wmax) {
  *vec_wave = cpl_vector_new(nsamples+2);
  *vec_flux = cpl_vector_new(nsamples+2);
  } else {
  *vec_wave = cpl_vector_new(nsamples+1);
  *vec_flux = cpl_vector_new(nsamples+1);
  }
  */
  pvec_wave = cpl_vector_get_data(*vec_wave);
  pvec_flux = cpl_vector_get_data(*vec_flux);
  //sinfo_msg("sk1 nsamples=%d",nsamples);

  // add one point at the start of the wave range
  cpl_table_and_selected_double(table, cwav, CPL_LESS_THAN,wmin+wstp);
  tab_tmp = cpl_table_extract_selected(table);
  pvec_wave[0] = wmin;
  //sinfo_msg_warning("wmin=%g",wmin);
  pvec_flux[0] = cpl_table_get_column_median(tab_tmp, cratio);
  cpl_table_select_all(table);
  sinfo_free_table(&tab_tmp);
 
  phigh=sinfo_fill_tell_fit_regions(band,NULL);


  if (phigh != NULL) {
    //int nrow = 0;
    //sinfo_msg("Fill median ratio values" );
    for (kh = 0; phigh->lambda_min != 0.; phigh++) {
      sinfo_msg("Process line %d Flag [%g,%g]",kh,phigh->lambda_min,phigh->lambda_max);
      cpl_table_and_selected_double(table, cwav,
          CPL_NOT_LESS_THAN, phigh->lambda_min);
      //sinfo_msg("Select min=%d",nrow);
      int nrow=cpl_table_and_selected_double(table, cwav, CPL_LESS_THAN,
          phigh->lambda_max);
      //sinfo_msg("Select max=%d",nrow);
      tab_tmp = cpl_table_extract_selected(table);
      cpl_table_select_all(table);
      //TODO SINFONI requires this extra check
      if(nrow>0) {
          
      pvec_wave[kh+1] = 0.5 * (phigh->lambda_max + phigh->lambda_min);
      pvec_flux[kh+1] = cpl_table_get_column_median(tab_tmp, cratio);
      //sinfo_msg_warning("wave=%g",pvec_wave[kh+1]);
      //sinfo_msg_warning("flux=%g",pvec_flux[kh+1]);
      } else {
          sinfo_msg_warning("No data point at wave=%g",phigh->lambda_max);
      }
      kh++;
      sinfo_free_table(&tab_tmp);
    }
  }
 
  /*
  if(wmax_fit_region>wmax) {
    cpl_table_select_all(table);

    cpl_table_and_selected_double(table, cwav, CPL_NOT_LESS_THAN,wmax-wstp);
    tab_tmp = cpl_table_extract_selected(table);

    pvec_wave[kh+1] = wmax;
    sinfo_msg("wave=%g",pvec_wave[kh+1]);
    check_nomsg(pvec_flux[kh+1] = cpl_table_get_column_median(tab_tmp, cratio));
    cpl_table_delete(tab_tmp);
  }
  */


  cpl_table_select_all(table);

  cpl_table_and_selected_double(table, cwav, CPL_NOT_LESS_THAN,wmax-wstp);
  tab_tmp = cpl_table_extract_selected(table);

  pvec_wave[kh+1] = wmax;
  pvec_flux[kh+1] = cpl_table_get_column_median(tab_tmp, cratio);
  //sinfo_msg_warning("wmax=%g",wmax);
  //cpl_table_save(tab_tmp,NULL,NULL,"tab_tmp.fits",CPL_IO_DEFAULT);
  //cpl_vector_dump(vec_wave,stdout);
  //cpl_vector_save(*vec_wave,"vec_wave.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  //cpl_vector_save(*vec_flux,"vec_flux.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  sinfo_free_table(&tab_tmp);
 

 //cleanup:

  return cpl_error_get_code();
}



static cpl_table*
sinfo_extract_ranges_to_fit(cpl_table* table_in,const char* cwav,new_sinfo_band band) {

  cpl_table* stack = NULL;
  HIGH_ABS_REGION * phigh = NULL;

  phigh = sinfo_fill_tell_compute_resid_regions(band, NULL);

  cpl_table* tab_tmp = NULL;
  int nsamples = 0;
  /*
  cpl_table_dump(table_in,0,2,stdout);
  sinfo_msg("wmin=%g wmax=%g",
          cpl_table_get_column_min(table_in,cwav),
         cpl_table_get_column_max(table_in,cwav));
  */
  /* we remove from table points that should not be sampled */
  if (phigh != NULL) {
    int counter = 0;
    //int kh = 0;
    int nrow = 0;
    //sinfo_msg("Extract from ratio only regions to be sampled" );
    for (; phigh->lambda_min != 0.; phigh++) {
      //sinfo_msg("Flag [%g,%g]",phigh->lambda_min,phigh->lambda_max);
      check_nomsg(nrow = cpl_table_and_selected_double(table_in, cwav, CPL_NOT_LESS_THAN,
                         phigh->lambda_min));
      //sinfo_msg("Select min=%d",nrow);
      check_nomsg(nrow = cpl_table_and_selected_double(table_in,cwav, CPL_LESS_THAN,
                         phigh->lambda_max));
      //sinfo_msg("Select max=%d",nrow);
      check_nomsg(tab_tmp = cpl_table_extract_selected(table_in));
      cpl_table_select_all(table_in);
      if (counter == 0) {
        //sinfo_msg("create stack table");
        stack = cpl_table_duplicate(tab_tmp);
        counter++;
      } else {
        nrow=cpl_table_get_nrow(stack);
        //sinfo_msg("append to stack table size=%d",nrow);
        cpl_table_insert(stack, tab_tmp, nrow);
      }
      sinfo_free_table(&tab_tmp);
      nsamples++;
    }
  }
  sinfo_free_table(&tab_tmp);

 cleanup:
  return stack;
}

static cpl_table*
sinfo_table_resample_table(cpl_table* tinp,const char* cwinp, const char* cfinp,
           cpl_table* tref,const char* cwref, const char* cfref)
{

  cpl_table* tres=NULL;
  double* pwinp=NULL;
  double* pwref=NULL;
  double* pfinp=NULL;
  double* pfres=NULL;
  int size_inp=0;
  int size_ref=0;
  double wmin=0;
  double wmax=0;
  int i=0;

  check_nomsg(size_inp=cpl_table_get_nrow(tinp));
  check_nomsg(size_ref=cpl_table_get_nrow(tref));
  //sinfo_msg("cwinp=%s",cwinp);
  check_nomsg(wmin=cpl_table_get_column_min(tinp,cwinp));
  check_nomsg(wmax=cpl_table_get_column_max(tinp,cwinp));
  //sinfo_msg("wmin=% vag wmax=%g",wmin,wmax);

  /* we first duplicate ref to resampled table to have the same wavelength
     sampling and then we take care of computing the flux */
  tres=cpl_table_duplicate(tref);
  check_nomsg(pwinp=cpl_table_get_data_double(tinp,cwinp));
  check_nomsg(pwref=cpl_table_get_data_double(tref,cwref));

  check_nomsg(pfinp=cpl_table_get_data_double(tinp,cfinp));
  check_nomsg(pfres=cpl_table_get_data_double(tres,cfref));

  for(i=0;i<size_ref;i++) {
    pfres[i]=sinfo_resample_double(pwref[i],pwinp,pfinp,wmin,wmax,size_inp);
  }

 cleanup:
  return tres;
}

static cpl_table*
sinfo_table_select_range(cpl_table* table_in,const char* col,const double wmin, const double wmax)
{
  cpl_table* table_ext=NULL;

  cpl_table_and_selected_double(table_in, col, CPL_NOT_LESS_THAN, wmin);
  cpl_table_and_selected_double(table_in, col, CPL_NOT_GREATER_THAN, wmax);
  table_ext=cpl_table_extract_selected(table_in);

  return table_ext;
}

static cpl_table*
sinfo_telluric_model_eval(cpl_frame* frame_m,sinfo_spectrum* s,
                          new_sinfo_band band,cpl_size* model_idx)
{

  cpl_table* tab_res=NULL;
  cpl_table* table_s=NULL;
  cpl_table* table_m=NULL;
  cpl_table* table_se=NULL;
  cpl_table* table_mm=NULL;
  cpl_table* table_rm = NULL;
  cpl_table* table_rs_div_mc=NULL;
  cpl_table* table_me=NULL;
  cpl_table* table_rs=NULL;
  const char* name_model=NULL;

  double wmin=0;
  double wmax=0;
  //int i=0;
  double lmin=7.001; //1097.731 nm was 7.0 for 1096.633 nm
  double lmax=7.1; //1211.967 nm
  double wlogstp = 10.e-6;

  double um2nm=1000.; // 1nm

  int ext=0;
  int next=0;
  double mean=0;
  double rms=0;
  //int kh=0;
  int nsamples=0;
  char fname[MAX_NAME_SIZE];
  cpl_table* tab_corr = NULL;
  cpl_table* tab_stack = NULL;
  //int size_mm=0;
  cpl_table* ext_tab = NULL;
  double* pmean=NULL;
  double* prms=NULL;
  int* pext=NULL;
  int norm_xcorr=0;
  HIGH_ABS_REGION * phigh = NULL;
  cpl_propertylist* qc_head=NULL;
  
         if(band == INSTRUMENT_BAND_UVB) {
  } else if(band == INSTRUMENT_BAND_VIS) {
      lmin=6.828; //(923.342 nm)
      lmax=6.894; //(986.339 nm)
       //lmax=6.888; //(986.339 nm)
       wlogstp = 7.826e-6; //(1 nm)
       norm_xcorr=1;
  } else if(band == INSTRUMENT_BAND_NIR) {
      lmin=7.0; //1096.633 nm
      lmax=7.1; //1211.967 nm
  } else if(band == INSTRUMENT_BAND_J) {
      lmin=7.001; //1097.731 nm was 7.0 for 1096.633 nm
      lmax=7.1; //1211.967 nm
  } else if(band == INSTRUMENT_BAND_H) {
      lmin=7.268; //1434.525 nm was 7.0 for 1096.633 nm
      lmax=7.531; //1865.28 nm
  } else if(band == INSTRUMENT_BAND_K) {
      lmin=7.565; //1928.54 nm 
      lmax=7.812; //2471.215 nm
  } else if(band == INSTRUMENT_BAND_HK) {
      lmin=7.268; //1434.525 nm was 7.0 for 1096.633 nm
      lmax=7.812; //2471.215 nm
  } else if(band == INSTRUMENT_BAND_ALL) {
      lmin=7.0; //1096.633 nm
      lmax=7.1; //1211.967 nm
      norm_xcorr=1;
      //um2nm=10000.;
  } else {
      lmin=7.250; //1408.731 nm 
      lmax=7.820; //2491.5 nm
  }



 
  //sinfo_msg("wmin=%g wmax=%g",wmin,wmax);
  //wstp= sinfo_spectrum_get_lambda_step(s);
  check_nomsg(table_s=sinfo_spectrum_to_table(s));
  

  
  
  if(band == INSTRUMENT_BAND_J || 
     band == INSTRUMENT_BAND_H ||
     band == INSTRUMENT_BAND_K ||
     band == INSTRUMENT_BAND_HK ||
     band == INSTRUMENT_BAND_L) {
      // TODO: convert SINFONI data from um to nm units :
      // we need to rescale also the logwave column
      int nrow=cpl_table_get_nrow(table_s);
      double* pflux=cpl_table_get_data_double(table_s,"flux");

      for(int i=0;i<nrow;i++){
          if( isnan(pflux[i]) ) {
              //cpl_table_set_invalid(table_s,"flux",i);
              pflux[i]=0;
          }
      }
      cpl_table_erase_invalid(table_s);
      wmin=cpl_table_get_column_min(table_s,"wavelength");
      wmax=cpl_table_get_column_max(table_s,"wavelength");
  } else {
      wmin= sinfo_spectrum_get_lambda_min(s);
      wmax= sinfo_spectrum_get_lambda_max(s);
      sinfo_msg_warning("restrict wavelength to %g %g",wmin,wmax);
  }
  /* 
   cpl_table_multiply_scalar(table_s,"wavelength",um2nm);
  cpl_table_exponential_column(table_s,"logwave",CPL_MATH_E);
  cpl_table_multiply_scalar(table_s,"logwave",um2nm);
  cpl_table_logarithm_column(table_s, "logwave", CPL_MATH_E);
   */
 

  snprintf(fname,MAX_NAME_SIZE,"spectrum_obs.fits");
  check_nomsg(sinfo_table_save(table_s,NULL,NULL,fname,0));
  check_nomsg(phigh=sinfo_fill_tell_fit_regions(band,NULL));


  for (; phigh->lambda_min != 0.; phigh++) {
    nsamples++;
  }

  check_nomsg(name_model=cpl_frame_get_filename(frame_m));
  //sinfo_msg_warning("name_model=%s",name_model);
  //cpl_frame_set_type(frame_m,CPL_FRAME_TYPE_TABLE);
  check_nomsg(next=cpl_frame_get_nextensions(frame_m));
  check_nomsg(tab_res=cpl_table_new(next));
  cpl_table_new_column(tab_res,"id",CPL_TYPE_INT);
  cpl_table_new_column(tab_res,"mean",CPL_TYPE_DOUBLE);
  cpl_table_new_column(tab_res,"rms",CPL_TYPE_DOUBLE);



  cpl_table_fill_column_window_int(tab_res, "id", 0, next, 0);
  cpl_table_fill_column_window_double(tab_res, "mean", 0, next, 0);
  cpl_table_fill_column_window_double(tab_res, "rms", 0, next, 0);
  pext=cpl_table_get_data_int(tab_res,"id");
  pmean=cpl_table_get_data_double(tab_res,"mean");
  prms=cpl_table_get_data_double(tab_res,"rms");
  
  //sinfo_msg("name_model %s",name_model);
  for (ext = 0; ext < next; ext++) {
      
      
    /* load input model spectrum from appropriate extension */
    check_nomsg(table_m = cpl_table_load(name_model, ext+1, 1));
    snprintf(fname,MAX_NAME_SIZE,"tell_model_org.fits");
    check_nomsg(sinfo_table_save(table_m,NULL,NULL,fname,ext));
    
    /* the model wavelength are in um units, we need to convert to nm */
    cpl_table_multiply_scalar(table_m, "lam", um2nm);
    cpl_table_duplicate_column(table_m, "logwave", table_m, "lam");
    cpl_table_logarithm_column(table_m, "logwave", CPL_MATH_E);
    snprintf(fname,MAX_NAME_SIZE,"tell_model_res_nm.fits");
    check_nomsg(sinfo_table_save(table_m,NULL,NULL,fname,ext));
    
    /* we restrict model to range covered by observation */
    sinfo_msg_warning("restrict wavelength to %g %g",wmin,wmax);
    //if(band == INSTRUMENT_BAND_J) {
    table_mm=sinfo_table_select_range(table_m,"lam",wmin, wmax);
    //} else {
    //    table_mm=cpl_table_duplicate(table_m);
    //}
    sinfo_free_table(&table_m);
    //size_mm = cpl_table_get_nrow(table_mm);
    snprintf(fname,MAX_NAME_SIZE,"tell_model_range_arm.fits");
    check_nomsg(sinfo_table_save(table_mm,NULL,NULL,fname,ext));
    
    /* Step 2: extract the proper ranges for cross correlation */
    /* Apply this to the model */
    sinfo_msg_warning("select log wave to lmin=%g lmax=%g",lmin,lmax);
    //if(band == INSTRUMENT_BAND_J) {
    table_me=sinfo_table_select_range(table_mm,"logwave",lmin, lmax);
    //} else {
    //    table_me=cpl_table_duplicate(table_mm);
    //}
    
    /* make a test to verify that exponential of log comes back the same
     as original wave column value
     check_nomsg(cpl_table_duplicate_column(table_me,"wav",table_me,"logwave"));
     check_nomsg(cpl_table_exponential_column(table_me,"wav",CPL_MATH_E));
     */
    snprintf(fname,MAX_NAME_SIZE,"tell_model_selected_wrange_log.fits");
    check_nomsg(sinfo_table_save(table_me,NULL,NULL,fname,ext));
    
    check_nomsg(sinfo_table_save(table_s,NULL,NULL,"table_s.fits",ext));
    
    /* Apply this to the spectrum */
    /*
    sinfo_msg_warning("monitor wmin=%g wmax=%g",
                      cpl_table_get_column_min(table_s,"logwave"),
    cpl_table_get_column_min(table_s,"logwave"));
    */

    //if(band == INSTRUMENT_BAND_J) {
        table_se=sinfo_table_select_range(table_s,"logwave",lmin, lmax);
    //} else {
    //    table_se=cpl_table_duplicate(table_s);
    //}
    cpl_table_duplicate_column(table_se, "wav", table_se, "logwave");
    cpl_table_exponential_column(table_se, "wav", CPL_MATH_E);
    snprintf(fname,MAX_NAME_SIZE,"spectrum_selected_wrange_log.fits");
    check_nomsg(sinfo_table_save(table_se,NULL,NULL,fname,ext));
    
    /* Step 3: now we need to do the cross-correlation:
     Before doing this we need to be sure that each spectrum is sampled
     to the same point. We use the model as reference and re-sample (linearly)
     the observed spectrum so that we can then compute the cross-correlation

     Step 4: fit Gaussian to +/-0.001 in log_lam_nm around cross correlation peak

     Step 5: adjust model wavelength scale:
     :log_lam_nm_0 = :log_lam_nm+<position>


      6) create Gaussian with measured FWHM (corrected by FWHM of model spectrum,
      but that effect is small since the model spectra have a resolution of 60000,
      so the FWHM of the cross-correlation peak is dominated by the observation)

      7) convolve model with Gaussain in log_lam_nm space (X-shooter has contant lambda/dlambda)

      We apply this operations to the model sub-range corresponding to the spectrum
      */
    //sinfo_msg("size of model tab=%d",(int)cpl_table_get_nrow(table_me));
    //sinfo_msg("size of spectrum tab=%d",(int)cpl_table_get_nrow(table_se));
    //check_nomsg(sinfo_table_save(table_me,NULL,NULL,"table_me.fits",ext));
    check_nomsg(sinfo_table_save(table_se,NULL,NULL,"table_se.fits",ext));
    if( (band == INSTRUMENT_BAND_J) || 
		(band == INSTRUMENT_BAND_H) ||
		(band == INSTRUMENT_BAND_K) ||
        (band == INSTRUMENT_BAND_HK) ||
		(band == INSTRUMENT_BAND_UVB) ||
        (band == INSTRUMENT_BAND_VIS) ||
        (band == INSTRUMENT_BAND_NIR) ||
        (band == INSTRUMENT_BAND_ALL)) {
        check_nomsg(sinfo_align_model_to_spectrum(table_me,table_se,wlogstp,norm_xcorr,ext,&table_mm));
    } else {
        cpl_table_duplicate_column(table_mm,"flux_c",table_mm,"flux");
    }
   
    sinfo_free_table(&table_me);
    sinfo_free_table(&table_se);

    check_nomsg(sinfo_table_save(table_mm,NULL,NULL,"model_shift.fits",ext));

    /*
     8) convert convolved and shifted model to lam_nm space
     */
   
    check_nomsg(cpl_table_duplicate_column(table_mm,"wave",table_mm,"logwave"));
    check_nomsg(cpl_table_exponential_column(table_mm,"wave",CPL_MATH_E));
    snprintf(fname,MAX_NAME_SIZE,"model_aligned_to_obs.fits");
 
    //check_nomsg(sinfo_table_save(table_mm,NULL,NULL,fname,ext));
   
    /*
     9) divide observed spectrum by convolved and shifted model
     Here we get a telluric-corrected observed spectrum
     */
    sinfo_free_table(&table_rs);

    sinfo_free_table(&table_rm);
    /* In order to divide spectrum by model we need to put them on same sampling steps:
     * If we re-sample the model to the coarser spectrum step we introduce artifacts in the re-sampled model spectrum
     * Better is to up-sample the observed spectrum to the model. This simply artificially refine the observed spectrum
     */

    /* NOTE THAT WE NEED table_rpfitm at the very end of this function when we downsample table_rs */

    //TODO: this part seems weird, we need to clarify it, why are we sampling
    //table_mm to follow table_s but then we sample table_s to follow table_mm?

    check_nomsg(sinfo_table_save(table_mm,NULL,NULL,"table_mm_before_res.fits",ext));

    check_nomsg(table_rm = sinfo_table_resample_table(table_mm,"wave","flux",table_s,"wavelength","flux"));
    snprintf(fname,MAX_NAME_SIZE,"table_rm.fits");
    check_nomsg(sinfo_table_save(table_rm,NULL,NULL,fname,ext));
   
    check_nomsg(sinfo_table_save(table_rm,NULL,NULL,"table_rm_after_res.fits",ext));

    check_nomsg(table_rs=sinfo_table_resample_table(table_s,"wavelength","flux",table_mm,"wave","flux"));

    //PIPPO
    snprintf(fname,MAX_NAME_SIZE,"spectrum_resampled_to_model.fits");
    check_nomsg(sinfo_table_save(table_rs,NULL,NULL,fname,ext));
 
    cpl_table_duplicate_column(table_rs, "flux_model_convolved", table_mm, "flux_c");
    cpl_table_duplicate_column(table_rs, "ratio", table_rs, "flux");
    sinfo_free_table(&table_mm);
    check_nomsg(cpl_table_divide_columns(table_rs,"ratio","flux_model_convolved"));
    /* NB: the ratio spectrum corresponding to the model that minimizes
           the resisiduals is actually the final product we need to use for
           the final response computation */
    
    snprintf(fname,MAX_NAME_SIZE,"spectrum_observed_divided_by_model_convolved.fits");
    check_nomsg(sinfo_table_save(table_rs,NULL,NULL,fname,ext));
    
    snprintf(fname,MAX_NAME_SIZE,"spectrum_observed_divided_by_model_convolved_ALL.fits");
    check_nomsg(sinfo_table_save(table_rs,NULL,NULL,fname,ext));

    table_rs_div_mc=cpl_table_duplicate(table_rs);
     /*
     10) use predefined continuum points (avoiding regions of strong
     telluric absorption as well as known stellar lines) and fit spline
     without smoothing

     (points.list, email from Sabine).
     Take around each of them +/- 1nm.
     take the median in the range [-1nm+wc,wc+1nm]
     and then fit a spline (order 3: cubic)
     No smoothing
     This gives the continuum fit
     */

    check_nomsg(tab_stack=sinfo_extract_ranges_to_fit(table_rs,"wave",band));
    snprintf(fname,MAX_NAME_SIZE,"tab_stack.fits");
  
    //check_nomsg(sinfo_table_save(tab_stack,NULL,NULL,fname,ext));

    /* here we make the spline fit of median flux computed in each range
     * (to rule out that oscillations in each range creates problems)
     * */

    cpl_vector* vec_wave = NULL;
    cpl_vector* vec_flux = NULL;
    //double* pwav=NULL;

    check_nomsg(sinfo_table_save(table_rs,NULL,NULL,"table_rs.fits",ext));
    check_nomsg(sinfo_extract_points_to_fit(table_rs,"wave","ratio",nsamples,band,&vec_wave,&vec_flux));
   
    //pwav = cpl_table_get_data_double(table_rm, "wave");
    double* sfit=NULL;
    double* pfit=NULL;
    //double* pwave=NULL;
    //cpl_vector_dump(vec_wave,stdout);
   //cpl_vector_dump(vec_flux,stdout);

   /*

    double* pflux=NULL;

    snprintf(fname,MAX_NAME_SIZE,"ratio_sel.fits");
    check_nomsg(sinfo_table_save(tab_stack,NULL,NULL,fname,ext));
    */
    int nsel = 0;

    check_nomsg(nsel=cpl_table_get_nrow(tab_stack));
    cpl_table_new_column(tab_stack, "fit", CPL_TYPE_DOUBLE);
    cpl_table_fill_column_window_double(tab_stack, "fit", 0, nsel, 0);
    pfit = cpl_table_get_data_double(tab_stack, "fit");
    //pwave = cpl_table_get_data_double(tab_stack, "wave");
    
    /*


     pwav = cpl_table_get_data_double(tab_stack, "wave");
     pflux = cpl_table_get_data_double(tab_stack, "ratio");
     vflux=cpl_vector_wrap(size_mm,pflux);
     vflux_med=cpl_vector_filter_median_create(vflux,15);
     sinfo_msg("size_mm=%d",size_mm);
     snprintf(fname,MAX_NAME_SIZE,"flux_med.fits");
     cpl_vector_save(vflux_med,fname,CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
     mflux=cpl_vector_get_data(vflux_med);

    */

    /* here we perform the actual fit: we pass now the median points via:
     * vec_wave,pvec_flux,nsamples params
     *
     * To fit each point in each fit window region we would instead pass
     * pwav,pflux,nsel params
     *
     */
  
    double* pvec_wave=NULL;
    double* pvec_flux=NULL;
    pvec_wave=cpl_vector_get_data(vec_wave);
    pvec_flux=cpl_vector_get_data(vec_flux);
    if(ext == 64){
    	   cpl_vector_save(vec_wave,"papero.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
    	    cpl_vector_save(vec_flux,"FluxSelected.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
    }
    //sinfo_msg("Max wave=%g",cpl_vector_get_max(vec_wave));

    //sfit = xsh_bspline_fit_data2(pvec_wave, pvec_flux, nsamples, phigh,instrument, 1);
   
   //TODO: we need to clarify this, why this interpolation and then a similar
    //one below?
    check_nomsg(sfit = sinfo_bspline_interpolate_data_at_pos(pvec_wave, pvec_flux, nsamples+2,
                             pvec_wave,nsamples+2));

    //sinfo_msg("Max wave=%g",cpl_vector_get_max(vec_wave));

    //cpl_vector_delete(vflux_med);
    //cpl_vector_unwrap(vflux);
    int i=0;
    /* this instruction is to fill the fit column in table tab_stack with result of the fit
     for(i=0;i<nsel;i++) {
     pfit[i]=sfit[i];
     sinfo_msg("sfit=%g",sfit[i]);
     }


    snprintf(fname,MAX_NAME_SIZE,"ratio_fit.fits");
    check_nomsg(sinfo_table_save(tab_stack,NULL,NULL,fname,ext));
     */
    sinfo_free_table(&tab_stack);

    cpl_table* tab_med = cpl_table_new(nsamples+2);

    check_nomsg(cpl_table_wrap_double(tab_med,pvec_wave,"wave"));
    check_nomsg(cpl_table_wrap_double(tab_med,pvec_flux,"ratio"));
    check_nomsg(cpl_table_wrap_double(tab_med,sfit,"fit"));
    snprintf(fname,MAX_NAME_SIZE,"ratio_med.fits");
    //check_nomsg(sinfo_table_save(tab_med,NULL,NULL,fname,ext));
  
    //sinfo_msg("Max wave=%g",cpl_vector_get_max(vec_wave));
    /*
    check_nomsg(
        tab_corr=sinfo_table_downsample_table(tab_med,"wave","fit",table_rs,"wave","fit"));
    */
    /*
    int nrow=cpl_table_get_nrow(tab_corr);
    */
    double* wave=NULL;
   
    tab_corr=cpl_table_duplicate(table_rs);
    int nrow=cpl_table_get_nrow(tab_corr);
    cpl_table_new_column(tab_corr,"fit",CPL_TYPE_DOUBLE);
    cpl_table_fill_column_window_double(tab_corr, "fit", 0, nrow, 0);

    check_nomsg(pfit=cpl_table_get_data_double(tab_corr,"fit"));
    check_nomsg(wave=cpl_table_get_data_double(tab_corr,"wave"));
    //sinfo_msg("Max wave=%g",cpl_vector_get_max(vec_wave));
    cpl_free(sfit);

    check_nomsg(sfit = sinfo_bspline_interpolate_data_at_pos(pvec_wave, pvec_flux,
                             nsamples+2,wave,nrow));
   
    for (i = 0; i < nrow; i ++) {
       if( !isnan( sfit[i]) && !isinf( sfit[i]) )  {
           pfit[i]=sfit[i];
        }
    }
   
    //check_nomsg(cpl_table_wrap_double(tab_corr,sfit,"fit"));
    
    check_nomsg(sinfo_table_save(tab_corr,NULL,NULL,"ratio_fit_continuum.fits",ext));
    
    //check_nomsg(sinfo_table_save(table_rs,NULL,NULL,"table_rs_check.fits",ext));
   
    /*
     11) divide telluric corrected spectrum by continuum fit
     */
    check_nomsg(cpl_table_duplicate_column(tab_corr, "correction", table_rs, "ratio"));
    
    check_nomsg(cpl_table_divide_columns(tab_corr, "correction", "fit"));
    snprintf(fname,MAX_NAME_SIZE,"spectrum_obs_corrected.fits");
    check_nomsg(sinfo_table_save(tab_corr,NULL,NULL,fname,ext));
    
	check_nomsg(sinfo_table_save(tab_corr,NULL,NULL,"ratio_quality.fits",ext));

   
    cpl_table_unwrap(tab_med,"wave");
    cpl_table_unwrap(tab_med,"ratio");
    cpl_table_unwrap(tab_med,"fit");
    sinfo_free_table(&tab_med);
    sinfo_free_table(&table_rs);
    cpl_vector_delete(vec_wave);
    cpl_vector_delete(vec_flux);

    cpl_free(sfit);

    sinfo_print_rec_status(151);
                     sinfo_msg_warning("vk151");
    /*
     13) find  a way to identify the best correction from the residuals
     (I tried some statistical tests, but am not really convinced)
     */
    check_nomsg(sinfo_evaluate_tell_model(tab_corr,band,ext,&mean,&rms));
    sinfo_print_rec_status(152);
                     sinfo_msg_warning("vk152");
    sinfo_msg_warning("info mean=%g rms=%g ext=%d",mean,rms,ext+1);
    qc_head=cpl_propertylist_new();
    cpl_propertylist_append_double(qc_head,SINFO_QC_TELLCORR_RATAVG,mean);
    cpl_propertylist_append_double(qc_head,SINFO_QC_TELLCORR_RATRMS,rms);
    //cpl_propertylist_dump(qc_head,stdout);
    check_nomsg(sinfo_table_save(tab_corr,NULL,qc_head,fname,ext));

    cpl_msg_warning("","::::::MODELS: ext=%d mean=%g rms=%g", ext,mean, rms);
    sinfo_free_propertylist(&qc_head);
    sinfo_free_table(&ext_tab);
    sinfo_free_table(&tab_corr);
    pmean[ext]=mean;
    prms[ext]=rms;
    pext[ext]=ext;
    sinfo_free_table(&table_rs_div_mc);
    sinfo_print_rec_status(16);
                  sinfo_msg_warning("vk16");
  }
 
  sinfo_print_rec_status(17);
                sinfo_msg_warning("vk17");
  double model_mean=0;
  double model_rms=0;
  int status=0;
  check_nomsg(model_mean=cpl_table_get_column_min(tab_res,"mean"));
  //check_nomsg(sinfo_table_save(tab_res,NULL,NULL,"tab_res.fits",0));
  //sinfo_msg("optimal model mean %g",model_mean);
  cpl_table_get_column_minpos(tab_res,"mean",model_idx);
  check_nomsg(model_rms=cpl_table_get_double(tab_res,"rms",*model_idx,&status));
  sinfo_msg("optimal model: mean=%g rms=%g",model_mean,model_rms);
  cpl_msg_warning("", "optimal model id %d mean %g rms %g",
      (int)(*model_idx),model_mean,model_rms);

  /* load finally optimal solution */
  //sinfo_msg("fname=%s",fname);
  snprintf(fname,MAX_NAME_SIZE,"spectrum_observed_divided_by_model_convolved.fits");
  //xsh_add_temporary_file(fname);
  ext=(int)(*model_idx);
  sinfo_msg("fname=%s ext=%d",fname,ext);
  check_nomsg(table_rs=cpl_table_load(fname,ext+1,0));
  //sinfo_msg("table_rs nrow=%d",(int)cpl_table_get_nrow(table_rs));
  qc_head=cpl_propertylist_new();
  sinfo_print_rec_status(18);
                sinfo_msg_warning("vk18");
  cpl_propertylist_append_double(qc_head,SINFO_QC_TELLCORR_RATAVG,mean);
  cpl_propertylist_append_double(qc_head,SINFO_QC_TELLCORR_RATRMS,rms);
  cpl_propertylist_append_int(qc_head,SINFO_QC_TELLCORR_OPTEXTID,ext+1);
  //cpl_propertylist_dump(qc_head,stdout);
  //snprintf(fname,MAX_NAME_SIZE,"model_optimal.fits");
 
  check_nomsg(sinfo_table_save(table_rs,NULL,qc_head,fname,0));
 
  //cpl_table_dump(table_rs,1,2,stdout);
  

  //sinfo_msg("table_rm nrow=%d",(int)cpl_table_get_nrow(table_rm));
  //cpl_table_dump(table_rm,1,2,stdout);
  //sinfo_msg("ok1");
  sinfo_print_rec_status(19);
                sinfo_msg_warning("vk19");
  //check_nomsg(sinfo_table_save(table_rs,NULL,NULL,"check.fits",0));
  check_nomsg(tab_corr=sinfo_table_downsample_table(table_rs,"wave","ratio",table_rm,"wavelength","ratio"));
 
  //sinfo_msg("tab_cor nrow=%d",(int)cpl_table_get_nrow(tab_corr));
  //cpl_table_dump(tab_corr,1,2,stdout);
  check_nomsg(sinfo_table_save(tab_corr,NULL,NULL,"selected_corrected_modigli.fits",0));
  
 cleanup:
 
  sinfo_print_rec_status(100);
  sinfo_free_propertylist(&qc_head);
  sinfo_free_table(&table_rs);
  sinfo_free_table(&tab_res);
  sinfo_free_table(&table_s);
  sinfo_free_table(&table_rm);
  return tab_corr;
}

static cpl_error_code
sinfo_rv_ref_wave_init(sinfo_std_star_id std_star_id ,new_sinfo_band band,
		sinfo_rv_ref_wave_param* w){

    switch (band) {
    
    case INSTRUMENT_BAND_UVB:
      w->wguess = 486.1322;
      w->range_wmin=450;
      w->range_wmax=550;
      w->ipol_wmin_max=470;
      w->ipol_wmax_min=500;
      w->ipol_hbox=0.5;

      break;
    case INSTRUMENT_BAND_VIS:
      w->wguess= 656.2793;
      w->range_wmin=640;
      w->range_wmax=670;
      w->ipol_wmin_max=653;
      w->ipol_wmax_min=661;
      w->ipol_hbox=1.5;

      break;
    case INSTRUMENT_BAND_NIR:
       w->wguess= 1094.12;
       w->range_wmin=1000;
       w->range_wmax=1200;
       w->ipol_wmin_max=1090;
       w->ipol_wmax_min=1100;
       w->ipol_hbox=1.5;
       cpl_msg_warning("", "NIIIIR");
       break;
    case INSTRUMENT_BAND_ALL:
       w->wguess= 1094.12;
       w->range_wmin=1000;
       w->range_wmax=1200;
       w->ipol_wmin_max=1090;
       w->ipol_wmax_min=1100;
       w->ipol_hbox=1.5;
       cpl_msg_warning("", "SYNTHETIC");
       break;
    case INSTRUMENT_BAND_J:
        w->wguess= 1094.12;
        w->range_wmin=1000;
        w->range_wmax=1200;
        w->ipol_wmin_max=1090;
        w->ipol_wmax_min=1100;
        w->ipol_hbox=1.5;
        cpl_msg_warning("", "JJJJJJ");
        break;
        
    case INSTRUMENT_BAND_H:
        w->wguess= 1094.12;
        w->range_wmin=1000;
        w->range_wmax=1200;
        w->ipol_wmin_max=1090;
        w->ipol_wmax_min=1100;
        w->ipol_hbox=1.5;
        break;
        
    case INSTRUMENT_BAND_K:
        w->wguess= 1094.12;
        w->range_wmin=1000;
        w->range_wmax=1200;
        w->ipol_wmin_max=1090;
        w->ipol_wmax_min=1100;
        w->ipol_hbox=1.5;
        break;
        
    case INSTRUMENT_BAND_HK:
        w->wguess= 1094.12;
        w->range_wmin=1000;
        w->range_wmax=1200;
        w->ipol_wmin_max=1090;
        w->ipol_wmax_min=1100;
        w->ipol_hbox=1.5;
        break;

    default :
        w->wguess= 1094.12;
        w->range_wmin=1000;
        w->range_wmax=1200;
        w->ipol_wmin_max=1090;
        w->ipol_wmax_min=1100;
        w->ipol_hbox=1.5;
        break;
    }

  return cpl_error_get_code();
}

static sinfo_rv_ref_wave_param * sinfo_rv_ref_wave_param_create(void){

  sinfo_rv_ref_wave_param * self = cpl_malloc(sizeof(sinfo_rv_ref_wave_param));
  self->wguess=999.;    /* Reference line wavelength position */
  self->range_wmin=999.; /* minimum of wavelength box for line fit */
  self->range_wmax=999.; /* maximum of wavelength box for line fit */
  self->ipol_wmin_max=999.; /* maximum lower sub-range wavelength value used to fit line slope */
  self->ipol_wmax_min=999.; /* minimum upper sub-range wavelength value used to fit line slope */

  return self;
}

static cpl_error_code
sinfo_std_star_spectra_to_vector_range(sinfo_star_flux_list * obs_std_star_list,
                                     const double wmin,
                                     const double wmax,
                                     cpl_vector** vec_wave,
                                     cpl_vector** vec_flux)
{
    double* pwave_in=NULL;
    double* pflux_in=NULL;

    double* pwave_ou=NULL;
    double* pflux_ou=NULL;

    int size=0;
    int i=0;
    int k=0;
   
    pwave_in=obs_std_star_list->lambda;
    pflux_in=obs_std_star_list->flux;
    size=obs_std_star_list->size;
    
    *vec_wave=cpl_vector_new(size);
    *vec_flux=cpl_vector_new(size);
    pwave_ou=cpl_vector_get_data(*vec_wave);
    pflux_ou=cpl_vector_get_data(*vec_flux);
    /*
    sinfo_msg_warning("ek3 wmin=%g wmax=%g pwave: min=%g max=%g",
                      wmin,wmax,pwave_in[0],pwave_in[size-1]);
                      */
        
    for(i=0;i<size;i++) {
        if(pwave_in[i]>=wmin && pwave_in[i]<= wmax) {
            pwave_ou[k]=pwave_in[i];
            pflux_ou[k]=pflux_in[i];
            k++;
        }
    }
    cpl_vector_set_size(*vec_wave,k);
    cpl_vector_set_size(*vec_flux,k);
    
    return cpl_error_get_code();
}


static cpl_polynomial *
sinfo_polynomial_fit_1d_create(
                   const cpl_vector    *   x_pos,
                   const cpl_vector    *   values,
                   int                     degree,
                   double              *   mse
                   )
{
    cpl_polynomial * fit1d = cpl_polynomial_new(1);
//    cpl_vector* x_copy = cpl_vector_duplicate(x_pos);
//    cpl_vector* values_copy = cpl_vector_duplicate(values);
    int x_size = cpl_vector_get_size(x_pos);
    double rechisq = 0;
    cpl_size loc_deg=(cpl_size)degree;
    cpl_matrix     * samppos = cpl_matrix_wrap(1, x_size,
                                               (double*)cpl_vector_get_data_const(x_pos));
    cpl_vector     * fitresidual = cpl_vector_new(x_size);

    cpl_polynomial_fit(fit1d, samppos, NULL, values, NULL,
                       CPL_FALSE, NULL, &loc_deg);
    cpl_ensure(!cpl_error_get_code(), cpl_error_get_code(), NULL);
    
    if ( x_size > (degree + 1) )  {  
      cpl_vector_fill_polynomial_fit_residual(fitresidual, values, NULL, fit1d,
                          samppos, &rechisq);
      cpl_ensure(!cpl_error_get_code(), cpl_error_get_code(), NULL);
    }
    if (mse)  
    {
        *mse = cpl_vector_product(fitresidual, fitresidual)
            / cpl_vector_get_size(fitresidual);
    }
    cpl_matrix_unwrap(samppos);
    cpl_vector_delete(fitresidual);
    return fit1d;
}



/*
  @brief generates a vector of values defined at each input wave pos that is a linear fit of values in range
         (wave[0],wmin_max)---(wmax_min,wave[max])
  @param vec_wave wave data
  @param vec_flux flux data
  @param wmin_max max value of start wave range up to which use data for fit of continuum
  @param wmax_min min value of final wave range from which use data for fit of continuum
  @return fit of continuum data vector
 * */
static cpl_vector*
sinfo_vector_fit_slope(cpl_vector* vec_wave,cpl_vector* vec_flux,
                       const double wmin_max,const double wmax_min,
                       const int degree)
{
  cpl_vector* vec_slope=NULL;
  int i=0;
  int size=0;
  int k=0;
  double* pwave=NULL;
  double* pflux=NULL;
  double* pslope=NULL;
  double* pvec_x=NULL;
  double* pvec_y=NULL;
  double mse=0;
  cpl_polynomial* pfit=NULL;

  cpl_vector* vec_x=NULL;
  cpl_vector* vec_y=NULL;

  /* input parameters consistency checks */
  cpl_ensure(vec_wave,CPL_ERROR_NULL_INPUT,NULL);
  cpl_ensure(vec_flux,CPL_ERROR_NULL_INPUT,NULL);
  cpl_ensure(wmin_max<wmax_min,CPL_ERROR_INCOMPATIBLE_INPUT,NULL);
  cpl_ensure(degree>0 && degree < 3,CPL_ERROR_INCOMPATIBLE_INPUT,NULL);

  /* extract from vector values that are supposed to follow linear relation */
  size=cpl_vector_get_size(vec_flux);
  vec_x=cpl_vector_new(size);
  vec_y=cpl_vector_new(size);
  pwave=cpl_vector_get_data(vec_wave);
  pflux=cpl_vector_get_data(vec_flux);
  pvec_x=cpl_vector_get_data(vec_x);
  pvec_y=cpl_vector_get_data(vec_y);

  for(i=0;i<size;i++) {
    if( pwave[i] <= wmin_max || pwave[i] >= wmax_min ) {
      pvec_x[k]=pwave[i];
      pvec_y[k]=pflux[i];
      k++;
    }
  }

  cpl_vector_set_size(vec_x,k);
  cpl_vector_set_size(vec_y,k);

  cpl_msg_warning("", "SELECTION: %f %f", wmin_max, wmax_min);

  /* makes linear fit */
  pfit=sinfo_polynomial_fit_1d_create(vec_x,vec_y,degree,&mse);

  /* define results */
  vec_slope=cpl_vector_new(size);
  pslope=cpl_vector_get_data(vec_slope);
  for(i=0;i<size;i++) {
    pslope[i] = cpl_polynomial_eval_1d( pfit, pwave[i], NULL);
  }

 cpl_vector_delete(vec_x);
 cpl_vector_delete(vec_y);
 sinfo_free_polynomial(&pfit);
 return vec_slope;
}


/* selects a sub range in a star line spectrum */
static cpl_error_code
sinfo_select_line_core(cpl_vector* wave,cpl_vector* flux,const double wguess,const double wrange,cpl_vector** xfit,cpl_vector** yfit)
{

    int size=0;
    double* pxfit=NULL;
    double* pyfit=NULL;
    double* pwave=NULL;
    double* pflux=NULL;
    int i=0;
    int k=0;

    size=cpl_vector_get_size(wave);
    *xfit=cpl_vector_new(size);
    *yfit=cpl_vector_new(size);
    pxfit=cpl_vector_get_data(*xfit);
    pyfit=cpl_vector_get_data(*yfit);

    pwave=cpl_vector_get_data(wave);
    pflux=cpl_vector_get_data(flux);

    k=0;
    for(i=0;i<size;i++) {
        if(pwave[i]>=(wguess-wrange) &&
                        pwave[i]<=(wguess+wrange) ) {

            pxfit[k]=pwave[i];
            pyfit[k]=pflux[i];
            k++;
        }
    }
    check_nomsg(cpl_vector_set_size(*xfit,k));
    check_nomsg(cpl_vector_set_size(*yfit,k));

    cleanup:
    return cpl_error_get_code();
}

/*---------------------------------------------------------------------------*/
/**
 * @brief    Sort an array @a u1 of doubles, and permute an array @a u2
 *               in the same way as @a u1 is permuted.
 * @param[in,out]   u1   Pointer to the first array.
 * @param[in,out]   u2   Pointer to the second array.
 * @param    n           The common length of both arrays.
 *
 * @return   @c CPL_ERROR_NONE or the appropriate error code.
 *
 */
/*---------------------------------------------------------------------------*/
static cpl_error_code sinfo_sort_double_pairs(double *u1, double *u2, cpl_size n)
{
    cpl_vector * biu1=NULL;
    cpl_vector * biu2=NULL;
    cpl_bivector * bi_all=NULL;

    if (n < 1)
        return cpl_error_set(cpl_func, CPL_ERROR_ILLEGAL_INPUT);
    cpl_ensure_code(u1 != NULL, CPL_ERROR_NULL_INPUT);
    cpl_ensure_code(u2 != NULL, CPL_ERROR_NULL_INPUT);


    biu1=cpl_vector_wrap(n,u1);
    biu2=cpl_vector_wrap(n,u2);

    bi_all=cpl_bivector_wrap_vectors(biu1, biu2);
    cpl_bivector_sort(bi_all,bi_all, CPL_SORT_ASCENDING,CPL_SORT_BY_X);


    /* cleaning up */

    cpl_bivector_unwrap_vectors(bi_all);
    cpl_vector_unwrap(biu1);
    cpl_vector_unwrap(biu2);

    return CPL_ERROR_NONE;
}

static double
sinfo_std_star_spectra_correlate(sinfo_star_flux_list * obs_std_star_list,
                               sinfo_star_flux_list * ref_std_star_list,sinfo_rv_ref_wave_param* w)

{

    double wave_shift = 0;
    int naxis1 = 0;
    double wstep = 0;
    double wmin = 0;
    double wmax = 0;

    /*
  double* prw = 0;
  double* prf = 0;
  double* pow = 0;
  double* pof = 0;
  double* pvrf = 0;
  double* pvof = 0;
     */
    //double* pvrw = 0;

    //double* pvow = 0;

    double hbox_wave=0.5;
    int hbox = (int)(hbox_wave/wstep+0.5);
    //int hbox = 500;


    int i=0;
    /*
  int k=0;
  int box=2*hbox+1;
  int size=0;
     */
    //UFFA

    sinfo_msg_warning("ck0 obs_std_star_list=%p",obs_std_star_list);
    sinfo_msg_warning("ck0 obs_std_star_list header=%p",obs_std_star_list->header);
    //cpl_propertylist_dump(obs_std_star_list->header,stdout);
    sinfo_print_rec_status(0);
    naxis1 = sinfo_pfits_get_naxis1(obs_std_star_list->header);
    wstep = sinfo_pfits_get_cdelt1(obs_std_star_list->header);
    //wmin = sinfo_pfits_get_crval1(obs_std_star_list->header);

    //wmax = wmin + naxis1 * wstep;
    //wmin=w->wguess-hbox*wstep;
    //wmax=w->wguess+(hbox+1)*wstep;

    //sinfo_msg("hbox=%d",hbox);
    /* wrap the observed std star spectrum into a vector */
    cpl_vector* vec_wave_obs = NULL;
    cpl_vector* vec_flux_obs = NULL;
    cpl_vector* vec_slope_obs=NULL;
    cpl_vector* correl=NULL;

    /* we first extract the relevant wavelength range used for cross correlation, covering just one line */
    wmin=w->range_wmin;
    wmax=w->range_wmax;
    //sinfo_msg("wmin=%g wmax=%g",wmin,wmax);
    
    sinfo_std_star_spectra_to_vector_range(obs_std_star_list,wmin,wmax,&vec_wave_obs,&vec_flux_obs);
    
    /*
  check_nomsg(cpl_vector_save(vec_flux_obs,"vec_flux_obs.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check_nomsg(cpl_vector_save(vec_flux_ref,"vec_flux_ref.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
     */

    /* we fit a slope (or parabola) on the flat region of the spectrum */
    double wmin_max=w->ipol_wmin_max;
    double wmax_min=w->ipol_wmax_min;
    //sinfo_msg("wmin_max=%g wmax_min=%g",wmin_max,wmax_min);
    vec_slope_obs=sinfo_vector_fit_slope(vec_wave_obs,vec_flux_obs,wmin_max,wmax_min,2);


	check_nomsg(cpl_vector_save(vec_flux_obs,"vec_flux_obs.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
    check_nomsg(cpl_vector_save(vec_slope_obs,"vec_slope_obs.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));


    /* we divide the spectrum by the slope */
    cpl_vector_divide(vec_flux_obs,vec_slope_obs);

    cpl_vector_delete(vec_slope_obs);

    cpl_vector_add_scalar(vec_flux_obs,2);

    check_nomsg(cpl_vector_save(vec_flux_obs,"vec_slope_obs_divided.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

    /*
  check_nomsg(cpl_vector_save(vec_flux_obs,"vec_flux_obs_corr.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check_nomsg(cpl_vector_save(vec_flux_ref,"vec_flux_ref_corr.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check_nomsg(cpl_vector_save(vec_wave_obs,"vec_wave_obs.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
  check_nomsg(cpl_vector_save(vec_wave_ref,"vec_wave_ref.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

     */

    /* at this points both line spectra (on ref std star and on observed std) have same ranges and a "continuum" */

    /* we now restrict to the line core to limit the effects introduced by the wide line wings */

    double wave_range=w->ipol_hbox;
    cpl_vector* xfit_obs=NULL;
    cpl_vector* yfit_obs=NULL;
    //cpl_vector* xfit_ref=NULL;
    //cpl_vector* yfit_ref=NULL;
    double* pxfit=NULL;
    double* pyfit=NULL;
    /*
  double* pwave=NULL;
  double* pflux=NULL;
     */

    sinfo_select_line_core(vec_wave_obs,vec_flux_obs,w->wguess,wave_range,&xfit_obs,&yfit_obs);
 
    
    check_nomsg(cpl_vector_save(yfit_obs,"vec_slope_obs_divided_selected.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));


    /*
  cpl_vector_save(xfit_obs,"xfit_obs.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  cpl_vector_save(yfit_obs,"yfit_obs.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
     */
    //   sinfo_select_line_core(vec_wave_ref,vec_flux_ref,w->wguess,wave_range,&xfit_ref,&yfit_ref);
 
    /*
  cpl_vector_save(xfit_ref,"xfit_ref.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  cpl_vector_save(yfit_ref,"yfit_ref.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
     */
    /*
  cpl_vector_delete(vec_flux_obs);
  cpl_vector_delete(vec_wave_obs);
     */

    /*
  double ymax_obs=0;
  double ymax_ref=0;
  k=cpl_vector_get_size(yfit_obs);
  pflux=cpl_vector_get_data(yfit_obs);
  ymax_obs=(pflux[0] <= pflux[k-1]) ? pflux[0] : pflux[k-1];

  k=cpl_vector_get_size(yfit_ref);
  pflux=cpl_vector_get_data(yfit_ref);
  ymax_ref=(pflux[0] <= pflux[k-1]) ? pflux[0] : pflux[k-1];
     */
    /*
  int size_x=k;
  int size_x_ref=0;
     */
    int size_x_obs=0;
    /* we now make sure to have a symmetric distribution (because later we get the fit-centroid)
  sinfo_line_balance(&xfit_ref,&yfit_ref,ymax_ref);
  pwave=cpl_vector_get_data(xfit_ref);
  pflux=cpl_vector_get_data(yfit_ref);

  k=0;
  for(i=0;i<size_x;i++) {
    if(pflux[i]<=ymax_ref) {

      pxfit[k]=pwave[i];
      pyfit[k]=pflux[i];
      k++;
    }
  }
  check_nomsg(cpl_vector_set_size(xfit_ref,k));
  cpl_vector_set_size(yfit_ref,k);
  size_x_ref=k;
     */

    /* we now make sure to have a symmetric distribution (because later we get the fit-centroid)
  pwave=cpl_vector_get_data(xfit_obs);
  pflux=cpl_vector_get_data(yfit_obs);

  k=0;
  for(i=0;i<size_x;i++) {
    if(pflux[i]<=ymax_obs) {

      pxfit[k]=pwave[i];
      pyfit[k]=pflux[i];
      k++;
    }
  }
  sinfo_msg(">>>k=%d ymax_obs=%g",k,ymax_obs);
  check_nomsg(cpl_vector_set_size(xfit_obs,k));
  check_nomsg(cpl_vector_set_size(yfit_obs,k));

  size_x_obs=k;
     */


    /* here we try to do median smoothing to have better fit */
    //cpl_vector* yfit_obs_med=cpl_vector_filter_median_create(yfit_obs,3);
    //check_nomsg(cpl_vector_save(yfit_obs_med,"yfit_obs_med.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

    /* because lines cross correlation is not very accurate, here we try to make a poly (parabola)lation
     * fit to have more robust min determination */
    cpl_polynomial* poly_fit_obs=NULL;

    double mse=0;
    double* pindex=NULL;
    poly_fit_obs=sinfo_polynomial_fit_1d_create(xfit_obs,yfit_obs,4,&mse);
    //sinfo_msg("mse=%g",mse);
    cpl_vector* yfit_pol_obs=NULL;
    cpl_vector* yfit_index=NULL;
    size_x_obs=cpl_vector_get_size(xfit_obs);
    yfit_pol_obs=cpl_vector_new(size_x_obs);
    yfit_index=cpl_vector_new(size_x_obs);
    check_nomsg(pyfit=cpl_vector_get_data(yfit_pol_obs));
    check_nomsg(pxfit=cpl_vector_get_data(xfit_obs));
    check_nomsg(pindex=cpl_vector_get_data(yfit_index));

    for(i=0;i<size_x_obs;i++) {
        pyfit[i]=cpl_polynomial_eval_1d(poly_fit_obs,pxfit[i],NULL);
        pindex[i]=i;
    }
    sinfo_free_polynomial(&poly_fit_obs);
    //check_nomsg(cpl_vector_save(yfit_pol_obs,"yfit_pol_obs.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));
    //double ymin=0;
    double xmin=0;
    cpl_vector_save(yfit_obs, "yfit_obs.fits", CPL_TYPE_DOUBLE, NULL, CPL_IO_CREATE);
    cpl_vector_save(yfit_pol_obs, "yfit_pol_obs.fits", CPL_TYPE_DOUBLE, NULL, CPL_IO_CREATE);
    /* we get the parabola min/max */
    sinfo_sort_double_pairs(pyfit,pindex,size_x_obs);
    //cpl_vector_dump(yfit_index,stdout);
    //cpl_vector_dump(yfit_pol_obs,stdout);
    xmin=pxfit[(int)pindex[0]];
    cpl_msg_warning("", "BESST IDX : %i",(int)pindex[0]);
    //ymin=pyfit[0];
    cpl_vector_delete(yfit_index);
    //sinfo_msg("Obs line spectrum poly fit min wave pos=%g value=%g",xmin,ymin);

    /* makes Gaussian fit to obs spectrum
  cpl_vector* sigy_obs=NULL;
  cpl_vector* sigy_ref=NULL;

  sigy_obs=cpl_vector_duplicate(yfit_obs);
  cpl_vector_multiply_scalar(sigy_obs,0.1);

  sigy_ref=cpl_vector_duplicate(yfit_ref);
  cpl_vector_multiply_scalar(sigy_ref,0.1);


  double peakpos_obs=xmin;
  double sigma=1;
  double area=1;
  double offset=0;
  double red_chisq=0;
  cpl_matrix* covariance=NULL;

  check_nomsg(cpl_vector_fit_gaussian(xfit_obs,NULL,yfit_obs,sigy_obs,CPL_FIT_CENTROID,&peakpos_obs,&sigma,&area,&offset,&mse,&red_chisq,&covariance));
  sinfo_msg("sigma=%g area=%g offset=%g",sigma,area,offset);
     */
    double peakpos_obs=xmin;
    double peakpos_ref=w->wguess;
    cpl_msg_warning("", "POSSSS guess: %g obs: %10.8g Pos ref: %10.8g diff=%10.8g",
            w->wguess,peakpos_obs,peakpos_ref,peakpos_obs-peakpos_ref);

    sinfo_msg("Now compute shift by spectra correlation");

    //PIPPO
    /* WE DECIDED NOT TO FIT THE MODEL FOR RV COMPUTATION, ONLY THE OBJECT AND TO MAKE A POLY FIT
     * BECAUSE THE FOLLOWING WAS NOT VERY ACCURATE: IN ANY CASE ONE WOULD HAVE TO FIT A GAUUSS TO
     * THE CORRELATION MAX
     *

  cpl_vector* flux_obs_fine=NULL;
  cpl_vector* flux_ref_fine=NULL;

  flux_obs_fine=xsh_vector_upsample(vec_flux_obs,10);
  flux_ref_fine=xsh_vector_upsample(vec_flux_ref,10);
  cpl_vector_save(flux_obs_fine,"flux_obs_fine.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  cpl_vector_save(flux_ref_fine,"flux_ref_fine.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);

  cpl_size shift=0;
  hbox=5000;
  hbox=cpl_vector_get_size(vec_flux_ref);
  correl=cpl_vector_new(2*hbox+1);

  check_nomsg(shift=cpl_vector_correlate(correl,vec_flux_obs,vec_flux_ref));
  //UFFA
  cpl_vector_save(correl,"correl.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);

  double* flux_o =cpl_vector_get_data(flux_obs_fine);
  double* flux_r =cpl_vector_get_data(flux_ref_fine);
  int width_i=2*hbox+1;
  int width_t=2*hbox+1;
  int half_search=(int)(0.5*hbox+1);
  double delta=0;
  double corr=0;
  double* xcorr=NULL;


  XSH_GAUSSIAN_FIT gfit;
  int size_o=0;
  size_o = cpl_vector_get_size(flux_obs_fine);
  double step=(wmax-wmin)/size_o;

  check_nomsg(sinfo_correl_spectra(flux_o,flux_r,size_o,hbox,step,0.1,0,&gfit));

  sinfo_msg("sigma=%g",gfit.sigma);
  sinfo_msg("computed shift[lognm]=%g",(gfit.peakpos-hbox*step));

  hbox=(int)3.*CPL_MATH_FWHM_SIG * gfit.sigma/step;
  sinfo_msg("hsearch=%d",hbox);
  check_nomsg(xsh_correl_spectra(flux_o,flux_r,size_o,hbox,step,0.1,0,&gfit));




  //check_nomsg(xcorr=xsh_function1d_xcorrelate(line_i,width_i,line_t,width_t,half_search,0,&corr,&delta));
  //cpl_vector_save(xcorr,"xcorr.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT);
  sinfo_msg("RV corr=%g delta=%g shift=%g",corr,delta,delta*wstep);

  xsh_free(xcorr);
     */
    /*
  check_nomsg(correl_max=cpl_vector_get_max(correl));
  sinfo_msg("correl_max=%g",correl_max);
  check_nomsg(correl_pos=cpl_vector_find(correl,correl_max));
  sinfo_msg("correl: shift=%d max=%g val(shift)=%g pos=%d",
      (int)shift,correl_max,cpl_vector_get(correl,shift+1),(int)correl_pos);
  wave_shift=delta*wstep;
     */
    wave_shift=peakpos_obs-peakpos_ref;
    //check_nomsg(cpl_vector_save(correl,"correl.fits",CPL_BPP_IEEE_FLOAT,NULL,CPL_IO_DEFAULT));

    cleanup:
   
    cpl_vector_delete(yfit_pol_obs);
    cpl_vector_delete(xfit_obs);
    cpl_vector_delete(yfit_obs);
    //cpl_vector_delete(xfit_ref);
    //cpl_vector_delete(yfit_ref);
    cpl_vector_delete(vec_wave_obs);
    cpl_vector_delete(vec_flux_obs);
    cpl_vector_delete(correl);

    return wave_shift;
}

static cpl_table*
sinfo_table_shift_rv(cpl_table* orig, const char* col_wave,const double offset) {
  cpl_table* shifted = NULL;
  double* pwav = NULL;
  int nrows=0;
  int i=0;
  SINFO_ASSURE_NOT_NULL_MSG(orig, "Null input table");

  shifted=cpl_table_duplicate(orig);
  pwav = cpl_table_get_data_double(shifted, col_wave);
  nrows=cpl_table_get_nrow(shifted);
  for(i=0;i<nrows;i++){
    pwav[i] *= (1.+offset);
  }
cleanup:
  return shifted;
}


static void sinfo_atmos_ext_list_free( sinfo_atmos_ext_list ** list )
{
  if ( list != NULL && *list != NULL ) {
    check_nomsg( cpl_free( (*list)->lambda ) ) ;
    check_nomsg( cpl_free( (*list)->K ) ) ;
    check_nomsg( cpl_free( *list ) ) ;
    *list = NULL ;
  }

 cleanup:
  return ;
}

/**
 * This function corrects a spectrum for contributes due to atmospheric extinction, airmass,exptime,gain
 *
 * @param star_list Table of ref star flux
 * @param spectrum Table of observed star flux
 * @param atmos_K  atmospheric extinction curve
 * @param the_arm observing arm
 * @param airmass  airmass value
 * @param exptime  exptime (dit*ndit) value
 * @param gain     gain value
 * @param obj_list Table of observed star flux corrected by atmospheric extinction, airmass,exptime,gain
 *
 * @return cpl error code
 */

static void
save_spectrum_debug(const cpl_image * img, const char * fname){

	cpl_size sz = cpl_image_get_size_x(img);
	cpl_vector * v = cpl_vector_new(sz);
	for(cpl_size  i = 0; i < sz; ++i){
		int rej = 0;
		double d = cpl_image_get(img, i + 1, 1, &rej);
		cpl_vector_set(v, i, d);
	}

	cpl_vector_save(v, fname, CPL_TYPE_DOUBLE, NULL, CPL_IO_CREATE);
	cpl_vector_delete(v);
}

static void
save_double_debug(double * d, cpl_size sz, const char * fname){
	cpl_vector * v = cpl_vector_wrap(sz, d);
	cpl_vector_save(v, fname, CPL_TYPE_DOUBLE, NULL, CPL_IO_CREATE);
	cpl_vector_unwrap(v);
}

static cpl_error_code
sinfo_spectrum_correct(
                sinfo_star_flux_list * star_list,
                sinfo_spectrum * spectrum,
                double * atmos_K,
                new_sinfo_band  band,
                double airmass,
                double exptime,
                double gain,
                sinfo_star_flux_list ** obj_list)
{

	cpl_msg_warning("", "COO++++RECT: %f %f %f", airmass, exptime, gain);

	save_spectrum_debug(spectrum->flux, "sinfo_spectrum_correct_spectrum.fits");
	save_double_debug(atmos_K, star_list->size, "atmos_K.fits");
	save_double_debug(star_list->flux, star_list->size, "star_list.fits");

    double lambda=0;
    double kvalue=0;


    double * flux_added = NULL ;
    double * lambda_star = NULL;
    double  * flux_star = NULL ;

    double * lambda_spectrum = NULL;
    double * plambda=NULL ;
    double * flux_spectrum = NULL ;
    //int * qual_spectrum = NULL ;

    int i=0;
    int  size=0;
    int nlambda_spectrum=0;
    double lambda_spectrum_min=0;
    double lambda_spectrum_max=0;
    double lambda_star_min=0;
    double lambda_star_max=0;
    double step_spectrum=0 ;
    double step_star=0 ;
    int npix_in_interval=0 ;
    int bin_size=1;
    int binx=1;
    int biny=1;
    cpl_frame* obj_integrated=NULL;
    
    check_nomsg( nlambda_spectrum = sinfo_spectrum_get_size( spectrum ) ) ;
    check_nomsg( flux_spectrum = sinfo_spectrum_get_flux( spectrum ) ) ;
    //check_nomsg( qual_spectrum = sinfo_spectrum_get_qual( spectrum ) ) ;
    check_nomsg( lambda_spectrum_min = sinfo_spectrum_get_lambda_min( spectrum ) ) ;
    check_nomsg( lambda_spectrum_max = sinfo_spectrum_get_lambda_max( spectrum ) ) ;
    check_nomsg( step_spectrum = sinfo_spectrum_get_lambda_step( spectrum ) ) ;
  
    /* In NIR, no binning ==> set 1x1 */
    /* TODO : for non-NIR cases we need the binning */
    //if ( the_arm != XSH_ARM_NIR ) {
        /* we correct for bin size:
         * bin along wave direction* bin along spatial direction
         */
    //TODO: to be changed to support UVB,VIS
        //check_nomsg(binx = sinfo_pfits_get_binx( spectrum->flux_header )) ;
        //check_nomsg(biny = sinfo_pfits_get_biny( spectrum->flux_header )) ;
        bin_size=binx*biny;
    //}

       
    flux_star = star_list->flux ;
    size = star_list->size ;
    lambda_star = star_list->lambda ;


    lambda_star_min = *lambda_star ;
    lambda_star_max = *(lambda_star+size-1) ;
    step_star = (lambda_star_max-lambda_star_min)/(size-1) ;

    sinfo_msg( "Spectrum  - size %d Nlambda: %d, Min: %lf, Max: %lf, Step: %lf",
             size,nlambda_spectrum, lambda_spectrum_min, lambda_spectrum_max,
             step_spectrum ) ;
    sinfo_msg( "           - BinX: %d, BinY: %d", binx, biny ) ;
   

    SINFO_CALLOC( flux_added, double, size ) ;
    /* For each lambda in star_list
      if there is such a lambda in rec_list Then
      integrate (add) the flux from (lambda-delta/2) to (lambda+delta/2)
      calculate ratio rec_add_flux/star_flux
      endif
      EndFor
     */
    /* First create lambda_spectrum array */
    SINFO_CALLOC( lambda_spectrum, double, nlambda_spectrum ) ;

    for( lambda = lambda_spectrum_min, plambda = lambda_spectrum, i = 0 ;
                    i<nlambda_spectrum ;
                    i++, plambda++, lambda += step_spectrum )
    {
        *plambda = lambda ;
    }

    /* Create response list (identical to star_list ?) */
    //sinfo_msg("step_star=%g step_spectrum=%g", step_star,step_spectrum);

    npix_in_interval = step_star/step_spectrum ;
    cpl_msg_warning("", ":::::::::::::::::::::  step_star/step_spectrum: %f, step_star %f, step_spectrum %f", step_star/step_spectrum, step_star, step_spectrum ) ;
   
    /* for debug purposes uses stores results in a table */
    cpl_table* debug = NULL;
    double* pwav = NULL;
    double* pflux_obs = NULL;
    double* pflux_ref = NULL;
    double* pflux_corr=NULL;
    double* pkavalue = NULL;
    double* pobj_atm_corr = NULL;
    double* pobj_bin_cor = NULL;
    double cor_fct = 0;
    debug = cpl_table_new(size);

    cpl_table_new_column(debug, "wavelength", CPL_TYPE_DOUBLE);
    cpl_table_new_column(debug, "flux_obs", CPL_TYPE_DOUBLE);
    cpl_table_new_column(debug, "flux_ref", CPL_TYPE_DOUBLE);
    cpl_table_new_column(debug, "flux_obs_cor_gain_bin_exptime", CPL_TYPE_DOUBLE);

    cpl_table_new_column(debug, "kvalue", CPL_TYPE_DOUBLE);
    cpl_table_new_column(debug, "flux_obs_cor_atm", CPL_TYPE_DOUBLE);
    cpl_table_new_column(debug, "flux_obs_cor_obs2ref_bin_size", CPL_TYPE_DOUBLE);
 
    cpl_table_fill_column_window_double(debug, "wavelength", 0, size, 0.);
    cpl_table_fill_column_window_double(debug, "flux_obs", 0, size,0.);
    cpl_table_fill_column_window_double(debug, "flux_obs_cor_gain_bin_exptime", 0,size, 0.);
    cpl_table_fill_column_window_double(debug, "flux_ref", 0, size, 0.);
    cpl_table_fill_column_window_double(debug, "kvalue", 0, size, 0.);
    cpl_table_fill_column_window_double(debug, "flux_obs_cor_atm", 0, size,0.);
    cpl_table_fill_column_window_double(debug, "flux_obs_cor_obs2ref_bin_size", 0, size,0.);

    pwav = cpl_table_get_data_double(debug, "wavelength");
    pflux_obs = cpl_table_get_data_double(debug, "flux_obs");
    pflux_corr = cpl_table_get_data_double(debug, "flux_obs_cor_gain_bin_exptime");
    pflux_ref = cpl_table_get_data_double(debug, "flux_ref");
    pkavalue = cpl_table_get_data_double(debug, "kvalue");
    pobj_atm_corr = cpl_table_get_data_double(debug, "flux_obs_cor_atm");
    pobj_bin_cor = cpl_table_get_data_double(debug, "flux_obs_cor_obs2ref_bin_size");
    check_nomsg(*obj_list = sinfo_star_flux_list_create( size ));
  
    cor_fct = gain * exptime * bin_size;
    //sinfo_msg("cor factor=%g",cor_fct);
    sinfo_msg("cor lambda sampling: %g %g %g",step_star/step_spectrum,step_star,step_spectrum );
    /* Loop over lamba_star */
    for ( i = 0 ; i < size ; i++ ) {
        /* correct for gain,bin,exptime */
        /* TODO: We should use only good pixels  */
        (*obj_list)->flux[i] =   flux_spectrum[i]/cor_fct ;
        (*obj_list)->lambda[i] = lambda_star[i] ;

        pwav[i]=lambda_star[i];
        pflux_corr[i]=(*obj_list)->flux[i];
        pflux_obs[i]=flux_spectrum[i];
        pflux_ref[i]=flux_star[i];

        /* Correct for atmos ext*/
        if ( atmos_K != NULL ) {
            kvalue = pow(10., -airmass*atmos_K[i]*0.4 ) ;
            (*obj_list)->flux[i] /= kvalue ;
            pobj_atm_corr[i]=(*obj_list)->flux[i];
            pkavalue[i]=kvalue;
            //sinfo_msg("resp: %g kvalue %g k %g",(*obj_list)->flux[i],kvalue,atmos_K[i]);
        }

        /* Multiply by ratio star_binning/spectrum_binning */
        (*obj_list)->flux[i] *= step_star/step_spectrum ;
        pobj_bin_cor[i]=(*obj_list)->flux[i];

    }

    check_nomsg(cpl_table_duplicate_column(debug,"response",debug,"flux_ref"));

    check_nomsg(cpl_table_divide_columns(debug,"response","flux_obs_cor_obs2ref_bin_size"));
    //check_nomsg(cpl_table_save(debug,NULL,NULL,"debug.fits",CPL_IO_DEFAULT));
    sinfo_free_table(&debug);

    check_nomsg(obj_integrated=sinfo_star_flux_list_save(*obj_list,"obj_list_corrected.fits","TEST")) ;
    
  
    cleanup:

    cpl_free( lambda_spectrum ) ;
    cpl_free( flux_added ) ;
    sinfo_free_frame(&obj_integrated);
    sinfo_free_table(&debug);

	save_double_debug((*obj_list)->flux, (*obj_list)->size, "obj_list.fits");


    return cpl_error_get_code();

}




static sinfo_atmos_ext_list * sinfo_atmos_ext_list_load( cpl_frame * ext_frame )
{
  cpl_table *table = NULL ;
  const char * tablename = NULL ;
  sinfo_atmos_ext_list * result = NULL ;
  int nentries, i ;
  double * plambda, * pK ;

  /* verify input */
  SINFO_ASSURE_NOT_NULL( ext_frame);

  /* get table filename */
  check_nomsg(tablename = cpl_frame_get_filename( ext_frame ));

  /* Load the table */
  SINFO_TABLE_LOAD( table, tablename ) ;
  check_nomsg( nentries = cpl_table_get_nrow( table ) ) ;

  /* Create the list */
  check_nomsg( result = sinfo_atmos_ext_list_create( nentries ) ) ;

  plambda = result->lambda ;
  pK = result->K ;
  if(!cpl_table_has_column(table,SINFO_ATMOS_EXT_LIST_COLNAME_K)){
     sinfo_msg_warning("You are using an obsolete atm extinction line table");
     cpl_table_duplicate_column(table,SINFO_ATMOS_EXT_LIST_COLNAME_K,
                                table,SINFO_ATMOS_EXT_LIST_COLNAME_OLD);
  }
  /* Fill the list */
  for( i = 0 ; i< nentries ; i++, plambda++, pK++ ) {
    float value ;

    check_nomsg( sinfo_get_table_value( table, SINFO_ATMOS_EXT_LIST_COLNAME_WAVELENGTH,
                CPL_TYPE_FLOAT, i, &value ) ) ;
    *plambda = value ;
    check_nomsg( sinfo_get_table_value( table, SINFO_ATMOS_EXT_LIST_COLNAME_K,
                CPL_TYPE_FLOAT, i, &value ) ) ;
    *pK = value ;
  }

 cleanup:
  if (cpl_error_get_code () != CPL_ERROR_NONE) {
    sinfo_msg_error("can't load frame %s",cpl_frame_get_filename(ext_frame));
    sinfo_atmos_ext_list_free(&result);
  }
  cpl_table_delete( table);
  return result ;
}
/** 
 * Interpolate atms extinction at same sampling step as ref std star 
 * 
 * @param star_list Table of ref star flux
 * @param[in] atmos_ext_tab atmospheric extinction table
 * @param[in] instrument instrument arm and lamp setting
 * @param[out] atmos_lambda interpolated wave array
 * @param[out] atmos_K      interpolated atmospheric extinction K
 * @return cpl error code
 */

static cpl_error_code 
sinfo_interpolate_atm_ext(sinfo_star_flux_list * star_list,
                        cpl_table*atmos_ext_tab,
                        new_sinfo_band band, 
                        double** atmos_lambda, 
                        double** atmos_K)
{

    int nrow=0;
    double* pk=0;
    double* pw=0;
    int j=0 ;
    int nlambda_star=0 ;
    double * lambda_star = NULL;

    SINFO_ASSURE_NOT_NULL_MSG (star_list,"null input ref std flux table!");
    SINFO_ASSURE_NOT_NULL_MSG (atmos_ext_tab,"null input atm ext table!");

    nlambda_star = star_list->size ;
    lambda_star = star_list->lambda ;

    /* Fill the atmos_lambda and K with only the subset of lambdas available
      in star list (the std star) */
    SINFO_CALLOC( *atmos_lambda, double, nlambda_star ) ;
    SINFO_CALLOC( *atmos_K, double, nlambda_star ) ;
    if(!cpl_table_has_column(atmos_ext_tab,SINFO_ATMOS_EXT_LIST_COLNAME_K)){
        sinfo_msg_warning("You are using an obsolete atm extinction line table");
        cpl_table_duplicate_column(atmos_ext_tab,SINFO_ATMOS_EXT_LIST_COLNAME_K,
                                   atmos_ext_tab,SINFO_ATMOS_EXT_LIST_COLNAME_OLD);
    }
    cpl_table_cast_column(atmos_ext_tab,SINFO_ATMOS_EXT_LIST_COLNAME_K,"K",CPL_TYPE_DOUBLE);
    cpl_table_cast_column(atmos_ext_tab,"LAMBDA","WAVE",CPL_TYPE_DOUBLE);

    nrow=cpl_table_get_nrow(atmos_ext_tab);
    pw=cpl_table_get_data_double(atmos_ext_tab,"WAVE");
    pk=cpl_table_get_data_double(atmos_ext_tab,"K");
   //PAPERO
    /* X-shooter specific (UVB-VIS) */
    if(band == INSTRUMENT_BAND_UVB ||
       band == INSTRUMENT_BAND_VIS ||
       band == INSTRUMENT_BAND_ALL) {
        for( j = 0 ; j<nlambda_star ; j++) {
            //if(j<10) sinfo_msg("atmo lambda=%g",lambda_star[j] );
            (*atmos_lambda)[j] = lambda_star[j] ;
            (*atmos_K)[j] = sinfo_data_interpolate((*atmos_lambda)[j],nrow,pw,pk);

            //sinfo_msg("w=%g k=%g",atmos_lambda[j],atmos_K[j]);
        }
    } else {

        //SINFONI (NIR) case
        for( j = 0 ; j<nlambda_star ; j++) {

            (*atmos_lambda)[j] = lambda_star[j] ;
            (*atmos_K)[j] = 0;

            //sinfo_msg("w=%g k=%g",atmos_lambda[j],atmos_K[j]);
        }
    }

    cpl_table_erase_column(atmos_ext_tab,"WAVE");
    cpl_table_erase_column(atmos_ext_tab,"K");


    cleanup:
    return cpl_error_get_code();
}


/**
 * Corrects the observed spectrum by gain, exptime, spectral bin size, atmospheric extinction, airmass.
 * at the same wavelengths as the ones at which it is sampled the ref std star
 *
 * @param obs_std_star Rectified frame (1D spectrum)
 * @param ref_std_star_list Ref std star structure
 * @param atmos_ext Atmospheric Extinction Frame
 * @param phigh pointer to high abs region structure
 * @param instrument Instrument structure
 *
 * @return structure with corrected observed std starspectrum
 */
static sinfo_star_flux_list *
sinfo_obs_std_correct(cpl_frame* obs_std_star, sinfo_star_flux_list * ref_std_star_list,
                    cpl_frame* atmos_ext, new_sinfo_band band )
{

    sinfo_star_flux_list * corr_obs_std_star_list=NULL;
    double dRA = 0;
    double dDEC = 0;
    double airmass = 0;
    double exptime=0;
    
    /* extract relevant info (STD RA,DEC,airmass, ID; bin x,y, gain, exptime/DIT) from observed spectrum */
    check_nomsg(sinfo_frame_sci_get_ra_dec_airmass(obs_std_star, &dRA, &dDEC,&airmass));
        
    const char* filename = NULL;
    cpl_propertylist* plist = NULL;
    double gain = 0;

  

    check_nomsg(filename = cpl_frame_get_filename(obs_std_star));
    sinfo_msg_warning("filename=%s",filename);
    check_nomsg(plist = cpl_propertylist_load(filename, 0));
    
    
    /* extract airmass from observed spectrum */
    airmass=sinfo_pfits_get_airm_mean(plist);

    /* we get the bin along the dispersion direction */
    //TODO SINFONI specific
    if (band == INSTRUMENT_BAND_NIR ||
                    band == INSTRUMENT_BAND_J ||
                    band == INSTRUMENT_BAND_H || 
                    band == INSTRUMENT_BAND_K ||
                    band == INSTRUMENT_BAND_HK ||
                    band == INSTRUMENT_BAND_L) {
        /* we assume gain in units of ADU/e- as ESO standard */
        gain = 1. / 2.12;
        //bin = 1;
        sinfo_msg_warning("pk1");
        check_nomsg(exptime=sinfo_pfits_get_dit(plist));
        sinfo_msg_warning("pk2");
    } else if (band == INSTRUMENT_BAND_ALL) {
        gain = 1./2.5;
        exptime=20.;
    } else {
        sinfo_msg_warning("UVB or VIS band");
        check( gain = sinfo_pfits_get_gain(plist), "Could not read gain factor");
        check_nomsg(exptime=sinfo_pfits_get_exptime(filename));
    }
    sinfo_msg_warning("gain=%g exptime=%g",gain,exptime);
    /* load atmospheric extinction frame */
    sinfo_atmos_ext_list * atmos_ext_list = NULL;
    cpl_table* atmos_ext_tab = NULL;

    if (atmos_ext != NULL) {
        check_nomsg( atmos_ext_list = sinfo_atmos_ext_list_load( atmos_ext) );
        filename=cpl_frame_get_filename(atmos_ext);
        atmos_ext_tab=cpl_table_load(filename,1,0);
    } else {
        sinfo_msg_warning(
                        "Missing input %s_%s frame. Skip response and efficiency computation", SINFO_ATMOS_EXT, sinfo_band_tostring(band));
    }
        
    double * atmos_lambda = NULL ;
    double * atmos_K = NULL ;

    check_nomsg(sinfo_interpolate_atm_ext(ref_std_star_list,atmos_ext_tab,
                    band, &atmos_lambda,&atmos_K));

    //XSH_ARM the_arm=xsh_instrument_get_arm(instrument);
 
    sinfo_spectrum * obs_std_spectrum = NULL ; /**< 1d spectrum */
    check_nomsg(obs_std_spectrum = sinfo_spectrum_load_from_table( obs_std_star,
                                "wavelength","counts_bkg"));
        
    //check_nomsg( obs_std_spectrum = sinfo_spectrum_load( obs_std_star));

    check_nomsg(sinfo_spectrum_correct(ref_std_star_list,
                    obs_std_spectrum,atmos_K,band,
                    airmass,exptime,gain,&corr_obs_std_star_list));
   
    cleanup:
    
    sinfo_free_table(&atmos_ext_tab);
    sinfo_atmos_ext_list_free(&atmos_ext_list);
    sinfo_free_propertylist(&plist);
    cpl_free( atmos_lambda ) ;
    cpl_free( atmos_K ) ;

    sinfo_spectrum_free( &obs_std_spectrum ) ;
    return corr_obs_std_star_list;
}


static sinfo_star_flux_list*
sinfo_star_flux_list_duplicate( sinfo_star_flux_list * list )
{
  SINFO_ASSURE_NOT_NULL( list ) ;

  sinfo_star_flux_list* result=NULL;
  int size=list->size;
  /* Create the list */
  result = sinfo_star_flux_list_create(size);

  //check_nomsg(result->header = cpl_propertylist_duplicate(list->header));

  
  memcpy(result->lambda,list->lambda,size * sizeof(double));
  memcpy(result->flux,list->flux,size * sizeof(double));

 cleanup:
  return result;
}


static cpl_vector *
get_windowed(cpl_size start, cpl_size stop, double * v, cpl_size sz){

	cpl_size true_size = 0;
	double * v_data = cpl_calloc(stop - start + 1, sizeof(double));
	for(cpl_size i = start; i <= stop; ++i){
		if(i < 0 || i >= sz) continue;
		if(isnan(v[i]) || isinf(v[i])) continue;
		v_data[true_size] = v[i];
		true_size++;
	}

	if(true_size == 0){
		cpl_free(v_data);
		return NULL;
	}

	return cpl_vector_wrap(true_size, v_data);
}

static cpl_error_code
sinfo_star_flux_list_filter_median(sinfo_star_flux_list * result, 
                                 int hsize )
{
   SINFO_ASSURE_NOT_NULL_MSG(result, "Null input flux list table frame");
   SINFO_ASSURE_NOT_ILLEGAL_MSG(result->size > 2*hsize,"size  < 2*hsize. You set a too large half window size.");

   cpl_vector* v_out  = cpl_vector_new(result->size);


   for(cpl_size i = 0; i < result->size; i++){

	   cpl_size start = i - hsize;
	   cpl_size stop = i + hsize;


	   cpl_vector * windowed = get_windowed(start, stop, result->flux, result->size);
	   if(windowed == NULL){
		   cpl_vector_set(v_out, i, NAN);
		   continue;
	   }

	   cpl_vector_set(v_out, i, cpl_vector_get_median(windowed));
	   cpl_vector_delete(windowed);
   }

   for(cpl_size i = 0 ; i < result->size; ++i){
	   result->flux[i] = cpl_vector_get(v_out, i);
   }

cleanup:

   cpl_vector_delete(v_out);
   return cpl_error_get_code();
}

static double * sinfo_star_flux_list_get_lambda( sinfo_star_flux_list * list )
{
  SINFO_ASSURE_NOT_NULL( list ) ;

 cleanup:
  return list->lambda ;
}

static double * sinfo_star_flux_list_get_flux( sinfo_star_flux_list * list )
{
  SINFO_ASSURE_NOT_NULL( list ) ;

 cleanup:
  return list->flux ;
}


static double *
sinfo_star_flux_list_duplicate_flux( sinfo_star_flux_list * list )
{
  SINFO_ASSURE_NOT_NULL( list ) ;
  double* to_ret = NULL;

  SINFO_CALLOC( to_ret, double, list->size) ;

  memcpy(to_ret, sinfo_star_flux_list_get_flux(list), sizeof(double) * list->size);

 cleanup:

 return to_ret;
}

static double *
sinfo_star_flux_list_duplicate_lambda( sinfo_star_flux_list * list )
{
  SINFO_ASSURE_NOT_NULL( list ) ;
  double* to_ret = NULL;

  SINFO_CALLOC( to_ret, double, list->size) ;

  memcpy(to_ret, sinfo_star_flux_list_get_lambda(list), sizeof(double) * list->size);

 cleanup:

 return to_ret;
}

static sinfo_star_flux_list *
sinfo_bspline_fit_interpol(sinfo_star_flux_list * ref_std_star_list,
                           cpl_table* resp_fit_points,HIGH_ABS_REGION * phigh,
                           new_sinfo_band band)
{
    sinfo_star_flux_list * res=NULL;
    cpl_table* tab=NULL;
    cpl_table* ext_data=NULL;
    cpl_table* ext_fit=NULL;
    /*
  const char* fname=NULL;
  double* lambda=NULL;
  double wstp_fit=0;
     */
    double* wave=NULL;
    double* plambda=NULL;
    double* flux=NULL;
    double* xfit=NULL;
    double* yfit=NULL;
    double wrange=0;
    double wmin_data=0;
    double wmax_data=0;
    double wmin_fit=0;
    double wmax_fit=0;


    int i=0;
    int size_data=0;
    int size_fit=0;
    double med=0;
    int imin=0;
    int imax=0;
    int nsel=0;
    double * pflux=NULL;

    double flux_save=0;

    cpl_error_ensure(ref_std_star_list != NULL, CPL_ERROR_NULL_INPUT,return res,"NULL input reference std star list");
    cpl_error_ensure(resp_fit_points != NULL, CPL_ERROR_NULL_INPUT,return res,"NULL input table with response fit points");
    //cpl_error_ensure(inst != NULL, CPL_ERROR_NULL_INPUT,return res,"NULL input instrument");

    //fname=cpl_frame_get_filename(resp_fit_points);
    //fit_points=cpl_table_load(fname,1,0);
    
    
    /* defines a band dependent size range over which to evaluate the median */
    /* TODO support other bands */
    if (band == INSTRUMENT_BAND_UVB) {
        wrange=0.4;
    } else if (band == INSTRUMENT_BAND_VIS) {
        wrange=1.0;
    } else if (band == INSTRUMENT_BAND_ALL ||
               band == INSTRUMENT_BAND_NIR ||
               band == INSTRUMENT_BAND_J ||
               band == INSTRUMENT_BAND_H ||
               band == INSTRUMENT_BAND_K ||
               band == INSTRUMENT_BAND_HK ||
               band == INSTRUMENT_BAND_L) {
        wrange=4.0;
    }
    
   
    
    size_data = ref_std_star_list->size ;
    wave = sinfo_star_flux_list_duplicate_lambda(ref_std_star_list) ;
    flux = sinfo_star_flux_list_duplicate_flux(ref_std_star_list) ;
    res=sinfo_star_flux_list_duplicate(ref_std_star_list);

    tab=cpl_table_new(size_data);
    cpl_table_wrap_double(tab,wave,"wave");
    cpl_table_wrap_double(tab,flux,"response");
    cpl_table_save(tab,NULL,NULL,"response_smoothed_modigli.fits",CPL_IO_DEFAULT);

    check_nomsg(wmin_data=cpl_table_get_column_min(tab,"wave"));
    check_nomsg(wmax_data=cpl_table_get_column_max(tab,"wave"));
    sinfo_msg("wmin_data=%g wmax_data=%g",wmin_data,wmax_data);
    size_fit=cpl_table_get_nrow(resp_fit_points);
    cpl_table_save(resp_fit_points,NULL,NULL,"resp_fit_points.fits",CPL_IO_DEFAULT);
    //cpl_table_save(resp_fit_points,NULL,NULL,"pippo.fits",CPL_IO_DEFAULT);
    cpl_table_save(tab,NULL,NULL,"pippo1.fits",CPL_IO_DEFAULT);
    pflux=cpl_table_get_data_double(tab,"response");
    /* remove points that have either value NAN or INF */
    for(i=0;i<size_data;i++) {
        if(isnan(pflux[i])) {
            cpl_table_set_invalid(tab,"response",i);
        }
        if(isinf(pflux[i])) {

            cpl_table_set_invalid(tab,"response",i);
        }
    }
   
    //cpl_table_erase_invalid(tab);
    cpl_table_save(tab,NULL,NULL,"pippo2.fits",CPL_IO_DEFAULT);
    /* check that the sampling points are not falling in any of the high abs windows */
    if (phigh != NULL) {
        sinfo_msg_warning("Flag High Absorption Regions" );
        for (; phigh->lambda_min != 0.; phigh++) {
            cpl_msg_warning("", "Flag [%g,%g]",phigh->lambda_min,phigh->lambda_max);
            for (i = 0; i < size_data; i++) {
                if ( (wave[i] >= phigh->lambda_min && wave[i] <= phigh->lambda_max) &&
                     (wave[i] >= wmin_data && wave[i] <= wmax_data) ){
                    cpl_table_set_invalid(tab,"response", i);
                }
            }
        }
       
    }
    cpl_table_erase_invalid(tab);
    cpl_table_save(tab,NULL,NULL,"pippo3.fits",CPL_IO_DEFAULT);
    
    
   
    double wmin_good=cpl_table_get_column_min(tab,"wave");
    double wmax_good=cpl_table_get_column_max(tab,"wave");
    
    /* limit the points where the raw response is sampled to the region actually
     * covered by the data
     */
    check_nomsg(wmin_fit=cpl_table_get_column_min(resp_fit_points,"LAMBDA"));
    check_nomsg(wmax_fit=cpl_table_get_column_max(resp_fit_points,"LAMBDA"));

    cpl_table_and_selected_double(resp_fit_points,"LAMBDA",CPL_NOT_LESS_THAN,wmin_data);
    cpl_table_and_selected_double(resp_fit_points,"LAMBDA",CPL_NOT_GREATER_THAN,wmax_data);
    ext_fit=cpl_table_extract_selected(resp_fit_points);
    cpl_table_save(ext_fit,NULL,NULL,"resp_fit.fits",CPL_IO_DEFAULT);
    cpl_table_save(tab,NULL,NULL,"resp_data.fits",CPL_IO_DEFAULT);
    check_nomsg(wmin_fit=cpl_table_get_column_min(ext_fit,"LAMBDA"));
    check_nomsg(wmax_fit=cpl_table_get_column_max(ext_fit,"LAMBDA"));
    cpl_msg_warning("", "AA: wmin_fit=%g wmax_fit=%g",wmin_fit,wmax_fit);
    cpl_msg_warning("", "BB: wmin_data=%g wmax_data=%g",wmin_data,wmax_data);
    cpl_msg_warning("", "CC: wmin_good=%g wmax_good=%g",wmin_good,wmax_good);
    cpl_table_save(tab,NULL,NULL,"pippo4.fits",CPL_IO_DEFAULT);
    
    size_fit=cpl_table_get_nrow(ext_fit);
    cpl_size size_fit_ori=cpl_table_get_nrow(ext_fit);

    //sinfo_msg_warning("size_fit=%d wstp_fit=%g",size_fit,wstp_fit);
    imin=0;
    if( (wmin_fit>wmin_data)  && (wmin_fit > wmin_good) ) {
        //sinfo_msg("wmin_fit>wmin_data");
        size_fit+=1;
        imin=1;
    }
    imax=size_fit-1;
    if( (wmax_fit<wmax_data) && (wmax_fit<wmax_good) ) {
        //sinfo_msg("wmax_fit<wmax_data");
        size_fit+=1;
    }
    sinfo_msg("imin=%d imax=%d",imin,imax);
    cpl_table_save(tab,NULL,NULL,"pippo5.fits",CPL_IO_DEFAULT);
    sinfo_msg_warning("fk5");
    sinfo_print_rec_status(5);
    cpl_msg_warning(cpl_func, "size_fit=%d, size_fit_ori = %lld",size_fit, size_fit_ori);

    sinfo_msg("imin=%d imax=%d",imin,imax);
    SINFO_CALLOC( xfit, double, size_fit) ;
    SINFO_CALLOC( yfit, double, size_fit) ;
    sinfo_msg("sk8 wmin_data=%g wmin_fit=%g, wrange=%g",wmin_data,wmin_fit,wrange);
    /* now computes in a robust way (median) the response fit values */
    cpl_table_save(tab,NULL,NULL,"pippo6.fits",CPL_IO_DEFAULT);
    /* determine in robust way first fit point value. This may be not a point
     * of list of fit points */
    cpl_table_select_all(tab);

    if( wmin_fit > wmin_good ) {
        cpl_msg_warning("", "wmin_fit>wmin_good");

        sinfo_msg("selecting range [%g,%g]",wmin_good,wmin_good+wrange);
        nsel=cpl_table_and_selected_double(tab,"wave",CPL_NOT_GREATER_THAN,wmin_good+wrange);
        if(nsel==0) {
            sinfo_msg_warning("Problem to get spline fit sampling points");
            sinfo_msg_warning("Possibly XSH_HIGH_ABS_WIN table wave ranges incompatible with RESP_FIT_POINTS_CAT table");
        }
        ext_data=cpl_table_extract_selected(tab);
        cpl_table_select_all(tab);
        cpl_table_save(ext_data, NULL, NULL, "ext_data_1.fits", CPL_IO_CREATE);
        if(nsel > 0 ) {
        check_nomsg(med=cpl_table_get_column_median(ext_data,"response"));
        yfit[0]=med;
        xfit[0]=wmin_data;
        sinfo_msg("xfit[%d]=%g yfit=%g",0,xfit[0],yfit[0]);
        }
        

        sinfo_msg("xfit=%g yfit=%g",wmin_good,med);
        sinfo_free_table(&ext_data);

    }


    /*
  sinfo_msg("wmin_data=%g",wmin_data);
  sinfo_msg("wmin_fit=%g",wmin_fit);
  sinfo_msg("wmax_data=%g",wmax_data);
  sinfo_msg("wmax_fit=%g",wmax_fit);
  sinfo_msg("imin=%d imax=%d",imin,imax);
     */
       /* determine the other fit points in a robust way */
     plambda=cpl_table_get_data_double(ext_fit,"LAMBDA");
    for(i=0;i< size_fit_ori;i++) {
        cpl_msg_warning("", ":::<><>plambda [%g]",plambda[i]);
        cpl_table_and_selected_double(tab,"wave",CPL_NOT_LESS_THAN,plambda[i]-wrange);
        nsel=cpl_table_and_selected_double(tab,"wave",CPL_NOT_GREATER_THAN,plambda[i]+wrange);


        sinfo_msg_warning("selecting range [%g,%g] with %i",plambda[i]-wrange,plambda[i]+wrange, nsel);

        if(nsel>0) {
            check_nomsg(ext_data=cpl_table_extract_selected(tab));
            //sinfo_msg("wmin=%g wmax=%g",plambda[i]-wrange,plambda[i]+wrange);
            check_nomsg(med=cpl_table_get_column_median(ext_data,"response"));
            xfit[i + imin]=plambda[i];
            yfit[i + imin]=med;
            flux_save=med;
            sinfo_msg("xfit[%d]=%g yfit=%g",i,xfit[i],yfit[i]);
            sinfo_free_table(&ext_data);
        } else {
            yfit[i + imin]=flux_save;
        }
        cpl_table_select_all(tab);
    }
    cpl_table_select_all(tab);
    sinfo_msg_warning("fk7");
       sinfo_print_rec_status(7);
       /* determine the last fit points in a robust way */

    if( wmax_fit<wmax_good ) {

    	cpl_msg_warning("", "wmax_fit<wmax_data");
        nsel=cpl_table_and_selected_double(tab,"wave",CPL_NOT_LESS_THAN,wmax_good-wrange);
        ext_data=cpl_table_extract_selected(tab);
        cpl_table_select_all(tab);
        sinfo_msg_warning("nsel=%d",nsel);
        if(nsel>0) {
        check_nomsg(med=cpl_table_get_column_median(ext_data,"response"));
        yfit[size_fit-1]=med;
        xfit[size_fit-1]=wmax_data;
        }
        
        sinfo_msg("xfit[%d]=%g yfit=%g",size_fit-1,wmax_good,med);
        sinfo_free_table(&ext_data);
    }

    sinfo_msg_warning("fk8");
       sinfo_print_rec_status(8);

  cpl_table_save(tab,NULL,NULL,"resp_data_selected.fits",CPL_IO_DEFAULT);

  sinfo_free_table(&tab);
  tab=cpl_table_new(size_fit);
  cpl_table_wrap_double(tab,xfit,"xfit");
  check_nomsg(cpl_table_wrap_double(tab,yfit,"yfit"));
  cpl_table_save(tab,NULL,NULL,"resp_data_fit.fits",CPL_IO_DEFAULT);

    //sinfo_msg("size_fit=%d size_data=%d",size_fit,size_data);
    /* as xsh_bspline_interpolate_data_at_pos allocates memory for flux vector
     * and we store it in an already allocated structure we need to deallocate
     * the corresponding memory first: better would be to replace the res->flux
     * values by the newly computed ones
     */
    cpl_free(res->flux);
    sinfo_msg_warning("xfit[0]=%g xfit[size_fit-1]=%g",xfit[0],xfit[size_fit-1]);
    sinfo_msg_warning("xfit[size_fit-3]=%g xfit[size_fit-2]=%g",xfit[size_fit-3],xfit[size_fit-2]);
    check_nomsg(res->flux=sinfo_bspline_interpolate_data_at_pos(xfit,yfit,size_fit,res->lambda,size_data));
    //sinfo_star_flux_list_save(res,"fit_response.fits", "TEST" ) ;
    sinfo_msg_warning("fk9");
       sinfo_print_rec_status(9);
       //exit(0);
    cleanup:

    sinfo_msg_warning("fk10");
       sinfo_print_rec_status(10);
    cpl_free(xfit);
    cpl_free(yfit);
    sinfo_free_table(&ext_fit);
    cpl_table_unwrap(tab,"xfit");
    cpl_table_unwrap(tab,"yfit");
    cpl_free(tab);
    sinfo_msg_warning("fk11");
           sinfo_print_rec_status(11);
    return res;
}



static sinfo_star_flux_list *
sinfo_bspline_fit_smooth(sinfo_star_flux_list * ref_std_star_list,HIGH_ABS_REGION * phigh,new_sinfo_band band)
{
    sinfo_star_flux_list * result=NULL;
    size_t n = 0;
    size_t order = 4;
    size_t ncoeffs = 0;
    size_t nbreak = 0;

    size_t i, j;
    gsl_bspline_workspace *bw;
    gsl_vector *B;

    gsl_rng *r;
    gsl_vector *c, *w;
    gsl_vector * x, *y;
    gsl_matrix *X, *cov;
    gsl_multifit_linear_workspace *mw;
    double chisq=0, Rsq=0, dof=0;
    //double tss;
    //double wmin=0;
    //double wmax=0;
    //double wstep=0;
    double* wave = NULL;
    double* response = NULL;
    double* pwav = NULL;
    double* pfit = NULL;
    //int kh = 0;
    double dump_factor = 1.e10;
    //cpl_table* tab_response = NULL;
    cpl_table* tab_resp_fit = NULL;
    //const char* fname = NULL;

    /* set optimal number of coeffs for each arm */
    if (band == INSTRUMENT_BAND_UVB) {
        ncoeffs = 21;
    } else if (band == INSTRUMENT_BAND_VIS) {
        ncoeffs = 16;
    } else if ( (band == INSTRUMENT_BAND_ALL ) ||
                (band == INSTRUMENT_BAND_NIR ) ||
                (band == INSTRUMENT_BAND_J ) ||
                (band == INSTRUMENT_BAND_H ) ||
                (band == INSTRUMENT_BAND_K ) ||
                (band == INSTRUMENT_BAND_HK ) ||
                (band == INSTRUMENT_BAND_L ) ) {
        ncoeffs = 6;
    }
    
    nbreak = ncoeffs + 2 - order;

    n = ref_std_star_list->size ;
    wave = ref_std_star_list->lambda ;
    response = ref_std_star_list->flux ;
    //wmin = *wave ;
    //wmax = *(wave+n-1) ;
    //wstep = (wmax-wmin)/(n-1) ;

    gsl_rng_env_setup();
    r = gsl_rng_alloc(gsl_rng_default);

    /* allocate a cubic bspline workspace (ORDER = order) */
    bw = gsl_bspline_alloc(order, nbreak);
    B = gsl_vector_alloc(ncoeffs);

    x = gsl_vector_alloc(n);
    y = gsl_vector_alloc(n);
    X = gsl_matrix_alloc(n, ncoeffs);
    c = gsl_vector_alloc(ncoeffs);
    w = gsl_vector_alloc(n);
    cov = gsl_matrix_alloc(ncoeffs, ncoeffs);
    mw = gsl_multifit_linear_alloc(n, ncoeffs);

    //printf("#m=0,S=0\n");
    /* this is the data to be fitted */
    for (i = 0; i < n; ++i) {
        double sigma;
        double xi = (double) wave[i];
        double yi = (double) response[i];

        sigma = 0.001 * yi;
        double dy = gsl_ran_gaussian(r, sigma);
        yi += dy;

        gsl_vector_set(x, i, xi);

        if(isnan(yi) || isinf(yi)) {
            gsl_vector_set(y, i, 0);
            gsl_vector_set(w, i, 1.0 / dump_factor);
            //sinfo_msg("wave=%g y=%g w=%g",xi,yi,gsl_vector_get(w, i));
        } else {
            gsl_vector_set(y, i, yi);
            gsl_vector_set(w, i, 1.0 / (sigma * sigma));
            //sinfo_msg("wave=%g y=%g w=%g",xi,yi,gsl_vector_get(w, i));
        }
        //printf("%f %g\n", xi, yi);
    }

    if (phigh != NULL) {

        sinfo_msg("Flag High Absorption Regions" );
        for (; phigh->lambda_min != 0.; phigh++) {
            sinfo_msg("Flag [%g,%g]",phigh->lambda_min,phigh->lambda_max);
            for (i = 0; i < n; i++) {
                if (wave[i] >= phigh->lambda_min && wave[i] <= phigh->lambda_max) {
                    //gsl_vector_set(w, i, gsl_vector_get(w, i) / dump_factor);
                    gsl_vector_set(w, i, 1.0 / dump_factor);
                    //sinfo_msg("w=%g",gsl_vector_get(w, i));
                }
            }
        }
    }

    /* use uniform breakpoints on [0, 15] */
    gsl_bspline_knots_uniform(wave[0], wave[n - 1], bw);

    /* construct the fit matrix X */
    for (i = 0; i < n; ++i) {
        double xi = gsl_vector_get(x, i);

        /* compute B_j(xi) for all j */
        gsl_bspline_eval(xi, B, bw);

        /* fill in row i of X */
        for (j = 0; j < ncoeffs; ++j) {
            double Bj = gsl_vector_get(B, j);
            gsl_matrix_set(X, i, j, Bj);
        }
    }

    /* do the fit */
    gsl_multifit_wlinear(X, w, y, c, cov, &chisq, mw);

    dof = n - ncoeffs;
    /*
  tss = gsl_stats_wtss(w->data, 1, y->data, 1, y->size);
  Rsq = 1.0 - chisq / tss;
     */
    printf("chisq/dof = %e, Rsq = %f\n", chisq / dof, Rsq);

    tab_resp_fit = cpl_table_new(n);
    cpl_table_new_column(tab_resp_fit, "wave", CPL_TYPE_DOUBLE);
    cpl_table_new_column(tab_resp_fit, "fit", CPL_TYPE_DOUBLE);
    cpl_table_fill_column_window_double(tab_resp_fit, "wave", 0, n, 0);
    cpl_table_fill_column_window_double(tab_resp_fit, "fit", 0, n, 0);
    pwav = cpl_table_get_data_double(tab_resp_fit, "wave");
    pfit = cpl_table_get_data_double(tab_resp_fit, "fit");
    result=sinfo_star_flux_list_create(n);
    /* output the smoothed curve */
    {
        double yi, yerr;
        //printf("#m=1,S=0\n");
        for (i = 0; i < n; i++) {
            double xi = (double) wave[i];
            gsl_bspline_eval(xi, B, bw);
            gsl_multifit_linear_est(B, c, cov, &yi, &yerr);
            //printf("%f %g\n", xi, yi);
            pwav[i] = xi;
            pfit[i] = yi;
            result->lambda[i]=xi;
            result->flux[i]=yi;
        }
    }
    //cpl_table_save(tab_resp_fit, NULL, NULL, "resp_fit.fits", CPL_IO_DEFAULT);


    //cleanup:
    gsl_rng_free(r);
    gsl_bspline_free(bw);
    gsl_vector_free(B);
    gsl_vector_free(x);
    gsl_vector_free(y);
    gsl_matrix_free(X);
    gsl_vector_free(c);
    gsl_vector_free(w);
    gsl_matrix_free(cov);
    gsl_multifit_linear_free(mw);
    sinfo_free_table(&tab_resp_fit);

    return result;
}

cpl_error_code
sinfo_response_merge_obj_std_info(cpl_frame* result,
                                sinfo_star_flux_list * star_list,
                                sinfo_star_flux_list * obj_list)
{

    cpl_table* table=NULL;
    const char* name=NULL;
    cpl_propertylist* plist=NULL;
    int nrow=0;
    int i=0;

    double* pobj=NULL;
    double* pref=NULL;
    double* pdiv=NULL;

    name=cpl_frame_get_filename(result);
    plist=cpl_propertylist_load(name,0);
    table=cpl_table_load(name,1,0);
    nrow=cpl_table_get_nrow(table);

    check_nomsg(cpl_table_name_column(table,"FLUX", "RESPONSE"));
    cpl_table_new_column(table,"OBS",CPL_TYPE_DOUBLE);
    cpl_table_new_column(table,"REF",CPL_TYPE_DOUBLE);
    cpl_table_new_column(table,"REF_DIV_OBS",CPL_TYPE_DOUBLE);
    cpl_table_fill_column_window_double(table,"OBS",0,nrow,0);
    cpl_table_fill_column_window_double(table,"REF",0,nrow,0);
    cpl_table_fill_column_window_double(table,"REF_DIV_OBS",0,nrow,0);

    pobj=cpl_table_get_data_double(table,"OBS");
    pref=cpl_table_get_data_double(table,"REF");
    pdiv=cpl_table_get_data_double(table,"REF_DIV_OBS");

    for(i=0;i<nrow;i++) {
        pobj[i]=obj_list->flux[i];
        pref[i]=star_list->flux[i];
        pdiv[i]=pref[i]/pobj[i];
    }

    cpl_table_save(table,plist,NULL,name,CPL_IO_DEFAULT);

    cleanup:
    sinfo_free_table(&table);
    sinfo_free_propertylist(&plist);

    return cpl_error_get_code();
}
static void
sinfo_star_flux_list_extrapolate_wave_end( sinfo_star_flux_list * list ,
		const double wmax)
{
  SINFO_ASSURE_NOT_NULL( list ) ;
  int size=list->size;
  int i=0;
  int k=0;
  int off=10;
  double f1=0;
  double f2=0;
  double w1=0;
  double w2=0;
  double x1=0;
  double x2=0;
  double m=0;
  double x=0;
  double w=0;
  int computed=0;


  /* Rayleigh-Jeans extrapolation of null flux values:
   * we can linearize the formula to have a quick fit
   * flux=A/lambda^4+B. X=1/lambda^4, Y=f==>
   * Y=A*X+B==> we can linearize */
  for (i = 0; i < size; i++) {
    if (list->lambda[i]<wmax) {
      k++;
    } else {

      if (computed == 0) {
        f2 = list->flux[k];
        f1 = list->flux[k - off];
        w2 = 1./list->lambda[k];
        x2 = w2*w2*w2*w2;
        w1 = 1./list->lambda[k - off];
        x1 = w1*w1*w1*w1;
        m = (f2 - f1) / (x2 - x1);
        computed=1;

      } else {
         w=1./list->lambda[i];
         x=w*w*w*w;

         list->flux[i]=f1+m*(x-x1);

      }
    }
  }

 cleanup:
  return;
}
static void
sinfo_frame_spectrum_save(cpl_frame* frm,const char* name_o)
{
   sinfo_spectrum* s=NULL;
   //const char* name=NULL;
   const char* tag=NULL;
   cpl_frame* loc_frm=NULL;
   //name=cpl_frame_get_filename(frm);
   sinfo_msg_warning("frm=%p",frm);
   sinfo_print_rec_status(20);
   //cpl_ensure(frm != NULL,   CPL_ERROR_NULL_INPUT, NULL);
   sinfo_print_rec_status(21);
   s=sinfo_spectrum_load(frm);
   sinfo_print_rec_status(22);
   loc_frm=sinfo_spectrum_save(s,name_o,tag);
   sinfo_print_rec_status(23);
   cpl_frame_set_filename(frm,name_o);
   sinfo_print_rec_status(24);
   sinfo_spectrum_free(&s);
   sinfo_free_frame(&loc_frm);
   return;
}
static char * sinfo_get_basename(const char *filename)
{
  char *p ;
  p = strrchr (filename, '/');
  return p ? p + 1 : (char *) filename;
}


static cpl_frame *
sinfo_compute_response(cpl_frame * obs_std_star,
                                   cpl_frame * flux_std_star_cat,
                                   cpl_frame * atmos_ext,
                                   cpl_frame* high_abs,
                                   cpl_frame* resp_fit_points,
                                   cpl_frame* tell_mod_cat,
                                   new_sinfo_band band,
                                   double exptime,
                                   const int tell_corr )
{

    /* TODO: exptime should not appear in this function interface as it can be
     * retrieved from input frame */
    cpl_frame* result = NULL;
    cpl_ensure(obs_std_star != NULL,   CPL_ERROR_NULL_INPUT, NULL);
    cpl_ensure(flux_std_star_cat != NULL,   CPL_ERROR_NULL_INPUT, NULL);
    cpl_ensure(atmos_ext != NULL,   CPL_ERROR_NULL_INPUT, NULL);
    //cpl_ensure(high_abs != NULL,   CPL_ERROR_NULL_INPUT, NULL);
    cpl_ensure(resp_fit_points != NULL,   CPL_ERROR_NULL_INPUT, NULL);
    cpl_ensure(tell_mod_cat != NULL,   CPL_ERROR_NULL_INPUT, NULL);

    double dRA = 0;
    double dDEC = 0;
    double airmass = 0;

    double wmin = 0;
    double wmax = 0;
    double wstep = 0;
    int naxis1 = 0;
    double cdelt1 = 0;
    const char* filename = NULL;
    cpl_propertylist* plist = NULL;
    int is_complete=0;
    cpl_frame* ipol_ref_std_star_frame=NULL;
    cpl_frame* frm_tmp=NULL;

    cpl_table* tbl_shift_std_spectrum=NULL;
    cpl_frame* shift_std_star_frame=NULL;
    const char* tbl_shift_std_fname="shift_flux_std_star.fits";
    char resp_obs_std_star_fname[MAX_NAME_SIZE];
    cpl_frame* obs_std_star_resp=NULL;
    cpl_table* resp_fit_points_tbl=NULL;
    cpl_table* tbl_ref_std_spectrum = NULL;
    const double um2nm=1000;
    cpl_frame* ipol_shift_std_star_frame=NULL;

   
    sinfo_star_flux_list * shift_std_star_list = NULL;
    sinfo_star_flux_list * corr_obs_std_star_list = NULL;
    sinfo_star_flux_list * response =NULL;
    sinfo_star_flux_list * response_fit =NULL;


    sinfo_star_flux_list * obs_std_star_list = NULL;
    /* X-shooter specific */
    sinfo_rv_ref_wave_param* rv_ref_wave=NULL;
    
    sinfo_spectrum* obs_spectrum=NULL;
    
    HIGH_ABS_REGION * phigh = NULL;
     
    sinfo_star_flux_list * ref_std_star_list = NULL;



    filename = cpl_frame_get_filename(obs_std_star);
    const char* obs_name = filename + strlen(filename);
    while(*obs_name != '/') obs_name--;
    obs_name++;

    char fname_obs[MAX_NAME_SIZE];
    snprintf(fname_obs, MAX_NAME_SIZE, "%s", obs_name);
    sinfo_msg_warning("filename=%s",filename);
    cpl_table* tab_obs_std_star=cpl_table_load(filename,1,0);
    plist = cpl_propertylist_load(filename, 0);

    //naxis1 = sinfo_pfits_get_naxis1(plist);
    int status;
    naxis1 = cpl_table_get_nrow(tab_obs_std_star);
    /* TODO: adjust SINFONI scale from um to nm */
    if( band == INSTRUMENT_BAND_J ||
        band == INSTRUMENT_BAND_H ||
        band == INSTRUMENT_BAND_K ||
        band == INSTRUMENT_BAND_HK ||
        band == INSTRUMENT_BAND_L ) {
        sinfo_msg_warning("correct wave scale from um to nm");
        cpl_table_multiply_scalar(tab_obs_std_star,"wavelength",um2nm);
    }
    cpl_table_save(tab_obs_std_star, plist, NULL, "tmp_obs_std_star_ORI.fits", CPL_IO_CREATE);
    cpl_table_save(tab_obs_std_star, plist, NULL, "tmp_obs_std_star.fits", CPL_IO_CREATE);
    cpl_frame_set_filename(obs_std_star,"tmp_obs_std_star.fits");
    wmin=cpl_table_get_column_min(tab_obs_std_star,"wavelength");
    wmax=cpl_table_get_column_max(tab_obs_std_star,"wavelength");
    int nrow=0;
    nrow=cpl_table_get_nrow(tab_obs_std_star);
    wstep=(wmax-wmin)/(nrow-1);
    sinfo_msg_warning("rk1 wmin=%g wmax=%g wstep=%g cdelt1=%g naxis1=%d",
                          wmin,wmax,wstep,cdelt1,naxis1);
    /* inaccurate
    if(cpl_table_get_column_type(tab_obs_std_star, "wavelength") == CPL_TYPE_DOUBLE) {
        wstep=cpl_table_get_double(tab_obs_std_star,"wavelength",1,&status)-wmin;
    } else {
        wstep=cpl_table_get_float(tab_obs_std_star,"wavelength",1,&status)-wmin;
    }
    */


    
     /*
      * cdelt1 = sinfo_pfits_get_cdelt1(plist);
       wstep=cdelt1;
       wmin = sinfo_pfits_get_crval1(plist);
     */

    //wmax = wmin + cdelt1 * naxis1;
    obs_std_star_resp=cpl_frame_duplicate(obs_std_star);
    
    obs_std_star_list = sinfo_star_flux_list_load_spectrum(obs_std_star_resp) ;
    cpl_frame* frm=sinfo_star_flux_list_save( obs_std_star_list,"obs_std_star_list.fits", "TEST" ) ;
    sinfo_star_flux_list_free(&obs_std_star_list);
    sinfo_free_frame(&frm);
    //exit(0);
    
    sinfo_frame_sci_get_ra_dec_airmass(obs_std_star_resp,&dRA,&dDEC,&airmass);
    sinfo_msg_warning("rk1 wmin=%g wmax=%g wstep=%g cdelt1=%g naxis1=%d",
                      wmin,wmax,wstep,cdelt1,naxis1);
    sinfo_print_rec_status(1);
    sinfo_std_star_id std_star_id=0;
    sinfo_parse_catalog_std_stars(flux_std_star_cat, dRA, dDEC,
                                STAR_MATCH_DEPSILON, &tbl_ref_std_spectrum,&std_star_id);


    sinfo_msg_warning("dRA=%g dDEC=%g airmass=%g",dRA,dDEC,airmass);
    
    /* for QC we save the ref STD spectrum in a FITS file to be used for comparison */
    cpl_frame* ref_std_star_frame=NULL;
    const char* tbl_ref_std_fname="ref_flux_std_star.fits";
    /* TODO: the following table in a NULL pointer" */
    sinfo_msg_warning("tbl_ref_std_spectrum1=%p",tbl_ref_std_spectrum);
    cpl_table_save(tbl_ref_std_spectrum,NULL,NULL,tbl_ref_std_fname,
                   CPL_IO_DEFAULT);

   
    ref_std_star_frame=sinfo_frame_product(tbl_ref_std_fname,"FLUX_STD_STAR",
                    CPL_FRAME_TYPE_TABLE,
                    CPL_FRAME_GROUP_PRODUCT,
                    CPL_FRAME_LEVEL_INTERMEDIATE);

    
    /* re-sample reference STD spectrum on the same sampling pixel base of the observed STD spectrum.
     * over the allowed wave ranges. This operation need to conserve flux.QC: verify that after
     * re-sampling the re-sampled spectrum overlaps the original one. The re-sampling should be given by a
     * (Hermite) spline fit of large enough degree
     *  */

    sinfo_msg_warning("rk2 wstep=%g wmin=%g wmax=%g",wstep,wmin,wmax);
    sinfo_print_rec_status(2);
    check_nomsg(ipol_ref_std_star_frame=sinfo_spectrum_interpolate_linear(ref_std_star_frame,wstep,wmin,wmax));
    
    sinfo_msg_warning("rk3");
    sinfo_print_rec_status(3);
    check_nomsg(ref_std_star_list = sinfo_star_flux_list_load( ipol_ref_std_star_frame )) ;
    sinfo_free_frame(&ipol_ref_std_star_frame);
    sinfo_free_frame(&ref_std_star_frame);

    //TODO: next line can be commented out
    //sinfo_star_flux_list_save( ref_std_star_list,"ref_std_star_list.fits", "TEST" ) ;
    /* Does not apply for SINFONI */
    if( band == INSTRUMENT_BAND_UVB && is_complete !=0){
        sinfo_star_flux_list_extrapolate_wave_end(ref_std_star_list,545.);
        frm_tmp=sinfo_star_flux_list_save( ref_std_star_list,"extrapol_ref_std_star_list.fits", "TEST" ) ;
        sinfo_free_frame(&frm_tmp);
    }
    
    
   
    //PIPPO: we moved here telluric correction.
     sinfo_msg_warning("tell_corr=%d tell_mod_cal=%p",tell_corr,tell_mod_cat);
     sinfo_msg_warning("rk4");
     sinfo_print_rec_status(4);
 
    
     if(band != INSTRUMENT_BAND_UVB && tell_corr == 1 && tell_mod_cat!= NULL) {
         sinfo_msg_warning("Telluric correction");
         cpl_table* tab_res=NULL;
         //int next=0;
         
         cpl_size  model_idx=0;
         obs_std_star_list = sinfo_star_flux_list_load_spectrum(obs_std_star_resp) ;
         
         cpl_frame* frm=sinfo_star_flux_list_save( obs_std_star_list,"obs_std_star_list.fits", "TEST" ) ;
         sinfo_free_frame(&frm);
         //exit(0);
         obs_spectrum = sinfo_spectrum_load_from_table( obs_std_star_resp,
                         "wavelength","counts_bkg");
         sinfo_print_rec_status(3);
         sinfo_msg_warning("ok3");
         
         check_nomsg(tab_res=sinfo_telluric_model_eval(tell_mod_cat,obs_spectrum,band,
                         &model_idx));

         cpl_table_save(tab_res, NULL, NULL, "corrected_obs_modigli.fits", CPL_IO_CREATE);


         //exit(0);
         sinfo_print_rec_status(4);
                 sinfo_msg_warning("ok4");
        
         sinfo_spectrum_free(&obs_spectrum);
         
         /*
         sinfo_msg("response size %d table size %d",
         response->size,cpl_table_get_nrow(tab_obs_tell_cor));
         sinfo_msg("wstep=%g",wstep);
         tab_res=sinfo_table_resample_uniform(tab_obs_tell_cor,"wave","ratio",wstep);
         sinfo_msg("response size %d table size %d",
         response->size,cpl_table_get_nrow(tab_res));
          */
         double* pwav=NULL;
         double* pflx=NULL;
         int i=0;
         //cpl_table_dump(tab_res,1,2,stdout);
         pwav = cpl_table_get_data_double(tab_res, "wavelength");
       
         pflx = cpl_table_get_data_double(tab_res, "ratio");
        
         sinfo_msg_warning("ok1 size_obs=%d size_mod_corr=%d",
         obs_std_star_list->size,(int)cpl_table_get_nrow(tab_res));
         sinfo_print_rec_status(5);
                          sinfo_msg_warning("ok5");
         for(i=0;i<obs_std_star_list->size;i++) {
           
             obs_std_star_list->lambda[i] = pwav[i];
             
             obs_std_star_list->flux[i] = pflx[i];
            
         }
         sinfo_print_rec_status(6);
                          sinfo_msg_warning("ok6");
         sinfo_free_table(&tab_res);
         frm_tmp=sinfo_star_flux_list_save( obs_std_star_list,"obs_std_star_list_tell.fits", "TEST" ) ;
         sinfo_print_rec_status(1);
       
         sinfo_free_frame(&frm_tmp);
         sinfo_star_flux_list_to_frame(obs_std_star_list,obs_std_star_resp);
         sinfo_print_rec_status(7);
                                   sinfo_msg_warning("ok7");
     } else {
         check_nomsg(obs_std_star_list = sinfo_star_flux_list_load_spectrum(obs_std_star_resp)) ;
         //frm_tmp=sinfo_star_flux_list_save( obs_std_star_list,"obs_std_star_list.fits", "TEST" ) ;
         //sinfo_free_frame(&frm_tmp);

     }
     sinfo_print_rec_status(8);
                      sinfo_msg_warning("ok8");
    
     
     //CONTINUE
     /* TODO: here we need to have the image converted in table
     check_nomsg(ipol_obs_std_star_frame=xsh_spectrum_interpolate_linear(obs_std_star_resp,wstep/10,wmin,wmax));
      */


     /* TODO: shift the ref STD star spectrum by a value corresponding to the its radial velocity */
     rv_ref_wave=sinfo_rv_ref_wave_param_create();

     /* get reference lambda from given ref std star */
     sinfo_rv_ref_wave_init(std_star_id ,band,rv_ref_wave);
     //sinfo_msg("guess=%12.8g",rv_ref_wave->wguess);

    
     /* get corresponding lambda from observed std star */
     /* this method is not very robust to get the shift between spectra as:
      * 1) the observed spectrum is noisy
      * 2) the lines are in absorbtion and do not have a Gaussian shape
      * 3) some line have an inversion (a small max in the minimum area of the line)
      * Thus it is better to make a cross correlation between lines that allows to solve all problems
      */
     /*
     double wave_obs=0;
     wave_obs=xsh_get_obs_std_star_wave_ref(obs_std_star_resp,wave_guess,wmin,wstep);
      double wave_shift=0;
     double wave_ref=0;
     wave_ref=xsh_get_ref_std_star_wave_ref(tbl_ref_std_spectrum,wave_guess,wstep);
     sinfo_msg("wave: guess %g ref %g shift %g",wave_guess,wave_ref,wave_guess-wave_ref);
      */
     //sinfo_star_flux_list_save( obs_std_star_list,"obs_std_star_list.fits", "TEST" ) ;
     frm_tmp=sinfo_star_flux_list_save( ref_std_star_list,"ref_flux_std_star.fits", "TEST" ) ;
     sinfo_free_frame(&frm_tmp);
     double wave_shift=0;
     sinfo_print_rec_status(9);
                         sinfo_msg_warning("ok9");
                         if(band == INSTRUMENT_BAND_ALL ||
                            band == INSTRUMENT_BAND_J ||
                            band == INSTRUMENT_BAND_NIR ||
                            band == INSTRUMENT_BAND_VIS ||
							band == INSTRUMENT_BAND_UVB) {
                        	 cpl_msg_warning("", "COOOOOOOOORRRRELATE!!!!!!!!");
     wave_shift=sinfo_std_star_spectra_correlate(obs_std_star_list,ref_std_star_list,rv_ref_wave);
                         } 
                         else{
                        	 cpl_msg_warning("", "||!!poejiwfibweifiwNOOOOOCOOOOOOOOORRRRELATE!!!!!!!!");
                         }
     //double offset=(wave_obs-wave_guess)/wave_guess;
     double offset=(wave_shift)/rv_ref_wave->wguess;
     cpl_msg_warning("","wave: guess[nm] %g RV shift[nm] %g offset[unitless]=%g",rv_ref_wave->wguess,wave_shift,offset);
     cpl_msg_warning("", "offset::::: %e", offset);
     tbl_shift_std_spectrum=sinfo_table_shift_rv(tbl_ref_std_spectrum,"LAMBDA",offset);

     sinfo_free_table(&tbl_ref_std_spectrum);

     check_nomsg(cpl_table_save(tbl_shift_std_spectrum,NULL,NULL,tbl_shift_std_fname,
                     CPL_IO_DEFAULT));
     sinfo_print_rec_status(10);
                         sinfo_msg_warning("ok10");

      cpl_table_save(tbl_shift_std_spectrum, NULL, NULL, "tbl_shift_std_spectrum.fits", CPL_IO_CREATE);

     //TODO: This tmp product has been left for SABINE to check the RV shift
     sinfo_free_table(& tbl_shift_std_spectrum);
     check_nomsg(shift_std_star_frame=sinfo_frame_product(tbl_shift_std_fname,"FLUX_STD_STAR",
                     CPL_FRAME_TYPE_TABLE,
                     CPL_FRAME_GROUP_PRODUCT,
                     CPL_FRAME_LEVEL_INTERMEDIATE));


     /* Define (possibly STD star dependent) arm dependent windows where the (either reference and observed)
      * STD star spectrum should not be sampled (these are usually narrow regions around line cores)
      * */

     sinfo_print_rec_status(11);
                         sinfo_msg_warning("ok11");

     check_nomsg(phigh=sinfo_fill_high_abs_regions(band,high_abs));

     check_nomsg(ipol_shift_std_star_frame=sinfo_spectrum_interpolate_linear(shift_std_star_frame,wstep,wmin,wmax));
    
     check_nomsg( shift_std_star_list = sinfo_star_flux_list_load( ipol_shift_std_star_frame ) ) ;
     
     sinfo_free_frame(&ipol_shift_std_star_frame);
     sinfo_free_frame(&shift_std_star_frame);
    
     sinfo_print_rec_status(12);
                         sinfo_msg_warning("ok12");
     frm_tmp=sinfo_star_flux_list_save( shift_std_star_list,"shift_std_star_list.fits", "TEST" ) ;
     sinfo_free_frame(&frm_tmp);
     sinfo_msg_warning("ok13");
     //exit(0);
     /* median-average over few pix (half-box of 2 pix radii, ie the number of pixels 'around'
      * the central pixel which define the neighbourhood of that pixel=> filter size is 2*radii+1)
      * the observed STD spectrum to remove residual CRH/BP.
      * DONE WITHIN xsh_obs_std_correct
      */

     /*
      * FOR VIS,NIR that are affected by telluric lines:
      * Then filter-max over half-box of 20 pix (to remove isolated telluric lines)
      * Then filter smooth over 50 pix, for smoothing. */
     /* TODO: for the moment not implemented also to be able to check how much bad pixels affects result */
     //sinfo_star_flux_list * obs_std_star_list = NULL;

     /* correct observed spectrum for atmospheric absorbtion and airmass dimming effects, wavelength direction
      * bin size, gain, exptime */
     sinfo_print_rec_status(13);
     check_nomsg( corr_obs_std_star_list = sinfo_obs_std_correct(obs_std_star_resp, shift_std_star_list, atmos_ext,band ) ) ;
     sinfo_print_rec_status(14);
     sinfo_free_frame(&obs_std_star_resp);
     sinfo_msg_warning("ok14");
     //check_nomsg(sinfo_star_flux_list_save(corr_obs_std_star_list,"corr_obs_std_star_list.fits", "TEST" )) ;

     /* divide observed (corrected) spectrum by reference one at the allowed sampling pixels
      * QC: verify that the resulting spectrum is smooth */

     sinfo_print_rec_status(15);
                         sinfo_msg_warning("ok15");
     check_nomsg(response=sinfo_star_flux_list_duplicate(shift_std_star_list));
     //check_nomsg(sinfo_star_flux_list_fill_null_end(response));
     //check_nomsg(sinfo_star_flux_list_save(response,"dup_obs_std_star_list.fits", "TEST" )) ;

     check_nomsg(frm_tmp=sinfo_star_flux_list_save( response,"response_before_division.fits", "TEST" )) ;
     sinfo_free_frame(&frm_tmp);
     check_nomsg(frm_tmp=sinfo_star_flux_list_save( corr_obs_std_star_list,"corr_obs_std_star_list_before_division.fits", "TEST" )) ;
     sinfo_free_frame(&frm_tmp);

     //sinfo_star_flux_list_filter_median(response,2);
     check_nomsg(sinfo_star_flux_list_divide(response,corr_obs_std_star_list));
     //check_nomsg(sinfo_star_flux_list_save(response,"std_div_obs_star.fits", "TEST" )) ;

     /* smooth over a pixel radii window equivalent to 5nm the resulting ration to get the response
      * QC: verify the resulting spectrum is smooth
      */

     check_nomsg(frm_tmp=sinfo_star_flux_list_save( response,"response_before_smooth.fits", "TEST" )) ;
     sinfo_free_frame(&frm_tmp);



     check_nomsg(sinfo_star_flux_list_filter_median(response, 11 ));
     //check_nomsg(sinfo_star_flux_list_save(response,"med_response.fits", "TEST" )) ;
     //int hsize=(int)(0.25/wstep+0.5);
     /*
     int hsize=(int)(1./wstep+0.5);

     check_nomsg(sinfo_star_flux_list_filter_lowpass(response,CPL_LOWPASS_LINEAR, hsize ));
      */
     check_nomsg(frm_tmp=sinfo_star_flux_list_save( response,"response_after_smooth.fits", "TEST" )) ;
     sinfo_free_frame(&frm_tmp);

     /* interpolate response over pre-defined windows */
     //check_nomsg(xsh_interpolate_high_abs_regions2(ref_std_star_list,response,phigh));
     //check_nomsg(sinfo_star_flux_list_save( response,"response_window.fits", "TEST" )) ;

     /* B-spline- fit response over flagging out pre-defined windows */
     sinfo_print_rec_status(16);
                         sinfo_msg_warning("ok16");
     //PAPERO: here was set telluric correction.

                         if(resp_fit_points == NULL) cpl_msg_warning("", "resp_fit_points == NULL");
                         else cpl_msg_warning("", "resp_fit_points <> NULL");

     if(resp_fit_points!=NULL) {
    	 	 cpl_msg_warning("", "EXTRACTING FROM %s", cpl_frame_get_filename(resp_fit_points));
         if (CPL_ERROR_NONE
                         != sinfo_parse_catalog_std_stars(resp_fit_points, dRA,
                                         dDEC, STAR_MATCH_DEPSILON,
                                         &resp_fit_points_tbl, &std_star_id)) {
             sinfo_msg_warning(
                             "Problem parsing input STD catalog. For robustness recipe goes on.");
             sinfo_msg_warning("%s", cpl_error_get_message());
             cpl_error_reset();
             sinfo_free_table(&resp_fit_points_tbl);
             return NULL ;
         }
         sinfo_msg_warning("rk18");
             sinfo_print_rec_status(18);
         sinfo_msg_warning("Determine response by cubic spline fit interpolation of points defined in input RESP_FIT_POINTS_CAT_%s",
                 sinfo_band_tostring(band));
         cpl_table_save(resp_fit_points_tbl,NULL, NULL, "resp_fit_points_tbl.fits", CPL_IO_CREATE);
         
         //cpl_table_dump(resp_fit_points_tbl,0,cpl_table_get_nrow(resp_fit_points_tbl),stdout);
         check_nomsg(response_fit=sinfo_bspline_fit_interpol(response,resp_fit_points_tbl,phigh,band));
         sinfo_msg_warning("rk19");
             sinfo_print_rec_status(19);
          
     } else {
         sinfo_msg_warning("rk20");
             sinfo_print_rec_status(20);
         sinfo_msg("Determine response by cubic spline smooth interpolation of pipeline defined regions");
         response_fit=sinfo_bspline_fit_smooth(response,phigh,band);
         sinfo_msg_warning("rk21");
             sinfo_print_rec_status(21);
     }
     sinfo_msg_warning("rk22");
        sinfo_print_rec_status(22);
     //response_fit=xsh_bspline_interpol(response,phigh,instrument);
     //abort();
     /*
     double* spline_fit=NULL; 
     int i=0;
     spline_fit=sinfo_bspline_fit_smooth_data(response->lambda,response->flux,
                       response->size,phigh,instrument,0);

     for(i=0;i<response->size;i++) {
       ref_std_star_list->flux[i]=spline_fit[i];
     }
      */
     /* Final step: frame creation, merging info from star and object list */
     char* tag = cpl_sprintf("RESPONSE_MERGE1D_%s",sinfo_band_tostring(band));
     char* fname = cpl_sprintf("RESPONSE_MERGE1D_%s",fname_obs);


     check_nomsg( result = sinfo_star_flux_list_save( response_fit, fname, tag ) ) ;
     cpl_free(fname);
     cpl_free(tag);
     check_nomsg(sinfo_response_merge_obj_std_info(result,shift_std_star_list, corr_obs_std_star_list));
     sinfo_msg_warning("rk23");
        sinfo_print_rec_status(23);

    cleanup:
    sinfo_rv_ref_wave_param_destroy(rv_ref_wave);
    sinfo_star_flux_list_free(&response);
    sinfo_star_flux_list_free(&shift_std_star_list);
    sinfo_star_flux_list_free(&ref_std_star_list);
    sinfo_star_flux_list_free(&response_fit);
    sinfo_star_flux_list_free(&corr_obs_std_star_list);
    sinfo_star_flux_list_free(&obs_std_star_list);
    sinfo_msg_warning("rk24");
       sinfo_print_rec_status(24);
    sinfo_free_propertylist(&plist);
    sinfo_free_table(&tab_obs_std_star);
    sinfo_free_table(&resp_fit_points_tbl);
    
    sinfo_free_frame(&obs_std_star_resp);
    sinfo_free_table(&tbl_ref_std_spectrum);
    return result;

}

const char* SINFO_RESPONSE_BUG_REPORT = "amodigli@eso.org";
static void sinfo_response_test(const char* spc_name, const char* cat_name,
                   const char* atm_ext_name, const char* high_abs_name,
                   const char* resp_fit_points_name, const char* tell_mod_cat_name)
{

	cpl_test(CPL_ERROR_NONE == CPL_ERROR_NONE);
	cpl_frame * obs_std_star = NULL;
    cpl_frame * flux_std_star_cat = NULL;
    cpl_frame * atmos_ext = NULL;
    cpl_frame* high_abs = NULL;
    cpl_frame* resp_fit_points = NULL;
    cpl_frame* tell_mod_cat = NULL;
    new_sinfo_band band = INSTRUMENT_BAND_K;
    double exptime=600;
    const int tell_corr=1;
   
    obs_std_star=sinfo_frame_product(spc_name, "STD_STAR_SPECTRA", CPL_FRAME_TYPE_TABLE,
                      CPL_FRAME_GROUP_PRODUCT,CPL_FRAME_LEVEL_FINAL);
    sinfo_msg_warning("cat_name=%s",cat_name);
    flux_std_star_cat=sinfo_frame_product(cat_name, "FLUX_STD_CATALOG_NIR", CPL_FRAME_TYPE_TABLE,
                          CPL_FRAME_GROUP_PRODUCT,CPL_FRAME_LEVEL_FINAL);

    atmos_ext=sinfo_frame_product(atm_ext_name, "ATMOS_EXT_NIR", CPL_FRAME_TYPE_TABLE,
                          CPL_FRAME_GROUP_PRODUCT,CPL_FRAME_LEVEL_FINAL);

    /*
    high_abs=sinfo_frame_product(high_abs_name, "ATMOS_EXT_NIR", CPL_FRAME_TYPE_TABLE,
                          CPL_FRAME_GROUP_PRODUCT,CPL_FRAME_LEVEL_FINAL);
                          */

    resp_fit_points=sinfo_frame_product(resp_fit_points_name, "RESP_FIT_POINTS_CAT_NIR",
                    CPL_FRAME_TYPE_TABLE,CPL_FRAME_GROUP_PRODUCT,
                    CPL_FRAME_LEVEL_FINAL);

    tell_mod_cat=sinfo_frame_product(tell_mod_cat_name, "TELL_MOD_CAT_NIR",
                    CPL_FRAME_TYPE_TABLE, CPL_FRAME_GROUP_PRODUCT,
                    CPL_FRAME_LEVEL_FINAL);
    sinfo_msg_warning("ok5 %s",spc_name);
    cpl_propertylist* plist = cpl_propertylist_load(spc_name,0);
    char test_band[10];
    char instrume[10];
    strcpy(instrume, cpl_propertylist_get_string(plist, "INSTRUME"));
    sinfo_msg_warning("INSTRUME=%s",instrume);
    if (!strcmp(instrume, "SINFONI"))  {
        strcpy(test_band, cpl_propertylist_get_string(plist, KEY_NAME_FILT_NAME));
        if (!strcmp(test_band, "J"))  {
            band=INSTRUMENT_BAND_J ;
        } else if (!strcmp(test_band, "H"))  {
            band=INSTRUMENT_BAND_H ;
        } else if (!strcmp(test_band, "K"))  {
            band=INSTRUMENT_BAND_K ;
        } else if (!strcmp(test_band, "HK") || !strcmp(test_band, "H+K"))  {
            band=INSTRUMENT_BAND_HK ;
        } else   {
            band=INSTRUMENT_BAND_L ;
        }
    } else if (!strcmp(instrume, "XSHOOTER"))  {
        strcpy(test_band, cpl_propertylist_get_string(plist, "ESO SEQ ARM"));
               if (!strcmp(test_band, "UVB"))  {
                   band=INSTRUMENT_BAND_UVB ;
        } else if (!strcmp(test_band, "VIS"))  {
            band=INSTRUMENT_BAND_VIS ;
        } else if (!strcmp(test_band, "NIR"))  {
            band=INSTRUMENT_BAND_NIR ;
        }
    } else if (!strcmp(instrume, "SYNTHETIC"))  {
        band=INSTRUMENT_BAND_ALL ;
        sinfo_msg_warning("SYNTHETIC");
    }

    sinfo_free_propertylist(&plist);
    cpl_frame* resp=NULL;

    resp=sinfo_compute_response(obs_std_star,flux_std_star_cat,
                                       atmos_ext,high_abs,resp_fit_points,
                                       tell_mod_cat,band,
                                       exptime,tell_corr );

    sinfo_free_frame(&obs_std_star);
    sinfo_free_frame(&flux_std_star_cat);
    sinfo_free_frame(&atmos_ext);
    //sinfo_free_frame(&high_abs);
    sinfo_free_frame(&resp_fit_points);
    sinfo_free_frame(&tell_mod_cat);
    sinfo_free_frame(&resp);

}
static int test_small(void){


cpl_image* input_image = cpl_image_new(5,5,CPL_TYPE_FLOAT);
   cpl_image_add_scalar(input_image,5000.);
   cpl_image_dump_structure(input_image,stdout);
   cpl_image_save(input_image, "pippo.fits", CPL_TYPE_FLOAT, NULL, CPL_IO_CREATE);
}


int main( int argc, char** argv)
{
	cpl_test_init(SINFO_RESPONSE_BUG_REPORT, CPL_MSG_WARNING);
	//test_small();
	

  const char * obs_spc_std_name = NULL;
  const char * ref_std_cat_name = NULL;
  const char * atm_ext_name = NULL;
  const char * high_abs_name = NULL;
  const char * resp_fit_points_name = NULL;
  const char * tell_mod_cat_name = NULL;

  /* Analyse parameters */
  sinfo_msg("argc=%d",argc);
  if ( argc == 3) {
      sinfo_ima2table(argv);
  } else if ( argc > 5) {
    obs_spc_std_name = argv[1];
    ref_std_cat_name = argv[2];
    atm_ext_name = argv[3];
    //high_abs_name = argv[4];
    resp_fit_points_name = argv[4];
    tell_mod_cat_name = argv[5];
    sinfo_msg_warning("obs_spc_std_name=%s ref_std_cat_name=%s",obs_spc_std_name,ref_std_cat_name);
    sinfo_response_test(obs_spc_std_name,ref_std_cat_name,atm_ext_name,high_abs_name,
                  resp_fit_points_name,tell_mod_cat_name);
  }
  else{
    printf(SYNTAX);
    return cpl_test_end(0);
  
  }

     
	return cpl_test_end(0);
}
