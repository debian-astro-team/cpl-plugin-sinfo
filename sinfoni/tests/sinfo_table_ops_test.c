/*                                                                            *
 *   This file is part of the ESO SINFO Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2013-08-15 12:29:21 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.5  2012/04/26 15:23:49  amodigli
 * cleaned errors from Jenkins static checks
 *
 * Revision 1.4  2009/06/05 06:06:11  amodigli
 * updated init/end to cpl5
 *
 * Revision 1.3  2008/08/26 11:21:05  amodigli
 * separate comments
 *
 * Revision 1.2  2008/02/12 10:09:49  amodigli
 * shortened lines
 *
 * Revision 1.1  2007/08/29 10:59:37  amodigli
 * added to repository
 *
 * Revision 1.2  2007/08/10 06:36:47  amodigli
 * fixed leaks
 *
 * Revision 1.1  2007/08/09 13:00:31  amodigli
 * added to repository
 *
 * Revision 1.2  2007/03/27 14:38:48  amodigli
 * fixed compilation warnings
 *
 * Revision 1.1  2007/02/23 13:10:24  amodigli
 * added test
 *
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdio.h>
#include <math.h>

#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_globals.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_utils_wrappers.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_table_ops_test  SINFO library unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/
   





/**
   @brief      test streehl compuatation
 */
/*---------------------------------------------------------------------------*/
#define SIZE 80
static void
test_table_flag_nan(void)
{
 
  cpl_table* tab=NULL;
  const int sz=10;
  int i=0;

   int* pseq=NULL;
  double* pval=NULL;


  check_nomsg(tab=cpl_table_new(sz));
  check_nomsg(cpl_table_new_column(tab,"SEQ",CPL_TYPE_INT));
  check_nomsg(cpl_table_new_column(tab,"VAL",CPL_TYPE_DOUBLE));
  check_nomsg(cpl_table_fill_column_window_int(tab,"SEQ",0,sz,0));
  check_nomsg(cpl_table_fill_column_window_double(tab,"VAL",0,sz,0));


  check_nomsg(pseq=cpl_table_get_data_int(tab,"SEQ"));
  check_nomsg(pval=cpl_table_get_data_double(tab,"VAL"));

  for(i=0;i<sz;i++) {
    sinfo_msg_warning("pval=%f",pval[i]);
    sinfo_msg_warning("pseq=%d",pseq[i]);
    check_nomsg(pseq[i]=i);
    check_nomsg(pval[i]=(double)sqrt(fabs(i)));
    sinfo_msg("seg=%d val=%g",pseq[i],pval[i]);

  }
  check_nomsg(cpl_table_set_double(tab,"VAL",sz/2,ZERO));
  cpl_table_dump(tab,0,sz,stdout);
  check_nomsg(cpl_table_save(tab, NULL, NULL,"tab.fits", CPL_IO_DEFAULT));
  for(i=0;i<sz;i++) {
    if(isnan(pval[i])) {
      cpl_table_set_invalid(tab,"VAL",i);
      cpl_table_set_invalid(tab,"SEQ",i);
      sinfo_msg_warning("pval=%f pseq=%d",pval[i],pseq[i]);
    }
  }

  check_nomsg(cpl_table_save(tab, NULL, NULL,"tab_flag.fits", CPL_IO_DEFAULT));
 

 cleanup:
 
  sinfo_free_table(&tab);
 
 
  return;

}
/*----------------------------------------------------------------------------*/
/**
  @brief   SINFONI pipeline unit test for cube coadd

**/
/*----------------------------------------------------------------------------*/

int main(void)
{

  cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);
  cpl_errorstate ESTATE=cpl_errorstate_get();

  check(test_table_flag_nan(),"Fail testing table NAN flagging");

  cleanup:
  cpl_errorstate_dump(ESTATE, CPL_FALSE, cpl_errorstate_dump_one);

  return cpl_test_end(0);

}


/**@}*/
