/*                                                                            *
 *   This file is part of the ESO SINFO Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 06:06:11 $
 * $Revision: 1.8 $
 * $Name: not supported by cvs2svn $
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdio.h>
#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_resampling.h>
#include <sinfo_image_ops.h>
#include <sinfo_utils_wrappers.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_image_ops_test  SINFO library unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/
   






/*
static void
test_line_corr(void)
{

  int width=4;
  int filt_rad=3;
  int kappa=18;//good value to indentify reasonably deviating bad pixels.

  const char* dir="/data2/sinfoni/sinfo_demo/raw/DARK/";
  const char* name="SINFO.2005-08-22T11:28:56.699.fits";

  char file[256];
  cpl_image* ima=NULL;
  cpl_image* ima_out=NULL;


  sprintf(file,"%s%s",dir,name);

  check_nomsg(ima=cpl_image_load(file,CPL_TYPE_FLOAT,0,0));

  check_nomsg(sinfo_image_line_corr(width,filt_rad,kappa,ima,&ima_out));

  check_nomsg(cpl_image_save(ima_out,"ima_new.fits",CPL_BPP_IEEE_FLOAT,
			     NULL,CPL_IO_DEFAULT));


 cleanup:

  return ;


}

*/
/**
   @brief      Shift an image 
 */
/*---------------------------------------------------------------------------*/
#define SIZE 80
static void
test_image_shift(void)
{
  int sx=64;
  int sy=64;

  double xc=sx/2;
  double yc=sy/2;
  double sigx=sx/16;
  double sigy=sy/16;
  double min_noise=-0;
  double max_noise=+30;
  double y=1000;
  double shift_x=sx/8;
  double shift_y=sy/8;
  double * kernel =NULL;

  cpl_image* img_o=NULL;
  cpl_image* img_s=NULL;

  char name[SIZE];
  char * kernel_type = "tanh";
 

  check_nomsg(img_o=cpl_image_new(sx,sy,CPL_TYPE_FLOAT));
  check_nomsg(img_s=cpl_image_new(sx,sy,CPL_TYPE_FLOAT));
  check_nomsg(cpl_image_fill_gaussian(img_o,xc,yc,y,sigx,sigy));
  check_nomsg(cpl_image_fill_noise_uniform(img_s,min_noise,max_noise));
  check_nomsg(cpl_image_add(img_o,img_s));
  sinfo_free_image(&img_s);

  sprintf(name,"%s","img.fits");
  check_nomsg(cpl_image_save(img_o,name,CPL_BPP_IEEE_FLOAT,
			     NULL,CPL_IO_DEFAULT));
  cknull_nomsg(kernel = sinfo_generate_interpolation_kernel( kernel_type )); 
  check_nomsg(img_s=sinfo_new_shift_image(img_o,shift_x, shift_y,kernel));
  sprintf(name,"%s","img_shift.fits");
  check_nomsg(cpl_image_save(img_s,name,CPL_BPP_IEEE_FLOAT,
			     NULL,CPL_IO_DEFAULT));

 cleanup:
  cpl_free(kernel);
  sinfo_free_image(&img_s);
  sinfo_free_image(&img_o);
  return;

}
/*----------------------------------------------------------------------------*/
/**
  @brief   SINFONI pipeline unit test for cube coadd

**/
/*----------------------------------------------------------------------------*/

int main(void)
{
  cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    /* Initialize CPL + SINFO messaging */
  check(test_image_shift(),"Fail testing image shift");
  /* TODO: Make work following unit test
  check(test_line_corr(),"Fail testing line corr");
  */
 cleanup:

  return cpl_test_end(0);

}


/**@}*/
