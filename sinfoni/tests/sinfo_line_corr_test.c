/*                                                                            *
 *   This file is part of the ESO SINFO Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */

/*
 * $Author: amodigli $
 * $Date: 2013-08-15 12:39:35 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 * */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_image_ops.h>

const char* SINFO_EFF_BUG_REPORT = "amodigli@eso.org";

static cpl_image* prepare_image(double* sigma)
{
	const int IMG_SZX = 500;
	const int IMG_SZY = 500;
	int i = 0;
	int j = 0;
	const float FLUX_MIN = 6;
	const float FLUX_MAX = 7;
	const float FLUX_OFFSET = 2;
	const float LINE_VALUE = 3;
	const int LINE_POS = 100;
	cpl_image* pimage = cpl_image_new(IMG_SZX, IMG_SZY, CPL_TYPE_FLOAT);
	cpl_image_fill_noise_uniform(pimage, FLUX_MIN, FLUX_MAX);
	for (j = 0; j < IMG_SZY; j++)
	{
		for (i = 0; i < 4; i++)
		{
			int pis_rejected;
			double value = cpl_image_get(pimage,i + 1,  j + 1, &pis_rejected);
			cpl_image_set(pimage, i + 1, j + 1, (value - FLUX_OFFSET) > 0 ? (value - FLUX_OFFSET) : FLUX_MIN - FLUX_OFFSET);
			cpl_image_set(pimage, IMG_SZX - 4 + i + 1, j + 1, (value - FLUX_OFFSET) > 0 ? (value - FLUX_OFFSET) : FLUX_MIN - FLUX_OFFSET);
		}
	}
	*sigma = cpl_image_get_stdev(pimage);
	// create a line
	for (i = 3; i < IMG_SZX - 4; i++)
	{
	  //int pis_rejected;
		//double value = cpl_image_get(pimage,i + 1,  LINE_POS + (i%5), &pis_rejected);
		cpl_image_set(pimage, i + 1, LINE_POS, /*value*/ LINE_VALUE);
		cpl_image_set(pimage, i + 1, LINE_POS + 100, /*value*/ LINE_VALUE);
	}
	return pimage;

}

static void check_image(cpl_image* pimage, double sigma)
{
	double new_sigma = cpl_image_get_stdev(pimage);
	cpl_msg_warning(cpl_func, "old sigma [%f], new [%f]", sigma, new_sigma);
}

static void line_correction_test()
{
	int width = 4;
	int filt_rad = 3;
	int kappa = 3;
	double sigma = 0;
	cpl_image * pimage = 0;
	cpl_image* poutput = 0;
	pimage = prepare_image(&sigma);
	cpl_test(pimage);
	cpl_error_code err =
		sinfo_image_line_corr(width, filt_rad, kappa,
								pimage,  &poutput);
	cpl_test(err == CPL_ERROR_NONE);
	check_image(poutput, sigma);
	cpl_image_save(pimage, "corr_in.fits", CPL_BPP_IEEE_FLOAT, NULL, CPL_IO_CREATE);
	cpl_image_save(poutput, "corr_out.fits", CPL_BPP_IEEE_FLOAT, NULL, CPL_IO_CREATE);
	cpl_image_delete(pimage);
	cpl_image_delete(poutput);
        return;
}


int main(void)
{
	cpl_test_init(SINFO_EFF_BUG_REPORT, CPL_MSG_WARNING);
	line_correction_test();
	return cpl_test_end(0);
}
