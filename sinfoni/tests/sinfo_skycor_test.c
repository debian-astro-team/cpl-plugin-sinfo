/*                                                                            *
 *   This file is part of the ESO SINFO Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 06:06:11 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 * $Log: not supported by cvs2svn $
 * Revision 1.4  2008/02/12 10:09:49  amodigli
 * shortened lines
 *
 * Revision 1.3  2008/01/03 12:06:39  amodigli
 * added IRPLIB_TEST_INIT/END
 *
 * Revision 1.2  2007/03/27 14:38:48  amodigli
 * fixed compilation warnings
 *
 * Revision 1.1  2007/02/23 13:10:24  amodigli
 * added test
 *
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include<sinfo_msg.h>
#include <cpl_test.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_skycor_test  SINFO library unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/
   
/*----------------------------------------------------------------------------*/
/**
  @brief   SINFONI pipeline unit test for skycor

**/
/*----------------------------------------------------------------------------*/

int main(void)
{
    /* Initialize CPL + SINFO messaging */
  cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

  sinfo_msg("Hello World!");

  return cpl_test_end(0);

}


/**@}*/
