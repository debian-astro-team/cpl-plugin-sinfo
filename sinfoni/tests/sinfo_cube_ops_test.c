/*                                                                            *
 *   This file is part of the ESO SINFO Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 06:06:11 $
 * $Revision: 1.6 $
 * $Name: not supported by cvs2svn $
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <stdio.h>
#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_utils_wrappers.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_cube_ops_test  SINFO library unit tests
 */
#define SIZE 256
/*----------------------------------------------------------------------------*/
/**@{*/
   


/**
   @brief      test cube load
 */
/*---------------------------------------------------------------------------*/
/* TODO: find a way to generate a cube with the unit test
static void
test_cube_load_data(void)
{
  cpl_imagelist* cube_tst=NULL;
  cpl_image* img=NULL;
  char name[SIZE];
  int i=0;
  int sz=0;
  //char* src_data="/data2/sinfoni/sinfo_demo/pro/sinfo_rec_jitter_objnod_358/";
  //char* src_data="/data2/sinfoni/sinfo_demo/test/";
  char* src_data="/home/amodigli/pipeline/sinfonip/sinfoni/tests/";

  //We use input data generated by the pipeline
  //sprintf(name,"%s%s%2.2d%s",src_data,"out_cube_obj",i,".fits");
  sprintf(name,"%s%s",src_data,"cub_bas0.fits");
  sinfo_msg_warning("name=%s",name);
  check_nomsg(cube_tst = cpl_imagelist_load(name,CPL_TYPE_FLOAT,0));
  sz=cpl_imagelist_get_size(cube_tst);

  for(i=sz/2;i<sz/2+10;i++) {
    check_nomsg(img=cpl_imagelist_get(cube_tst,i));
    sprintf(name,"%s%4.4d%s","ima_tst",i,".fits");
    check_nomsg(cpl_image_save(img,name,CPL_BPP_IEEE_FLOAT,
			       NULL,CPL_IO_DEFAULT));
  }

  sprintf(name,"%s","cub_tst.fits");
  check_nomsg(cpl_imagelist_save(cube_tst,name,CPL_BPP_IEEE_FLOAT,
				 NULL,CPL_IO_DEFAULT));
  sinfo_msg_warning("Note that fails before this line");
 cleanup:
  sinfo_msg_warning("about to exit");

  sinfo_free_imagelist(&cube_tst);

  return;

}

*/
/*----------------------------------------------------------------------------*/
/**
  @brief   SINFONI pipeline unit test for cube load

**/
/*----------------------------------------------------------------------------*/

int main(void)
{
  cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

    /* Initialize CPL + SINFO messaging */
  //test_cube_load_data();

  return cpl_test_end(0);

}


/**@}*/
