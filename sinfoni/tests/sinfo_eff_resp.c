#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <string.h>
#include <sinfo_eff_resp.h>
#include <sinfo_msg.h>
#include <sinfo_error.h>
#include <sinfo_pfits.h>
#include <sinfo_utils_wrappers.h>
#include <stdbool.h>

#define SINFO_EXTCOEFF_TAB "EXTCOEFF_TABLE"

#define PRO_STD_STAR_SPECTRA "STD_STAR_SPECTRA"
static const char COL_NAME_HIGH_ABS[] = "HIGH_ABS";
static const char COL_NAME_WAVELENGTH[] = "WAVELENGTH";
//static const char COL_NAME_WAVELENGTH_C[] = "WAVELENGTH";
static const char COL_NAME_WAVE_ATMDISP[] = "LAMBDA";
static const char COL_NAME_ABS_ATMDISP[] = SINFO_ATMOS_EXT_LIST_COLNAME_K;

static const char COL_NAME_REF[]        = "REF";
static const char COL_NAME_COR[]        = "COR";
static const char COL_NAME_SRC_COR[]    = "SRC_COR";

static const char COL_NAME_WAVE_OBJ[] = "WAVELENGTH";
static const char COL_NAME_INT_OBJ[]    = "INT_OBJ";//"counts_tot";
static const char COL_NAME_ORD_OBJ[]    = "ORD";//"counts_tot";
static const char COL_NAME_WAVE_REF[]       = "LAMBDA";
static const char COL_NAME_FLUX_REF[]       = "FLUX";
static const char COL_NAME_BINWIDTH_REF[]   = "BIN_WIDTH";
static const char COL_NAME_EPHOT[]      = "EPHOT";
static const char COL_NAME_EXT[]        = "EXT";
static const char COL_NAME_SRC_EFF[]    = "EFF";
//static const char PAR_NAME_DIT[]      = "ESO DET DIT";
//static const double UVES_flux_factor =1.e16;


HIGH_ABS_REGION NirJHighAbsRegions[] = {
  {1076.0, 1100.0}, // nm (water vapour) dummy
  {1112.0, 1176.0}, // nm (water vapour)
  {1260.0, 1296.0}, // nm (water vapour)
  {1330.0, 1506.0}, // nm (region between J and H)
  {0., 0.}
} ;

HIGH_ABS_REGION NirHHighAbsRegions[] = {
  {1076.0, 1176.0}, // nm (water vapour)
  {1236.0, 1296.0}, // nm (water vapour)
  {1316.0, 1506.0}, // nm (region between J and H)
  {1736.0, 1986.0}, // nm (region between H and K)
  {1996.0, 2036.0}, // nm (region between H and K)
  {2046.0, 2086.0}, // nm (region between H and K)
  {0., 0.}
} ;

HIGH_ABS_REGION NirKHighAbsRegions[] = {
//  {1076.0, 1176.0}, // nm (water vapour)
//  {1236.0, 1296.0}, // nm (water vapour)
//  {1316.0, 1506.0}, // nm (region between J and H)
//  {1736.0, 1986.0}, // nm (region between H and K)
  {1996.0, 2036.0}, // nm (region between H and K)
  {2046.0, 2086.0}, // nm (region between H and K)
  {0., 0.}
} ;

HIGH_ABS_REGION NirHKHighAbsRegions[] = {
  {1076.0, 1176.0}, // nm (water vapour)
  {1236.0, 1296.0}, // nm (water vapour)
  {1316.0, 1506.0}, // nm (region between J and H)
  {1736.0, 1986.0}, // nm (region between H and K)
  {1996.0, 2036.0}, // nm (region between H and K)
  {2046.0, 2086.0}, // nm (region between H and K)
  {0., 0.}
} ;

HIGH_ABS_REGION NirHighAbsRegions[] = {
  {1076.0, 1176.0}, // nm (water vapour)
  {1236.0, 1296.0}, // nm (water vapour)
  {1316.0, 1506.0}, // nm (region between J and H)
  {1736.0, 1986.0}, // nm (region between H and K)
  {1996.0, 2036.0}, // nm (region between H and K)
  {2046.0, 2086.0}, // nm (region between H and K)
  {0., 0.}
} ;


HIGH_ABS_REGION AllHighAbsRegions[] = {
  {500.0, 532.0}, // nm
  {634.0, 642.0}, // nm
  {684.5, 696.0}, // nm
  {758.0, 770.0}, // nm
  {812.0, 819.0}, // nm
  {894.0, 924.0}, // nm
  {928.0, 968.0}, // nm
  {974.0, 984.0}, // nm
  {1076.0, 1176.0}, // nm (water vapour)
  {1236.0, 1296.0}, // nm (water vapour)
  {1316.0, 1506.0}, // nm (region between J and H)
  {1736.0, 1986.0}, // nm (region between H and K)
  {1996.0, 2036.0}, // nm (region between H and K)
  {2046.0, 2086.0}, // nm (region between H and K)
  {0., 0.}
} ;


/* 1.5.32 */
HIGH_ABS_REGION VisHighAbsRegions[] = {
  {506.0, 509.0}, // nm
  {512.0, 532.0}, // nm
  {634.0, 642.0}, // nm
  {684.5, 696.0}, // nm
  {758.0, 770.0}, // nm
  {812.0, 819.0}, // nm
  {894.0, 924.0}, // nm
  {928.0, 968.0}, // nm
  {974.0, 984.0}, // nm
  {0., 0.}
} ;


HIGH_ABS_REGION UvbHighAbsRegions[] = {
  {556.0, 580.0}, // nm
  {0., 0.}
} ;







static char FRM_EXTCOEFF_TAB[]  = SINFO_EXTCOEFF_TAB;
static void
sinfo_frame_config(const char* fname,
                 const char* tag,
                 cpl_frame_type type,
                 cpl_frame_group group,
                 cpl_frame_level level,
                 cpl_frame** frame)
{

  cpl_frame_set_filename(*frame,fname);
  cpl_frame_set_tag(*frame,tag);
  cpl_frame_set_type(*frame,type);
  cpl_frame_set_group(*frame,group);
  cpl_frame_set_level(*frame,level);

  return;

}
/*---------------------------------------------------------------------------*/
/**
   @brief free memory associated to a rec_list
   @param list the rec_list to free
*/
/*---------------------------------------------------------------------------*/
void sinfo_rec_list_free(sinfo_rec_list** list)
{
  if (list != NULL && *list != NULL ) {
    /*
      BUG - Laurent - 09-01-2009
      Something strange occurs (under Linux, not MacOs):
       in the recipe sinfo_scired_slit_nod, for UVB (not for VIS), there is
       a crash in this function during one of the cpl_free calls.
      In order to allow continuation of tests, freeing of the rec_list
       is - temporarily - disable until the problem is understood and
       fixed.
    */
    int i ;
    sinfo_rec_list * plist = NULL;

    plist = *list;
    /* free the list */

    for (i = 0; i < plist->size; i++) {
      sinfo_rec * pr = &(plist->list[i]) ;

      //sinfo_msg_dbg_high( "Freeing order index %d", i ) ;
      if ( pr == NULL ) continue ;
      //sinfo_msg_dbg_high( "     Abs Order: %d", pr->order ) ;
      cpl_free( pr->slit ) ;
      cpl_free( pr->lambda ) ;
      cpl_free( pr->data1 ) ;
      cpl_free( pr->errs1 ) ;
      cpl_free( pr->qual1 ) ;
    }
    if ((*list)->list){
      cpl_free((*list)->list);
    }

    sinfo_free_propertylist(&((*list)->header));
    cpl_free(*list);
    *list = NULL;
  }
}
/*---------------------------------------------------------------------------*/
/**
  @brief
    Allocate memory for the order idx of the rectify list
  @param[in] list
    The rectify list
  @param[in] idx
    The index of the order in the list
  @param[in] absorder
    The absorder of the order
  @param[in] nlambda
    The number of lambda values
  @param[in] ns
    The number of slit values
*/
/*---------------------------------------------------------------------------*/
static void sinfo_rec_list_set_data_size( sinfo_rec_list * list, int idx, int absorder,
                 int nlambda, int ns )
{
  sinfo_rec * prec = NULL ;
  int depth ;

  SINFO_ASSURE_NOT_NULL( list) ;
  SINFO_ASSURE_NOT_ILLEGAL( idx < list->size);
  assure( idx >= 0, CPL_ERROR_ILLEGAL_INPUT, "Index not in range");
  assure( idx < list->size, CPL_ERROR_ILLEGAL_INPUT,"Index not in range");
  assure( ns > 0, CPL_ERROR_ILLEGAL_INPUT,"Check size in slit");
  assure( nlambda > 0, CPL_ERROR_ILLEGAL_INPUT,"Check size in lambda");

  prec = &list->list[idx] ;
  SINFO_ASSURE_NOT_NULL( prec);

  //if ( list->max_nlambda < nlambda ) list->max_nlambda = nlambda ;
  //if ( list->max_nslit < ns ) list->max_nslit = ns;
  depth = nlambda*ns;

  prec->order = absorder;
  prec->nlambda = nlambda;
  prec->nslit = ns;
  /*
  sinfo_msg_dbg_high( "Rec Data Size: nlambda: %d, ns: %d, depth: %d",
            nlambda, ns, depth);
            */
  SINFO_CALLOC( prec->slit, float, ns) ;
  SINFO_CALLOC( prec->lambda, double, nlambda);
  SINFO_CALLOC( prec->data1, float, depth);
  SINFO_CALLOC( prec->errs1, float, depth);
  SINFO_CALLOC( prec->qual1, int, depth);

 cleanup:
  return ;
}


sinfo_rec_list* sinfo_rec_list_load_eso(cpl_frame* frame,
                    new_sinfo_band band)
{
  sinfo_rec_list* result = NULL;
  const char * imagelist_name = NULL ;
  cpl_image * ima_data = NULL;
  cpl_image * ima_errs = NULL;
  cpl_image * ima_qual = NULL;
  cpl_propertylist* header = NULL;
  cpl_propertylist* hdata=NULL;
  cpl_propertylist* herrs=NULL;
  cpl_propertylist* hqual=NULL;
  const char* ext_name=NULL;

  int nbext, i,j ;
  int size;

  /* check input parameters */
  SINFO_ASSURE_NOT_NULL(frame);
  //SINFO_ASSURE_NOT_NULL(instrument);


  /* get table filename */
  check_nomsg(imagelist_name = cpl_frame_get_filename(frame));
  //sinfo_msg_dbg_low( "Loading Rectified Frame: %s", imagelist_name ) ;

  check_nomsg( nbext = cpl_frame_get_nextensions( frame));


  size=(nbext+1)/3;

  /* Create internal structure */
  /* Allocate memory */
  SINFO_CALLOC(result, sinfo_rec_list, 1);
  result->size = size;
  SINFO_ASSURE_NOT_ILLEGAL(result->size > 0);
  result->band = band;

  SINFO_CALLOC( result->list, sinfo_rec, result->size);

  SINFO_NEW_PROPERTYLIST( result->header);


  check_nomsg(header = cpl_propertylist_load(imagelist_name,0));
  check_nomsg(cpl_propertylist_erase_regexp(header,
                        "^(ARCFILE|ORIGFILE|CHECKSUM|DATASUM)$", CPL_FALSE));
  check_nomsg(cpl_propertylist_append(result->header, header));

  sinfo_free_propertylist(&header);

  //nbext = size;

  //sinfo_msg_dbg_medium( "   Nb of extensions: %d", nbext ) ;
  /* Loop over FITS extensions */

  for( i = 0,j=0 ; j<nbext ; i++, j+=3 ) {
    int k, order, depth, nlambda, nslit ;
    const float * farray = NULL ;
    const int * iarray = NULL ;
    double s_step=0;
    double w_step=0;
    double s_start=0;
    double w_start=0;
    int sx=0;
    int sy=0;

    check_nomsg(ima_data=cpl_image_load(imagelist_name,CPL_TYPE_FLOAT,0,j+0));
    check_nomsg(ima_errs=cpl_image_load(imagelist_name,CPL_TYPE_FLOAT,0,j+1));
    check_nomsg(ima_qual=cpl_image_load(imagelist_name,CPL_TYPE_INT,0,j+2));

    check_nomsg( hdata = cpl_propertylist_load( imagelist_name, j+0 ) ) ;
    check_nomsg( herrs = cpl_propertylist_load( imagelist_name, j+1 ) ) ;
    check_nomsg( hqual = cpl_propertylist_load( imagelist_name, j+2 ) ) ;
    check_nomsg(ext_name=sinfo_pfits_get_extname(herrs));
    order=10*(ext_name[3]-48)+(ext_name[4]-48);
    //sinfo_msg("iter=%d order=%d",i,order);

    /* Now populate the structure */
    result->list[i].order = order ;

    nlambda=sinfo_pfits_get_naxis1(herrs);
    nslit=sinfo_pfits_get_naxis2(herrs);

    w_step=sinfo_pfits_get_cdelt1(herrs);
    s_step=sinfo_pfits_get_cdelt2(herrs);

    w_start=sinfo_pfits_get_crval1(herrs);
    s_start=sinfo_pfits_get_crval2(herrs);

    result->list[i].nlambda = nlambda ;
    result->list[i].nslit = nslit ;

    sinfo_rec_list_set_data_size( result, i, order, nlambda, nslit ) ;

    /* Get arrays (slit, lambda) */
    if (nslit > 1){
      for(k=0;k<nslit;k++) {
    *(result->list[i].slit+k)=(s_start+k*s_step);
      }
    }
    else{
      for(k=0;k<nslit;k++) {
    *(result->list[i].slit+k)=0;
      }
    }


    for(k=0;k<nlambda;k++) {
      *(result->list[i].lambda+k)=(w_start+k*w_step);
    }
    /* Get arrays (flux, errs, qual ) */
    depth = nlambda*nslit ;

    /* Copy FLUX1 data to rec_list */
    check_nomsg( farray = cpl_image_get_data_float( ima_data)) ;
    sx = cpl_image_get_size_x( ima_data ) ;
    sy = cpl_image_get_size_y( ima_data ) ;
    SINFO_ASSURE_NOT_ILLEGAL( sx*sy == depth ) ;

    for( k = 0 ; k<depth ; k++ ) *(result->list[i].data1+k) = *farray++ ;
    farray = NULL ;

    /* Copy ERRS1 data to rec_list */
    check_nomsg( farray = cpl_image_get_data_float( ima_errs)) ;
    for( k = 0 ; k<depth ; k++ ) *(result->list[i].errs1+k) = *farray++ ;

    /* Copy QUAL1 data to rec_list */
    check_nomsg( iarray = cpl_image_get_data_int( ima_qual)) ;
    for( k = 0 ; k<depth ; k++ ) *(result->list[i].qual1+k) = *iarray++ ;

    /*
    sinfo_msg_dbg_low( "   Loaded, order %d, nlambda %d, nslit %d",
             order, nlambda, nslit ) ;
    */
    sinfo_free_image( &ima_data ) ;
    sinfo_free_image( &ima_errs ) ;
    sinfo_free_image( &ima_qual ) ;
    sinfo_free_propertylist( &hdata ) ;
    sinfo_free_propertylist( &herrs ) ;
    sinfo_free_propertylist( &hqual ) ;

  }

 cleanup:
  sinfo_free_image( &ima_data ) ;
  sinfo_free_image( &ima_errs ) ;
  sinfo_free_image( &ima_qual ) ;
  sinfo_free_propertylist( &hdata ) ;
  sinfo_free_propertylist( &herrs ) ;
  sinfo_free_propertylist( &hqual ) ;

  sinfo_free_propertylist(&header);

  return result ;
}
/*****************************************************************************/
/**
  @brief
    Compute flux and error associated to each merged spectrum point
  @param[in] flux_a
    Input flux from a contribute
  @param[in] err_a
    Error associated to flux_a
  @param[in] weight_a
    Weight associated to flux_a
  @param[in] flux_b
    Input flux from b contribute
  @param[in] err_b
    Error associated to flux_b
  @param[in] weight_b
    Weight associated to flux_b
  @param[out] flux_res
    Input flux from merged contributes
  @param[out] err_res
    Error associated to flux_res

  @return
    void (
*/
/*****************************************************************************/
void sinfo_merge_point( double flux_a, double weight_a,
  double flux_b, double weight_b, double *flux_res,
  double *err_res)
{
  SINFO_ASSURE_NOT_NULL( flux_res);
  SINFO_ASSURE_NOT_NULL( err_res);

  double tmp_val = 1.0/(weight_a+weight_b);
  *flux_res = (weight_a*flux_a+weight_b*flux_b) * tmp_val;
  *err_res =  sqrt(tmp_val);

  cleanup:
    return;
}

sinfo_rec_list* sinfo_rec_list_load_eso_1d(cpl_frame* frame,
                    new_sinfo_band band)
{
  sinfo_rec_list* result = NULL;
  const char * imagelist_name = NULL ;
  cpl_vector * vec_data = NULL;
  cpl_vector * vec_errs = NULL;
  cpl_vector * vec_qual = NULL;
  cpl_propertylist* header = NULL;
  cpl_propertylist* hdata=NULL;
  cpl_propertylist* herrs=NULL;
  cpl_propertylist* hqual=NULL;
  const char* ext_name=NULL;

  int nbext, i,j ;
  int size;

  /* check input parameters */
  SINFO_ASSURE_NOT_NULL(frame);
  //SINFO_ASSURE_NOT_NULL(instrument);


  /* get table filename */
  check_nomsg(imagelist_name = cpl_frame_get_filename(frame));
  //sinfo_msg_dbg_low( "Loading Rectified Frame: %s", imagelist_name ) ;

  check_nomsg( nbext = cpl_frame_get_nextensions( frame));


  size=(nbext+1)/3;

  /* Create internal structure */
  /* Allocate memory */
  SINFO_CALLOC(result, sinfo_rec_list, 1);
  result->size = size;
  SINFO_ASSURE_NOT_ILLEGAL(result->size > 0);
  result->band = band;

  SINFO_CALLOC( result->list, sinfo_rec, result->size);

  SINFO_NEW_PROPERTYLIST( result->header);


  check_nomsg(header = cpl_propertylist_load(imagelist_name,0));
  check_nomsg(cpl_propertylist_erase_regexp(header,
                        "^(ARCFILE|ORIGFILE|CHECKSUM|DATASUM)$", CPL_FALSE));
  check_nomsg(cpl_propertylist_append(result->header, header));

  sinfo_free_propertylist(&header);

  //nbext = size;

  //sinfo_msg_dbg_medium( "   Nb of extensions: %d", nbext ) ;
  /* Loop over FITS extensions */

  for( i = 0,j=0 ; j<nbext ; i++, j+=3 ) {
    int k, order, depth, nlambda;
    const double * farray = NULL ;
    const double * iarray = NULL ;
    double w_step=0;
    double w_start=0;
    int sx=0;

    check_nomsg(vec_data=cpl_vector_load(imagelist_name,j+0));
    check_nomsg(vec_errs=cpl_vector_load(imagelist_name,j+1));
    check_nomsg(vec_qual=cpl_vector_load(imagelist_name,j+2));

    check_nomsg( hdata = cpl_propertylist_load( imagelist_name, j+0 ) ) ;
    check_nomsg( herrs = cpl_propertylist_load( imagelist_name, j+1 ) ) ;
    check_nomsg( hqual = cpl_propertylist_load( imagelist_name, j+2 ) ) ;
    check_nomsg(ext_name=sinfo_pfits_get_extname(herrs));
    order=10*(ext_name[3]-48)+(ext_name[4]-48);
    //sinfo_msg("iter=%d order=%d",i,order);

    /* Now populate the structure */
    result->list[i].order = order ;

    nlambda=sinfo_pfits_get_naxis1(herrs);

    w_step=sinfo_pfits_get_cdelt1(herrs);

    w_start=sinfo_pfits_get_crval1(herrs);

    result->list[i].nlambda = nlambda ;

    sinfo_rec_list_set_data_size( result, i, order, nlambda, 1 ) ;

    /* Get arrays (lambda) */
    for(k=0;k<nlambda;k++) {
      *(result->list[i].lambda+k)=(w_start+k*w_step);
    }
    /* Get arrays (flux, errs, qual ) */
    depth = nlambda;

    /* Copy FLUX1 data to rec_list */
    check_nomsg( farray = cpl_vector_get_data( vec_data)) ;
    sx = cpl_vector_get_size( vec_data ) ;
    SINFO_ASSURE_NOT_ILLEGAL( sx == depth ) ;

    for( k = 0 ; k<depth ; k++ ) *(result->list[i].data1+k) = (float)*farray++ ;
    farray = NULL ;

    /* Copy ERRS1 data to rec_list */
    check_nomsg( farray = cpl_vector_get_data( vec_errs)) ;
    for( k = 0 ; k<depth ; k++ ) *(result->list[i].errs1+k) = (float)*farray++ ;

    /* Copy QUAL1 data to rec_list */
    check_nomsg( iarray = cpl_vector_get_data( vec_qual)) ;
    for( k = 0 ; k<depth ; k++ ) *(result->list[i].qual1+k) = (int)*iarray++ ;

    //sinfo_msg_dbg_low( "   Loaded, order %d, nlambda %d",order, nlambda) ;

    cpl_vector_delete( vec_data ) ;
    cpl_vector_delete( vec_errs ) ;
    cpl_vector_delete( vec_qual ) ;
    sinfo_free_propertylist( &hdata ) ;
    sinfo_free_propertylist( &herrs ) ;
    sinfo_free_propertylist( &hqual ) ;

  }

 cleanup:
  cpl_vector_delete( vec_data ) ;
  cpl_vector_delete( vec_errs ) ;
  cpl_vector_delete( vec_qual ) ;
  sinfo_free_propertylist( &hdata ) ;
  sinfo_free_propertylist( &herrs ) ;
  sinfo_free_propertylist( &hqual ) ;

  sinfo_free_propertylist(&header);

  return result ;
}


/*---------------------------------------------------------------------------*/
/**
   @brief load an rec list from a frame
   @param frame the table wich contains polynomials coefficients of the
   orders
   @param instrument instrument in use
   @return the rec list structure

*/
/*---------------------------------------------------------------------------*/

sinfo_rec_list* sinfo_rec_list_load(cpl_frame* frame,
                new_sinfo_band band)
{
  sinfo_rec_list* result = NULL;
  const char * tablename = NULL ;
  cpl_table * table = NULL;
  cpl_propertylist* header = NULL;
  int nbext, i ;
  int size;

  /* check input parameters */
  SINFO_ASSURE_NOT_NULL(frame);
  //SINFO_ASSURE_NOT_NULL(instrument);

  /* get table filename */
  check_nomsg(tablename = cpl_frame_get_filename(frame));
  //sinfo_msg_dbg_low( "Loading Rectified Frame: %s", tablename ) ;

  check_nomsg( size = cpl_frame_get_nextensions( frame));

  /* Create internal structure */
  /* Allocate memory */
  SINFO_CALLOC(result, sinfo_rec_list, 1);
  result->size = size;
  SINFO_ASSURE_NOT_ILLEGAL(result->size > 0);
  result->band = band;

  SINFO_CALLOC( result->list, sinfo_rec, result->size);

  SINFO_NEW_PROPERTYLIST( result->header);

  check_nomsg(header = cpl_propertylist_load(tablename,0));
  check_nomsg(cpl_propertylist_erase_regexp(header,
                        "^(ARCFILE|ORIGFILE|CHECKSUM|DATASUM)$", CPL_FALSE));
  check_nomsg(cpl_propertylist_append(result->header, header));
  //sinfo_msg("tablename=%s",tablename);
  //check(result->slit_min=sinfo_pfits_get_extract_slit_min(header));
  //check(result->slit_max=sinfo_pfits_get_extract_slit_max(header));

  sinfo_free_propertylist(&header);

  nbext = size;

  //sinfo_msg_dbg_medium( "   Nb of extensions: %d", nbext ) ;
  /* Loop over FITS extensions */
  for( i = 0 ; i<nbext ; i++ ) {
    int nb, k, order, depth, nlambda, nslit ;
    const cpl_array * data_array ;
    const float * farray = NULL ;
    const int * iarray = NULL ;

    check_nomsg( table = cpl_table_load( tablename, i+1, 0 ) ) ;
    check_nomsg(sinfo_get_table_value(table, SINFO_REC_TABLE_COLNAME_ORDER,
                  CPL_TYPE_INT, 0, &order));
    /* Now populate the structure */
    result->list[i].order = order ;
    check_nomsg(sinfo_get_table_value(table, SINFO_REC_TABLE_COLNAME_NLAMBDA,
                  CPL_TYPE_INT, 0, &nlambda ));
    result->list[i].nlambda = nlambda ;
    check_nomsg(sinfo_get_table_value(table, SINFO_REC_TABLE_COLNAME_NSLIT,
                  CPL_TYPE_INT, 0, &nslit ));
    result->list[i].nslit = nslit ;
    sinfo_rec_list_set_data_size( result, i, order, nlambda, nslit ) ;
    /* Get arrays (slit, lambda) */
    if (nslit > 1){
    check_nomsg( sinfo_table_get_array_float( table, SINFO_REC_TABLE_COLNAME_SLIT,
      result->list[i].slit, nslit));
    }
    else{
      check_nomsg(sinfo_get_table_value(table, SINFO_REC_TABLE_COLNAME_SLIT,
                              CPL_TYPE_FLOAT, 0, result->list[i].slit ));
    }
    check_nomsg( sinfo_table_get_array_double( table, SINFO_REC_TABLE_COLNAME_LAMBDA,
      result->list[i].lambda, nlambda));

    /* Get arrays (flux, errs, qual ) */
    depth = nlambda*nslit ;

    check_nomsg( data_array = cpl_table_get_array( table,
                         SINFO_REC_TABLE_COLNAME_FLUX1,
                         0 )) ;
    nb = cpl_array_get_size( data_array ) ;
    SINFO_ASSURE_NOT_ILLEGAL( nb == depth ) ;

    /* Copy FLUX1 data to rec_list */
    check_nomsg( farray = cpl_array_get_data_float_const( data_array ) ) ;
    for( k = 0 ; k<depth ; k++ ) *(result->list[i].data1+k) = *farray++ ;

    farray = NULL ;
    check_nomsg( data_array = cpl_table_get_array( table,
                         SINFO_REC_TABLE_COLNAME_ERRS1,
                         0 )) ;
    check_nomsg( nb = cpl_array_get_size( data_array ) ) ;
    /* Copy ERRS1 data to rec_list */
    check_nomsg( farray = cpl_array_get_data_float_const( data_array ) ) ;
    for( k = 0 ; k<depth ; k++ ) *(result->list[i].errs1+k) = *farray++ ;

    check_nomsg( data_array = cpl_table_get_array( table,
                         SINFO_REC_TABLE_COLNAME_QUAL1,
                         0 )) ;
    nb = cpl_array_get_size( data_array ) ;
    /* Copy QUAL1 data to rec_list */
    iarray = cpl_array_get_data_int_const( data_array ) ;
    for( k = 0 ; k<depth ; k++ ) *(result->list[i].qual1+k) = *iarray++ ;
    /*
    sinfo_msg_dbg_low( "   Loaded, order %d, nlambda %d, nslit %d",
             order, nlambda, nslit ) ;
     */
    cpl_table_delete( table ) ;
  }

 cleanup:
  sinfo_free_propertylist(&header);
  return result ;
}


/*---------------------------------------------------------------------------*/
/**
   @brief    Read a table value from a fits table
   @param    table table to read
   @param    colname Name of column to read
   @param    coltype Type of column
   @param    i row to read
   @param    result The value read

   @return   CPL_ERROR_NONE iff OK
   @note derived from UVES pipeline
*/
/*---------------------------------------------------------------------------*/
cpl_error_code sinfo_get_table_value (const cpl_table* table,
  const char *colname, cpl_type coltype, int i, void* result)
{
  int flag = 0;
  /* Check input */
  SINFO_ASSURE_NOT_NULL( table);
  SINFO_ASSURE_NOT_NULL( colname);
  SINFO_ASSURE_NOT_NULL( result);

  /* Read the column */

  switch (coltype) {
    case CPL_TYPE_INT:
      check (*((int *) result) = cpl_table_get_int(table,colname,i,&flag),
        "Could not get (integer) value of %s at row %d", colname,i);
    break;
  case CPL_TYPE_FLOAT:
    check (*((float *) result) = cpl_table_get_float( table,
      colname,i,&flag),
      "Could not get (float) value of %s at row %d",colname,i);
    break;
  case CPL_TYPE_DOUBLE:
    check (*((double *) result) = cpl_table_get_double (table,
      colname,i,&flag),
      "Could not get (double) value of %s at row %d",colname,i);
    break;
  case CPL_TYPE_STRING:
    check(*((const char **) result) =
               cpl_table_get_string (table, colname,i),
               "Could not get (string) value of %s at row %d",colname,i);
    break;
  default:
    assure (false, CPL_ERROR_INVALID_TYPE, "Unknown type");
  }

cleanup:
  return cpl_error_get_code ();
}


SINFO_TABLE_GET_ARRAY(int)
SINFO_TABLE_GET_ARRAY(float)
SINFO_TABLE_GET_ARRAY(double)

/*---------------------------------------------------------------------------*/
/**
 * @brief Create a 1D spectrum  structure
 *
 * @param[in] lambda_min minimum wavelength of spectrum
 * @param[in] lambda_max maximum wavelength of spectrum
 * @param[in] lambda_step lambda binning
 *
 * @return the spectrum structure
 */
/*---------------------------------------------------------------------------*/
sinfo_spectrum* sinfo_spectrum_1D_create( double lambda_min, double lambda_max,
  double lambda_step)
{
  sinfo_spectrum* result = NULL;


  /* check input parameters */
  SINFO_ASSURE_NOT_ILLEGAL( lambda_min >= 0.0 && lambda_min <= lambda_max);
  SINFO_ASSURE_NOT_ILLEGAL( lambda_step >=0);

  SINFO_CALLOC(result, sinfo_spectrum,1);

  result->lambda_min = lambda_min;
  result->lambda_max = lambda_max;
  result->lambda_step = lambda_step;

  SINFO_NEW_PROPERTYLIST( result->flux_header);
  check_nomsg(sinfo_pfits_set_wcs1(result->flux_header, 1.0, lambda_min, lambda_step));

  SINFO_NEW_PROPERTYLIST( result->errs_header);
  check_nomsg( sinfo_pfits_set_extname ( result->errs_header, "ERRS"));
  check_nomsg(sinfo_pfits_set_wcs1(result->errs_header, 1.0, lambda_min, lambda_step));

  SINFO_NEW_PROPERTYLIST( result->qual_header);
  check_nomsg( sinfo_pfits_set_extname ( result->qual_header, "QUAL"));


  result->size_lambda = (int)((lambda_max-lambda_min)/lambda_step+0.5)+1;
  result->size_slit = 1;
  result->slit_min = 0;
  result->slit_max = 0;
  result->size = result->size_lambda;

  check_nomsg( result->flux = cpl_image_new( result->size_lambda, 1,
    CPL_TYPE_DOUBLE));
  check_nomsg( result->errs = cpl_image_new( result->size_lambda, 1,
    CPL_TYPE_DOUBLE));
  check_nomsg( result->qual = cpl_image_new( result->size_lambda, 1,
    CPL_TYPE_INT));

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      sinfo_spectrum_free(&result);
    }
    return result;
}


/*---------------------------------------------------------------------------*/
/**
 * @brief Create a 2D spectrum structure
 *
 * @param[in] lambda_min minimum wavelength of spectrum
 * @param[in] lambda_max maximum wavelength of spectrum
 * @param[in] lambda_step lambda binning
 * @param[in] slit_min minimum slit value
 * @param[in] slit_max maximum slit value
 * @param[in] slit_step slit step
 *
 * @return the spectrum structure
 */
/*---------------------------------------------------------------------------*/
sinfo_spectrum* sinfo_spectrum_2D_create( double lambda_min, double lambda_max,
  double lambda_step, double slit_min, double slit_max, double slit_step)
{
  sinfo_spectrum* result = NULL;

  /* check input parameters */
  SINFO_ASSURE_NOT_ILLEGAL( lambda_min >= 0.0 && lambda_min <= lambda_max);
  SINFO_ASSURE_NOT_ILLEGAL( lambda_step >=0);
  SINFO_ASSURE_NOT_ILLEGAL( slit_min <= slit_max);
  SINFO_ASSURE_NOT_ILLEGAL( slit_step >=0);


  SINFO_CALLOC(result, sinfo_spectrum,1);

  result->lambda_min = lambda_min;
  result->lambda_max = lambda_max;
  result->lambda_step = lambda_step;
  result->slit_min = slit_min;
  result->slit_max =  slit_max;
  result->slit_step = slit_step;

  SINFO_NEW_PROPERTYLIST( result->flux_header);
  check_nomsg(sinfo_pfits_set_wcs1(result->flux_header, 1.0, lambda_min, lambda_step));
  check_nomsg(sinfo_pfits_set_wcs2(result->flux_header, 1.0, slit_min, slit_step));

  check_nomsg(sinfo_set_cd_matrix2d(result->flux_header));

  SINFO_NEW_PROPERTYLIST( result->errs_header);
  check_nomsg( sinfo_pfits_set_extname ( result->errs_header, "ERRS"));
  SINFO_NEW_PROPERTYLIST( result->qual_header);
  check_nomsg( sinfo_pfits_set_extname ( result->qual_header, "QUAL"));


  result->size_lambda = (int)((lambda_max-lambda_min)/lambda_step+0.5)+1;
  result->size_slit = (int)((slit_max-slit_min)/slit_step+0.5)+1;
  result->size = result->size_lambda * result->size_slit;
  check_nomsg( result->flux = cpl_image_new( result->size_lambda, result->size_slit,
    CPL_TYPE_DOUBLE));
  check_nomsg( result->errs = cpl_image_new( result->size_lambda, result->size_slit,
    CPL_TYPE_DOUBLE));
  check_nomsg( result->qual = cpl_image_new( result->size_lambda, result->size_slit,
    CPL_TYPE_INT));

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE) {
      sinfo_spectrum_free(&result);
    }
    return result;
}


/*----------------------------------------------------------------*/
/**
 * @brief Convert a CPL type to a string
 * @param t  Type to convert
 * @return A textual representation of @em  t.
 */
/*----------------------------------------------------------------*/
const char *
sinfo_tostring_cpl_type (cpl_type t)
{

  /* Note that CPL_TYPE_STRING is shorthand
     for CPL_TYPE_CHAR | CPL_TYPE_FLAG_ARRAY . */

  if (!(t & CPL_TYPE_FLAG_ARRAY))
    switch (t & (~CPL_TYPE_FLAG_ARRAY)) {
    case CPL_TYPE_CHAR:
      return "char";
      break;
    case CPL_TYPE_UCHAR:
      return "uchar";
      break;
    case CPL_TYPE_BOOL:
      return "boolean";
      break;
    case CPL_TYPE_INT:
      return "int";
      break;
    case CPL_TYPE_UINT:
      return "uint";
      break;
    case CPL_TYPE_LONG:
      return "long";
      break;
    case CPL_TYPE_ULONG:
      return "ulong";
      break;
    case CPL_TYPE_FLOAT:
      return "float";
      break;
    case CPL_TYPE_DOUBLE:
      return "double";
      break;
    case CPL_TYPE_POINTER:
      return "pointer";
      break;
/* not in CPL3.0: case CPL_TYPE_COMPLEX:    return "complex"; break; */
    case CPL_TYPE_INVALID:
      return "invalid";
      break;
    default:
      return "unrecognized type";
    }
  else
    switch (t & (~CPL_TYPE_FLAG_ARRAY)) {
    case CPL_TYPE_CHAR:
      return "string (char array)";
      break;
    case CPL_TYPE_UCHAR:
      return "uchar array";
      break;
    case CPL_TYPE_BOOL:
      return "boolean array";
      break;
    case CPL_TYPE_INT:
      return "int array";
      break;
    case CPL_TYPE_UINT:
      return "uint array";
      break;
    case CPL_TYPE_LONG:
      return "long array";
      break;
    case CPL_TYPE_ULONG:
      return "ulong array";
      break;
    case CPL_TYPE_FLOAT:
      return "float array";
      break;
    case CPL_TYPE_DOUBLE:
      return "double array";
      break;
    case CPL_TYPE_POINTER:
      return "pointer array";
      break;
/* not in CPL3.0: case CPL_TYPE_COMPLEX:    return "complex array"; break; */
    case CPL_TYPE_INVALID:
      return "invalid (array)";
      break;
    default:
      return "unrecognized type";
    }
}

/*---------------------------------------------------------------------------*/
/**
   @brief    Read a property value from a property list
   @param    plist       Propertylist to read
   @param    keyword     Name of property to read
   @param    keywordtype Type of keyword
   @param    result      The value read

   @return   CPL_ERROR_NONE iff OK

   This function wraps  @c cpl_propertylist_get_int(),
   @c cpl_propertylist_get_bool(), @c cpl_propertylist_get_double() and
   @c cpl_propertylist_get_string().
   It checks existence and type of the requested keyword before reading
   and describes what went wrong if the property could not be read.

   @note The result is written to the variable pointed to by the
   parameter @em result. Because this is a void pointer,
   it is the responsibility of the caller to make sure that the type
    of this pointer variable corresponds to the requested @em keywordtype.
   E.g. if @em keywordtype is CPL_TYPE_BOOL, then @em result must be an bool
   pointer (bool *). If @em keywordtype isCPL_TYPE_STRING, then @em result
   must be a char **, and so on.

*/
/*---------------------------------------------------------------------------*/
cpl_error_code
sinfo_get_property_value(const cpl_propertylist * plist,
            const char *keyword,
                        cpl_type keywordtype,
            void *result)
{
  cpl_type t;

  /* Check input */
  assure (plist != NULL, CPL_ERROR_NULL_INPUT, "Null property list");
  assure (keyword != NULL, CPL_ERROR_NULL_INPUT, "Null keyword");
  /* Check for existence... */
  assure (cpl_propertylist_has (plist, keyword), CPL_ERROR_DATA_NOT_FOUND,
      "Keyword %s does not exist", keyword);
  /* ...and type of keyword */
  check(t = cpl_propertylist_get_type (plist, keyword),
         "Could not read type of keyword '%s'", keyword);
  assure (t == keywordtype, CPL_ERROR_TYPE_MISMATCH,
      "Keyword '%s' has wrong type (%s). %s expected",
      keyword, sinfo_tostring_cpl_type (t),
      sinfo_tostring_cpl_type (keywordtype));
  /* Read the keyword */
  switch (keywordtype) {
  case CPL_TYPE_INT:
    check(*((int *) result) = cpl_propertylist_get_int (plist, keyword),
           "Could not get (integer) value of %s", keyword);
    break;
  case CPL_TYPE_BOOL:
    check(*((bool *) result) = cpl_propertylist_get_bool (plist, keyword),
           "Could not get (boolean) value of %s", keyword);
    break;
  case CPL_TYPE_DOUBLE:
    check(*((double *) result) =
           cpl_propertylist_get_double (plist, keyword),
           "Could not get (double) value of %s", keyword);
    break;
  case CPL_TYPE_STRING:
    check(*((const char **) result) =
           cpl_propertylist_get_string (plist, keyword),
           "Could not get (string) value of %s", keyword);
    break;
  default:
    assure (false, CPL_ERROR_INVALID_TYPE, "Unknown type");
  }

cleanup:
  return cpl_error_get_code ();
}


/**
  @brief    find out the rectify space (slit) binning
  @param    plist       property list to read from
  @return   double      the requested value
 */
/*--------------------------------------------------------------------------- */
double sinfo_pfits_get_rectify_bin_space(cpl_propertylist * plist)
{
  double returnvalue = 0.0;

  check(sinfo_get_property_value (plist, SINFO_RECTIFY_BIN_SPACE,
    CPL_TYPE_DOUBLE, &returnvalue),
    "Error reading keyword '%s'", SINFO_RECTIFY_BIN_SPACE);

cleanup:
  return returnvalue;
}
/**
  @brief    find out the rectify SPACE max
  @param    plist       property list to read from
  @return   double      the requested value
 */
double sinfo_pfits_get_rectify_space_max(cpl_propertylist * plist)
{
  double returnvalue = 0.0;

  check (sinfo_get_property_value (plist, SINFO_RECTIFY_SPACE_MAX,
    CPL_TYPE_DOUBLE, &returnvalue),
    "Error reading keyword '%s'", SINFO_RECTIFY_SPACE_MAX);

cleanup:
  return returnvalue;
}
/**
  @brief    find out the rectify space min
  @param    plist       property list to read from
  @return   double      the requested value
 */
double sinfo_pfits_get_rectify_space_min(cpl_propertylist * plist)
{
  double returnvalue = 0.0;

  check (sinfo_get_property_value (plist, SINFO_RECTIFY_SPACE_MIN,
    CPL_TYPE_DOUBLE, &returnvalue),
    "Error reading keyword '%s'", SINFO_RECTIFY_SPACE_MIN);

cleanup:
  return returnvalue;
}
/**
  @brief    find out the rectify lambda min
  @param    plist       property list to read from
  @return   double      the requested value
 */
double sinfo_pfits_get_rectify_lambda_min(cpl_propertylist * plist)
{
  double returnvalue = 0.0;

  check (sinfo_get_property_value (plist, SINFO_RECTIFY_LAMBDA_MIN,
    CPL_TYPE_DOUBLE, &returnvalue),
    "Error reading keyword '%s'", SINFO_RECTIFY_LAMBDA_MIN);

cleanup:
  return returnvalue;
}

/**
  @brief    find out the rectify lambda max
  @param    plist       property list to read from
  @return   double      the requested value
 */
double sinfo_pfits_get_rectify_lambda_max(cpl_propertylist * plist)
{
  double returnvalue = 0.0;

  check (sinfo_get_property_value (plist, SINFO_RECTIFY_LAMBDA_MAX,
    CPL_TYPE_DOUBLE, &returnvalue),
    "Error reading keyword '%s'", SINFO_RECTIFY_LAMBDA_MAX);

cleanup:
  return returnvalue;
}


/**
  @brief    find out the rectify lambda binning
  @param    plist       property list to read from
  @return   double      the requested value
 */
/*--------------------------------------------------------------------------- */
double sinfo_pfits_get_rectify_bin_lambda(cpl_propertylist * plist)
{
  double returnvalue = 0.0;

  check(sinfo_get_property_value (plist, SINFO_RECTIFY_BIN_LAMBDA,
    CPL_TYPE_DOUBLE, &returnvalue),
    "Error reading keyword '%s'", SINFO_RECTIFY_BIN_LAMBDA);

cleanup:
  return returnvalue;
}
/*--------------------------------------------------------------------------- */
/**
 *   @brief    find out the pcatg
 *   @param    plist       property list to read from
 *   @return   pointer to statically allocated character string
 *   */
/*--------------------------------------------------------------------------- */
const char * sinfo_pfits_get_pcatg (const cpl_propertylist * plist)
{
  const char *returnvalue = "";

  check(sinfo_get_property_value
         (plist, SINFO_PCATG, CPL_TYPE_STRING, &returnvalue),
         "Error reading keyword '%s'", SINFO_PCATG);
cleanup:
  return returnvalue;
}
/*--------------------------------------------------------------------------- */
/**
 *   @brief    find out the EXTNAME
 *   @param    plist       property list to read from
 *   @return   pointer to statically allocated character string
 *   */
/*--------------------------------------------------------------------------- */
const char *
sinfo_pfits_get_extname (const cpl_propertylist * plist)
{
  const char *returnvalue = "";

  check (sinfo_get_property_value
             (plist, SINFO_EXTNAME, CPL_TYPE_STRING, &returnvalue),
             "Error reading keyword '%s'", SINFO_EXTNAME);
cleanup:
  return returnvalue;
}
/*---------------------------------------------------------------------------*/
/**
  @brief    find out the BINX value
  @param    plist property list to read from
  @return   the requested value
*/
/*---------------------------------------------------------------------------*/
int sinfo_pfits_get_binx(const cpl_propertylist * plist)
{
  int ret = 0;

  SINFO_PFITS_GET( ret, plist, SINFO_WIN_BINX, CPL_TYPE_INT);

  cleanup:
    return ret;
}

/*---------------------------------------------------------------------------*/
/**
  @brief    find out the BINY value
  @param    plist property list to read from
  @return   the requested value
*/
/*---------------------------------------------------------------------------*/
int sinfo_pfits_get_biny(const cpl_propertylist * plist)
{
  int ret = 0;

  SINFO_PFITS_GET( ret, plist, SINFO_WIN_BINY, CPL_TYPE_INT);

  cleanup:
    return ret;
}

void
sinfo_pfits_set_extname (cpl_propertylist * plist, const char *value)
{
  check(cpl_propertylist_update_string (plist, SINFO_EXTNAME, value),
         "Error writing keyword '%s'", SINFO_EXTNAME);
cleanup:
  return;
}
void sinfo_pfits_set_ctype1(cpl_propertylist * plist, const char* value)
{
  check (cpl_propertylist_update_string (plist, SINFO_CTYPE1, value),
             "Error writing keyword '%s'", SINFO_CTYPE1);
  cleanup:
    return;
}
void sinfo_pfits_set_cunit1(cpl_propertylist * plist, const char* value)
{
  check (cpl_propertylist_update_string (plist, SINFO_CUNIT1, value),
             "Error writing keyword '%s'", SINFO_CUNIT1);
  cleanup:
    return;
}


void
sinfo_pfits_set_pcatg (cpl_propertylist * plist, const char *value)
{
  cpl_propertylist_update_string (plist, SINFO_PCATG, value);

  return;
}

const char *
sinfo_tostring_cpl_frame_type (cpl_frame_type ft)
{
  switch (ft) {
  case CPL_FRAME_TYPE_NONE:
    return "NONE";
    break;
  case CPL_FRAME_TYPE_IMAGE:
    return "IMAGE";
    break;
  case CPL_FRAME_TYPE_MATRIX:
    return "MATRIX";
    break;
  case CPL_FRAME_TYPE_TABLE:
    return "TABLE";
    break;
  default:
    return "unrecognized frame type";
  }
}


int
sinfo_print_rec_status(const int val) {
   if(cpl_error_get_code() != CPL_ERROR_NONE) {
      sinfo_msg_error("Recipe status at %d",val);
      sinfo_msg_error("%s",(const char*) cpl_error_get_message());
      sinfo_msg_error("%s",(const char*) cpl_error_get_where());
      return -1;
   }
   return 0;
}

const char* sinfo_band_tostring(new_sinfo_band band){
  return 
    (band == INSTRUMENT_BAND_UVB) ? "UVB" :
    (band == INSTRUMENT_BAND_VIS) ? "VIS" :
    (band == INSTRUMENT_BAND_NIR) ? "NIR" :
    (band == INSTRUMENT_BAND_J) ? "J" :
    (band == INSTRUMENT_BAND_H) ? "H" :
    (band == INSTRUMENT_BAND_K) ? "K" : "HK";
}




cpl_frame*
sinfo_frame_product(const char* fname, const char* tag, cpl_frame_type type,
                  cpl_frame_group group,cpl_frame_level level)
{
  cpl_frame* frame=NULL;
  frame=cpl_frame_new();

  sinfo_frame_config(fname,tag,type,group,level,&frame);

  return frame;

}

void sinfo_pfits_set_bunit(cpl_propertylist * plist, const char* value)
{
  check (cpl_propertylist_update_string (plist, SINFO_BUNIT, value),
             "Error writing keyword '%s'", SINFO_BUNIT);
  cleanup:
    return;
}

double sinfo_pfits_get_airm_mean (const cpl_propertylist * plist)
{
  double airmass_start=0;
  double airmass_end=0;
 airmass_start = sinfo_pfits_get_airm_start(plist);
 airmass_end = sinfo_pfits_get_airm_end(plist);

 return 0.5*(airmass_start+airmass_end);
}



int
sinfo_pfits_get_naxis1 (const cpl_propertylist * plist)
{
  int ret = 0;

  sinfo_get_property_value( plist, SINFO_NAXIS1, CPL_TYPE_INT, &ret);

  cleanup:
    return ret;
}



int sinfo_pfits_get_naxis (const cpl_propertylist * plist)
{
  int ret = 0;

  sinfo_get_property_value( plist, SINFO_NAXIS, CPL_TYPE_INT, &ret);

  cleanup:
    return ret;
}

cpl_error_code
sinfo_pfits_set_wcs1(cpl_propertylist* header,
                   const double crpix1,
                   const double crval1,
                   const double cdelt1) {

  cpl_propertylist_append_double(header, SINFO_CRPIX1, crpix1);
  cpl_propertylist_append_double(header, SINFO_CRVAL1, crval1);
  cpl_propertylist_append_double(header, SINFO_CDELT1, cdelt1);
  cpl_propertylist_append_string(header, SINFO_CTYPE1, "LINEAR");

  return cpl_error_get_code();
}


cpl_error_code
sinfo_pfits_set_wcs2(cpl_propertylist* header,
                   const double crpix2,
                   const double crval2,
                   const double cdelt2) {

  cpl_propertylist_append_double(header, SINFO_CRPIX2, crpix2);
  cpl_propertylist_append_double(header, SINFO_CRVAL2, crval2);
  cpl_propertylist_append_double(header, SINFO_CDELT2, cdelt2);
  cpl_propertylist_append_string(header, SINFO_CTYPE2, "LINEAR");

  return cpl_error_get_code();
}

/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD1_1 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd11(cpl_propertylist * plist, double value)
{
  check(cpl_propertylist_update_double (plist, SINFO_CD11, value),
             "Error writing keyword '%s'", SINFO_CD11);
  cleanup:
    return;
}

/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD1_2 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd12(cpl_propertylist * plist, double value)
{
  check(cpl_propertylist_update_double (plist, SINFO_CD12, value),
             "Error writing keyword '%s'", SINFO_CD12);
  cleanup:
    return;
}

/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD2_1 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd21(cpl_propertylist * plist, double value)
{
  check(cpl_propertylist_update_double (plist, SINFO_CD21, value),
             "Error writing keyword '%s'", SINFO_CD21);
  cleanup:
    return;
}

/*--------------------------------------------------------------------------- */
/**
 *   @brief    Write the CD2_2 value
 *   @param    plist      Property list to write to
 *   @param    value      The value to write
 */
/*--------------------------------------------------------------------------- */
static void sinfo_pfits_set_cd22(cpl_propertylist * plist, double value)
{
  check(cpl_propertylist_update_double (plist, SINFO_CD22, value),
             "Error writing keyword '%s'", SINFO_CD22);
  cleanup:
    return;
}
static cpl_error_code
sinfo_pfits_set_cd_matrix(cpl_propertylist* header,
                        const double cdelt1,
                        const double cdelt2) {

  sinfo_pfits_set_cd11(header, cdelt1);
  sinfo_pfits_set_cd12(header, 0);
  sinfo_pfits_set_cd21(header, 0);
  sinfo_pfits_set_cd22(header, cdelt2);

  return cpl_error_get_code();
}



cpl_error_code
sinfo_pfits_set_wcs(cpl_propertylist* header, const double crpix1,
    const double crval1, const double cdelt1, const double crpix2,
    const double crval2, const double cdelt2) {

  sinfo_pfits_set_wcs1(header, crpix1, crval1, cdelt1);
  sinfo_pfits_set_wcs2(header, crpix2, crval2, cdelt2);
  sinfo_pfits_set_cd_matrix(header, cdelt1, cdelt2);

  return cpl_error_get_code();
}


/*---------------------------------------------------------------------------*/
/**
 * @brief  Set CD matrix
 * @param  plist input propertylist
 * @return updated propertylist
 */
/*---------------------------------------------------------------------------*/
cpl_error_code sinfo_set_cd_matrix2d(cpl_propertylist* plist)
{
   double cdelt1=0;
   double cdelt2=0;

   /* dummy values */
   check_nomsg(cdelt1=sinfo_pfits_get_cdelt1(plist));
   check_nomsg(cdelt2=sinfo_pfits_get_cdelt2(plist));
   check_nomsg(sinfo_pfits_set_cd11(plist,cdelt1));
   check_nomsg(sinfo_pfits_set_cd12(plist,0.));
   check_nomsg(sinfo_pfits_set_cd21(plist,0.));
   check_nomsg(sinfo_pfits_set_cd22(plist,cdelt2));

  cleanup:

   return cpl_error_get_code();

}



/*--------------------------------------------------------------------------- */
/**
  @brief    find out the BUNIT
  @param    plist       property list to read from
  @return   pointer to statically allocated character string
 */
/*--------------------------------------------------------------------------- */
const char *
sinfo_pfits_get_bunit (const cpl_propertylist * plist)
{
  const char *returnvalue = "";

  check (sinfo_get_property_value
         (plist, SINFO_BUNIT, CPL_TYPE_STRING, &returnvalue),
         "Error reading keyword '%s'", SINFO_BUNIT);

cleanup:
  return returnvalue;
}

void sinfo_pfits_set_qc_eff_fclip( cpl_propertylist * plist,
                         double value )
{
  check(cpl_propertylist_update_double(plist,SINFO_QC_EFF_FCLIP,value),
        "error writing keyword '%s'", SINFO_QC_EFF_FCLIP);
 cleanup:
  return ;
}
/*--------------------------------------------------------------------------- */
/**
  @brief    find out the DET WIN1 DIT1 value
  @param    plist       property list to read from
  @return   the requested value
 */
/*--------------------------------------------------------------------------- */
double
sinfo_pfits_get_win1_dit1 (const cpl_propertylist * plist)
{
  double returnvalue = 0;

  check (sinfo_get_property_value
         (plist, SINFO_DET_WIN1_DIT1, CPL_TYPE_DOUBLE, &returnvalue),
         "Error reading keyword '%s'", SINFO_DET_WIN1_DIT1);

cleanup:
  return returnvalue;
}
void sinfo_pfits_set_qc_eff_nclip( cpl_propertylist * plist,
                         int value )
{
  check(cpl_propertylist_update_int(plist,SINFO_QC_EFF_NCLIP,value),
        "error writing keyword '%s'", SINFO_QC_EFF_NCLIP);
 cleanup:
  return ;
}
/*--------------------------------------------------------------------------- */
/**
  @brief    find out the CONAD value
  @param    plist property list to read from
  @return   the requested value
*/
/*--------------------------------------------------------------------------- */
double sinfo_pfits_get_conad (const cpl_propertylist * plist)
{
  double ret = 0;

  SINFO_PFITS_GET( ret, plist, SINFO_CONAD, CPL_TYPE_DOUBLE);

  cleanup:
    return ret;
}

/*--------------------------------------------------------------------------*/
/**
   @brief    Computes round(x)
   @param    x  input
   @return   round(x)
*/
/*--------------------------------------------------------------------------*/
long sinfo_round_double(double x)
{
    return (x >=0) ? (long)(x+0.5) : (long)(x-0.5);
}

/*---------------------------------------------------------------------------*/
/**
 * @brief free memory associated to an 1D spectrum
 *
 * @param[in] s spectrum structure to free
*/
/*---------------------------------------------------------------------------*/
void sinfo_spectrum_free( sinfo_spectrum** s)
{
  if (s && *s){
    sinfo_free_propertylist( &((*s)->flux_header));
    sinfo_free_propertylist( &((*s)->errs_header));
    sinfo_free_propertylist( &((*s)->qual_header));
    sinfo_free_image( &((*s)->flux));
    sinfo_free_image( &((*s)->errs));
    sinfo_free_image( &((*s)->qual));
    SINFO_FREE( (*s));
  }
}

/**
@brief get RA, DEC, airmass (mean) of a frame
@param frm_sci input frame
@param ra right ascension
@param dec declination
@param airmass airmass (mean)
@return void
 */
void
sinfo_frame_sci_get_ra_dec_airmass(cpl_frame* frm_sci,
                                 double* ra,
                                 double* dec,
                                 double* airmass)
{

   const char* name_sci=NULL;
   cpl_propertylist* plist=NULL;

   name_sci=cpl_frame_get_filename(frm_sci);
   cpl_msg_warning("", "RA/DEC/Amass loadfing from %s", name_sci);
   check_nomsg(plist=cpl_propertylist_load(name_sci,0));
   *ra=sinfo_pfits_get_ra(plist);
   *dec=sinfo_pfits_get_dec(plist);
   *airmass=sinfo_pfits_get_airm_mean(plist);

cleanup:

   sinfo_free_propertylist(&plist);
   return;
}

//typedef struct _star_index_ star_index;
static const char* COL_NAME_EXTID   = "ext_id";
static const char* COL_NAME_NAME    = "name";
static const char* COL_NAME_RA  = "ra";
static const char* COL_NAME_DEC     = "dec";

static star_index* star_index_construct(const char* fits_file);
static void star_index_destruct(star_index* pindex);
// private functions

static star_index* star_index_construct(const char* fits_file)
{
    star_index* pret = cpl_malloc(sizeof(star_index));
    pret->index_size = 0;
    pret->index_table = 0;
    pret->cache_size = 0;
    pret->cache = 0;
    pret->cache_index = 0;
    if (fits_file)
    {
        size_t bt = strlen(fits_file) * sizeof(*fits_file)+1;
        pret->fits_file_name = cpl_malloc(bt);
        strcpy(pret->fits_file_name, fits_file);
    }
    else
    {
        pret->fits_file_name = 0;
    }
    return pret;
}

static void star_index_destruct(star_index* pindex)
{
    if(pindex)
    {
        if (pindex->cache)
        {
            int i = 0;
            for ( i = 0; i < pindex->cache_size; i++)
            {
                cpl_table_delete(pindex->cache[i]);
            }
            cpl_free(pindex->cache);
            pindex->cache = 0;
            pindex->cache_size = 0;
        }
        cpl_table_delete(pindex->index_table);
        if(pindex->fits_file_name)
        {
            cpl_free(pindex->fits_file_name);
        }
        cpl_free(pindex->cache_index);
        cpl_free(pindex);
    }

}
star_index* star_index_create(void)
{
    star_index* pret = star_index_construct(0);
    // initialize table
    pret->index_table = cpl_table_new(pret->index_size);
    // create columns ext_id, name, ra, dec
    check_nomsg(cpl_table_new_column(pret->index_table, COL_NAME_EXTID, CPL_TYPE_INT));
    check_nomsg(cpl_table_new_column(pret->index_table, COL_NAME_NAME, CPL_TYPE_STRING));
    check_nomsg(cpl_table_new_column(pret->index_table, COL_NAME_RA, CPL_TYPE_DOUBLE));
    check_nomsg(cpl_table_new_column(pret->index_table, COL_NAME_DEC, CPL_TYPE_DOUBLE));
    return pret;
    cleanup:
    star_index_destruct(pret);
    return 0;
}
star_index* star_index_load(const char* fits_file)
{
    star_index* pret = star_index_construct(fits_file);
    // load index table from the file
    cpl_table* pindex = 0;
    check_nomsg(pindex = cpl_table_load(fits_file,1,0));
    // TODO check the structure of the table
    pret->index_table = pindex;
    check_nomsg(pret->index_size = cpl_table_get_nrow(pindex));
    return pret;
    cleanup:
    star_index_destruct(pret);
    cpl_error_reset();
    return 0;
}
void star_index_delete(star_index* pindex)
{
    star_index_destruct(pindex);
}
int star_index_add(star_index* pindex, double RA, double DEC, const char* star_name, cpl_table* ptable)
{
    int retval = 0;
    if (pindex)
    {
        // expand the index table

        check_nomsg(cpl_table_insert_window(pindex->index_table, pindex->index_size++, 1));
        if (!pindex->cache)
        {
            pindex->cache_size = 1;
            pindex->cache = cpl_malloc(sizeof(cpl_table*) * pindex->cache_size);
            pindex->cache_index = cpl_malloc(sizeof(pindex->cache_index[0]) * pindex->cache_size);
        }
        else
        {
            // add new entry
            pindex->cache_size++;
            pindex->cache = cpl_realloc(pindex->cache, sizeof(cpl_table*) * pindex->cache_size);
        }
        check_nomsg(pindex->cache[pindex->cache_size - 1] = cpl_table_duplicate(ptable));
        // fill the index table with values
        check_nomsg(cpl_table_set_string(pindex->index_table, COL_NAME_NAME, pindex->index_size - 1 ,star_name));
        check_nomsg(cpl_table_set(pindex->index_table, COL_NAME_RA, pindex->index_size - 1 ,RA));
        check_nomsg(cpl_table_set(pindex->index_table, COL_NAME_DEC, pindex->index_size - 1,DEC));
        check_nomsg(cpl_table_set_int(pindex->index_table, COL_NAME_EXTID, pindex->index_size - 1 ,pindex->index_size + 1));
        retval = pindex->index_size;
    }
    return retval;

    cleanup:
    return 0;
}

int start_index_get_size(star_index* pindex)
{
    return pindex ? pindex->index_size : 0;
}

int star_index_remove_by_name(star_index* pindex, const char* starname)
{
    int i = 0;
    int index_pos = -1;
    for (i = 0; i < pindex->index_size; i++)
    {
        const char* curr_star_name = 0;
        check_nomsg(curr_star_name = cpl_table_get_string(pindex->index_table, COL_NAME_NAME, i));
        if (strcmp(curr_star_name, starname) == 0)
        {
            index_pos = i;
            break;
        }
    }
    if (index_pos >= 0)
    {
        // star is found
        // clear only the index table, real data would be cleaned during save operation
        cpl_table_set_int(pindex->index_table, COL_NAME_EXTID, index_pos, -1);
        if (index_pos - pindex->index_size + pindex->cache_size  >= 0)
        {
            // clear cache
            int cache_index = index_pos - pindex->index_size + pindex->cache_size;
            cpl_table_delete(pindex->cache[cache_index]);
            pindex->cache[cache_index] = 0;
        }
    }
    cleanup:
    return index_pos;
}

int star_index_save(star_index* pindex, const char* fits_file)
{
    int i  = 0;
    int inull = 0;
    cpl_table* pnew_index = 0;
    int nrows = 0;
    // firstly save the index table - deleted entries should be removed firstly
    check_nomsg(cpl_table_unselect_all(pindex->index_table));
    check_nomsg(cpl_table_or_selected_int(pindex->index_table, COL_NAME_EXTID, CPL_EQUAL_TO, -1));
    // inverse selection
    check_nomsg(cpl_table_not_selected(pindex->index_table));
    check_nomsg(pnew_index = cpl_table_extract_selected(pindex->index_table));

    nrows = cpl_table_get_nrow(pnew_index);
    for (i = 0; i < nrows; i++)
    {
        cpl_table_set_int(pnew_index, COL_NAME_EXTID, i, i+2); // ext in fits starts from 1, and another 1 is used by index_table
    }
    check_nomsg(cpl_table_save(pnew_index, NULL, NULL, fits_file, CPL_IO_CREATE));
    cpl_table_delete(pnew_index);
    pnew_index = 0;
    // save the data
    for (i = 0;i < pindex->index_size; i++)
    {
        // 2. save cache
        int saved_ext = cpl_table_get_int(pindex->index_table, COL_NAME_EXTID, i, &inull);
        if (saved_ext > 0) // check that was not removed
        {
            cpl_table* ptable = 0;
            if (i < pindex->index_size - pindex->cache_size)
            {
                check_nomsg(ptable = cpl_table_load(pindex->fits_file_name, saved_ext, 0));
            }
            else
            {
                ptable = cpl_table_duplicate(pindex->cache[i - pindex->index_size + pindex->cache_size ]);
            }
            check_nomsg(cpl_table_save(ptable, NULL, NULL, fits_file, CPL_IO_EXTEND));
            cpl_table_delete(ptable);
        }
    }
    cleanup:
    return nrows;
}
cpl_table* star_index_get(star_index* pindex, double RA, double DEC, double RA_EPS, double DEC_EPS, const char** pstar_name)
{
    int i = 0;
    cpl_table* pret = 0;
    int inull = 0;

    for (i = 0; i < pindex->index_size; i++)
    {
        double curr_ra = 0;
        double curr_dec = 0;
        int ext_id = 0;

        check_nomsg(ext_id = cpl_table_get_int(pindex->index_table, COL_NAME_EXTID, i ,&inull));
        check_nomsg(curr_ra = cpl_table_get(pindex->index_table, COL_NAME_RA, i,&inull));
        check_nomsg(curr_dec = cpl_table_get(pindex->index_table, COL_NAME_DEC, i,&inull));
        if ((ext_id > 0) && (fabs(curr_ra - RA) < RA_EPS) && (fabs(curr_dec - DEC) < DEC_EPS))
        {
        	cpl_msg_warning("", "EXETION SELECTED FOR fit POINTS : %i", ext_id);
            // found
            // retrieve the data
            if (i - pindex->index_size + pindex->cache_size  >= 0)
            {
                // data is in cache
                pret = cpl_table_duplicate(pindex->cache[i - pindex->index_size + pindex->cache_size ]);
            }
            else
            {
                // data is on disk
                pret = cpl_table_load(pindex->fits_file_name, ext_id, 0);
            }
            if (pret && pstar_name)
            {
                check_nomsg(*pstar_name = cpl_table_get_string(pindex->index_table, COL_NAME_NAME, i));
            }
            break;
        }
    }
    cleanup:
    return pret;
}

void star_index_dump(star_index* pindex, FILE* pfile)
{
    cpl_table_dump(pindex->index_table, 0,  cpl_table_get_nrow(pindex->index_table), pfile);
}


/*---------------------------------------------------------------------------*/
/**
  @brief    parse referece std stars catalog

  @param    cat        input frame catalog
  @param    dRA        Right Ascension
  @param    dDEC       Declination
  @param    EPSILON    tolerance to find ref spectra on catalog on (ra,dec)
  @param    pptable     pointer to new table
  @return   cpl error code. The table will contain interpolated data points
 */
/*---------------------------------------------------------------------------*/

cpl_error_code sinfo_parse_catalog_std_stars(cpl_frame* cat, double dRA,
    double dDEC, double EPSILON, cpl_table** pptable,sinfo_std_star_id* std_star_id) {
  const char* name = NULL;
  SINFO_ASSURE_NOT_NULL_MSG(cat, "Provide input catalog");
  if (cat) {
    check_nomsg(name=cpl_frame_get_filename(cat));
    if (name) {
      star_index* pstarindex = star_index_load(name);
      if (pstarindex) {
        const char* star_name = NULL;
        cpl_msg_warning("",
            "Searching std RA[%f] DEC[%f] with tolerance[%f] in star catalog", dRA, dDEC, EPSILON);
        *pptable = star_index_get(pstarindex, dRA, dDEC, EPSILON, EPSILON,
            &star_name);
        if(star_name != NULL) {
             if ( strcmp(star_name,"GD71")     == 0 ) *std_star_id = SINFO_GD71;
        else if ( strcmp(star_name,"Feige110") == 0 ) *std_star_id = SINFO_Feige110;
        else if ( strcmp(star_name,"GD153")    == 0 ) *std_star_id = SINFO_GD153;
        else if ( strcmp(star_name,"LTT3218")  == 0 ) *std_star_id = SINFO_LTT3218;
        else if ( strcmp(star_name,"LTT7987")  == 0 ) *std_star_id = SINFO_LTT7987;
        else if ( strcmp(star_name,"BD17")     == 0 ) *std_star_id = SINFO_BD17;
        else if ( strcmp(star_name,"EG274")    == 0 ) *std_star_id = SINFO_EG274;
        else if ( strcmp(star_name,"SYNTH")    == 0 ) *std_star_id = SINFO_SINTH;
        }
        sinfo_msg_warning("star index=%d",*std_star_id);
        sinfo_msg_warning("pptable=%p",*pptable);
        sinfo_msg_warning("star_name=%s",star_name);
        if (*pptable && star_name != NULL) {
          sinfo_msg_warning("Found STD star: %s", star_name);

        } else {
          sinfo_msg_warning(
              "ERROR - REF star %s could not be found in the catalog", star_name);
        }
      } else {
        sinfo_msg("ERROR - could not load the catalog");
      }
      star_index_delete(pstarindex);
    }
  }
  cleanup: return cpl_error_get_code();
}






/*---------------------------------------------------------------------------*/
/**
  @brief    Interpolate data points
  @param    wav       value at which is desired an interpolated value
  @param    nrow      number of data points
  @param    pw        pointer to wave array
  @param    pe        pointer to efficiency array
  @return   Interpolated data points
 */
/*---------------------------------------------------------------------------*/
double
sinfo_data_interpolate(
             double wav,
             int nrow,
             double* pw,
             double* pe
             )
{
  double y = 0;
  double w1=pw[0];
  double w2=pw[nrow-1];
  double y1_=pe[0]; /*was changed from y1 to y1_ due a warning from compiler - shadowed variable*/
  double y2=pe[nrow-1];
  if(wav < pw[0])
    {
      w1=pw[0];
      w2=pw[1];
      y1_=pe[0];
      y2=pe[1];
    }
  else if (wav > pw[nrow - 1])
    {
      w1=pw[nrow - 2];
      w2=pw[nrow - 1];
      y1_=pe[nrow - 2];
      y2=pe[nrow - 1];
    }
  else
    {
      int l = 0;
      int h = nrow - 1;
      int curr_row = 0;
      curr_row = (h-l) / 2;
      while( h-l >1 )
    {
      if(wav < pw[curr_row])
        {
          h = curr_row;
        }
      else
        {
          l = curr_row;
        }
      curr_row = (h-l) / 2 + l;
    }
      w1=pw[curr_row];
      w2=pw[curr_row + 1];
      y1_=pe[curr_row];
      y2=pe[curr_row + 1];
    }
  y=y1_+(y2-y1_)/(w2-w1)*(wav-w1);
  return y;
}

int
sinfo_column_to_double(cpl_table* ptable, const char* column)
{
  const char* TEMP = "_temp_";
  check_nomsg(cpl_table_duplicate_column(ptable, TEMP, ptable, column));
  check_nomsg(cpl_table_erase_column(ptable, column));
  check_nomsg(cpl_table_cast_column(ptable, TEMP, column, CPL_TYPE_DOUBLE));
  check_nomsg(cpl_table_erase_column(ptable, TEMP ));
  return 0;
 cleanup:
  sinfo_msg(" error column to double [%s]", column);
  return -1;
}

double*
sinfo_create_column_double(cpl_table* tbl, const char* col_name, int nrow)
{
  double* retval = 0;
  check_nomsg(cpl_table_new_column(tbl, col_name, CPL_TYPE_DOUBLE));
  check_nomsg(cpl_table_fill_column_window_double(tbl, col_name, 0, nrow, -1));
  check_nomsg(retval = cpl_table_get_data_double(tbl,col_name));
  return retval;
 cleanup:
  return retval;
}
/*---------------------------------------------------------------------------*/
/**
   @brief    Interpolate table columns
   @param    tbl        Table
   @param    wav    value at which is desired to get an interpolated value
   @param    colx   1st column name
   @param    coly   2nd column name

   @return   interpolated value if ok

*/
/*---------------------------------------------------------------------------*/
double
sinfo_table_interpolate(cpl_table* tbl,
          double wav,
          const char* colx,
          const char* coly)
{

  double y=0;
  double* pe=NULL;
  double* pw=NULL;
  int nrow=0;

  check_nomsg(pw=cpl_table_get_data_double(tbl,colx));
  check_nomsg(pe=cpl_table_get_data_double(tbl,coly));
  check_nomsg(nrow=cpl_table_get_nrow(tbl));

  y = sinfo_data_interpolate(wav, nrow, pw, pe);
 cleanup:
  return y;

}
/*---------------------------------------------------------------------------*/
/**
  @brief Compute efficiency
  @param tbl_obj_spectrum     input object spectrum
  @param tbl_atmext           input atmospheric extinction table
  @param tbl_ref              input reference flux STD table
  @param exptime              input exposure time
  @param airmass              input observed frame airmass
  @param aimprim              input airmass corrective factor
  @param gain                 input gain
  @param biny                 bin on Y (spatial) direction
  @param src2ref_wave_sampling input cnversion factor to pass from src 2 ref units
  @param col_name_atm_wave atmospheric extinction table wave column name
  @param col_name_atm_abs  atmospheric extinction table absorption column name
  @param col_name_ref_wave reference flux std table wave column name
  @param col_name_ref_flux reference flux std table flux column name
  @param col_name_ref_bin  reference flux std table bin value
  @param col_name_obj_wave observed std table wave column name
  @param col_name_obj_flux observed std table flux column name

  @return   table with computed efficiency
 */
/*---------------------------------------------------------------------------*/

cpl_table*
sinfo_utils_efficiency_internal(cpl_table* tbl_obj_spectrum,
                  cpl_table* tbl_atmext,
                  cpl_table* tbl_ref,
                  double exptime,
                  double airmass,
                  double aimprim,
                  double gain,
                  int    biny,
                              double src2ref_wave_sampling,
                              const char* col_name_atm_wave,
                              const char* col_name_atm_abs,
                              const char* col_name_ref_wave,
                              const char* col_name_ref_flux,
                              const char* col_name_ref_bin,
                              const char* col_name_obj_wave,
                              const char* col_name_obj_flux,
                              int* ntot,
                              int* nclip
                  )
{

  const double TEL_AREA     = 51.2e4; /* collecting area in cm2 */
  double cdelta1 = 0;
  int i = 0;
  cpl_table* tbl_sel = NULL;

  /* structure of the input table
   * col            type    description
   * wavelength double
   * flux           double
   * */
  cpl_table* tbl_result = NULL; // output table

  /*
   * structure of the output table
   * col            type    description
   * wavelength double
   * EFF            double  efficiency in range 0-1
   * */

  double* pref = NULL;
  double* pext = NULL;
  double* pcor = NULL;
  double* peph = NULL;

  double* pw = NULL; // wavelength of the input table
  int nrow = 0;
  double ref_bin_size=0;
  double um2nm=0.001;
  double eff_med=0.;
  double eff_rms=0.;
  double eff_thresh=0.;

  double kappa=5;
  double nm2AA=10.;
  int nsel=0;
  double eff=0;
  nrow = cpl_table_get_nrow(tbl_obj_spectrum);
  /*
  sinfo_msg_dbg_medium("Starting efficiency calculation: exptime[%f] airmass[%f] nrow[%d]",
      exptime, airmass, nrow);
  */
  //cpl_table_dump(tbl_obj_spectrum,0,3,stdout);
  //sinfo_msg("col wave=%s col_flux=%s",col_name_obj_wave,col_name_obj_flux);

  /* convert to double */
  sinfo_column_to_double(tbl_obj_spectrum,col_name_obj_wave);
  sinfo_column_to_double(tbl_obj_spectrum,col_name_obj_flux);

  check_nomsg(sinfo_column_to_double(tbl_atmext,col_name_atm_wave ));
  check_nomsg(sinfo_column_to_double(tbl_atmext,col_name_atm_abs ));
  check_nomsg(sinfo_column_to_double(tbl_ref,col_name_ref_wave ));
  check_nomsg(sinfo_column_to_double(tbl_ref,col_name_ref_flux ));
  check_nomsg(sinfo_column_to_double(tbl_ref,col_name_ref_bin ));

  /* as the reference spectra are erg cm-2 s-1 AA-1 and the wave scale
     have been converted from AA to nm to be uniform to XSH scale
     we apply a corresponding factor to the flux */
  //check_nomsg(cpl_table_multiply_scalar(tbl_ref,col_name_ref_flux,nm2AA));

  /* get bin size of ref STD star spectrum */
  ref_bin_size=cpl_table_get_double(tbl_ref,col_name_ref_bin,0,NULL);
  //sinfo_msg_dbg_medium("ref_bin_size[nm]=%g",ref_bin_size);

  /*
  sinfo_msg("Unit conversion factor src/ref STD spectrum=%g",
          src2ref_wave_sampling);
  */

  /* convert obj spectrum wave unit to the same of the reference one
     This is required to be able to obtain the interpolated value of
     the extinction and reference std star spectrum at the sampled
     object wavelength when we compute the correction term due to
     atmospheric extinction
    */
  check_nomsg(cpl_table_multiply_scalar(tbl_obj_spectrum,col_name_obj_wave,
                                 src2ref_wave_sampling));


  /* Computes the atmospheric distorsion correction:
     COL_NAME_REF: reference std star flux value
     COL_NAME_EXT: atm extinction
     COL_NAME_EPHOT: photon energy
     COL_NAME_COR: corrective factor (due to atmospheric extinction)
  */
  //sinfo_msg_dbg_medium("object 2 src std  wave factor %g",src2ref_wave_sampling);
  check_nomsg(pw=cpl_table_get_data_double(tbl_obj_spectrum,col_name_obj_wave));

  // prepare columns for the output data
  check_nomsg(tbl_result=cpl_table_new(nrow));
  check_nomsg(pref=sinfo_create_column_double(tbl_result, COL_NAME_REF, nrow));
  check_nomsg(pext=sinfo_create_column_double(tbl_result, COL_NAME_EXT, nrow));
  check_nomsg(pcor=sinfo_create_column_double(tbl_result, COL_NAME_COR, nrow));
  check_nomsg(peph=sinfo_create_column_double(tbl_result, COL_NAME_EPHOT, nrow));
  //sinfo_msg_dbg_medium("wave range: [%g,%g] nm",pw[0],pw[nrow-1]);
  //sinfo_msg_dbg_medium("src_bin_size[nm]=%g",pw[1] - pw[0]);

  cdelta1 = (pw[1] - pw[0]) / src2ref_wave_sampling ; /* we want the delta in original units. As we rescaled to AA we need to correct for this */
  //sinfo_msg_dbg_medium("nrow=%d cdelta1=%g",nrow,cdelta1);
  for (i = 0; i < nrow; i++)
    {
      check_nomsg(pext[i] = sinfo_table_interpolate(tbl_atmext, pw[i],col_name_atm_wave, col_name_atm_abs));
      check_nomsg(pref[i] = sinfo_table_interpolate(tbl_ref, pw[i], col_name_ref_wave,col_name_ref_flux));
      pcor[i] = pow(10,(0.4*pext[i] * (aimprim - airmass)));
      /*formula expects pw to be in um, but it is in nm. So we need to convert it*/
      eff = 1.e7*1.986e-19/(pw[i]*um2nm);
      if(!isnan(eff)) {
         peph[i]=eff;
      } else {
         /*
         sinfo_msg("pw=%g pext=%g pref=%g pcor=%g pef=%g",
                      pw[i],pext[i],pref[i],pcor[i],peph[i]);
         */
         cpl_table_set_invalid(tbl_result,COL_NAME_EPHOT,i);
      }
      /* ph energy: 1.986*10^-19(J.ph^-1)/lam(um) ==>
         in as pw is expressed in nm units we need to multiply by um2nm: 10-3
       */
      /*
      if(i< 2) {
         sinfo_msg("pw[i]=%g,pcor=%g peph=%g",pw[i],pcor[i],peph[i]);
      }
      */
    }
  cpl_table_erase_invalid(tbl_result);

  /*
  sinfo_msg("atm: %s, %s ref: %s, %s obj: %s, %s",
          col_name_atm_wave,col_name_atm_abs,
          col_name_ref_wave,col_name_ref_flux,
          col_name_obj_wave,col_name_obj_flux);
  */
  /* add in result table also ORDER, OBJ(flux), WAVELENGTH columns */
  check_nomsg(cpl_table_duplicate_column(tbl_result,"ORDER",
                   tbl_obj_spectrum,COL_NAME_ORD_OBJ ));
  check_nomsg(cpl_table_duplicate_column(tbl_result,COL_NAME_SRC_COR,
                   tbl_obj_spectrum, col_name_obj_flux));
  check_nomsg(cpl_table_duplicate_column(tbl_result,col_name_obj_wave,
                   tbl_obj_spectrum,col_name_obj_wave));

  /* correct for atmospheric extintion:
     bring the observed STD star out of the atmosphere */
  check_nomsg(cpl_table_multiply_columns(tbl_result,COL_NAME_SRC_COR,COL_NAME_COR));

  /* correct object flux by binning:
     As the reference std star spectra flux values are in units of
     erg/cm2/s/A we have to obtain the bin size in Angstrom units.
     Because the sampling step is in nm units, we need to apply a nm2AA factor:
     cdelt1[src_sampling]*src2ref_wave_sampling*nm2AA */
  cpl_table_divide_scalar(tbl_result, COL_NAME_SRC_COR, src2ref_wave_sampling);
  cpl_table_divide_scalar(tbl_result, COL_NAME_SRC_COR, cdelta1);
  cpl_table_divide_scalar(tbl_result, COL_NAME_SRC_COR, nm2AA);
  /* correct for spatial bin size: Why?? */
  cpl_table_divide_scalar(tbl_result,COL_NAME_SRC_COR,biny);


  /*correct ref std star flux by binning:
    divides by the bin size in ref_wave_sampling (usually AA) */
  check_nomsg(cpl_table_divide_scalar(tbl_result,COL_NAME_REF,ref_bin_size));

  /* create efficiency column, initialized with the intensity value (ADU)
     corrected for extinction and sampling step */
  check_nomsg(cpl_table_duplicate_column(tbl_result,COL_NAME_SRC_EFF,
                   tbl_result,COL_NAME_SRC_COR));

  /* We need now to convert ADU to erg/cm2/s :
   correct for detector gain, exposure time, telescope area: we are now
   in units of electrons/s/cm2 */
  check_nomsg(cpl_table_multiply_scalar(tbl_result,COL_NAME_SRC_EFF,
                  gain / (exptime * TEL_AREA)));

  /* To pass from electron to ergs we correct for photon energy */
  check_nomsg(cpl_table_multiply_columns(tbl_result,COL_NAME_SRC_EFF,
                   COL_NAME_EPHOT));

  /* our observed STD star value is now finally in the same units
     as the one of the reference STD star: the efficiency is the ratio
     observed_flux/reference_flux */

  check_nomsg(cpl_table_divide_columns(tbl_result,COL_NAME_SRC_EFF,COL_NAME_REF));
  /* apply factor UVES_flux_factor (1.e16) as reference catalog has fluxes
     in units of UVES_flux_factor (1e-16) */



  /* To have cleaner plots we clean from outliers */

  *ntot=cpl_table_get_nrow(tbl_result);
  *nclip = 0;
  cpl_boolean cleanup = CPL_FALSE;
  if(cleanup){
    eff_med=cpl_table_get_column_median(tbl_result,COL_NAME_SRC_EFF);
    eff_rms=cpl_table_get_column_stdev(tbl_result,COL_NAME_SRC_EFF);

    eff_thresh=(eff_med+kappa*eff_rms<10.) ? eff_med+kappa*eff_rms:10.;
    if(irplib_isinf(eff_thresh)) eff_thresh=10.;

    cpl_table_and_selected_double(tbl_result,COL_NAME_SRC_EFF,
                  CPL_GREATER_THAN,1.e-5);

    cpl_table_and_selected_double(tbl_result,COL_NAME_SRC_EFF,
                  CPL_LESS_THAN,eff_thresh);

    eff_med=cpl_table_get_column_median(tbl_result,COL_NAME_SRC_EFF);
    eff_rms=cpl_table_get_column_stdev(tbl_result,COL_NAME_SRC_EFF);

    eff_thresh=(eff_med+kappa*eff_rms<10.) ? eff_med+kappa*eff_rms:10.;
    if(irplib_isinf(eff_thresh)) eff_thresh=10.;

    cpl_table_and_selected_double(tbl_result,COL_NAME_SRC_EFF,
                  CPL_LESS_THAN,eff_thresh);

    nsel=cpl_table_and_selected_double(tbl_result,COL_NAME_SRC_EFF,
                                        CPL_LESS_THAN,1);

    *nclip=(*ntot)-nsel;
    tbl_sel=cpl_table_extract_selected(tbl_result);
  } else {
     tbl_sel=cpl_table_duplicate(tbl_result);
  }

  cleanup:
  sinfo_free_table(&tbl_result);
  return tbl_sel;
}


HIGH_ABS_REGION *
sinfo_fill_high_abs_regions(
   new_sinfo_band band,
   cpl_frame* high_abs_frame)
{
   HIGH_ABS_REGION * phigh=NULL;
   int nrow=0;
   double* pwmin=0;
   double* pwmax=0;
   int i=0;
   cpl_table* high_abs_tab=NULL;
   //XSH_ARM the_arm;

  if(high_abs_frame !=NULL) {
     high_abs_tab=cpl_table_load(cpl_frame_get_filename(high_abs_frame),1,0);
  }
  //the_arm=xsh_instrument_get_arm(instrument);



   if(high_abs_tab!=NULL) {
      nrow=cpl_table_get_nrow(high_abs_tab);
      check_nomsg(pwmin=cpl_table_get_data_double(high_abs_tab,"LAMBDA_MIN"));
      check_nomsg(pwmax=cpl_table_get_data_double(high_abs_tab,"LAMBDA_MAX"));

      phigh = (HIGH_ABS_REGION *) cpl_calloc(nrow + 1, sizeof(HIGH_ABS_REGION));
      for(i=0;i<nrow;i++) {
         phigh[i].lambda_min=pwmin[i];
         phigh[i].lambda_max=pwmax[i];
      }
      phigh[nrow].lambda_min=0;
      phigh[nrow].lambda_max=0;

   } else {

      if (band == INSTRUMENT_BAND_UVB) {
          phigh = UvbHighAbsRegions;
      } else if (band == INSTRUMENT_BAND_VIS) {
          phigh = VisHighAbsRegions;
      } else if (band == INSTRUMENT_BAND_NIR) {
          phigh = NirHighAbsRegions;
      } else if (band == INSTRUMENT_BAND_ALL) {
             phigh = AllHighAbsRegions;
      } else if (band == INSTRUMENT_BAND_J) {
          phigh = NirJHighAbsRegions;
      } else if (band == INSTRUMENT_BAND_H) {
          phigh = NirHHighAbsRegions;
      } else if (band == INSTRUMENT_BAND_K) {
          phigh = NirKHighAbsRegions;

      } else {
        phigh = NirHKHighAbsRegions;
      }


   }
  cleanup:
   sinfo_free_table(&high_abs_tab);
   return phigh;
}
/*---------------------------------------------------------------------------*/
/**
 * @brief save a spectrum
 *
 * @param[in] s spectrum structure to save
 * @param[in] filename name of the save file
 * @param[in] tag spectrum pro catg
 *
 * @return 1D spectrum frame
 */
/*---------------------------------------------------------------------------*/
cpl_frame* sinfo_spectrum_save( sinfo_spectrum* s, const char* filename,
                  const char* tag)
{
  cpl_frame *product_frame = NULL;

  SINFO_ASSURE_NOT_NULL(s);
  SINFO_ASSURE_NOT_NULL(filename);

  check_nomsg( sinfo_pfits_set_extname(s->flux_header , "FLUX"));
  check_nomsg(sinfo_plist_set_extra_keys(s->flux_header,"IMAGE","DATA","RMSE",
                                 "FLUX","ERRS","QUAL",0));

  check_nomsg( sinfo_pfits_set_extname(s->errs_header , "ERRS"));
  check_nomsg(sinfo_plist_set_extra_keys(s->errs_header,"IMAGE","DATA","RMSE",
                                 "FLUX","ERRS","QUAL",1));

  check_nomsg( sinfo_pfits_set_extname(s->qual_header , "QUAL"));
  check_nomsg(sinfo_plist_set_extra_keys(s->qual_header,"IMAGE","DATA","RMSE",
                                 "FLUX","ERRS","QUAL",2));

  /* Save the file */
   if ( s->size_slit > 1){

    double crval1=0;
    double crpix1=0;
    double cdelt1=0;

    double crval2=0;
    double crpix2=0;
    double cdelt2=0;

    crval1=sinfo_pfits_get_crval1(s->flux_header);
    crpix1=sinfo_pfits_get_crpix1(s->flux_header);
    cdelt1=sinfo_pfits_get_cdelt1(s->flux_header);

    crval2=sinfo_pfits_get_crval2(s->flux_header);
    crpix2=sinfo_pfits_get_crpix2(s->flux_header);
    cdelt2=sinfo_pfits_get_cdelt2(s->flux_header);

    sinfo_pfits_set_wcs(s->errs_header,crpix1,crval1,cdelt1,crpix2,crval2,cdelt2);
    sinfo_pfits_set_wcs(s->qual_header,crpix1,crval1,cdelt1,crpix2,crval2,cdelt2);

    check_nomsg( sinfo_pfits_set_pcatg( s->flux_header, tag));
    check(cpl_image_save ( s->flux, filename, SINFO_SPECTRUM_DATA_BPP,
      s->flux_header, CPL_IO_DEFAULT),
      "Could not save data to %s extension 0", filename);
    check(cpl_image_save ( s->errs, filename, SINFO_SPECTRUM_ERRS_BPP,
      s->errs_header, CPL_IO_EXTEND),
      "Could not save errs to %s extension 1", filename);
    check(cpl_image_save ( s->qual, filename, SINFO_SPECTRUM_QUAL_BPP,
      s->qual_header, CPL_IO_EXTEND),
      "Could not save qual to %s extension 2", filename);
  }
  else{
    cpl_vector *flux1D = NULL;
    cpl_vector *err1D = NULL;
    cpl_vector *qual1D = NULL;

    double crval1=0;
    double crpix1=0;
    double cdelt1=0;

    crval1=sinfo_pfits_get_crval1(s->flux_header);
    crpix1=sinfo_pfits_get_crpix1(s->flux_header);
    cdelt1=sinfo_pfits_get_cdelt1(s->flux_header);



    sinfo_pfits_set_ctype1(s->flux_header,"LINEAR");
    sinfo_pfits_set_cunit1(s->flux_header,"nm");
    cpl_propertylist_erase_regexp(s->flux_header, "^CTYPE2", 0);



    check_nomsg(sinfo_pfits_set_wcs1(s->errs_header, crpix1, crval1, cdelt1));
    sinfo_pfits_set_cunit1(s->errs_header,"nm");

    check_nomsg(sinfo_pfits_set_wcs1(s->qual_header, crpix1, crval1, cdelt1));
    sinfo_pfits_set_cunit1(s->qual_header,"nm");
    sinfo_pfits_set_bunit(s->qual_header,SINFO_BUNIT_NONE_C);

    check_nomsg( flux1D = cpl_vector_new_from_image_row( s->flux, 1));
    check_nomsg( err1D = cpl_vector_new_from_image_row( s->errs, 1));
    check_nomsg( qual1D = cpl_vector_new_from_image_row( s->qual, 1));
    check_nomsg( cpl_vector_save( flux1D, filename, SINFO_SPECTRUM_DATA_BPP,
      s->flux_header, CPL_IO_DEFAULT));
    check_nomsg( cpl_vector_save( err1D, filename, SINFO_SPECTRUM_ERRS_BPP,
      s->errs_header, CPL_IO_EXTEND));
    check_nomsg( cpl_vector_save( qual1D, filename, SINFO_SPECTRUM_QUAL_BPP,
      s->qual_header, CPL_IO_EXTEND));
    cpl_vector_delete( flux1D);
    cpl_vector_delete( err1D);
    cpl_vector_delete( qual1D);
  }


  check_nomsg( product_frame = cpl_frame_new() ) ;
  check_nomsg( cpl_frame_set_filename( product_frame,filename ));
  check_nomsg( cpl_frame_set_type( product_frame, CPL_FRAME_TYPE_IMAGE )) ;
  check_nomsg( cpl_frame_set_level( product_frame, CPL_FRAME_LEVEL_FINAL )) ;
  check_nomsg( cpl_frame_set_group( product_frame, CPL_FRAME_GROUP_PRODUCT ));

  cleanup:
    if (cpl_error_get_code () != CPL_ERROR_NONE) {
      sinfo_free_frame(&product_frame);
      product_frame = NULL;
    }
    return product_frame;
}

/*---------------------------------------------------------------------------*/
/**
   @brief    Normalize a spectrum
   @param    spectrum           The 1d (merged) or 2d (non-merged or 2d
                                extracted+merged) spectrum to be normalized
   @param    spectrum_error     Error (1 sigma) of @em spectrum, or NULL.
   @param    spectrum_header    Header describing the geometry of the input spectrum
   @param    binx               x bin
   @param    gain               detector's gain
   @param    exptime            observed object's exposure time
   @param    airmass            observed object's airmass
   @param    n_traces           Number of spatial bins (1 unless 2d extracted)
   @param    atm_extinction     The table of extinction coefficients
   @param    scaled_error       (output) If non-NULL, error of output spectrum
   @return   The normalized spectrum

   The spectrum is divided by exposure time, gain and (optionally) binning.
   Also corrected for atmospheric extinction using the provided table of
   extinction coefficients.

   Bad pixels are propagated.sinfo_utils.h
*/
/* Needs to be updated for multi extension (ORDER-BY-ORDER) */
/*----------------------------------------------------------------------------*/
static cpl_image *
sinfo_normalize_spectrum_image(const cpl_image *spectrum,
                             const cpl_image *spectrum_error,
                             const cpl_propertylist *spectrum_header,
                             const int bin_size,
                             const double gain,
                             const double exptime,
                             const double airmass,
                             const int n_traces,
                             const cpl_table *atm_extinction,
                             cpl_image **scaled_error)
{
    cpl_image *scaled = NULL;
    int norders, ny, nx;
    double cor_fct=gain*exptime*bin_size;
    SINFO_ASSURE_NOT_NULL_MSG(spectrum,"Null input spectrum");
    SINFO_ASSURE_NOT_NULL_MSG(scaled_error,"Null input scaled error");
    SINFO_ASSURE_NOT_NULL_MSG(spectrum_error, "Null input spectrum error");
    SINFO_ASSURE_NOT_NULL_MSG(spectrum_header,"Null input spectrum header");
    SINFO_ASSURE_NOT_NULL_MSG(atm_extinction,"Null input atmospheric extinction table");

    nx = cpl_image_get_size_x(spectrum);
    ny = cpl_image_get_size_y(spectrum);


    if (spectrum_error != NULL)
    {
        assure( nx == cpl_image_get_size_x(spectrum_error) &&
            ny == cpl_image_get_size_y(spectrum_error), CPL_ERROR_INCOMPATIBLE_INPUT,
            "Error spectrum geometry differs from spectrum: %" CPL_SIZE_FORMAT "x%" CPL_SIZE_FORMAT " vs. %dx%d",
            cpl_image_get_size_x(spectrum_error),
            cpl_image_get_size_y(spectrum_error),
            nx, ny);
    }

    assure( ny % n_traces == 0, CPL_ERROR_INCOMPATIBLE_INPUT,
        "Spectrum image height (%d) is not a multiple of "
        "the number of traces (%d). Confused, bailing out",
        ny, n_traces);

    norders = ny / n_traces;

    /*
     * Correct for exposure time, gain, bin
     */
    assure( exptime > 0, CPL_ERROR_ILLEGAL_INPUT, "Non-positive exposure time: %f s", exptime);
    assure( gain    > 0, CPL_ERROR_ILLEGAL_INPUT, "Non-positive gain: %f", gain);
    assure( bin_size    > 0, CPL_ERROR_ILLEGAL_INPUT, "Illegal binning: %d", bin_size);

    //sinfo_msg_dbg_medium("Correcting for exposure time = %f s, gain = %f, bin_size = %d", exptime, gain, bin_size);

    check(scaled=cpl_image_divide_scalar_create(spectrum,cor_fct),
       "Error correcting spectrum for gain, exposure time, binning");

    if (scaled_error != NULL)
    {
      check( *scaled_error=cpl_image_divide_scalar_create(spectrum_error,
                                  cor_fct),
           "Error correcting rebinned spectrum for gain, exposure time, binning");
    }

    /*
     * Correct for atmospheric extinction
     */
    {
    double dlambda, lambda_start;
    int order;

    //sinfo_msg_dbg_medium("Correcting for extinction through airmass %f", airmass);
    check( dlambda = sinfo_pfits_get_cdelt1(spectrum_header),
           "Error reading bin width from header");

    for (order = 1; order <= norders; order++)
        {
        int trace;

        /* If spectrum was already merged, then read crval1,
         * otherwise read wstart for each order
         */

        if (norders == 1)
            {
            check( lambda_start = sinfo_pfits_get_crval1(spectrum_header),
                   "Error reading start wavelength from header");
            }
        else
            {
            /* Here need to be updated for multi extension (ORDER-BY-ORDER) */
            check( lambda_start =  sinfo_pfits_get_crval1(spectrum_header),
                   "Error reading start wavelength from header");
            }

        for (trace = 1; trace <= n_traces; trace++)
            {
            int spectrum_row = (order - 1)*n_traces + trace;
            int x;

            for (x = 1; x <= nx; x++)
                {
                int pis_rejected1;
                int pis_rejected2;
                double flux;
                double dflux = 0;
                double extinction;
                double lambda;

                lambda = lambda_start + (x-1) * dlambda;

                flux  = cpl_image_get(scaled, x, spectrum_row, &pis_rejected1);
                if (scaled_error != NULL)
                    {
                    dflux = cpl_image_get(*scaled_error, x,
                                  spectrum_row, &pis_rejected2);
                    }

                if (!pis_rejected1 && (scaled_error == NULL || !pis_rejected2))
                    {
                       int istart = 0;

                       /* Read extinction (units: magnitude per airmass) */
                    check( extinction =
                           sinfo_spline_hermite_table(
                           lambda, atm_extinction,
                           "D_LAMBDA", "D_EXTINCTION", &istart),
                           "Error interpolating extinction coefficient");

                    /* Correct for extinction using
                     * the magnitude/flux relation
                     * m = -2.5 log_10 F
                     *  =>
                     * F = 10^(-m*0.4)
                     *
                     * m_top-of-atmosphere = m - ext.coeff*airmass
                     * F_top-of-atmosphere = F * 10^(0.4 * ext.coeff*airmass)
                     */

                    cpl_image_set(
                        scaled, x, spectrum_row,
                        flux * pow(10, 0.4 * extinction * airmass));
                    if (scaled_error != NULL)
                        {
                        cpl_image_set(
                            *scaled_error, x, spectrum_row,
                            dflux * pow(10, 0.4 * extinction * airmass));
                        }
                    }
                else
                    {
                    cpl_image_reject(scaled, x, spectrum_row);
                    if (scaled_error != NULL)
                        {
                        cpl_image_reject(*scaled_error, x, spectrum_row);
                        }
                    }
                } /* for each x */

            } /* for each (possibly only 1) trace */

        } /* for each (possibly only 1) order */
    }

  cleanup:
    if (cpl_error_get_code() != CPL_ERROR_NONE)
    {
        sinfo_free_image(&scaled);
        if (scaled_error != NULL)
        {
            sinfo_free_image(scaled_error);
        }
    }

    return scaled;
}



/*---------------------------------------------------------------------------*/
/**
   @brief    Normalize spectrum slice
   @param    name_s  spectrum filename
   @param    tag_o  output frame pro catg
   @param    ext     extension value
   @param    binx    X bin size
   @param    gain    detector's gain
   @param    exptime exposure time
   @param    airmass airmass
   @param    tbl_atm_ext atmospheric extinction table
   @return   CPL error code

   The spectrum is divided by exposure time, gain and (optionally) binning.
   Also corrected for atmospheric extinction using the provided table of
   extinction coefficients.

   Bad pixels are propagated.sinfo_utils.h
*/
cpl_error_code
sinfo_normalize_spectrum_image_slice(const char* name_s,
                   const char* tag_o,
                   const int ext,
                   const int bin_size,
                   const double gain,
                   const double exptime,
                   const double airmass,
                   const cpl_table* tbl_atm_ext)
{


  cpl_image* data_ima=NULL;
  cpl_image* errs_ima=NULL;
  cpl_image* qual_ima=NULL;
  cpl_image* data_nrm=NULL;
  cpl_image* errs_nrm=NULL;

  cpl_image* data_tmp=NULL;
  cpl_image* errs_tmp=NULL;

  cpl_image* data_nrm_tmp=NULL;
  cpl_image* errs_nrm_tmp=NULL;

  cpl_vector* data_vec=NULL;
  cpl_vector* errs_vec=NULL;
  cpl_vector* qual_vec=NULL;

  cpl_propertylist* hdat=NULL;
  cpl_propertylist* herr=NULL;
  cpl_propertylist* hqua=NULL;
  int naxis=0;
  char name_o[256];

  sprintf(name_o,"%s.fits",tag_o);

  sinfo_free_propertylist(&hdat);
  sinfo_free_propertylist(&herr);
  sinfo_free_propertylist(&hqua);

  hdat=cpl_propertylist_load(name_s,ext+0);
  herr=cpl_propertylist_load(name_s,ext+1);
  hqua=cpl_propertylist_load(name_s,ext+2);


  naxis=sinfo_pfits_get_naxis(hdat);
  sinfo_pfits_set_pcatg(hdat,tag_o);
  if(naxis == 1) {

    check_nomsg(data_vec=cpl_vector_load(name_s,ext+0));
    check_nomsg(errs_vec=cpl_vector_load(name_s,ext+1));
    sinfo_free_image(&data_ima);
    sinfo_free_image(&errs_ima);
    check_nomsg(data_ima=sinfo_vector_to_image(data_vec,CPL_TYPE_FLOAT));
    check_nomsg(errs_ima=sinfo_vector_to_image(errs_vec,CPL_TYPE_FLOAT));
    cpl_vector_delete(data_vec);
    cpl_vector_delete(errs_vec);

  } else {

    sinfo_free_image(&data_ima);
    sinfo_free_image(&errs_ima);
    sinfo_free_image(&qual_ima);
    check_nomsg(data_ima=cpl_image_load(name_s,CPL_TYPE_FLOAT,0,ext+0));
    check_nomsg(errs_ima=cpl_image_load(name_s,CPL_TYPE_FLOAT,0,ext+1));
    check_nomsg(qual_ima=cpl_image_load(name_s,CPL_TYPE_INT,0,ext+2));

  }
  sinfo_free_image(&data_tmp);
  sinfo_free_image(&errs_tmp);
  data_tmp=cpl_image_cast(data_ima,CPL_TYPE_DOUBLE);
  errs_tmp=cpl_image_cast(errs_ima,CPL_TYPE_DOUBLE);

  sinfo_free_image(&data_nrm_tmp);
  sinfo_free_image(&errs_nrm_tmp);

  check_nomsg(data_nrm_tmp=sinfo_normalize_spectrum_image(data_tmp,errs_tmp,hdat,
                          bin_size,gain,exptime,airmass,1,
                          tbl_atm_ext,&errs_nrm_tmp));

  sinfo_free_image(&data_nrm);
  sinfo_free_image(&errs_nrm);
  check_nomsg(data_nrm=cpl_image_cast(data_nrm_tmp,CPL_TYPE_FLOAT));
  check_nomsg(errs_nrm=cpl_image_cast(errs_nrm_tmp,CPL_TYPE_FLOAT));

  if(naxis==1) {
    if(data_vec != NULL) cpl_vector_delete(data_vec);
    if(errs_vec != NULL) cpl_vector_delete(errs_vec);

    check_nomsg(data_vec=sinfo_image_to_vector(data_nrm));
    check_nomsg(errs_vec=sinfo_image_to_vector(errs_nrm));

    if(ext==0) {
      check_nomsg(cpl_vector_save(data_vec,name_o,CPL_TYPE_FLOAT,hdat,CPL_IO_DEFAULT));
    } else {
      check_nomsg(cpl_vector_save(data_vec,name_o,CPL_TYPE_FLOAT,hdat,CPL_IO_EXTEND));
    }
    check_nomsg(cpl_vector_save(errs_vec,name_o,CPL_TYPE_FLOAT,herr,CPL_IO_EXTEND));
    cpl_vector_delete(qual_vec);
    check_nomsg(qual_vec=cpl_vector_load(name_s,ext+2));
    check_nomsg(cpl_vector_save(qual_vec,name_o,CPL_TYPE_FLOAT,hqua,CPL_IO_EXTEND));


  } else {
    if(ext==0) {
      check_nomsg(cpl_image_save(data_nrm,name_o,CPL_TYPE_FLOAT,hdat,CPL_IO_DEFAULT));
    } else {
      check_nomsg(cpl_image_save(data_nrm,name_o,CPL_TYPE_FLOAT,hdat,CPL_IO_EXTEND));
    }
    check_nomsg(cpl_image_save(errs_nrm,name_o,CPL_TYPE_FLOAT,herr,CPL_IO_EXTEND));
    check_nomsg(cpl_image_save(qual_ima,name_o,CPL_TYPE_INT,hqua,CPL_IO_EXTEND));

  }
  /* the qual ext is treated separately: we duplicate it to product */


 cleanup:
  sinfo_free_image(&data_ima);
  sinfo_free_image(&errs_ima);
  sinfo_free_image(&qual_ima);
  sinfo_free_image(&data_nrm);
  sinfo_free_image(&errs_nrm);
  sinfo_free_image(&data_tmp);
  sinfo_free_image(&errs_tmp);
  sinfo_free_image(&data_nrm_tmp);
  sinfo_free_image(&errs_nrm_tmp);

  sinfo_free_propertylist(&hdat);
  sinfo_free_propertylist(&herr);
  sinfo_free_propertylist(&hqua);

  cpl_vector_delete(data_vec);
  cpl_vector_delete(errs_vec);
  cpl_vector_delete(qual_vec);

  return cpl_error_get_code();
}


/*--------------------------------------------------------------------------- */
/**
  @brief    find out the exposure time
  @param    plist       property list to read from
  @return   Exposure time
 */
/*--------------------------------------------------------------------------- */
double sinfo_pfits_getexptime (const cpl_propertylist * plist)
{
  double returnvalue = 0.0;

  check(sinfo_get_property_value (plist, SINFO_EXPTIME,CPL_TYPE_DOUBLE,
              &returnvalue),
         "Error reading keyword '%s'", SINFO_EXPTIME);

cleanup:
  return returnvalue;
}


/**
 * Return the number of slit from a sinfo_rec_list structure.
 *
 * @param list Rectify structure pointer
 * @param idx Index in the list
 *
 * @return Nb of slit
 */
int sinfo_rec_list_get_nslit( sinfo_rec_list* list, int idx )
{
  int res = 0 ;

  SINFO_ASSURE_NOT_NULL(list);
  res = list->list[idx].nslit ;

 cleanup:
  return res ;
}

/*---------------------------------------------------------------------------*/
/**
   @brief    Normalize a spectrum
   @param    obj_frame           The 1d (merged) or 2d (non-merged or 2d extracted+merged) spectrum to be normalized
   @param    atm_ext_frame     The frame with atmospheric extinction spectrum
   @param    correct_binning    Flag indicating whether or not to divide by the x-binning factor
   @param    instrument         instrument arm
   @param    tag_o              output product tag
   @return   The normalized spectrum

   The spectrum is divided by exposure time, gain and (optionally) binning.
   Also corrected for atmospheric extinction using the provided table of
   extinction coefficients.

   Bad pixels are propagated.sinfo_utils.h
*/
/* Needs to be updated for multi extension (ORDER-BY-ORDER) */
cpl_frame *
sinfo_normalize_spectrum_ord(const cpl_frame *obj_frame,
               const cpl_frame *atm_ext_frame,
               cpl_boolean correct_binning,
               new_sinfo_band band,
               const char* tag_o)
{

  cpl_frame* result=NULL;
  const char* name_s=NULL;
  const char* aname=NULL;

  cpl_table* tbl_atm_ext=NULL;
  cpl_propertylist* hdat=NULL;
  char* name_o=NULL;

  int next=0;
  int ext=0;
  int binx=1;
  int biny=1;
  int bin_size=1;
  double gain=0;
  double exptime=0;
  double airmass=0;

  SINFO_ASSURE_NOT_NULL_MSG(obj_frame,"Null input object frame");
  SINFO_ASSURE_NOT_NULL_MSG(atm_ext_frame,"Null input atm ext frame");

  next=cpl_frame_get_nextensions( obj_frame);
  name_s=cpl_frame_get_filename(obj_frame);

  aname=cpl_frame_get_filename(atm_ext_frame);
  tbl_atm_ext=cpl_table_load(aname,1,0);
  check_nomsg(cpl_table_cast_column( tbl_atm_ext,"LAMBDA","D_LAMBDA",CPL_TYPE_DOUBLE));
  if(!cpl_table_has_column(tbl_atm_ext,SINFO_ATMOS_EXT_LIST_COLNAME_K)){
     sinfo_msg_warning("You are using an obsolete atm extinction line table");
     cpl_table_duplicate_column(tbl_atm_ext,SINFO_ATMOS_EXT_LIST_COLNAME_K,
                                tbl_atm_ext,SINFO_ATMOS_EXT_LIST_COLNAME_OLD);
  }
  check_nomsg(cpl_table_cast_column( tbl_atm_ext,SINFO_ATMOS_EXT_LIST_COLNAME_K,"D_EXTINCTION",CPL_TYPE_DOUBLE));
  name_o=cpl_sprintf("%s.fits",tag_o);

  hdat=cpl_propertylist_load(name_s,0);
  /* observed frame scaling factors */
  check_nomsg(exptime = sinfo_pfits_getexptime(hdat));
  if( band == INSTRUMENT_BAND_NIR){
    /* we assume gain in units of ADU/e- as ESO standard */
    gain=1./2.12;
  } else {
     check_nomsg(gain = sinfo_pfits_get_gain(hdat));
  }

  if (correct_binning && (band != INSTRUMENT_BAND_NIR)) {
    /* x-binning of rotated image is y-binning of raw image */
    check_nomsg(binx  = sinfo_pfits_get_binx(hdat));
    check_nomsg(biny  = sinfo_pfits_get_biny(hdat));
    bin_size=binx*biny;
  } else {
    //sinfo_msg_dbg_medium("Spectrum will not be normalized to unit binning");
  }
  check_nomsg(airmass=sinfo_pfits_get_airm_mean(hdat));

  for(ext=0;ext<next;ext+=3) {
    check_nomsg(sinfo_normalize_spectrum_image_slice(name_s,tag_o,ext,bin_size,gain,
                             exptime,airmass,tbl_atm_ext));
  }/* end loop over extention */

  result=sinfo_frame_product(name_o,tag_o,CPL_FRAME_TYPE_IMAGE,CPL_FRAME_GROUP_PRODUCT,CPL_FRAME_LEVEL_FINAL);

 cleanup:

  sinfo_free_table(&tbl_atm_ext);
  sinfo_free_propertylist(&hdat);
  cpl_free(name_o);

  return result;

}

/*----------------------------------------------------------------------------*/
/**
  @brief    Convert a vector to a 1d image
  @param    vector to convert
  @param    type   of image

  @return   a new allocated 1d image whose elements are the same as the vector
  @em The image need to be allocated .

 */
/*----------------------------------------------------------------------------*/

cpl_image*
sinfo_vector_to_image(const cpl_vector* vector,cpl_type type)
{
   int i=0;
   cpl_image* image=NULL;
   int size=0;
   const double* pv=NULL;
   int* pi=NULL;
   float* pf=NULL;
   double* pd=NULL;


   size=cpl_vector_get_size(vector);
   image=cpl_image_new(size,1,type);
   pv=cpl_vector_get_data_const(vector);
   if(type == CPL_TYPE_INT) {
      pi=cpl_image_get_data_int(image);
      for(i=0;i<size;i++) {
         pi[i]=pv[i];
      }
   } else if (type == CPL_TYPE_FLOAT) {
      pf=cpl_image_get_data_float(image);
      for(i=0;i<size;i++) {
         pf[i]=pv[i];
      }
   } else if (type == CPL_TYPE_DOUBLE) {
      pd=cpl_image_get_data_double(image);
      for(i=0;i<size;i++) {
         pd[i]=pv[i];
      }
   } else {
      assure( false, CPL_ERROR_INVALID_TYPE,
              "No CPL type to represent BITPIX = %d", type);
   }

  cleanup:
   if (cpl_error_get_code() != CPL_ERROR_NONE){
      sinfo_free_image(&image);
   }

   return image;

}




/**
@brief converts a fits image to a spectral vector
@name sinfo_image_to_vector()
@param spectrum 1-D Fits image that should be converted to a spectral vector
@return spectral vector with length lx*ly
@note input image is destroyed
*/

cpl_vector *
sinfo_image_to_vector( cpl_image * spectrum )
{
    cpl_vector * result =NULL;
    cpl_type type=CPL_TYPE_FLOAT;

    int i =0;
    int ilx=0;
    int ily=0;

    int* pi=NULL;
    float* pf=NULL;
    double* pd=NULL;
    double* pv=NULL;

    int size=0;


    SINFO_ASSURE_NOT_NULL_MSG(spectrum,"NULL input spectrum (1D) image!Exit.");
    ilx=cpl_image_get_size_x(spectrum);
    ily=cpl_image_get_size_y(spectrum);
    size=ilx*ily;

    result=cpl_vector_new(size);
    pv=cpl_vector_get_data(result);

    switch(type) {

       case CPL_TYPE_INT:
          pi=cpl_image_get_data_int(spectrum);
          for ( i = 0 ; i < size ; i++ ) pv[i] = (double) pi[i] ;
          break;

       case CPL_TYPE_FLOAT:
          pf=cpl_image_get_data_float(spectrum);
          for ( i = 0 ; i < size ; i++ ) pv[i] = (double) pf[i] ;
          break;

       case CPL_TYPE_DOUBLE:
          pd=cpl_image_get_data_double(spectrum);
          for ( i = 0 ; i < size ; i++ ) pv[i] = (double) pd[i] ;
          break;

       default:
          sinfo_msg_error("Wrong input image data type %d",type);
    }

  cleanup:

    return result ;
}
/**
 * Return the data1 buffer from a sinfo_rec_list structure.
 *
 * @param list Rectify structure pointer
 * @param idx Index in the list
 *
 * @return Pointer to data1 buffer
 */
float * sinfo_rec_list_get_data1( sinfo_rec_list* list, int idx )
{
  float * res = NULL ;

  SINFO_ASSURE_NOT_NULL(list);
  res = list->list[idx].data1 ;

 cleanup:
  return res ;
}

/**
 * Return the errs1 buffer from a sinfo_rec_list structure.
 *
 * @param list Rectify structure pointer
 * @param idx Index in the list
 *
 * @return Pointer to errs1 buffer
 */
float * sinfo_rec_list_get_errs1( sinfo_rec_list* list, int idx )
{
  float * res = NULL ;

  SINFO_ASSURE_NOT_NULL(list);
  res = list->list[idx].errs1 ;

 cleanup:
  return res ;
}

/**
 * Return the qual1 buffer from a sinfo_rec_list structure.
 *
 * @param list Rectify structure pointer
 * @param idx Index in the list
 *
 * @return Pointer to qual1 buffer
 */
int * sinfo_rec_list_get_qual1( sinfo_rec_list* list, int idx )
{
  int * res = NULL ;

  SINFO_ASSURE_NOT_NULL(list);
  res = list->list[idx].qual1 ;

 cleanup:
  return res ;
}

/**
 * Return the number of lambda from a sinfo_rec_list structure.
 *
 * @param list Rectify structure pointer
 * @param idx Index in the list
 *
 * @return nlambda
 */
int sinfo_rec_list_get_nlambda( sinfo_rec_list* list, int idx )
{
  int res = 0 ;

  SINFO_ASSURE_NOT_NULL( list);
  res = list->list[idx].nlambda ;

 cleanup:
  return res ;
}

/**
 * Return the slit buffer from a sinfo_rec_list structure.
 *
 * @param list Rectify structure pointer
 * @param idx Index in the list
 *
 * @return Pointer to slit buffer
 */
float * sinfo_rec_list_get_slit( sinfo_rec_list* list, int idx )
{
  float * res = NULL ;

  SINFO_ASSURE_NOT_NULL(list);
  res = list->list[idx].slit ;

 cleanup:
  return res ;
}

double sinfo_rec_list_get_lambda_min( sinfo_rec_list* list)
{
  double lambda_min = 10000;
  int i;

  SINFO_ASSURE_NOT_NULL( list);

  for( i=0; i< list->size; i++){
    if ( list->list[i].lambda != NULL){
      double lambda;

      lambda = list->list[i].lambda[0];
      if ( lambda < lambda_min){
        lambda_min = lambda;
      }
    }
  }

  cleanup:
    return lambda_min;
}

/**
 * Return the lambda buffer from a sinfo_rec_list structure.
 *
 * @param list Rectify structure pointer
 * @param idx Index in the list
 *
 * @return Pointer to lambda buffer
 */
double* sinfo_rec_list_get_lambda( sinfo_rec_list* list, int idx )
{
  double* res = NULL ;

  SINFO_ASSURE_NOT_NULL(list);
  res = list->list[idx].lambda ;

 cleanup:
  return res ;
}

/*---------------------------------------------------------------------------*/
/**
 * @brief Get slit axis ize of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return the slit axis size of flux data in spectrum
 */
/*---------------------------------------------------------------------------*/
int sinfo_spectrum_get_size_slit( sinfo_spectrum* s)
{
  int res=0;

  SINFO_ASSURE_NOT_NULL( s);

  res = s->size_slit;

  cleanup:
    return res;
}
/*---------------------------------------------------------------------------*/
/**
 * @brief Get flux of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return the flux data of spectrum
 */
/*---------------------------------------------------------------------------*/
double* sinfo_spectrum_get_flux( sinfo_spectrum* s)
{
  double *res=NULL;

  SINFO_ASSURE_NOT_NULL( s);

  check_nomsg( res = cpl_image_get_data_double( s->flux));

  cleanup:
    return res;
}


/*---------------------------------------------------------------------------*/
/**
 * @brief Get errs of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return the errs data of spectrum
 */
/*---------------------------------------------------------------------------*/
double* sinfo_spectrum_get_errs( sinfo_spectrum* s)
{
  double *res=NULL;

  SINFO_ASSURE_NOT_NULL( s);

  check_nomsg( res = cpl_image_get_data_double( s->errs));

  cleanup:
    return res;
}


/*---------------------------------------------------------------------------*/
/**
 * @brief Get qual of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return the qual data of spectrum
 */
/*---------------------------------------------------------------------------*/
int* sinfo_spectrum_get_qual( sinfo_spectrum* s)
{
  int* res = NULL;

  SINFO_ASSURE_NOT_NULL( s);

  check_nomsg( res = cpl_image_get_data_int( s->qual));

  cleanup:
    return res;
}

/*---------------------------------------------------------------------------*/
/**
 * @brief Get lambda axis size of spectrum
 *
 * @param[in] s spectrum structure
 *
 * @return the lambda axis size of flux data in spectrum
 */
/*---------------------------------------------------------------------------*/
int sinfo_spectrum_get_size_lambda( sinfo_spectrum* s)
{
  int res=0;

  SINFO_ASSURE_NOT_NULL( s);

  res = s->size_lambda;

  cleanup:
    return res;
}



/*----------------------------------------------------------------------------*/
/**
   @brief        Spline interpolation based on Hermite polynomials
   @param xp     x-value to interpolate
   @param x      x-values
   @param y      y-values
   @param n      array length
   @param istart    (input/output) initial row (set to 0 to search all row)

   @return The interpolated value.

   The x column must be sorted (ascending or descending) and all x column
   values must be different.

   Adopted from: Cristian Levin - ESO La Silla, 1-Apr-1991
*/
/*----------------------------------------------------------------------------*/
static double
sinfo_spline_hermite( double xp, const double *x, const double *y, int n, int *istart )
{
    double yp1, yp2, yp = 0;
    double xpi, xpi1, l1, l2, lp1, lp2;
    int i;

    if ( x[0] <= x[n-1] && (xp < x[0] || xp > x[n-1]) )    return 0.0;
    if ( x[0] >  x[n-1] && (xp > x[0] || xp < x[n-1]) )    return 0.0;

    if ( x[0] <= x[n-1] )
    {
        for ( i = (*istart)+1; i <= n && xp >= x[i-1]; i++ )
        ;
    }
    else
    {
        for ( i = (*istart)+1; i <= n && xp <= x[i-1]; i++ )
        ;
    }

    *istart = i;
    i--;

    lp1 = 1.0 / (x[i-1] - x[i]);
    lp2 = -lp1;

    if ( i == 1 )
    {
        yp1 = (y[1] - y[0]) / (x[1] - x[0]);
    }
    else
    {
        yp1 = (y[i] - y[i-2]) / (x[i] - x[i-2]);
    }

    if ( i >= n - 1 )
    {
        yp2 = (y[n-1] - y[n-2]) / (x[n-1] - x[n-2]);
    }
    else
    {
        yp2 = (y[i+1] - y[i-1]) / (x[i+1] - x[i-1]);
    }

    xpi1 = xp - x[i];
    xpi  = xp - x[i-1];
    l1   = xpi1*lp1;
    l2   = xpi*lp2;

    yp = y[i-1]*(1 - 2.0*lp1*xpi)*l1*l1 +
         y[i]*(1 - 2.0*lp2*xpi1)*l2*l2 +
         yp1*xpi*l1*l1 + yp2*xpi1*l2*l2;

    return yp;
}


/*----------------------------------------------------------------------------*/
/**
   @brief        Spline interpolation based on Hermite polynomials
   @param xp     x-value to interpolate
   @param t      Table containing the columns to interpolate
   @param column_x  Column of x-values
   @param column_y  Column of y-values
   @param istart    (input/output) initial row (set to 0 to search all row)

   @return The interpolated value.
*/
/*----------------------------------------------------------------------------*/

double
sinfo_spline_hermite_table( double xp, const cpl_table *t, const char *column_x,
                const char *column_y, int *istart )
{
    double result = 0;
    int n;

    const double *x, *y;

    check( x = cpl_table_get_data_double_const(t, column_x),
       "Error reading column '%s'", column_x);
    check( y = cpl_table_get_data_double_const(t, column_y),
       "Error reading column '%s'", column_y);

    n = cpl_table_get_nrow(t);

    result = sinfo_spline_hermite(xp, x, y, n, istart);

  cleanup:
    return result;
}


/**
 * @brief Concatenate an arbitrary number of strings.
 *
 * @param s First string
 * @param  ...  Other strings (at least 1 more)
 *
 * @return Pointer to concatenated string or NULL if error

 * @note The resulting string must be deallocated using @c cpl_free()
 * @note THE LAST STRING MUST BE a NULL ptr or AN EMPTY STRING ("").
 *       However, one should not just use the plain NULL macro in the parameter
 *       list, since this can lead to undefined behaviour. On some platforms the
 *       NULL macro might not expant to a pointer type, i.e. sizeof(NULL) might
 *       not equal sizeof(void*).
 *       Instead one should use code like the following:
 *       @code
 *         y = sinfo_stringcat_any(x, (void*)NULL)
 *       @endcode
 *
 */
char *
sinfo_stringcat_any (const char *s, ...)
{
  char *result = NULL;
  int size = 2;
  va_list av;

  va_start (av, s);
  result = cpl_malloc (2);
  assure (result != NULL, CPL_ERROR_ILLEGAL_OUTPUT,
      "Memory allocation failed");
  result[0] = '\0';
  for (;;) {
    size += strlen (s) + 2;
    result = cpl_realloc (result, size);
    assure (result != NULL, CPL_ERROR_ILLEGAL_OUTPUT,
        "Memory allocation failed");
    strcat (result, s);
    s = va_arg (av, char *);
    if (s == NULL || *s == '\0')
      break;
  }

cleanup:
  if (cpl_error_get_code () != CPL_ERROR_NONE) {
    cpl_free (result);
    result = NULL;
  }

  va_end (av);
  return result;
}


void sinfo_ima2table(char** argv) {

    cpl_vector* vec;
    cpl_table* tab;
    const char* fname1;
    const char* fname2;
    cpl_propertylist* plist;
    cpl_propertylist* head;

    fname1=argv[1];
    fname2=argv[2];
    sinfo_msg_warning("fname=%s",fname1);
    vec=cpl_vector_load(fname1,0);
    plist=cpl_propertylist_load(fname1,0);
    head=cpl_propertylist_load(fname2,0);

    sinfo_msg_warning("nk1");
    double crpix1=cpl_propertylist_get_double(plist,"CRPIX1");

    sinfo_msg_warning("nk2");
    double crval1=cpl_propertylist_get_double(plist,"CRVAL1");

    sinfo_msg_warning("nk3");
    double cdelt1=cpl_propertylist_get_double(plist,"CDELT1");

    sinfo_msg_warning("nk4 wstep=%g",cdelt1);
    int nrow=cpl_vector_get_size(vec);
    tab=cpl_table_new(nrow);
    cpl_table_wrap_double(tab,cpl_vector_get_data(vec),"counts_d");
    cpl_table_duplicate_column(tab,"wave_d",tab,"counts_d");
    double* pwav=cpl_table_get_data_double(tab,"wave_d");
    double x;
    for (int i=0; i<nrow; i++) {
        x=i;
        pwav[i]=crval1+(x-crpix1+1)*cdelt1;
    }
    sinfo_msg_warning("wmin=%g wmax=%g wstep=%g",pwav[0],pwav[nrow-1],cdelt1);
    cpl_table_cast_column(tab,"wave_d","wavelength",CPL_TYPE_FLOAT);
    cpl_table_cast_column(tab,"counts_d", "counts_bkg",CPL_TYPE_FLOAT);
    cpl_table_erase_column(tab,"wave_d");
    cpl_table_unwrap(tab,"counts_d");
    cpl_table_save(tab,head,NULL,"sci.fits",CPL_IO_DEFAULT);

    cpl_table_delete(tab);
    cpl_propertylist_delete(plist);
    cpl_propertylist_delete(head);
    cpl_vector_delete(vec);
    return;

}
