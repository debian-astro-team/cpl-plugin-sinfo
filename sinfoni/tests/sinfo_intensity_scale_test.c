/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2013-08-15 12:05:49 $
 * $Revision: 1.8 $
 */
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
//#include <unistd.h>
#include <string.h>

#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_utils_wrappers.h>
#include <getopt.h>

const char* SINFO_STAR_BUG_REPORT = "amodigli@eso.org";






int main(int argc, char * const argv[])
{
	cpl_test_init(SINFO_STAR_BUG_REPORT, CPL_MSG_WARNING);
    int print_usage = 0;
	const char* file_on = NULL ;
	const char* file_off = NULL ;
	double scale=1.0;
	int opt = 0;

	if (argc < 4)
	{
		print_usage = 1;
	}
	else
	{
		file_on = argv[1];
		file_off = argv[2];
		scale = atof(argv[3]);
	}
	if (file_on == NULL)
	{
		print_usage = 1;
	}
	if(print_usage)
	{
		fprintf(stderr, "usage: %s filename_on.fits filename_off.fits\n", argv[0]);
		return 0;
	}
	cpl_propertylist* hon=NULL;
	cpl_propertylist* htab=NULL;
	cpl_table* tab=NULL;
	hon=cpl_propertylist_load(file_on,0);
	htab=cpl_propertylist_load(file_on,1);
	tab=cpl_table_load(file_on,1,0);
	cpl_image* ima_on=NULL;
	cpl_image* ima_off=NULL;

	double mean = 0;
	double median = 0;
	double stdev =0;
	double max=0;
	ima_on=cpl_image_load(file_on,CPL_TYPE_DOUBLE,0,0);
	mean=cpl_image_get_mean(ima_on);
	median=cpl_image_get_median(ima_on);
	stdev=cpl_image_get_stdev(ima_on);
	sinfo_msg_warning("On frame mean=%g median=%g stdev=%g",mean,median,stdev);

	ima_off=cpl_image_load(file_off,CPL_TYPE_DOUBLE,0,0);
	mean=cpl_image_get_mean(ima_off);
	median=cpl_image_get_median(ima_off);
	stdev=cpl_image_get_stdev(ima_off);
	sinfo_msg_warning("Off frame mean=%g median=%g stdev=%g",mean,median,stdev);

	cpl_image_subtract(ima_on,ima_off);

	mean=cpl_image_get_mean(ima_on);
	median=cpl_image_get_median(ima_on);
	stdev=cpl_image_get_stdev(ima_on);
	max=cpl_image_get_max(ima_on);
	double thresh = 3* median;
	cpl_image_threshold(ima_on,thresh,max,0,1);
	sinfo_msg_warning("Diff frame mean=%g median=%g stdev=%g",mean,median,stdev);
	cpl_image_save(ima_on,"ima_diff.fits",CPL_BPP_IEEE_FLOAT,hon,CPL_IO_DEFAULT);
    cpl_image_multiply_scalar(ima_on,scale);
    cpl_image_add(ima_on,ima_off);
    cpl_image_save(ima_on,"ima_scale.fits",CPL_BPP_IEEE_FLOAT,hon,CPL_IO_DEFAULT);
    cpl_table_save(tab,hon,htab,"ima_scale.fits",CPL_IO_EXTEND);
	cpl_image_delete(ima_on);
	cpl_image_delete(ima_off);
	cpl_table_delete(tab);
	cpl_propertylist_delete(hon);
	cpl_propertylist_delete(htab);
	cpl_test_end(0);
	return 0;
}
