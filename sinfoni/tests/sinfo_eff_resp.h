#ifndef SINFO_EFF_RESP_H
#define SINFO_EFF_RESP_H
#include <cpl.h>
#include <sinfo_error.h>

#define SINFO_SPECTRUM_DATA_BPP CPL_BPP_IEEE_FLOAT
#define SINFO_SPECTRUM_ERRS_BPP CPL_BPP_IEEE_FLOAT
#define SINFO_SPECTRUM_QUAL_BPP CPL_BPP_32_SIGNED
#define SINFO_BUNIT "BUNIT"
#define SINFO_BUNIT_FLUX_ABS_C "erg/s/cm2/Angstrom"

#define SINFO_BUNIT_NONE_C ""

#define SINFO_CRPIX1 "CRPIX1"
#define SINFO_CRVAL1 "CRVAL1"

#define SINFO_CDELT1 "CDELT1"
#define SINFO_CTYPE1 "CTYPE1"
#define SINFO_CUNIT1 "CUNIT1"
#define SINFO_CUNIT1_C "Wavelength units"
#define SINFO_NAXIS1 "NAXIS1"
#define SINFO_NAXIS "NAXIS"

#define SINFO_EXTNAME "EXTNAME"
#define SINFO_PCATG "ESO PRO CATG"

#define SINFO_ATMOS_EXT_LIST_COLNAME_K "EXTINCTION"
#define SINFO_WIN_BINX "ESO DET WIN1 BINX"
#define SINFO_WIN_BINY "ESO DET WIN1 BINY"
#define SINFO_ATMOS_EXT_LIST_COLNAME_K "EXTINCTION"
#define SINFO_ATMOS_EXT_LIST_COLNAME_OLD "LA_SILLA"


#define SINFO_MERGE_EXT1D "MERGE_EXT1D"
#define SINFO_MERGE_OXT1D "MERGE_OXT1D"
#define SINFO_MERGE1D "MERGE1D"
#define SINFO_MERGE2D "MERGE2D"

#define SINFO_ORDER1D "ORDER1D"
#define SINFO_ORDER_EXT1D "ORDER_EXT1D"
#define SINFO_ORDER_OXT1D "ORDER_OXT1D"

#define SINFO_NORM_OXT1D "NORM_OXT1D"
#define SINFO_NORM_ORDER1D "NORM_ORDER1D"
#define SINFO_NORM_ORDER2D "NORM_ORDER2D"
#define SINFO_NORM_MERGE1D "NORM_MERGE1D"
#define SINFO_NORM_MERGE2D "NORM_MERGE2D"

#define SINFO_FLUX_OXT1D "FLUX_OXT1D"
#define SINFO_FLUX_ORDER2D "FLUX_ORDER2D"
#define SINFO_FLUX_MERGE1D "FLUX_MERGE1D"

#define SINFO_FLUX_ORDER1D "FLUX_ORDER1D"
#define SINFO_FLUX_ORDER1D "FLUX_ORDER1D"
#define SINFO_FLUX_MOXT1D "FLUX_MOXT1D"

#define SINFO_COMBINED_OFFSET_2D_SLIT     "COMBINED_OFFSET_2D_SLIT_"

#define SINFO_FLUX_ORDER2D "FLUX_ORDER2D"
#define SINFO_FLUX_MERGE1D "FLUX_MERGE1D"
#define SINFO_ORDER_EXT1D "ORDER_EXT1D"
#define SINFO_ORDER_OXT1D "ORDER_OXT1D"
#define SINFO_ORDER2D "ORDER2D"

#define SINFO_CRPIX1 "CRPIX1"
#define SINFO_CRPIX2 "CRPIX2"
#define SINFO_CRPIX3 "CRPIX3"


#define SINFO_CDELT1 "CDELT1"
#define SINFO_CDELT2 "CDELT2"
#define SINFO_CDELT3 "CDELT3"

#define SINFO_CRVAL1 "CRVAL1"
#define SINFO_CRVAL2 "CRVAL2"
#define SINFO_CRVAL3 "CRVAL3"


#define SINFO_CTYPE1 "CTYPE1"
#define SINFO_CTYPE2 "CTYPE2"
#define SINFO_CTYPE3 "CTYPE3"

#define SINFO_CD11 "CD1_1"
#define SINFO_CD12 "CD1_2"
#define SINFO_CD21 "CD2_1"
#define SINFO_CD22 "CD2_2"
#define SINFO_EXPTIME "EXPTIME"

#define SINFO_RECTIFY_BIN_SPACE  "ESO PRO RECT BIN SPACE"
#define SINFO_RECTIFY_SPACE_MIN  "ESO PRO RECT SPACE MIN"
#define SINFO_RECTIFY_SPACE_MAX  "ESO PRO RECT SPACE MAX"
#define SINFO_RECTIFY_LAMBDA_MIN "ESO PRO RECT LAMBDA MIN"
#define SINFO_RECTIFY_LAMBDA_MAX "ESO PRO RECT LAMBDA MAX"
#define SINFO_RECTIFY_BIN_LAMBDA "ESO PRO RECT BIN LAMBDA"

#define SINFO_REC_TABLE_COLNAME_FLUX1   "FLUX1"
#define SINFO_REC_TABLE_COLNAME_ERRS1   "ERRS1"
#define SINFO_REC_TABLE_COLNAME_QUAL1   "QUAL1"
#define SINFO_REC_TABLE_COLNAME_LAMBDA "LAMBDA"
#define SINFO_REC_TABLE_COLNAME_SLIT   "SLIT"
#define SINFO_REC_TABLE_COLNAME_NSLIT "NSLIT"
#define SINFO_REC_TABLE_COLNAME_NLAMBDA "NLAMBDA"
#define SINFO_REC_TABLE_COLNAME_ORDER "ORDER"
#define SINFO_COMBINED_OFFSET_2D_IFU     "COMBINED_OFFSET_2D_IFU_"
#define  SINFO_QC_EFF_FCLIP "ESO QC FCLIP"


#define  SINFO_QC_EFF_NCLIP "ESO QC NCLIP"
#define SINFO_DET_WIN1_DIT1 "ESO DET WIN1 DIT1"
#define SINFO_CONAD "ESO DET OUT1 CONAD"
#define SINFO_GET_TAG_FROM_SLITLET( TAG, slitlet, instr)\
 ( (sinfo_instrument_get_arm(instr)) == SINFO_ARM_UVB && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_SLIT)) ? TAG##_UVB : \
 ( (sinfo_instrument_get_arm(instr)) == SINFO_ARM_VIS && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_SLIT)) ? TAG##_VIS : \
 ( (sinfo_instrument_get_arm(instr)) == SINFO_ARM_NIR && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_SLIT)) ? TAG##_NIR : \
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_UVB) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == LOWER_IFU_SLITLET)) ? TAG##_DOWN_IFU_UVB :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_UVB) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == CENTER_IFU_SLITLET)) ? TAG##_CEN_IFU_UVB :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_UVB) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == UPPER_IFU_SLITLET)) ? TAG##_UP_IFU_UVB :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_VIS) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == LOWER_IFU_SLITLET)) ? TAG##_DOWN_IFU_VIS :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_VIS) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == CENTER_IFU_SLITLET)) ? TAG##_CEN_IFU_VIS :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_VIS) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == UPPER_IFU_SLITLET)) ? TAG##_UP_IFU_VIS :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_NIR) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == LOWER_IFU_SLITLET)) ? TAG##_DOWN_IFU_NIR :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_NIR) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == CENTER_IFU_SLITLET)) ? TAG##_CEN_IFU_NIR :\
 ( (sinfo_instrument_get_arm(instr) == SINFO_ARM_NIR) && \
   (sinfo_instrument_get_mode(instr) == SINFO_MODE_IFU) && \
   (slitlet == UPPER_IFU_SLITLET)) ? TAG##_UP_IFU_NIR : "??TAG??"


#define SINFO_ASSURE_NOT_NULL(pointer) \
  assure(pointer != NULL, CPL_ERROR_NULL_INPUT,\
    "You have null pointer in input: " #pointer)

#define SINFO_ASSURE_NOT_NULL_MSG(pointer,msg)       \
  assure(pointer != NULL, CPL_ERROR_NULL_INPUT,\
    "You have null pointer in input: " #pointer "\n" msg)

#define SINFO_ASSURE_NOT_ILLEGAL(cond) \
  assure(cond, CPL_ERROR_ILLEGAL_INPUT,\
    "condition failed: " #cond )

#define SINFO_ASSURE_NOT_ILLEGAL_MSG(cond, msg) \
  assure(cond, CPL_ERROR_ILLEGAL_INPUT,\
    "condition failed: " #cond "\n" msg)

#define SINFO_ASSURE_NOT_MISMATCH(cond) \
  assure(cond, CPL_ERROR_TYPE_MISMATCH,\
    "condition failed: "#cond )

#define SINFO_CALLOC( POINTER, TYPE, SIZE) \
  assure(POINTER == NULL, CPL_ERROR_ILLEGAL_OUTPUT,\
    "Try to allocate non NULL pointer");\
  POINTER = (TYPE*)(cpl_calloc(SIZE,sizeof(TYPE)));\
  assure (POINTER != NULL, CPL_ERROR_ILLEGAL_OUTPUT,\
    "Memory allocation failed!")
#define SINFO_PFITS_GET(RET, LIST, KW, TYPE) \
  check( sinfo_get_property_value( LIST, KW, TYPE, &RET),\
             "Error reading keyword '%s'", KW)

#define SINFO_FREE(POINTER)\
  if(POINTER!=NULL) cpl_free(POINTER);\
  POINTER = NULL

#define SINFO_NEW_PROPERTYLIST( POINTER) \
  assure(POINTER == NULL, CPL_ERROR_ILLEGAL_OUTPUT,\
    "Try to allocate non NULL pointer");\
  POINTER = cpl_propertylist_new();\
  assure (POINTER != NULL, CPL_ERROR_ILLEGAL_OUTPUT,\
    "Memory allocation for propertylist failed!")



enum {
  CENTER_SLIT,
  LOWER_IFU_SLITLET, CENTER_IFU_SLITLET, UPPER_IFU_SLITLET
} ;

typedef struct {
  double lambda_min ;
  double lambda_max ;
} HIGH_ABS_REGION ;

typedef enum _INSTRUMENT_BAND_ {
    INSTRUMENT_BAND_UVB,
    INSTRUMENT_BAND_VIS,
    INSTRUMENT_BAND_NIR,
    INSTRUMENT_BAND_ALL,
    INSTRUMENT_BAND_J,
    INSTRUMENT_BAND_JS,
    INSTRUMENT_BAND_JBLOCK,
    INSTRUMENT_BAND_H,
    INSTRUMENT_BAND_K,
    INSTRUMENT_BAND_HK,
    INSTRUMENT_BAND_KS,
    INSTRUMENT_BAND_L,
    INSTRUMENT_BAND_M,
    INSTRUMENT_BAND_LP,
    INSTRUMENT_BAND_MP,
    INSTRUMENT_BAND_Z,
    INSTRUMENT_BAND_SZ,
    INSTRUMENT_BAND_SH,
    INSTRUMENT_BAND_SK,
    INSTRUMENT_BAND_SL,
    INSTRUMENT_BAND_UNKNOWN
} new_sinfo_band ;

typedef enum {
  SINFO_GD71,
  SINFO_Feige110,
  SINFO_GD153,
  SINFO_LTT3218,
  SINFO_LTT7987,
  SINFO_BD17,
  SINFO_EG274,
  SINFO_SINTH
}  sinfo_std_star_id;

typedef struct{
  /* size of the order */
  int size;
  /* Minimal spectrum lambda */
  double lambda_min;
  /* Maximal spectrum lambda */
  double lambda_max;
  /* step in lambda */
  double lambda_step;
  /* Minimal slit position */
  double slit_min;
  /* Maximal slit position */
  double slit_max;
  /* step in slit */
  double slit_step;
  /* size in lambda */
  int size_lambda;
  /* size in slit */
  int size_slit;
  /* flux data */
  cpl_propertylist* flux_header;
  cpl_image *flux;
  /* errs */
  cpl_propertylist* errs_header;
  cpl_image *errs;
  /* qual */
  cpl_propertylist* qual_header;
  cpl_image *qual;
}sinfo_spectrum;


typedef struct{
  int order ;
  int nlambda;
  int nslit ;
  float * slit ;
  double * lambda ;
  float * data1 ;
  float * data2 ;
  float * data3 ;
  float * errs1 ;
  /* FOR IFU mode */
  float * errs2 ;
  float * errs3 ;
  int * qual1 ;
  int * qual2 ;
  int * qual3 ;
} sinfo_rec ;

typedef struct{
  int size;
  double slit_min;
  double slit_max;
  int nslit;
  //int max_nlambda ;
  //int max_nslit ;
  sinfo_rec * list;
  new_sinfo_band band;
  cpl_propertylist * header;
} sinfo_rec_list;


//#define SINFO_CONCAT(A,B) A ## B
#define SINFO_TABLE_GET_ARRAY( TYPE) \
  void sinfo_table_get_array_##TYPE( cpl_table* table, const char* colname, \
    TYPE* pointer, int nb)\
  {\
  const cpl_array* array = NULL;\
  int array_size = 0, k=0;\
  const TYPE* data = NULL;\
  SINFO_ASSURE_NOT_NULL( pointer);\
  check_nomsg( array = cpl_table_get_array( table, colname, 0));\
  check_nomsg( array_size = cpl_array_get_size( array));\
  SINFO_ASSURE_NOT_ILLEGAL( nb == array_size);\
  check_nomsg( data = cpl_array_get_data_##TYPE##_const( array));\
  for( k=0; k< array_size; k++){\
    pointer[k] = data[k];\
  }\
  cleanup:\
    return;\
  }




HIGH_ABS_REGION *
sinfo_fill_high_abs_regions(
   new_sinfo_band band,
   cpl_frame* high_abs_frame);


struct _star_index_
{
    cpl_table* index_table;
    char* fits_file_name;
    int index_size;
    cpl_table** cache;
    int cache_size;
    int* cache_index;
};

typedef struct _star_index_ star_index;

/* Loading the index from the fits file
 * */
star_index* star_index_load(const char* fits_file);

/*Save the index to the fits file
 * */
star_index* star_index_create(void);
/* Add a new start to the index. To save the changed index to the file star_index_save() should be called
 * */
int star_index_add(star_index* pindex, double RA, double DEC, const char* star_name, cpl_table* ptable);
int star_index_remove_by_name(star_index* pindex, const char* starname);
int start_index_get_size(star_index* pindex);
int star_index_save(star_index* pindex, const char* fits_file_name);
cpl_table* star_index_get(star_index* pindex, double RA, double DEC, double RA_EPS, double DEC_EPS, const char** pstar_name);
void star_index_delete(star_index* pindex);
void star_index_dump(star_index* pindex, FILE* pfile);



cpl_error_code
sinfo_get_property_value(const cpl_propertylist * plist,
            const char *keyword,
                        cpl_type keywordtype,
            void *result);
cpl_frame*
sinfo_frame_product(const char* fname, const char* tag, cpl_frame_type type,
                  cpl_frame_group group,cpl_frame_level level);
cpl_error_code
sinfo_pfits_set_wcs1(cpl_propertylist* header,
                   const double crpix1,
                   const double crval1,
                   const double cdelt1);
int sinfo_pfits_get_binx(const cpl_propertylist * plist);
int sinfo_pfits_get_biny(const cpl_propertylist * plist);
void sinfo_pfits_set_bunit(cpl_propertylist * plist, const char* value);
void sinfo_pfits_set_cunit1(cpl_propertylist * plist, const char* value);
void sinfo_pfits_set_pcatg (cpl_propertylist * plist, const char *value);
void sinfo_pfits_set_ctype1(cpl_propertylist * plist, const char* value);
void sinfo_pfits_set_extname (cpl_propertylist * plist, const char *value);

int sinfo_pfits_get_naxis (const cpl_propertylist * plist);
const char *sinfo_pfits_get_bunit (const cpl_propertylist * plist);

double sinfo_pfits_get_rectify_bin_lambda(cpl_propertylist * plist);
const char * sinfo_pfits_get_extname (const cpl_propertylist * plist);
double sinfo_pfits_get_rectify_bin_space(cpl_propertylist * plist);
double sinfo_pfits_get_rectify_space_max(cpl_propertylist * plist);
double sinfo_pfits_get_rectify_space_min(cpl_propertylist * plist);
double sinfo_pfits_get_rectify_lambda_max(cpl_propertylist * plist);
double sinfo_pfits_get_conad(const cpl_propertylist * plist);

double sinfo_pfits_get_win1_dit1 (const cpl_propertylist * plist);
void sinfo_pfits_set_qc_eff_nclip( cpl_propertylist * plist,int value );
void sinfo_pfits_set_qc_eff_fclip( cpl_propertylist * plist,double value );
static int
sinfo_column_to_double(cpl_table* ptable, const char* column);
static double*
sinfo_create_column_double(cpl_table* tbl, const char* col_name, int nrow);
void
sinfo_frame_sci_get_ra_dec_airmass(cpl_frame* frm_sci,double* ra, double* dec, double* airmass);
cpl_error_code sinfo_set_cd_matrix2d(cpl_propertylist* plist);
cpl_error_code
sinfo_pfits_set_wcs2(cpl_propertylist* header,
                   const double crpix2,
                   const double crval2,
                   const double cdelt2);

double
sinfo_table_interpolate(cpl_table* tbl,
          double wav,
          const char* colx,
              const char* coly);

double
sinfo_data_interpolate(
             double wav,
             int nrow,
             double* pw,
             double* pe
             );

cpl_table*
sinfo_utils_efficiency_internal(
                  cpl_table* tbl_obj_spectrum,
                  cpl_table* tbl_atmext,
                  cpl_table* tbl_ref,
                  double exptime,
                  double airmass,
                  double aimprim,
                  double gain,
                  int    biny,
                              double src2ref_wave_sampling,
                              const char* col_name_atm_wave,
                              const char* col_name_atm_abs,
                              const char* col_name_ref_wave,
                              const char* col_name_ref_flux,
                              const char* col_name_ref_bin,
                              const char* col_name_obj_wave,
                              const char* col_name_obj_flux,
                              int* ntot, int* nclip
   );
const char * sinfo_tostring_cpl_frame_type (cpl_frame_type ft);
const char* sinfo_band_tostring(new_sinfo_band band);
cpl_vector * sinfo_image_to_vector( cpl_image * spectrum );
cpl_image* sinfo_vector_to_image(const cpl_vector* vector,cpl_type type);
double
sinfo_spline_hermite_table( double xp, const cpl_table *t, const char *column_x,
                const char *column_y, int *istart );
double sinfo_pfits_get_rectify_lambda_min(cpl_propertylist * plist);
const char * sinfo_pfits_get_pcatg (const cpl_propertylist * plist);
double sinfo_pfits_getexptime (const cpl_propertylist * plist);

long sinfo_round_double(double x);
void sinfo_spectrum_free( sinfo_spectrum** list);
cpl_frame* sinfo_spectrum_save( sinfo_spectrum* s, const char* filename,
                  const char* tag);
void sinfo_rec_list_free(sinfo_rec_list** list);

float* sinfo_rec_list_get_data1( sinfo_rec_list * list, int idx);

float * sinfo_rec_list_get_errs1( sinfo_rec_list * list, int idx);

int * sinfo_rec_list_get_qual1( sinfo_rec_list * list, int idx);
int sinfo_rec_list_get_nslit( sinfo_rec_list * list, int idx);

int sinfo_rec_list_get_nlambda( sinfo_rec_list * list, int idx);
double* sinfo_rec_list_get_lambda( sinfo_rec_list* list, int idx );
cpl_error_code
sinfo_pfits_set_wcs(cpl_propertylist* header, const double crpix1,
    const double crval1, const double cdelt1, const double crpix2,
    const double crval2, const double cdelt2);
double sinfo_rec_list_get_lambda_min( sinfo_rec_list* list);
cpl_frame *
sinfo_normalize_spectrum_ord(const cpl_frame *obj_frame,
                       const cpl_frame *atm_ext_frame,
                       cpl_boolean correct_binning,
                       new_sinfo_band band,
                       const char* tag);

void sinfo_merge_point( double flux_a, double weight_a,
  double flux_b, double weight_b, double *flux_res,
  double *err_res);
char *sinfo_stringcat_any( const char *s, ...) ;
cpl_error_code
sinfo_normalize_spectrum_image_slice(const char* name_s,
                   const char* tag_o,
                   const int ext,
                   const int bin_size,
                   const double gain,
                   const double exptime,
                   const double airmass,
                   const cpl_table* tbl_atm_ext);
cpl_error_code sinfo_get_table_value (const cpl_table* table,
  const char *colname, cpl_type coltype, int i, void* result);
void sinfo_table_get_array_float( cpl_table* table, const char* colname,
  float* pointer, int nb);
void sinfo_table_get_array_double( cpl_table* table, const char* colname,
  double* pointer, int nb);

void sinfo_table_get_array_int( cpl_table* table, const char* colname,
  int* pointer, int nb);

cpl_error_code sinfo_parse_catalog_std_stars(cpl_frame* cat, double dRA,
    double dDEC, double EPSILON, cpl_table** pptable,sinfo_std_star_id* std_star_id);


int sinfo_spectrum_get_size_lambda( sinfo_spectrum* s);
int sinfo_spectrum_get_size_slit( sinfo_spectrum* s);
int* sinfo_spectrum_get_qual( sinfo_spectrum* s);
double* sinfo_spectrum_get_errs( sinfo_spectrum* s);
double* sinfo_spectrum_get_flux( sinfo_spectrum* s);
sinfo_rec_list* sinfo_rec_list_load_eso(cpl_frame* frame, new_sinfo_band band);
sinfo_rec_list* sinfo_rec_list_load_eso_1d(cpl_frame* frame, new_sinfo_band band);
sinfo_rec_list* sinfo_rec_list_load(cpl_frame* frame, new_sinfo_band band);


sinfo_spectrum* sinfo_spectrum_1D_create( double lambda_min, double lambda_max,
  double lambda_step);
sinfo_spectrum* sinfo_spectrum_2D_create( double lambda_min, double lambda_max,
  double lambda_step, double slit_min, double slit_max, double slit_step);


int sinfo_print_rec_status(const int val);
void sinfo_ima2table(char** argv);
#endif
