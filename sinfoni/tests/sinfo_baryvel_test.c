/*                                                                            *
 *   This file is part of the ESO SINFO Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 06:06:11 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 *
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include<sinfo_msg.h>
#include<sinfo_baryvel.h>
#include<sinfo_key_names.h>
#include<sinfo_dfs.h>
#include <cpl.h>
/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_absolute_test  SINFO library unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/

static cpl_error_code
sinfo_test_baryvel(void){

    cpl_propertylist* head=cpl_propertylist_new();
    double bary_corr=0;
    double helio_corr=0;

    /*
    qc_ra       = m$value({p1},O_POS(1))
     qc_dec      = m$value({p1},O_POS(2))
     qc_geolat   = m$value({p1},{h_geolat})
     qc_geolon   = m$value({p1},{h_geolon})
     qc_obs_time = m$value({p1},O_TIME(7))  !using an image as input it take the
                                            !date from the descriptor O_TIME(1,2,3)
                                            !and the UT from O_TIME(5)
    */
    /* We use values from demo example file (sinfo_rec_jitter_stdstar.sof)
    SINFO.2005-08-22T07:47:54.305.fits
    */
    const double ra=48.070611;

    const double dec=-20.62172;
    const double geolat=-24.6270;
    const double geolon=-70.4040;
    const double utc=28071.000;
    const double mjd_obs=53604.32493408;
    const double bary_corr_expected=22.124927;
    const double helio_corr_expected= 22.120876;

    cpl_propertylist_append_double(head,"RA",ra);
    cpl_propertylist_append_double(head,"DEC",dec);
    cpl_propertylist_append_double(head,H_GEOLON,geolon);
    cpl_propertylist_append_double(head,H_GEOLAT,geolat);
    cpl_propertylist_append_double(head,H_UTC,utc);
    cpl_propertylist_append_double(head,KEY_NAME_MJD_OBS,mjd_obs);

    sinfo_baryvel(head,&bary_corr,&helio_corr);

    cpl_test_eq(bary_corr, bary_corr_expected);
    cpl_test_eq(helio_corr, helio_corr_expected);

    cpl_test_error(CPL_ERROR_NONE);

    /* free memory */
    cpl_propertylist_delete(head);

    return cpl_error_get_code();
}

/*----------------------------------------------------------------------------*/
/**
  @brief   SINFONI pipeline unit test for skycor

**/
/*----------------------------------------------------------------------------*/

int main(void)
{
    /* Initialize CPL + SINFO messaging */
  cpl_test_init(PACKAGE_BUGREPORT, CPL_MSG_WARNING);

  sinfo_test_baryvel();

  cpl_test_error(CPL_ERROR_NONE);
  return cpl_test_end(0);

}


/**@}*/
