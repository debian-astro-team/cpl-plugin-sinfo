/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-09-18 06:53:36 $
 * $Revision: 1.5 $
 * $Log: not supported by cvs2svn $
 * Revision 1.4  2010/06/18 06:55:06  amodigli
 * added comment to make this test working
 *
 * Revision 1.3  2010/06/17 11:45:34  kmirny
 * changed the image file
 *
 * Revision 1.2  2009/07/13 14:40:39  kmirny
 * fixing unit test failure in star catalog
 *
 * Revision 1.1  2009/06/16 15:13:43  kmirny
 * fit gaussian test
 *
 * Revision 1.4  2009/06/15 08:17:12  kmirny
 * put aimprim level up to prepared to use as a recipe parameter
 *
 * Revision 1.3  2009/06/10 14:58:31  kmirny
 * sinfoni efficiency utility recipe
 *
 * Revision 1.2  2009/06/09 14:58:04  kmirny
 * sinfoni efficiency unit test
 *
 * Revision 1.1  2009/06/09 08:57:18  kmirny
 * adding test for sinfo_utl_efficiency
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_pro_save.h>
#include <sinfo_error.h>
#include <sinfo_msg.h>
#include <sinfo_utils_wrappers.h>
//const char* FITS_FILE_IMAGE = "bad_gauss_fit.fits";
//const char* FITS_FILE_IMAGE = "bad_gauss_fit_psf.fits";
const char* FITS_FILE_IMAGE = "__img.fits";

const char* SINFO_GAUSS_BUG_REPORT = "amodigli@eso.org";
void fit_test()
{
	double norm = 0;
	double xcen = 0;
	double ycen = 0;
	double sig_x = 0;
	double sig_y = 0;
	double fwhm_x = 0;
	double fwhm_y = 0;
	// load image
	cpl_image* pimage = 0;
	pimage = cpl_image_load(FITS_FILE_IMAGE, CPL_TYPE_DOUBLE, 0, 0);
	cpl_test(pimage);
	cpl_test(CPL_ERROR_NONE == cpl_image_fit_gaussian(pimage,
	                                                31,//41,
	                                                36, //33,
	                                                12, //Min to success
	                                                &norm,
	                                                &xcen,
	                                                &ycen,
	                                                &sig_x,
	                                                &sig_y,
	                                                &fwhm_x,
	                                                &fwhm_y));
	cpl_image_delete(pimage);
}

int main(void)
{
	cpl_test_init(SINFO_GAUSS_BUG_REPORT, CPL_MSG_WARNING);
	//fit_test();
	return cpl_test_end(0);
}
