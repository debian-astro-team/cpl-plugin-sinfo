/*                                                                            *
 *   This file is part of the ESO SINFO Pipeline                              *
 *   Copyright (C) 2004,2005 European Southern Observatory                    *
 *                                                                            *
 *   This library is free software; you can redistribute it and/or modify     *
 *   it under the terms of the GNU General Public License as published by     *
 *   the Free Software Foundation; either version 2 of the License, or        *
 *   (at your option) any later version.                                      *
 *                                                                            *
 *   This program is distributed in the hope that it will be useful,          *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of           *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *
 *   GNU General Public License for more details.                             *
 *                                                                            *
 *   You should have received a copy of the GNU General Public License        *
 *   along with this program; if not, write to the Free Software              *
 *   Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA     *
 *                                                                           */
 
/*
 * $Author: amodigli $
 * $Date: 2009-06-05 06:06:11 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 *
 */

/*-----------------------------------------------------------------------------
                                Includes
 -----------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <string.h>
#include <cpl.h>
#include <cpl_test.h>
#include <sinfo_eff_resp.h>
#include <sinfo_key_names.h>
#include <sinfo_msg.h>
#include <sinfo_error.h>
#include <sinfo_tpl_utils.h>
#include <sinfo_utils_wrappers.h>
#include <sinfo_pfits.h>


/*-----------------------------------------------------------------------------
                                Defines
 -----------------------------------------------------------------------------*/
static const double STAR_MATCH_DEPSILON=0.0166667; /*60 arcsecs */
static const char COL_NAME_ORD_OBJ[]     = "ORD";//"counts_tot";
static const char COL_NAME_WAVE_OBJ[] = "WAVELENGTH";
static const char COL_NAME_INT_OBJ[]     = "INT_OBJ";//"counts_tot"
static const char COL_NAME_WAVE_ATMDISP[] = "LAMBDA";
static const char COL_NAME_ABS_ATMDISP[] = SINFO_ATMOS_EXT_LIST_COLNAME_K;
static const char COL_NAME_WAVE_REF[]            = "LAMBDA";
static const char COL_NAME_FLUX_REF[]            = "FLUX";
static const char COL_NAME_BINWIDTH_REF[]        = "BIN_WIDTH";
#define SINFO_QC_EFF_PEAK_ORD "ESO QC EFF PEAK ORD"
#define SINFO_QC_EFF_MED_ORD "ESO QC EFF MED ORD"
const char* SINFO_EFFICIENCY_BUG_REPORT = "amodigli@eso.org";
static const char COL_NAME_HIGH_ABS[] = "HIGH_ABS";
static const char COL_NAME_WAVELENGTH[] = "WAVELENGTH";
/*-----------------------------------------------------------------------------
                            Functions prototypes
 -----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_skycor_test  SINFO library unit tests
 */
/*----------------------------------------------------------------------------*/
/**@{*/
#define SYNTAX "Test efficiency computation algorithm\n"\
  "usage : \n"\
  "./sinfo_compute_efficiency_test REC1D_SPECTRA STD_STAR_CAT ATM_EXT_TAB HIGH_ABS\n"\
  "REC1D_SPECTRA     => extracted std star spectrum order by order\n"\
  "ATM_EXT_TAB       => Table with atm extinction coeffs\n"\
  "MERGED_RESPONSE1D => Table with response fit points\n"

/*----------------------------------------------------------------------------*/
/**
  @brief   SINFONI pipeline unit test for skycor

**/
/*----------------------------------------------------------------------------*/
// TO BE FIXED (gain is wrong in NIR)
static void
sinfo_frame_sci_get_gain_airmass_exptime_naxis1_biny(cpl_frame* frm_sci,
                                           new_sinfo_band band,
                                           double* gain,
                                           double* airmass,
                                           double* exptime,
                                           int* naxis1,
                                           int* biny)
{

   const char* name_sci=NULL;
   cpl_propertylist* plist=NULL;

   name_sci=cpl_frame_get_filename(frm_sci);
   check_nomsg(plist=cpl_propertylist_load(name_sci,0));
   check_nomsg(*naxis1=sinfo_pfits_get_naxis1(plist));
   *airmass=sinfo_pfits_get_airm_mean(plist);

  if( band == INSTRUMENT_BAND_NIR ||
      band == INSTRUMENT_BAND_J ||
      band == INSTRUMENT_BAND_H ||
      band == INSTRUMENT_BAND_K ||
      band == INSTRUMENT_BAND_L
                  ){
    *gain=2.12;
    *biny=1;
    check_nomsg(*exptime=sinfo_pfits_get_dit(plist));
  } else {
    check_nomsg(*gain=sinfo_pfits_get_conad(plist));
    check_nomsg(*biny=sinfo_pfits_get_biny(plist));
    check_nomsg(*exptime=sinfo_pfits_get_win1_dit1(plist));
  }

cleanup:
   sinfo_free_propertylist(&plist);

   return;
}


static cpl_error_code
sinfo_efficiency_add_high_abs_regions(cpl_table** eff,HIGH_ABS_REGION * phigh)
{

    sinfo_msg_warning("eff=%p",*eff);
    int nrow=cpl_table_get_nrow(*eff);
    double* pwav=NULL;
    int* phigh_abs=NULL;



    cpl_table_new_column(*eff,COL_NAME_HIGH_ABS,CPL_TYPE_INT);
    cpl_table_fill_column_window_int(*eff,COL_NAME_HIGH_ABS,0,nrow,0);
    pwav=cpl_table_get_data_double(*eff,COL_NAME_WAVELENGTH);
    phigh_abs=cpl_table_get_data_int(*eff,COL_NAME_HIGH_ABS);

    if ( phigh != NULL ) {
        for( int k = 0 ; phigh->lambda_min != 0. ; k++, phigh++ ) {
            for(int i=0;i<nrow;i++) {
                if(pwav[i]>=phigh->lambda_min && pwav[i]<=phigh->lambda_max) {
                    phigh_abs[i]=1;
                }
            }
        }
    }

    return cpl_error_get_code();

}

/**
@brief computes efficiency
@param frm_sci science frame
@param frm_cat catalog frame
@param frm_atmext atmospheric extinction frame
@param instrument instrument arm setting
 */
cpl_frame*
sinfo_efficiency_compute(cpl_frame* frm_sci,
                       cpl_frame* frm_cat,
                       cpl_frame* frm_atmext,
                       cpl_frame* high_abs_win,
                       new_sinfo_band band)

{

  cpl_image* ima_sci=NULL;
  cpl_vector* vec_ord=NULL;
  cpl_image* ima_obj=NULL;
  cpl_table* obj_tab=NULL;

  const char* name_sci=NULL;
  const char* name_atm=NULL;
  cpl_propertylist* plist=NULL;
  cpl_propertylist* x_plist=NULL;


  cpl_table* tbl_ord=NULL;
  cpl_table* tot_eff=NULL;
  cpl_table* tbl_eff=NULL;
  cpl_table* tbl_ref=NULL;
  cpl_table* tbl_atmext=NULL;

  double * pobj=NULL;
  double * pw=NULL;
  double * pf=NULL;
  int * po=NULL;

  cpl_frame* frm_eff=NULL;

  //char name_eff[256];

  double crval1=0;
  double cdelt1=0;
  int naxis1=0;
  int nrow=0;


  double exptime=600;
  cpl_vector* rec_profile=NULL;
  int i=0;
  double airmass=0;
  double dRA=0;
  double dDEC=0;
  char key_name[40];
  int next=0;
  int nord=0;



  int j=0;
  double wav=0;
  double gain=0;
  int biny=1;
  double aimprim=0;
  int ord=0;
  //double nm2AA=10.;
  double nm2nm=1.;

  char fname[256];
  char tag[256];
  double emax=0;
  double emed=0;
  int nclip=0;
  int ntot=0;

  int nclip_tot=0;
  int neff_tot=0;
  double fclip=0;
  sinfo_std_star_id std_star_id=0;
  int vec_size=0;
  char name_eff[80];

  SINFO_ASSURE_NOT_NULL_MSG(frm_sci,"Null input sci frame set!Exit");
  SINFO_ASSURE_NOT_NULL_MSG(frm_cat,"Null input std star cat frame set!Exit");
  SINFO_ASSURE_NOT_NULL_MSG(frm_atmext,"Null input atmospheric ext frame set!Exit");


  check_nomsg(next = cpl_frame_get_nextensions(frm_sci));
  //sinfo_msg("next=%d",next);
  nord=(next+1)/3;

  sinfo_frame_sci_get_ra_dec_airmass(frm_sci,&dRA,&dDEC,&airmass);
  name_sci=cpl_frame_get_filename(frm_sci);

  sinfo_msg_warning("name_sci = %s", name_sci ? name_sci : "NULL");

  plist=cpl_propertylist_load(name_sci,0);
  sinfo_frame_sci_get_gain_airmass_exptime_naxis1_biny(frm_sci,band,
                                                     &gain,&airmass,&exptime,
                                                     &naxis1,&biny);



  cpl_error_code fail = (sinfo_parse_catalog_std_stars(frm_cat,dRA,dDEC,STAR_MATCH_DEPSILON,
                                    &tbl_ref,&std_star_id));

  if(fail){
      sinfo_msg_error("Unable to parse the star catalog");
      sinfo_free_propertylist(&plist);
      return NULL;
  }


  /*
  sinfo_msg_dbg_medium("gain=%g airm=%g exptime=%g airmass=%g ra=%g dec=%g",
          gain,airmass,exptime,airmass,dRA,dDEC);

  sinfo_msg_dbg_medium("name_sci=%s",name_sci);
  */
  nrow=naxis1*nord;
  /* note that this table is overdimensioned: naxis1 is the size of the multi
     extension image holding the 2D spectrum (usually order by order)
     but orders may have different length==> different naxis1
  */
  obj_tab=cpl_table_new(nrow);


  cpl_table_new_column(obj_tab,COL_NAME_ORD_OBJ,CPL_TYPE_INT);
  cpl_table_new_column(obj_tab,COL_NAME_WAVE_OBJ,CPL_TYPE_DOUBLE);
  cpl_table_new_column(obj_tab,COL_NAME_INT_OBJ,CPL_TYPE_DOUBLE);

  check_nomsg(cpl_table_fill_column_window_int(obj_tab,COL_NAME_ORD_OBJ,0,nrow,-1));
  check_nomsg(cpl_table_fill_column_window_double(obj_tab,COL_NAME_WAVE_OBJ,0,nrow,-1));
  check_nomsg(cpl_table_fill_column_window_double(obj_tab,COL_NAME_INT_OBJ,0,nrow,-1));



  /* extract the object signal on a given slit
     extract the sky signal on 2 given slits
     computes the extracted sky per pixel on each sky window
     average the two extracted sky
     rescale the averaged sky to the object windows and
     subtract it from the object
  */

  check_nomsg(name_atm=cpl_frame_get_filename(frm_atmext));
  //sinfo_msg_dbg_medium("name_atm=%s",name_atm);
  check_nomsg(tbl_atmext=cpl_table_load(name_atm,1,0));

  if(!cpl_table_has_column(tbl_atmext,SINFO_ATMOS_EXT_LIST_COLNAME_K)){
     sinfo_msg_warning("You are using an obsolete atm extinction line table");
     cpl_table_duplicate_column(tbl_atmext,SINFO_ATMOS_EXT_LIST_COLNAME_K,
                                tbl_atmext,SINFO_ATMOS_EXT_LIST_COLNAME_OLD);
  }



  if(tbl_ref == NULL) {
    sinfo_msg_error("Provide std sar catalog frame");
    return NULL;
  //cpl_table_dump(tbl_ref,0,3,stdout);
  }

  double factor;

  /* Trick to make it work with SINFONI data */
  if(band == INSTRUMENT_BAND_UVB ||
     band == INSTRUMENT_BAND_VIS ||
     band == INSTRUMENT_BAND_NIR )
  {
      factor=1.; /*XSHOOTER*/
  } else {
     next=1; /*SINFONI*/
     nord=1;
     factor=1000.;
  }
  int crpix1;
  for(i=0;i<next;i+=3) {

     sinfo_msg_warning("Processing extracted spectrum extension %d",i);
     cpl_vector_delete(vec_ord);
     pobj=NULL;
     sinfo_free_propertylist(&x_plist);
     check_nomsg(vec_ord=cpl_vector_load(name_sci,i));
     check_nomsg(x_plist=cpl_propertylist_load(name_sci,i));
     vec_size=cpl_vector_get_size(vec_ord);

     //sinfo_msg("naxis1=%d vec_ord nx=%d",naxis1,vec_size);
     check_nomsg(pobj=cpl_vector_get_data(vec_ord));
     check_nomsg(crval1=sinfo_pfits_get_crval1(x_plist));
     check_nomsg(cdelt1=sinfo_pfits_get_cdelt1(x_plist));
     check_nomsg(crpix1=sinfo_pfits_get_crpix1(x_plist));
     sinfo_free_table(&tbl_ord);
     tbl_ord=cpl_table_new(vec_size);
     check_nomsg(cpl_table_copy_structure(tbl_ord,obj_tab));
     check_nomsg(cpl_table_fill_column_window_int(tbl_ord,COL_NAME_ORD_OBJ,0,vec_size,-1));
     check_nomsg(cpl_table_fill_column_window_double(tbl_ord,COL_NAME_WAVE_OBJ,0,vec_size,-1));
     check_nomsg(cpl_table_fill_column_window_double(tbl_ord,COL_NAME_INT_OBJ,0,vec_size,-1));

     check_nomsg(po=cpl_table_get_data_int(tbl_ord,COL_NAME_ORD_OBJ));
     check_nomsg(pw=cpl_table_get_data_double(tbl_ord,COL_NAME_WAVE_OBJ));
     check_nomsg(pf=cpl_table_get_data_double(tbl_ord,COL_NAME_INT_OBJ));


     sinfo_msg_warning("crval1=%g cdelt1=%g",crval1,cdelt1);
     ord=i/3;
     for(j=0;j<vec_size;j++) {
        po[j]=ord;
        wav=crval1+cdelt1*(j-crpix1);
        pw[j]=wav*factor;
        pf[j]=pobj[j];
        //sinfo_msg("j=%d Flux[%g]=%g",j,pw[j],pobj[j]);
     }

     //cpl_table_dump(tbl_ord,0,3,stdout);
     sprintf(name_eff,"obj_ord%d.fits",ord);
     cpl_table_save(tbl_ord,plist, x_plist,name_eff, CPL_IO_DEFAULT);

     sprintf(name_eff,"ref_ord%d.fits",ord);
     cpl_table_save(tbl_ref,plist, x_plist,name_eff, CPL_IO_DEFAULT);

     cpl_table_save(tbl_atmext,plist, x_plist,"table_atm.fits", CPL_IO_DEFAULT);

     sinfo_free_table(&tbl_eff);
     check_nomsg(tbl_eff=sinfo_utils_efficiency_internal(tbl_ord,tbl_atmext,tbl_ref,
                                                 exptime,airmass,aimprim,gain,
                                                 biny,nm2nm,
                                                 COL_NAME_WAVE_ATMDISP,
                                                 COL_NAME_ABS_ATMDISP,
                                                 COL_NAME_WAVE_REF,
                                                 COL_NAME_FLUX_REF,
                                                 COL_NAME_BINWIDTH_REF,
                                                 COL_NAME_WAVE_OBJ,
                                                 COL_NAME_INT_OBJ,
                                                 &ntot,&nclip));


     sinfo_free_table(&tbl_ord);
     //cpl_table_dump(tbl_eff,1,2,stdout);
     nrow=cpl_table_get_nrow(tbl_eff);
     if(nrow>0) {
        check_nomsg(emax=cpl_table_get_column_max(tbl_eff,"EFF"));
     } else {
        emax=-999;
     }
     sprintf(key_name,"%s%2d", SINFO_QC_EFF_PEAK_ORD,ord);
     cpl_propertylist_append_double(plist,key_name,emax);
     cpl_propertylist_set_comment(plist,key_name,"Peak efficiency");
     if(nrow>0) {
       check_nomsg(emed=cpl_table_get_column_median(tbl_eff,"EFF"));
     } else {
        emed=-999;
     }
     sprintf(key_name,"%s%2d", SINFO_QC_EFF_MED_ORD,ord);
     cpl_propertylist_append_double(plist,key_name,emed);
     cpl_propertylist_set_comment(plist,key_name,"Median efficiency");


     neff_tot+=ntot;
     //sinfo_msg("neff_tot=%d vec_size=%d",neff_tot,vec_size);
     if(ord==0) {
        tot_eff=cpl_table_duplicate(tbl_eff);
     } else {
        cpl_table_insert(tot_eff,tbl_eff,neff_tot);
     }
     nclip_tot+=nclip;

     //sprintf(name_eff,"eff_ord%d.fits",ord);
     //check(cpl_table_save(tbl_eff,plist, x_plist,name_eff, CPL_IO_DEFAULT));
  }

  //abort();

  sinfo_msg("nclip_tot=%d",nclip_tot);
  HIGH_ABS_REGION * phigh=NULL;
  phigh=sinfo_fill_high_abs_regions(band,high_abs_win);

  check_nomsg(sinfo_efficiency_add_high_abs_regions(&tot_eff,phigh));
  /*
  sprintf(tag,"EFFICIENCY_%s_%s",sinfo_instrument_mode_tostring(band),
          sinfo_instrument_arm_tostring(band));
          */
  //AMO: changed from XSHOOTER

  const char * nm = name_sci + strlen(name_sci);

  while(*nm != '/') nm--;

  nm++;

  sprintf(tag,"EFFICIENCY_%s",nm);
  sprintf(fname,"%s",tag);

  nrow=cpl_table_get_nrow(tot_eff);
  fclip=nclip_tot;

  if(neff_tot > 0) {
     fclip/=neff_tot;
  } else {
     fclip=-999;
  }

  sinfo_msg("nclip_tot =%d neff_tot=%d fclip=%g",nclip_tot,neff_tot,fclip);
  sinfo_pfits_set_qc_eff_fclip(plist,fclip);
  sinfo_pfits_set_qc_eff_nclip(plist,nclip_tot);

  //cpl_propertylist_dump(plist,stdout);
  check_nomsg(cpl_table_save(tot_eff,plist, x_plist,fname, CPL_IO_DEFAULT));

  sinfo_free_table(&obj_tab);

  check_nomsg(frm_eff=sinfo_frame_product(fname,tag,CPL_FRAME_TYPE_TABLE,
                                  CPL_FRAME_GROUP_CALIB,
                                  CPL_FRAME_LEVEL_FINAL));



  cleanup:

 //if(phigh!=NULL) cpl_free(phigh);


  sinfo_free_table(&tot_eff);
  sinfo_free_table(&tbl_ref);
  sinfo_free_table(&tbl_atmext);
  sinfo_free_table(&obj_tab);
  sinfo_free_table(&tbl_ord);
  sinfo_free_table(&tbl_eff);
  sinfo_free_image(&ima_sci);
  cpl_vector_delete(rec_profile);
  cpl_vector_delete(vec_ord);
  sinfo_free_image(&ima_obj);
  sinfo_free_propertylist(&plist);
  sinfo_free_propertylist(&x_plist);

  return frm_eff;

}


cpl_frame* sinfo_compute_efficiency(cpl_frame* mer1D, cpl_frame* std_cat,
                  cpl_frame* atm_ext, cpl_frame* high_abs_win,
                       new_sinfo_band band)
{

  cpl_frame* eff=NULL;

  if(NULL == (eff=sinfo_efficiency_compute(mer1D,std_cat,atm_ext,high_abs_win,band))) {
    sinfo_msg_error("An error occurred during efficiency computation");
    sinfo_msg_error("The recipe recovers without efficiency product generation");
    cpl_error_reset();
  }

 cleanup:
 return eff;
}

void
sinfo_compute_efficiency_drive(const char* res1D_name, const char* std_cat_name,
                   const char* atm_ext_name, const char* high_abs_name,
                   new_sinfo_band band)
{

    cpl_test(CPL_ERROR_NONE == CPL_ERROR_NONE);
    cpl_frame * obs_std_star = NULL;
    cpl_frame * flux_std_star_cat = NULL;
    cpl_frame * atmos_ext = NULL;
    cpl_frame* high_abs = NULL;
    cpl_frame* resp_fit_points = NULL;
    cpl_frame* tell_mod_cat = NULL;


    obs_std_star=sinfo_frame_product(res1D_name, "STD_STAR_SPECTRA",
                     CPL_FRAME_TYPE_TABLE, CPL_FRAME_GROUP_PRODUCT,
                     CPL_FRAME_LEVEL_FINAL);

    flux_std_star_cat=sinfo_frame_product(std_cat_name, "FLUX_STD_CATALOG_NIR",
                    CPL_FRAME_TYPE_TABLE, CPL_FRAME_GROUP_PRODUCT,
                    CPL_FRAME_LEVEL_FINAL);

    atmos_ext = sinfo_frame_product(atm_ext_name, "ATMOS_EXT_NIR",
                    CPL_FRAME_TYPE_TABLE, CPL_FRAME_GROUP_PRODUCT,
                    CPL_FRAME_LEVEL_FINAL);

    if(high_abs_name!= NULL) {
        high_abs=sinfo_frame_product(high_abs_name, "ATMOS_EXT_NIR",
                        CPL_FRAME_TYPE_TABLE, CPL_FRAME_GROUP_PRODUCT,
                        CPL_FRAME_LEVEL_FINAL);
    }
    cpl_propertylist* plist = cpl_propertylist_load(res1D_name,0);
    char test_band[10];
    char instrume[10];
    const char * strmen = cpl_propertylist_get_string(plist, "INSTRUME");
    strcpy(instrume, strmen);

    if (!strcmp(instrume, "SINFONI"))  {
        strcpy(test_band, cpl_propertylist_get_string(plist, KEY_NAME_FILT_NAME));
        if (!strcmp(test_band, "J"))  {
            band=INSTRUMENT_BAND_J ;
        } else if (!strcmp(test_band, "H"))  {
            band=INSTRUMENT_BAND_H ;
        } else if (!strcmp(test_band, "K"))  {
            band=INSTRUMENT_BAND_K ;
        } else   {
            band=INSTRUMENT_BAND_L ;
        }
    } else if (!strcmp(instrume, "XSHOOTER"))  {
        strcpy(test_band, cpl_propertylist_get_string(plist, "ESO SEQ ARM"));
               if (!strcmp(test_band, "UVB"))  {
                   band=INSTRUMENT_BAND_UVB ;
        } else if (!strcmp(test_band, "VIS"))  {
            band=INSTRUMENT_BAND_VIS ;
        } else if (!strcmp(test_band, "NIR"))  {
            band=INSTRUMENT_BAND_NIR ;
        }
    }
    sinfo_free_propertylist(&plist);
    cpl_frame* eff=NULL;
    eff=sinfo_compute_efficiency(obs_std_star, flux_std_star_cat,
                                 atmos_ext, high_abs, band);


    sinfo_free_frame(&obs_std_star);
    sinfo_free_frame(&flux_std_star_cat);
    sinfo_free_frame(&atmos_ext);
    //sinfo_free_frame(&high_abs);
    sinfo_free_frame(&resp_fit_points);
    sinfo_free_frame(&tell_mod_cat);
    sinfo_free_frame(&eff);

}

int main(int argc, char** argv)
{
  cpl_test_init(SINFO_EFFICIENCY_BUG_REPORT, CPL_MSG_WARNING);

  const char * res1Deso_eff_frame_name = NULL;
  const char * frm_std_cat_name = NULL;

  const char * atm_ext_name = NULL;
  const char * high_abs_name = NULL;

  new_sinfo_band band = SINFO_BAND_J;

  /* Analyse parameters */
  sinfo_msg_warning("argc=%d",argc);
  if ( argc == 3) {
      sinfo_ima2table(argv);
  } else if ( argc > 3) {
    res1Deso_eff_frame_name = argv[1];
    frm_std_cat_name = argv[2];
    atm_ext_name = argv[3];
    if ( argc > 4) {
        high_abs_name = argv[4];
    }
    sinfo_msg_warning("res1Deso_eff_frame_name=%s frm_std_cat_name=%s high_abs_name=%s atm_ext_name=%s",
                      res1Deso_eff_frame_name,frm_std_cat_name, high_abs_name,atm_ext_name);

    sinfo_compute_efficiency_drive(res1Deso_eff_frame_name, frm_std_cat_name,
                             atm_ext_name, high_abs_name, band);

    sinfo_msg_warning("Execution terminated!");


  }
  else{
    printf(SYNTAX);
    return cpl_test_end(0);

  }

}

/**@}*/
