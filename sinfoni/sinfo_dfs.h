/* $Id: sinfo_dfs.h,v 1.3 2010-02-17 09:23:43 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2010-02-17 09:23:43 $
 * $Revision: 1.3 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_DFS_H
#define SINFO_DFS_H

/*--------------------------------------------------------------------------
                                 general Includes
 --------------------------------------------------------------------------*/
#include <config.h>
#include <string.h>
#include <cpl.h>
#include "sinfo_pfits.h"
/* #include "utilities.h" */   /* critical for other modules */
/*------------------------------------------------------------------------
                                   Defines
 --------------------------------------------------------------------------*/
#include "sinfo_globals.h"
#include "sinfo_raw_types.h"
#include "sinfo_ref_types.h"
#include "sinfo_pro_types.h"
#include "sinfo_key_names.h"
/* ---------------------------------------------------------------------- 
   functions
---------------------------------------------------------------------- */
#include "sinfo_functions.h"
#endif
