/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name     :    sinfo_function_1d.h
   Author         :    Nicolas Devillard
   Created on    :    Tue, Sept 23 1997    
   Description    :    1d signal processing related routines    
 ---------------------------------------------------------------------------*/
/*
 $Id: sinfo_function_1d.h,v 1.6 2008-03-25 08:20:43 amodigli Exp $
 $Author: amodigli $
 $Date: 2008-03-25 08:20:43 $
 $Revision: 1.6 $
 */
#ifndef SINFO_FUNCTION_1D_H
#define SINFO_FUNCTION_1D_H
/*----------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <math.h>
#include "sinfo_local_types.h"
#include "sinfo_pixel_handling.h"
#include "sinfo_msg.h"
#include <cpl.h>
/*----------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/* Low pass filter types: */

#define LOW_PASS_LINEAR            100
#define LOW_PASS_GAUSSIAN        101
/*----------------------------------------------------------------------------
                          Function ANSI C prototypes
 ---------------------------------------------------------------------------*/
/**
  @name     sinfo_function1d_new
  @memo     Allocates a new array of pixelvalues.
  @param    nsamples    Number of values to store in the array.
  @return   Pointer to newly allocated array of pixelvalues.
  @doc
 
  The returned array must be freed using sinfo_function1d_del(), not free().
  This is in case some further housekeeping attributes are allocated
  together with the object in the future.
 
  Returns NULL in case of error.
 
 */

pixelvalue * 
sinfo_function1d_new(int nsamples);

/**
  @name     sinfo_function1d_del
  @memo     Deallocate an array of pixelvalues.
  @param    s   Array to deallocate.
  @return   void
  @doc
 
  Deallocates an array allocated by sinfo_function1d_new().
 */
void 
sinfo_function1d_del(pixelvalue * s);
/**
  @name     sinfo_function1d_dup
  @memo     Copy an array of pixelvalues to a new array.
  @param    arr     Array to copy.
  @param    ns      Number of samples in the array.
  @return   Pointer to newly allocated array of pixelvalues.
  @doc
 
  Creates a new array using sinfo_function1d_new(), with the same number of
  samples as the input signal, then copies over all values from source
  to destination array using memcpy().
 
  The returned array must be freed using sinfo_function1d_del(), not free().
 
 */

pixelvalue * 
sinfo_function1d_dup(pixelvalue * arr, int ns);

/**
  @name     sinfo_function1d_filter_lowpass
  @memo     Apply a low-pass filter to a 1d signal.
  @param    input_sig   Input signal
  @param    samples     Number of samples in the signal
  @param    filter_type Type of filter to use.
  @param    hw          Filter half-width.
  @return   Pointer to newly allocated array of pixels.
  @doc
 
  This kind of low-pass filtering consists in a convolution with a
  given kernel. The chosen filter type determines the kind of kernel
  to apply for convolution. Possible kernels and associated symbols
  can be found in function_1d.h.
 
  Smoothing the signal is done by applying this kind of low-pass
  filter several times.
 
  The returned smooth signal has been allocated using
  sinfo_function1d_new(), it must be freed using sinfo_function1d_del(). The
  returned signal has exactly as many samples as the input signal.
 
 */

pixelvalue *
sinfo_function1d_filter_lowpass(
    pixelvalue  *   input_sig,
    int             samples,
    int             filter_type,
    int             hw
) ;



/**
  @name     sinfo_function1d_interpolate_linear
  @memo     Linear signal interpolation.
  @param    x       Input list of x positions.
  @param    y       Input list of y positions.
  @param    len     Number of samples in x and y.
  @param    spl_x   List of abscissas where the signal must be computed.
  @param    spl_y   Output list of computed signal values.
  @param    spl_len Number of samples in spl_x and spl_y.
  @return   void
  @doc
 
  To apply this interpolation, you need to provide a list of x and y
  positions, and a list of x positions where you want y to be computed
  (with linear interpolation).
 
  The returned signal has spl_len samples. It has been allocated using
  sinfo_function1d_new() and must be deallocated using sinfo_function1d_del().
 
 */

void 
sinfo_function1d_interpolate_linear(
    pixelvalue  *   x,
    pixelvalue  *   y,
    int             len,
    pixelvalue  *   spl_x,
    pixelvalue  *   spl_y,
    int             spl_len
);


/**
  @name     sinfo_function1d_natural_spline
  @memo     Interpolate a sinfo_vector along new abscissas.
  @param    x       List of x positions.
  @param    y       List of y positions.
  @param    len     Number of samples in x and y.
  @param    splX    Input new list of x positions.
  @param    splY    Output list of interpolated y positions.
  @param    splLen  Number of samples in splX and splY.
  @return   Int 0 if Ok, -1 if error.
  @doc
 
  Reference:
 
  \begin{verbatim}
    Numerical Analysis, R. Burden, J. Faires and A. Reynolds.
    Prindle, Weber & Schmidt 1981 pp 112
  \end{verbatim}
 
  Provide in input a known list of x and y values, and a list where
  you want the signal to be interpolated. The returned signal is
  written into splY.
 
 */
 
int
sinfo_function1d_natural_spline(
    pixelvalue  *   x,
    pixelvalue  *   y,
    int             len,
    pixelvalue  *   splX,
    pixelvalue  *   splY,
    int             splLen
) ;



#endif
