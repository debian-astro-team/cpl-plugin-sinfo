/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------

 File name     :    pixel_handling.c
 Author         :    Nicolas Devillard
 Created on    :    March 04, 1997
 Description    :    Functions processing arrays of pixels.

 ---------------------------------------------------------------------------*/
/*

 $Id: sinfo_pixel_handling.c,v 1.6 2012-03-03 09:50:08 amodigli Exp $
 $Author: amodigli $
 $Date: 2012-03-03 09:50:08 $
 $Revision: 1.6 $

 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_pixel_handling.h"
/**@{*/
/**
 * @addtogroup sinfo_utilities  Pixel handling
 *
 * TBD
 */

/*---------------------------------------------------------------------------
 Function codes
 ---------------------------------------------------------------------------*/
/**
 @name        sinfo_pixel_qsort
 @memo        Sort an array of pixels by increasing pixelvalue.
 @param    pix_arr        Array to sort.
 @param    npix        Number of pixels in the array.
 @return    void
 @doc

 Optimized implementation of a fast pixel sort. The input array is
 modified.
 */
#define PIX_SWAP(a,b) { pixelvalue temp=(a);(a)=(b);(b)=temp; }
#define PIX_STACK_SIZE 50

void
sinfo_pixel_qsort(pixelvalue *pix_arr, int npix)
{
    int i, ir, j, k, l;
    int i_stack[PIX_STACK_SIZE * sizeof(pixelvalue)];
    int j_stack;
    pixelvalue a;

    ir = npix;
    l = 1;
    j_stack = 0;
    for (;;) {
        if (ir - l < 7) {
            for (j = l + 1; j <= ir; j++) {
                a = pix_arr[j - 1];
                for (i = j - 1; i >= 1; i--) {
                    if (pix_arr[i - 1] <= a)
                        break;
                    pix_arr[i] = pix_arr[i - 1];
                }
                pix_arr[i] = a;
            }
            if (j_stack == 0)
                break;
            ir = i_stack[j_stack-- - 1];
            l = i_stack[j_stack-- - 1];
        }
        else {
            k = (l + ir) >> 1;
            PIX_SWAP(pix_arr[k - 1], pix_arr[l])
            if (pix_arr[l] > pix_arr[ir - 1]) {
                PIX_SWAP(pix_arr[l], pix_arr[ir - 1])
            }
            if (pix_arr[l - 1] > pix_arr[ir - 1]) {
                PIX_SWAP(pix_arr[l - 1], pix_arr[ir - 1])
            }
            if (pix_arr[l] > pix_arr[l - 1]) {
                PIX_SWAP(pix_arr[l], pix_arr[l - 1])
            }
            i = l + 1;
            j = ir;
            a = pix_arr[l - 1];
            for (;;) {
                do
                    i++;
                while (pix_arr[i - 1] < a);
                do
                    j--;
                while (pix_arr[j - 1] > a);
                if (j < i)
                    break;
                PIX_SWAP(pix_arr[i - 1], pix_arr[j - 1]);
            }
            pix_arr[l - 1] = pix_arr[j - 1];
            pix_arr[j - 1] = a;
            j_stack += 2;
            if (j_stack > PIX_STACK_SIZE) {
                sinfo_msg_error("stack too small : aborting");
                exit(-2001);
            }
            if (ir - i + 1 >= j - l) {
                i_stack[j_stack - 1] = ir;
                i_stack[j_stack - 2] = i;
                ir = j - 1;
            }
            else {
                i_stack[j_stack - 1] = j - 1;
                i_stack[j_stack - 2] = l;
                l = i;
            }
        }
    }
}
#undef PIX_STACK_SIZE
#undef PIX_SWAP

/**@}*/
