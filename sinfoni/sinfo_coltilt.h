#ifndef SINFO_COLTILT_H
#define SINFO_COLTILT_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_coltilt.h,v 1.4 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  20/12/00  created
*/

/************************************************************************
 * sinfo_coltilt.h
 * routines to calculate and correct the spatial tilt of spectra in raw images
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h> 
#include "sinfo_msg.h"
#include "sinfo_recipes.h"
#include "sinfo_wave_calibration.h"
#include "sinfo_spiffi_types.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/**
@name sinfo_new_image_warp_fits
@memo correct optical distortions
@param image distorted image
@param kernel_type type of kernel to correct distortions
@param poly_table table containing distortion coefficients
@return image distortion corrected
*/
cpl_image * 
sinfo_new_image_warp_fits(cpl_image * image,
                      const char      * kernel_type,
                      const char      * poly_table );

#endif /*!SINFO_COLTILT_H*/

