/* $Id: sinfo_wavecal_config.h,v 1.1 2006-10-20 08:06:33 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2006-10-20 08:06:33 $
 * $Revision: 1.1 $
 * $Name: not supported by cvs2svn $
 */

 /****************************************************************
  *   Wavecal Frames Data Reduction Parameter Initialization        *
  ****************************************************************/
#include <cpl.h>    /* defines parlist structure */
void
sinfo_wavecal_config_add(cpl_parameterlist *list);
