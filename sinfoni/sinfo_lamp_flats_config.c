/* $Id: sinfo_lamp_flats_config.c,v 1.4 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.4 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/****************************************************************
 *   Lamp_Spec Frames Data Reduction Parameter Initialization        *
 ****************************************************************/

#include "sinfo_lamp_flats_config.h"
/**@{*/
/**
 * @addtogroup sinfo_lamp_cfg Flat frame manipulation functions
 *
 * TBD
 */

void
sinfo_lamp_flats_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /*Reconstruction */
    /* the fraction [0...1] of rejected low intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.low_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection: "
                    "percentage of rejected low intensity "
                    "pixels before averaging",
                    "sinfoni.lamp_flats",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-lo_rej");
    cpl_parameterlist_append(list, p);

    /* the fraction [0...1] of rejected high intensity pixels when taking
    the average of columns */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.high_rejection",
                    CPL_TYPE_DOUBLE,
                    "high rejection: "
                    "percentage of rejected high intensity "
                    "pixels before averaging",
                    "sinfoni.lamp_flats",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-hi_rej");
    cpl_parameterlist_append(list, p);


    /* indicator if the bad pixels of the flatfield should be interpolated */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.interpol_index",
                    CPL_TYPE_BOOL,
                    "Interpolation index switch: "
                    "indicator if the bad pixels of the flatfield "
                    "should be interpolated",
                    "sinfoni.lamp_flats",
                    FALSE);


    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,
                            "lamp_flats-interpol_index");
    cpl_parameterlist_append(list, p);


    /* maximal pixel distance from bad pixel to take valid pixels */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.max_rad",
                    CPL_TYPE_INT,
                    "Max Rad: "
                    "maximal pixel distance from bad pixel "
                    "to take valid pixels",
                    "sinfoni.lamp_flats",
                    4);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-max_rad");
    cpl_parameterlist_append(list, p);


    /*  indicator if a bad pixel mask should be generated or not */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.bad_ind",
                    CPL_TYPE_BOOL,
                    "indicator if a bad pixel mask should be "
                    "generated or not",
                    "sinfoni.lamp_flats",
                    FALSE);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-bad_ind");
    cpl_parameterlist_append(list, p);

    /*
   factor of the sigma noise limit; to remove the column intensity tilt only 
   pixels which lie within a defined noise limit are used to fit a straight 
   line
     */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.sigma_factor",
                    CPL_TYPE_DOUBLE,
                    "Sigma Factor: "
                    "factor of the sigma noise limit; "
                    "to remove the column intensity tilt only "
                    "pixels which lie within a defined noise "
                    "limit are used to fit a straight line",
                    "sinfoni.lamp_flats",
                    5.);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-sigma_factor");
    cpl_parameterlist_append(list, p);

    /*
   if |pixel - sinfo_median| > factor * standard deviation -> then the
    pixel value is replaced by the median of the 8 nearest neighbors
     */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.factor",
                    CPL_TYPE_DOUBLE,
                    "Factor: "
                    "if |pixel - median| > factor * standard deviation -> "
                    "then the pixel value is replaced by the median of the 8 "
                    "nearest neighbors",
                    "sinfoni.lamp_flats",
                    3.);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-factor");
    cpl_parameterlist_append(list, p);

    /* number of iterations to of sinfo_median filtering to find bad
     pixel clusters */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.iterations",
                    CPL_TYPE_INT,
                    "Iterations: "
                    "number of iterations to of median filtering "
                    "to find bad pixel clusters",
                    "sinfoni.lamp_flats",
                    8);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-iterations");
    cpl_parameterlist_append(list, p);

    /* percentage of rejected low intensity pixels before averaging */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.bad_low_rejection",
                    CPL_TYPE_DOUBLE,
                    "low rejection: "
                    "Percentage for bad pixel low rejection",
                    "sinfoni.lamp_flats",
                    10.,0.,100.);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-bad_lo_rej");
    cpl_parameterlist_append(list, p);

    /* percentage of rejected high intensity pixels before averaging */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.bad_high_rejection",
                    CPL_TYPE_DOUBLE,
                    "high rejection: "
                    "Percentage for bad pixel high rejection",
                    "sinfoni.lamp_flats",
                    10.,0.,100.);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-bad_hi_rej");
    cpl_parameterlist_append(list, p);


    /* to compute image statistics on a rectangular zone of the image
     the coordinates of the rectangle are needed
     */
    /* lower left x coordinate */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.llx",
                    CPL_TYPE_INT,
                    "Lower Lext X corner",
                    "sinfoni.lamp_flats",
                    1350,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-llx");
    cpl_parameterlist_append(list, p);

    /* lower left y coordinate */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.lly",
                    CPL_TYPE_INT,
                    "Lower Lext Y corner",
                    "sinfoni.lamp_flats",
                    1000,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-lly");
    cpl_parameterlist_append(list, p);

    /* upper right x coordinate */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.urx",
                    CPL_TYPE_INT,
                    "Upper right X corner",
                    "sinfoni.lamp_flats",
                    1390,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-rrx");
    cpl_parameterlist_append(list, p);

    /* upper right y coordinate */
    p = cpl_parameter_new_range("sinfoni.lamp_flats.ury",
                    CPL_TYPE_INT,
                    "Upper right Y corner",
                    "sinfoni.lamp_flats",
                    1200,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-ury");
    cpl_parameterlist_append(list, p);

    /* indicator that indicates if the values beyond a threshold deviation */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.thresh_ind",
                    CPL_TYPE_BOOL,
                    "Treshold index: ",
                    "sinfoni.lamp_flats",
                    FALSE);


    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-tresh_ind");
    cpl_parameterlist_append(list, p);

    /*
  factor to the clean standard deviation to define the threshold deviation
  from the clean mean
     */
    p = cpl_parameter_new_value("sinfoni.lamp_flats.mean_factor",
                    CPL_TYPE_DOUBLE,
                    "Mean Factor: "
                    "factor to the clean standard deviation to "
                    "define the threshold deviation "
                    "from the clean mean",
                    "sinfoni.lamp_flats",
                    10.);

    cpl_parameter_set_alias(p,  CPL_PARAMETER_MODE_CLI,"lamp_flats-mean_factor");
    cpl_parameterlist_append(list, p);


    /* QC LOG */
    /* FPN */


    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_xmin1",
                    CPL_TYPE_INT,
                    "qc_fpn_xmin1",
                    "sinfoni.lamp_flats",
                    512,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_fpn_xmin1");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_xmax1",
                    CPL_TYPE_INT,
                    "qc_fpn_xmax1",
                    "sinfoni.lamp_flats",
                    1536,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_fpn_xmax1");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_ymin1",
                    CPL_TYPE_INT,
                    "qc_fpn_ymin1",
                    "sinfoni.lamp_flats",
                    512,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_fpn_ymin1");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_ymax1",
                    CPL_TYPE_INT,
                    "qc_fpn_ymax1",
                    "sinfoni.lamp_flats",
                    1536,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_fpn_ymax1");
    cpl_parameterlist_append(list, p);






    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_xmin2",
                    CPL_TYPE_INT,
                    "qc_fpn_xmin2",
                    "sinfoni.lamp_flats",
                    1350,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p,CPL_PARAMETER_MODE_CLI, "lamp_flats-qc_fpn_xmin2");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_xmax2",
                    CPL_TYPE_INT,
                    "qc_fpn_xmax2",
                    "sinfoni.lamp_flats",
                    1390,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_fpn_xmax2");
    cpl_parameterlist_append(list, p);

    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_ymin2",
                    CPL_TYPE_INT,
                    "qc_fpn_ymin2",
                    "sinfoni.lamp_flats",
                    1000,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_fpn_ymin2");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_range("sinfoni.lamp_flats.qc_fpn_ymax2",
                    CPL_TYPE_INT,
                    "qc_fpn_ymax2",
                    "sinfoni.lamp_flats",
                    1200,DET_PIX_MIN,DET_PIX_MAX);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_fpn_ymax2");
    cpl_parameterlist_append(list, p);





    p = cpl_parameter_new_value("sinfoni.lamp_flats.qc_thresh_min",
                    CPL_TYPE_INT,
                    "qc_thresh_min",
                    "sinfoni.lamp_flats",
                    0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_thresh_min");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.lamp_flats.qc_thresh_max",
                    CPL_TYPE_INT,
                    "qc_thresh_max",
                    "sinfoni.lamp_flats",
                    49000);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_flats-qc_thresh_max");
    cpl_parameterlist_append(list, p);


}
/**@}*/
