/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_baddist_ini_by_cpl.h
   Author       :   Andrea Modigliani
   Created on   :   Jun 16, 2004
   Description  :   parse cpl input for the search for static bad pixels

 ---------------------------------------------------------------------------*/


#ifndef SINFO_BADDIST_INI_BY_CPL_H
#define SINFO_BADDIST_INI_BY_CPL_H


/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_msg.h"
#include "sinfo_bad_cfg.h"
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/
 
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/

/**
@name sinfo_bad_free
@memo free a bad_config object
@param cfg pointer to bad_config structure
@return void
*/
void 
sinfo_bad_free(bad_config * cfg);

/**
  @name     sinfo_parse_cpl_input_badnorm
  @memo     Parse input from CPL (parameters and set of input frames) 
            to create a blackboard.
  @param    config    cpl parameter list
  @param    sof       cpl frames list
  @return   1 newly allocated bad_config blackboard structure.
  @doc

  The requested cpl input is parsed and a blackboard object is created, then
  updated accordingly. Returns NULL in case of error.
 */

bad_config * 
sinfo_parse_cpl_input_baddist(cpl_parameterlist * cpl_cfg, 
                              cpl_frameset* sof, 
                              const char* procatg, 
                              cpl_frameset** raw);

#endif
