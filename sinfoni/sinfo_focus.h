#ifndef SINFO_FOCUS_H
#define SINFO_FOCUS_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_focus.h,v 1.3 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  04/02/02  created
*/

/************************************************************************
 * sinfo_focus.h
 * some functions to fit a 2-D Gaussian for focus finding
 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <cpl.h>
#include "sinfo_utilities.h"
#include "sinfo_new_cube_ops.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/
double 
sinfo_new_gaussian_ellipse(double * xdat, double * parlist) ;

void 
sinfo_new_gaussian_ellipse_deriv( double * xdat, 
                                  double * parlist, 
                                  double * dervs ) ;

int 
sinfo_new_lsqfitd ( double * xdat,
              int    * xdim,
              double * ydat,
              double * wdat,
              int    * ndat,
              double * fpar,
              double * epar,
              int    * mpar,
              int    * npar,
              double * tol ,
              int    * its ,
              double * lab  ) ;


int 
sinfo_new_fit_2d_gaussian ( cpl_image   * image,
                    double     * fit_par,
                    double     * derv_par,
                    int        * mpar,
                    int          lleftx,
                    int          llefty,
                    int          halfbox_x,
                    int          halfbox_y, int* check ) ;

float 
sinfo_new_determine_conversion_factor ( cpl_imagelist * cube,
                                  float     mag,
                                  float     exptime,
                                  int       llx,
                                  int       lly,
                                  int       halfbox_x,
                                  int       halfbox_y, int* check ) ;

#endif /*!SINFO_FOCUS_H*/

