/* $Id: sinfo_hidden.h,v 1.7 2010-02-12 17:56:35 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2010-02-12 17:56:35 $
 * $Revision: 1.7 $
 * $Name: not supported by cvs2svn $
 */

 /****************************************************************
  *           Bad pixel search  (noise method)                  *
  ****************************************************************/
#ifndef SINFO_HIDDEN_H
#define SINFO_HIDDEN_H

#define BP_LIN_OUT_FILENAME               "out_bp_lin.fits"
#define BP_LIN_GAIN_OUT_FILENAME          "out_gain_info.fits"
#define BP_LIN_LIN_DET_INFO_OUT_FILENAME  "out_lin_det_info.fits"
#define BP_LIN_COEFFS_CUBE_OUT_FILENAME   "out_bplin_coeffsCube.fits"
#define BP_NOISE_OUT_FILENAME             "out_bp_noise.fits"

#define BP_NORM_INT_COL_TILT_CORR_OUT_FILENAME  "out_int_col_tilt_corr.fits"

#define DARK_OUT_FILENAME        "out_dark.fits"

#define LAMP_FLATS_OUT_FILENAME   "out_flat.fits"
#define LAMP_FLATS_OUT_BPMAP      "out_bpmask.fits"
#define BP_NORM_OUT_FILENAME      "out_bp_norm.fits"
#define MASTER_BPMAP_OUT_FILENAME "out_bpmap_sum.fits"

#define DISTORTION_NS_OUT_FILENAME "out_ns_distortion.fits"
#define DISTORTION_OUT_FILENAME    "out_distortion.fits"
#define DISTORTION_STACK_OFF_OUT_FILENAME   "out_ns_stack_off.fits"
#define DISTORTION_STACK_ON_OUT_FILENAME    "out_ns_stack_on.fits"


#define SLITPOS_OUT_FILENAME       "out_slit_pos.fits"
#define BP_DIST_OUT_FILENAME       "out_bp_dist.fits"
#define COEFF_OUT_FILENAME         "out_coef_params.fits"
#define NS_TEST_DISTANCES_OUT_FILENAME "out_distances.fits"
#define NS_TEST_OUT_FILENAME           "out_ns.fits"


#define WAVECAL_OUT_FILENAME              "out_wavemap_ima.fits"
#define WAVECAL_FIT_PARAMS_OUT_FILENAME   "out_fit_params.fits"
#define WAVECAL_FIT_PARAMS_OUT_FILEASCII   "out_fit_params.ascii"
#define WAVECAL_COEFF_SLIT_OUT_FILENAME   "outCoeffsSlit.fits"
#define WAVECAL_SLIT_POS_OUT_FILENAME     "out_slitpos.fits"
#define WAVECAL_RESAMPLED_OUT_FILENAME   "out_resampled_arclamp.fits"


#define STACKED_OUT_FILENAME                 "out_stack.fits"
#define STACK_MFLAT_DIST_OUT_FILENAME        "out_stack_mflat_dist.fits"
#define STACK_MFLAT_DITHER_DIST_OUT_FILENAME "out_stack_mflat_dither_dist.fits"

#define STACK_SKY_DIST_OUT_FILENAME             "out_sky_stack_dist"

#define ESTIMATED_SLITLETS_DISTANCE 64.


#define PSF_OUT_FILENAME                   "out_psf.fits"
#define PSF_MED_CUB_025_FILENAME           "out_med_cube_025_mas.fits"
#define PSF_MED_CUB_100_FILENAME           "out_med_cube_100_mas.fits"
#define PSF_AO_PERFORMANCE_OUT_FILENAME    "out_ao_performance.fits"
#define PSF_ENC_ENERGY_OUT_FILENAME        "out_encircled_energy.fits"
#define STDSTAR_OUT_FILENAME               "out_starspectrum.fits"
#define STDSTAR_OUT_TABLE                  "out_std_star_spectrum.fits"
#define STDSTAR_CONV_OUT_FILENAME          "out_convfactor.fits"
#define SKYPMAP_OUT_FILENAME               "out_skymap.fits"
#define NSLITLETS               32

#define OBJNOD_OUT_BPMAP        "out_objnod_bpmap.fits"
#define OBJNOD_OUT_MED_CUBE     "out_objnod_med_cube.fits"
#define OBJNOD_OUT_FILENAME     "out_objnod.fits"

#define OBJNOD_OUT_MFLAT_CUBE_FILENAME     "out_mflat_cube.fits"
#define OBJNOD_OUT_MFLAT_AVG_FILENAME      "out_mflat_avg.fits"
#define OBJNOD_OUT_MFLAT_MED_FILENAME      "out_mflat_med.fits"
#define RESAMPLED_OUT_OBJ_FILENAME         "out_resampled_obj"
#define RESAMPLED_OUT_SKY_FILENAME         "out_resampled_sky"
#define RESAMPLED_OUT_FLAT_FILENAME        "out_resampled_flat"


#define STDSTAR_OUT_MED_CUBE    "out_stdstar_med_cube.fits"

#define FOCUS_OUT_FILENAME             "out_focus.fits"
#define FOCUS_FITPAR_OUT_FILENAME      "out_focus_fitpar.fits"
#define FOCUS_GAUSSPLOT_OUT_FILENAME   "out_focus_gaussplot.fits"

#define LAMPSPEC_OUT_FILENAME "out_lampspec.fits"
#define TWIFLAT_OUT_FILENAME  "out_twiflat.fits"
#define SKYSPIDER_OUT_FILENAME "out_objnod.fits"
#define SKYSPIDER_MASK_OUT_FILENAME "out_mask_cube_spider.fits"
#define EFFICIENCY_FILENAME "out_efficiency.fits"
#define SPECTRA_FLUXCAL_FILENAME "out_spectra_fluxcal.fits"
#define RESPONSE_FILENAME "out_response.fits"
#endif
