/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_ipow.h
   Author         :    N. Devillard
   Created on    :    June 1999
   Description    :    integer powers
 *--------------------------------------------------------------------------*/

/*
 $Id: sinfo_ipow.h,v 1.3 2007-06-06 07:10:45 amodigli Exp $
 $Author: amodigli $
 $Date: 2007-06-06 07:10:45 $
 $Revision: 1.3 $
 */

#ifndef SINFO_IPOW_H
#define SINFO_IPOW_H
/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
  @name     sinfo_ipow
  @memo     Same as pow(x,y) but for integer values of y only (faster).
  @param    x   A double number.
  @param    p   An integer power.
  @return   x to the power p.
  @doc
 
  This is much faster than the math function due to the integer. Some
  compilers make this optimization already, some do not.
 
  p can be positive, negative or null.
 */

double 
sinfo_ipow(double x, int p);

#endif
