/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_ns_cfg.h
   Author    :    Juergen Schreiber
   Created on    :    November 2001
   Description    :    ns_ini definitions + handling prototypes

 ---------------------------------------------------------------------------*/
#ifndef SINFO_NS_CFG_H
#define SINFO_NS_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  data cube creation blackboard container

  This structure holds all information related to the cube creation
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct ns_config {
/*-------General---------*/
        char inList[FILE_NAME_SZ] ;  /* name of the input file list 
                                        containing the on and off-frames */ 
        char outName[FILE_NAME_SZ] ; /* output name of the ASCII list 
                                        containing the determined distances */
        char ** framelist ; /* list of frames */
        int  *  frametype ; /* list of frame types on or off */
        int     nframes ;   /* number of frames in frame list */
        int     nobj ;      /* number of object frames in frame list */
        int     noff ;      /* number of off frames in frame list */

/*------ CleanMean ------*/
        /* percentage of rejected low intensity pixels */
        float loReject ;
        /* percentage of rejected high intensity pixels */
        float hiReject ;
        /* indicator if a bad pixel mask is applied or not */
        int maskInd ;
        /* file name of the bad pixel mask fits file */
        char mask[FILE_NAME_SZ] ;
/*------ GaussConvolution ------*/
        /* indicator if Gaussian convolution is applied or not */
        int gaussInd ;
        /* kernel half width of the Gaussian response function */
        int hw ;
/*------ NorthSouthTest ------*/
        /* name of the averaged output fits frame */
        char fitsname[FILE_NAME_SZ] ;      
        /* number of slitlets */
        int nslits ;      
        /* pixel half width of a box within which the spatial profile 
           is fitted by a Gaussian */
        int halfWidth ;
        /* first guess of the fwhm of the Gaussian fit function */
        float fwhm ;
        /* minimum amplitude above which the fit is carried out */
        float minDiff ;
        /* estimated average distance of spectra */
        float estimated_dist ;
        /* maximal pixel tolerance of the slitlet distances */
        float devtol ;
} ns_config ;



/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name    sinfo_ns_cfg_create()
   @memo    allocate memory for a ns_cfg struct
   @return  pointer to allocated base ns_cfg structure
   @note    only the main (base) structure is allocated
*/

ns_config * 
sinfo_ns_cfg_create(void);
/**
   @name sinfo_ns_cfg_destroy()
   @param ns_config to deallocate
   @return  void
   @doc  deallocate all memory associated with a ns_config data structure
*/

void 
sinfo_ns_cfg_destroy(ns_config * nc);

#endif
