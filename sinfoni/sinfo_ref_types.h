/* $Id: sinfo_ref_types.h,v 1.5 2012-08-10 07:55:23 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This proram is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-08-10 07:55:23 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

#ifndef SINFO_REF_TYPES_H
#define SINFO_REF_TYPES_H
CPL_BEGIN_DECLS
/* reference frames */
#define QC_REF_FRAME                       "ESO QC REF"
#define RAW_REF                            "REF"
#define REF_LINE_ARC                       "REF_LINE_ARC"
#define REF_LINE_OH                        "REF_LINE_OH"
#define REF_BP_MAP                         "REF_BP_MAP"
#define REF_SLIT_POS                       "REF_SLIT_POS"
#define DRS_SETUP_WAVE                     "DRS_SETUP_WAVE"
#define EXTCOEFF_TABLE                     "EXTCOEFF_TABLE"
#define FLUX_STD_TABLE                     "FLUX_STD_TABLE"
#define FLUX_STD_CATALOG                   "FLUX_STD_CATALOG"
#define TELL_MOD_CATALOG                   "TELL_MOD_CATALOG"
#define RESP_FIT_POINTS_CAT                "RESP_FIT_POINTS_CATALOG"
/* catalog of flux of standard stars*/
#define EFFICIENCY_WINDOWS		           "EFFICIENCY_WINDOWS"
#define RESPONSE_WINDOWS		           "RESPONSE_WINDOWS"
#define QUALITY_AREAS		               "QUALITY_AREAS"
#define FIT_AREAS		                   "FIT_AREAS"
#define HIGH_ABS_REGIONS		           "HIGH_ABS_REGIONS"
CPL_END_DECLS

#endif
