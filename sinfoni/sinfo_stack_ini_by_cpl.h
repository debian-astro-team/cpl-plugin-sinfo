/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   stack_ini_by_cpl.h
   Author       :   Andrea Modigliani
   Created on   :   May 23, 2004
   Description  :   preparing stack of frames cpl input handling for SPIFFI
 ---------------------------------------------------------------------------*/
#ifndef SINFO_STACK_INI_BY_CPL_H
#define SINFO_STACK_INI_BY_CPL_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_msg.h"
#include "sinfo_globals.h"
#include "sinfo_stack_cfg.h"
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/
#define FRAME_OFF    0  /* off frames, that means sky frames 
                           or calibration frames with lamp switched off */
#define FRAME_ON     1  /* object frames */
#define FRAME_POS1   2  /* frames exposed with grating position 1 */
#define FRAME_POS2   3  /* frames exposed with dithered grating position 2 */
#define FRAME_REF    4  /* reference frames */
#define FRAME_DRK    5  /* sinfo_dark frame*/
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/
/* generateStack_ini_file */
/**
  @name     parse_stack_ini_file
  @memo     Parse a ini_name.ini file and create a blackboard.
  @param    cpl_cfg pointer to parameterlist
  @param    sof pointer to input set of frames
  @param    raw pointer to input set of raw frames
  @return   1 newly allocated stack_config blackboard structure.
  @doc

  The requested ini file is parsed and a blackboard object is created, then
  updated accordingly. Returns NULL in case of error.
 */
/*--------------------------------------------------------------------------*/
stack_config_n * 
sinfo_parse_cpl_input_stack(cpl_parameterlist * cpl_cfg, 
                            cpl_frameset* sof, 
                            cpl_frameset** raw, 
                            fake* fk) ;
void sinfo_stack_free(stack_config_n ** cfg);

#endif
