/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_fit_curve.h
   Author         :    N. Devillard
   Created on    :    July 1998
   Description    :    1d and 2d fit related routines

 *--------------------------------------------------------------------------*/

/*
    $Id: sinfo_fit_curve.h,v 1.4 2007-06-06 07:10:45 amodigli Exp $
    $Author: amodigli $
    $Date: 2007-06-06 07:10:45 $
    $Revision: 1.4 $
*/

#ifndef SINFO_FIT_CURVE_H
#define SINFO_FIT_CURVE_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include "sinfo_matrix.h"
#include "sinfo_local_types.h"
#include "sinfo_msg.h"
/*---------------------------------------------------------------------------
                              Function codes
 ---------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------*/
/**
  @name     sinfo_fit_1d_poly
  @memo     Fit a polynomial to a list of dpoints.
  @param    poly_deg    Degree of the polynomial to fit.
  @param    list        List of dpoints.
  @param    np          Number of points in the list.
  @param    mse         Output mean squared error.
  @return   Array of (np+1) polynomial coefficients.
  @doc
 
  The fitted polynomial is such that:
  \[
    P(x) = c_0 + c_1 x + c_2 x^2 + ... + c_n x^n
  \]
  So requesting a polynomial of degree n will return n+1 coefficients.
  Beware that with such polynomials, two input points shall never be
  on the same vertical!
 
  If you are not interested in getting the mean squared error back,
  feed in NULL instead of a pointer to a double for mse.
 */
/*--------------------------------------------------------------------------*/

double *
sinfo_fit_1d_poly(
    int         poly_deg,
    dpoint  *   list,
    int         np,
    double  *   mean_squared_error
) ;

#endif
