/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*----------------------------------------------------------------------------
   
   File name     :    sinfo_resampling.h
   Author         :    Nicolas Devillard
   Created on    :    Jan 04, 1996
   Description    :    resampling routines

 ---------------------------------------------------------------------------*/

/*

 $Id: sinfo_new_resampling.h,v 1.9 2007-06-06 07:10:45 amodigli Exp $
 $Author: amodigli $
 $Date: 2007-06-06 07:10:45 $
 $Revision: 1.9 $

 */

#ifndef SINFO_NEW_RESAMPLING_H
#define SINFO_NEW_RESAMPLING_H

/*---------------------------------------------------------------------------
                                  Includes
 ---------------------------------------------------------------------------*/

/*
#include "my_image_handling.h"
#include "sinfo_matrix.h"
*/
#include "sinfo_poly2d.h"
#include <cpl.h>
#include "sinfo_msg.h"


/*---------------------------------------------------------------------------
                                  Defines
 ---------------------------------------------------------------------------*/


#define TRANSFO_AFFINE          0
#define TRANSFO_DEG2            1
#define TRANSFO_HOMOGRAPHIC     2

/* Number of pixels set to 0 by the shift resampling */
#define    SHIFT_REJECT_L            2
#define    SHIFT_REJECT_R            2
#define    SHIFT_REJECT_T            2
#define    SHIFT_REJECT_B            2

/*
 * Kernel definition in terms of sampling
 */


/* Number of tabulations in kernel  */
#define TABSPERPIX      (1000)
#define KERNEL_WIDTH    (2.0)
#define KERNEL_SAMPLES  (1+(int)(TABSPERPIX * KERNEL_WIDTH))

#define TANH_STEEPNESS    (5.0)


/*---------------------------------------------------------------------------
                         Function ANSI C prototypes
 ---------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
/**
  @name     sinfo_new_generate_interpolation_kernel
  @memo     Generate an interpolation kernel to use in this module.
  @param    kernel_type     Type of interpolation kernel.
  @return   1 newly allocated array of doubles.
  @doc
 
  Provide the name of the kernel you want to generate. Supported kernel
  types are:
 
  \begin{tabular}{ll}
  NULL          &   default kernel, currently "tanh" \\
  "default"     &   default kernel, currently "tanh" \\
  "tanh"        &   Hyperbolic tangent \\
  "sinc2"       &   Square sinc \\
  "lanczos"     &   Lanczos2 kernel \\
  "hamming"     &   Hamming kernel \\
  "hann"        &   Hann kernel
  \end{tabular}
 
  The returned array of doubles is ready of use in the various re-sampling
  functions in this module. It must be deallocated using free().
 */
/*--------------------------------------------------------------------------*/


double   *
sinfo_new_generate_interpolation_kernel(const char * kernel_type) ;


/*-------------------------------------------------------------------------*/
/**
  @name     sinfo_warp_image_generic
  @memo     Warp an image according to a polynomial transformation.
  @param    image_in        Image to warp.
  @param    kernel_type     Interpolation kernel to use.
  @param    poly_u          Polynomial transform in U.
  @param    poly_v          Polynomial transform in V.
  @return   1 newly allocated image.
  @doc
 
  Warp an image according to a polynomial transform. Provide two
  polynomials (see poly2d.h for polynomials in this library) Pu and Pv such
  as:
 
  \begin{verbatim}
  x = sinfo_poly2d_compute(Pu, u, v)
  y = sinfo_poly2d_compute(Pv, u, v)
  \end{verbatim}
 
  Attention! The polynomials define a reverse transform. (u,v) are
  coordinates in the warped image and (x,y) are coordinates in the original
  image. The transform you provide is used to compute from the warped
  image, which pixels contributed in the original image.
 
  The output image will have strictly the same size as in the input image.
  Beware that for extreme transformations, this might lead to blank images
  as result.
 
  See the function sinfo_generate_interpolation_kernel() for possible kernel
  types. If you want to use a default kernel, provide NULL for kernel type.
 
  The returned image is a newly allocated objet, use cpl_image_delete() to
  deallocate it.
 
 */
/*--------------------------------------------------------------------------*/


cpl_image *
sinfo_new_warp_image_generic(
    cpl_image       *    image_in,
    const char      *    kernel_type,
    cpl_polynomial  *    poly_u,
    cpl_polynomial  *    poly_v
) ;


/*-------------------------------------------------------------------------*/
/**
  @name     sinfo_new_generate_tanh_kernel
  @memo     Generate a hyperbolic tangent kernel.
  @param    steep   Steepness of the hyperbolic tangent parts.
  @return   1 pointer to a newly allocated array of doubles.
  @doc
 
  The following function builds up a good approximation of a box filter. It
  is built from a product of hyperbolic tangents. It has the following
  properties:
 
  \begin{itemize}
  \item It converges very quickly towards +/- 1.
  \item The converging transition is very sharp.
  \item It is infinitely differentiable everywhere (i.e. smooth).
  \item The transition sharpness is scalable.
  \end{itemize}
 
  The returned array must be deallocated using free().
 */
/*--------------------------------------------------------------------------*/

double * sinfo_new_generate_tanh_kernel(double steep) ;

#endif
