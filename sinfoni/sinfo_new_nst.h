#ifndef SINFO_NEW_NST_H
#define SINFO_NEW_NST_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*****************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_nst.h,v 1.7 2007-09-21 14:49:00 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  17/09/03  created
*/

/************************************************************************
 * new_nsh.h
 * Result of a north-south test exposure are 32 continuum spectra of a 
 * pinhole that means one spectrum in each slitlet at the same spatial 
 * position.
 * Each spectrum is fitted in sp[atial direction by a Gaussian to get the 
 * sub-pixel positions for each row.
 *
 * Then the distances are determined in each row and averaged
 *
 * Result: are distances of each slitlet from each other => 31 values stored 
 *  in an ASCII file this Python script needs a frame of a pinhole source with 
 *  a continuous spectrum, that is shifted exactly perpendicular to the 
 *  slitlets. It fits the spectra in spatial direction by a Gaussian fit 
 *  function and therefore determines the sub-pixel position of the source.
 *
 *  Then the distances of the slitlets from each other are determined and 
 *   saved in an ASCII list. 
 *
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h>  
#include "sinfo_msg.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
   Function     :       sinfo_nst()
   In           :       ini_file: file name of according .ini file
   Out          :       integer (0 if it worked, -1 if it doesn't) 
   Job          :
       
 * Result of a north-south test exposure are 32 continuum spectra of a 
 * pinhole that means one spectrum in each slitlet at the same spatial 
 * position.
 * Each spectrum is fitted in sp[atial direction by a Gaussian to get the 
 * sub-pixel positions for each row.
 *
 * Then the distances are determined in each row and averaged 


 ---------------------------------------------------------------------------*/
int 
sinfo_new_nst (const char* plugin_id, 
               cpl_parameterlist* config,
               cpl_frameset* set,
               cpl_frameset* ref_set) ;


#endif 

/*--------------------------------------------------------------------------*/
