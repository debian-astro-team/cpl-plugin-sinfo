/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004-2009 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2010-02-08 07:18:21 $
 * $Revision: 1.9 $
 *
 */

#ifndef SINFO_UTL_EFFICIENCY_H_
#define SINFO_UTL_EFFICIENCY_H_
#include <cpl.h>


void 
sinfo_load_ref_table(cpl_frameset* frames, 
                   double dRA, 
                   double dDEC, 
                   double EPSILON, 
                   cpl_table** pptable);



cpl_table*
sinfo_utl_efficiency(
		     cpl_frameset * frames,
		     double dGain,
		     double dEpsilon,
		     double aimprim,
                     const char* col_name_atm_wave,
                     const char* col_name_atm_abs,
                     const char* col_name_ref_wave,
                     const char* col_name_ref_flux,
                     const char* col_name_ref_bin,
                     const char* col_name_obj_wave,
                     const char* col_name_obj_flux
   );

cpl_table* 
sinfo_utl_efficiency_internal(
			      cpl_table* tbl_obj_spectrum,
			      cpl_table* tbl_atmext,
			      cpl_table* tbl_ref,
			      double exptime,
			      double airmass,
			      double aimprim,
			      double gain,
			      int    biny,
                              double src2ref_wave_sampling,
                              const char* col_name_atm_wave,
                              const char* col_name_atm_abs,
                              const char* col_name_ref_wave,
                              const char* col_name_ref_flux,
                              const char* col_name_ref_bin,
                              const char* col_name_obj_wave,
                              const char* col_name_obj_flux
   );

double
sinfo_data_interpolate(
		     double wav,
		     int nrow,
		     double* pw,
		     double* pe
		     );



cpl_table*
sinfo_efficiency_compute(cpl_frame* frm_sci, 
                       cpl_frame* frm_cat,
                       cpl_frame* frm_atmext);

cpl_error_code
sinfo_response_compute(const char* plugin_id, cpl_parameterlist* config,
		cpl_frameset* ref_set,cpl_frameset* sof);

cpl_error_code
sinfo_flux_calibrate_spectra(const char* plugin_id,
		cpl_parameterlist* config, cpl_frameset* ref_set, cpl_frameset* sof);

cpl_error_code
sinfo_flux_calibrate_cube(const char* procatg, const char* plugin_id,
		cpl_parameterlist* config, cpl_frameset* ref_set, cpl_frameset* sof);
int sinfo_can_flux_calibrate(cpl_frameset* sof);
int sinfo_can_compute_response(cpl_frameset* sof);
#endif
