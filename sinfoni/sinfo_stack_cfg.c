/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------

 File name     :    stack_cfg.c
 Author         : Juergen Schreiber
 Created on    :    September 2001
 Description    :    prepare stacked frames configuration handling tools

 *--------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_stack_cfg.h"
/**@{*/
/**
 * @addtogroup sinfo_rec_jitter Stack structure configuration
 *
 * TBD
 */

/*---------------------------------------------------------------------------
 Function codes
 ---------------------------------------------------------------------------*/

/**
 @brief allocate memory for a stack_config struct
 @name sinfo_stack_cfg_create()
 @param void
 @return pointer to allocated base stack_config structure
 @note only the main (base) structure is allocated
 */

stack_config_n *
sinfo_stack_cfg_create_n(void)
{
    return cpl_calloc(1, sizeof(stack_config_n));
}

/**
 @brief deallocate all memory associated with a stack_config data structure
 @name sinfo_stack_cfg_destroy()
 @param stack_config to deallocate
 @return void
 */

void
sinfo_stack_cfg_destroy_n(stack_config_n * sc)
{
    if (sc == NULL )
        return;

    /* Free list of frame types */
    /*cpl_free(sc->frametype);*/
    /* Free positions */
    /*cpl_free(sc->frameposition);*/
    /* Free main struct */
    cpl_free(sc);

    return;
}
/**@}*/



