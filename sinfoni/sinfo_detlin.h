#ifndef SINFO_DETLIN_H
#define SINFO_DETLIN_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_detlin.h,v 1.5 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* amodigli  04/01/06  created
*/

/************************************************************************
 * sinfo_detlin.h
 * detector linearity routines
 * to search for static bad pixels
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include "sinfo_image_ops.h"

/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/
/**
   @name      sinfo_new_fit_intensity_course()
   @param     flatStack: flats with in/decreasing intensity
                         stacked in a data cube
   @param     order:     order of the fit polynomial
   @return    data cube containing the fit result. The first polynomial 
              coefficients in the first plane and so on.
   @doc       this routine fits polynomials along the z-axis of
              a data cube and stores the resulting coefficients
              in a data cube. The eclipse routine sinfo_fit_1d_poly()
              is used for polynomial fitting
              The input is assumed to be a cube containing flatfield frames of
              different intensities (usually increasing or decreasing).
              for each pixel position on the detector, a curve is plotted
              of the pixel intensity in each plane against the clean mean
              intensity of the plane. Then a polynomial of user defined
              order is fitted to the curves.
*/

cpl_imagelist * 
sinfo_new_fit_intensity_course(cpl_imagelist * flatStack,
                              int       order,
                              float     loReject,
                              float     hiReject ) ;



/**
   @name    sinfo_new_search_bad_pixels()
   @param   coeffs fitted polynomial coefficients stored in a cube
   @param   threshSigmaFactor factor of determined sigma of an
                              image plane to determine the
                              threshold for good or bad pixels
   @param    nonlinearThresh   absolute threshold value of the found
                               non-linear polynomial coefficients beyond
                               which the pixels are declared as bad.
   @param    loReject 
   @param    hiReject percentage (0...100) of extreme pixel values that is not
                      considered for image statistics
   @return   Bad pixel mask image (1: good pixel, 0: bad pixel).
   @doc      this routine searches for static bad pixel positions
             by searching the fitted polynomial coefficients of each pixel 
             response for outliers.
             Pixel with high non-linear response are also declared as bad.
*/

cpl_image * 
sinfo_new_search_bad_pixels(cpl_imagelist *  coeffs,
                            double     threshSigmaFactor,
                            double     nonlinearThresh,
                            float      loReject,
                            float      hiReject ) ;

/**
   @name     sinfo_new_search_bad_pixels_via_noise()
   @param    darks sequence of darks (NDIT = 1)
                   stored in a cube, at least 10 to get good statistics
   @param    threshSigmaFactor factor to determined standard deviation
                               in each pixel to determine the threshold
                               beyond which a pixel is declared as bad.
   @param    loReject 
   @param    hiReject percentage (0...100) of extreme pixel
                      values that is not considered for image statistics
   @return  Bad pixel mask image (1: good pixel, 0: bad pixel).
   @doc     this routine searches for static bad pixel positions
            This is done by building a cube of sinfo_dark frames and examine
            the noise variations in each pixel. If big deviations
            from a clean mean pixel noise occurr, the pixel is
            declared as bad.
*/
cpl_image * 
sinfo_new_search_bad_pixels_via_noise( cpl_imagelist *  darks,
                                    float      threshSigmaFactor,
                                    float      loReject,
                                    float      hiReject ) ;


/*
   @name   sinfo_new_count_bad_pixels()
   @memo     this routine counts the number of bad pixels
   @param  bad bad pixel mask
   @return      number of bad pixels.
*/

int 
sinfo_new_count_bad_pixels ( cpl_image * bad ) ;


/*
   @name   sinfo_new_abs_dist_image()
   @param  image, a threshold parameter
   @result resulting image
   @doc    filter, calculates the absolute distances of the nearest neighbors 
           for an image by using the 8 closest pixels of every pixel.
           The values in the output image are determined according
           to the values of the input parameter.
           If fmedian = 0: always replace by abs. distances
           if fmedian < 0: replace by abs. distances if |median_dist - dist| >
                                        -fmedian
           if fmedian > 0: replace by abs. distances (fmedian as a factor of
                           the square root of the distance itself)
           if |median_dist - dist| >= fmedian * sqrt ( dist )
           This can be used to consider photon noise.
           This considers a dependence of the differences on the
           pixel values themselves.
   @note   it is assumed that most of the 8 nearest neighbor pixels
           are not bad pixels! blank pixels are not replaced!
*/


cpl_image * 
sinfo_new_abs_dist_image(cpl_image * im, float fmedian ) ;


/**
   @name  sinfo_new_mean_image_in_spec()
   @param image, a threshold parameter
   @return resulting image
   @doc    mean filter, calculates the mean for an image
           by using the 4 closest pixels of every pixel in spectral direction
          (column).
          The values in the output image are determined according
          to the values of the input parameter.
          If fmedian = 0: always replace by mean
          if fmedian < 0: replace by mean if |pixel - mean| > -fmedian
          if fmedian > 0: replace by mean (fmedian as a factor of
          the square root of the mean itself)
          if |pixel - mean| >= fmedian * sqrt ( mean )
          This can be used to consider photon noise.
          This considers a dependence of the differences on the
          pixel values themselves.
   @note  it is assumed that most of the 4 nearest neighbor pixels
          are not bad pixels! blank pixels are not replaced!
*/


cpl_image * 
sinfo_new_mean_image_in_spec(cpl_image * im, float fmedian ) ;

/**
   @name  sinfo_new_local_median_image()
   @param im input image
   @param fmedian  a factor to the local standard deviation
   @param loReject
   @param hiReject fraction of rejected values to determine
                   a clean standard deviation
   @param half_box_size integer half size of the running box to determine
                        the local clean standard deviation
   @return  resulting image
   @doc     filter, calculates the local stdev in a moving box
            Then it calculates the difference of the pixel to the sinfo_median
            of the nearest neighbors by using the 8 closest pixels of every 
            pixel. The values in the output image are determined according
            to the values of the input parameter.
            If fmedian = 0: always replace by sinfo_median
            if fmedian > 0: replace sinfo_median if |median_dist - dist| >
                            fmedian * stdev
   @note it is assumed that most of the 8 nearest neighbor pixels are not bad 
         pixels! blank pixels are not replaced!
*/

cpl_image * 
sinfo_new_local_median_image(cpl_image * im,
                             float fmedian,
                             float loReject,
                             float hiReject,
                             int half_box_size ) ;


#endif /*!SINFO_DETLIN_H*/

