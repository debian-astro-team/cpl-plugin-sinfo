/* $Id: sinfo_tpl_dfs.c,v 1.13 2012-03-03 10:17:31 amodigli Exp $
 *
 * This file is part of the SINFONI Pipeline
 * Copyright (C) 2002,2003 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * $Author: amodigli $
 * $Date: 2012-03-03 10:17:31 $
 * $Revision: 1.13 $
 * $Name: not supported by cvs2svn $
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*-----------------------------------------------------------------------------
 Includes
 -----------------------------------------------------------------------------*/

#include <string.h>
#include <math.h>

#include <cpl.h>
#include "sinfo_utilities.h"

#include "sinfo_tpl_dfs.h"
#include "sinfo_pro_types.h"
#include "sinfo_raw_types.h"
#include "sinfo_ref_types.h"
#include "sinfo_error.h"
#include "sinfo_msg.h"

/**@{*/
/*----------------------------------------------------------------------------*/
/**
 * @defgroup sinfo_tpl_dfs  DFS related functions
 *
 * TBD
 */
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
/**
 * @brief
 *   Check if all SOF files exist
 *
 * @param frameset The input set-of-frames
 *
 * @return 1 if not all files exist, 0 if they all exist.
 *
 */
/*----------------------------------------------------------------------------*/
static int
sinfo_dfs_files_dont_exist(cpl_frameset *frameset)
{
    const char *func = "dfs_files_dont_exist";

    if (frameset == NULL ) {
        cpl_error_set(func, CPL_ERROR_NULL_INPUT);
        return 1;
    }

    if (cpl_frameset_is_empty(frameset)) {
        return 0;
    }

    cpl_frameset_iterator* it = cpl_frameset_iterator_new(frameset);
    cpl_frame *frame = cpl_frameset_iterator_get(it);

    while (frame) {
        if (access(cpl_frame_get_filename(frame), F_OK)) {
            cpl_msg_error(func, "File %s (%s) was not found",
                            cpl_frame_get_filename(frame),
                            cpl_frame_get_tag(frame));
            cpl_error_set(func, CPL_ERROR_FILE_NOT_FOUND);
        }
        cpl_frameset_iterator_advance(it, 1);
        frame = cpl_frameset_iterator_get(it);

    }

    cpl_frameset_iterator_delete(it);

    if (cpl_error_get_code())
        return 1;

    return 0;
}

/*----------------------------------------------------------------------------*/
/**
 @brief    Set the group as RAW or CALIB in a frameset
 @param    set     the input frameset
 @return   0 if ok, -1 in error case
 */
/*----------------------------------------------------------------------------*/
int
sinfo_dfs_set_groups(cpl_frameset * set)
{
    cpl_frame * cur_frame = NULL;
    const char * tag = NULL;
    int nframes = 0;
    int i = 0;

    sinfo_skip_if(cpl_error_get_code());

    /* Check entries */
    if (set == NULL )
        return -1;

    /* Initialize */
    nframes = cpl_frameset_get_size(set);

    /* Loop on frames */
    for (i = 0; i < nframes; i++) {
        cur_frame = cpl_frameset_get_frame(set, i);
        tag = cpl_frame_get_tag(cur_frame);
        if (cpl_error_get_code())
            break;
        if (tag == NULL )
            sinfo_msg_warning("Frame %d has no tag", i);
        /* RAW frames */
        else if (!strcmp(tag, RAW_LINEARITY_LAMP) || !strcmp(tag, RAW_ON)
                        || !strcmp(tag, RAW_OFF) || !strcmp(tag, RAW_DARK)
                        || !strcmp(tag, RAW_FIBRE_NS)
                        || !strcmp(tag, RAW_FIBRE_PSF)
                        || !strcmp(tag, RAW_FIBRE_DARK)
                        || !strcmp(tag, RAW_FLAT_NS)
                        || !strcmp(tag, RAW_WAVE_NS)
                        || !strcmp(tag, RAW_FLAT_LAMP)
                        || !strcmp(tag, RAW_WAVE_LAMP)
                        || !strcmp(tag, RAW_PSF_CALIBRATOR)
                        || !strcmp(tag, RAW_SKY_PSF_CALIBRATOR)
                        || !strcmp(tag, RAW_STD) || !strcmp(tag, RAW_SKY)
                        || !strcmp(tag, RAW_SKY_STD)
                        || !strcmp(tag, RAW_OBJECT_NODDING)
                        || !strcmp(tag, RAW_SKY_NODDING)
                        || !strcmp(tag, RAW_OBJECT_JITTER)
                        || !strcmp(tag, RAW_SKY_JITTER)
                        || !strcmp(tag, RAW_PUPIL_LAMP)
                        || !strcmp(tag, RAW_IMAGE_PRE_OBJECT)
                        || !strcmp(tag, RAW_IMAGE_PRE_SKY)
                        || !strcmp(tag, RAW_OBJECT_SKYSPIDER))
            cpl_frame_set_group(cur_frame, CPL_FRAME_GROUP_RAW);
        /* CALIB frames */
        else if (!strcmp(tag, PRO_BP_MAP_HP)
                        || !strcmp(tag, SINFO_UTL_STDSTARS_RAW)
                        || !strcmp(tag, SINFO_CALIB_STDSTARS)
                        || !strcmp(tag, SINFO_CALIB_SED)
                        || !strcmp(tag, PRO_BP_MAP_NL)
                        || !strcmp(tag, PRO_BP_MAP_NO)
                        || !strcmp(tag, PRO_BP_MAP_DI)
                        || !strcmp(tag, PRO_BP_MAP_NL)
                        || !strcmp(tag, PRO_BP_MAP_NO)
                        || !strcmp(tag, PRO_BP_MAP) || !strcmp(tag, PRO_ILL_COR)
                        || !strcmp(tag, PRO_MASTER_BP_MAP)
                        || !strcmp(tag, PRO_MASTER_DARK)
                        || !strcmp(tag, PRO_DISTORTION)
                        || !strcmp(tag, PRO_SLITLETS_DISTANCE)
                        || !strcmp(tag, PRO_MASTER_FLAT_LAMP)
                        || !strcmp(tag, PRO_SLIT_POS)
                        || !strcmp(tag, PRO_SLIT_POS_GUESS)
                        || !strcmp(tag, PRO_FIRST_COL)
                        || !strcmp(tag, PRO_WAVE_MAP)
                        || !strcmp(tag, REF_LINE_ARC)
                        || !strcmp(tag, DRS_SETUP_WAVE)
                        || !strcmp(tag, EXTCOEFF_TABLE)
                        || !strcmp(tag, FLUX_STD_TABLE)
                        || !strcmp(tag, FLUX_STD_CATALOG)
                        || !strcmp(tag, EFFICIENCY_WINDOWS)
                        || !strcmp(tag, RESPONSE_WINDOWS)
                        || !strcmp(tag, TELL_MOD_CATALOG)
                        || !strcmp(tag, RESP_FIT_POINTS_CAT)
                        || !strcmp(tag, QUALITY_AREAS)
                        || !strcmp(tag, FIT_AREAS)
                        || !strcmp(tag, HIGH_ABS_REGIONS)
                        || !strcmp(tag, PRO_IMA) || !strcmp(tag, PRO_CUBE)
                        || !strcmp(tag, REF_BP_MAP)
                        || !strcmp(tag, PRO_SKY_DUMMY)
                        || !strcmp(tag, PRO_REF_ATM_REF_CORR)
	                    || !strcmp(tag, PRO_RESPONSE)
                        || !strcmp(tag, PRO_SPECTRUM)
						|| !strcmp(tag, REF_BP_MAP_HP)
						|| !strcmp(tag, REF_BP_MAP_NL)
						|| !strcmp(tag, REF_MASTER_DARK)
						|| !strcmp(tag, REF_MASTER_FLAT_LAMP)
						|| !strcmp(tag, REF_DISTORTION)
						|| !strcmp(tag, REF_SLITLETS_DISTANCE)
						)
            cpl_frame_set_group(cur_frame, CPL_FRAME_GROUP_CALIB);
        else if (!strcmp(tag, PRO_OBS_OBJ) || !strcmp(tag, PRO_MED_OBS_OBJ)
                        || !strcmp(tag, PRO_OBS_STD)
                        || !strcmp(tag, PRO_MED_OBS_STD)
                        || !strcmp(tag, PRO_OBS_PSF)
                        || !strcmp(tag, PRO_MED_OBS_PSF)
                        || !strcmp(tag, PRO_COADD_OBJ)
                        || !strcmp(tag, PRO_COADD_STD)
                        || !strcmp(tag, PRO_COADD_PSF)
                        || !strcmp(tag, PRO_COADD_PUPIL)
                        || !strcmp(tag, PRO_MED_COADD_OBJ)
                        || !strcmp(tag, PRO_MED_COADD_STD)
                        || !strcmp(tag, PRO_MED_COADD_PSF)
                        || !strcmp(tag, PRO_MED_COADD_PUPIL)
                        || !strcmp(tag, PRO_MASK_COADD_OBJ)
                        || !strcmp(tag, PRO_MASK_COADD_STD)
                        || !strcmp(tag, PRO_MASK_COADD_PSF)
                        || !strcmp(tag, PRO_MASK_COADD_PUPIL)
                        || !strcmp(tag, PRO_PSF_CALIBRATOR_STACKED)
                        || !strcmp(tag, PRO_SKY_PSF_CALIBRATOR_STACKED)
                        || !strcmp(tag, PRO_AO_PERFORMANCE)
                        || !strcmp(tag, PRO_ENC_ENERGY) || !strcmp(tag, PRO_PSF)
                        || !strcmp(tag, PRO_STD_STAR_SPECTRA)
                        || !strcmp(tag, PRO_STD_STAR_SPECTRUM)
                        || !strcmp(tag, PRO_MFLAT_CUBE)
                        || !strcmp(tag, PRO_MFLAT_AVG)
                        || !strcmp(tag, PRO_MFLAT_MED)
                        || !strcmp(tag, PRO_STACK_MFLAT_DIST)
                        || !strcmp(tag, PRO_WAVE_LAMP_STACKED)
                        || !strcmp(tag, PRO_FIBRE_NS_STACKED_OFF)
                        || !strcmp(tag, PRO_FIBRE_NS_STACKED_ON)
                        || !strcmp(tag, PRO_FIBRE_NS_STACKED)
                        || !strcmp(tag, PRO_SLITLETS_POS_PREDIST)
                        || !strcmp(tag, PRO_OBS_SKY)
                        || !strcmp(tag, PRO_SKY_MED)
                        || !strcmp(tag, PRO_FIBRE_NS_STACKED_DIST)

        )
            cpl_frame_set_group(cur_frame, CPL_FRAME_GROUP_PRODUCT);
        else
            sinfo_msg_warning("Frame %d has unknown tag: %s", i, tag);
    }
    ck0_nomsg(sinfo_dfs_files_dont_exist(set));
    sinfo_skip_if(i != nframes);

    sinfo_end_skip;

    if (cpl_error_get_code())
        sinfo_msg_error("Could not identify RAW and CALIB frames (in "
                        "frame set of size %d)", nframes);

    return cpl_error_get_code();
}

/**@}*/
