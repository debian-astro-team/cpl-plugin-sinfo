#ifndef SINFO_SHIFT_IMAGES_H
#define SINFO_SHIFT_IMAGES_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/***************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_shift_images.h,v 1.5 2008-02-12 14:21:57 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  05/03/03  created
*/

/************************************************************************
 * sinfo_shift_images.h
 * shift two emission line images on each other
 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <cpl.h>
#include "sinfo_spectrum_ops.h"
#include "sinfo_recipes.h"
#include "sinfo_image_ops.h"

/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

/**
   @name     sinfo_new_determine_shift_by_correlation()
   @memo    determines the sub-pixel shift of to emission line frames by 
             cross sinfo_correlation and fitting the sinfo_correlation
             function by a Gaussian
   @param    refImage: reference image
   @param    shiftedImage: image shifted in spectral direction
                                      with respect to the reference image
   @return   shift in sub-pixel accuracy
*/

double 
sinfo_new_determine_shift_by_correlation (cpl_image * refImage,
                                     cpl_image * shiftedImage ) ;


/**
   @name     sinfo_new_shift_image_in_spec()
   @memo   shifts an image by a given amount to integer pixel accuracy
   @param    shiftedImage: image to shift in spectral direction
   @param    shift: amount of shift, output of 
             sinfo_determineShiftByCorrelation
   @param    sub_shift: non-integer rest of shift < 1
   @result   shifted image
*/


cpl_image * 
sinfo_new_shift_image_in_spec (cpl_image * shiftedImage, 
                               double shift, 
                               double * sub_shift ) ;


/**
   @name   sinfo_new_fine_shift_image_in_spec_poly()
   @memo   shifts an image by a given amount to sub-pixel accuracy
   @param  shiftedImage: image to shift in spectral direction
   @param  sub_shift: amount of shift < 1, output of  sinfo_shiftImageInSpec
   @param  order:     order of polynomial
   @result shifted image
*/

cpl_image * 
sinfo_new_fine_shift_image_in_spec_poly (cpl_image * shiftedImage, 
                                         double sub_shift, 
                                         int order ) ;

/**
   @name   new_fine_shift_image_in_spec_cubic_spline()
   @param  shiftedImage: image to shift in spectral direction
   @param  sub_shift: amount of shift < 1, output of  sinfo_shiftImageInSpec
   @result shifted image
   @result shifts an image by a given amount to sub-pixel accuracy
*/


cpl_image * 
sinfo_new_fine_shift_image_in_spec_cubic_spline (cpl_image * shiftedImage, 
                                                 double sub_shift ) ;


/**
   @name  sinfo_align_cube_to_reference() 
   @memo shifts a images stacked in a cube by a given amount to 
         sub-pixel accuracy
   @param cube: cube containing images to shift in spectral direction
   @param refIm: reference image (OH spectrum)
   @param order: order of polynomial interpolation for the resampling
   @param shift_indicator: indicator for the polynomial interpolation (0)
                           or cubic spline interpolation shift (1).
   @result shifted cube
 */


cpl_imagelist  * 
sinfo_align_cube_to_reference (cpl_imagelist * cube,
                                 cpl_image * refIm,
                                 int order,
                                 int shift_indicator ) ;



#endif /*!SINFO_SHIFT_IMAGES_H*/

/*--------------------------------------------------------------------------*/
