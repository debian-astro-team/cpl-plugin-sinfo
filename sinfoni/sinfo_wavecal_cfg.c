/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */

/*---------------------------------------------------------------------------

 File name     :    sinfo_wavecal_cfg.c
 Author         : Juergen Schreiber
 Created on    :    September 2001
 Description    :    wavelength calibration configuration handling tools

 *--------------------------------------------------------------------------*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/

#include "sinfo_wavecal_cfg.h"

/**@{*/
/**
 * @addtogroup sinfo_rec_wavecal wavecal structure configuration
 *
 * TBD
 */

/*---------------------------------------------------------------------------
 Function codes
 ---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------
 Function :   sinfo_wave_cfg_create()
 In       :   void
 Out      :   pointer to allocated base wave_config structure
 Job      :   allocate memory for a wave_config struct
 Notice   :   only the main (base) structure is allocated
 ---------------------------------------------------------------------------*/

wave_config *
sinfo_wave_cfg_create(void)
{
    return cpl_calloc(1, sizeof(wave_config));
}

/*---------------------------------------------------------------------------
 Function :   sinfo_wave_cfg_destroy()
 In       :   wave_config to deallocate
 Out      :   void
 Job      :   deallocate all memory associated with a wave_config
 Notice   :
 ---------------------------------------------------------------------------*/

void
sinfo_wave_cfg_destroy(wave_config * wc)
{
    if (wc == NULL )
        return;

    /* Free main struct */
    cpl_free(wc);

    return;
}
/**@}*/



