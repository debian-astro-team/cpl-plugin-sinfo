/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------
   
   File name    :   sinfo_detlin_ini.h
   Author       :    Juergen Schreiber
   Created on   :    April 19, 2002
   Description  :    prepare handling of .ini file for the search for static 
                        bad pixels
 ---------------------------------------------------------------------------*/
#ifndef SINFO_DETLIN_INI_H
#define SINFO_DETLIN_INI_H
/*---------------------------------------------------------------------------
                                Includes
---------------------------------------------------------------------------*/
#include <cpl.h>
#include "sinfo_detlin_cfg.h"
/*---------------------------------------------------------------------------
                                Defines
---------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------
                             Function prototypes 
 ---------------------------------------------------------------------------*/
/**
  @name        generateDetlin_ini_file
  @memo        Generate a default ini file for the bad pixel search command.
  @param    ini_name    Name of the file to generate.
  @param    name_i        Name of the input file.
  @param    name_o        Name of the output file.
  @return    int 0 if Ok, -1 otherwise.
  @doc          This function generates a default ini file for the bad pixel 
                search command. The generated file will have the requested 
                name. 
 */
int 
generateDetlin_ini_file(
        char * ini_name,
        char * name_i,
        char * name_o
);
/**
  @name     parse_detlin_ini_file
  @memo     Parse an ini_name.ini file and create a blackboard.
  @param    ini_name    Name of the ASCII file to parse.
  @return   1 newly allocated detlin_config blackboard structure.
  @doc      The requested ini file is parsed and a blackboard object is 
            created, the updated accordingly. Returns NULL in case of error.
 */
detlin_config * 
parse_detlin_ini_file(char * ini_name) ;
#endif
