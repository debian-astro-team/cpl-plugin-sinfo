#ifndef SINFO_BP_NORM_H
#define SINFO_BP_NORM_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*******************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_bp_norm.h,v 1.4 2007-09-21 14:13:43 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  06/05/03  created
*/

/************************************************************************
 * bp_norm.h
 * routine to search for bad pixels
 *----------------------------------------------------------------------
 */

/*
 * header files
 */
#include <cpl.h>
#include "sinfo_msg.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

int 
sinfo_new_bp_search_normal (const char* plugin_id, 
                            cpl_parameterlist* config, 
                            cpl_frameset* set, 
                            cpl_frameset* ref_set, 
                            const char* procatg);
#endif

/*--------------------------------------------------------------------------*/
