/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*----------------------------------------------------------------------------

 File name    :   sinfo_detnoise_ini.c
 Author       :   Andrea Modiglini
 Created on   :   May 17, 2004
 Description  :   produce and read an .ini file for the search of static
 bad pixels
 ---------------------------------------------------------------------------*/
#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
/*---------------------------------------------------------------------------
 Includes
 ---------------------------------------------------------------------------*/
#include <string.h>
#include "sinfo_detnoise_ini_by_cpl.h"
#include "sinfo_raw_types.h"
#include "sinfo_globals.h"
#include "sinfo_hidden.h"
#include "sinfo_functions.h"
/*---------------------------------------------------------------------------
 Functions private to this module
 ---------------------------------------------------------------------------*/

static void
parse_section_frames(detnoise_config * cfg, cpl_frameset* sof,
                     cpl_frameset** raw, int* status);
static void
parse_section_badsearch(detnoise_config * cfg, cpl_parameterlist* cpl_cfg);

/**@{*/
/**
 * @addtogroup sinfo_bad_pix_search Bad Pixel Search
 *
 * TBD
 */

/****************************************************************************/
/**
 @name     sinfo_parse_cpl_input_detnoise
 @memo     Parse input frames & parameters and create a blackboard.
 @param    sof pointer to input set of frames
 @param    raw pointer to input set of raw frames
 @return   1 newly allocated detnoise_config blackboard structure.

 @doc      The requested ini file is parsed and a blackboard object is
 created, then updated accordingly. Returns NULL in case of error.
 
 */
detnoise_config *
sinfo_parse_cpl_input_detnoise(cpl_parameterlist * cpl_cfg, cpl_frameset* sof,
                               cpl_frameset** raw)
{

    detnoise_config * cfg;
    int status = 0;
    /* Removed check on ini_file */
    /* Removed load of ini file */

    cfg = sinfo_detnoise_cfg_create();

    /*
     * Perform sanity checks, fill up the structure with what was
     * found in the ini file
     */
    parse_section_badsearch(cfg, cpl_cfg);
    parse_section_frames(cfg, sof, raw, &status);

    if (status > 0) {
        sinfo_msg_error("parsing cpl input");
        sinfo_detnoise_free(cfg);
        cfg = NULL;
        return NULL ;
    }
    return cfg;
}

/**
 @name     parse_section_frames
 @memo     Parse input frames.
 @param    cfg pointer to detnoise_config
 @param    sof pointer to input set of frames
 @param    raw pointer to input set of raw frames
 @param    status status of operation
 @return   1 newly allocated detnoise_config blackboard structure.
 */

static void
parse_section_frames(detnoise_config * cfg, cpl_frameset * sof,
                     cpl_frameset** raw, int* status)
{
    int i;

    int nraw = 0;
    cpl_frame* frame = NULL;

    char spat_res[FILE_NAME_SZ];
    char lamp_status[FILE_NAME_SZ];
    char band[FILE_NAME_SZ];
    int ins_set = 0;

    sinfo_extract_raw_frames_type2(sof, raw, RAW_DARK);

    nraw = cpl_frameset_get_size(*raw);
    if (nraw < 1) {
        sinfo_msg_error("Too few (%d) raw frames (%s) present in"
                        "frameset!Aborting...", nraw, RAW_DARK);
        (*status)++;
        return;
    }

    /* get "general:infile" read it, check input sinfo_matrix */
    /* Allocate structures to go into the blackboard */
    /* Copy relevant information into the blackboard */
    cfg->nframes = nraw;
    cfg->framelist = cpl_malloc(nraw * sizeof(char*));
    /* read input frames */
    for (i = 0; i < nraw; i++) {
        frame = cpl_frameset_get_frame(*raw, i);
        /* Store file name into framelist */
        cfg->framelist[i] = cpl_strdup(cpl_frame_get_filename(frame));
    }

    strcpy(cfg->outName, BP_NOISE_OUT_FILENAME);

    frame = cpl_frameset_get_frame(*raw, 0);
    sinfo_get_spatial_res(frame, spat_res);
    switch (sinfo_frame_is_on(frame))
        {
    case 0:
        strcpy(lamp_status, "on");
        break;
    case 1:
        strcpy(lamp_status, "off");
        break;
    case -1:
        strcpy(lamp_status, "undefined");
        break;
    default:
        strcpy(lamp_status, "undefined");
        break;
        }

    sinfo_get_band(frame, band);
    sinfo_msg("Spatial resolution: %s lamp status: %s band: %s \n", spat_res,
                    lamp_status, band);

    sinfo_get_ins_set(band, &ins_set);
    return;

}

/**
 @name     parse_section_frames
 @memo     Parse input frames.
 @param    cfg pointer to detnoise_config
 @param    cpl_cfg pointer to input parameters
 @return   1 newly allocated detnoise_config blackboard structure.
 */
static void
parse_section_badsearch(detnoise_config * cfg, cpl_parameterlist * cpl_cfg)
{
    cpl_parameter *p;

    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.bp_noise.low_rejection");
    cfg->loReject = cpl_parameter_get_double(p);

    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.bp_noise.high_rejection");
    cfg->hiReject = cpl_parameter_get_double(p);

    p = cpl_parameterlist_find(cpl_cfg, "sinfoni.bp_noise.thresh_sigma_factor");
    cfg->threshSigmaFactor = cpl_parameter_get_double(p);

}
/**
 @name    sinfo_detnoise_free()
 @memo deallocate all memory associated with a detnoise_config data structure
 @param   cfg detnoise_config to deallocate
 @return  void
 */

void
sinfo_detnoise_free(detnoise_config * cfg)
{

    if (cfg != NULL ) {
        int i = 0;
        for (i = 0; i < cfg->nframes; i++) {
            if (cfg->framelist[i] != NULL )
                cpl_free(cfg->framelist[i]);
        }
        if (cfg->framelist) {
            if (cfg->framelist != NULL )
                cpl_free(cfg->framelist);
        }
        sinfo_detnoise_cfg_destroy(cfg);
    }
}
/**@}*/
