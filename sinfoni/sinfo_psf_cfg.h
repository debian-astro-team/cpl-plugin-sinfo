/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_psf_cfg.h
   Author    :    Juergen Schreiber
   Created on    :    February 2002
   Description    :    psf_cfg.c definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_PSF_CFG_H
#define SINFO_PSF_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include "sinfo_globals.h"
#include <cpl.h>
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  PSF star image reconstruction blackboard container

  This structure holds all information related to the sinfo_psf reconstruction
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct psf_config {
/*-------General---------*/
        char inFrame[FILE_NAME_SZ] ; /* input averaged, bad pixel corrected, 
                                        off subtracted, flatfielded, spectral 
                                        tilt corrected list of frames */
        char outName[FILE_NAME_SZ] ; /* output name of resulting fits 
                                        data cube */

/*------ Reconstruction ------*/
        /* the fraction [0...1] of rejected low intensity pixels when taking 
           the average of columns */
        float lo_reject ;
        /* the fraction [0...1] of rejected high intensity pixels when taking 
           the average of columns */
        float hi_reject ;
        /* indicates if the slitlet distances are determined by a 
           north-south test (1) or slitlet edge fits (0) */ 
        int northsouthInd  ;
        /* name of the ASCII list of the fitted slitlet edge positions or 
           the distances of the slitlets */
        char poslist[FILE_NAME_SZ] ;
        /* number of slitlets (32) */
        int nslits ;
        /* sub pixel position of the column position of the left sinfo_edge of 
           the first slitlet needed if the slitlet distances were determined 
           by a north south test */
        char  firstCol[FILE_NAME_SZ] ;
    /* indicator for the shifting method to use */
    char  method[1] ;
        /* order of polynomial if the polynomial interpolation shifting 
           method is used */
        int order ;
} psf_config ;

/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name    sinfo_psf_cfg_create()
   @return  pointer to allocated base psf_cfg structure
   @doc     allocate memory for a psf_config struct
   @note   only the main (base) structure is allocated
*/

psf_config * 
sinfo_psf_cfg_create(void);

/**
   @name   sinfo_psf_cfg_destroy()
   @memo  deallocate all memory associated with a psf_config data structure
   @param  psf_config to deallocate
   @return void
*/
void 
sinfo_psf_cfg_destroy(psf_config * cc);

#endif
