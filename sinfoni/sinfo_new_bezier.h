#ifndef SINFO_NEW_BEZIER_H
#define SINFO_NEW_BEZIER_H
/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*************************************************************************
* E.S.O. - VLT project
*
* "@(#) $Id: sinfo_new_bezier.h,v 1.12 2007-06-06 07:10:45 amodigli Exp $"
*
* who       when      what
* --------  --------  ----------------------------------------------
* schreib  10/10/03  created
*/

/************************************************************************
 * bezier.h
 * bad pixel interpolation routines using bezier splines
 *----------------------------------------------------------------------
 */

/*
 * header files
 */

#include <cpl.h>
#include "sinfo_spectrum_ops.h"
#include "sinfo_msg.h"
#include "sinfo_image_ops.h"
/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/
#define cubeNONEIGHBOR  -1000000 
/* define new number that indicates that not enough neighors 
   are available for sinfo_Bezier correction */
#define cubePT_BADPIXEL   0 /*Bad pixel*/
#define cubePT_USE        1 /*Good pixel*/
#define cubePT_FIND       2 /*Correct that pixel*/
#define cubePT_NOINFO     3 

typedef struct new_dim_ {
  unsigned short x;
  unsigned short y;
  unsigned short z;
} new_Dim;

typedef struct new_xyzw_ {
  double x;
  double y;
  double z;
  double w;
} new_XYZW;

typedef struct new_lookup_ {
    cpl_image *X;
    cpl_image *hX;
    cpl_image *Y;
    cpl_image *Z;
    cpl_imagelist  *id;
    cpl_imagelist  *jd;
} new_Lookup;


int sinfo_im_xy(cpl_image* im, int X, int Y); 
int sinfo_cu_xy(cpl_imagelist* cu, int X, int Y); 

/*----------------------------------------------------------------------------
 *                        Function ANSI C prototypes
 *--------------------------------------------------------------------------*/

cpl_image * 
sinfo_new_c_bezier_interpolate_image(cpl_image *im, 
                                          cpl_image *mask, 
                                          new_Lookup *look, 
                                          short rx, 
                                          short ry, 
                                          short rz,
                          int max_rad , 
                                          float   ** slit_edges );

cpl_image * 
sinfo_new_c_bezier_find_bad(cpl_image *im, 
                                 cpl_image *mask/*, sinfo_new_lookup *look*/, 
                                 short rx, 
                                 short ry, 
                                 short rz,
                     short lowerI, 
                                 short highI, 
                                 short lowerJ, 
                                 short highJ, 
                                 float factor );


float      
sinfo_new_c_bezier_correct_pixel(int ipos, 
                                     int jpos, 
                     cpl_image *im, 
                     cpl_image *mask, 
                     cpl_imagelist  *sc_im, 
                     cpl_imagelist  *drs_sc_mask, 
                     new_Lookup *look, 
                                     short rx, 
                                     short ry, 
                                     short rz  );


float      
sinfo_new_c_bezier_correct_pixel_2D( int ipos ,
                                        int jpos, 
                        cpl_image *im, 
                        cpl_image *mask, 
                        cpl_imagelist  *sc_im, 
                        cpl_imagelist  *drs_sc_mask, 
                        /*new_Lookup *look,*/ 
                                        short rx, 
                                        short ry, 
                                        short rz , 
                                        double *med, 
                                        double *stdev, 
                                        float factor );

float sinfo_new_c_bezier_interpol(cpl_imagelist * im, cpl_imagelist * action );

int sinfo_new_bezier(new_XYZW *p,int n,double mu,double munk,new_XYZW *res );

float      
sinfo_new_c_bezier_correct_cosmic( int ipos ,
                                       int jpos,
                                       cpl_image *im,
                                       cpl_image *mask,
                                       cpl_imagelist  *sc_im,
                                       cpl_imagelist  *drs_sc_mask,
                                       new_Lookup *look, 
                                       short rx, 
                                       short ry, 
                                       short rz , 
                                       double *med, 
                                       double *stdev, 
                                       float factor );


new_Lookup * sinfo_new_lookup( void ) ;

void sinfo_new_destroy_lookup( new_Lookup *l ) ;

int sinfo_new_change_mask(cpl_image * mask,cpl_image * im ) ;


#endif /*!SINFO_BEZIER_H*/

/*--------------------------------------------------------------------------*/
