/*
 * This file is part of the ESO SINFONI Pipeline
 * Copyright (C) 2004,2005 European Southern Observatory
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, 51 Franklin St, Fifth Floor, Boston, MA  02111-1307  USA
 */
/*---------------------------------------------------------------------------
   
   File name     :    sinfo_flat_cfg.h
   Author         :    Juergen Schreiber
   Created on    :    march 2002
   Description    :    flat_ini definitions + handling prototypes
 ---------------------------------------------------------------------------*/
#ifndef SINFO_FLAT_CFG_H
#define SINFO_FLAT_CFG_H
/*---------------------------------------------------------------------------
                                   Includes
 ---------------------------------------------------------------------------*/
#include <stdlib.h>
#include <cpl.h>
#include "sinfo_globals.h"
/*---------------------------------------------------------------------------
                                   Defines
 ---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------
                                   New types
 ---------------------------------------------------------------------------*/
/*
  prepare lamp flat fields blackboard container

  This structure holds all information related to the flatfield handling
  routine. It is used as a container for the flux of ancillary data,
  computed values, and algorithm status. Pixel flux is separated from
  the blackboard.
  */

typedef struct flat_config {
/*-------General---------*/
        char inFile[FILE_NAME_SZ] ; /* file name of the file containing the 
                                       list of all input frames */
        char outName[FILE_NAME_SZ] ; /* output name of resulting fits 
                                        wavelength map */
        char ** framelist ; /* list of frames */
        int  * frametype ; /* list of frame types on or off */
        int  * frameposition ; /* list of grating positions */  
        int    contains_sky ; /* indicates if off or sky frames were exposed */
     int    contains_dither ; /* indicates if spectral dithering was applied */
        int    nframes ; /* number of frames in frame list */
        int    nobj ;  /* number of object frames in frame list */
        int    noff ;  /* number of off frames in frame list */
     int    nditherobj ; /* number of dithered object frames in frame list */
        int    nditheroff ; /* number of dithered off frames in frame list */
/*------ CleanMean ------*/
        /* percentage of rejected low intensity pixels */
        float loReject ;      
        /* percentage of rejected high intensity pixels */
        float hiReject ;
/*------ BadPixel ------*/
        /* indicator if the bad pixels of the flat field are known and 
           if they should be interpolated or not */
        int interpolInd ;
        /* file name of the bad pixel mask fits file */
        char mask[FILE_NAME_SZ] ;
        /* maximal pixel distance from the bad pixel to which valid 
           pixels are searched for*/ 
        int maxRad ;
        /* file name of the slitlet sinfo_edge position list */
        char slitposList[FILE_NAME_SZ] ;
/*------ BadPix ------*/
        /* indicator if a bad pixel mask should be generated or not */
        int badInd ;
        /* name of the static bad pixel mask to be generated */
        char maskname[FILE_NAME_SZ] ;
        /* factor of noise within which the pixels are used to fit a 
           straight line to the column intensity */
        float sigmaFactor ;
        /* factor of calculated standard deviation beyond which the 
           deviation of a pixel value from the
           median of the 8 nearest neighbors declares a pixel as bad */
        float factor ;
        /* number of iterations of sinfo_median filter */
        int iterations ;
        /* percentage of extreme pixel value to reject when calculating 
           the mean and stdev */
        float badLoReject ;
        float badHiReject ;
        /* pixel coordinate of lower left edge of a rectangle zone 
           from which image statistics are computed */
        int llx ;
        int lly ;
        /* pixel coordinate of upper right edge of a rectangle zone from 
           which image statistics are computed */
        int urx ;
        int ury ;
/*------ Thresh ------*/
        /* indicates if the values beyond threshold values should be 
           marked as bad before proceeding
           to sinfo_median filtering */
        int threshInd ;
        /* factor to the clean standard deviation to define the 
           threshold deviation from the clean mean */
        float meanfactor ;


  /* QC LOG */

  /* FPN */
  int qc_fpn_xmin1;
  int qc_fpn_xmax1;
  int qc_fpn_ymin1;
  int qc_fpn_ymax1;

  int qc_fpn_xmin2;
  int qc_fpn_xmax2;
  int qc_fpn_ymin2;
  int qc_fpn_ymax2;

  int qc_thresh_min;
  int qc_thresh_max;


} flat_config ;
 
/*---------------------------------------------------------------------------
                               Function prototypes
 ---------------------------------------------------------------------------*/
/**
   @name    sinfo_flat_cfg_create()
   @memo    allocate memory for a flat_cfg struct
   @return  pointer to allocated base flat_cfg structure
   @note    only the main (base) structure is allocated
 */

flat_config * 
sinfo_flat_cfg_create(void);
/**
   @name    sinfo_flat_cfg_destroy()
   @memo    deallocate all memory associated with a flat_config data structure
   @param   flat_config to deallocate
   @return  void
*/

void 
sinfo_flat_cfg_destroy(flat_config * sc);

#endif
