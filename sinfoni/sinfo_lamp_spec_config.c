/* $Id: sinfo_lamp_spec_config.c,v 1.5 2012-03-02 08:42:20 amodigli Exp $
 *
 * This file is part of the CPL (Common Pipeline Library)
 * Copyright (C) 2002 European Southern Observatory
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
/*
 * $Author: amodigli $
 * $Date: 2012-03-02 08:42:20 $
 * $Revision: 1.5 $
 * $Name: not supported by cvs2svn $
 */

/****************************************************************
 *   Lamp_Spec Frames Data Reduction Parameter Initialization        *
 ****************************************************************/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include "sinfo_lamp_spec_config.h"
#include "sinfo_globals.h"

/**@{*/
/**
 * @defgroup sinfo_lamp_spec_config Spectroscopic flats manipulation
 *
 * TBD
 */

void
sinfo_lamp_spec_config_add(cpl_parameterlist *list)
{

    cpl_parameter *p;

    if (!list) {
        return;
    }

    /* Input file name */
    /* Output file name */
    /* output name of resulting fits wavelength map */
    p = cpl_parameter_new_value("sinfoni.lamp_spec.output_filename",
                    CPL_TYPE_STRING,
                    "Output File Name: ",
                    "sinfoni.lamp_spec",
                    "out_flatspec.fits");


    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"out-lampspec_filename");
    cpl_parameterlist_append(list, p);


    /* number of coefficients for the polynomial interpolation */
    p = cpl_parameter_new_value("sinfoni.lamp_spec.ncoeffs",
                    CPL_TYPE_INT,
                    "No of polynomial coeffs",
                    "sinfoni.lamp_spec",
                    3);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_spec-ncoeffs");
    cpl_parameterlist_append(list, p);

    /* number of image rows in the resampled frame
    (1280 for single frames, 2560 for interleaved frames) */
    p = cpl_parameter_new_value("sinfoni.lamp_spec.nrows",
                    CPL_TYPE_INT,
                    "No of image rows in resampled frame",
                    "sinfoni.lamp_spec",
                    SINFO_RESAMP_NROWS);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_spec-nrows");
    cpl_parameterlist_append(list, p);



    /*Reconstruction */
    /* the fraction [0...1] of rejected low intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_range("sinfoni.lamp_spec.lower_rejection",
                    CPL_TYPE_DOUBLE,
                    "lower rejection",
                    "sinfoni.lamp_spec",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_spec-low_rejection");
    cpl_parameterlist_append(list, p);

    /* the fraction [0...1] of rejected high intensity pixels when taking
the average of columns */
    p = cpl_parameter_new_range("sinfoni.lamp_spec.higher_rejection",
                    CPL_TYPE_DOUBLE,
                    "high rejection",
                    "sinfoni.lamp_spec",
                    0.1,0.0,1.0);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_spec-high_rejection");
    cpl_parameterlist_append(list, p);


    p = cpl_parameter_new_value("sinfoni.lamp_spec.counts_to_intensity",
                    CPL_TYPE_DOUBLE,
                    "Counts To Intensity",
                    "sinfoni.lamp_spec",
                    1.);

    cpl_parameter_set_alias(p, CPL_PARAMETER_MODE_CLI,"lamp_spec-cnt2int");
    cpl_parameterlist_append(list, p);


}
/**@}*/
