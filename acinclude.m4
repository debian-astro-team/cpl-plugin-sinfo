# LIB_CHECK_FFTW
#------------------
# Checks for the fftw library and header files.
AC_DEFUN([LIB_CHECK_FFTW],
[

    AC_MSG_CHECKING([for fftw])

    detmon_fftw_check_header="fftw3.h"
    detmon_fftw_check_lib="libfftw3f.a"

    detmon_fftw_includes=""
    detmon_fftw_libraries=""

    AC_ARG_WITH(fftw,
                AS_HELP_STRING([--with-fftw],
                               [location where fftw is installed]),
                [
                    detmon_with_fftw_includes=$withval/include
                    detmon_with_fftw_libs=$withval/lib
                ])

    AC_ARG_WITH(fftw-includes,
                AS_HELP_STRING([--with-fftw-includes],
                               [location of the fftw header files]),
                detmon_with_fftw_includes=$withval)

    AC_ARG_WITH(fftw-libs,
                AS_HELP_STRING([--with-fftw-libs],
                               [location of the fftw library]),
                detmon_with_fftw_libs=$withval)

    AC_ARG_ENABLE(fftw-test,
                  AS_HELP_STRING([--disable-fftw-test],
                                 [disables checks for the fftw library and headers]),
                  detmon_enable_fftw_test=$enableval,
                  detmon_enable_fftw_test=yes)


    if test "x$detmon_enable_fftw_test" = xyes; then

        # Check for the fftw includes

        if test -z "$detmon_with_fftw_includes"; then
            detmon_fftw_incdirs="/opt/fftw/include /usr/local/include /usr/include"

            test -n "$CPLDIR" && detmon_fftw_incdirs="$CPLDIR/include \
                $detmon_fftw_incdirs"

            test -n "$FFTWDIR" && detmon_fftw_incdirs="$FFTWDIR/include \
                $detmon_fftw_incdirs"
        else
            detmon_fftw_incdirs="$detmon_with_fftw_includes"
        fi

        ESO_FIND_FILE($detmon_fftw_check_header, $detmon_fftw_incdirs,
                      detmon_fftw_includes)


        # Check for the fftw library

        if test -z "$detmon_with_fftw_libs"; then
            detmon_fftw_libdirs="/opt/fftw/lib /usr/local/lib /usr/lib"

            test -n "$CPLDIR" && detmon_fftw_libdirs="$CPLDIR/lib \
                $detmon_fftw_libdirs"

            test -n "$FFTWDIR" && detmon_fftw_libdirs="$FFTWDIR/lib \
                $detmon_fftw_libdirs"
        else
            detmon_fftw_libdirs="$detmon_with_fftw_libs"
        fi

        ESO_FIND_FILE($detmon_fftw_check_lib, $detmon_fftw_libdirs,
                      detmon_fftw_libraries)


        if test x"$detmon_fftw_includes" = xno || \
            test x"$detmon_fftw_libraries" = xno; then
            detmon_fftw_notfound=""

            if test x"$detmon_fftw_includes" = xno; then
                if test x"$detmon_fftw_libraries" = xno; then
                    detmon_fftw_notfound="(headers and libraries)"
                else
                    detmon_fftw_notfound="(headers)"
                fi
            else
                detmon_fftw_notfound="(libraries)"
            fi

            AC_MSG_ERROR([fftw $detmon_fftw_notfound was not found on your system. Please check!])
        else
            AC_MSG_RESULT([libraries $detmon_fftw_libraries, headers $detmon_fftw_includes])
        fi

        # Set up the symbols

        FFTW_INCLUDES="-I$detmon_fftw_includes"
        FFTW_LDFLAGS="-L$detmon_fftw_libraries"
        LDFLAGS="$LDFLAGS $FFTW_LDFLAGS"
        AC_CHECK_LIB([fftw3f], [main], [LIBFFTWC="-lfftw3f"], AC_MSG_ERROR([Library fftwcblas not found]))
	AC_DEFINE_UNQUOTED(HAVE_FFTW, 1, [Define to 1 if you have FFTW v. 3.X])
        LIBS="$LIBS $LIBFFTWC"
        LIBFFTW="$LIBFFTWC"
    else
        AC_MSG_RESULT([disabled])
        AC_MSG_WARN([fftw checks have been disabled! This package may not build!])
        FFTW_INCLUDES=""
        FFTW_LDFLAGS=""
        LIBFFTW=""
    fi

    # To be removed after CPL switched to fftw
    FFTW_INCLUDES="$FFTW_INCLUDES"
    FFTW_LDFLAGS="$FFTW_LDFLAGS"
    LIBFFTW="$LIBFFTW"

    AC_SUBST(FFTW_INCLUDES)
    AC_SUBST(FFTW_LDFLAGS)
    AC_SUBST(LIBFFTW)

])


# SINFONI_SET_PREFIX(PREFIX)
#---------------------------
AC_DEFUN([SINFONI_SET_PREFIX],
[
    unset CDPATH
    # make $PIPE_HOME the default for the installation
    AC_PREFIX_DEFAULT($1)

    if test "x$prefix" = "xNONE"; then
        prefix=$ac_default_prefix
        ac_configure_args="$ac_configure_args --prefix $prefix"
    fi

    if test "x$exec_prefix" = "xNONE"; then
        exec_prefix=$prefix
    fi

])


# SINFONI_SET_VERSION_INFO(VERSION, [CURRENT], [REVISION], [AGE])
#----------------------------------------------------------------
# Setup various version information, especially the libtool versioning
AC_DEFUN([SINFONI_SET_VERSION_INFO],
[
    sinfoni_version=`echo "$1" | sed -e 's/[[a-z,A-Z]].*$//'`

    sinfoni_major_version=`echo "$sinfoni_version" | \
        sed 's/\([[0-9]]*\).\(.*\)/\1/'`
    sinfoni_minor_version=`echo "$sinfoni_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\)\(.*\)/\2/'`
    sinfoni_micro_version=`echo "$sinfoni_version" | \
        sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    if test -z "$sinfoni_major_version"; then
        sinfoni_major_version=0
    fi

    if test -z "$sinfoni_minor_version"; then
        sinfoni_minor_version=0
    fi

    if test -z "$sinfoni_micro_version"; then
        sinfoni_micro_version=0
    fi

    SINFONI_VERSION="$sinfoni_version"
    SINFONI_MAJOR_VERSION=$sinfoni_major_version
    SINFONI_MINOR_VERSION=$sinfoni_minor_version
    SINFONI_MICRO_VERSION=$sinfoni_micro_version

    if test -z "$4"; then
        SINFONI_INTERFACE_AGE=0
    else
        SINFONI_INTERFACE_AGE="$4"
    fi

    SINFONI_BINARY_AGE=`expr 100 '*' $SINFONI_MINOR_VERSION + $SINFONI_MICRO_VERSION`
    SINFONI_BINARY_VERSION=`expr 10000 '*' $SINFONI_MAJOR_VERSION + \
                          $SINFONI_BINARY_AGE`

    AC_SUBST(SINFONI_VERSION)
    AC_SUBST(SINFONI_MAJOR_VERSION)
    AC_SUBST(SINFONI_MINOR_VERSION)
    AC_SUBST(SINFONI_MICRO_VERSION)
    AC_SUBST(SINFONI_INTERFACE_AGE)
    AC_SUBST(SINFONI_BINARY_VERSION)
    AC_SUBST(SINFONI_BINARY_AGE)

    AC_DEFINE_UNQUOTED(SINFONI_MAJOR_VERSION, $SINFONI_MAJOR_VERSION,
                       [SINFONI major version number])
    AC_DEFINE_UNQUOTED(SINFONI_MINOR_VERSION, $SINFONI_MINOR_VERSION,
                       [SINFONI minor version number])
    AC_DEFINE_UNQUOTED(SINFONI_MICRO_VERSION, $SINFONI_MICRO_VERSION,
                       [SINFONI micro version number])
    AC_DEFINE_UNQUOTED(SINFONI_INTERFACE_AGE, $SINFONI_INTERFACE_AGE,
                       [SINFONI interface age])
    AC_DEFINE_UNQUOTED(SINFONI_BINARY_VERSION, $SINFONI_BINARY_VERSION,
                       [SINFONI binary version number])
    AC_DEFINE_UNQUOTED(SINFONI_BINARY_AGE, $SINFONI_BINARY_AGE,
                       [SINFONI binary age])

    ESO_SET_LIBRARY_VERSION([$2], [$3], [$4])
])


# SINFONI_SET_PATHS
#------------------
# Define auxiliary directories of the installed directory tree.
AC_DEFUN([SINFONI_SET_PATHS],
[

    if test -z "$plugindir"; then
        plugindir='${libdir}/esopipes-plugins/${PACKAGE}-${VERSION}'
    fi

    if test -z "$privatelibdir"; then
        privatelibdir='${libdir}/${PACKAGE}-${VERSION}'
    fi

    if test -z "$pipedocsdir"; then
        pipedocsdir='${datadir}/doc/esopipes/${PACKAGE}-${VERSION}'
    fi

    htmldir='${pipedocsdir}/html'

    if test -z "$confdir"; then
       configdir='${datadir}/${PACKAGE}/config'
    fi

    if test -z "$wkfextradir"; then
        wkfextradir='${datadir}/esopipes/${PACKAGE}-${VERSION}/reflex'
    fi

    if test -z "$wkfcopydir"; then
        wkfcopydir='${datadir}/reflex/workflows/${PACKAGE}-${VERSION}'
    fi

    AC_SUBST(plugindir)
    AC_SUBST(privatelibdir)
    AC_SUBST(htmldir)
    AC_SUBST(pipedocsdir)
    AC_SUBST(configdir)
    AC_SUBST(wkfextradir)
    AC_SUBST(wkfcopydir)

    # Define a preprocesor symbol for the plugin search paths

    AC_DEFINE_UNQUOTED(SINFONI_PLUGIN_DIR, "esopipes-plugins",
                       [Plugin directory tree prefix])

    eval plugin_dir="$plugindir"
    plugin_path=`eval echo $plugin_dir | \
                sed -e "s/\/${PACKAGE}-${VERSION}.*$//"`

    AC_DEFINE_UNQUOTED(SINFONI_PLUGIN_PATH, "$plugin_path",
                       [Absolute path to the plugin directory tree])

])


# SINFONI_CREATE_SYMBOLS
#-----------------------
# Define include and library related makefile symbols
AC_DEFUN([SINFONI_CREATE_SYMBOLS],
[

    # Symbols for package include file and library search paths

    SINFONI_INCLUDES='-I$(top_srcdir)/sinfoni'
    IRPLIB_INCLUDES='-I$(top_srcdir)/irplib'

    SINFONI_LDFLAGS='-L$(top_builddir)/sinfoni'
    IRPLIB_LDFLAGS='-I$(top_builddir)/irplib'

    # Library aliases

    LIBSINFONI='$(top_builddir)/sinfoni/libsinfo.la'
    LIBIRPLIB='$(top_builddir)/irplib/libirplib.la'

    # Substitute the defined symbols

    AC_SUBST(SINFONI_INCLUDES)
    AC_SUBST(SINFONI_LDFLAGS)
    AC_SUBST(IRPLIB_INCLUDES)
    AC_SUBST(IRPLIB_LDFLAGS)

    AC_SUBST(LIBSINFONI)
    AC_SUBST(LIBIRPLIB)

    # Check for CPL and user defined libraries
    AC_REQUIRE([CPL_CHECK_LIBS])
    AC_REQUIRE([ESO_CHECK_EXTRA_LIBS])

    all_includes='$(SINFONI_INCLUDES) $(IRPLIB_INCLUDES) $(CPL_INCLUDES) $(CX_INCLUDES) $(EXTRA_INCLUDES)'
    all_ldflags='$(SINFONI_LDFLAGS) $(IRPLIB_LDFLAGS) $(CPL_LDFLAGS) $(CX_LDFLAGS) $(EXTRA_LDFLAGS)'

    AC_SUBST(all_includes)
    AC_SUBST(all_ldflags)
])


