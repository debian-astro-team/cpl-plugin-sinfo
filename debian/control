Source: cpl-plugin-sinfo
Section: science
Priority: optional
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Build-Depends: debhelper (>= 11),
               libcpl-dev (>= 5.3.1),
               libgsl-dev,
               python3,
               python3-astropy,
               python3-cpl,
               python3-sphinx
Standards-Version: 4.1.4
Homepage: https://www.eso.org/sci/software/pipelines/sinfoni/sinfoni-pipe-recipes.html
Vcs-Git: https://salsa.debian.org/debian-astro-team/cpl-plugin-sinfo.git
Vcs-Browser: https://salsa.debian.org/debian-astro-team/cpl-plugin-sinfo

Package: cpl-plugin-sinfo
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: esorex | python3-cpl
Suggests: cpl-plugin-sinfo-calib (= ${binary:Version}), cpl-plugin-sinfo-doc
Multi-Arch: same
Description: ESO data reduction pipeline for the SINFONI instrument
 This is the data reduction pipeline for the SINFONI instrument of the
 Very Large Telescope (VLT) from the European Southern Observatory (ESO).
 .
 SINFONI is a near-infrared (1.1 - 2.45 µm) integral field spectrograph fed by
 an adaptive optics module, currently installed at the Cassegrain focus of
 UT4. The spectrograph operates with 4 gratings (J, H, K, H+K) providing a
 spectral resolution around 2000, 3000, 4000 in J, H, K, respectively, and
 1500 in H+K - each wavelength band fitting fully on the 2048 pixels of the
 Hawaii 2RG (2kx2k) detector in the dispersion direction. The spatial
 resolution is selectable from 0.25", 0.1" to 0.025" per image slice, which
 corresponds to a field-of-view of 8"x8", 3"x3", or 0.8"x0.8"
 respectively. The instrument can be also used for seeing limited open loop
 observations.

Package: cpl-plugin-sinfo-doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Section: doc
Description: ESO data reduction pipeline documentation for SINFONI
 This package contains the HTML documentation and manpages for the data
 reduction pipeline for the SINFONI instrument of the Very Large Telescope
 (VLT) from the European Southern Observatory (ESO).

Package: cpl-plugin-sinfo-calib
Architecture: all
Multi-Arch: foreign
Section: contrib/science
Depends: cpl-plugin-sinfo, wget, ${misc:Depends}
Description: ESO data reduction pipeline calibration data downloader for SINFONI
 This package downloads calibration data of the data reduction pipeline for
 the SINFONI instrument of the Very Large Telescope (VLT) from the European
 Southern Observatory (ESO).
